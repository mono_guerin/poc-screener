const path = require('path');

const basePath = path.resolve(__dirname, "");
const modulesPath = path.join(basePath, "node_modules");

module.exports = {
    entry: './src/demo.js',
    module: {
        rules: [{
            test: /\.(js)$/,
            exclude: modulesPath,
            loader: "babel-loader"
        }]
    },
    output: {
        filename: 'main.js',
        libraryTarget: 'umd',
        path: path.resolve(basePath, 'dist')
    }
}