import Vue from 'vue'
import VueCustomElement from 'vue-custom-element'
import PocScreener from './App.vue'

Vue.use(VueCustomElement)
Vue.customElement('poc-screener', PocScreener)

export default PocScreener
