import merge from 'lodash.merge'
import get from 'lodash.get'
import usBlueprint from 'ec/components/screener/src/config/blueprints/standard-top-nav-us/fund-config.json'
import equityConfig from 'ec/components/screener/src/config/blueprints/standard-top-nav-us/equity-config.json'

const _filtersDefinition = get(usBlueprint, 'blueprints.standardTopNavUsFund.components.filtersSecurities.settings.filtersDefinition')
const _filtersDefinitionEquity = get(equityConfig, 'blueprints.standardTopNavUSEquity.components.filtersSecurities.settings.filtersDefinition')

const filtersDefinition = merge({},
  _filtersDefinition,
  _filtersDefinitionEquity,
  {
    ethicalFunds: {
      type: 'checkbox',
      config: {
        disabled: false,
        horizontal: true,
        placeholder: 'ethicalFundsPlaceholder'
      },
      values: {
        listItems: [
          {
            id: 'true',
            name: 'yes'
          }
        ]
      }
    }
  })

const sections = ['ethicalFunds', 'starRating', 'geoRegion', 'marketCap', 'regionalExposureScope', 'dividendYield', 'shareClass']
const filters = [
  'term'
].concat(sections)

console.log('filtersDefinition', filtersDefinition)

export default merge({}, {
  settings: {
    languageId: 'en-US'
  },
  components: {
    ecScreener: {
      type: 'ecScreener',
      version: '3.0.0',
      blueprint: 'standardTopNavUSEquity',
      components: {
        // Filter 3 cols
        // Results 9 cols
        mainViewContainer: {
          type: 'ecContainer',
          settings: {
            sections: {
              criteria: {
                breakpoints: {
                  defaults: {
                    cols: 2
                  }
                }
              },
              results: {
                breakpoints: {
                  defaults: {
                    cols: 10
                  }
                }
              }
            }
          }
        },
        filtersSecurities: {
          type: 'ecFilters',
          components: {
            buttonToggleMoreFilters: {
              type: 'ecButton',
              settings: {
                visible: false
              }
            },
            containerFilter: {
              type: 'ecContainer',
              settings: {
                breakpoints: {
                  defaults: {
                    groups: {
                      filters: {
                        order: 0,
                        cols: 12,
                        sections: ['term']
                      },
                      all: {
                        order: 1,
                        cols: 12,
                        defaultSection: {
                          cols: 3
                        },
                        // Custom
                        sections
                      },
                      actionButtons: {
                        order: 3,
                        cols: 12,
                        sections: ['actionButtons']
                      }
                    }
                  },
                  medium: {
                    groups: {
                      filters: {
                        order: 0,
                        cols: 12,
                        sections: ['term']
                      },
                      all: {
                        order: 1,
                        cols: 12,
                        defaultSection: {
                          cols: 6
                        },
                        // Custom
                        sections
                      },
                      actionButtons: {
                        order: 3,
                        cols: 12,
                        sections: ['actionButtons']
                      }
                    }
                  },
                  small: {
                    groups: {
                      filters: {
                        order: 0,
                        cols: 12,
                        sections: ['term']
                      },
                      all: {
                        order: 1,
                        cols: 12,
                        defaultSection: {
                          cols: 12
                        },
                        // Custom
                        sections
                      }
                    }
                  }
                }
              }
            },
            inputText: {
              type: 'ecInput',
              labels: {
                placeholder: {
                  en: 'Ticker or Name'
                }
              }
            }
          },
          settings: {
            // Using the custom one
            filters,
            // Using the custom One
            filtersDefinition
          },
          labels: {
            brandingCompanyId: {
              en: 'IT provider'
            },
            brandingCompanyIdPlaceholder: {
              en: 'Select IT Provider'
            },
            categoryId: {
              en: 'Morningstar category'
            }
          }
          // settings: {
          //   _model: {
          //     filterValues: {}
          //   },
          //   filters: [
          //     'term',
          //     'customCategoryId1',
          //     // 'brandingCompanyId', // Doesn't work
          //     // 'categoryId', // Doesn't work
          //     'analystRatingScale'
          //   ],
          //   filtersDefinition: {
          //     customCategoryId1: {
          //       type: 'combo',
          //       values: {
          //         listItems: [
          //           {
          //             id: 5,
          //             name: 'Five'
          //           },
          //           {
          //             id: 4,
          //             name: 'Four'
          //           }
          //         ]
          //       }
          //     }
          //   },
          //   filtersSelectedValue: {
          //     customCategoryId1: 5
          //   }
          // },
          // components: {
          //   inputText: {
          //     type: 'ecInput',
          //     breakpoints: {
          //       defaults: {
          //         cols: 12
          //       }
          //     },
          //     labels: {
          //       placeholder: {
          //         en: 'Ticker or Name'
          //       }
          //     }
          //   }
          // }
        }
      },
      settings: {
        views: ['longTermPerformance', 'snapshot', 'availability', 'minimuns', 'fees'],
        viewDefinitions: {
          snapshot: {
            labelKey: 'snapshotTab',
            keyField: 'secId',
            fields: [
              'name',
              'sectorName',
              'industryName',
              'marketCap',
              'closePrice',
              'priceCurrency'
            ]
          },
          availability: {
            labelKey: 'availabilityTab',
            keyField: 'secId',
            fields: [
              'name',
              'sectorName',
              'industryName',
              'marketCap',
              'closePrice',
              'priceCurrency'
            ]
          },
          minimuns: {
            labelKey: 'minimunsTab',
            keyField: 'secId',
            fields: [
              'name',
              'sectorName',
              'industryName',
              'marketCap',
              'closePrice',
              'priceCurrency'
            ]
          },
          fees: {
            labelKey: 'feesTab',
            keyField: 'secId',
            fields: [
              'name',
              'sectorName',
              'industryName',
              'marketCap',
              'closePrice',
              'priceCurrency'
            ]
          }
        },
        _model: {
          filterValues: {
            api: {
              parameters: {
                transformPipeline: [
                  {
                    name: 'toIwtFilterValues',
                    options: {
                      apiFilterValues: [
                        'SectorId',
                        'BrandingCompanyId'
                      ]
                    }
                  }
                ]
              }
            }
          }
        }
      },
      labels: {
        longTermPerformance: {
          en: 'Performance'
        },
        snapshotTab: {
          en: 'Snapshot'
        },
        availabilityTab: {
          en: 'Availability'
        },
        minimunsTab: {
          en: 'Minimuns'
        },
        feesTab: {
          en: 'Fees'
        }
      }
    }
  }
})
