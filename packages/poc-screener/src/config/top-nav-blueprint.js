export default {
  settings: {
    fieldDefinitions: {
      quantitativeStarRating: {
        fieldSettings: {
          itemTemplate:
            '<div data-ng-if="!obj.quantitativeStarRating" title="{{labels.get(\'quantitativeStarRating\')}}">{{field.format(obj.quantitativeStarRating)}}</div><div title="{{labels.get(\'quantitativeStarRating\')}}{{obj.quantitativeStarRating}}" data-ng-if="obj.quantitativeStarRating" class="mstar-rating__value mstar-rating__value--{{obj.quantitativeStarRating}}"></div>'
        }
      }
    },
    queries: [
      {
        filterKey: 'exchange',
        queryItems: [
          {
            id: 'STCAN',
            universeIds: ['STCAN']
          },
          {
            id: 'STGLOBAL',
            universeIds: ['STGLOBAL']
          },
          {
            id: 'STNA',
            universeIds: ['STNA']
          },
          {
            id: 'STUSA',
            universeIds: ['STUSA']
          }
        ]
      }
    ],
    _model: {
      filterValues: {
        api: {
          headers: {
            authorization: '{{_ tokens().apiGatewayToken _}}'
          },
          url:
            'https://{{baseUrl}}/ec/v1/screener/filterDataSource?universe={{universeId}}&filterDataPoints={{filterDataPoints}}&filters={{filtersSelectedValue}}&langCult={{languageId}}&access_token={{accessToken}}',
          parameters: {
            transformPipeline: [
              {
                name: 'toIwtFilterValues',
                options: {
                  region: 'US'
                }
              }
            ]
          }
        },
        transformPipeline: [
          {
            name: 'fromIwtFilterValues',
            options: {
              filterNamesWithoutAll: ['IndustryId', 'ExchangeId']
            }
          }
        ]
      },
      securities: {
        transformPipeline: ['fromIwtUsaScreener'],
        api: {
          headers: {
            authorization: '{{_ tokens().apiGatewayToken _}}'
          },
          url:
            'https://{{baseUrl}}/ec/v1/screener?startIndex={{page}}&datatype={{datatype}}&results={{perPage}}&sort={{sortField}}&dir={{sortOrder}}&output=json&langcult={{languageId}}&universe={{universeId}}&securityType=st&securityDataPoints={{securityDataPoints}}&filters={{filtersSelectedValue}}&access_token={{accessToken}}',
          parameters: {
            transformPipeline: ['toIwtUsaScreener']
          }
        }
      }
    },
    exportTransformStack: [
      'toIwtUsaScreenerExport',
      'fromIwtUsaScreenerExport'
    ],
    filtersSelectedValue: {
      exchange: { id: 'STNA', name: 'all' },
      exchangeId: { id: 'NASDAQ', name: 'exchanges_nasdaq' },
      totalReturnTimeFrame: { id: 'null', name: 'all' },
      totalReturn: { id: 'null', name: 'all' }
    },
    languageId: 'en-US',
    universeId: 'STNA',
    viewDefinitions: {
      overview: {
        labelKey: 'overview',
        keyField: 'secId',
        fields: [
          'selected',
          'investment',
          'closePrice',
          'marketCap',
          'dividendYield',
          'priceEarningsRatio',
          'pegRatio',
          'marketCountryName',
          'quantitativeStarRating',
          'equityStyleBox',
          'ticker'
        ]
      },
      shortTermPerformance: {
        labelKey: 'shortTermPerformance',
        keyField: 'secId',
        fields: [
          'selected',
          'investment',
          'returnD1',
          'returnW1',
          'returnM1',
          'returnM3',
          'returnM6',
          'returnM0'
        ]
      },
      longTermPerformance: {
        labelKey: 'longTermPerformance',
        keyField: 'secId',
        fields: [
          'selected',
          'investment',
          'returnM12',
          'returnM36',
          'returnM60',
          'returnM120'
        ]
      }
    }
  },
  components: {
    filtersSecurities: {
      type: 'ecFilters',
      blueprint: 'standardHorizontalEquity',
      settings: {
        filters: [
          'term',
          'exchangeId',
          'starRating',
          'marketCap',
          'dividendYield',
          'peRatio',
          'peg',
          'sectorId',
          'industryId',
          'pbRatio',
          'psRatio',
          'totalReturnTimeFrame',
          'totalReturn',
          'economicMoat',
          'stewardshipGrade',
          'roattm',
          'roettm',
          'revenueGrowth3Y',
          'epsGrowth3YYear1',
          'debtEquityRatio',
          'netMargin',
          'equityStyle'
        ],
        filtersDefinition: {
          dividendYield: {
            type: 'combo',
            values: {
              listItems: [
                {
                  id: 'null',
                  name: 'all'
                },
                {
                  id: '{"min":0,"max":1}',
                  name: '0% - 1%'
                },
                {
                  id: '{"min":1,"max":2}',
                  name: '1% - 2%'
                },
                {
                  id: '{"min":2,"max":3}',
                  name: '2% - 3%'
                },
                {
                  id: '{"min":3,"max":4}',
                  name: '3% - 4%'
                },
                {
                  id: '{"min":4,"max":null}',
                  name: '> 4%'
                }
              ]
            }
          },
          exchangeId: {
            type: 'combo',
            values: {
              listItems: [
                { id: 'NASDAQ', name: 'exchanges_nasdaq' },
                {
                  id: 'NEW YORK STOCK EXCHANGE, INC.',
                  name: 'exchanges_nyse'
                },
                { id: 'OTC MARKETS', name: 'exchanges_otc' },
                {
                  id: 'CANADIAN NATIONAL STOCK EXCHANGE',
                  name: 'exchanges_cnsx'
                },
                {
                  id: 'TORONTO STOCK EXCHANGE',
                  name: 'exchanges_toronto'
                },
                { id: 'TSX VENTURE EXCHANGE', name: 'exchanges_tsx_v' }
              ]
            }
          },
          marketCap: {
            type: 'combo',
            values: {
              listItems: [
                { id: 'null', name: 'all' },
                { id: '{ "min": null, "max": 150 }', name: '< 150m' },
                { id: '{ "min": 150, "max": 500 }', name: '150m ~ 500m' },
                { id: '{ "min": 500, "max": 1000 }', name: '500m ~ 1bn' },
                {
                  id: '{ "min": 1000, "max": 10000 }',
                  name: '1bn ~ 10bn'
                },
                { id: '{ "min": 10000, "max": null }', name: '> 10bn' }
              ]
            }
          },
          starRating: {
            type: 'combo',
            values: {
              listItems: [
                { id: 'null', name: 'all' },
                { id: [5], name: 'fiveStars' },
                { id: [4, 5], name: 'fourStars' },
                { id: [3, 4, 5], name: 'threeStars' },
                { id: [2, 3, 4, 5], name: 'twoStars' },
                { id: [1, 2, 3, 4, 5], name: 'oneStar' }
              ]
            }
          },
          totalReturnTimeFrame: {
            type: 'combo',
            values: {
              listItems: [
                {
                  id: 'returnM0',
                  name: 'ytd'
                },
                {
                  id: 'returnM12',
                  name: 'oneYear'
                },
                {
                  id: 'returnM36',
                  name: 'threeYears'
                },
                {
                  id: 'returnM60',
                  name: 'fiveYears'
                },
                {
                  id: 'returnM120',
                  name: 'tenYears'
                }
              ]
            }
          }
        }
      },
      components: {
        containerFilter: {
          type: 'ecContainer',
          settings: {
            breakpoints: {
              defaults: {
                groups: {
                  all: {
                    sections: [
                      'exchangeId',
                      'starRating',
                      'sectorId',
                      'industryId',
                      'marketCap',
                      'dividendYield',
                      'peRatio',
                      'peg'
                    ]
                  }
                }
              },
              medium: {
                groups: {
                  all: {
                    sections: [
                      'exchangeId',
                      'starRating',
                      'sectorId',
                      'industryId',
                      'marketCap',
                      'dividendYield',
                      'peRatio',
                      'peg'
                    ]
                  }
                }
              },
              small: {
                groups: {
                  all: {
                    sections: [
                      'exchangeId',
                      'starRating',
                      'sectorId',
                      'industryId',
                      'marketCap',
                      'dividendYield',
                      'peRatio',
                      'peg'
                    ]
                  }
                }
              }
            }
          }
        }
      }
    },
    viewListSecurities: {
      type: 'ecTable',
      settings: {
        _model: {
          rows: {
            dataSourceType: 'api',
            transformPipeline: [
              {
                name: 'fromIwtUsaScreener',
                options: {
                  calledFromViewList: true
                }
              }
            ],
            api: {
              headers: {
                authorization: '{{_ tokens(_).apiGatewayToken _}}'
              },
              url:
                'https://{{baseUrl}}/ec/v1/screener?startIndex={{page}}&datatype={{datatype}}&results={{perPage}}&sort={{sortField}}&dir={{sortOrder}}&output=json&langcult={{languageId}}&universe={{universeId}}&securityDataPoints={{securityDataPoints}}&filters={{filtersSelectedValue}}&access_token={{accessToken}}',
              parameters: {
                transformPipeline: [
                  {
                    name: 'toIwtUsaScreenerViewList'
                  }
                ]
              }
            }
          }
        }
      }
    }
  },
  labels: {
    exchanges_cnsx: {
      en: 'Canadian National Stock Exchange'
    },
    exchanges_otc: {
      en: 'OTC Markets'
    },
    exchanges_tsx_v: {
      en: 'TSX Venture Exchange'
    },
    exchangeId: {
      en: 'Market'
    }
  }
}
