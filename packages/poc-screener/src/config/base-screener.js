export default {
  settings: {
    _parameters: {
      allowFetchingSavedLists: {
        binding: '{{_ model().showViewList _}}'
      },
      'ticker.currency': {
        binding: '{{_ parameters().currencyId _}}'
      },
      fixFirstRowSecId: {
        url: null
      }
    },
    dataPoints: {
      iwtIntlScreener: {
        aboveAvg: 'AboveAvg',
        avg: 'Avg',
        actualManagementFee: 'ActualManagementFee',
        administratorCompanyName: 'AdministratorCompanyName',
        aimcCategory: 'AIMCCategory',
        alphaM36: 'AlphaM36',
        analystRatingScale: 'AnalystRatingScale',
        annualReportOngoingCharge: 'AnnualReportOngoingCharge',
        asisaSectorName: 'ASISASectorName',
        averageCreditQuality: 'AverageCreditQualityCode',
        averageMarketCapital: 'AverageMarketCapital',
        avgVal1Y: 'AvgValGBP1Y',
        belowAvg: 'BelowAvg',
        betaM36: 'BetaM36',
        bondClosePrice: 'BondClosePrice',
        bondIndustryId: 'BondIndustryId',
        bondStyleBox: 'BondStyleBox',
        businessRisk: 'BusinessRisk',
        callable: 'Callable',
        categoryName: 'CategoryName',
        closePrice: 'ClosePrice',
        closePriceDate: 'ClosePriceDate',
        collectedSRRI: 'CollectedSRRI',
        couponFrequency: 'CouponFrequency',
        couponRate: 'CouponRate',
        customAdditionalBuyFee: 'CustomAdditionalBuyFee',
        customAttributes1: 'CustomAttributes1',
        customAttributes2: 'CustomAttributes2',
        customBuyFee: 'CustomBuyFee',
        customCategoryId2Name: 'CustomCategoryId2Name',
        customCategoryId3Name: 'CustomCategoryId3Name',
        customCategoryIdName: 'CustomCategoryIdName',
        customExternalURL1: 'CustomExternalURL1',
        customExternalURL2: 'CustomExternalURL2',
        customExternalURL3: 'CustomExternalURL3',
        customIsClosed: 'CustomIsClosed',
        customIsFavourite: 'CustomIsFavourite',
        customIsRecommended: 'CustomIsRecommended',
        customMarketCommentary: 'CustomMarketCommentary',
        customMinimumPurchaseAmount: 'CustomMinimumPurchaseAmount',
        customSellFee: 'CustomSellFee',
        customValue11: 'CustomValue11',
        customValue2: 'CustomValue2',
        debtEquityRatio: 'DebtEquityRatio',
        debtType: 'DebtType',
        denominationMultiple: 'denominationMultiple',
        dividendYield: 'DividendYield',
        ebtMarginYear1: 'EBTMarginYear1',
        economicMoat: 'EconomicMoat',
        effectiveDuration: 'EffectiveDuration',
        epsGrowth3YYear1: 'EPSGrowth3YYear1',
        epsGrowth5Y: 'EPSGrowth5Y',
        equityResearchStarRating: 'EquityResearchStarRating',
        equityStyleBox: 'EquityStyleBox',
        estimatedCumFairDiscount: 'EstimatedCumFairDiscount',
        estimatedCumFairNAV: 'EstimatedCumFairNAV',
        exchangeCode: 'ExchangeCode',
        exchangeId: 'ExchangeId',
        expenseRatio: 'ExpenseRatio',
        expertiseBasic: 'ExpertiseBasic',
        expertiseAdvanced: 'ExpertiseAdvanced',
        expertiseInformed: 'ExpertiseInformed',
        fairValueEstimate: 'FairValueEstimate',
        feeLevel: 'FeeLevel',
        fundCode: 'FundCode',
        fundTNAV: 'FundTNAV',
        gbrReturnD1: 'GBRReturnD1',
        gbrReturnM0: 'GBRReturnM0',
        gbrReturnM1: 'GBRReturnM1',
        gbrReturnM3: 'GBRReturnM3',
        gbrReturnM6: 'GBRReturnM6',
        gbrReturnM12: 'GBRReturnM12',
        gbrReturnM36: 'GBRReturnM36',
        gbrReturnM60: 'GBRReturnM60',
        gbrReturnM120: 'GBRReturnM120',
        gbrReturnW1: 'GBRReturnW1',
        high: 'High',
        holdingTypeId: 'HoldingTypeId',
        imaSectorName: 'ImaSectorName',
        inceptionDate: 'InceptionDate',
        indexFund: 'IndexFund',
        indexingApproach: 'IndexingApproach',
        industryName: 'IndustryName',
        initialPurchase: 'InitialPurchase',
        initialPurchaseBaseCurrencyId: 'InitialPurchaseBaseCurrencyId',
        initialPurchaseCurrency: 'InitialPurchaseCurrency',
        investorTypeEligibleCounterparty:
          'InvestorTypeEligibleCounterparty',
        investorTypeProfessional: 'InvestorTypeProfessional',
        investorTypeRetail: 'InvestorTypeRetail',
        isin: 'Isin',
        issuerCountry: 'IssuerCountry',
        issuerName: 'IssuerName',
        legalName: 'LegalName',
        largestRegion: 'LargestRegion',
        largestSector: 'LargestSector',
        low: 'Low',
        macaulayDuration: 'MacaulayDuration',
        managementFee: 'ManagementFee',
        managerTenure: 'ManagerTenure',
        marketCap: 'MarketCap',
        marketCountryName: 'MarketCountryName',
        maturityDate: 'MaturityDate',
        maxDeferredLoad: 'MaxDeferredLoad',
        maxFrontEndLoad: 'MaxFrontEndLoad',
        maximumEntryCostAcquired: 'MaximumEntryCostAcquired',
        maximumExitCostAcquired: 'MaximumExitCostAcquired',
        maxRedemptionFee: 'MaxRedemptionFee',
        minimumDenomination: 'MinimumDenomination',
        modifiedDuration: 'ModifiedDuration',
        moodysRating: 'MoodysRating',
        morningstarRating: 'MorningstarRating',
        morningstarRiskM255: 'MorningstarRiskM255',
        name: 'Name',
        navPublishedDate: 'NavPublishedDate',
        netMargin: 'NetMargin',
        ongoingCharge: 'OngoingCharge',
        ongoingCostActual: 'OngoingCostActual',
        pegRatio: 'PEGRatio',
        peRatio5YrAverage: 'PERatio5YrAverage',
        performanceFee: 'PerformanceFee',
        performanceFeeActual: 'PerformanceFeeActual',
        price: 'Price',
        priceBookRatio: 'PBRatio',
        priceCurrency: 'PriceCurrency',
        priceEarningsRatio: 'PERatio',
        priceSalesRatio: 'PSRatio',
        pricingFrequencyName: 'PricingFrequencyName',
        primaryBenchmarkName: 'PrimaryBenchmarkName',
        puttable: 'Puttable',
        qR_MonthDate: 'QR_MonthDate',
        qR_GBRReturnM12_1: 'QR_GBRReturnM12_1',
        qR_GBRReturnM12_2: 'QR_GBRReturnM12_2',
        qR_GBRReturnM12_3: 'QR_GBRReturnM12_3',
        qR_GBRReturnM12_4: 'QR_GBRReturnM12_4',
        qR_GBRReturnM12_5: 'QR_GBRReturnM12_5',
        quantitativeStarRating: 'QuantitativeStarRating',
        r2M36: 'R2M36',
        returnD1: 'ReturnD1',
        returnM0: 'ReturnM0',
        returnM1: 'ReturnM1',
        returnM3: 'ReturnM3',
        returnM6: 'ReturnM6',
        returnM12: 'ReturnM12',
        returnM36: 'ReturnM36',
        returnM36UA: 'ReturnM36UA',
        returnM60: 'ReturnM60',
        returnM60UA: 'ReturnM60UA',
        returnM120: 'ReturnM120',
        returnM255UA: 'ReturnM255UA',
        returnProfileGrowth: 'ReturnProfileGrowth',
        returnProfileHedging: 'ReturnProfileHedging',
        returnProfileIncome: 'ReturnProfileIncome',
        returnProfileOther: 'ReturnProfileOther',
        returnProfilePreservation: 'ReturnProfilePreservation',
        returnW1: 'ReturnW1',
        revenueGrowth3Y: 'RevenueGrowth3Y',
        riskRating: 'RiskRating',
        roattm: 'ROATTM',
        roettm: 'ROETTM',
        roeYear1: 'ROEYear1',
        roicYear1: 'ROICYear1',
        salesYield: 'SalesYield',
        secId: 'SecId',
        sectorName: 'SectorName',
        sedol: 'Sedol',
        sharpeM36: 'SharpeM36',
        standardDeviationM12: 'StandardDeviationM12',
        standardDeviationM36: 'StandardDeviationM36',
        standardDeviationM60: 'StandardDeviationM60',
        starRatingM255: 'StarRatingM255',
        stewardshipGrade: 'StewardshipGrade',
        subordinate: 'Subordinate',
        sustainabilityRank: 'SustainabilityRank',
        tenforeId: 'TenforeId',
        ticker: 'Ticker',
        totalAssets: 'TotalAssets',
        totalMarketValue: 'TotalMarketValue',
        trackRecordExtension: 'TrackRecordExtension',
        trailingDate: 'TrailingDate',
        transactionFeeActual: 'TransactionFeeActual',
        universe: 'Universe',
        yearsToMaturity: 'YearsToMaturity',
        yield_M12: 'Yield_M12',
        yieldToMaturity: 'YieldToMaturity'
      },
      iwtUsaScreener: {
        alpha: {
          type: 'array'
        },
        avgVolume1M: {
          type: 'array'
        },
        avgVolume3M: {
          type: 'array'
        },
        avgVolume1Yrs: {
          type: 'array'
        },
        baseCurrencyId: {
          type: 'text'
        },
        beta: {
          type: 'array'
        },
        brandingCompanyId: {
          type: 'text'
        },
        broadAsset: {
          type: 'text'
        },
        businessCountry: {
          type: 'text'
        },
        categoryId: {
          type: 'text'
        },
        creditRating: {
          type: 'text'
        },
        dailyClosingPrice: {
          type: 'array'
        },
        debtEquityRatio: {
          type: 'json'
        },
        dividendDistributionFrequency: {
          type: 'text'
        },
        dividendYield: {
          type: 'json'
        },
        economicMoat: {
          type: 'text'
        },
        epsGrowth3YYear1: {
          type: 'json'
        },
        epsGrowth5Y: {
          type: 'json'
        },
        equityStyle: {
          type: 'array'
        },
        etfActivelyManaged: {
          type: 'json'
        },
        etfIndex: {
          type: 'json'
        },
        etfInverse: {
          type: 'json'
        },
        etfLeveraged: {
          type: 'json'
        },
        etfOptionEligible: {
          type: 'json'
        },
        exchangeId: {
          type: 'text'
        },
        expenseRatio: {
          type: 'json'
        },
        fundSize: {
          type: 'json'
        },
        fundStyle: {
          type: 'array'
        },
        fvUncertainty: {
          type: 'text'
        },
        inceptionDate: {
          type: 'array'
        },
        industryId: {
          type: 'text'
        },
        leveragedFund: {
          type: 'text'
        },
        managementExpenseRatio: {
          type: 'array'
        },
        managementStyle: {
          type: 'array'
        },
        marketCap: {
          type: 'json'
        },
        mstarAnalystRating: {
          type: 'array'
        },
        mstarCategory: {
          type: 'text'
        },
        netIncomeGrowth5Y: {
          type: 'json'
        },
        netMargin: {
          type: 'json'
        },
        numOfHoldings: {
          type: 'array'
        },
        pbRatio: {
          type: 'json'
        },
        peRatio: {
          type: 'json'
        },
        peRatioForward: {
          type: 'json'
        },
        peg: {
          type: 'json'
        },
        percAssetsTop10Holdings: {
          type: 'array'
        },
        pillarRatings: {
          type: 'array'
        },
        pillarRatingsParent: {
          type: 'text'
        },
        pillarRatingsPeople: {
          type: 'text'
        },
        pillarRatingsPerformance: {
          type: 'text'
        },
        pillarRatingsPrice: {
          type: 'text'
        },
        pillarRatingsProcess: {
          type: 'text'
        },
        primaryBenchmarkName: {
          type: 'text'
        },
        price52WeeksHigh: {
          type: 'array'
        },
        price52WeeksLow: {
          type: 'array'
        },
        priceOverFV: {
          type: 'array'
        },
        psRatio: {
          type: 'json'
        },
        reached52WeeksHigh: {
          type: 'array'
        },
        reached52WeeksLow: {
          type: 'array'
        },
        regionAfricaMiddleEast: {
          type: 'json'
        },
        regionAmericas: {
          type: 'json'
        },
        regionAustralAsia: {
          type: 'json'
        },
        regionDevelopedAsia: {
          type: 'json'
        },
        regionEmerging1: {
          type: 'json'
        },
        regionEmerging2k: {
          type: 'json'
        },
        regionEmergingAsia: {
          type: 'json'
        },
        regionEmergingEurope: {
          type: 'json'
        },
        regionEuropeDeveloping: {
          type: 'json'
        },
        regionGreaterAsia: {
          type: 'json'
        },
        regionGreaterEurope: {
          type: 'json'
        },
        regionJapan: {
          type: 'json'
        },
        regionLatinAmerica: {
          type: 'json'
        },
        regionNorthAmerica: {
          type: 'json'
        },
        regionUnitedKingdom: {
          type: 'json'
        },
        returnM0: {
          type: 'json'
        },
        returnM1: {
          type: 'json'
        },
        returnM12: {
          type: 'json'
        },
        returnM120: {
          type: 'json'
        },
        returnM255UA: {
          type: 'json'
        },
        returnM3: {
          type: 'json'
        },
        returnM36: {
          type: 'json'
        },
        returnM60: {
          type: 'json'
        },
        returnM12QtrEnd: {
          type: 'json'
        },
        returnM36QtrEnd: {
          type: 'json'
        },
        returnM60QtrEnd: {
          type: 'json'
        },
        returnM120QtrEnd: {
          type: 'json'
        },
        returnRatings5Yrs: {
          type: 'array'
        },
        revenueGrowth3Y: {
          type: 'json'
        },
        revenueGrowth5Y: {
          type: 'json'
        },
        riskRatings5Yrs: {
          type: 'array'
        },
        roattm: {
          type: 'json'
        },
        roettm: {
          type: 'json'
        },
        rSquared: {
          type: 'array'
        },
        sectorBasicMaterials: {
          type: 'json'
        },
        sectorCommunicationServices: {
          type: 'json'
        },
        sectorConsumerCyclical: {
          type: 'json'
        },
        sectorConsumerDefensive: {
          type: 'json'
        },
        sectorCyclical: {
          type: 'json'
        },
        sectorDefensive: {
          type: 'json'
        },
        sectorEnergy: {
          type: 'json'
        },
        sectorFinancialServices: {
          type: 'json'
        },
        sectorHealthcare: {
          type: 'json'
        },
        sectorId: {
          type: 'text'
        },
        sectorIndustrials: {
          type: 'json'
        },
        sectorRealEstate: {
          type: 'json'
        },
        sectorregionJapan: {
          type: 'json'
        },
        sectorSensitive: {
          type: 'json'
        },
        sectorTechnology: {
          type: 'json'
        },
        sectorUtilities: {
          type: 'json'
        },
        shareClass: {
          type: 'text'
        },
        loadTransactionFee: {
          type: 'json'
        },
        sharpeRatio: {
          type: 'array'
        },
        standardDeviation: {
          type: 'array'
        },
        standardDeviationM36: {
          type: 'array'
        },
        starRating: {
          type: 'array'
        },
        stewardshipGrade: {
          type: 'text'
        },
        sustainabilityRating: {
          type: 'array'
        },
        term: {
          type: 'text'
        },
        totalAssets: {
          type: 'json'
        },
        trackingError: {
          type: 'array'
        },
        within52Weeks: {
          type: 'json'
        },
        yieldPercent: {
          type: 'array'
        }
      }
    },
    assetType: 'equities',
    assetTypeFields: {
      bonds: ['LegalName', 'Name', 'TenforeId'],
      equities: [
        'LegalName',
        'Name',
        'IndustryName',
        'SectorName',
        'TenforeId'
      ],
      exchangeTradedFunds: ['Name', 'PriceCurrency', 'TenforeId'],
      funds: ['Name', 'PriceCurrency', 'TenforeId'],
      investmentTrust: ['Name', 'TenforeId']
    },
    fieldDefinitions: {
      action: {
        fieldName: 'action'
      },
      actionButtons: {
        fieldName: 'action',
        fieldSettings: {
          itemTemplate:
            '<ec-action-container mstar-component-id="actionContainerButtons[{{obj.secId}}]" data-action-data="obj"></ec-action-container>'
        },
        sortingEnabled: false
      },
      actualManagementFee: {
        headerLabelKey: 'actualManagementFee',
        fieldName: 'actualManagementFee',
        format: 'shortDecimal'
      },
      addToShortlist: {
        headerLabelKey: 'addToShortlist',
        fieldName: 'addToShortlist'
      },
      administratorCompanyName: {
        headerLabelKey: 'administratorCompanyName',
        fieldName: 'administratorCompanyName'
      },
      aimcCategoryId: {
        headerLabelKey: 'aimcCategoryId',
        fieldName: 'aimcCategoryId'
      },
      alphaM36: {
        headerLabelKey: 'alphaM36',
        fieldName: 'alphaM36',
        format: 'shortDecimal'
      },
      analystRatingScale: {
        headerLabelKey: 'analystRatingScale',
        fieldName: 'analystRatingScale'
      },
      annualReportOngoingCharge: {
        headerLabelKey: 'annualReportOngoingCharge',
        fieldName: 'annualReportOngoingCharge',
        format: 'shortDecimal'
      },
      asisaSectorName: {
        headerLabelKey: 'asisaSectorName',
        fieldName: 'asisaSectorName'
      },
      assetAllocationBond: {
        headerLabelKey: 'assetAllocationBond',
        fieldName: 'assetAllocationBond',
        format: 'shortDecimal'
      },
      assetAllocationCash: {
        headerLabelKey: 'assetAllocationCash',
        fieldName: 'assetAllocationCash',
        format: 'shortDecimal'
      },
      assetAllocationEquity: {
        headerLabelKey: 'assetAllocationEquity',
        fieldName: 'assetAllocationEquity',
        format: 'shortDecimal'
      },
      avgVal1Y: {
        headerLabelKey: 'avgVal1Y',
        fieldName: 'avgVal1Y',
        format: 'shortDecimal'
      },
      averageCreditQuality: {
        headerLabelKey: 'averageCreditQuality',
        fieldName: 'averageCreditQuality'
      },
      averageMarketCapital: {
        headerLabelKey: 'averageMarketCapital',
        fieldName: 'averageMarketCapital',
        format: 'shortDecimal'
      },
      averageVolume3Month: {
        headerLabelKey: 'averageVolume3Month',
        fieldName: 'averageVolume3Month',
        format: 'shortDecimal'
      },
      averageVolume1Yr: {
        headerLabelKey: 'averageVolume1Yr',
        fieldName: 'averageVolume1Yr',
        format: 'shortDecimal'
      },
      best3MonthSinceInception: {
        headerLabelKey: 'best3MonthSinceInception',
        fieldName: 'best3MonthSinceInception',
        format: 'shortDecimal'
      },
      betaM36: {
        headerLabelKey: 'betaM36',
        fieldName: 'betaM36',
        format: 'shortDecimal'
      },
      bondClosePrice: {
        headerLabelKey: 'bondClosePrice',
        fieldName: 'bondClosePrice',
        format: 'shortDecimal'
      },
      bondIndustryId: {
        headerLabelKey: 'bondIndustryId',
        fieldName: 'bondIndustryId'
      },
      bondStyleBox: {
        headerLabelKey: 'bondStyleBox',
        fieldName: 'bondStyleBox',
        fieldSettings: {
          headerClass: 'ec-table__cell--trademark-symbol',
          itemTemplate: '{{#../html/bond-style-box.html#}}'
        }
      },
      businessRisk: {
        headerLabelKey: 'uncertaintyRating',
        fieldName: 'businessRisk'
      },
      businessCountry: {
        headerLabelKey: 'businessCountry',
        fieldName: 'businessCountry'
      },
      callable: {
        headerLabelKey: 'callable',
        fieldName: 'callable'
      },
      categoryName: {
        headerLabelKey: 'categoryName',
        fieldName: 'categoryName'
      },
      closePrice: {
        headerLabelKey: 'closePrice',
        format: 'shortDecimal',
        fieldName: 'closePrice'
      },
      closePriceDate: {
        headerLabelKey: 'closePriceDate',
        fieldName: 'closePriceDate'
      },
      closePriceMarkets: {
        headerLabelKey: 'closePriceMarkets',
        format: 'shortDecimal',
        fieldName: 'closePriceMarkets'
      },
      collectedSRRI: {
        headerLabelKey: 'collectedSRRI',
        fieldName: 'collectedSRRI'
      },
      couponFrequency: {
        headerLabelKey: 'couponFrequency',
        fieldName: 'couponFrequency'
      },
      couponRate: {
        headerLabelKey: 'couponRate',
        fieldName: 'couponRate',
        format: 'shortDecimal'
      },
      creditRating: {
        headerLabelKey: 'creditRating',
        fieldName: 'creditRating'
      },
      currency: {
        headerLabelKey: 'currency',
        fieldName: 'currency'
      },
      customAdditionalBuyFee: {
        headerLabelKey: 'customAdditionalBuyFee',
        fieldName: 'customAdditionalBuyFee',
        format: {
          dataType: 'number',
          maximumFractionDigits: 5,
          minimumFractionDigits: 2
        }
      },
      customBuyFee: {
        headerLabelKey: 'customBuyFee',
        fieldName: 'customBuyFee',
        format: 'shortDecimal'
      },
      customCategoryIdName: {
        headerLabelKey: 'customCategoryIdName',
        fieldName: 'customCategoryIdName'
      },
      customCategoryId2Name: {
        headerLabelKey: 'customCategoryId2Name',
        fieldName: 'customCategoryId2Name'
      },
      customCategoryId3Name: {
        headerLabelKey: 'customCategoryId3Name',
        fieldName: 'customCategoryId3Name'
      },
      customMinimumPurchaseAmount: {
        headerLabelKey: 'customMinimumPurchaseAmount',
        fieldName: 'customMinimumPurchaseAmount'
      },
      customSellFee: {
        headerLabelKey: 'customSellFee',
        fieldName: 'customSellFee',
        format: {
          dataType: 'number',
          maximumFractionDigits: 5,
          minimumFractionDigits: 2
        }
      },
      customValue11: {
        headerLabelKey: 'customValue11',
        fieldName: 'customValue11'
      },
      dailyReturn1M: {
        headerLabelKey: 'dailyReturn1M',
        fieldName: 'dailyReturn1M',
        format: 'shortDecimal'
      },
      dailyReturn3M: {
        headerLabelKey: 'dailyReturn3M',
        fieldName: 'dailyReturn3M',
        format: 'shortDecimal'
      },
      dailyReturnYTD: {
        headerLabelKey: 'dailyReturnYTD',
        fieldName: 'dailyReturnYTD',
        format: 'shortDecimal'
      },
      debtEquityRatio: {
        headerLabelKey: 'debtEquityRatio',
        fieldName: 'debtEquityRatio',
        format: 'shortDecimal'
      },
      debtType: {
        headerLabelKey: 'debtType',
        fieldName: 'debtType'
      },
      denominationMultiple: {
        headerLabelKey: 'denominationMultiple',
        fieldName: 'denominationMultiple'
      },
      dividendYield: {
        headerLabelKey: 'dividendYield',
        format: 'shortDecimal',
        fieldName: 'dividendYield'
      },
      ebtMarginYear1: {
        headerLabelKey: 'ebtMarginYear1',
        format: 'shortDecimal',
        fieldName: 'ebtMarginYear1'
      },
      economicMoat: {
        headerLabelKey: 'economicMoat',
        fieldName: 'economicMoat'
      },
      effectiveDuration: {
        headerLabelKey: 'effectiveDuration',
        fieldName: 'effectiveDuration',
        format: 'shortDecimal'
      },
      epsGrowth3YYear1: {
        headerLabelKey: 'epsGrowth3YYear1',
        format: 'shortDecimal',
        fieldName: 'epsGrowth3YYear1'
      },
      epsGrowth5Y: {
        headerLabelKey: 'epsGrowth5Y',
        fieldName: 'epsGrowth5Y',
        format: 'shortDecimal'
      },
      equityResearchStarRating: {
        headerLabelKey: 'equityResearchStarRating',
        fieldName: 'equityResearchStarRating',
        fieldSettings: {
          headerClass: 'ec-table__cell--trademark-symbol',
          itemTemplate: '{{#../html/equity-research-star-rating.html#}}'
        }
      },
      equityStyleBox: {
        headerLabelKey: 'equityStyleBox',
        fieldName: 'equityStyleBox',
        fieldSettings: {
          headerClass: 'ec-table__cell--trademark-symbol',
          itemTemplate: '{{#../html/equity-style-box.html#}}'
        }
      },
      estimatedCumFairDiscount: {
        headerLabelKey: 'estimatedCumFairDiscount',
        format: 'shortDecimal',
        fieldName: 'estimatedCumFairDiscount'
      },
      estimatedCumFairNAV: {
        headerLabelKey: 'estimatedCumFairNAV',
        format: 'shortDecimal',
        fieldName: 'estimatedCumFairNAV'
      },
      exchangeCode: {
        headerLabelKey: 'exchangeCode',
        fieldName: 'exchangeCode'
      },
      exchangeId: {
        headerLabelKey: 'market',
        fieldName: 'exchangeId'
      },
      expenseRatio: {
        headerLabelKey: 'expenseRatio',
        fieldName: 'expenseRatio',
        format: 'shortDecimal'
      },
      expenseRatioGross: {
        headerLabelKey: 'expenseRatioGross',
        fieldName: 'expenseRatioGross',
        format: 'shortDecimal'
      },
      feeLevel: {
        headerLabelKey: 'feeLevel',
        fieldName: 'feeLevel'
      },
      holdingType: {
        headerLabelKey: 'investmentType',
        fieldName: 'holdingTypeId'
      },
      inceptionDate: {
        headerLabelKey: 'inceptionDate',
        fieldName: 'inceptionDate'
      },
      indexFund: {
        headerLabelKey: 'indexFund',
        fieldName: 'indexFund'
      },
      fairValueEstimate: {
        headerLabelKey: 'fairValueEstimate',
        fieldName: 'fairValueEstimate',
        format: 'shortDecimal'
      },
      forwardDividendYield: {
        headerLabelKey: 'forwardDividendYield',
        fieldName: 'forwardDividendYield',
        format: 'shortDecimal'
      },
      fundCode: {
        headerLabelKey: 'fundCode',
        fieldName: 'fundCode'
      },
      fundTNAV: {
        headerLabelKey: 'fundTNAV',
        fieldName: 'fundTNAV',
        format: {
          dataType: 'number',
          abbreviate: {
            mode: 'millions',
            abbreviations: {
              millions: ' '
            }
          },
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        }
      },
      funds_Funds: {
        headerLabelKey: 'funds_Funds',
        fieldName: 'funds_Funds',
        format: {
          dataType: 'number'
        }
      },
      fundSize: {
        headerLabelKey: 'fundSize',
        fieldName: 'fundSize',
        format: 'shortDecimal'
      },
      funds_FixedAssetWeightedFunds: {
        headerLabelKey: 'funds_FixedAssetWeightedFunds',
        fieldName: 'funds_FixedAssetWeightedFunds',
        format: 'shortDecimal'
      },
      fvUncertainty: {
        headerLabelKey: 'fvUncertainty',
        fieldName: 'fvUncertainty'
      },
      gbrReturnD1: {
        headerLabelKey: 'gbrReturnD1',
        fieldName: 'gbrReturnD1',
        format: 'shortDecimal'
      },
      gbrReturnM0: {
        headerLabelKey: 'gbrReturnM0',
        fieldName: 'gbrReturnM0',
        format: 'shortDecimal'
      },
      gbrReturnM1: {
        headerLabelKey: 'gbrReturnM1',
        fieldName: 'gbrReturnM1',
        format: 'shortDecimal'
      },
      gbrReturnM3: {
        headerLabelKey: 'gbrReturnM3',
        fieldName: 'gbrReturnM3',
        format: 'shortDecimal'
      },
      gbrReturnM6: {
        headerLabelKey: 'gbrReturnM6',
        fieldName: 'gbrReturnM6',
        format: 'shortDecimal'
      },
      gbrReturnM12: {
        headerLabelKey: 'gbrReturnM12',
        fieldName: 'gbrReturnM12',
        format: 'shortDecimal'
      },
      gbrReturnM36: {
        headerLabelKey: 'gbrReturnM36',
        fieldName: 'gbrReturnM36',
        format: 'shortDecimal'
      },
      gbrReturnM60: {
        headerLabelKey: 'gbrReturnM60',
        fieldName: 'gbrReturnM60',
        format: 'shortDecimal'
      },
      gbrReturnM120: {
        headerLabelKey: 'gbrReturnM120',
        fieldName: 'gbrReturnM120',
        format: 'shortDecimal'
      },
      gbrReturnW1: {
        headerLabelKey: 'gbrReturnW1',
        fieldName: 'gbrReturnW1',
        format: 'shortDecimal'
      },
      imaSectorName: {
        headerLabelKey: 'imaSectorName',
        fieldName: 'imaSectorName'
      },
      IndexFund: {
        headerLabelKey: 'IndexFund',
        fieldName: 'IndexFund'
      },
      indexingApproach: {
        headerLabelKey: 'indexingApproach',
        fieldName: 'indexingApproach'
      },
      industryName: {
        headerLabelKey: 'industryName',
        fieldName: 'industryName'
      },
      investment: {
        headerLabelKey: 'investment',
        fieldName: 'name',
        fieldSettings: {
          hyperlink: {
            text: 'instrumentName',
            textField: 'name',
            target: '_blank',
            url:
              '/security-report-loader/#/!preview=report-default&currencyId={{1}}&languageId={{2}}?id={{0}}&idCurrencyId=&idType=msid&marketCode=',
            urlFields: ['secId', 'currencyId', 'languageId']
          },
          itemTemplate: '<div mstar-include="templates.hyperLink"></div>'
        }
      },
      isin: {
        headerLabelKey: 'isin',
        fieldName: 'isin'
      },
      initialPurchase: {
        headerLabelKey: 'initialPurchase',
        fieldName: 'initialPurchase'
      },
      initialPurchaseBaseCurrencyId: {
        headerLabelKey: 'initialPurchaseBaseCurrencyId',
        fieldName: 'initialPurchaseBaseCurrencyId'
      },
      initialPurchaseCurrency: {
        headerLabelKey: 'initialPurchaseCurrency',
        fieldName: 'initialPurchaseCurrency'
      },
      issuerCountry: {
        headerLabelKey: 'issuerCountry',
        fieldName: 'issuerCountry'
      },
      issuerName: {
        headerLabelKey: 'issuerName',
        fieldName: 'issuerName'
      },
      largestRegion: {
        headerLabelKey: 'largestRegion',
        fieldName: 'largestRegion'
      },
      largestSector: {
        headerLabelKey: 'largestSector',
        fieldName: 'largestSector'
      },
      legalName: {
        headerLabelKey: 'name',
        fieldName: 'legalName'
      },
      leveragedFund: {
        headerLabelKey: 'leveraged',
        fieldName: 'leveragedFund'
      },
      load: {
        headerLabelKey: 'load',
        fieldName: 'load'
      },
      macaulayDuration: {
        headerLabelKey: 'macaulayDuration',
        fieldName: 'macaulayDuration',
        format: 'shortDecimal'
      },
      managementExpenseRatio: {
        headerLabelKey: 'managementExpenseRatio',
        fieldName: 'managementExpenseRatio',
        format: 'shortDecimal'
      },
      managementFee: {
        headerLabelKey: 'managementFee',
        fieldName: 'managementFee',
        format: 'shortDecimal'
      },
      managerTenure: {
        headerLabelKey: 'managerTenure',
        fieldName: 'managerTenure',
        format: 'shortDecimal'
      },
      marketCap: {
        headerLabelKey: 'marketCap',
        format: {
          dataType: 'number',
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        },
        fieldName: 'marketCap'
      },
      marketCountryName: {
        headerLabelKey: 'marketCountryName',
        fieldName: 'marketCountryName'
      },
      maturityDate: {
        headerLabelKey: 'maturityDate',
        fieldName: 'maturityDate',
        format: 'yyyy-mm-dd'
      },
      maxDeferredLoad: {
        headerLabelKey: 'maxDeferredLoad',
        fieldName: 'maxDeferredLoad',
        format: 'shortDecimal'
      },
      maxFrontEndLoad: {
        headerLabelKey: 'maxFrontEndLoad',
        fieldName: 'maxFrontEndLoad',
        format: 'shortDecimal'
      },
      maximumDeferredLoad: {
        headerLabelKey: 'maximumDeferredLoad',
        fieldName: 'maximumDeferredLoad',
        format: 'shortDecimal'
      },
      maxRedemptionFee: {
        headerLabelKey: 'maxRedemptionFee',
        fieldName: 'maxRedemptionFee',
        format: 'shortDecimal'
      },
      minimumDenomination: {
        headerLabelKey: 'minimumDenomination',
        fieldName: 'minimumDenomination'
      },
      modifiedDuration: {
        headerLabelKey: 'modifiedDuration',
        fieldName: 'modifiedDuration',
        format: 'shortDecimal'
      },
      moodysRating: {
        headerLabelKey: 'moodysRating',
        fieldName: 'moodysRating'
      },
      morningstarRiskM255: {
        headerLabelKey: 'morningstarRiskM255',
        fieldName: 'morningstarRiskM255'
      },
      morningstarRiskRating: {
        headerLabelKey: 'morningstarRiskRating',
        fieldName: 'morningstarRiskRating',
        fieldSettings: {
          itemTemplate:
            "<span data-ng-if='obj.morningstarRiskRating'>{{labels.get('riskRating_' + obj.morningstarRiskRating)}}</span><span data-ng-if='!obj.morningstarRiskRating'>{{'_'}}</span></div>"
        }
      },
      mstarAnalystRating: {
        headerLabelKey: 'mstarAnalystRating',
        fieldName: 'mstarAnalystRating',
        fieldSettings: {
          headerClass: '',
          itemTemplate:
            '<div data-ng-if="!obj.mstarAnalystRating" title="{{labels.get(\'mstarAnalystRating\')}}">{{field.format(obj.mstarAnalystRating)}}</div><div title="{{labels.get(\'mstarAnalystRating\')}}{{obj.mstarAnalystRating}}" data-ng-if="obj.mstarAnalystRating" class="analyst-rating__value analyst-rating__value--{{obj.mstarAnalystRating}}"></div>'
        }
      },
      name: {
        headerLabelKey: 'name',
        fieldName: 'name'
      },
      navPublishedDate: {
        headerLabelKey: 'navPublishedDate',
        fieldName: 'navPublishedDate'
      },
      netMargin: {
        headerLabelKey: 'netMargin',
        fieldName: 'netMargin',
        format: 'shortDecimal'
      },
      netIncomeGrowth5Y: {
        headerLabelKey: 'netIncomeGrowth5Y',
        fieldName: 'netIncomeGrowth5Y',
        format: 'shortDecimal'
      },
      ongoingCharge: {
        headerLabelKey: 'ongoingCharge',
        fieldName: 'ongoingCharge',
        format: 'shortDecimal'
      },
      operatingMargin: {
        headerLabelKey: 'operatingMargin',
        fieldName: 'operatingMargin',
        format: 'shortDecimal'
      },
      pcfRatio: {
        headerLabelKey: 'pcfRatio',
        fieldName: 'pcfRatio',
        format: 'shortDecimal'
      },
      primaryBenchmarkName: {
        headerLabelKey: 'primaryBenchmarkName',
        fieldName: 'primaryBenchmarkName'
      },
      pegRatio: {
        headerLabelKey: 'pegRatio',
        fieldName: 'pegRatio',
        format: 'shortDecimal'
      },
      peRatio5YrAverage: {
        headerLabelKey: 'peRatio5YrAverage',
        fieldName: 'peRatio5YrAverage',
        format: 'shortDecimal'
      },
      peRatioForward: {
        headerLabelKey: 'peRatioForward',
        fieldName: 'peRatioForward',
        format: 'shortDecimal'
      },
      performanceFee: {
        headerLabelKey: 'performanceFee',
        fieldName: 'performanceFee',
        format: 'shortDecimal'
      },
      percAssetsTop10Holdings: {
        headerLabelKey: 'percAssetsTop10Holdings',
        fieldName: 'percAssetsTop10Holdings',
        format: 'shortDecimal'
      },
      pillarRatingsParent: {
        headerLabelKey: 'pillarRatingsParent',
        fieldName: 'pillarRatingsParent',
        fieldSettings: {
          itemTemplate:
            "<div class='ratings__stars ratings__stars--{{obj.pillarRatingsParent}} {{ ::namespaceClass(\"__cell-content\") }}'><span class='ratings__stars--empty' data-ng-if='obj.pillarRatingsParent==1'>{{labels.get('Negative')}}</span><span class='ratings__stars--empty' data-ng-if='obj.pillarRatingsParent==2'>{{labels.get('Neutral')}}</span><span class='ratings__stars--empty' data-ng-if='obj.pillarRatingsParent==3'>{{labels.get('Positive')}}</span><span class='ratings__stars--empty' data-ng-if='!obj.pillarRatingsParent'>{{'_'}}</span></div>"
        }
      },
      pillarRatingsPerformance: {
        headerLabelKey: 'pillarRatingsPerformance',
        fieldName: 'pillarRatingsPerformance',
        fieldSettings: {
          itemTemplate:
            "<div class='ratings__stars ratings__stars--{{obj.pillarRatingsPerformance}} {{ ::namespaceClass(\"__cell-content\") }}'><span class='ratings__stars--empty' data-ng-if='obj.pillarRatingsPerformance==1'>{{labels.get('Negative')}}</span><span class='ratings__stars--empty' data-ng-if='obj.pillarRatingsPerformance==2'>{{labels.get('Neutral')}}</span><span class='ratings__stars--empty' data-ng-if='obj.pillarRatingsPerformance==3'>{{labels.get('Positive')}}</span><span class='ratings__stars--empty' data-ng-if='!obj.pillarRatingsPerformance'>{{'_'}}</span></div>"
        }
      },
      pillarRatingsPeople: {
        headerLabelKey: 'pillarRatingsPeople',
        fieldName: 'pillarRatingsPeople',
        fieldSettings: {
          itemTemplate:
            "<div class='ratings__stars ratings__stars--{{obj.pillarRatingsPeople}} {{ ::namespaceClass(\"__cell-content\") }}'><span class='ratings__stars--empty' data-ng-if='obj.pillarRatingsPeople==1'>{{labels.get('Negative')}}</span><span class='ratings__stars--empty' data-ng-if='obj.pillarRatingsPeople==2'>{{labels.get('Neutral')}}</span><span class='ratings__stars--empty' data-ng-if='obj.pillarRatingsPeople==3'>{{labels.get('Positive')}}</span><span class='ratings__stars--empty' data-ng-if='!obj.pillarRatingsPeople'>{{'_'}}</span></div>"
        }
      },
      pillarRatingsPrice: {
        headerLabelKey: 'pillarRatingsPrice',
        fieldName: 'pillarRatingsPrice',
        fieldSettings: {
          itemTemplate:
            "<div class='ratings__stars ratings__stars--{{obj.pillarRatingsPrice}} {{ ::namespaceClass(\"__cell-content\") }}'><span class='ratings__stars--empty' data-ng-if='obj.pillarRatingsPrice==1'>{{labels.get('Negative')}}</span><span class='ratings__stars--empty' data-ng-if='obj.pillarRatingsPrice==2'>{{labels.get('Neutral')}}</span><span class='ratings__stars--empty' data-ng-if='obj.pillarRatingsPrice==3'>{{labels.get('Positive')}}</span><span class='ratings__stars--empty' data-ng-if='!obj.pillarRatingsPrice'>{{'_'}}</span></div>"
        }
      },
      pillarRatingsProcess: {
        headerLabelKey: 'pillarRatingsProcess',
        fieldName: 'pillarRatingsProcess',
        fieldSettings: {
          itemTemplate:
            "<div class='ratings__stars ratings__stars--{{obj.pillarRatingsProcess}} {{ ::namespaceClass(\"__cell-content\") }}'><span class='ratings__stars--empty' data-ng-if='obj.pillarRatingsProcess==1'>{{labels.get('Negative')}}</span><span class='ratings__stars--empty' data-ng-if='obj.pillarRatingsProcess==2'>{{labels.get('Neutral')}}</span><span class='ratings__stars--empty' data-ng-if='obj.pillarRatingsProcess==3'>{{labels.get('Positive')}}</span><span class='ratings__stars--empty' data-ng-if='!obj.pillarRatingsProcess'>{{'_'}}</span></div>"
        }
      },
      price: {
        headerLabelKey: 'price',
        fieldName: 'price',
        format: 'shortDecimal'
      },
      price52WeeksHigh: {
        headerLabelKey: 'price52WeeksHigh',
        fieldName: 'price52WeeksHigh',
        format: 'shortDecimal'
      },
      price52WeeksLow: {
        headerLabelKey: 'price52WeeksLow',
        fieldName: 'price52WeeksLow',
        format: 'shortDecimal'
      },
      priceBookRatio: {
        headerLabelKey: 'priceBookRatio',
        fieldName: 'priceBookRatio',
        format: 'shortDecimal'
      },
      priceCurrency: {
        headerLabelKey: 'priceCurrency',
        fieldName: 'priceCurrency'
      },
      priceEarningsRatio: {
        headerLabelKey: 'priceEarningsRatio',
        format: 'shortDecimal',
        fieldName: 'priceEarningsRatio'
      },
      priceOverFV: {
        headerLabelKey: 'priceOverFV',
        fieldName: 'priceOverFV',
        format: 'shortDecimal'
      },
      priceSalesRatio: {
        headerLabelKey: 'priceSalesRatio',
        fieldName: 'priceSalesRatio',
        format: 'shortDecimal'
      },
      pricingFrequencyName: {
        headerLabelKey: 'pricingFrequencyName',
        fieldName: 'pricingFrequencyName'
      },
      puttable: {
        headerLabelKey: 'puttable',
        fieldName: 'puttable'
      },
      qR_GBRReturnM12_1: {
        headerLabelKey: 'qR_GBRReturnM12_1',
        fieldName: 'qR_GBRReturnM12_1',
        format: 'shortDecimal'
      },
      qR_GBRReturnM12_2: {
        headerLabelKey: 'qR_GBRReturnM12_2',
        fieldName: 'qR_GBRReturnM12_2',
        format: 'shortDecimal'
      },
      qR_GBRReturnM12_3: {
        headerLabelKey: 'qR_GBRReturnM12_3',
        fieldName: 'qR_GBRReturnM12_3',
        format: 'shortDecimal'
      },
      qR_GBRReturnM12_4: {
        headerLabelKey: 'qR_GBRReturnM12_4',
        fieldName: 'qR_GBRReturnM12_4',
        format: 'shortDecimal'
      },
      qR_GBRReturnM12_5: {
        headerLabelKey: 'qR_GBRReturnM12_5',
        fieldName: 'qR_GBRReturnM12_5',
        format: 'shortDecimal'
      },
      quantitativeStarRating: {
        headerLabelKey: 'quantitativeStarRating',
        fieldName: 'quantitativeStarRating',
        fieldSettings: {
          headerClass: 'ec-table__cell--trademark-symbol',
          itemTemplate: '{{#../html/quant-star-rating.html#}}'
        }
      },
      r2M36: {
        headerLabelKey: 'r2M36',
        fieldName: 'r2M36',
        format: 'shortDecimal'
      },
      returnD1: {
        headerLabelKey: 'returnD1',
        fieldName: 'returnD1',
        format: 'shortDecimal'
      },
      returnW1: {
        headerLabelKey: 'returnW1',
        fieldName: 'returnW1',
        format: 'shortDecimal'
      },
      returnM0: {
        headerLabelKey: 'returnM0',
        fieldName: 'returnM0',
        format: 'shortDecimal'
      },
      returnM1: {
        headerLabelKey: 'returnM1',
        fieldName: 'returnM1',
        format: 'shortDecimal'
      },
      returnM3: {
        headerLabelKey: 'returnM3',
        fieldName: 'returnM3'
      },
      returnM6: {
        headerLabelKey: 'returnM6',
        fieldName: 'returnM6',
        format: 'shortDecimal'
      },
      returnM12: {
        headerLabelKey: 'returnM12',
        fieldName: 'returnM12',
        format: 'shortDecimal'
      },
      returnM12QtrEnd: {
        headerLabelKey: 'returnM12QtrEnd',
        fieldName: 'returnM12QtrEnd',
        format: 'shortDecimal'
      },
      returnM36: {
        headerLabelKey: 'returnM36',
        fieldName: 'returnM36',
        format: 'shortDecimal'
      },
      returnM36QtrEnd: {
        headerLabelKey: 'returnM36QtrEnd',
        fieldName: 'returnM36QtrEnd',
        format: 'shortDecimal'
      },
      returnM36UA: {
        headerLabelKey: 'returnM36UA',
        fieldName: 'returnM36UA',
        format: 'shortDecimal'
      },
      returnM60: {
        headerLabelKey: 'returnM60',
        fieldName: 'returnM60',
        format: 'shortDecimal'
      },
      returnM60QtrEnd: {
        headerLabelKey: 'returnM60QtrEnd',
        fieldName: 'returnM60QtrEnd',
        format: 'shortDecimal'
      },
      returnM60UA: {
        headerLabelKey: 'returnM60UA',
        fieldName: 'returnM60UA',
        format: 'shortDecimal'
      },
      returnM120: {
        headerLabelKey: 'returnM120',
        fieldName: 'returnM120',
        format: 'shortDecimal'
      },
      returnM120QtrEnd: {
        headerLabelKey: 'returnM120QtrEnd',
        fieldName: 'returnM120QtrEnd',
        format: 'shortDecimal'
      },
      returnM255UA: {
        headerLabelKey: 'returnM255UA',
        fieldName: 'returnM255UA',
        format: 'shortDecimal'
      },
      revenueGrowth3Y: {
        headerLabelKey: 'revenueGrowth3Y',
        fieldName: 'revenueGrowth3Y'
      },
      revenueGrowth5Y: {
        headerLabelKey: 'revenueGrowth5Y',
        fieldName: 'revenueGrowth5Y',
        format: 'shortDecimal'
      },
      riskRating: {
        headerLabelKey: 'riskRating',
        fieldName: 'riskRating'
      },
      roattm: {
        headerLabelKey: 'roattm',
        fieldName: 'roattm',
        format: 'shortDecimal'
      },
      roettm: {
        headerLabelKey: 'roettm',
        fieldName: 'roettm',
        format: 'shortDecimal'
      },
      roeYear1: {
        headerLabelKey: 'roeYear1',
        format: 'shortDecimal',
        fieldName: 'roeYear1'
      },
      roicYear1: {
        headerLabelKey: 'roicYear1',
        format: 'shortDecimal',
        fieldName: 'roicYear1'
      },
      salesYield: {
        headerLabelKey: 'salesYield',
        fieldName: 'salesYield'
      },
      sectorName: {
        headerLabelKey: 'sectorName',
        fieldName: 'sectorName'
      },
      selected: {
        fieldSettings: {
          headerTemplate: 'checkbox',
          itemTemplate: 'checkbox'
        },
        sortingEnabled: false
      },
      sharpeM36: {
        headerLabelKey: 'sharpeM36',
        fieldName: 'sharpeM36',
        format: 'shortDecimal'
      },
      starRatingM255: {
        headerLabelKey: 'starRatingM255',
        fieldName: 'starRatingM255',
        fieldSettings: {
          headerClass: 'ec-table__cell--trademark-symbol',
          itemTemplate: '{{#../html/star-rating-m255.html#}}'
        }
      },
      standardDeviationM12: {
        headerLabelKey: 'standardDeviationM12',
        fieldName: 'standardDeviationM12',
        format: 'shortDecimal'
      },
      standardDeviationM36: {
        headerLabelKey: 'standardDeviationM36',
        fieldName: 'standardDeviationM36',
        format: 'shortDecimal'
      },
      standardDeviationM60: {
        headerLabelKey: 'standardDeviationM60',
        fieldName: 'standardDeviationM60',
        format: 'shortDecimal'
      },
      stewardshipGrade: {
        headerLabelKey: 'stewardshipGrade',
        fieldName: 'stewardshipGrade'
      },
      strategicBeta: {
        headerLabelKey: 'strategicBeta',
        fieldName: 'strategicBeta',
        format: 'shortDecimal'
      },
      strategicBetaGroup: {
        headerLabelKey: 'strategicBetaGroup',
        fieldName: 'strategicBetaGroup'
      },

      subordinate: {
        headerLabelKey: 'subordinate',
        fieldName: 'subordinate'
      },
      sustainabilityRank: {
        headerLabelKey: 'sustainabilityRank',
        fieldName: 'sustainabilityRank',
        fieldSettings: {
          headerClass: 'ec-table__cell--trademark-symbol',
          itemTemplate: '{{#../html/sustainability-rank.html#}}'
        }
      },
      sustainabilityRating: {
        headerLabelKey: 'sustainabilityRating',
        fieldName: 'sustainabilityRating',
        fieldSettings: {
          itemTemplate: '{{#../html/sustainability-rating.html#}}'
        }
      },
      ticker: {
        headerLabelKey: 'ticker',
        fieldName: 'ticker'
      },
      totalAssets: {
        headerLabelKey: 'totalAssets',
        fieldName: 'totalAssets',
        format: {
          dataType: 'number',
          abbreviate: {
            mode: 'millions',
            abbreviations: {
              millions: ' '
            }
          },
          minimumFractionDigits: 2,
          maximumFractionDigits: 2
        }
      },
      totalMarketValue: {
        headerLabelKey: 'totalMarketValue',
        fieldName: 'totalMarketValue',
        format: 'shortDecimal'
      },
      trackingErrorFor5Year: {
        headerLabelKey: 'trackingErrorFor5Year',
        fieldName: 'trackingErrorFor5Year',
        format: 'shortDecimal'
      },
      trailing1YearAverage: {
        headerLabelKey: 'trailing1YearAverage',
        fieldName: 'trailing1YearAverage',
        format: 'shortDecimal'
      },
      trailingDate: {
        headerLabelKey: 'trailingDate',
        fieldName: 'trailingDate',
        format: 'yyyy-mmm-dd'
      },
      transactionFee: {
        headerLabelKey: 'transactionFee',
        fieldName: 'transactionFee'
      },
      turnoverRatio: {
        headerLabelKey: 'turnoverRatio',
        fieldName: 'turnoverRatio',
        format: 'shortDecimal'
      },
      worst3MonthSinceInception: {
        headerLabelKey: 'worst3MonthSinceInception',
        fieldName: 'worst3MonthSinceInception',
        format: 'shortDecimal'
      },
      yearsToMaturity: {
        headerLabelKey: 'yearsToMaturity',
        fieldName: 'yearsToMaturity',
        format: 'shortDecimal'
      },
      yield_M12: {
        headerLabelKey: 'yield_M12',
        fieldName: 'yield_M12',
        format: 'shortDecimal'
      },
      yieldToMaturity: {
        headerLabelKey: 'yieldToMaturity',
        fieldName: 'yieldToMaturity',
        format: 'shortDecimal'
      }
    },
    holdingTypeIdMap: {
      '2': 'openEndedFund',
      '3': 'equity',
      '21': 'investmentTrust',
      '22': 'exchangeTradedFund'
    },
    _model: {
      filterValues: {
        dataSourceType: 'api',
        transformPipeline: [
          {
            name: 'fromIwtFilterValues',
            options: {
              filterNamesWithoutAll: ['IndustryId'],
              filtersMappedValue: ['AnalystRatingScale', 'BaseCurrencyId']
            }
          }
        ],
        ancestor: null,
        api: {
          method: 'get',
          url:
            '//{{baseUrl}}/api/rest.svc/{{urlKey}}/security/screener?languageId={{languageId}}&currencyId={{currencyId}}&universeIds={{universeId}}&outputType=json&filterDataPoints={{filterDataPoints}}&filters={{filtersSelectedValue}}',
          validationExpression:
            'parameters.universeId !== undefined  && parameters.universeId.length > 0',
          parameters: {
            isTransformSync: true,
            transformPipeline: ['toIwtFilterValues'],
            watchedPaths: ['universeId', 'filtersSelectedValue.exchange'],
            deep: true,
            throttleTime: 0
          }
        }
      },
      removeSecurities: {
        dataSourceType: 'api',
        api: {
          method: 'put',
          url:
            '//{{baseUrl}}/api/v1/saved_lists/{{selectedItemId}}?list_action=remove',
          validationExpression:
            'parameters.allowSecurityDelete === true && parameters.selectedItemKeysViewList.length > 0',
          parameters: {
            transformPipeline: ['toRemoveSelectedSecurity'],
            watchedPaths: ['allowSecurityDelete']
          },
          headers: {
            'X-API-IwtSiteId': '{{_ tokens().XAPIIwtSiteId _}}',
            'X-API-SiteSession': '{{_ tokens().XAPISiteSession _}}'
          }
        }
      },
      savedLists: {
        dataSourceType: 'api',
        api: {
          method: 'get',
          url: '//{{baseUrl}}/api/v1/saved_lists?sort=updated&order=desc',
          validationExpression:
            "settings.get('screenerStorage') === STORAGE_TYPE.apiStorage && parameters.allowFetchingSavedLists !== false",
          parameters: {
            transformPipeline: ['toSavedLists'],
            watchedPaths: ['allowFetchingSavedLists']
          },
          headers: {
            'X-API-IwtSiteId': '{{_ tokens().XAPIIwtSiteId _}}',
            'X-API-SiteSession': '{{_ tokens().XAPISiteSession _}}'
          }
        }
      },
      securities: {
        dataSourceType: 'api',
        transformPipeline: [
          {
            name: 'fromIwtScreener',
            options: {
              performanceDate: {
                format: 'yy-mmm-dd',
                maxYearOffset: 6,
                separator: '/'
              }
            }
          }
        ],
        api: {
          method: 'get',
          url:
            '//{{baseUrl}}/api/rest.svc/{{urlKey}}/security/screener?page={{page}}&pageSize={{perPage}}&sortOrder={{sortField}}%20{{sortOrder}}&outputType=json&version=1&languageId={{languageId}}&currencyId={{currencyId}}&universeIds={{universeId}}&securityDataPoints={{securityDataPoints}}&filters={{filtersSelectedValue}}&term={{term}}&subUniverseId={{subUniverseId}}',
          validationExpression:
            'parameters.filtersSelectedValue !== undefined && filtersForm.$valid && parameters.universeId !== undefined  && parameters.universeId.length > 0',
          parameters: {
            isTransformSync: true,
            transformPipeline: ['toIwtScreener'],
            watchedPaths: [
              'universeId',
              'page',
              'perPage',
              'sortField',
              'sortOrder',
              'filtersSelectedValue',
              'term'
            ],
            deep: true,
            throttleTime: 0
          }
        }
      },
      selectedListDB: {
        dataSourceType: 'api',
        api: {
          method: 'get',
          url: '//{{baseUrl}}/api/v1/saved_lists/{{selectedListId}}',
          validationExpression:
            "settings.get('screenerStorage') === STORAGE_TYPE.apiStorage && !!parameters.selectedList === true",
          parameters: {
            transformPipeline: ['toSelectedList'],
            watchedPaths: ['selectedList']
          },
          headers: {
            'X-API-IwtSiteId': '{{_ tokens().XAPIIwtSiteId _}}',
            'X-API-SiteSession': '{{_ tokens().XAPISiteSession _}}'
          }
        }
      }
    },
    showActionBar: true,
    ticker: {
      id: 'XIUSA04CG4',
      idType: 'Morningstar',
      name: 'FTSE 100 PR GBP',
      securityToken: 'XIUSA04CG4]'
    },
    usageTrackingHitMap: {
      criteriaChanged: {
        type: 'event',
        action: 'criteriaChanged',
        label: 'Criteria Changed'
      },
      resultCountChanged: {
        type: 'event',
        action: 'resultCountChanged',
        label: 'Result Count Changed'
      },
      gridTabChanged: {
        type: 'event',
        action: 'gridTabChanged',
        label: 'Grid Tab Changed'
      },
      gridPageChanged: {
        type: 'event',
        action: 'gridPageChanged',
        label: 'Grid Page Changed'
      },
      gridSortChanged: {
        type: 'event',
        action: 'gridSortChanged',
        label: 'Grid Sort Changed'
      },
      gridRowCountChanged: {
        type: 'event',
        action: 'gridRowCountChanged',
        label: 'Grid Row Count Changed'
      },
      xrayActionTriggered: {
        type: 'event',
        action: 'xrayActionTriggered',
        label: 'Xray Action Triggered'
      },
      chartActionTriggered: {
        type: 'event',
        action: 'chartActionTriggered',
        label: 'Chart Action Triggered'
      },
      reportActionTriggered: {
        type: 'event',
        action: 'reportActionTriggered',
        label: 'Report Action Triggered'
      },
      compareActionTriggered: {
        type: 'event',
        action: 'compareActionTriggered',
        label: 'Compare Action Triggered'
      },
      exportActionTriggered: {
        type: 'event',
        action: 'exportActionTriggered',
        label: 'Export Action Triggered'
      },
      watchlistActionTriggered: {
        type: 'event',
        action: 'watchlistActionTriggered',
        label: 'Watchlist Action Triggered'
      }
    }
  },
  components: {
    actionBar: {
      type: 'ecActionContainer',
      components: {
        actionDeselectAll: {
          type: 'ecButton',
          settings: {
            _parameters: {
              onClick: {
                binding:
                  '{{_ parameters(_._.tableSecurities).deselectAll _}}'
              }
            }
          }
        },
        actionExport: {
          type: 'ecButton',
          labels: {
            caption: {
              da: 'Eksportér',
              en: 'Export',
              'fr-CA': 'Exporter',
              it: 'Esporta',
              ja: '出力',
              sv: 'Exportera',
              'zh-CN': '输出',
              'zh-HK': '輸出'
            }
          }
        },
        actionInvestmentCompare: {
          type: 'ecButton',
          labels: {
            caption: {
              da: 'Diagram',
              en: 'Chart',
              'fr-CA': 'Graphique',
              it: 'Grafico',
              ja: 'チャート',
              sv: 'Graf',
              'zh-CN': '图表',
              'zh-HK': '圖表'
            }
          }
        },
        actionNavigateToChart: {
          type: 'ecButton',
          labels: {
            caption: {
              en: 'View Chart'
            }
          }
        },
        actionNavigateToInvestmentCompare: {
          type: 'ecButton',
          labels: {
            caption: {
              en: 'Compare Selected'
            }
          }
        },
        actionNavigateToWatchlist: {
          type: 'ecButton',
          labels: {
            caption: {
              en: 'Add to Watch List'
            }
          }
        },
        addToList: {
          type: 'ecButton',
          labels: {
            caption: {
              da: 'Tilføj til Min liste',
              en: 'Add to My List',
              'fr-CA': 'Ajouter à ma liste',
              it: 'Aggiungi alla mia lista',
              ja: 'マイリストに追加',
              sv: 'Lägg till i min lista',
              'zh-CN': '添加到我的列表',
              'zh-HK': '添加到我的列表'
            }
          }
        },
        addToPortfolio: {
          type: 'ecButton',
          labels: {
            caption: {
              da: 'Tilføj til Portefølje',
              en: 'Add to Portfolio',
              'fr-CA': 'Ajouter au portefeuille',
              it: 'Aggiungi al portafoglio',
              ja: 'ポートフォリオに追加',
              sv: 'Lägg till portfölj',
              'zh-CN': '添加到投资组合',
              'zh-HK': '添加到投資組合'
            }
          }
        },
        viewFundReport: {
          type: 'ecButton',
          labels: {
            caption: {
              da: 'Åbn Fondrapport',
              en: 'View Fund Report',
              'fr-CA': 'Afficher rapport de fonds',
              it: 'Vedi Report fondo',
              ja: 'ファンド詳細を見る',
              sv: 'Öppna fondrapport',
              'zh-CN': '查看基金报告',
              'zh-HK': '查看基金報告'
            }
          }
        },
        viewList: {
          type: 'ecButton',
          settings: {
            _parameters: {
              badgeContent: {
                binding: '{{_ parameters(_._).viewListKeys.length _}}'
              }
            },
            buttonType: 'badge'
          },
          labels: {
            caption: {
              da: 'Se Min liste',
              en: 'View My list',
              'fr-CA': 'Afficher ma liste',
              it: 'Visualizza la mia lista',
              ja: 'マイリストを見る',
              sv: 'Visa min lista',
              'zh-CN': '查看我的列表',
              'zh-HK': '查看我的列表'
            }
          }
        }
      },
      settings: {
        actions: [
          {
            actionType: 'custom',
            instanceId: 'actionInvestmentCompare'
          },
          {
            actionType: 'custom',
            instanceId: 'actionExport'
          },
          {
            actionType: 'custom',
            instanceId: 'addToPortfolio'
          },
          {
            actionType: 'custom',
            instanceId: 'viewFundReport'
          },
          {
            actionType: 'custom',
            instanceId: 'addToList'
          },
          {
            actionType: 'custom',
            instanceId: 'viewList'
          }
        ],
        primaryActionsCount: {
          defaults: 2
        },
        showMoreActionsHeader: true
      }
    },
    btnToScreener: {
      type: 'ecButton',
      settings: {
        buttonCSS: 'muic-btn'
      },
      labels: {
        caption: {
          en: 'Back'
        }
      }
    },
    compareChart: {
      type: 'ecSecurityLineChart',
      settings: {
        _parameters: {
          compareTickers: {
            binding: '{{_ parameters(_).compareTickers _}}'
          },
          ticker: {
            binding: '{{_ parameters(_).ticker _}}'
          }
        },
        chartSettings: {
          chartOptions: 'interactive',
          compact: true,
          dataType: 'growth10K',
          height: 500,
          intervalType: 'FiveYear',
          showLegendFullName: true,
          useEUTimeSeriesData: true,
          yAxisOrient: 'left'
        },
        defaultTimelineKey: 'year',
        growthBaseValue: 1000,
        visibleTimelineKeys: [
          'fivedays',
          'onemonth',
          'threemonths',
          'sixmonths',
          'year',
          'threeyears',
          'fiveyears',
          'tenyears'
        ],
        widgetSettings: {
          frequencyPriority: 'default',
          intervalFrequencyMap: {
            OneMonth: 'd',
            ThreeMonth: 'd'
          }
        }
      }
    },
    containerCompareChartView: {
      type: 'ecContainer',
      settings: {
        breakpoints: {
          defaults: {
            groups: {
              main: {
                order: 0,
                cols: 12,
                show: '{{_ model(_).showChart _}}',
                sections: ['chartControls']
              }
            }
          }
        }
      }
    },
    disclaimer: {
      type: 'ecDisclaimer'
    },
    filtersSecurities: {
      type: 'ecFilters',
      settings: {
        _model: {
          filterValues: {
            dataSourceType: 'selector',
            selector: '{{_ model(_).filterValues _}}'
          }
        },
        _parameters: {
          filtersSelectedValue: {
            binding: '{{_ parameters(_).filtersSelectedValue _}}'
          },
          universeId: {
            binding: '{{_ parameters(_).universeId _}}'
          }
        }
      }
    },
    resultsViewContainer: {
      type: 'ecContainer',
      settings: {
        breakpoints: {
          defaults: {
            groups: {
              all: {
                order: 0,
                cols: 12,
                sections: [
                  'resultsViewShortlist',
                  'actionHeader',
                  'tableSecurities',
                  'resultsViewAllResultsPaginator'
                ]
              }
            }
          }
        },
        sections: {
          resultsViewAllResultsPaginator: {
            cols: 12,
            show: '{{_ model(_).componentLoaded _}}'
          }
        }
      }
    },
    saveDialog: {
      type: 'ecSaveDialog',
      settings: {
        _parameters: {
          dataToSave: {
            binding: '{{_ parameters(_).selectedItemKeys _}}'
          },
          modalStateOpen: {
            binding: '{{_ parameters(_).showSaveDialog _}}'
          }
        },
        _validation: {
          modelRules: {
            hasSelectedItem: {
              transformPipeline: [
                {
                  name: 'propertyOf',
                  options: {
                    criteria: ['name']
                  }
                },
                'size',
                'arrayWrap',
                {
                  name: 'filter',
                  options: {
                    criteria: [
                      {
                        fn: 'gt',
                        args: {
                          value: 0
                        }
                      }
                    ]
                  }
                },
                'size'
              ],
              ok: 1
            }
          }
        }
      },
      components: {
        newList: {
          type: 'ecInput',
          labels: {
            placeholder: {
              en: 'Enter list name'
            }
          }
        },
        newListRadio: {
          type: 'ecInput',
          labels: {
            saveNew: {
              en: 'Create New List'
            }
          }
        },
        updateList: {
          type: 'ecInput',
          labels: {
            selectboxText: {
              en: 'Select a list'
            }
          }
        },
        updateListRadio: {
          type: 'ecInput',
          labels: {
            updateExisting: {
              en: 'Add to Existing List'
            }
          }
        }
      },
      labels: {
        addItem: {
          en: 'Add to List'
        },
        itemNameIsRequired: {
          en: 'A list name is required'
        },
        updatedExistingItemAlert: {
          en:
            'List {{_ parameters().selectedItem.name _}} has been updated with {{_ parameters(_).selectedItemKeys.length _}} item(s).'
        }
      }
    },
    savedListsDB: {
      type: 'ecInput',
      settings: {
        _model: {
          listItems: {
            dataSourceType: 'selector',
            selector: '{{_ model(_).savedLists _}}',
            transformPipeline: ['fromSavedLists']
          }
        },
        inputType: 'select',
        keyField: 'id',
        valueField: 'name'
      },
      labels: {
        selectboxText: {
          en: 'Select Saved List'
        }
      }
    },
    tableSecurities: {
      type: 'ecTable',
      settings: {
        enablePartialCheckbox: true,
        loader: {
          enabled: true,
          loaderContainer: 'selectorBased',
          selector: '.ec-screener'
        },
        _parameters: {
          page: {
            binding: '{{_ parameters(_).page _}}'
          },
          perPage: {
            binding: '{{_ parameters(_).perPage _}}'
          },
          totalRows: {
            binding: '{{_ model(_).securities.total _}}'
          },
          selectedItemKeys: {
            binding: '{{_ parameters(_).selectedItemKeys _}}'
          },
          sortField: {
            binding: '{{_ parameters(_).sortField _}}'
          },
          sortOrder: {
            binding: '{{_ parameters(_).sortOrder _}}'
          },
          fields: {
            binding: '{{_ parameters(_).activeView.fields _}}'
          },
          keyField: {
            binding: '{{_ parameters(_).activeView.keyField _}}'
          }
        },
        stickyLeadEnabled: true,
        standalone: false,
        columnSkipActionsClass:
          'ec-table-column-nav ec-table-column-nav--link-buttons',
        stickyLeadBreakPoints: [
          {
            stickyColIndices: [0, 1],
            columnsPerPage: 5,
            minWidth: 768,
            maxWidth: 1024
          }
        ],
        _model: {
          rows: {
            dataSourceType: 'selector',
            selector: '{{_ model(_).securities.rows _}}'
          }
        }
      },
      components: {
        actionContainerButtons: {
          type: 'ecActionContainer',
          components: {
            actionBuy: {
              type: 'ecButton',
              labels: {
                caption: {
                  en: 'Buy'
                }
              }
            },
            actionSell: {
              type: 'ecButton',
              labels: {
                caption: {
                  en: 'Sell'
                }
              }
            }
          },
          settings: {
            actions: [
              {
                actionType: 'custom',
                instanceId: 'actionBuy'
              },
              {
                actionType: 'custom',
                instanceId: 'actionSell'
              }
            ]
          }
        },
        paginator: {
          type: 'ecPaginator',
          settings: {
            _parameters: {
              page: {
                binding: '{{_ parameters(_).page _}}'
              },
              perPage: {
                binding: '{{_ parameters(_).perPage _}}'
              }
            }
          }
        }
      },
      labels: {}
    },
    viewTabs: {
      type: 'ecTabs',
      settings: {
        _parameters: {
          selectedTabIndex: {
            binding: '{{_ parameters(_).activeViewIndex _}}'
          }
        }
      }
    },
    viewListActionBar: {
      type: 'ecActionContainer',
      components: {
        viewListActionDelete: {
          type: 'ecButton',
          labels: {
            caption: {
              en: 'Delete',
              'fr-CA': 'Supprimer',
              'fr-FR': 'Supprimer'
            }
          }
        },
        viewListActionExport: {
          type: 'ecButton',
          labels: {
            caption: {
              da: 'Eksportér',
              en: 'Export',
              'fr-CA': 'Exporter',
              it: 'Esporta',
              ja: '出力',
              sv: 'Exportera',
              'zh-CN': '输出',
              'zh-HK': '輸出'
            }
          }
        },
        viewListActionInvestmentCompare: {
          type: 'ecButton',
          labels: {
            caption: {
              en: 'Chart',
              'fr-CA': 'Graphique',
              it: 'Grafico',
              ja: 'チャート',
              sv: 'Graf',
              'zh-CN': '图表',
              'zh-HK': '圖表'
            }
          }
        },
        viewListAddToPortfolio: {
          type: 'ecButton',
          labels: {
            caption: {
              en: 'Add to Portfolio',
              'fr-CA': 'Ajouter au portefeuille',
              it: 'Aggiungi al portafoglio',
              ja: 'ポートフォリオに追加',
              'zh-CN': '添加到投资组合',
              'zh-HK': '添加到投資組合'
            }
          }
        },
        viewListViewFundReport: {
          type: 'ecButton',
          labels: {
            caption: {
              da: 'Åbn Fondrapport',
              en: 'View Fund Report',
              'fr-CA': 'Afficher rapport de fonds',
              it: 'Vedi Report fondo',
              ja: 'ファンド詳細を見る',
              'zh-CN': '查看基金报告',
              'zh-HK': '查看基金報告'
            }
          }
        }
      },
      settings: {
        actions: [
          {
            actionType: 'custom',
            instanceId: 'viewListActionDelete'
          },
          {
            actionType: 'custom',
            instanceId: 'viewListActionInvestmentCompare'
          },
          {
            actionType: 'custom',
            instanceId: 'viewListActionExport'
          },
          {
            actionType: 'custom',
            instanceId: 'viewListAddToPortfolio'
          },
          {
            actionType: 'custom',
            instanceId: 'viewListViewFundReport'
          }
        ],
        primaryActionsCount: {
          defaults: 3
        },
        showMoreActionsHeader: true
      }
    },
    viewListContainer: {
      type: 'ecContainer',
      settings: {
        breakpoints: {
          defaults: {
            groups: {
              all: {
                order: 0,
                cols: 12,
                show: '{{_ model(_).visibilityOfViewList _}}',
                sections: [
                  'viewListSection',
                  'viewListActionHeader',
                  'viewListResults',
                  'disclaimer'
                ]
              }
            }
          }
        },
        sections: {
          viewListSection: {
            cols: 12,
            show: '{{_ model(_).showViewList _}}'
          },
          viewListActionHeader: {
            cols: 12,
            show: '{{_ model(_).showViewList _}}'
          },
          viewListResults: {
            cols: 12,
            show: '{{_ model(_).showViewList _}}'
          },
          disclaimer: {
            cols: 12,
            show: '{{_ model(_).showViewList _}}'
          }
        }
      }
    },
    viewListSecurities: {
      type: 'ecTable',
      settings: {
        _parameters: {
          fields: {
            binding: '{{_ parameters(_).activeViewViewList.fields _}}'
          },
          keyField: {
            binding: '{{_ parameters(_).activeViewViewList.keyField _}}'
          },
          page: {
            binding: '{{_ parameters(_).pageForViewList _}}'
          },
          perPage: {
            binding: '{{_ parameters(_).perPageForViewList _}}'
          },
          sortField: {
            binding: '{{_ parameters(_).sortFieldForViewList _}}'
          },
          sortOrder: {
            binding: '{{_ parameters(_).sortOrderForViewList _}}'
          },
          selectedItemKeys: {
            binding: '{{_ parameters(_).selectedItemKeysViewList _}}'
          }
        },
        _templates: {
          hyperLink: '{{#blueprints/standard-top-nav/html/hyperlink.html#}}'
        },
        enablePartialCheckbox: true,
        loader: {
          enabled: true,
          loaderContainer: 'componentBased'
        },
        stickyLeadEnabled: true,
        standalone: false,
        columnSkipActionsClass:
          'ec-table-column-nav ec-table-column-nav--link-buttons',
        stickyLeadBreakPoints: [
          {
            stickyColIndices: [0, 1],
            columnsPerPage: 5,
            minWidth: 736,
            maxWidth: 1024
          }
        ],
        _model: {
          rows: {
            dataSourceType: 'api',
            transformPipeline: [
              {
                name: 'fromIwtScreenerViewList'
              }
            ],
            api: {
              method: 'get',
              url:
                '//{{baseUrl}}/api/rest.svc/{{urlKey}}/security/screener?page={{page}}&pageSize={{perPage}}&sortOrder={{sortField}}%20{{sortOrder}}&outputType=json&version=1&languageId={{languageId}}&currencyId={{currencyId}}&universeIds={{universeId}}&securityDataPoints={{securityDataPoints}}&filters={{filtersSelectedValue}}',
              validationExpression:
                '!!parameters.filterSecIds === true && parameters.filterSecIds.length > 0',
              parameters: {
                transformPipeline: [
                  {
                    name: 'toIwtScreenerViewList'
                  }
                ],
                watchedPaths: [
                  'page',
                  'perPage',
                  'sortField',
                  'sortOrder',
                  'filterSecIds'
                ],
                deep: true,
                throttleTime: 200
              }
            }
          }
        }
      },
      components: {
        paginator: {
          type: 'ecPaginator',
          settings: {
            _parameters: {
              page: {
                binding: '{{_ parameters(_).page _}}'
              },
              perPage: {
                binding: '{{_ parameters(_).perPage _}}'
              }
            }
          }
        }
      }
    },
    viewListTabs: {
      type: 'ecTabs',
      settings: {
        _parameters: {
          selectedTabIndex: {
            binding: '{{_ parameters(_).activeViewIndexViewTab _}}'
          }
        }
      }
    }
  }
}
