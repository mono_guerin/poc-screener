// startref
/// <reference path='../../.references.ts'/>
// endref
module AsterixNg {
    export enum TooltipPlacements {
        'top', 'right', 'bottom', 'left'
    }

    export interface TooltipSettings {
        /** Tooltip fading
         * @default true
         */
        animation: boolean;

        // onHide event override
        onHide: any;

        // onShow event override
        onShow: any;

        /**
         * @default top-left
         */
        placement: TooltipPlacements;

        /** JQuery selector to delegate the tooltip
         * @default false
         */
        selector: any;

        /** The template for the tooltip.
         * @format html
         * @default <div class=\"tooltip\" role=\"tooltip\">\r\n\t<div class=\"tooltip-arrow\"><\/div>\r\n\t<div class=\"tooltip-inner\"><\/div>\r\n<\/div>\r\n
         */
        template: string;

        // The tooltip text
        title: string;

        /** Action or actions to trigger the tooltip
         * @default hover focus
         */
        trigger: string;
    }
}
