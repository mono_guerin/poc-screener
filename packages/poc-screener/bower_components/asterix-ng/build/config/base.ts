// startref
/// <reference path='../../.references.ts'/>
// endref
module AsterixNg {
    export interface ComponentSettings extends AsterixCore.ComponentSettings {
        /**
         * @default null
         */
        tooltip?: AsterixNg.TooltipSettings;

        /**
         * @default null
         */
        loader?: {
            template: string;
        }
    }

    export interface Parameter {
        /** The parameter key. Will default to this parameter's key in the settings.
         * @default null
         */
        path?: string;

        /** The url binding.
         * @default null
         */
        url?: ParameterUrl;

        /** Use to bind this parameter to some other component's parameter (or model). Syntax: {{_ parameters(selector).property.path _}}
         * @default null
         */
        binding?: string;
    }

    export interface ParameterUrl {
        /** The parameter url key. Will default to the parameter's key if empty.
         * @default null
         */
        key?: string;

        /** Whether or not to namespace the query key.
         * @default null
         */
        namespace?: boolean;
    }
}
