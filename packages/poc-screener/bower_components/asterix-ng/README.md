**Please DO NOT contribute to this repo directly. All PR should go to the team project**

# **ng**

# Introduction
Bridges between asterix-core and angular and provides angular-specific utilities.

# mstarModelBinder
Responsible for data binding and handling cross-component data flow. This includes things such as:
- Populating data models with *API* data
- *Selecting* data from another parent in the hierarchy into the component's own data model
- *Transformations* (adapters) between API or parent data and self.
- *Propagating* save events (when the user is actively working with the data model)
- Populating data models with *default values*
- Hooking up *calculated properties*

mstarModelBinder will be automatically invoked by calling mstarComponentId's *bindModelData* method (this is usually already hooked up in all components that are expected to have data).

## API data configurations

### GET with model input data in the querystring
**todo: this approach doesn't support "to transformations" right now (it does support them only when applied to the API headers field).**
In this example (a modified version taken from the *ec-asset-allocator* component) we hook our component up with an API data call. Since we have some variable data that we need to pass from the component to the API, we use the *underscore mustache* `{{_ _}}` (expression(s) that will be evaluated by asterix-core's `ConfigEvaluator`) to hook this model input data up with its corresponding querystring parameters. Note that these model input properties will be watched and a new API call will be invoked whenever any of them changes.

We use the *validationExpressions* property to specify an array of expressions that have to evaluate to `true` in order for the component state to be considered ready to invoke the API.

We also specify a *transformPipeline* called *fromIwtAssetAllocatorToAssetAllocatorResults*. This is a reference to a transformation function that will be passed the returned API data and is expected to return the data in the format that the front-end expects. The purpose of this is basically that of an *adapter* pattern; we want our components to be able to deal with multiple different API data structures, while keeping the actual front-end model consistent. For more information on transformations, see the *Transformations* section below.

The resulting data will be available under the ComponentInstance's model property *results* (as specified in the configuration). Thanks to bindModel, the model will also be accessible through `scope.model` when working with angular.
> Example: Invoking an API with GET
```js
"_model": {
    "results": {
        "dataSourceType": "api",
        "transformPipeline": ["fromIwtAssetAllocatorToAssetAllocatorResults"],
        "api": {
            "method": "get",
            "url": "https://lt.morningstar.com/api/rest.svc/assetallocator/{{_ settings().urlKey _}}/json?AssetAllocationType={{_ model().input.assetAllocationType _}}&AssetClassWeights={{_ model().input.assetClassWeights _}}",
            "validationExpressions": ["{{_ model().input.assetAllocationType != null _}}", "{{_ model().input.assetClassWeights != null _}}"]
        }
    },
    "input": {
        // these properties will be inputted in the component UI
    }
}
```

#### JSONP
APIs can be invoked using JSONP by passing the `jsonp: true` option.

### POST with model input data in the request body (and custom headers)
In this example we invoke an API using the `POST` method. We also specify a request body using the `data` property and a custom header using the `headers` property. As in the example above, the involved model input data properties will be watched, and a new API call will be invoked whenever any of them changes.
> Example: Invoking an API with POST
```js
"_model": {
    "results": {
        "dataSourceType": "api",
        "api": {
            "url": "https://morningstar.com",
            "method": "post",
            "data": {
                "parameterProperty": "{{_ parameters().parameterProperty _}}",
                "modelProperty": "{{_ model().modelProperty _}}",
                "someToken": "{{_ tokens().someToken _}}"
            },
            "headers": {
                "modelProperty": "{{_ model().modelProperty _}}"
            }
        }
    }
}
```
A `defaultApiHeaders` token can be set using `morningstar.asterix.context.setToken` to store default headers that need to be applied to every ajax call (e.g. authentication headers).
An `ignoreDefaultHeaders: true` option can be set inside each api config to ignore those default headers.

### Refreshing authentication tokens
If an API requires an authentication token, the API will return a 401 (Unauthorized) or 403 (Forbidden) error if the token is not supplied, has expired or if the resource is forbidden. To refresh the token a callback can be supplied in the `options` object sent into asterix as `apiTokenExpiredCallback`. When a 401 or 403 error status is received the callback is called and should return the token name and new token value. Once received, the API is retried with this updated token value. The API is only retried once to prevent an infinite loop of calls if the API always returns a 401 or 403 status.

The callback is called with two parameters - an object to help identify which API token needs refreshing and a callback to call with the new token. Example object passed to callback:
```js
{
    url: 'https//www.myapi.com?param1={{param1}}', //original URL for the request
    parameters: { //parameters for the request (can be undefined)
        param1: 'abc'
    },
    headers: { //headers for the request (can be undefined)
        authToken: '{{_ tokens().authToken _}}'
    }
}
```

> Example auth token refresh callback function:
```js
morningstar.asterix.bootstrap.init({
    angularOptions: {
        bootstrap: false
    },
    appConfig: morningstar.appConfig,
    configOptions: {
        env: 'uat'
    },
    apiTokenExpiredCallback: function(options, callback) {
        var tokenName, tokenValue;
        if (options.url === 'https//www.myapi.com') {
            tokenName = 'MyApiToken';
            tokenValue = 'xxxxxx'; //Fetch new token value
        } else if (options.url === 'https//www.myotherapi.com') {
            tokenName = 'MyOtherApiToken';
            tokenValue = 'yyyyyy'; //Fetch new token value
        }
        if (tokenName && tokenValue) {
            callback(tokenName, tokenValue);
        }
    }
});
```

## Selector data configurations
Here we fetch data from a parent model property through the use of a *selector*. The *selector* property is expected to be an *underscore mustache* `{{_ _}}` expression (much like the model input data bindings above). One key difference is that in this example we use the `_` modifier within the parentheses following `model`. `_` is used to navigate one step up the component hierarchy. Basically, we're saying: give us the `account.holdings` path of our parent component's model.

Note that just as in the examples above, this binding will be watched and the data will be "re-fetched" whenever the `account.holdings` of the parent's model is changed. If need be, we can use a *transformPipeline* to transform the parent data into a format which is more suitable for our component (see the *Transformations* section below).
> Example: Fetching data from parent
```js
"_model": {
    "results": {
        "dataSourceType": "selector",
        "selector": "{{_ model(_).account.holdings _}}"
    }
}
```

It's also possible to define multiple selectors. This is useful if you need access to multiple data sources in the transformation that acts between the parent and child as in the example below. We *select* `oldHoldings` and `newHoldings` and run them through our transform `extractHoldingDiff` (not actually implemented, but you get the idea) to extract the *diff* that is expected in `holdingDiff`. `extraHoldingDiff` would be passed an object containing the properties `newHoldings` and `oldHoldings`.

> Example: Fetching data from parent (multiple selectors)
```js
"_model": {
    "holdingDiff": {
        "dataSourceType": "selector",
        "transformPipeline": ["extractHoldingDiff"],
        "selector": {
            "newHoldings": "{{_ model(_).account.newHoldings _}}",
            "oldHoldings": "{{_ model(_).account.oldHoldings _}}"
        }
    }
}
```

## Editing model data and propagating changes
In the data binding examples above we have handled the basics of a one-way data flow. Data is expected to be retrieved from an API or from a parent component. In some situations, data needs to flow in the other direction, too.

For example, a component *edit-profile* might fetch its initial model data source *input* from its parent component *profile-lister*. When the user changes values in *edit-profile*'s form (and, perhaps, presses a save button) we would want to *propagate* this change to *profile-lister* so that it knows that it should update its model. This could, in turn, lead to an API call to save the new state *(todo: we don't have any examples of this yet)*.

`propagateChanges` to the rescue! By using this configuration option (as seen in the example below) we will make sure that `ModelBinder` hooks up functionality for either automatically propagating all changes, or through a manual trigger. (To be clear: "propagate" in this context is simply a matter of raising a `ComponentInstance` event called `save`. The parent component would then have to listen to this event and apply changes accordingly.)

> Example: Propagating changes automatically
```js
"_model": {
    "results": {
        "dataSourceType": "selector",
        "selector": "{{_ model(_).activeProfile _}}",
        "propagateChanges": {
            "trigger": "auto",
            // "transformPipeline": ["massageDataBeforeParentReceivesIt"], // we could have a transform defined here
        }
    }
}
```

In some cases, we might not want to propagate changes to a data source immediately. In these cases we can use `trigger: 'manual'`. A `_$save` method will be attached to the data source and needs to be invoked manually from the component (e.g. when clicking a save button).

> Example: Propagating changes manually
```js
"_model": {
    "results": {
        "dataSourceType": "selector",
        "selector": "{{_ model(_).activeProfile _}}",
        "propagateChanges": {
            "trigger": "manual",
            // "transformPipeline": ["massageDataBeforeParentReceivesIt"], // we could have a transform defined here
        }
    }
}
```

> Example: Invoking _$save
```js
function save() {
    // todo: check validity of model (see `mstarModelValidate`)
    scope.model.results._$save();
}
```

> Example: Listening to save (in parent component)
```js
// Always make sure to use addUnregisterCallback to 'off' any instance listeners when the component is destroyed. This is NOT needed for scope watchers.
mstarComponent.addUnregisterCallback(scope.instance.getRelative('child').on('save', function(path, data) {
    if (path === 'results') {
        scope.model.activeProfile = data;
    }
}));
```

## Specifying default (initial) values for a data source
We've covered the cases for dynamic data retrieval from API's and other components above. But what if we simply wanted to specify some initial data for a data source? `defaults` to the rescue.

> Example: Using defaults to specify initial values
```js
"_model": {
    "results": {
        "dataSourceType": "selector",
        "selector": "{{_ model(_).activeProfile _}}",
        "defaults": [
            { "prop": "a" },
            { "prop": "b" },
            { "prop": "c" }
        ]
    }
}
```

**Note** that due to legacy reasons initial data can also be assigned directly to the data source property. This is, however, confusing as it mixes data source definition properties with data properties and *should be avoided*. Also note that doing this might end up with unexpected behaviour when using e.g. *calculated properties*.
> Example: Legacy way of assigning initial/default values - **DON'T DO THIS**
```js
"_model": {
    "results": [
        { "prop": "a" },
        { "prop": "b" },
        { "prop": "c" }
    ]
}
```

## Calculated properties
As you may have realized, when defining these models we're essentially defining domain classes *without code*! This is great in some ways, since it allows flexibility through configuration. But what, you might say... what about instance methods and dynamically calculated data? What if I want a method `getSelectedHoldings` that is essentially just a simple filter of a data source `holdings` through its input `selectedHoldingIds`? Something like this:
> Example: getSelectedHoldings
```js
MyDomainClass.prototype.getSelectedHoldings = function getSelectedHoldings() {
    var selectedHoldingIds = this.selectedHoldingIds;
    return _.filter(this.holdings, function(holding) {
        return _.contains(selectedHoldingIds, holding.holdingId);
    });
};
```

So where would we add this method? After reviewing so many tickets, I've realized that to most people the answer would be: *add it to the scope in the directive code*. Or, in some cases: *add it to scope.model in the directive code*. There are some situations where the second approach *could* be OK, but generally this puts fairly straightforward logic in code rather than in configuration. What if we, for some unknown reason, wanted to modify `getSelectedHoldings` for some specific instance of this? Well, we wouldn't be able to. Also, what happens if we want to use this logic in multiple components? Well, we suddenly have duplication of logic, or at *best* a shared "domain component" that encapsulates this logic in a service. (Note: in the future we *are* planning on adding data source "components" that make re-usability of data models across components easier. This is on our todo.)

This is where *calculated properties* come in. Since the logic in `getSelectedHoldings` is such a basic *aggregate* (or rather *projection*?) of our data, we might as well just define it in our data source definition. **Note** that this often makes use of *pre-defined transforms*, so to avoid confusion you might want to read the *Pre-defined transforms* section before proceeding here. The first example will, however, create a custom transform for this specific case to make it easier to understand. We'll then proceed to convert this into a more generic approach (using pre-defined, underscore-esque transforms). When defining a transform whose purpose is to act as the calculating part of a calc prop, we prefix its name with *calculate*.

> Example: Defining the calculateSelectedHoldings transform
```js
morningstar.asterix.ng.transforms.register('calculateSelectedHoldings', function calculateSelectedHoldings(data) {
    return _.filter(data.holdings, function(holding) {
        return _.contains(data.selectedHoldingIds, holding.holdingId);
    });
});
```

*There's definitely some room for improvement in this transform. Rather than making it specific for the holdings case, we could have introduced transform options to make it capable of handling other objects, too.*

So now that we have this new transform, we need to define it as a calculated property to make it work its magic. As you can see, we expect the `data` argument to be an object that contains the properties `holdings` and `selectedHoldingIds`. Let's keep that in mind for later.

There are two ways of defining calculated properties. Either by using the `properties` property or by using the `aggregates` property. Basically, the difference between these two is that the latter expects the data source to be an array, whereas the former expects the data source to be an object. Both will, however, attach the defined calculation as a property to the data source (Javascript allows defining properties on arrays).

In this example, we will be using `aggregates` since `holdings` is an array and we want to assign `selected` as a property of the collection itself, rather than assigning it for each element. See the comment in the example below for info on the meaning of `self` expressions depending on if we're using `aggregates` or `properties`.

> Example: Defining holdings.selected using aggregates and the calculateSelectedHoldings transform
```js
"_model": {
    "holdings": {
        "calculated": {
            "aggregates": {
                "selected": {
                    "data": { "holdings": "{{_ self() _}}", "selectedHoldingIds": "{{_ model().selectedHoldingIds _}}" }, // "self" is a reference to the whole collection, since this is an aggregate definition. Note that if this was defined under "properties" rather than "aggregates", the calculated property would be applied for each element in holdings, and "self" would reference the item itself rather than the collection
                    "transformPipeline": [
                        {
                            "name": "calculateSelectedHoldings"
                        }
                    ]
                }
            }
        }
    }
}
```

There we go! We can now expect `model.holdings` to contain a `selected` property that will be dynamically calculated whenever we retrieve it.

I'll tell you what, let's remove the need for our custom transform that was created above. Since this is such a straightforward projection of our two data sources `holdings` and `selectedHoldingIds`, we are able to convert it into a config-defined chain of pre-defined, underscore-driven transforms. (This concept gets fairly complex rather quickly, so if you feel a bit uncomfortable with the example below, that's just fine.)

> Example: Defining holdings.selected using aggregates with underscore transforms
```js
"_model": {
    "holdings": {
        "calculated": {
            "aggregates": {
                "selected": {
                    "data": "{{_ self() _}}",
                    "transformPipeline": [
                        {
                            "name": "filter",
                            "options": {
                                "itemAlias": "holding",
                                "criteria": {
                                    "data": "{{_ model().selectedHoldingIds _}}",
                                    "transformPipeline": [
                                        {
                                            "name": "contains",
                                            "options": {
                                                "criteria": "{{_ holding.holdingId _}}"
                                            }
                                        }
                                    ]
                                }
                            }
                        }
                    ]
                }
            }
        }
    }
}
```

More about this approach can be found in the *Pre-defined transforms* section. In short, this is the exact same code we wrote in `calculateSelectedHoldings`, but this time expressed directly in the config.

At this point you *might* be a little bit bored of this particular example, but let's go ahead and solve it in yet another way. Rather than dynamically filtering `holdings` based on `selectedHoldingIds`, we could define a calculated property `isSelected` for each holding that indicates whether that holding is selected. This simplifies the aggregate significantly.

> Example: Defining selected for each holding
```js
"_model": {
    "holdings": {
        "calculated": {
            "properties": {
                "isSelected": {
                    "data": "{{_ model().selectedHoldingIds _}}",
                    "transformPipeline": [
                        {
                            "name": "contains",
                            "options": {
                                "criteria": "{{_ self().holdingId _}}"
                            }
                        }
                    ]
                }
            }
        }
    }
}
```

Since we want this calculated property to be applied for each `holding` in `holdings`, we use a `properties` definition rather than `aggregates`. Once we have this in place, expressing the aggregate becomes cleaner:

> Example: Defining the holdings.selected aggregate once again
```js
"_model": {
    "holdings": {
        "calculated": {
            "aggregates": {
                "selected": {
                    "data": "{{_ self() _}}",
                    "transformPipeline": [{
                        "name": "filter",
                        "options": {
                            "criteria": "isSelected"
                        }
                    }]
                }
            }
        }
    }
}
```

# Transformations (also referred to as "transforms")
Transformations are a key concept used within the asterix-ng module. The basic purpose is that of a *data adapter* pattern; it allows us to transform data from one format into another.

Note: transformations make use of angular's $q promise service which is why they are in asterix-ng. If this was replaced with another promise service they could be moved to asterix-core in the future.

There are two base types of transformations: *synchronous* (sync) and *asynchronous* (async). These are defined by their expected return values; a sync transform is expected to return a value immediately, whereas an async transform is expected to return a promise that will be resolved later. For most cases, sync transforms are good enough, however, if a transform needs to do some asynchronous work (e.g. an API call), the async approach is needed.

Note that generally there's rarely a need to call `transforms.transform` directly. This will be done by asterix-ng internally when using for example `ModelBinder` (`mstarComponent.bindModelData`). See the *Usage* section below.

## Sync transforms
### Array wrap
In this example we have a sync transform in its most basic form. We receive `data` as our input and return it wrapped in an array.
> Example: Defining arrayWrap
```js
morningstar.asterix.ng.transforms.register('arrayWrap', function(data) {
    return [data];
});
```
> Example: Using arrayWrap
```js
morningstar.asterix.ng.transforms.transform(['arrayWrap'], 2);
// -> [2]
```
You may notice that as its first argument, `transform` expects an array. This is due to the chainability of transforms (hence the name *transformPipeline*). See the *Piping transforms* section below.

### Multiply
Here we allow the caller of the transform to specify a transform *option* called `multiplier`. This is additional data that will be used internally by the transform.
> Example: Defining multiply
```js
morningstar.asterix.ng.transforms.register('multiply', function(data) {
    return this.options.multiplier * data;
});
```
> Example: Using multiply
```js
morningstar.asterix.ng.transforms.transform([{name: 'multiply', options: { multiplier: 2 }}], 2);
// -> 4
```
In this example, since our transform is expecting an option, we need to refer to it by its object syntax. When doing this, `name` specifies the transform name and `options` specifies the options object. Options are a great way of creating flexible and re-usable transforms.

## Async transforms
Defining async versions of *arrayWrap* and *multiply* is very similar to the sync approach. The only difference being that we return a promise that (at some point...) resolves with the expected value as its result. Note, however, that when invoking async transforms, we need to make sure that we use the `transform.transformAsync` method.
> Example: Defining arrayWrapAsync
```js
morningstar.asterix.ng.transforms.register('arrayWrapAsync', function(data) {
    // $q is a built-in angular utility used to create promises
    return $q(function(resolve, reject) {
        resolve([data]);
    });
});
```
> Example: Using arrayWrapAsync
```js
morningstar.asterix.ng.transforms.transformAsync(['arrayWrapAsync'], 2).then(function(result) {
    // result is [2]
});
```
> Example: Defining multiplyAsync
```js
morningstar.asterix.ng.transforms.register('multiplyAsync', function(data) {
    var self = this;
    // $q is a built-in angular utility used to create promises
    return $q(function(resolve, reject) {
        resolve(data * self.options.multiplier);
    });
});
```
> Example: Using multiplyAsync
```js
morningstar.asterix.ng.transforms.transformAsync([{name: 'multiplyAsync', options: { multiplier: 2 } }], 2).then(function(result) {
    // result is 4
});
```

## Piping transforms
As mentioned above, the `transform` method expects an array as its first argument. This is to support a *pipeline* of transformations. When looking into pre-defined transforms, you'll soon realize that most of them are small, re-usable blocks of code. By using the pipeline approach, we can construct complex transforms by using these smaller blocks in conjunction each other. This goes well hand-in-hand with one of our overall goals: to reduce/eliminate code duplication and repetition.

Before writing your own custom transform, always try to analyze the issue at hand and break it up into smaller pieces. Maybe this particular problem could be approached by using a pipeline of existing transforms? There are, of course, very specific cases that can and should be resolved by creating a new custom transform, but these cases should be the exception rather than the norm.

Sometimes you may run into cases where the issue is simple enough that it (according to you) *should* be solvable by pre-defined transforms, but isn't. In such cases, we encourage you to implement the transform in asterix-ng and create a PR. This way, we end up with something re-usable for other developers, rather than just solving your specific problem.

### Piping the sync transforms we defined in the examples above
Lets demonstrate a pipeline example by using the transforms we defined above: *arrayWrap* and *multiply*.
> Example: Using multiply and arrayWrap in a sequence
```js
morningstar.asterix.ng.transforms.transform([{name: 'multiply', options: { multiplier: 2 }}, 'arrayWrap'], 2);
// -> [4]
```
### Piping the async transforms we defined in the examples above
> Example: Using multiplyAsync and arrayWrapAsync in a sequence
```js
morningstar.asterix.ng.transforms.transformAsync([{name: 'multiplyAsync', options: { multiplier: 2 }}, 'arrayWrapAsync'], 2).then(function(result) {
    // result is [4]
});
```

Piping async and sync transforms *is* allowed when using `transformAsync`, so we could also do:
> Example: Using multiplyAsync and arrayWrap in a sequence
```js
morningstar.asterix.ng.transforms.transformAsync([{name: 'multiplyAsync', options: { multiplier: 2 }}, 'arrayWrap'], 2).then(function(result) {
    // result is [4]
});
```

### Composite (pre-defined pipelines of) transforms
Sometimes it might be useful to define a transform based on a pipeline of other transforms. For these cases, we use `transform.register` as always, but rather than passing a function as its second argument, we pass in an array.

> Example: Defining a transform based on a pipeline of other transforms
```js
morningstar.asterix.ng.transforms.register('mapSingle', ['arrayWrap', {name: 'map', options: { criteria: { path: 'criteria'  } } }, 'first']);
```

In the example above (taken from *asterix-ng*), we define a transform *mapSingle* that is essentially just a single element wrapper case of the pre-defined transform *map* (see the *Pre-defined transforms* section below). The first segment is wrapping the element in an array (since *map* expects an array), the second segment is the actual *map* call and the third is a call to *first* (to unwrap the element from the array again). You might have noticed that the second segment has `options: { criteria: { path: 'criteria' } }` specified. This is telling the *map* transform that it should map its inner `criteria` option to the outer `criteria` option.

**Note** that this composite way of defining transforms does **not** currently support passing in function segments directly.

## Pre-defined transforms

A set of pre-defined transforms are available to use in *asterix-ng*, including some wrapping functions from the *underscore* library. These cover common functionality, allowing us to perform tasks such as *filter* or *find* with just configuration, thus helping to reduce code duplication. As with custom transforms, these pre-defined transforms can be chained through a *transformPipeline* allowing us to perform complex transformations of data using config only.

`arrayWrap` - returns the specified data wrapped in an array
```js
"model": {
    "values": ['aaa', 'bbb', 'ccc'],
    "wrappedLength": {
        "dataSourceType": "selector",
        "selector": "{{_ model().values.length _}}",
        "transformPipeline": [
            {
                "name": "arrayWrap"
            }
        ]
    }
}
// model.wrappedLength = [3]
```

`booleanize` - returns a boolean value from the given input (using !! notation)
```js
"model": {
    "value1": 'aaa',
    "value2": 0,
    "booleanized1": {
        "dataSourceType": "selector",
        "selector": "{{_ model().value1 _}}",
        "transformPipeline": [
            {
                "name": "booleanize"
            }
        ]
    },
    "booleanized2": {
        "dataSourceType": "selector",
        "selector": "{{_ model().value2 _}}",
        "transformPipeline": [
            {
                "name": "booleanize"
            }
        ]
    }
}
// model.booleanized1 = true
// model.booleanized2 = false
```

`filter` - filter an array based on specified criteria
```js
// Example 1
"model": {
    "values": ["aaa", "bbb", "ccc", "abc"],
    "filteredValues": {
        "dataSourceType": "selector",
        "selector": "{{_ model().values _}}",
        "transformPipeline": [
            {
                "name": "filter",
                "options": {
                    "criteria": {
                        "fn": "startsWith",
                        "args": {
                            "value": "a"
                        }
                    }
                }
            }
        ]
    }
}
// model.filteredValues = ["aaa", "abc"]

// Example 2
"model": {
    "values": [{"a": 5, "b": 1}, {"a": 4, b: "0"}, {"a": 4, "b": 1}],
    "filteredValues": {
        "dataSourceType": "selector",
        "selector": "{{_ model().values _}}",
        "transformPipeline": [
            {
                "name": "filter",
                "options": {
                    "criteria": {
                        "fn": "and",
                        "args": [{
                            "fn": "eq",
                            "args": {
                                "property": "a",
                                "value": 4
                            }
                        },
                        {
                            "fn": "gt",
                            "args": {
                                "property": "b",
                                "value": 0
                            }
                        }]
                    }
                }
            }
        ]
    }
}
// model.filteredValues = [{"a": 4, "b": 1}]
```

`join` - returns the items from an array as a string joined with the specified separator
```js
"model": {
    "values": ["aaa", "bbb", "ccc"],
    "value": {
        "dataSourceType": "selector",
        "selector": "{{_ model().values _}}",
        "transformPipeline": [
            {
                "name": "join",
                "options": {
                    "separator": ","
                }
            }
        ]
    }
}
// model.value = "aaa,bbb,ccc"
```

`mapSingle` - single element case of the *map* function (see *Composite transforms* section for more info)
```js
"model": {
    "item": {
        "a": 1,
        "b": 2,
        "c": 3,
        "d": {
            "e": {
                f: 4
            }
        }
    },
    "newItem": {
        "dataSourceType": "selector",
        "selector": "{{_ model().item _}}",
        "transformPipeline": [
            {
                "name": "mapSingle",
                "options": {
                    "criteria": {
                        "fn": "select",
                        "args": {
                            "a": "b", "b": "c", "c": "d.e.f"
                        }
                    }
                }
            }
        ]
    }
}
// model.newItem = {a: 2, b: 3, c: 4}
```

`pluck` - extract a list of property values from a collection
```js
"model": {
    "items": [
        {
            "a": 1,
            "b": 2,
            "c": 3
        },
        {
            "a": "aaa",
            "b": "bbb",
            "c": "ccc"
        },
        {
            "a": "AA",
            "c": "CC"
        }
    ],
    "newItems": {
        "dataSourceType": "selector",
        "selector": "{{_ model().items _}}",
        "transformPipeline": [
            {
                "name": "pluck",
                "options": {
                    "criteria": ["b"]
                }
            }
        ]
    }
}
// model.newItems = [2, 'bbb', null]
```

`split` - split a string into an array at the specified separator
```js
"model": {
    "value": "aaa|bbb|ccc",
    "values": {
        "dataSourceType": "selector",
        "selector": "{{_ model().value _}}",
        "transformPipeline": [
            {
                "name": "split",
                "options": {
                    "separator": "|"
                }
            }
        ]
    }
}
// model.values = ["aaa", "bbb", "ccc"]
```

`transformChildren` - transform child items in an object using specified options
```js
"model": {
    "item": {
        "child1": [1, 1, 1],
        "child2": {
            "innerChild": [true, false, 0, 1, null]
        },
        "child3": [1, 5, 12, 17, 9, 21, 13, 2]
    },
    "transformedChildren": {
        "dataSourceType": "selector",
        "selector": "{{_ model().item _}}",
        "transformPipeline": [
            {
                "name": "transformChildren",
                "options": {
                    "childMap": {
                        "child1": [{ "name": "reduce", "options": { "criteria": [{ "fn": "sum" }, 0] } }],
                        "child2.innerChild": [{ name: "filter" }],
                        "child3": [{ "name": "filter", "options": { "criteria": { "fn": "gt", "args": 10 } } }]
                    }
                }
            }
        ]
    }
}
/*
    model.transformedChildren = {
        "child1": 3,    // sum of 1+1+1
        "child2": {
            "innerChild":[true, 1]   // only truthy values
        },
        "child3": [12, 17, 21, 13] // only > 10
    }
*/
```

As described above, transformations can be chained so data can be piped from one to another in order to achieve the desired final output.

Example - get the IDs of all items with a specified category as a comma-delimited string:
```js
"model": {
    "items": [
        {"id": "aaa", "category": 1},
        {"id": "bbb", "category": 3},
        {"id": "ccc", "category": 1},
        {"id": "ddd", "category": 1},
        {"id": "eee", "category": 2}
    ],
    "idList": {
        "dataSourceType": "selector",
        "selector": "{{_ model().items _}}",
        "transformPipeline": [{
            "name": "filter",
            "options": {
                "criteria": {
                    "fn": "eq",
                    "args": {
                        "property": "category",
                        "value": 1
                    }
                }
            }
        },
        {
            "name": "pluck",
            "options": {
                "criteria": ["id"]
            }
        },
        {
            "name": "join",
            "options": {
                "separator": ","
            }
        }]
    }
}
// model.idList = "aaa,ccc,ddd"
```

**underscore functions**
A set of underscore functions are available as pre-defined transforms.

* compact
* difference
* filter
* find
* first
* flatten
* indexOf
* intersection
* last
* lastIndexOf
* map
* object
* range
* reduce
* reduceRight
* rest
* sample
* size
* union
* uniq
* unzip
* where
* without
* zip

**Pre-defined functions**
A set of pre-defined functions are available to call from transforms. These are specified with the *fn* property in the transform *criteria*.

`sum, difference, product, quotient` - aggregate functions to use with *reduce* transform
```js
"model": {
    "input": [2, 4, 4]
    "output": {
        "dataSourceType": "selector",
        "selector": "{{_ model().input _}}",
        "transformPipeline": [
            {
                "name": "reduce",
                "options": {
                    "criteria": {
                        "fn": "product"
                    }
                }
            }
        ]
    }
}
// model.output = 64
```

`select` - create a new object based on selected property values from another object
```js
"model": {
    "input": [{ "a": 1, "b": 2, "c": 3 }, { "a": "a", "b": "b", "c": "c" }, { "a": "A", "b": "B", "c": "C" }],
    "output": {
        "dataSourceType": "selector",
        "selector": "{{_ model().input _}}",
        "transformPipeline": [
            {
                "name": "map",
                "options": {
                    "criteria": {
                        "fn": "select",
                        "args": {
                            "one": "a", "two": "b", "three": "c"
                        }
                    }
                }
            }
        ]
    }
}
// model.output =  [{"one":1,"two":2,"three":3},{"one":"a","two":"b","three":"c"},{"one":"A","two":"B","three":"C"}]
```

`add, subtract, divide, multiply` - add, subtract, divide or multiply value by a specified number
```js
"model": {
    "input": [1, 2, 3],
    "output": {
        "dataSourceType": "selector",
        "selector": "{{_ model().input _}}",
        "transformPipeline": [
            {
                "name": "map",
                "options": {
                    "criteria": {
                        "fn": "multiply",
                        "args": 2
                    }
                }
            }
        ]
    }
}
// model.output =  [2 ,4, 6]
```

`eq, gt, lt, gte, lte` - compare two values
```js
"model": {
    "input": [1, 2, 3, 4],
    "output": {
        "dataSourceType": "selector",
        "selector": "{{_ model().input _}}",
        "transformPipeline": [
            {
                "name": "map",
                "options": {
                    "criteria": {
                        "fn": "gt",
                        "args": 2
                    }
                }
            }
        ]
    }
}
// model.output = [false, false, true, true]
```

`empty, nonEmpty` - test if a value is empty or non-empty
```js
"model": {
    "input": [null, "", 1, undefined],
    "output": {
        "dataSourceType": "selector",
        "selector": "{{_ model().input _}}",
        "transformPipeline": [
            {
                "name": "map",
                "options": {
                    "criteria": {
                        "fn": "empty"
                    }
                }
            }
        ]
    }
}
// model.output = [true, false, false, true]
```

`and, or, not` - test specified criteria, e.g. when using *filter* transform
```js
"model": {
    "input": [{"a": 1, "b": 10}, {"a": 2, "b": 0}, {"a": 1, "b": 2}],
    "output": {
        "dataSourceType": "selector",
        "selector": "{{_ model().input _}}",
        "transformPipeline": [
            {
                "name": "map",
                "options": {
                    "criteria": {
                        "fn": "and",
                        "args": [{
                            "fn": "eq",
                            "args": {
                                "property": "a",
                                "value": 1
                            }
                        },
                        {
                            "fn": "gt",
                            "args": {
                                "property": "b",
                                "value": 2
                            }
                        }]
                    }
                }
            }
        ]
    }
}
// model.output = [true, false, false]
```

`startsWith` - test if a value starts with a specified string
```js
"model": {
    "input": ["aaa", "bbb", "ccc", "abc"],
    "output": {
        "dataSourceType": "selector",
        "selector": "{{_ model().input _}}",
        "transformPipeline": [
            {
                "name": "map",
                "options": {
                    "criteria": {
                        "fn": "startsWith",
                        "args": {
                            "value": "a"
                        }
                    }
                }
            }
        ]
    }
}
// model.output = [true, false, false, true]
```

`defaultIfEmpty` - set default values in an object where values are missing
```js
"model": {
    "input": [{"a": "aaa", "c": "CCC", "d": null}],
    "output": {
        "dataSourceType": "selector",
        "selector": "{{_ model().input _}}",
        "transformPipeline": [
            {
                "name": "map",
                "options": {
                    "criteria": {
                        "fn": "defaultIfEmpty",
                        "args": {
                            "defaults": {
                                "a": "a",
                                "b": "b",
                                "c": "c",
                                "d": "d"
                            }
                        }
                    }
                }
            }
        ]
    }
}
// model.output = [{"a":"aaa", "c":"CCC", "d":"d", "b":"b"}]
```

`appendString` - append a string to another
```js
"model": {
    "input": "foo",
    "output": {
        "dataSourceType": "selector",
        "selector": "{{_ model().input _}}",
        "transformPipeline": [
            {
                "name": "appendString",
                "options": {
                    "string": "Bar"
                }
            }
        ]
    }
}
// model.output = "fooBar"
```

`prependString` - prepend a string to another
```js
"model": {
    "input": "foo",
    "output": {
        "dataSourceType": "selector",
        "selector": "{{_ model().input _}}",
        "transformPipeline": [
            {
                "name": "prependString",
                "options": {
                    "string": "Bar"
                }
            }
        ]
    }
}
// model.output = "Barfoo"
```

`trim`, `trimLeft`, `trimRight` - Apply the string prototype function with the same name
```js
"model": {
    "input": " foo ",
    "output1": {
        "dataSourceType": "selector",
        "selector": "{{_ model().input _}}",
        "transformPipeline": [
            {
                "name": "trim"
            }
        ]
    },
    "output2": {
        "dataSourceType": "selector",
        "selector": "{{_ model().input _}}",
        "transformPipeline": [
            {
                "name": "trimLeft"
            }
        ]
    },
    "output3": {
        "dataSourceType": "selector",
        "selector": "{{_ model().input _}}",
        "transformPipeline": [
            {
                "name": "trimRight"
            }
        ]
    }
}
// model.output1 = "trim"
// model.output2 = "trim "
// model.output3 = " trim"
```

`toUpperCase`, `toLowerCase`, `toLocaleUpperCase`, `toLocaleLowerCase` - Apply the string prototype function with the same name
```js
"model": {
    "input": "FooBar",
    "output1": {
        "dataSourceType": "selector",
        "selector": "{{_ model().input _}}",
        "transformPipeline": [
            {
                "name": "toUpperCase"
            }
        ]
    },
    "output2": {
        "dataSourceType": "selector",
        "selector": "{{_ model().input _}}",
        "transformPipeline": [
            {
                "name": "toLowerCase"
            }
        ]
    }
}
// model.output1 = "FOOBAR"
// model.output2 = "foobar"
```


## Usage
As mentioned above, transformations are used internally by asterix-ng in multiple places. A brief overview of these are:
- `ModelBinder`
    - when fetching data from an API or from a selector
    - when calculating *calculated properties*
    - when propagating model data to a parent (*propagateChanges*)
- `mstarModelValidate`
    - when evaluating model validation rules

Depending on the *context* in which `transform` is called, it might be passed a third (optional) argument: `context`. This is an object that will be exposed to all transforms in the pipeline through `this.context`. It is, for example, used in the from API/selector data transform to expose *labels* and *settings*.

There's also `this.origData` that can be used to access the initial data (that is, what was passed in at the beginning of the pipeline) and `this.options` (mentioned above) that will assume the form of whatever was specified in the transform pipeline for the transform in question.


# Services

## mstarModal

Creates and launches a Modal with the specified content.

```js
mstarModal.launch(content, modalScope, options);
```

- content (string): the HTML content to add to the modal
- modalScope (object): the scope of the component calling the modal
- options (object):
    - overrideTemplate (boolean): whether to override the default template or not (default: false)
    - parentComponentPath (string): the `mstarComponentId` of the component calling the modal
    - modalId (string): and `id` attribute for the modal HTML element 
    - open (boolean): whether to automatically open the modal when calling launch (default: true)

Default modal template
```html
<div class="mstar-modal__content">
    <div data-mstar-include="modalContent"></div>
    <div data-ng-click="closeModal()" class="mstar-modal__close-btn"></div>
</div>
<div class="mstar-modal__mask" data-ng-click="closeModal()"></div>
```

When passing `false` to the `open` option, the service creates the modal but doesn't open it by default, instead the modal HTML element is returned by the service as a promise.
Example (using mwc-ec-ui API for launching an mwc-ec-mds-modal):

```js
mstarModal.launch('someContent', modalScope, {
    open: false,
    modalId: 'modalElementId'
}).then(function(modalElement) {
    mwcECUI.launchModal(modalElement.html(), 'modalElementId', settings, labels);
});
```

## mstarModelValidate
Responsible for listening to changes in the bindings involved (see the *Configuration structure* example below) and updating the validation state when a change occurs.

> Example: Binding and resetting
```js
// Hooks up the model validation rules (see below) for the given instance
mstarModelValidate.bind(instance);
// Used for testing to remove the listeners and reset the state
mstarModelValidate.reset();
```

> Example: Configuration structure
```js
  _validation: {
    validationMessageHandlerType: 'self OR parent', // "self" indicates that the message(s) should appear in this instance's validation message container (this needs to be registered through mstarValidationMessageContainers.register). "parent" indicates that the message(s) should be handled by an ascendant.
    modelRules: {
        testRule1: {
            labelKey: 'testArr_sums_to_3',
            data: '{{_ model().testArr _}}', // the input data for "transformPipeline"
            transformPipeline: [{
                name: 'reduce',
                options: {
                    criteria: [{fn: 'sum'}, 0]
                }
            }],
            ok: 3 // to be considered valid, the result of transformPipeline above should equal the value of "ok"
        },
        testRule2: {
            labelKey: 'testArr_has_3_elements',
            data: '{{_ model().testArr _}}', // the input data for "transformPipeline"
            transformPipeline: ['size'],
            ok: 3 // to be considered valid, the result of "transformPipeline" should equal the value of "ok"
        }
    }
 }
```

## mstarValidationMessageContainers
Responsible for listening to validationStateChange and determine whether or not a given state (own or descendant) should be handled by this container's handleMessages.

> Example: Registering and resetting
 ```js
 // Registers the instance as a validation message container with the using the provided handleMessages function
mstarValidationMessageContainers.register(instance, handleMessages);
// Used for testing to remove the listeners and reset the state
mstarValidationMessageContainers.reset();
 ```

### ValidationMessages (class)
handleMessages will receive an instance of ValidationMessages. See asterix-core's Validation State documentation (todo: link) for the structure of messages.

> Public interface
```js
getAll() - Retrieves all messages
getErrors() - Retrieves all error messages
```

todo: Document the other services.

# Directives

## mstarValidate
Responsible for listening to changes on the field level (through ng-model) and updating the validation state when a change occurs. todo: This should be hooked up with mstarValidationMessageContainers.

In order to enable validation the `mstar-validate` attribute must be added to the HTML element:

```html
<ec-element mstar-component-id="instanceName" mstar-validate></ec-element>
```

And a `_validation` object must be added to the component settings:

```json
{
  "settings": {
    "_validation": {
      "isRequired": {
        "type": "required",
        "labelKey": "fieldIsRequired"
      },
      "errorWhen": "dirty"
    }
  },
  "labels": {
    "fieldIsRequired": {
      "en": "Mandatory Field!"
    }
  }
}
```

There are 5 different ways of triggering validation, defined with the attribute `errorWhen` (for an inline element) or `tooltipWhen` (for a tooltip). Please note that the tooltip option will be deprecated as part of the MDS upgrade so new configurations should use `errorWhen`:
- never
- always
- touched - When the element gets clicked or hovered [`default`]
- dirty - When the model changes
- manual

If set to `manual` there's a function available on the component's instance that can be called to trigger the validation:

```js
morningstar.asterix.instanceRegistry.get('instanceName').triggerValidation();
```

todo: Document the other directives.


# Usage tracking
AsterixNg performs some built-in usage tracking for components. They are as follows.

- mstarAjax/API calls
API calls made with model-binding or otherwise using mstarAjax module are tracked for time taken to complete. Event-type is *"timing"*. *"ajax.api"* & *"ajax.jsonp"* operations are tracked with the time taken to load them.

- Component loaded
Time taken to load component is tracked. Event type is *"timing"*. In this case the start time is considered as *window.performance.timing.fetchStart*. The end-time depends on how the component's usageTracking metadata is configured. If this metadata is not defined for component then "component load" tracking will not be enabled for the component 

To set this metadata refer to below code
```js
    morningstar.asterix.meta.register('ecScreener', 'usageTracking', {
        componentLoad: {
            endsWhen: 'elementRendered',
            dependentModelProperty: 'securities',
            modelPropertyPathToCheck: 'rows.0.legalName',
            elementSelector: '.ec-table__investment-link'
        }
    });
```

`usageTracking.componentLoad` metadata object
| Property | Description |
| -------- | ----------- |
| endsWhen | When the timer for load component should stop. Possible values are `directiveEnds`, `dataLoaded`: When api data is loaded & `elementRendered`: when an element is rendered after api-data is loaded. For *elementRendered*, additional properties such as *dependentModelProperty*, *modelPropertyPathToCheck* & *elementSelector* are also *required* |
| elementSelector | Selector for the element to check for |
| dependentModelProperty | Model object for which to wait before checking existence of element |
| modelPropertyPathToCheck | Model-property-path or datapoint through which element is populated |
| intervalDelay | Check for element is rendered with expected value is ran at each interval. The internal can be configured here in milliseconds. Default is **100** milliseconds|
| intervalCount | Max no of times the interval will be run. Default is **50** times |

- `directiveEnds` it waits for directive linkFn execution to complete
- `dataLoaded`:  it waits for api to be loaded
- `elementRendered`: it waits for element to be rendered and has the value specified in `modelPropertyPathToCheck` property. It runs an interval for milliseconds for counts

For registering Usage tracking listeners, refer to Asterix Core documentation
