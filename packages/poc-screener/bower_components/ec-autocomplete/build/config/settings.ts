// startref
/// <reference path='../../.references.ts'/>
// endref
module EcAutocomplete {
    export interface Settings extends AsterixNg.ComponentSettings {
        _model: {
            securities: AsterixCore.DataSourceSettings;
        }

        _templates: Templates;

        _parameters: {

            filters: AsterixNg.Parameter;

            latestSelectedSecurity: AsterixNg.Parameter;

            maxRows: AsterixNg.Parameter;

            securityDataPoints: AsterixNg.Parameter;

            sortField: AsterixNg.Parameter;

            sortOrder: AsterixNg.Parameter;

            search: AsterixNg.Parameter;

            subUniverseId: AsterixNg.Parameter;

            universeId: AsterixNg.Parameter;

            urlKey: AsterixNg.Parameter;
        };

        /**
         * @default holdingTypeId
         */
        groupBy: string;

        /**
         * @default
         */
        filters: any;

        /**
         * value in which the security data object is stored
         */
        latestSelectedSecurity: any;

        /**
         * @default 100
         */
        maxRows: number;

        /**
         * @default 2
         */
        minSearchSize: number;

        /** Input name, used to reference angular form
         * @default
         * @type string
         */
        name: string;

        /** The namespaceClass
         * @default ec-autocomplete
         * @type string
         */
        namespaceClass: string;

        /**
         * @default {"name": "name", "secId": "secId", "holdingTypeId": "holdingTypeId"}
         */
        propagatedFields: any;

        /**
         * show results list when focusing the component and search criteria is not empty
         * @default false
         */
        restoreResultsOnFocus: boolean;

        /**
         * @default
         */
        search: string;

        /**
         * security data points
         * @default SecId,TenforeId,Name,InvestmentType,HoldingTypeId,Universe
         */
        securityDataPoints: string;

        /**
         * Add the aria-describedby attribute containing descriptive text
         * @default true
         */
        showAriaDescribedBy: boolean;

        /**
         * show or hide the search text after selection of security
         * @default true
         */
        showSearchText: boolean;

        /**
         * @default name
         */
        sortField: string;

        /**
         * @default asc
         */
        sortOrder: string;

        /**
         * SubUniverse id, multiple split by |
         * @default
         */
        subUniverseId: string;

        /**
         * Universe id
         */
        universeId: string;

        /** Usage tracking Hit objects
         */
        usageTrackingHitMap: any

        /**
         * @default
         */
        urlKey: string;
    }

    export interface Templates {
        /** The main template.
         * @format html
         * @default <div class=\"{{ ::namespaceClass('search') }}\" data-ng-class=\"loading\">\r\n    <ec-input mstar-component-id=\"searchInput\"\r\n        class=\"{{ ::namespaceClass('search__input') }}\"\r\n        data-name=\"{{ :: settings.get('name') || namespaceId('searchInput') }}\"\r\n        data-ng-model=\"parameters.search\"\r\n        data-ng-model-options=\"{ updateOn: 'default', debounce: {'default': 300, 'blur': 0} }\"\r\n        data-aria-autocomplete=\"both\"\r\n        data-aria-owns=\"{{ ::namespaceId('results') }}\"\r\n        data-aria-label=\"{{ :: labels.contains('ariaLabel') ? labels.get('ariaLabel') : undefined }}\"\r\n        data-aria-describedby=\"{{ :: showAriaDescribedBy ? namespaceId('describedby') : undefined }}\"\r\n        data-role=\"combobox\"><\/ec-input>\r\n    <div class=\"{{ ::namespaceClass('search-spinner') }}\" >\r\n        <div data-ng-repeat=\"i in ::times(12) track by $index\"\r\n            class=\"{{ ::namespaceClass('search-spinner__bar') }} {{ ::namespaceClass('search-spinner__bar--'+ $index) }}\">\r\n        <\/div>\r\n    <\/div>\r\n\r\n    <div class=\"{{ ::namespaceClass('container') }}\" data-ng-show=\"showResult\">\r\n        <ul id=\"{{ ::namespaceId('results')}}\" class=\"{{ ::namespaceClass('container__results') }}\" role=\"listbox\" tabindex=\"-1\" aria-expanded=\"showResult\">\r\n            <li id=\"{{ ::namespaceId('results' + $index)}}\"\r\n                class=\"{{ ::namespaceClass('container__item') }} {{ ::namespaceClass('container__item')+'_'+$index }}\"\r\n                data-ng-class=\"{ '{{::namespaceClass('container__item--group')}}': s.group}\"\r\n                data-ng-repeat=\"s in securities\"\r\n                data-ng-click=\"!s.group && selectItem(s)\"\r\n                role=\"option\"\r\n                tabindex=\"-1\">\r\n                <div mstar-include=\"templates.item\"><\/div>\r\n            <\/li>\r\n        <\/ul>\r\n    <\/div>\r\n    <span id=\"{{ ::namespaceId('describedby')}}\" data-ng-show=\"false\" data-ng-if=\"showAriaDescribedBy\">{{ ::labels.get('ariaDescription') }}<\/span>\r\n    <div class=\"{{ ::namespaceClass('__zero-result-found') }}\" data-ng-show=\"showNoResults && !loading\">\r\n        <span>{{ ::labels.get('noResults') }}<\/span>\r\n    <\/div>\r\n<\/div>\r\n
         */
        main: string;

        /** The single item template.
         * @format html
         * @default <span>{{s.name}}<\/span>
         */
        item: string;
    }
}
