// startref
/// <reference path='../../.references.ts'/>
// endref
module EcAutocomplete {
    export enum _Type { ecAutocomplete }

    /**
     * @title Autocomplete
     */
    export interface EcAutocomplete {
        /** Component type.
         * @default ecAutocomplete
         * @options.hidden true
         * @type string
         * @readonly true
         */
        type: _Type;

        settings: Settings;

        labels: Labels;

        components: {
            searchInput: EcInput.EcInput;
        }
    }
}
