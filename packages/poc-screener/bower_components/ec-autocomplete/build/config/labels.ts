// startref
/// <reference path='../../.references.ts'/>
// endref
module EcAutocomplete {
    export interface Labels {
        holdingTypeId1: AsterixCore.Label;
        holdingTypeId2: AsterixCore.Label;
        holdingTypeId21: AsterixCore.Label;
        holdingTypeId22: AsterixCore.Label;
        holdingTypeId3: AsterixCore.Label;
        holdingTypeId4: AsterixCore.Label;
        holdingTypeId7: AsterixCore.Label;
        holdingTypeId8: AsterixCore.Label;
        holdingTypeId9: AsterixCore.Label;
        holdingTypeId23: AsterixCore.Label;
        holdingTypeId50: AsterixCore.Label;
        FC: AsterixCore.Label;
        FE: AsterixCore.Label;
        FI: AsterixCore.Label;
        FO: AsterixCore.Label;
        noResults: AsterixCore.Label;
        ST: AsterixCore.Label;
    }
}
