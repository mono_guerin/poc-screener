// startref
/// <reference path='../../.references.ts'/>
// endref
module EcDisclaimer {
    export interface Labels {
        careInfo: AsterixCore.Label;
        copyright: AsterixCore.Label;
        disclaimerTitle: AsterixCore.Label;
        logoAltTitle: AsterixCore.Label;
    }
}
