// startref
/// <reference path='../../.references.ts'/>
// endref
module EcDisclaimer {
    export interface Settings extends AsterixNg.ComponentSettings {
        _model: AsterixCore.DataSourceSettings;
        _parameters: any;
        _templates: Templates;

        /** copyright logo link */
        copyRightLinkUrl: string;

        /** Logo link*/
        logoLinkUrl: string;

        /**Logo override source link*/
        logoSrc: string;

        /** SVG content of the default logo */
        logoSvg: string;

        /**
         * @default ec-disclaimer
         */
        namespaceClass: string;
    }

    export interface Templates {
        /** The logo image template.
         * @format html
         * @default <div class=\"{{ ::namespaceClass('__copyright') }}\">\r\n    <a data-ng-if=\"::settings.get('copyRightLinkUrl')\" target=\"_blank\"\r\n       class=\"{{ ::namespaceClass('__copyright--link') }}\"\r\n       data-ng-href=\"{{ ::settings.get('copyRightLinkUrl') }}\">{{labels.get('copyright')}}<\/a>\r\n    <div data-ng-if=\"::!settings.get('copyRightLinkUrl')\">{{labels.get('copyright')}}<\/div>\r\n<\/div>\r\n
         */
        copyRight: string;

        /** The logo image template.
         * @format html
         * @default <div class=\"{{ ::namespaceClass('__logo') }}\">\r\n    <a data-ng-if=\"::settings.get('logoLinkUrl')\"  target=\"_blank\"\r\n       data-ng-href=\"{{ ::settings.get('logoLinkUrl') }}\"\r\n       class=\"{{ ::namespaceClass('__logo--link') }}\">\r\n        <span data-ng-if=\"::!logoSrc\" mstar-include=\"::settings.get('logoSvg')\" \/>\r\n        <img alt=\"{{ ::labels.get('logoAltTitle') }}\" title=\"{{ ::labels.get('logoAltTitle') }}\"\r\n             data-ng-src=\"{{ ::logoSrc }}\" data-ng-if=\"::logoSrc\"\/>\r\n    <\/a>\r\n    <div data-ng-if=\"::!settings.get('logoLinkUrl')\">\r\n        <span data-ng-if=\"::!logoSrc\" mstar-include=\"::settings.get('logoSvg')\" \/>\r\n        <img alt=\"{{ ::labels.get('logoAltTitle') }}\" title=\"{{ ::labels.get('logoAltTitle') }}\"\r\n             data-ng-src=\"{{ ::logoSrc }}\" data-ng-if=\"::logoSrc\"\/>\r\n    <\/div>\r\n<\/div>\r\n
         */
        logo: string;

        /** The main template.
         * @format html
         * @default <ec-section>\r\n    <ec-container mstar-component-id=\"containerDisclaimer\">\r\n        <ec-section section-id=\"disclaimerContent\" data-template-type=\"light\">\r\n            <div class=\"{{ ::namespaceClass('__top') }}\">\r\n                <div class=\"{{ ::namespaceClass('__careinfo') }}\" data-ng-if=\"::labels.contains('disclaimerTitle')\">\r\n                    <strong>{{labels.get('disclaimerTitle')}}<\/strong>\r\n                <\/div>\r\n                <div class=\"{{ ::namespaceClass('__careinfo') }}\">\r\n                    {{labels.get('careInfo')}}\r\n                <\/div>\r\n            <\/div>\r\n        <\/ec-section>\r\n        <ec-section section-id=\"disclaimerCopyright\" data-template-type=\"light\">\r\n            <div mstar-include=\"::templates.copyRight\"><\/div>\r\n        <\/ec-section>\r\n        <ec-section section-id=\"disclaimerLogo\" data-template-type=\"light\">\r\n            <div mstar-include=\"::templates.logo\"><\/div>\r\n        <\/ec-section>\r\n    <\/ec-container>\r\n<\/ec-section>\r\n
         */
        main: string;
    }
}
