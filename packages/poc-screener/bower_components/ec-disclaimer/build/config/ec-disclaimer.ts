// startref
/// <reference path='../../.references.ts'/>
// endref
module EcDisclaimer {
    export enum _Type { ecDisclaimer }

    /**
     * @title Disclaimer
     */
    export interface EcDisclaimer {
        components: {
            containerDisclaimer: EcContainer.EcContainer;
        }

        settings: Settings;

        labels: Labels;

        /** Component type.
         * @default ecDisclaimer
         * @options.hidden true
         * @type string
         * @readonly true
         */
         type: _Type;
    }
}
