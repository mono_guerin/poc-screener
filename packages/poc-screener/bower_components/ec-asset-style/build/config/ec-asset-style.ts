// startref
/// <reference path='../../.references.ts'/>
// endref
module EcAssetStyle {
    export enum _Type { ecAssetStyle }

    /**
     * @title AssetStyle
     */
    export interface EcAssetStyle {
        components: {
            containerAssetStyle: EcContainer.EcContainer;
            tableMobile: EcTable.EcTable;
        }

        labels: Labels;

        settings: Settings;

        /** Component type.
         * @default ecAssetStyle
         * @options.hidden true
         * @type string
         * @readonly true
         */
        type: _Type;
    }
}
