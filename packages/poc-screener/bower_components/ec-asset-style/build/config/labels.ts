// startref
/// <reference path='../../.references.ts'/>
// endref
module EcAssetStyle {
    export interface Labels {
        assetStyleTitle: AsterixCore.Label;
        assetStyleWeight: AsterixCore.Label;
        percentBenchmark: AsterixCore.Label;     
        percentWeight: AsterixCore.Label;
        percentWeightProposed: AsterixCore.Label;
        xAxisEquityBlend: AsterixCore.Label;
        xAxisEquityCore: AsterixCore.Label;
        xAxisEquityGrowth: AsterixCore.Label;
        xAxisEquityGrowthAbbreviation: AsterixCore.Label;
        xAxisEquityValue: AsterixCore.Label;
        xAxisEquityValueAbbreviation: AsterixCore.Label;
        xAxisFixedIncomeExt: AsterixCore.Label;
        xAxisFixedIncomeLtd: AsterixCore.Label;
        xAxisFixedIncomeMod: AsterixCore.Label;
        yAxisEquityLarge: AsterixCore.Label;
        yAxisEquityLargeAbbreviation: AsterixCore.Label;
        yAxisEquityMed: AsterixCore.Label;
        yAxisEquitySmall: AsterixCore.Label;
        yAxisEquitySmallAbbreviation: AsterixCore.Label;
        yAxisFixedIncomeHigh: AsterixCore.Label;
        yAxisFixedIncomeLow: AsterixCore.Label;
        yAxisFixedIncomeMed: AsterixCore.Label;
    }
}
