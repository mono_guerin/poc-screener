// startref
/// <reference path='../../.references.ts'/>
// endref
module EcAssetStyle {
    export interface Settings extends AsterixNg.ComponentSettings {
        _model: {
            asOfDate: any;
            assetStyleBoxValue: any;
            assetStyleData: any;
        }

        _templates: Templates;

        _parameters: any;

        /** The bar legend
         * @default ["0","10","25","50",">"]
         * @type string
         */
        barLegend: string[];

        /** whether use blend labels of equity box
         * @default false
         * @type boolean
         */
        displayEquityBlend: boolean;

        /** The highlight label
         * @type string
         */
        highlightLabels: string[];

        /** The namespaceClass
         * @default ec-asset-style
         * @type string
         */
        namespaceClass: string;
         
        /** whether the style box can selectable
         * @default false
         */
        selectable: boolean;

         /** The selectable default class name
         * @default equity-base-0-9
         * @type string
         */
        selectableDefaultClassName: string; 

         /** The selected class name
         * @default equity-base-25-49
         * @type string
         */
        selectedClassName: string; 

        /** whether display Title and tooltip
         * @default true
         */
        showTitle: boolean;

        /** Import mbc-style-box, so define this setting to keep the parameter that using in mbc-style-box
        */
        styleBoxSettings: any;
 
    }

    export interface Templates {
        /** The main template.
         * @format html
         * @default <ec-section title=\"{{::settings.get(showTitle) ? labels.get('assetStyleTitle'):undefined }}\" title-tooltip-label-id=\"assetStyleTitleTooltip\" sub-title=\"{{ model.asOfDate && model.asOfDate.length !== 0 ? format.date(model.asOfDate) : null }}\">\r\n    <ec-container mstar-component-id=\"containerAssetStyle\">\r\n        <ec-section data-template-type=\"light\" section-id=\"highlight\">\r\n            <label class=\"{{::namespaceClass('__highlight-label')}}\">{{highlightItem.label}}<\/label>\r\n            <label class=\"{{::namespaceClass('__highlight-value')}}\">{{highlightItem.value}}<\/label>\r\n        <\/ec-section>\r\n        <ec-section data-template-type=\"light\" section-id=\"barLegend\">\r\n            <div class=\"{{::namespaceClass('__bar-label')}}\">\r\n                {{::labels.get('assetStyleWeight')}}\r\n            <\/div>\r\n            <div class=\"{{::namespaceClass('__bar-legend')}}\">\r\n                <ul class=\"{{::namespaceClass('__bar-legend--tab')}}\">\r\n                    <li data-ng-repeat=\"item in ::settings.get('barLegend')\" title=\"{{::item}}\" aria-label=\"{{::item}}\">\r\n                        <span class=\"{{::namespaceClass('__bar-legend--tab-'+$index)}} {{::settings.get('styleBoxSettings').colors[$index]}}\"><\/span>\r\n                        <span>{{::item}}<\/span>\r\n                    <\/li>\r\n                <\/ul>\r\n            <\/div>\r\n        <\/ec-section>\r\n        <ec-section data-template-type=\"light\" section-id=\"styleBox\">\r\n            <div id=\"{{::namespaceId('styleBox')}}\" class=\"{{::namespaceClass('__style-box')}}\">\r\n            <\/div>\r\n        <\/ec-section>\r\n        <ec-table data-template-type=\"light\" mstar-component-id=\"tableMobile\"><\/ec-table>\r\n    <\/ec-container>\r\n<\/ec-section>
         */
        main: string;
    }
}
