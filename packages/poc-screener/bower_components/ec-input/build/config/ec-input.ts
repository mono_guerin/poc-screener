// startref
/// <reference path='../../.references.ts'/>
// endref
module EcInput {
    export enum _Type { ecInput }

    export enum InputType {
        text,
        password,
        email,
        search,
        url,
        number,
        numberLocalized,
        date,
        'datetime-local',
        time,
        week,
        month,
        switchToggle,
        checkbox,
        checkboxGroup,
        radioGroup
    }

    export enum Autocomplete {
        on,
        off
    }

    export enum TextareaWrap {
        hard, soft
    }

    export interface EcInput {
        /** Component type.
         * @default ecInput
         * @options.hidden true
         * @type string
         * @readonly true
         */
        type: _Type;

        settings: Settings;

        labels: Labels;
    }

    export interface Settings extends AsterixNg.ComponentSettings {
        _model: {
            listItems: any;
        };

        _parameters: Parameters;

        _templates: Templates;

        _validation: AsterixCore.Validation;

        /**
         * @default off
         */
        autocomplete?: Autocomplete;

        /**
         * @default false
         */
        autofocus?: boolean;

        /** Textarea columns */
        cols?: number;

        /** http://api.jqueryui.com/datepicker/#options */
        datepickerOptions: DatepickerOptions;

        /**
         * @default false
         */
        disabled?: boolean;

        form?: string;

        /**
         * Enable/disable horizontal display for radio list
         * @default false
         */
        horizontal: boolean;

        /**
         * @default text
         */
        inputType: InputType;

        /**
         * Key field in Listitems for Select and Checkbox Group.
         * Default value of keyField is set as key.
         * @default key
         */
        keyField: string;

        /**
         * When true, the key field will be set into the ng-model instead of the whole object.
         * Supported by 'select' input type.
         * @default false
         */
        keyFieldInModel: boolean;

        /**
         * Change the name of radio button
         * @default button
         */
        nameField: string;

        /**
         * @default ec-input
         */
        namespaceClass: string;

        name?: string;

        /**
         * Override the pattern for the input when using the numberLocalized option
         * (which renders input as type text instead of number for IE and Edge)
         * Supports {decimalSeparator} token which will be replaced at runtime
         * with the decimal separator for the current culture (languageId)
         * @default [0-9]*[{decimalSeparator}]?[0-9]*
         */
        numberInputPattern?: string;

        /**
         * @default false
         */
        readonly?: boolean;

        /** Textarea rows */
        rows?: number;

        /**
         * @default 20
         */
        size?: number;

        /**
         * Value label in Listitems. Used in Select and Checkbox Group.
         * Default value of valueField is set as value.
         * @default value
         */
        valueField: string;

        /**
         * Like ValueField, it is intended to supplement label text like a microcopy.
         * Default value of microValueField is set as value.
         * @default value
         */
        microValueField: string;

        /** Textarea wrap */
        wrap?: TextareaWrap;
    }

    export interface Templates {
        /** The main template. Will be required from the directive, so the default here is empty and will only have a value when we want to override it.
         * @format html
         */
        main: string;

        /**
         * @default <input\r\n    type=\"{{ :: parameters.inputType}}\"\r\n    data-ng-disabled=\"parameters.disabled\"\r\n    data-ng-attr-name=\"{{ :: parameters.name || name || undefined }}\"\r\n    data-ng-attr-placeholder=\"{{ :: labels.contains('placeholder') ? labels.get('placeholder') : undefined }}\"\r\n    data-ng-attr-autocomplete=\"{{ :: (!parameters.name && !name) ? undefined : parameters.autocomplete || undefined }}\"\r\n    data-ng-attr-size=\"{{ :: parameters.size || undefined }}\"\r\n    data-ng-readonly=\"ngReadonly || parameters.readonly || undefined\"\r\n    data-ng-attr-autofocus=\"{{ :: parameters.autofocus || undefined }}\"\r\n    data-ng-class=\"{'ec-form__input--disabled': parameters.disabled}\"\r\n    data-mstar-validate\r\n    data-ng-model=\"ngModel\"\r\n    data-ng-attr-role=\"{{ :: role ? role : undefined }}\"\r\n    data-ng-attr-aria-label=\"{{ :: ariaLabel ? ariaLabel : labels.contains('ariaText') ? labels.get('ariaText') : undefined }}\"\r\n    data-ng-attr-aria-autocomplete=\"{{ :: ariaAutocomplete ? ariaAutocomplete : undefined }}\"\r\n    data-ng-attr-aria-owns=\"{{ :: ariaOwns ? ariaOwns : undefined }}\"\r\n    data-ng-attr-aria-describedby=\"{{ :: ariaDescribedby ? ariaDescribedby : undefined }}\"\r\n    class=\"ec-form__input ec-form__input--textbox\"\r\n    id=\"{{namespaceId('textbox')}}\" \/>\r\n
         */
        text: string;

        /**
         * @default <input type=\"{{::numberInputType}}\"\r\n    data-ng-disabled=\"parameters.disabled\"\r\n    data-ng-attr-name=\"{{ :: parameters.name || undefined }}\"\r\n    data-ng-attr-placeholder=\"{{ :: labels.contains('placeholder') ? labels.get('placeholder') : undefined }}\"\r\n    data-ng-readonly=\"ngReadonly || parameters.readonly || undefined\"\r\n    data-ng-attr-autofocus=\"{{ :: parameters.autofocus || undefined }}\"\r\n    data-mstar-validate\r\n    data-ng-model=\"ngModel\"\r\n    data-ng-attr-aria-label=\"{{ :: labels.contains('ariaText') ? labels.get('ariaText') : undefined }}\"\r\n    data-ng-attr-step=\"{{ :: numberInputType === 'number' ? 'any' : undefined}}\"\r\n    data-ng-attr-pattern=\"{{::numberInputPattern}}\"\r\n    class=\"ec-form__input ec-form__input--number\"\r\n    id=\"{{namespaceId('number')}}\" \/>\r\n
         */
        number: string;

        /**
         * @default <input\r\n    type=\"{{parameters.inputType}}\"\r\n    data-ng-disabled=\"parameters.disabled\"\r\n    data-ng-attr-name=\"{{ parameters.name || undefined }}\"\r\n    data-ng-readonly=\"ngReadonly || parameters.readonly || undefined\"\r\n    data-ng-attr-autofocus=\"{{ parameters.autofocus || undefined }}\"\r\n    data-mstar-validate\r\n    data-ng-model=\"ngModel\"\r\n    class=\"ec-form__input ec-form__input--date\"\r\n    data-ng-attr-aria-label=\"{{ labels.contains('ariaText') ? labels.get('ariaText') : undefined }}\"\r\n    id=\"{{namespaceId('date')}}\" \/>\r\n
         */
        date: string;

        /**
         * @default <textarea\r\n    data-ng-attr-placeholder=\"{{ labels.contains('placeholder') ? labels.get('placeholder') : undefined }}\"\r\n    data-ng-attr-autocomplete=\"{{ autocomplete || undefined }}\"\r\n    data-ng-disabled=\"parameters.disabled\"\r\n    data-ng-attr-name=\"{{ parameters.name || undefined }}\"\r\n    data-ng-attr-cols=\"{{ parameters.cols || undefined }}\"\r\n    data-ng-attr-rows=\"{{ parameters.rows || undefined }}\"\r\n    data-ng-readonly=\"ngReadonly || parameters.readonly || undefined\"\r\n    data-ng-attr-autofocus=\"{{ parameters.autofocus || undefined }}\"\r\n    data-mstar-validate\r\n    data-ng-model=\"ngModel\"\r\n    data-ng-attr-aria-label=\"{{ :: labels.contains('ariaText') ? labels.get('ariaText') : undefined }}\"\r\n    class=\"ec-form__input ec-form__input--textarea\"\r\n    id=\"{{namespaceId('textarea')}}\"><\/textarea>\r\n
         */
        textarea: string;

        /**
         * @default <div data-ng-disabled=\"parameters.disabled\" class=\"ec-form__switch\">\r\n    <input type=\"checkbox\"\r\n           name=\"{{namespaceId('switch')}}\"\r\n           data-ng-model=\"ngModel\"\r\n           class=\"ec-form__input ec-form__input--switch\"\r\n           id=\"{{namespaceId('switch')}}\" \/>\r\n    <label class=\"ec-form__label ec-form__label--switch\" for=\"{{namespaceId('switch')}}\">\r\n        {{ labels.get(\"switchText\") }}\r\n    <\/label>\r\n<\/div>\r\n
         */
        switchToggle: string;

        /**
         * @default <div class=\"{{ ::namespaceClass('__select') }}\"\r\n    data-ng-class=\"{'{{ ::namespaceClass('--disabled') }}': parameters.disabled}\">\r\n    <select\r\n    \tdata-ng-if=\"!settings.get('keyFieldInModel')\"\r\n    \tdata-ng-model=\"$parent.ngModel\"\r\n        data-ng-disabled=\"parameters.disabled\"\r\n        data-ng-attr-aria-label=\"{{ :: labels.contains('ariaText') ? labels.get('ariaText') : undefined }}\"\r\n        class=\"{{ ::namespaceClass('__select-box') }}\"\r\n        data-ng-options=\"listItem as labels.contains(listItem[settings.get('valueField')]) ? labels.get(listItem[settings.get('valueField')]) : listItem[settings.get('valueField')] for listItem in model['listItems'] track by listItem[settings.get('keyField')]\"\r\n        id=\"{{ ::namespaceId('select') }}\">\r\n        <option value=\"\" data-ng-if=\"::labels.get('selectboxText') !== 'selectboxText'\" >{{::labels.get('selectboxText')}}<\/option>\r\n    <\/select>\r\n    <select\r\n    \tdata-ng-if=\"settings.get('keyFieldInModel')\"\r\n    \tdata-ng-model=\"$parent.ngModel\"\r\n        data-ng-disabled=\"parameters.disabled\"\r\n        data-ng-attr-aria-label=\"{{ :: labels.contains('ariaText') ? labels.get('ariaText') : undefined }}\"\r\n        class=\"{{ ::namespaceClass('__select-box') }}\"\r\n        id=\"{{ ::namespaceId('select') }}\">\r\n        <option value=\"\" data-ng-if=\"::labels.get('selectboxText') !== 'selectboxText'\" >{{::labels.get('selectboxText')}}<\/option>\r\n        <option data-ng-repeat=\"listItem in model['listItems']\" value=\"{{listItem[settings.get('keyField')]}}\">{{ labels.contains(listItem[settings.get('valueField')]) ? labels.get(listItem[settings.get('valueField')]) : listItem[settings.get('valueField')]}}<\/option>\r\n    <\/select>\r\n    <i class=\"{{ ::namespaceClass('__select-arrow') }}\"><\/i>\r\n<\/div>\r\n
         */
        select: string;

        /**
         * @default <div class=\"ec-form__checkbox\">\r\n    <label class=\"ec-form__label ec-form__label--checkbox\">\r\n        <input type=\"checkbox\"\r\n           data-ng-disabled=\"ngDisabled || parameters.disabled\"\r\n           data-ng-attr-name=\"{{ parameters.name || undefined }}\"\r\n           data-ng-model=\"ngModel\"\r\n           class=\"ec-form__input ec-form__input--checkbox\"\r\n           id=\"{{namespaceId('checkbox')}}\" \/>\r\n        <span class=\"ec-form__checkbox-text\">\r\n            {{ label || labels.get(\"checkboxText\") }}\r\n        <\/span>\r\n    <\/label>\r\n<\/div>\r\n
         */
        checkbox: string;

        /**
         * @default <fieldset data-ng-disabled=\"parameters.disabled\"\r\n          class=\"ec-form__fieldset ec-form__fieldset--checkbox\"\r\n          data-ng-class=\"{'ec-form__fieldset--horizontal': {{ ::settings.get('horizontal') }}}\" role=\"group\">\r\n    <legend data-ng-if=\"labels.contains('fieldsetLegend')\" class=\"ec-form__legend\">\r\n        <span>{{ ::labels.get('fieldsetLegend') }}<\/span>\r\n    <\/legend>\r\n    <div data-ng-repeat=\"listItem in model.listItems track by $index\" class=\"{{ ::namespaceClass('__checkbox') }}\">\r\n        <label class=\"{{ ::namespaceClass('__checkbox-labels') }}\">\r\n            <input type=\"checkbox\"\r\n               data-ng-model=\"checkboxStatus[listItem[settings.get('keyField')]]\"\r\n               data-ng-change=\"checkboxChange()\"\r\n               data-ng-true-value=\"'{{ ::listItem[settings.get('keyField')] }}'\"\r\n               data-ng-false-value=\"null\"\r\n               class=\"{{ ::namespaceClass('__checkbox-input') }}\"\r\n               name=\"{{ ::namespaceId('buttonName') }}\"\r\n               value=\"{{listItem[settings.get('keyField')]}}\" >\r\n               <span class=\"{{ ::namespaceClass('__checkbox-text') }}\">{{ labels.contains(listItem[settings.get('valueField')]) ? labels.get(listItem[settings.get('valueField')]) : listItem[settings.get('valueField')]}}<\/span>\r\n               <span class=\"{{ ::namespaceClass('__checkbox-microcopy') }}\">{{ labels.contains(listItem[settings.get('microValueField')]) ? labels.get(listItem[settings.get('microValueField')]) : listItem[settings.get('microValueField')]}}<\/span>\r\n        <\/label>\r\n    <\/div>\r\n<\/fieldset>\r\n
         */
        checkboxGroup: string;

        /**
         * @default <fieldset data-ng-disabled=\"parameters.disabled\"\r\n          class=\"ec-form__fieldset ec-form__fieldset--radio\"\r\n          data-ng-class=\"{'ec-form__fieldset--horizontal': parameters.horizontal,\r\n          '{{ ::namespaceClass('--radio-disabled') }}': parameters.disabled}\">\r\n    <legend data-ng-if=\"labels.contains('fieldsetLegend')\" class=\"ec-form__legend\">\r\n        <span>{{ ::labels.get('fieldsetLegend') }}<\/span>\r\n    <\/legend>\r\n    <div data-ng-repeat=\"listItem in model.listItems track by $index\" class=\"{{ ::namespaceClass('__radio') }}\">\r\n        <label class=\"{{ ::namespaceClass('__radio-labels') }}\">\r\n            <input type=\"radio\"\r\n               data-ng-model=\"$parent.ngModel\"\r\n               data-ng-change=\"onRadioChange()\"\r\n               class=\"{{ ::namespaceClass('__radio-button-input') }}\"\r\n               id=\"{{ ::namespaceId('-') + listItem[settings.get('keyField')] }}\"\r\n               name=\"{{ ::namespaceClass('__') + parameters.nameField }}\"\r\n               value=\"{{listItem[settings.get('keyField')]}}\" >\r\n               <span class=\"{{ ::namespaceClass('__radio-button-text') }}\">\r\n                  {{ labels.contains(listItem[settings.get('valueField')]) ? labels.get(listItem[settings.get('valueField')]) : listItem[settings.get('valueField')]}}\r\n                <\/span>\r\n        <\/label>\r\n    <\/div>\r\n<\/fieldset>\r\n
         */
        radioGroup: string;

        /**
         * @default <input\r\n    type=\"date\"\r\n    data-ng-disabled=\"parameters.disabled\"\r\n    data-ng-attr-name=\"{{ parameters.name || undefined }}\"\r\n    data-ng-attr-placeholder=\"{{ dateFormat }}\"\r\n    data-ng-readonly=\"ngReadonly || parameters.readonly || undefined\"\r\n    data-ng-attr-autofocus=\"{{ parameters.autofocus || undefined }}\"\r\n    class=\"ec-form__input ec-form__input--date\"\r\n    aria-label=\"{{ labels.contains('ariaText') ? labels.get('ariaText') : dateFormat }}\"\r\n    id=\"{{ namespaceId('date') }}\" \/>\r\n<input\r\n    type=\"hidden\"\r\n    data-ng-model=\"ngModel\"\r\n    data-mstar-validate\r\n    mstar-tooltip\r\n    id=\"actual-date\"\/>\r\n<div id=\"invalid-date\" class=\"{{ ::namespaceClass('__error-message') }}\">{{ invalidDateMessage }}<\/div>\r\n
         */
        datepicker: string;
    }

    export interface Parameters {
        inputType: any;
        name: any;
        autocomplete: any;
        autofocus: any;
        disabled: any;
        horizontal: any;
        nameField: any
        form: any;
        size: any;
        rows: any;
        cols: any;
        wrap: any;
        readonly: any;
    }

    export interface Labels {
        /** text to pass to the aria-label tag */
        ariaText: AsterixCore.Label;
        checkboxText: AsterixCore.Label;
        invalidDate: AsterixCore.Label;
        placeholder: AsterixCore.Label;
        selectboxText: AsterixCore.Label;
        switchText: AsterixCore.Label;
    }

    export interface DatepickerOptions {
        /** id of the hidden input
         * @default #actual-date
         */
        altField: string;

        /** show a dropdown for months
         * @default true
         */
        changeMonth: boolean;

        /** show a dropdown for years
         * @default true
         */
        changeYear: boolean;

        /** year span for year dropdown
         * @default 1950:+10
         */
        yearRange: string;
    }
}
