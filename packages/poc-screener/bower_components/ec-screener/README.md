**Please DO NOT contribute to this repo directly. All PR should go to the team project**

## screener

### Introduction

#### Google Analytics Integration code

For implementing google analytics we need to create hook up for the events and provide integration code for each event where the clients will write their code.

	+ The events which we have triggered from our core components are [Hook up code] :
	
		1. ecChosen Component :
			Event created is 'ecChosenItemSelected'.
		2. ecTable Component :
			Events created are - 
				'ecTableColumnSorted'
				'ecTableRowExpanded'
				'ecTableRowCollapsed'
				'ecTableRowSelected'
		3. ecInput Component :
			Event created is 'ecInputChanged'
		4. ecPaginator Component :
			Event created is 'ecPaginatorPageChanged'
		5. ecTabs Component :
			Event created is 'ecTabsSelected'
		6. ecVideo Component :
			Event created is 'ecVideoLaunched'
			
	+ The code for capturing the events [Integration code] :
	
		```javascript
		// <stx>code for GA
		var investmentFinderId = 'fidelitySelect50FundsScreener';

		function bindHandler(instancePattern, eventTopic, handler) {
			var instances = morningstar.asterix.instanceRegistry.findAll(instancePattern);
			for(var i=0,length=instances.length; i<length; i++) {
				instances[i].on(eventTopic, handler);
			}
		}

		$(document).ready(function(){
			registerAddToShortlist();
			registerChartGrowthButton();
			registerEditFilters();
			registerPaginatorClick();
			registerRemoveFromShortlist();
			registerResetFilter();
			registerSearchInvestment();
			registerSortByFieldName();
			registerTabs();
			registerUniverseSelector();
			registerViewLessFilter();
			registerViewMoreFilter();
			registerViewResults();
			triggerFilterHandler();
		});

		function registerViewLessFilter() {
			bindHandler(investmentFinderId + '.*.filtersSecurities', 'ecFiltersShowLess', viewLessFilterHandler)
		}
		function viewLessFilterHandler() {
			console.log('View less button clicked');
		}
		function registerAddToShortlist() {
			bindHandler(investmentFinderId + '.*.tableSecurities', 'ecTableRowSelected', addToShortlistHandler)
		}
		function addToShortlistHandler(value) {
			console.log(value.name + ' Added to shortlist');
		}
		function registerChartGrowthButton() {
			bindHandler(investmentFinderId + '.*.chartButton', 'click', chartGrowthButtonHandler)
		}
		function chartGrowthButtonHandler() {
			console.log('Chart growth of £ 1000 button clicked.');
		}
		function registerEditFilters() {
			bindHandler(investmentFinderId + '.*.editFilterButton', 'click', editFiltersHandler)
		}
		function editFiltersHandler() {
			console.log('Edit filters button clicked.');
		}
		function registerPaginatorClick() {
			bindHandler(investmentFinderId + '.*.tableSecurities.paginator', 'ecPaginatorPageChanged', paginatorClickHandler)
		}
		function paginatorClickHandler() {
			console.log('Show more result triggered.');
		}
		function registerRemoveFromShortlist() {
			bindHandler(investmentFinderId + '.*.shortListedSecurities', 'ecTableRowSelected', removeFromShortlistHandler)
		}
		function removeFromShortlistHandler(value) {
			console.log(value.name + ' Removed from shortlist');
		}
		function registerResetFilter() {
			bindHandler(investmentFinderId + '.*.buttonReset', 'click', resetFilterHandler)
		}
		function resetFilterHandler() {
			console.log('Reset button clicked');
		}
		function registerSearchInvestment() {
			bindHandler(investmentFinderId + '.*.searchTerm', 'ecInputChanged', searchInvestmentHandler)
		}
		function searchInvestmentHandler() {
			console.log('Search for an investment triggered.');
		}
		function registerSortByFieldName() {
			bindHandler(investmentFinderId + '.*.tableSecurities', 'ecTableColumnSorted', sortByFieldNameHandler)
		}
		function sortByFieldNameHandler(columnName) {
			console.log(columnName + ' Column sorted.');
		}
		function registerTabs() {
			bindHandler(investmentFinderId + '.*.viewTabs', 'ecTabsSelected', TabsHandler)
		}
		function TabsHandler(tabName) {
			console.log(tabName + 'Tab clicked');
		}
		function registerUniverseSelector() {
			morningstar.asterix.instanceRegistry.find(investmentFinderId +'.universeSelector').on('ecChosenItemSelected', investmentTypeChangeHandler)
		}
		function investmentTypeChangeHandler(investmentTypeUniverse) {
			var listItems = this.getModel().listItems, investmentType;
			for(var i=0,length=listItems.length; i<length; i++) {
				if(listItems[i].key === investmentTypeUniverse) {
					investmentType = listItems[i].value;
					break;
				}
			}
			console.log('Investment type selected: ' + investmentType);
		}
		function registerViewMoreFilter() {
			bindHandler(investmentFinderId + '.*.filtersSecurities', 'ecFiltersShowMore', viewMoreFilterHandler)
		}
		function viewMoreFilterHandler() {
			console.log('View more button clicked');
		}
		function registerViewResults() {
			bindHandler(investmentFinderId + '.*.buttonViewResult', 'click', viewResultsHandler)
		}
		function viewResultsHandler() {
			console.log('View results button clicked.');
		}

		function triggerFilterHandler() {
			var instances = morningstar.asterix.instanceRegistry.findAll(investmentFinderId + '.*.filtersSecurities.*');
			for (var i = 0; i < instances.length; i++) {
				if (instances[i].getModuleType() === 'ecChosen') {
					instances[i].on('ecChosenItemSelected', function (value) {
						console.log('Filter selection changed', this.getOwnInstanceId(), value);
					});
				}
			}
		}
		//<etx>code for GA*/
