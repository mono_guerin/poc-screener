// startref
/// <reference path='../../.references.ts'/>
// endref
module EcScreener {
    export enum _Type { ecScreener }

    /**
     * @title Screener
     */
    export interface EcScreener {
        /** Component type.
         * @default ecScreener
         * @options.hidden true
         * @type string
         * @readonly true
         */
        type: _Type;

        settings: Settings;

        labels: Labels;

        components: {
            actionBar: EcActionContainer.EcActionContainer;
            chosenPageSize: EcChosen.EcChosen;
            compareChart: EcSecurityLineChart.EcSecurityLineChart;
            criteriaSelectorContainer: EcContainer.EcContainer;
            filtersSecurities: EcFilters.EcFilters;
            resultsViewContainer: EcContainer.EcContainer;
            mainViewContainer: EcContainer.EcContainer;
            saveDialog: EcSaveDialog.EcSaveDialog;
            shortListedSecurities: EcTable.EcTable;
            tableSecurities: EcTable.EcTable;
            viewListActionBar: EcActionContainer.EcActionContainer;
            viewListContainer: EcContainer.EcContainer;
            viewListSecurities: EcTable.EcTable;
            viewTabs: EcTabs.EcTabs;
        }
    }
}
