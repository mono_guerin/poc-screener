// startref
/// <reference path='../../.references.ts'/>
// endref
module EcScreener {
    export interface Settings extends AsterixCore.ComponentSettings {
        _templates: Templates;
        _parameters: {
            activeView: AsterixNg.Parameter;

            activeViewIndex: AsterixNg.Parameter;

            allowSecurityDelete: AsterixNg.Parameter;

            applyTrackRecordExtension: AsterixNg.Parameter;

            baseUrlApiGatewayEU: AsterixNg.Parameter;

            baseUrlMorningstarUS: AsterixNg.Parameter;

            baseUrlMorningstarEU: AsterixNg.Parameter;

            compareTickers: AsterixNg.Parameter;

            currencyId: AsterixNg.Parameter;

            filtersSelectedValue: {
                url: AsterixNg.ParameterUrl;
            }

            filterToApiParams: AsterixNg.Parameter;

            fixFirstRowSecId: {
                url: AsterixNg.ParameterUrl;
            }

            /** To trigger api to get all saved lists
             */
            allowFetchingSavedLists: AsterixNg.Parameter;

            languageId: AsterixNg.Parameter;

            noItemSelected: any;

            noItemSelectedViewList: any;

            maxShortlistLength: AsterixNg.Parameter;

            ticker: AsterixNg.Parameter;

            page: {
                url: AsterixNg.ParameterUrl;
            }

            pageForViewList: {
                url: AsterixNg.ParameterUrl;
            }

            perPage: {
                url: AsterixNg.ParameterUrl;
            }

            perPageForViewList: {
                url: AsterixNg.ParameterUrl;
            }

            queries: AsterixNg.Parameter;

            selectedItems: AsterixNg.Parameter;

            selectedItemKeys: AsterixNg.Parameter;

            selectedItemsViewList: AsterixNg.Parameter

            selectedItemKeysViewList: AsterixNg.Parameter;

            /**
             * Selected list from saved lists dropdown
             */
            selectedList: AsterixNg.Parameter;

            shortListedItems: any;

            shortListedItemKeys: AsterixNg.Parameter;

            screenerStorage: AsterixNg.Parameter;

            showAdvanceFilters:AsterixNg.Parameter;

            showActionBar: any;

            showComparisonChart: AsterixNg.Parameter;

            showDataTable: AsterixNg.Parameter;

            showFilters: any;

            showFiltersToggle: AsterixNg.Parameter;

            showOnlySelect50Funds: AsterixNg.Parameter;

            showSaveDialog: AsterixNg.Parameter;

            sortField: {
                url: AsterixNg.ParameterUrl;
            }

            sortFieldForViewList: {
                url: AsterixNg.ParameterUrl;
            }

            sortOrder: {
                url: AsterixNg.ParameterUrl;
            }

            sortOrderForViewList: {
                url: AsterixNg.ParameterUrl;
            }

            step: any;

            subUniverseId: {
                url: AsterixNg.ParameterUrl;
            };

            universeId: {
                url: AsterixNg.ParameterUrl;
            };

            urlKey: AsterixNg.Parameter;

            views: AsterixNg.Parameter;

            viewListItems: AsterixNg.Parameter;

            viewListKeys: AsterixNg.Parameter;
        };

        _model: {
            /**
             * boolean for checking the back of view list
             */
            goBackToViewList: boolean;
            securities: AsterixCore.DataSourceSettings;
            filterMinMax: AsterixCore.DataSourceSettings;
            selectedItemCountForExport: number;
        }

        /* set initial filter value*/
        filtersSelectedValue: any;

        /** Boolean to allow delete security list api call of view list
         * @default false
         *
         */
        allowSecurityDelete: boolean;

        /** Boolean to toggle TRE
         * @default false
         *
         */
        applyTrackRecordExtension: boolean;

        /** Mapping object for assetType's data fields
         *
         */
        assetTypeFields: any;

        /**
         * @default ec-screener
         */
        namespaceClass: string;

        /**
         * @default 0
         */
        activeViewIndex: number;

        /** Api data point mapping with screener
         *
         */
        dataPoints: any;

        /**
         * @default {{secId}}]{{holdingTypeId}}]0]{{universe}}
         */
        defaultKeyField: string;

        /** list of field names to use mapping while export - key for label should match this {fieldName_fieldValue} e.g. analystRatingScale_1
         * @default []
         */
        exportMappingFieldList: string[];

        /**
         * @default 100
         */
        exportRowsLimit: number;

        /**
         * @default 1
         */
        exportPage: number;

        /**
        * @default ["toIwtScreener", "fromIwtScreenerExport"]
        **/
        exportTransformStack: string[];

        /**
         * Mapping of api parameters and filter parameters
         */
        filterToApiParams: any;

        /** Mapping of api parameters fixfirstrowto
         * @default
         */
        fixFirstRowSecId: string;

        /** To trigger api to get all saved lists
         */
        allowFetchingSavedLists: boolean;

        /**
         * It store all the mappings used to transform data, for example: exchange names
         */
        mapping: Mapping;

        /**
         * @default 7
         */
        maxShortlistLength: number;

        /**
         * @default 1
         */
        page: number;

        /**
         * @default 20
         */
        perPage: string;

        /** Filter queries
         */
        queries: FilterQuery[];

        /** Mapping object for holdingTypeId's
         *
         */
        holdingTypeIdMap: any;

        /** This is the type of screener possible values funds/equities/exchangeTradedFunds/investmentTrust/bondsGilts
         * @type string
         */
        investmentType:string;

        /**
         * Defines the type of storage need to use in screener
         * @type screenerStorageType
         * @default 1
         */
        screenerStorage: screenerStorageType

        /** Define the separator to split secIds, tickers, isins
         * @default ,
         */
        securityIdFieldsSeparator: string;

        /** settings & parameter to store the selectedItems to perform various actions on screener.*/
        selectedItems: any;

        /**
         * Selected list from saved lists dropdown
         */
        selectedList: any;

        /**
         * @default false
         */
        showActionBar: boolean;

        /**
         * @default false
         */
        showDataTable: boolean;

        /**
         * @default true
         */
        showFilters: boolean;

        /**
         * @default false
         */
        showFiltersToggle: boolean;

        /**
        * @default false
        */
        showComparisonChart: boolean;

        /**
         * @default true
         */
        showAdvanceFilters: boolean;

        /**
         * @default false
         */
        showOnlySelect50Funds: boolean;

        /** to hide/show save-dialog
         * @default false
         */
        showSaveDialog: boolean;

        /**
         * @default name
         */
        sortField: string;

        /**
         * @default asc
         */
        sortOrder: string;

        /** String for sub universeId
         */
        subUniverseId: string

        ticker: string;

        /** String of universeId
         */
        universeId: string;

        /** this is solution key for IWT
         * @default 9k2sbrqvva
         */
        urlKey: string;

        /** Usage tracking Hit objects
         */
        usageTrackingHitMap: any

        /** enables timeSeriesApi datasource to be used within charts else, tenforeId will be used
         * @type boolean
         * @default true
         */
        useTimeSeriesAPI: boolean;

        views: View[];

        /** Definition for views
        */
        viewDefinitions: any;
        /** this property stores security keys
         * @default []
         */
        viewListKeys: string[];
    }


    export enum screenerStorageType {
        flashStorage = 0,
        sessionStorage = 1,
        localStorage = 2,
        apiStorage = 3
    }
    export interface View {
        fieldNames: string[];
        keyField: string;
        labelKey: string;
        stickyLeadBreakPoints: EcTable.StickyLeadBreakPoint[];
    }

    export interface Mapping {
        /** Mapping for exchange names  */
        exchangeNames: any;
    }

    export interface Templates {
        /** The main template.
         * @format html
         * @default <div mstar-loader=\"model.securityDetails._$isLoading\">\r\n<div class=\"ec-screener__window\" data-ng-class=\"windowToggleClass\">\r\n    <div class=\"ec-screener__wrapper\">\r\n        <div class=\"ec-screener__criteria\">\r\n            <header>\r\n                <h2>{{labels.get('criteriaHeader')}}<\/h2>\r\n                <a data-ng-if=\"parameters.showFiltersToggle\" class=\"ec-screener__toggle-filters\" href=\"javascript:;\" data-ng-click=\"handleShowFiltersToggle()\">{{ labels.get(parameters.showFilters ? 'hideFilters' : 'showFilters') }}<\/a>\r\n            <\/header>\r\n            <ec-filters data-ng-show=\"parameters.showFilters\" data-ng-form=\"filtersForm\" mstar-component-id=\"filtersSecurities\"><\/ec-filters>\r\n\r\n\r\n        <\/div>\r\n\r\n        <div class=\"ec-screener__results\">\r\n            <div class=\"results-found\">\r\n                {{model.securities.total}} results\r\n                <span data-ng-show=\"parameters.selectedItemKeys.length > 0\">\r\n                ({{parameters.selectedItemKeys.length}} selected)\r\n                <\/span>\r\n            <\/div>\r\n            <ec-tabs mstar-component-id=\"viewTabs\">\r\n                <ec-tab-action data-ng-repeat=\"viewId in parameters.views\"\r\n                               name=\"{{labels.get(getViewById(viewId).labelKey)}}\"\r\n                               on-active=\"setActiveView(index, tab)\">\r\n                <\/ec-tab-action>\r\n                <ec-action-container mstar-component-id=\"actionBar\" data-ng-if=\"parameters.showActionBar\"><\/ec-action-container>\r\n                <ec-table mstar-component-id=\"tableSecurities\"><\/ec-table>\r\n            <\/ec-tabs>\r\n        <\/div>\r\n    <\/div>\r\n<\/div>\r\n<\/div>\r\n
         */
        main: string;
    }

    /**Store a list of pre-defined filter queries */
    export interface FilterQuery {
        filterKey: string;
        queryItems: FilterQueryItem[];
    }

    export interface FilterQueryItem {
        filters: FilterQueryValue[];
        key: string;
        universeIds: string[];
    }

    export interface FilterQueryValue {
        fieldName: string;
        operator: string;
        values: string[];
    }
}
