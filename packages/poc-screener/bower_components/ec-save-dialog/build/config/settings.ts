// startref
/// <reference path='../../.references.ts'/>
// endref
module EcSaveDialog {
    export interface Settings extends AsterixNg.ComponentSettings {

        _model: {
            disableNewList: boolean;
            disableUpdateList: boolean;
            saveMode: string;
        }

        _parameters: {
            /**
             * baseURl for API gateway
             */
            baseUrlApiGatewayEU: AsterixNg.Parameter;

            /**
             * Data to be saved. Used to bind with other components
             */
            dataToSave: AsterixNg.Parameter;

            /**
             * To know current state of dialog (open or close)
             */
            modalStateOpen: AsterixNg.Parameter;

            /**
             * Name of new list
             */
            newItemName: AsterixNg.Parameter;

            /**
             * Used to call save api on click of save button
             */
            saveItem: AsterixNg.Parameter;

            /**
             * Selected item object for update
             */
            selectedItem: AsterixNg.Parameter;

            /**
             * Used to call save api on click of save button to update existing item
             */
            updateExistingItem: AsterixNg.Parameter;
        }

        _templates: Templates;

        /**
         * Data to be saved.
         */
        dataToSave: any;

        /** current state of dialog (open or close)
         * @default true
         */
        modalStateOpen: boolean;

        /** The namespaceClass
         * @default ec-save-dialog
         * @type string
         */
        namespaceClass: string;

        /** Name of new item to be saved.
         */
        newItemName?: string;

        /**Used to call save api on click of save button
         * @default true
         */
        saveItem: boolean;

        /**
         * Selected item object for update
         */
        selectedItem: any;

        /**
         * Used to call save api on click of save button to update existing item
         @default true
         */
        updateExistingItem: boolean;
    }

    export interface Templates {

        /** The main template.
         * @format html
         * @default <ec-section data-template-type=\"light\" class=\"{{ ::namespaceClass('__dialog-box') }}\">\r\n    <ec-container mstar-component-id=\"containerMainView\" class=\"{{ ::namespaceClass('__modal-container') }}\">\r\n        <ec-section data-template-type=\"light\" class=\"{{ ::namespaceClass('__modal-section') }}\" section-id=\"sectionActionButtons\">\r\n            <div class=\"{{ ::namespaceClass('__modal-header') }}\">\r\n                <div class=\"{{ ::namespaceClass('__modal-header-label') }}\">\r\n                    <span>{{ ::labels.get('addItem') }}<\/span>\r\n                <\/div>\r\n                <div class=\"{{ ::namespaceClass('__modal-header-action') }}\">\r\n                    <ec-button mstar-component-id=\"cancelButton\" data-ng-click=\"closeModal()\"><\/ec-button>\r\n                    <ec-button mstar-component-id=\"saveButton\"\r\n                               data-ng-click=\"saveData(closeModal)\"\r\n                               class=\"{{ ::namespaceClass('__modal-header-action--save') }}\">\r\n                    <\/ec-button>\r\n                <\/div>\r\n            <\/div>\r\n        <\/ec-section>\r\n        <ec-section data-template-type=\"light\" class=\"{{ ::namespaceClass('__modal-section') }}\" section-id=\"sectionListName\">\r\n            <div class=\"{{ ::namespaceClass('__modal-dialog-boxes')}}\">\r\n                <ec-input mstar-component-id=\"newListRadio\" data-ng-model=\"model.saveMode\" on-radio-change=\"setSaveMode()\"><\/ec-input>\r\n                <ec-input mstar-component-id=\"newList\" data-ng-model=\"parameters.newItemName\"\r\n                          class=\"{{ ::namespaceClass('__modal-input-text') }}\">\r\n                <\/ec-input>\r\n                <div data-ng-if=\"model.saveMode === 'newList'\" class=\"{{::namespaceClass('__error-message')}}\">\r\n                    {{validationErrors.itemNameIsRequired}}\r\n                <\/div>\r\n            <\/div>\r\n            <div class=\"{{ ::namespaceClass('__modal-dialog-boxes') }}\">\r\n                <ec-input mstar-component-id=\"updateListRadio\" data-ng-model=\"model.saveMode\" on-radio-change=\"setSaveMode()\"><\/ec-input>\r\n                <ec-input mstar-component-id=\"updateList\" data-ng-model=\"parameters.selectedItem\"\r\n                          class=\"{{ ::namespaceClass('__modal-input-select') }}\"\r\n                          data-ng-class=\"{ '{{ ::namespaceClass('__error-select') }}' : model.saveMode === 'updatedList' && validationErrors.itemSelectionIsRequired }\">\r\n                <\/ec-input>\r\n            <\/div>\r\n        <\/ec-section>\r\n    <\/ec-container>\r\n<\/ec-section>\r\n\r\n<div class=\"{{ ::namespaceClass('__mask') }}\" data-ng-click=\"closeModal()\"><\/div>\r\n
         */
        main: string;
    }
}
