// startref
/// <reference path='../../.references.ts'/>
// endref
module EcSaveDialog {
    export enum _Type { ecSaveDialog }

    /**
     * @title SaveDialog
     */
    export interface EcSaveDialog {
        /** Component type.
         * @default ecSaveDialog
         * @options.hidden true
         * @type string
         * @readonly true
         */
        type: _Type;

        settings: Settings;

        labels: Labels;

        components: {
            cancelButton: EcButton.EcButton;
            containerMainView: EcContainer.EcContainer;
            newList: EcInput.EcInput;
            saveButton: EcButton.EcButton;
            newListRadio: EcInput.EcInput;
            updateListRadio: EcInput.EcInput;
            updateList: EcInput.EcInput;
        };
    }
}
