// startref
/// <reference path='../../.references.ts'/>
// endref
module EcSaveDialog {
    export interface Labels {
        addItem: AsterixCore.Label;
        itemNameIsRequired: AsterixCore.Label;
        itemSelectionIsRequired: AsterixCore.Label;
        savedToDBAlert: AsterixCore.Label;
        updatedExistingItemAlert: AsterixCore.Label;
    }
}
