// startref
/// <reference path='../../.references.ts'/>
// endref
module EcActionContainer {
    export interface Labels {
        alertXraySelectLimitExceeded: AsterixCore.Label;
        alertXraySelectItems: AsterixCore.Label;
        moreActionsHeader: AsterixCore.Label;
    }
}

