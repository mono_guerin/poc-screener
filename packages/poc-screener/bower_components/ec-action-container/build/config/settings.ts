// startref
/// <reference path='../../.references.ts'/>
// endref
module EcActionContainer {

    export interface Settings extends AsterixCore.ComponentSettings {
        _parameters: {
            actions: AsterixNg.Parameter;
            moreActions: AsterixNg.Parameter;
            selectedItemKeys: AsterixNg.Parameter;
            showMenu: AsterixNg.Parameter;
            visibleActions: AsterixNg.Parameter;
        };

        _templates: Templates;

        actions: any;

        /** Actions present inside more actions list. Similar parameter exist, hence created a setting for consistency.
         * It is generated using the primaryActionsCount setting.
         */
        moreActions: any;

        /** Namespace for css/scss classes
         * @default ec-action-container
         */
        namespaceClass: string;

        /** To set the number of visible actions on action bar out of total actions present in action-container.
         *  Rest of the actions other than visible ones will be present in more actions list.
         */
        primaryActionsCount: CountForBreakpoints;

        selectedItemKeys: string[];

        /** to show/hide more-actions popover
         * @default false
         * @type boolean
         */
        showMenu: boolean

        /** to show More-actions header
         * @default false
         * @type boolean
         */
        showMoreActionsHeader: boolean;

        /** Actions which are visible on the action bar. Similar parameter exist, hence created a setting for consistency.
         * It is generated using the primaryActionsCount setting.
         */
        visibleActions: any;

        xray: any;
    }

    export interface Templates {
        /** The main template.
         * @format html
         * @default <ec-section data-template-type=\"light\">\r\n    <ul class=\"{{ ::namespaceClass('__list') }}\">\r\n        <li data-ng-repeat=\"action in parameters.visibleActions\" class=\"{{ ::namespaceClass('__list-item') }}\">\r\n            <ec-button mstar-component-id=\"{{ ::action.instanceId }}\" label=\"{{ ::action.label || undefined }}\" data-button-data=\"model\"><\/ec-button>\r\n        <\/li>\r\n        <li data-ng-if=\"parameters.moreActions.length\" class=\"{{ ::namespaceClass('__list-item--more') }}\">\r\n            <ec-button mstar-component-id=\"showMoreActionsButton\"><\/ec-button>\r\n            <nav data-ng-if=\"parameters.showMenu\" class=\"{{ ::namespaceClass('__more-actions') }}\">\r\n                <div data-ng-if=\"settings.get('showMoreActionsHeader')\" class=\"{{ ::namespaceClass('__more-actions-header') }}\">\r\n                    {{ ::labels.get('moreActionsHeader') }}\r\n                <\/div>\r\n                <ul class=\"{{ ::namespaceClass('__more-actions-list') }}\">\r\n                    <li data-ng-repeat=\"action in parameters.moreActions\" class=\"{{ ::namespaceClass('__more-actions-list-item') }}\">\r\n                        <ec-button mstar-component-id=\"{{ ::action.instanceId }}\" label=\"{{ ::action.label || undefined }}\" data-button-data=\"model\"><\/ec-button>\r\n                    <\/li>\r\n                <\/ul>\r\n            <\/nav>\r\n        <\/li>\r\n    <\/ul>\r\n<\/ec-section>\r\n
         */
        main: string;
    }

    export interface Action {
        /** The action type
         * @default custom
         * @type string
         */
        actionType: ActionType;
        instanceId: string;
        settings: EcButton.Settings;
    }

    export interface CountForBreakpoints {
        defaults: number;
        large: number;
        medium: number;
        small: number;
        xlarge: number;
    }

    export enum ActionType {
        chart,
        custom,
        deselectAll,
        facebook,
        twitter,
        xray
    }
}
