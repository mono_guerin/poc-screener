// startref
/// <reference path='../../.references.ts'/>
// endref

module EcActionContainer {
    export enum _Type { ecActionContainer }

    /**
     * @title ActionContainer
     */
    export interface EcActionContainer {
        /** Component type.
         * @default ecActionContainer
         * @options.hidden true
         * @type string
         * @readonly true
         */
        type: _Type;

        settings: Settings;

        components: {
            actionDeselectAll: EcButton.EcButton;
            actionFacebook: EcButton.EcButton;
            actionTwitter: EcButton.EcButton;
            actionXray: EcButton.EcButton;
            showMoreActionsButton: EcButton.EcButton;
        }
    }
}
