# ActionContainer

## Introduction
ec-actionContainer is a component which can render one to many ec-buttons. It takes actions array as input configuration to render actions/ec-buttons

### Usage

ecActionContainer can be used as an Angular directive 

#### Example:
	<ec-action-container mstar-component-id="ecActionContainer"></ec-action-container>

###Configurable Options for ecbuttons

 1. actionType: can be standard action or custom action
 2. instanceId:  instance id of ec-button
 3. settings: settings for ec-button, inherited from ecButton.Settings

###How to run
Assuming you have setup ec correctly. You can run the ecActionContainer as follows:

	gulp previewComponent -c actionContainer

###How To configure
Add configuration of defined ecActionContainer in components section of js/_config.js. 

	morningstar.appConfig = morningstar.asterix.extend({
	    components: {
	      ecActionContainer: {
                type: "ecActionContainer",
                settings: {
                    actions: [{
                            actionType: "custom",
                            instanceId: "actionXRay"
                        }]
                },
                components: {
                    actionXRay: {
                        type: 'ecButton',
                        settings: {
                            buttonType: "button",
                            buttonCSS: "custombutton",
                            onClick: function(){}
                        },
                        labels: {
                            "caption": {
                                "en": "X-Ray"
                            }
                        }
                    }
                }
	         }
	      }
	   })
