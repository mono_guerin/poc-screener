// startref
/// <reference path='../../.references.ts'/>
// endref
module EcChosen {
    export interface Settings extends AsterixNg.ComponentSettings {

        _model: {
            listItems: AsterixCore.DataSourceSettings;
        }

        _parameters: {
            accordionQueries: AccordionFilterItem;
            allowSingleDeselect: any;
            controlClass: AsterixNg.Parameter;
            controlType: AsterixNg.Parameter;
            disabled: any;
            disableSearch: any;
            disableSearchThreshold: any;
            displayDisabledOptions: any;
            displaySelectedOptions: any;
            enableSplitWordSearch: any;
            horizontal: AsterixNg.Parameter;
            inheritSelectClasses: any;
            maxSelectedOptions: any;
            multiple: any;
            imageOptionsClass: AsterixNg.Parameter;
            imageOptionsClassActive: AsterixNg.Parameter;
            imageOptionsClassInactive: AsterixNg.Parameter;
            searchContains: any;
            singleBackstrokeDelete: any;
            width: any;
        };

        _templates: Templates;

        _validation: AsterixCore.Validation;

        accordionQueries: AccordionFilterItem;

        /**
         * @default true
         */
        allowSingleDeselect: boolean;

        /**
         * @default false
         */
        allowSelectableAccordionTitle: boolean;

        controlClass: string;

        /**
         * @default select
         */
        controlType: string;

        customItemOptions: CustomItemOption[];

        /**
         * @default false
         */
        disabled?: boolean;

        /**
         * @default disabled
         */
        disabledField: string;

        /**
         * @default true
         */
        displayFirstEmptyOption: boolean;

        /**
         * @default false
         */
        disableSearch: boolean;

        disableSearchThreshold: number;
        displayDisabledOptions: boolean;
        displaySelectedOptions: boolean;
        enableSplitWordSearch: boolean;

        /**
         * @default group-class
         */
        groupClass: string;

        /**
         * Enable/disable horizontal display for radio or check box list
         */
        horizontal: boolean;

        imageOptions: CriteriaImage[];
        imageOptionsClass: string;

        /**
         * @default active
         */
        imageOptionsClassActive: string;

        /**
         * @default inactive
         */
        imageOptionsClassInactive: string;

        inheritSelectClasses: boolean;

        /**
         * @default item-class
         */
        itemClass: string;

        /**
         * @default key
         */
        keyField: string;

        /**
         * @default true
         */
        loaderEnabled: boolean;

        maxSelectedOptions: number;

        /**
         * @default false
         */
        multiple: boolean;

        /**
         * @default ec-chosen
         */
        namespaceClass: string;

         /** runtimeSelectLabel,  load labels statically or dynamically
         * @default true
         */
        runtimeSelectLabel:boolean;

        searchContains: boolean;
        singleBackstrokeDelete: boolean;

        /**
         * @default value
         */
        valueField: string;

        /**
         * Like ValueField, it is intended to supplement label text like a microcopy.
         * Default value of microValueField is set as value. Currently supported for Checkbox template only.
         */
        microValueField: string;

        width: number;
    }

    export interface Templates {
        /** The main template.
         * @format html
         * @default <div mstar-include=\"templates[chosenType]\"><\/div>\r\n
         */
        main: string;

        /**
         * @default <div data-ng-if=\"::settings.get('controlType') === 'accordionCheckboxList'\" data-ng-disabled=\"parameters.disabled\" class=\"ec-form__fieldset--accordion-checkbox\">\r\n    <div class=\"ec-accordion__container\">\r\n        <div class=\"ec-accordion__tab\" data-ng-repeat=\"listItem in model.listItems track by $index\">\r\n            <div class=\"ec-accordion__tab-title\" name=\"{{ ::listItem.parentKey }}\" data-ng-click=\"openTab(tab-$index)\">\r\n                <input class=\"ec-accordion__parent\" data-ng-if=\" ::settings.get('allowSelectableAccordionTitle') \" type=\"checkbox\"\r\n                       data-ng-model=\"listItem.selectAll\"\r\n                       data-ng-click=\"toggleChildItems($index,$event)\"\r\n                       name=\"{{ ::namespaceId('checkbox') }}\"\r\n                       value=\"{{ ::listItem.parentKey }}\">\r\n                {{ ::(!labels.contains(listItem.parentValue) ? listItem.parentValue : labels.get(listItem.parentValue)) }}\r\n            <\/div>\r\n            <div class=\"ec-accordion__tab-content\" data-ng-class=\"isOpenTab(tab-$index) ? 'open-collapse-section' : 'close-collapse-section'\" id=\"collapse-{{ ::namespaceId(listItem.parentKey) }}-{{ ::$index }}\">\r\n                <div data-ng-repeat=\"childItem in listItem.childListItems track by $index\">\r\n                    <label class=\"ec-form__label ec-form__label--checkbox\">\r\n                        <input type=\"checkbox\" name=\"{{ ::namespaceId('checkbox') }}\"\r\n                            value=\"{{ ::childItem[settings.get('keyField')] }}\"\r\n                            data-ng-model=\"childItem.selected\"\r\n                            data-ng-change=\"addCheckedItem(childItem, listItem.parentKey)\"\r\n                            class=\"ec-form__input ec-form__input--checkbox\" \/>\r\n                        {{ ::(settings.get('runtimeSelectLabel') && labels.contains(childItem[settings.get('valueField')]) ? labels.get(childItem[settings.get('valueField')]) : childItem[settings.get('valueField')]) }}\r\n                    <\/label>\r\n                <\/div>\r\n            <\/div>\r\n        <\/div>\r\n    <\/div>\r\n<\/div>\r\n
         */
        accordionCheckboxList: string;

        /**
         * @default <div data-ng-if=\"::(settings.get('controlType') === 'checkbox' && !settings.get('imageOptions'))\" data-ng-disabled=\"parameters.disabled\" class=\"ec-form__fieldset ec-form__fieldset--checkbox\">\r\n    <div data-ng-repeat=\"listItem in model.listItems\" class=\"ec-form__checkbox\" mstar-drag=\"{{ listItem.rearrangeEnabled ? namespaceId() : ''}}\" mstar-drop=\"{{ listItem.rearrangeEnabled ? namespaceId() : ''}}\">\r\n        <label class=\"ec-form__label ec-form__label--checkbox {{ listItem[settings.get('disabledField')] ? 'ec-form__checkbox--disabled' : ''}} \">\r\n            <input type=\"checkbox\" name=\"{{ ::namespaceId('checkbox') }}\"\r\n                   value=\"{{ ::listItem[settings.get('keyField')] }}\"\r\n                   data-ng-model=\"checkboxStatus[listItem[settings.get('keyField')]]\"\r\n                   data-ng-change=\"checkboxChange()\"\r\n                   data-ng-true-value=\"'{{ ::listItem[settings.get('keyField')] }}'\"\r\n                   data-ng-false-value=\"null\"\r\n                   data-ng-disabled=\"listItem[settings.get('disabledField')]\"\r\n                   class=\"ec-form__input ec-form__input--checkbox\" \/>\r\n            <span class=\"ec-form__checkbox-text\">\r\n                {{ ::(settings.get('runtimeSelectLabel') && labels.contains(listItem[settings.get('valueField')])) ? labels.get(listItem[settings.get('valueField')]) : listItem[settings.get('valueField')] }}\r\n            <\/span>\r\n            <span class=\"ec-form__checkbox-microcopy\" data-ng-if=\"listItem[settings.get('microValueField')]\">{{ labels.contains(listItem[settings.get('microValueField')]) ? labels.get(listItem[settings.get('microValueField')]) : listItem[settings.get('microValueField')]}}<\/span>\r\n            <span class=\"ec-form__checkbox-tooltip\" data-ng-if=\"::listItem.tooltipKey\" mstar-tooltip-content=\"{{ ::labels.get(listItem.tooltipKey) }}\" mstar-tooltip>\r\n            <\/span>\r\n        <\/label>\r\n    <\/div>\r\n<\/div>
         */
        checkbox: string;

        /**
         * @default <div data-ng-if=\"::settings.get('imageOptions')\" data-ng-disabled=\"parameters.disabled\" class=\"ec-form__fieldset ec-form__fieldset--image\">\r\n    <div data-ng-repeat=\"image in images track by $index\"\r\n        data-ng-class=\"[image.currentClasses]\"\r\n        data-ng-click=\"imageClicked(image.key)\"\r\n        aria-pressed=\"{{ image.selected }}\"\r\n        class=\"{{ ::namespaceClass('__' + settings.get('imageOptionsClass'))}}\">\r\n    <\/div>\r\n<\/div>\r\n
         */
        checkboxImage: string;

        /**
         * @default <div data-ng-if=\"::settings.get('imageOptions')\" data-ng-disabled=\"parameters.disabled\" class=\"ec-form__fieldset ec-form__fieldset--image\">\r\n    <div data-ng-repeat=\"image in images track by $index\"\r\n        data-ng-class=\"[image.currentClasses]\"\r\n        data-ng-click=\"imageClicked(image.key)\"\r\n        aria-pressed=\"{{ image.selected }}\"\r\n        class=\"{{ ::namespaceClass('__' + settings.get('imageOptionsClass'))}}\">\r\n    <\/div>\r\n<\/div>\r\n
         */
        minimumRating: string;

        /**
         * @default <fieldset data-ng-if=\"::(settings.get('controlType') === 'radio' && !settings.get('imageOptions'))\" data-ng-disabled=\"parameters.disabled\" class=\"ec-form__fieldset ec-form__fieldset--radio{{ ::settings.get('horizontal') ? ' ec-form__fieldset--horizontal' : ''  }}\">\r\n    <legend data-ng-if=\"::labels.contains('fieldsetLegend')\" class=\"ec-form__legend\">\r\n        <span>{{ ::labels.get('fieldsetLegend') }}<\/span>\r\n    <\/legend>\r\n    <div data-ng-repeat=\"listItem in model.listItems track by $index\" class=\"ec-form__radio\">\r\n        <label class=\"ec-form__label ec-form__label--radio\">\r\n            <input type=\"radio\" name=\"{{namespaceId('radio')}}\"\r\n                   value=\"{{ ::listItem[settings.get('keyField')] }}\"\r\n                   data-ng-model=\"radio.selected\"\r\n                   data-ng-change=\"radioChange()\"\r\n                   class=\"ec-form__input ec-form__input--radio\" \/>\r\n            <span class=\"ec-form__radio-text\">\r\n                {{ ::(settings.get('runtimeSelectLabel') && labels.contains(listItem[settings.get('valueField')]) ? labels.get(listItem[settings.get('valueField')]) : listItem[settings.get('valueField')]) }}\r\n            <\/span>\r\n        <\/label>\r\n    <\/div>\r\n<\/fieldset>\r\n
         */
        radio: string;

        /**
         * @default <div data-ng-if=\"::settings.get('imageOptions')\" data-ng-disabled=\"parameters.disabled\" class=\"ec-form__fieldset ec-form__fieldset--image\">\r\n    <div data-ng-repeat=\"image in images track by $index\"\r\n        data-ng-class=\"[image.currentClasses]\"\r\n        data-ng-click=\"imageClicked(image.key)\"\r\n        aria-pressed=\"{{ image.selected }}\"\r\n        class=\"{{ ::namespaceClass('__' + settings.get('imageOptionsClass'))}}\">\r\n    <\/div>\r\n<\/div>\r\n
         */
        radioImage: string;

        /**
         * @default <div data-ng-if=\"::(settings.get('controlType') === 'select' && !settings.get('multiple'))\" class=\"{{ ::namespaceClass('__select') + ' ' + namespaceClass('__select--single') }}\">\r\n    <select data-ng-model=\"$parent.ngModel\"\r\n            name=\"todo\" mstar-validate\r\n            data-ng-disabled=\"parameters.disabled\" id=\"{{ ::namespaceId('select') }}\"\r\n            class=\"ec-form__select\">\r\n        <option value=\"\" data-ng-if=\"::!settings.get('multiple') && ::settings.get('displayFirstEmptyOption')\"><\/option>\r\n        <option data-ng-repeat=\"listItem in model.listItems track by $index\" value=\"{{ listItem[settings.get('keyField')] }}\">\r\n            {{ (settings.get('runtimeSelectLabel') && labels.contains(listItem[settings.get('valueField')]) ? labels.get(listItem[settings.get('valueField')]) : listItem[settings.get('valueField')]) }}\r\n        <\/option>\r\n    <\/select>\r\n<\/div>\r\n\r\n<div data-ng-if=\"::(settings.get('controlType') === 'select' && settings.get('multiple'))\" class=\"{{ ::namespaceClass('__select') + ' ' + namespaceClass('__select--multiple') }}\">\r\n    <select data-ng-model=\"$parent.ngModel\"\r\n            name=\"todo\" multiple=\"multiple\" mstar-validate\r\n            data-ng-disabled=\"parameters.disabled\" id=\"{{ ::namespaceId('select') }}\"\r\n            class=\"ec-form__select\">\r\n        <option value=\"\" data-ng-if=\"::!settings.get('multiple')\"><\/option>\r\n        <option data-ng-repeat=\"listItem in model.listItems track by $index\" value=\"{{ listItem[settings.get('keyField')] }}\">\r\n            {{ (settings.get('runtimeSelectLabel') && labels.contains(listItem[settings.get('valueField')]) ? labels.get(listItem[settings.get('valueField')]) : listItem[settings.get('valueField')]) }}\r\n        <\/option>\r\n    <\/select>\r\n<\/div>\r\n
         */
        select: string;

        /**
         * @default <ul data-ng-if=\"::settings.get('controlType') === 'customList'\" class=\"{{ ::namespaceClass(settings.get('groupClass')) }}\">\r\n    <li data-ng-repeat=\"customItemOption in ::customItemOptions track by $index\"\r\n        mstar-include=\"templates.customChosenTemplate\"\r\n        data-ng-click=\"chooseCustomItem(customItemOption.key)\"\r\n        class=\"{{ ::namespaceClass(settings.get('itemClass'))}}{{ customItemOption.key === ngModel ? ' ' + namespaceClass(settings.get('itemClass')) + '--selected':''}}\"><\/li>\r\n<\/ul>\r\n
         */
        customList: string;

        /**
         * @default <ul class=\"{{ ::namespaceClass('__button-list') }}\" data-ng-if=\"::(settings.get('controlType') === 'buttonList' && !settings.get('imageOptions'))\">\r\n    <li class=\"{{ ::namespaceClass('__button-item') }}\" data-ng-repeat=\"listItem in model.listItems track by $index\">\r\n        <button class=\"{{ ::namespaceClass('__btn') }}\"\r\n            data-ng-click=\"chooseButtonItem(listItem.key)\"\r\n            data-ng-class=\"parameters.buttonItemSelected == listItem.key || !parameters.buttonItemSelected && $index == 0 ? 'is-active' : ''\"\r\n            aria-pressed=\"{{ parameters.buttonItemSelected == listItem.key || !parameters.buttonItemSelected && $index == 0 }}\">\r\n            <span class=\"{{ ::namespaceClass('__btn-caption') }}\">{{ ::labels.get(listItem.value) }}<\/span>\r\n        <\/button>\r\n    <\/li>\r\n<\/ul>\r\n
         */
        buttonList: string;
    }


}
