// startref
/// <reference path='../../.references.ts'/>
// endref
module EcChosen {
    export enum _Type { ecChosen }

    /**
     * @title Chosen
     */
    export interface EcChosen {
        /** Component type.
         * @default ecChosen
         * @options.hidden true
         * @type string
         * @readonly true
         */
        type: _Type;

        settings: Settings;

        labels: Labels;
    }

    export interface CriteriaImage {
        key: any;
        itemClass: string;
    }

    export interface AccordionFilterItem {
        childFilters: string[];
        parentFilters: string[];
    }

    export interface CustomItemOption {
        /**
         * @default --modifier
         */
        classModifier: string;
        key: any;
        labelKey: string;
        preSelected: boolean;
    }

}
