// startref
/// <reference path='../../.references.ts'/>
// endref
module EcChosen {
    export interface Labels {
        noResultsText: AsterixCore.Label;
        placeholderTextMultiple: AsterixCore.Label;
        placeholderTextSingle: AsterixCore.Label;
    }
}
