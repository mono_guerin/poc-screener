// startref
/// <reference path='../../.references.ts'/>
// endref
module EcTable {
    export interface Settings extends AsterixCore.ComponentSettings {
        _model: any;

        _parameters: {
            caption: any;
            defaultPage: AsterixNg.Parameter;
            fields: any;
            keyField: any;

            page: any;
            perPage: any;

            /** Number of rows to move or change at a time
             *
             */
            paginationWindowSize: AsterixNg.Parameter;

            selectedItemKeys: any;
            sortField: any;
            sortFunc: AsterixNg.Parameter;
            sortOrder: any;
            sortableFields: AsterixNg.Parameter;
            stickyLeadBreakPoints: any;
            stickyState: any;
            totalRows: any;
        };

        _templates: {
            /** The main template.
             * @format html
             * @default <ec-section>\r\n\r\n    <ec-paginator mstar-component-id=\"paginatorTop\" data-ng-if=\"settings.get('paginationMode') == 'top' || settings.get('paginationMode') == 'both'\"><\/ec-paginator>\r\n\r\n    <ec-paginator mstar-component-id=\"paginatorSticky\" data-ng-class=\"settings.get('columnSkipActionsClass')\"\r\n        data-ng-if=\"settings.get('stickyLeadEnabled') && parameters.stickyState.stickyMode && settings.get('stickyPaginationMode') === 'top'\">\r\n    <\/ec-paginator>\r\n\r\n    <div mstar-include=\"templates.sortControls\" data-ng-if=\"settings.get('sortingEnabled')\"><\/div>\r\n    <table role=\"grid\" mstar-loader=\"model.rows._$isLoading\" data-ng-class=\":: settings.get('stickyLeadEnabled') ? 'ec-table__table--sticky' : ((settings.get('orientation') === 'vertical' ? 'ec-table__table--vertical' : 'ec-table__table--horizontal') + (settings.get('responsiveEnabled')?'':'-non-responsive'))\"\r\n        data-ng-swipe-disable-mouse data-ng-swipe-left=\"swipeLeft()\" data-ng-swipe-right=\"swipeRight()\" class=\"ec-table__table\">\r\n\r\n        <caption class=\"{{ settings.get('hideCaption') || !settings.get('caption') ? 'ec-table__caption--hidden' : 'ec-table__caption' }}\" mstar-tooltip-content=\"{{ :: labels.contains(settings.get('caption') + 'Tooltip') ? labels.get(settings.get('caption') + 'Tooltip') : '' }}\" mstar-tooltip>\r\n            {{ settings.get('caption') ? labels.get(settings.get('caption')) : namespaceId() }}\r\n        <\/caption>\r\n\r\n        <thead data-ng-if=\"settings.get('orientation') === 'vertical'\" class=\"ec-table__thead  ec-table__thead--vertical{{ :: settings.get('responsiveEnabled')?'':'-non-responsive' }}\">\r\n            <tr class=\"ec-table__row-header\">\r\n                <th data-ng-if=\"!field.fieldSettings.headerSkipped\" scope=\"col\" ec-table-cell data-ng-click=\"field.sortingEnabled && sort(field.fieldName)\" data-ng-repeat=\"field in visibleFields track by field.fieldName\" data-ng-class=\"{ 'ec-table__cell--numeric': field.formatDataType == 'number',\r\n                    'ec-table__cell--header-sorted': field.sortingEnabled && field.fieldName === parameters.sortField }\" class=\"ec-table__cell ec-table__cell--header\" mstar-drag=\"{{field.rearrangeEnabled && settings.get('columnsRearrangeEnabled') ? namespaceId() : ''}}\"\r\n                    mstar-drop=\"{{field.rearrangeEnabled && settings.get('columnsRearrangeEnabled') ? namespaceId() : ''}}\">\r\n                    <a href=\"javascript:;\" data-ng-if=\"field.sortingEnabled\" class=\"ec-table__sort\">\r\n                        <span mstar-tooltip-content=\"{{ :: labels.contains(field.headerLabelKey + 'Tooltip') ? labels.get(field.headerLabelKey + 'Tooltip') : '' }}\" mstar-tooltip>{{ ::field.header }}<\/span>\r\n                        <i class=\"fa fa-fw fa-sort\" data-ng-if=\"field.fieldName !== parameters.sortField\"><\/i>\r\n                        <i class=\"fa fa-fw fa-sort-{{parameters.sortOrder}}\" data-ng-if=\"field.fieldName === parameters.sortField\"><\/i>\r\n                    <\/a>\r\n                    <span data-ng-if=\"!field.sortingEnabled\" mstar-tooltip-content=\"{{ :: labels.contains(field.headerLabelKey + 'Tooltip') ? labels.get(field.headerLabelKey + 'Tooltip') : '' }}\" mstar-tooltip>{{ ::field.header }}<\/span>\r\n                <\/th>\r\n            <\/tr>\r\n        <\/thead>\r\n\r\n        <tbody data-ng-if=\"settings.get('orientation') === 'vertical'\" class=\"ec-table__tbody ec-table__tbody--vertical{{ :: settings.get('responsiveEnabled')?'':'-non-responsive' }}\">\r\n            <tr data-ng-repeat-start=\"obj in visibleData track by keyFieldParsed(obj) || $index\" data-ng-class=\"[isSelected(keyFieldParsed(obj)) ? 'ec-table__row--selected' : '', (templates.expandedRow && obj._displayingExpandedView) ? 'ec-table__row--expanded' : '', settings.get('selectByRowEnabled') ? 'ec-table__row--clickable' : '', (settings.get('accordionMode') && $index > settings.get('accordionMinRow') - 1) ? 'ec-table__row--hide' : '']\"\r\n                data-ng-click=\"toggleSelectedRow(keyFieldParsed(obj), obj, $event)\" data-ng-init=\"rowIndex=$index\" class=\"ec-table__row\" id=\"{{namespaceId('-row-'+$index)}}\">\r\n                <td ec-table-cell data-ng-repeat=\"field in visibleFields track by field.fieldId || field.fieldName\" data-ng-class=\"[(settings.get('accordionDetailsEnable') && $index > settings.get('accordionDetailsMinRow') - 1) ? 'ec-table__cell--details-hide' : '', field.formatDataType == 'number' ? 'ec-table__cell--numeric' : '']\"\r\n                    data-ng-init=\"colIndex=$index\" data-title=\"{{ ::field.header }}\" class=\"ec-table__cell ec-table__cell--data\">\r\n                    <div class=\"ec-table__cell-content\">\r\n                        {{ field.format(field.getter(obj)) }}\r\n                    <\/div>\r\n                <\/td>\r\n                <td class=\"ec-table__cell--showdetails\" data-ng-if=\"settings.get('accordionDetailsEnable')\">\r\n                    <button data-ng-click=\"handleTableDetailsToggle($event)\" data-ng-class=\"tableDetailsToggleClass\" class=\"ec-table__toggle-details\">{{accordionDetailsButton}}<\/button>\r\n                <\/td>\r\n            <\/tr>\r\n            <tr data-ng-repeat-end data-ng-if=\"templates.expandedRow && obj._displayingExpandedView\" class=\"ec-table__row ec-table__row--extended\">\r\n                <td colspan=\"{{ visibleFields.length }}\" class=\"ec-table__cell ec-table__cell--data ec-table__cell--last\">\r\n                    <div mstar-include=\"templates.expandedRow\"><\/div>\r\n                <\/td>\r\n                <td class=\"ec-table__cell--showdetails\" data-ng-if=\"settings.get('accordionDetailsEnable')\"><\/td>\r\n            <\/tr>\r\n        <\/tbody>\r\n\r\n        <tbody data-ng-if=\"settings.get('orientation') === 'horizontal'\" class=\"ec-table__tbody ec-table__tbody--horizontal\">\r\n            <tr class=\"ec-table__row-header\" data-ng-if=\"settings.get('horizontalHeaderField')\">\r\n                <td data-ng-if=\"!labels.contains(settings.get('horizontalHeaderField'))\"><\/td>\r\n                <th scope=\"col\" data-ng-if=\"labels.contains(settings.get('horizontalHeaderField'))\" class=\"ec-table__cell ec-table__cell--header\">{{labels.get(settings.get('horizontalHeaderField'))}}<\/th>\r\n                <th scope=\"col\" data-ng-repeat=\"obj in visibleData track by keyFieldParsed(obj) || $index\" class=\"ec-table__cell ec-table__cell--header\">\r\n                    {{ obj[settings.get('horizontalHeaderField')] }}\r\n                <\/th>\r\n            <\/tr>\r\n            <tr data-ng-class=\"(settings.get('accordionMode') && $index > settings.get('accordionMinRow') - 1) ? 'ec-table__row--hide' : ''\" data-ng-repeat=\"field in visibleFields track by field.fieldId || field.fieldName\" data-ng-init=\"rowIndex=$index\" class=\"ec-table__row\">\r\n                <th scope=\"row\" ec-table-cell data-ng-click=\"field.sortingEnabled && sort(field.fieldName)\" data-col-index=\"0\" data-ng-if=\"!field.fieldSettings.headerSkipped\" data-ng-class=\"{ 'ec-table__cell--header-sorted': field.sortingEnabled && field.fieldName === parameters.sortField }\"\r\n                    class=\"ec-table__cell ec-table__cell--header\" mstar-drag=\"{{field.rearrangeEnabled && settings.get('columnsRearrangeEnabled') ? namespaceId() : ''}}\" mstar-drop=\"{{field.rearrangeEnabled && settings.get('columnsRearrangeEnabled') ? namespaceId() : ''}}\">\r\n                    <div class=\"ec-table__cell-content\">\r\n                        <a data-ng-if=\"field.sortingEnabled\" href=\"javascript:;\" class=\"ec-table__sort\">\r\n                            <span mstar-tooltip-content=\"{{ :: labels.contains(field.headerLabelKey + 'Tooltip') ? labels.get(field.headerLabelKey + 'Tooltip') : '' }}\" mstar-tooltip>{{ ::field.header }}<\/span>\r\n                            <i class=\"fa fa-fw fa-sort\" data-ng-if=\"field.fieldName !== parameters.sortField\"><\/i>\r\n                            <i class=\"fa fa-fw fa-sort-{{parameters.sortOrder}}\" data-ng-if=\"field.fieldName === parameters.sortField\"><\/i>\r\n                        <\/a>\r\n                        <span data-ng-if=\"!field.sortingEnabled\" mstar-tooltip-content=\"{{ :: labels.contains(field.headerLabelKey + 'Tooltip') ? labels.get(field.headerLabelKey + 'Tooltip') : '' }}\" mstar-tooltip>\r\n                            {{ ::field.header }}\r\n                        <\/span>\r\n                    <\/div>\r\n                <\/th>\r\n                <td ec-table-cell data-ng-class=\"[isSelected(keyFieldParsed(obj)) ? 'ec-table__row--selected' : '', field.formatDataType == 'number' ? 'ec-table__cell--numeric' : '']\" data-ng-repeat=\"obj in visibleData track by keyFieldParsed(obj) || $index\"\r\n                    data-ng-init=\"colIndex=$index\" data-title=\"{{ ::field.header }}\" class=\"ec-table__cell ec-table__cell--data\">\r\n                    <div class=\"ec-table__cell-content\">\r\n                        {{ field.format(field.getter(obj)) }}\r\n                    <\/div>\r\n                <\/td>\r\n            <\/tr>\r\n        <\/tbody>\r\n\r\n    <\/table>\r\n\r\n    <footer data-ng-if=\"footNotes\" class=\"ec-table__footer\">\r\n        <ul class=\"ec-table__foot-notes\">\r\n            <li data-ng-repeat=\"footNote in footNotes\" class=\"ec-table__foot-note\">\r\n                <ec-table-foot-note><\/ec-table-foot-note>\r\n            <\/li>\r\n        <\/ul>\r\n    <\/footer>\r\n\r\n    <button data-ng-if=\"settings.get('accordionMode') && showMoreLink()\" data-ng-click=\"handleTableRowsToggle($event)\" data-ng-class=\"tableRowsToggleClass\" class=\"ec-table__toggle-row\">\r\n        {{accordionShowMoreLessText}}\r\n    <\/button>\r\n\r\n    <ec-paginator mstar-component-id=\"paginator\" data-ng-if=\"settings.get('paginationMode') == 'bottom' || settings.get('paginationMode') == 'both'\"><\/ec-paginator>\r\n\r\n    <ec-paginator mstar-component-id=\"paginatorSticky\" data-ng-class=\"settings.get('columnSkipActionsClass')\"\r\n        data-ng-if=\"settings.get('stickyLeadEnabled') && parameters.stickyState.stickyMode && settings.get('stickyPaginationMode') === 'bottom'\">\r\n    <\/ec-paginator>\r\n\r\n<\/ec-section>\r\n
             */
            main: string;

            /** The main (light) template.
             * @format html
             * @default <ec-section template-type=\"light\">\r\n    <table class=\"ec-table__table ec-table__table--{{ :: settings.get('stickyLeadEnabled') ? 'sticky' : (settings.get('orientation') === 'vertical' ? 'vertical' : 'horizontal') }}{{ :: settings.get('stickyLeadEnabled') || settings.get('responsiveEnabled') ? '' : '-non-responsive' }}\">\r\n\r\n        <caption class=\"{{ settings.get('hideCaption') || !settings.get('caption') ? 'ec-table__caption--hidden' : 'ec-table__caption' }}\" mstar-tooltip-content=\"{{ :: labels.contains(settings.get('caption') + 'Tooltip') ? labels.get(settings.get('caption') + 'Tooltip') : '' }}\" mstar-tooltip>\r\n            {{ settings.get('caption') ? labels.get(settings.get('caption')) : namespaceId() }}\r\n        <\/caption>\r\n\r\n        <thead data-ng-if=\"settings.get('orientation') === 'vertical'\" class=\"ec-table__thead ec-table__thead--vertical{{ :: settings.get('responsiveEnabled') ? '' : '-non-responsive' }}\">\r\n            <tr class=\"ec-table__row-header\">\r\n                <th scope=\"col\" ec-table-cell data-ng-repeat=\"field in visibleFields track by field.fieldName\" data-ng-class=\"{ 'ec-table__cell--numeric': field.formatDataType == 'number'}\" class=\"ec-table__cell ec-table__cell--header\">\r\n                    <span mstar-tooltip-content=\"{{ :: labels.contains(field.headerLabelKey + 'Tooltip') ? labels.get(field.headerLabelKey + 'Tooltip') : '' }}\" mstar-tooltip>{{ ::field.header }}<\/span>\r\n                <\/th>\r\n            <\/tr>\r\n        <\/thead>\r\n\r\n        <tbody data-ng-if=\"settings.get('orientation') === 'vertical'\" class=\"ec-table__tbody ec-table__tbody--vertical{{ :: settings.get('responsiveEnabled') ? '' : '-non-responsive' }}\">\r\n            <tr data-ng-repeat=\"obj in visibleData track by keyFieldParsed(obj) || $index\" class=\"ec-table__row\">\r\n                <td ec-table-cell-light data-ng-repeat=\"field in visibleFields track by field.fieldId || field.fieldName\"\r\n                    data-title=\"{{ ::field.header }}\" class=\"ec-table__cell ec-table__cell--data {{ :: field.formatDataType === 'number' ? 'ec-table__cell--numeric' : '' }}\">\r\n                    <div class=\"ec-table__cell-content\">\r\n                        {{ field.format(field.getter(obj)) }}\r\n                    <\/div>\r\n                <\/td>\r\n            <\/tr>\r\n        <\/tbody>\r\n\r\n        <tbody data-ng-if=\"settings.get('orientation') === 'horizontal'\" class=\"ec-table__tbody ec-table__tbody--horizontal\">\r\n            <tr data-ng-repeat=\"field in visibleFields track by field.fieldId || field.fieldName\" class=\"ec-table__row\">\r\n                <th scope=\"row\" ec-table-cell-light data-ng-if=\"!field.fieldSettings.headerSkipped\"\r\n                    class=\"ec-table__cell ec-table__cell--header\">\r\n                    <div class=\"ec-table__cell-content\">\r\n                        <span mstar-tooltip-content=\"{{ :: labels.contains(field.headerLabelKey + 'Tooltip') ? labels.get(field.headerLabelKey + 'Tooltip') : '' }}\" mstar-tooltip>\r\n                            {{ ::field.header }}\r\n                        <\/span>\r\n                    <\/div>\r\n                <\/th>\r\n                <td ec-table-cell-light data-ng-repeat=\"obj in visibleData track by keyFieldParsed(obj) || $index\"\r\n                    data-title=\"{{ ::field.header }}\" class=\"ec-table__cell ec-table__cell--data {{ :: field.formatDataType === 'number' ? 'ec-table__cell--numeric' : '' }}\">\r\n                    <div class=\"ec-table__cell-content\">\r\n                        {{ field.format(field.getter(obj)) }}\r\n                    <\/div>\r\n                <\/td>\r\n            <\/tr>\r\n        <\/tbody>\r\n\r\n    <\/table>\r\n    <footer data-ng-if=\"footNotes\" class=\"ec-table__footer\">\r\n        <ul class=\"ec-table__foot-notes\">\r\n            <li data-ng-repeat=\"footNote in footNotes\" class=\"ec-table__foot-note\">\r\n                <ec-table-foot-note><\/ec-table-foot-note>\r\n            <\/li>\r\n        <\/ul>\r\n    <\/footer>\r\n<\/ec-section>\r\n
             */
            light: string;

            /** Expanded Row Template
             * @format html
             */
            expandedRow: string;

            /**Sort Control Template
             * @format html
             * @default <div class=\"ec-table__sort-controls\">\r\n\t<div class=\"ec-table__sort-options\">\r\n    \t<label class=\"ec-table__sort-field\">{{labels.get(\"sortField\")}}<\/label>\r\n        <ec-chosen mstar-component-id=\"sortFieldsDropdown\" data-ng-model=\"parameters.sortField\"><\/ec-chosen>\r\n    <\/div>\r\n    <div class=\"ec-table__sort-order\">\r\n    \t<label class=\"ec-table__sort-label\">{{labels.get(\"sortBy\")}}<\/label>\r\n    \t<ec-chosen mstar-component-id=\"sortOrderRadio\" data-ng-model=\"parameters.sortOrder\"><\/ec-chosen>\r\n    <\/div>\r\n<\/div>\r\n
             */
            sortControls: string;

            /** The foot template.
             * @format html
             * @default ﻿<span class=\"ec-table__foot-note-indicator\">{{labels.get(footNote.indicatorLabelKey) }}:<\/span>\r\n<span class=\"ec-table__foot-note-content\" data-ng-bind-html=\"footNote.content\"><\/span>\r\n
             */
            footNoteItemTemplate: string;
        };

        /** More Details accordion for expand/collapse in mobile
         * @default false
         */
        accordionDetailsEnable: boolean;

        /** Show more label for button
         * @default Hide full details
         */
        accordionDetailsHide: string;

        /** Show more label for button
         * @default Show full details
         */
        accordionDetailsShow: string;

        /** Number of rows to show on pageload (if accordionMode is enabled)
         * @default 3
         */
        accordionDetailsMinRow: number;

        /** Max "breakpoint" width for button to show
         * @default 668
         */
        accordionDetailsComponentMaxWidth: number;

        /** Number of rows to show on pageload (if accordionMode is enabled)
         * @default 5
         */
        accordionMinRow: number;

        /** show/hide accordion for expand/collapse.
         * @default false
         */
        accordionMode: boolean;

        /** Label key for table caption, please always add a caption to the table even when using hideCaption=true */
        caption: string;

        /**
         * @default false
         */
        columnsRearrangeEnabled: boolean;

        /** Sticky Lead column skip actions class */
        columnSkipActionsClass: string;

        /** Default field settings. Templates, css classes, formats */
        defaultFieldSettings: TableFieldSettings;

        /** Default page to set
         * @default 1
         */
        defaultPage: number;

        /** Whether expanded all rows
         *  @default false
         */
        expandAllRows: boolean;

        /** Enable partial checkbox support
         * @default false
         */
        enablePartialCheckbox: boolean;

        /** Fields
         * @format tabs
         * @title Fields
         */
        fields: TableField[];

        /**
         * @default false
         */
        fixedTableHeaderEnabled?: boolean;

        fixedTableHeaderScrollContainer?: string;

        /** Use this setting in order to hide the caption, but keep it readable for Screen readers
         * @default false
         */
        hideCaption: boolean;

        /** For Horizontal Orientation, display header with this field */
        horizontalHeaderField: string;

        /** Key field */
        keyField: string;

        /**
         @default ec-table
         */
        namespaceClass: string;

        /** The table orientation
         * @default vertical
         * @type string
         */
        orientation: TableOrientation;

        /**
         * @default 1
         */
        page: number;

        /**
         * @default bottom
         */
        paginationMode: TablePaginationMode;

        /** Number of rows to move or change at a time
         *
         */
        paginationWindowSize?: number;

        /** Position of sticky pagination
         * @default top
         */
        stickyPaginationMode: TableStickyPaginationMode

        /**
         * @default 2
         */
        perPage: number;

        /** Persist state in querystring.
         * @default false
         */
        query: boolean;

        /** Turn the responsive feature on / off.
         * @default true
         */
        responsiveEnabled: boolean;

        /** Enable row select by clicking on row
         * @default false
         */
        selectByRowEnabled: any;

        /** Selected item keys.
         * @default []
         */
        selectedItemKeys: string[];

        /** Shared templates that could be referenced by config
         * @default {}
         */
        sharedTemplates: any;

        /** Sort case-insensitive
         * @default false
         */
        sortCaseInsensitive: boolean;

        /** Sort field */
        sortField: string;

        /** Sorting enabled
         * @default true
         */
        sortingEnabled: boolean;

        /** Sort order.
         * @default asc
         * @type string
         */
        sortOrder: SortOrder;

        /** Sortable Fields.*/
        sortableFields: any;

        /** custom sort function*/
        sortFunc: any;

        /** Standalone mode @default true */
        standalone: boolean;

        /** Swipe Behavior. Only support page or column,the column option only works if stickyLeadEnabled is set to true
         * When swiping the table there are two options, one is changing page, another is changing column
         *@default page
         */
        swipeBehavior: string;

        /** Sticky Lead enabled
         *@default false
         */
        stickyLeadEnabled: boolean;

        /** Sticky Lead break points
         */
        stickyLeadBreakPoints: StickyLeadBreakPoint[];
    }

    export interface TableField {
        /** arrange enabled
         * @default true
         */
        rearrangeEnabled: boolean;

        /** Field name */
        fieldName: string;

        /** Field settings. Templates, css classes, formats. */
        fieldSettings?: TableFieldSettings;

        /** Header
         * @default null
         */
        header: string;

        /** Header Label Key */
        headerLabelKey: string;

        /** Sorting enabled
         * @default true
         */
        sortingEnabled?: boolean;
    }

    /**
     * @options.collapsed true
     */
    export interface TableFieldSettings {
        /** Checkbox header template
         * @format html
         * @default <th>\r\n    <div class=\"{{namespaceClass('__cell-content')}}\">\r\n        <div class=\"ec-form__checkbox\" data-ng-class=\"{'ec-form__checkbox--mixed' : mixedSelect && settings.get('enablePartialCheckbox')}\">\r\n            <label class=\"ec-form__label ec-form__label--checkbox\">\r\n                <!-- TODO: ecChosen's checkbox? -->\r\n                <input type=\"checkbox\" data-ng-checked=\"selectAll\" class=\"{{namespaceClass('__checkbox')}}\" data-ng-click=\"toggleSelectedRows($event)\" \/>\r\n                <span class=\"ec-form__checkbox-text\">\r\n                    <!-- TODO: Add label -->\r\n                <\/span>\r\n            <\/label>\r\n        <\/div>\r\n    <\/div>\r\n<\/th>\r\n
         */
        checkboxHeaderTemplate?: string;

        /** Checkbox item template
         * @format html
         * @default <td>\r\n    <div class=\"{{namespaceClass('__cell-content')}} {{namespaceClass('__cell--checkbox')}}\">\r\n        <div class=\"ec-form__checkbox\">\r\n            <label class=\"ec-form__label ec-form__label--checkbox\">\r\n                <!-- TODO: ecChosen's checkbox? -->\r\n                <input type=\"checkbox\" data-ng-checked=\"isSelected(keyFieldParsed(obj))\" data-ng-click=\"toggleSelectedRow(keyFieldParsed(obj), obj, $event, field)\" class=\"{{namespaceClass('__checkbox')}}\" \/>\r\n                <span class=\"ec-form__checkbox-text\">\r\n                    <!-- TODO: Add label -->\r\n                <\/span>\r\n            <\/label>\r\n        <\/div>\r\n\r\n    <\/div>\r\n<\/td>\r\n
         */
        checkboxItemTemplate?: string;

        /** The header class
         * @default null
         */
        headerClass?: string;

        /** Header colspan
         * @type number
         * @default -1
         */
        headerColspan: number;

        /** Header rowspan
         * @type number
         * @default -1
         */
        headerRowspan: number;

        /** Header skipped
         * @type number
         * @default false
         */
        headerSkipped: boolean;

        /** Header template
         * @format html
         */
        headerTemplate?: string;

        /** Hyperlink item template
         * @format html
         * @default <td>\r\n    <div class=\"{{namespaceClass('__cell-content')}}\">\r\n        <a data-ng-href=\"{{getHyperlinkUrl(obj, field.fieldSettings.hyperlink)}}\"\r\n           data-ng-attr-target=\"{{field.fieldSettings.hyperlink.target || undefined}}\"\r\n           data-ng-class=\"field.fieldSettings.hyperlink.cssClass\"\r\n           data-ng-click=\"openHyperlink($event);\"\r\n           class=\"{{namespaceClass('__link')}}\">{{getHyperlinkText(obj, field.fieldSettings.hyperlink)}}<\/a>\r\n    <\/div>\r\n<\/td>\r\n
         */
        hyperlinkItemTemplate: string;

        /** Hyperlink field settings */
        hyperlink: HyperlinkSettings;

        /** Button item template
         * @format html
         * @default <td>\r\n    <div class=\"{{namespaceClass('__cell-content')}}\">\r\n        <ec-button mstar-component-id=\"{{settings.get('orientation') === 'horizontal' ? field.fieldSettings.button.componentId + '[' + colIndex + ']' : field.fieldSettings.button.componentId + '[' + rowIndex + ']'}}\" data-ng-click=\"field.fieldSettings.button.click(keyFieldParsed(obj), $event)\"><\/ec-button>\r\n    <\/div>\r\n<\/td>\r\n
         */
        buttonItemTemplate: string;

        /** Button field settings */
        button: ButtonSettings;

        /** The item class
         *  @default null
         */
        itemClass?: string;

        /** Item template
         * @format html
         */
        itemTemplate?: string;

        /** Radio item template
         * @format html
         * @default <td>\r\n    <div class=\"{{namespaceClass('__cell-content')}} {{namespaceClass('__cell--radio')}}\" data-ng-class=\"isSelected(keyFieldParsed(obj)) ? namespaceClass('__cell-radio--selected') : ''\">\r\n        <div class=\"ec-form__radio\">\r\n            <label class=\"ec-form__label ec-form__label--radio\">\r\n                <input type=\"radio\" name=\"{{namespaceId('radio')}}\" data-ng-checked=\"isSelected(keyFieldParsed(obj))\" data-ng-click=\"toggleSelectedRow(keyFieldParsed(obj), obj, $event, field)\" class=\"{{namespaceClass('__radio')}}\" \/>\r\n                <span class=\"ec-form__radio-text\"><\/span>\r\n            <\/label>\r\n        <\/div>\r\n    <\/div>\r\n<\/td>\r\n
         */
        radioItemTemplate?: string;
    }

    export interface HyperlinkSettings {
        cssClass?: string;
        target?: string;
        text?: string;
        textField?: string;
        url?: string;
        urlFields?: string;
    }

    export interface ButtonSettings {
        componentId?: string;
        click?: any;
    }
}
