// startref
/// <reference path='../../.references.ts'/>
// endref
module EcTable {
    export enum _TableType { ecTable }

    export enum SortOrder {
        asc,
        desc
    }

    export enum TableOrientation {
        vertical,
        horizontal
    }

    export enum TablePaginationMode {
        none,
        top,
        bottom,
        both
    }

    export enum TableStickyPaginationMode {
        top,
        bottom
    }

    export class StickyLeadBreakPoint {
        /**
         * @default [0]
         */
        stickyColIndices: number[];

        /**
         * @default 1
         */
        columnsPerPage: number;

        minWidth: number;
        maxWidth: number;
        skip: number;
    }

    export interface EcTable {
        components: {
            paginator: EcPaginator.EcPaginator;
            paginatorTop: EcPaginator.EcPaginator;
            paginatorSticky: EcPaginator.EcPaginator;
            sortFieldsDropdown: EcChosen.EcChosen;
            sortOrderRadio: EcChosen.EcChosen;
        };

        labels: Labels;

        settings: Settings;

        /** Component type.
         * @default ecTable
         * @options.hidden true
         * @type string
         * @readonly true
         */
        type: _TableType;
    }
}
