// startref
/// <reference path='../../.references.ts'/>
// endref
module EcTable {
    export interface Labels {
    	sortBy: AsterixCore.Label;
    	sortField: AsterixCore.Label;
    }
}
