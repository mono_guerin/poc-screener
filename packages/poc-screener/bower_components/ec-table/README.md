**Please DO NOT contribute to this repo directly. All PR should go to the team project**

## table

### Introduction


### Accessibility considerations

+ Marking up tables: https://www.w3.org/WAI/tutorials/tables/
+ ARIA roles: https://www.w3.org/TR/wai-aria/roles#grid
