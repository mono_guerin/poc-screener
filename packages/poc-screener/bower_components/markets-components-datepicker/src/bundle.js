define([], function() {
    require('es5-shim');
    require('jquery');
    require('handlebars/runtime');
    require('asterix-core');
    require('markets-components-core');
    require('../build/markets-components-datepicker.js');
});
