define(['morningstar'], function (morningstar) {
	'use strict';
	var QSAPI = morningstar.components['markets-components-core'];
	return QSAPI.components.register('datepicker', require('./js/component.js'));
});