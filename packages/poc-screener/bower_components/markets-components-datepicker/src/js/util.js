define(['morningstar'], function(morningstar) {
    'use strict';

    return {
        isDate: function(obj) {
            return (/Date/).test(Object.prototype.toString.call(obj)) && !isNaN(obj.getTime());
        },
        isWeekend: function(date) {
            var day = date.getDay();
            return day === 0 || day === 6;
        },
        isLeapYear: function(year) {
            return year % 4 === 0 && year % 100 !== 0 || year % 400 === 0;
        },
        getDaysInMonth: function(year, month) {
            return [31, this.isLeapYear(year) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
        },
        setToStartOfDay: function(date) {
            if (this.isDate(date)) date.setHours(0, 0, 0, 0);
        },
        compareDates: function(a, b) {
            if (this.isDate(a) && this.isDate(b)) {
                return a.getTime() === b.getTime();
            } else {
                return false;
            }

        }
    };


});