define(['morningstar', './util', './locale', '../html/tableTemplate.html', '../html/titleTemplate.html', '../html/datePicker.html', '../html/monthYearTemplate.html'],
    function(morningstar, util, locale, tableTemplate, titleTemplate, datePickerTemplate, monthYearTemplate) {
        'use strict';

        return function(container, options, callbacks) {

            var componentName = 'markets-date-picker',
                QSAPI = morningstar.components['markets-components-core'],
                $ = QSAPI.$,
                $container,
                _callbacks,
                _options,
                _startDate,
                _endDate,
                _needInputs = false,
                _currentField,
                _isVisble,
                _which,
                _calendars,
                _calenderEl,
                _year,
                _month,
                _scrollTimer;


            var defaultOptions = {

                // bind the picker to a form field
                dataFields: [],
                //language
                lang: 'pt',
                // position of the datepicker, relative to the field (default to bottom & left)
                position: 'bottom left',

                // automatically fit in the viewport
                reposition: true,

                //format function, only works when no inputs passed in 
                format: function(date) {
                    return date.toLocaleDateString();
                },

                triggerClass: 'mkts-cmpt-datepicker-input',
                //single or mutiple
                isMutiple: true,

                // first day of week (0: Sunday, 1: Monday etc)
                firstDay: 0,

                //high light days between start date and end date
                highlightBetween: true,

                //disable insignificance days
                showDisable: true,

                //highlight today
                highlightToday: true,

                //highlight both start date, end date
                highlightMutiSelected: true,

                // the minimum/earliest date that can be selected
                minDate: null,
                // the maximum/latest date that can be selected
                maxDate: null,

                // number of years either side, or array of upper/lower range at first load
                yearRange: 10,

                minYear: 1000,

                maxYear: function() { var date = new Date(); return date.getFullYear() + 1; }(),

                // Render the month after year in the calendar title
                showMonthAfterYear: false,
            };

            function loadConfig(options) {
                if (!_options) {
                    _options = $.extend(true, {}, defaultOptions);
                }
                var opts = $.extend(true, {}, _options, options);

                _startDate = opts.startDate;
                _endDate = opts.endDate;

                var labels = locale.getLabels(opts.lang);
                opts.labels = $.extend(true, {}, labels, opts.labels);

                opts.disableWeekends = !!opts.disableWeekends;

                if (!util.isDate(opts.minDate)) {
                    opts.minDate = false;
                }
                if (!util.isDate(opts.maxDate)) {
                    opts.maxDate = false;
                }
                if ((opts.minDate && opts.maxDate) && opts.maxDate < opts.minDate) {
                    opts.maxDate = opts.minDate = false;
                }
                if (opts.minDate) {
                    setMinDate(opts.minDate)
                }
                if (opts.maxDate) {
                    util.setToStartOfDay(opts.maxDate);
                    opts.maxYear = opts.maxDate.getFullYear();
                    opts.maxMonth = opts.maxDate.getMonth();
                }

                if ($.isArray(opts.yearRange)) {
                    var fallback = new Date().getFullYear() - 10;
                    opts.yearRange[0] = parseInt(opts.yearRange[0], 10) || fallback;
                    opts.yearRange[1] = parseInt(opts.yearRange[1], 10) || fallback;
                } else {
                    opts.yearRange = Math.abs(parseInt(opts.yearRange, 10)) || defaultOptions.yearRange;
                }
                return opts;
            }

            function renderMonthYearSelection() {
                var year = _calendars[0].year,
                    month = _calendars[0].month;
                var monthArr, yearArr,
                    isMinYear = year === _options.minYear,
                    isMaxYear = year === _options.maxYear,
                    i, j;
                for (monthArr = [], i = 0; i < 12; i++) {
                    monthArr.push({
                        value: i,
                        label: _options.labels.shortMonths[i],
                        selectedClass: i === month ? ' selected' : ''
                    });
                }
                if ($.isArray(_options.yearRange)) {
                    i = _options.yearRange[0];
                } else {
                    i = year - _options.yearRange;
                }
                j = _options.maxYear + 1;
                for (yearArr = []; i < j && i <= _options.maxYear; i++) {
                    if (i >= _options.minYear) {
                        yearArr.push({
                            value: i,
                            label: i,
                            selectedClass: i === year ? ' selected' : '',
                            isCurrentYear: i === year,
                            monthList: monthArr
                        });
                    }
                }
                $container.find('.mkts-cmpt-datepicker-body').html(monthYearTemplate({
                    yearList: yearArr
                }));
                var yearEle = $container.find('.mkts-cmpt-datepicker-select-year');
                yearEle.on('scroll.mkts-cmpt-datepicker-container', appendYears);
                scrollTo(yearEle, $container.find('.mkts-cmpt-datepicker-current-year'));
            }

            function appendYears(e) {
                var $target = $(e.target || e.srcElement);
                clearTimeout(_scrollTimer);
                _scrollTimer = setTimeout(function() {
                    var ulHeight = $target.height();
                    var scroll_top = $target.scrollTop();
                    var liLength = $target.find('li').length;
                    var liEl = $target.find('li').eq(0);
                    var liHeight = liEl.height() + Number(liEl.css("margin-bottom").replace("px", '')) + Number(liEl.css("margin-top").replace("px", ''));
                    var year = liEl.data('year');
                    if ((scroll_top / (liHeight * liLength)) < 0.1) {
                        for (var i = 1; i <= _options.yearRange; i++) {
                            if (year - i > _options.minYear) {
                                var liItem = '<li data-year=' + (year - i) + ' class="mkts-cmpt-datepicker-year-li">' + (year - i) + '</li>';
                                $target.prepend(liItem);
                            }
                        }
                        $target.scrollTop(liHeight * liLength / 5);
                    }
                }, 100);
            }

            function renderTitle(year, month) {
                var i, j,
                    isMinYear = year === _options.minYear,
                    isMaxYear = year === _options.maxYear,
                    html = '',
                    prev = true,
                    next = true;

                if (isMinYear && month === 0) {
                    prev = false;
                }
                if (isMaxYear && month === 11) {
                    next = false;
                }
                var html = titleTemplate({
                    showMonthAfterYear: _options.showMonthAfterYear,
                    month: _options.labels.months[month],
                    year: year,
                    preDisableClass: prev ? '' : ' is-disabled',
                    nextDisableClass: next ? '' : ' is-disabled'
                });
                $container.find('.mkts-cmpt-datepicker-title').html(html);
            };

            function renderBody(year, month) {
                var now = new Date(),
                    days = util.getDaysInMonth(year, month),
                    day,
                    dayArr = [],
                    before = new Date(year, month, 1).getDay(),
                    data = [],
                    row = [];
                util.setToStartOfDay(now);
                if (_options.firstDay > 0) {
                    before -= _options.firstDay;
                    if (before < 0) {
                        before += 7;
                    }
                }
                var cells = Math.ceil((days + before) / 7) * 7;
                var tableObject = {
                    weekObjects: [],
                    dayNames: []
                };
                for (var i = 0, r = 0; i < cells; i++) {

                    var date = new Date(year, month, 1 + (i - before)),
                        isBetween = false,
                        isSelected = false,
                        isSelectedStart = false,
                        isSelectedEnd = false,
                        isCurrentMonth = true,
                        isToday = util.compareDates(date, now),
                        isDisabled = (_options.minDate && date < _options.minDate) ||
                        (_options.maxDate && date > _options.maxDate) ||
                        (_options.disableWeekends && util.isWeekend(date)),
                        dayNum = date.getDate(),
                        monthNum = month;
                    if (i < before) {
                        monthNum = month - 1;
                        isCurrentMonth = false;
                    } else if (i >= (days + before)) {
                        monthNum = month + 1;
                        isCurrentMonth = false;
                    }

                    if (_options.highlightMutiSelected) {
                        if (util.compareDates(date, _startDate)) {
                            isSelectedStart = true;
                            isSelected = true;
                        } else if (util.compareDates(date, _endDate)) {
                            isSelectedEnd = true;
                            isSelected = true;
                        }

                    } else {
                        var compareDate = _which === 'start' ? _startDate : _endDate;
                        if (util.compareDates(date, compareDate)) {
                            isSelected = true;
                        }
                    }

                    if (!isDisabled && util.isDate(_startDate) && $(_currentField).hasClass('mkts-cmpt-datepicker-end')) {
                        isDisabled = date < _startDate;
                    }

                    if (util.isDate(_startDate) && util.isDate(_endDate)) {
                        isBetween = (date > _startDate && date < _endDate);
                    }
                    if (!isSelected) {
                        isSelectedStart = false;
                        isSelectedEnd = false;
                    }
                    var arr = ['mkts-cmpt-datepicker-td'];
                    if (isDisabled && _options.showDisable) {
                        arr.push('is-disabled');
                    }
                    if (isToday && _options.highlightToday) {
                        arr.push('is-today');
                    }
                    if (isSelected) {
                        arr.push('is-selected');
                    }
                    if (isBetween && _options.highlightBetween) {
                        arr.push('is-between');
                    }
                    if (isSelectedStart) {
                        arr.push('is-selected-start');
                    }
                    if (isSelectedEnd) {
                        arr.push('is-selected-end');
                    }
                    if (!isCurrentMonth) {
                        arr.push('not-current-month');
                    }
                    var dayObject = {
                        d: dayNum,
                        className: arr.join(' '),
                        y: year,
                        m: monthNum
                    };

                    dayArr.push(dayObject);
                    if (++r === 7) {
                        tableObject.weekObjects.push({
                            days: $.extend(true, {}, dayArr)
                        });
                        dayArr = [];
                        r = 0;
                    }
                }

                for (i = 0; i < 7; i++) {
                    tableObject.dayNames.push({ name: _options.labels.shortDays[i] });
                }
                $container.find(".mkts-cmpt-datepicker-body").html(tableTemplate(tableObject));
            };

            function onMonthSelected(e) {
                var target = e.target || e.srcElement;
                var year = $(target).parent().data("year");
                var month = $(target).data("month");
                _calendars[0].year = year;
                _calendars[0].month = month;
                adjustCalendars(true);
            };

            function getDate(which) {
                var date = _startDate;
                if (which && which === "end") {
                    date = _endDate;
                }
                return util.isDate(date) ? new Date(date.getTime()) : null;
            };

            function expandMonth(target) {
                clearTimeout(_scrollTimer);
                var $monthUl = $('.mkts-cmpt-datepicker-current-year').removeClass("mkts-cmpt-datepicker-current-year").next();
                $monthUl.data('year', $(target).data('year'));
                $(target).after($monthUl).addClass("mkts-cmpt-datepicker-current-year");
                scrollTo($('.mkts-cmpt-datepicker-select-year'), $(target), true);
            };

            function scrollTo(container, target, isAnimate) {
                var speed = isAnimate ? '100' : 0;
                container.animate({
                    scrollTop: target.offset().top - container.offset().top + container.scrollTop()
                }, 100);
            };

            function createFields(fields) {
                _options.fields = [];
                for (var i = 0; i < fields.length; i++) {
                    var field = $(fields[i]),
                        date, $input = field.clone();
                    $input.addClass("mkts-cmpt-datepicker-" + field.attr("class"));
                    if (i === 0) {
                        $input.attr('data-pika-field', 'start').addClass("mkts-cmpt-datepicker-data-start mkts-cmpt-datepicker-start");
                        if (_options.startDate) {
                            $input[0].dateValue = _options.startDate;
                            $input[0].value = _options.format(_options.startDate);
                        }

                    } else {
                        $input.attr('data-pika-field', 'end').addClass("mkts-cmpt-datepicker-data-end mkts-cmpt-datepicker-end");
                        $input[0].dateValue = _options.endDate;
                        $input[0].value = _options.format(_options.endDate);
                    }
                    $input.attr('readonly', true);
                    $input.removeAttr('name');
                    field.hide();
                    $input.addClass('mkts-cmpt-datepicker-input');
                    field.after($input);
                    _options.fields.push($input[0]);
                }

            }


            function setDate(date, which) {
                if (which) {
                    _which = which;
                    _currentField = which === 'start' ? _options.fields[0] : _options.fields[1];
                }
                if (!date) {
                    if (_which === "start") {
                        _startDate = null;
                    } else {
                        _endDate = null;
                    }
                    if (_currentField) {
                        $(_currentField).val('');
                    }
                    return drawCalendar();
                }

                if (typeof date === 'string') {
                    date = new Date(Date.parse(date));
                }
                if (!util.isDate(date)) {
                    return;
                }
                var min = _options.minDate,
                    max = _options.maxDate,
                    result, tmp;

                if (util.isDate(min) && date < min) {
                    date = min;
                } else if (util.isDate(max) && date > max) {
                    date = max;
                }
                result = new Date(date.getTime());
                if (_which === "start") {
                    _startDate = result;
                } else if (_which === "end") {
                    _endDate = result;
                }

                util.setToStartOfDay(result);
                gotoDate(result);
                if (_currentField && util.isDate(_currentField.dateValue)) {
                    $(_currentField).val(_options.format(_currentField.dateValue));
                    if (!which) {
                        $(_currentField).trigger("change");
                    } else {
                        _currentField = null;
                    }
                }
            }

            function _onClick(e) {
                e = e || window.event;
                var target = e.target || e.srcElement,
                    $pEl = $(target);
                if (!target) {
                    return;
                }
                do {
                    if ($pEl.hasClass('mkts-cmpt-datepicker-single') || $pEl.hasClass('mkts-cmpt-datepicker-select-month-li') || $pEl.hasClass(_options.triggerClass)) {
                        return;
                    }
                }
                while ($pEl.parent().length && ($pEl = $pEl.parent()));
                if (_isVisble) {
                    hideCalendar();
                }
            }

            function _onMouseDown(e) {
                if (!_isVisble) {
                    return;
                }
                e = e || window.event;
                var target = e.target || e.srcElement;
                if (!target) {
                    return;
                }
                if (!$(target).parent().hasClass('is-disabled')) {
                    if ($(target).hasClass('mkts-cmpt-datepicker-button')) {
                        setDate(new Date($(target).data('pika-year'), $(target).data('pika-month'), $(target).data('pika-day')));
                        // If there is no end date yet, we don't close on click the calendar
                        if (typeof _callbacks.onSelectDate === 'function') {
                            _callbacks.onSelectDate({
                                component: componentName,
                                date: getDate(_which),
                                which: _which
                            });
                        };
                        if (_endDate || !_options.isMutiple) {
                            hideCalendar(100);
                        }
                        return;
                    } else if ($(target).hasClass('mkts-cmpt-datepicker-prev')) {
                        prevMonth();
                    } else if ($(target).hasClass('mkts-cmpt-datepicker-next')) {
                        nextMonth();
                    } else if ($(target).hasClass('mkts-cmpt-datepicker-year-li') && !$(target).hasClass("mkts-cmpt-datepicker-current-year")) {
                        expandMonth(target);
                    } else if ($(target).hasClass('mkts-cmpt-datepicker-today')) {
                        setDate(new Date());
                        hideCalendar(300);
                    }
                }
                if (!$(target).hasClass('mkts-cmpt-datepicker-select')) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    } else {
                        e.returnValue = false;
                        return false;
                    }
                }
            }


            function showCalendar(e) {
                var $target = $(e.target || e.srcElement);
                var field;
                if ($target.hasClass('mkts-cmpt-datepicker-input')) {
                    field = $target[0];
                } else {
                    field = $target.find('.mkts-cmpt-datepicker-input')[0];
                }
                if (_currentField != null) {
                    if (_currentField !== field) {
                        _isVisble = false;
                    } else {
                        hideCalendar();
                        return;
                    }
                }
                _currentField = field;
                show(_currentField);
            }

            function gotoDate(date) {
                var newCalendar = true;

                if (!util.isDate(date)) {
                    return;
                }
                if (_currentField) {
                    _currentField.dateValue = date;
                }
                if (_calendars) {
                    var firstVisibleDate = new Date(_calendars[0].year, _calendars[0].month, 1),
                        lastVisibleDate = new Date(_calendars[_calendars.length - 1].year, _calendars[_calendars.length - 1].month, 1),
                        visibleDate = date.getTime();
                    // get the end of the month
                    lastVisibleDate.setMonth(lastVisibleDate.getMonth() + 1);
                    lastVisibleDate.setDate(lastVisibleDate.getDate() - 1);
                    newCalendar = (visibleDate < firstVisibleDate.getTime() || lastVisibleDate.getTime() < visibleDate);
                }
                if (newCalendar) {
                    _calendars = [{
                        month: date.getMonth(),
                        year: date.getFullYear()
                    }];
                }
                adjustCalendars();
            }

            function adjustCalendars(force) {
                var calendar = _calendars[0];
                if (calendar.month < 0) {
                    calendar.year -= Math.ceil(Math.abs(calendar.month) / 12);
                    calendar.month += 12;
                }
                if (calendar.month > 11) {
                    calendar.year += Math.floor(Math.abs(calendar.month) / 12);
                    calendar.month -= 12;
                }
                drawCalendar(force);
            }

            function gotoToday() {
                gotoDate(new Date());
            }

            function gotoMonth(month) {
                if (!isNaN(month)) {
                    _calendars[0].month = parseInt(month, 10);
                    adjustCalendars();
                }
            }

            function nextMonth() {
                _calendars[0].month++;
                adjustCalendars();
            }

            function prevMonth() {
                _calendars[0].month--;
                adjustCalendars();
            }

            /**
             * change the minDate
             */
            function setMinDate(value) {
                util.setToStartOfDay(value);
                _options.minDate = value;
                _options.minYear = value.getFullYear();
                _options.minMonth = value.getMonth();
            }

            /**
             * change the maxDate
             */
            function setMaxDate(value) {
                _options.maxDate = value;
            }

            function isVisible() {
                return _isVisble;
            }

            function setCallback(fn, which) {
                which = which || 'onSelect';
                _options[which] = fn;
            }

            function show(field) {
                var fieldDate, ref;
                if (!field) {
                    field = _options.fields[0];
                }
                if (!_isVisble) {
                    $(_calenderEl).removeClass('is-hidden');
                    _isVisble = true;
                    _which = $(field).hasClass('mkts-cmpt-datepicker-start') ? "start" : "end";
                    fieldDate = field.dateValue;
                    if (util.isDate(fieldDate)) {
                        gotoDate(fieldDate);
                    } else {
                        // If field is pika-out we show the start date if it's present
                        if ($(field).hasClass('mkts-cmpt-datepicker-end') && _startDate != null) {
                            gotoDate(_startDate);
                        } else {
                            gotoDate(new Date());
                        }
                    }
                    adjustPosition(field);
                    if (typeof _callbacks['onOpen'] === 'function') {
                        _callbacks['onOpen']({
                            component: componentName,
                            field: field
                        });
                    }
                }
            }


            function drawCalendar(force) {

                if (!_isVisble && !force) {
                    return;
                }
                var minYear = _options.minYear,
                    maxYear = _options.maxYear,
                    minMonth = _options.minMonth,
                    maxMonth = _options.maxMonth;


                if (_year <= minYear) {
                    _year = minYear;
                    if (!isNaN(minMonth) && _month < minMonth) {
                        _month = minMonth;
                    }
                }
                if (_year >= maxYear) {
                    _year = maxYear;
                    if (!isNaN(maxMonth) && _month > maxMonth) {
                        _month = maxMonth;
                    }
                }

                renderTitle(_calendars[0].year, _calendars[0].month);
                renderBody(_calendars[0].year, _calendars[0].month);

                if (typeof _callbacks['onDraw'] === 'function') {
                    setTimeout(function() {
                        _callbacks['onDraw']({
                            component: componentName
                        });
                    }, 0);
                }
            }

            function adjustPosition(field) {
                var pEl = field,
                    width = _calenderEl.offsetWidth,
                    height = _calenderEl.offsetHeight,
                    containerWidth = $container[0].innerWidth || document.documentElement.clientWidth,
                    containerHeight = $container[0].innerHeight || document.documentElement.clientHeight,
                    scrollTop = window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop,
                    left, top, clientRect;
                var offset = $(pEl).offset();
                if (_options.position.indexOf('top') > -1) {
                    top = offset.top - $(_calenderEl).outerHeight();
                } else {
                    top = offset.top + $(pEl).outerHeight();
                }
                if (_options.position.indexOf('left') > -1) {
                    left = offset.left;
                } else {
                    left = offset.left + $(pEl).outerWidth();
                }
                if ((_options.reposition && left + width > containerWidth) || _options.position.indexOf('right') > -1) {
                    if (left + $(pEl).outerWidth() > width) {
                        left = left - width + $(pEl).outerWidth();
                    }
                }
                if ((_options.reposition && top + height > containerHeight + scrollTop) || _options.position.indexOf('top') > -1) {
                    if (top - $(pEl).outerHeight() > height) {
                        top = top - height - $(pEl).outerHeight();
                    }
                }
                $(_calenderEl).offset({ left: left, top: top });
                _calenderEl.style.position = 'absolute';
            }

            function which() {
                return _which;
            }

            function hideCalendar(timeout) {
                var time = timeout ? timeout : 0;
                setTimeout(function() {
                    if (_isVisble !== false) {
                        _calenderEl.style.position = 'absolute';
                        _calenderEl.style.left = 'auto';
                        _calenderEl.style.top = 'auto';
                        $(_calenderEl).addClass('is-hidden');
                        _isVisble = false;

                        $.each(_options.fields, function(i, field) {
                            field.blur();
                        });
                        if (_isVisble !== undefined && typeof _callbacks.onClose === 'function') {
                            _callbacks.onClose({
                                component: componentName,
                                _currentField: _currentField
                            });
                        }
                        _currentField = null;
                    }
                }, timeout);
            }

            function destroy() {
                $container.off(".mkts-cmpt-datepicker-container");
                $container.empty();
                clearTimeout(_scrollTimer);
                $.each(_options.fields, function(index, field) {
                    $(document).remove(field);
                });
            }

            function bindEvents() {
                $(document).on('click.mkts-cmpt-datepicker-container', '.' + _options.triggerClass, showCalendar);
                $(document).on('mousedown.mkts-cmpt-datepicker-container', '.mkts-cmpt-datepicker-single', _onMouseDown);
                $(document).on('click.mkts-cmpt-datepicker-container', '.mkts-cmpt-datepicker-ym-btn', renderMonthYearSelection);
                $(document).on('click.mkts-cmpt-datepicker-container', '.mkts-cmpt-datepicker-select-month-li', onMonthSelected);
                $(document).on('click.mkts-cmpt-datepicker-container', _onClick);
            }

            function init() {
                container = container.jquery ? container : $(container);
                _callbacks = $.extend(true, {}, callbacks);
                _options = loadConfig(options.configuration);
                if (!$.isArray(_options.dataFields) || _options.dataFields.length === 0) {
                    _needInputs = true;
                }

                container.append(datePickerTemplate({
                    needInputs: _needInputs,
                    isMutiple: _options.isMutiple
                }));
                $container = container.find('.mkts-cmpt-datepicker-container');
                if (_needInputs) {
                    if (_options.isMutiple) {
                        _options.dataFields = [$container.find(".mkts-cmpt-datepicker-start-input"), $container.find(".mkts-cmpt-datepicker-end-input")];
                    } else {
                        _options.dataFields = [$container.find(".mkts-cmpt-datepicker-start-input")];
                    }
                }
                createFields(_options.dataFields);
                _calenderEl = $container.find('.mkts-cmpt-datepicker-single')[0];
                $.each(_options.fields, function(index, field) {
                    var tmp = new Date(Date.parse(field.dateValue));
                    if (util.isDate(tmp)) {
                        util.setToStartOfDay(tmp);
                        if (index > 0) {
                            _endDate = tmp;
                        } else {
                            _startDate = tmp;
                        }
                        _currentField = field;
                        gotoDate(tmp);
                    } else {
                        _currentField = field;
                        gotoDate(new Date());
                    }
                });
            }


            init();
            bindEvents();
            hideCalendar();
            if (typeof _callbacks.onLoad === "function") {
                _callbacks.onLoad({
                    component: componentName
                });
            }
            var datePicker = function() {}
            datePicker.showCalendar = showCalendar;
            datePicker.setMinDate = setMinDate;
            datePicker.setMaxDate = setMaxDate;
            datePicker.setDate = function(dateObj) {
                setDate(dateObj.date, dateObj.which);
            };
            datePicker.destroy = destroy;
            return datePicker;
        };

    });