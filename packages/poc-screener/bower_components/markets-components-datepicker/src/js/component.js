define(['./datepicker'], function (datepicker) {
	'use strict';

	function init(container, component){
		var instance = new datepicker(container, component, component.callbacks);
		//here, we need add code to initlize the component
		return instance;
	}

	return {
		init: init,
		authorized:true,
        properties: {
			needTrack: true,
            setter: {
				'calendar':'showCalendar',
				'minDate':'setMinDate',
				'maxDate':'setMaxDate',
				'date':'setDate'
			},
			getter: {},
			event:{
				'selectDate':'onSelectDate',
				'load': 'onLoad'
			}
        }
	};
});