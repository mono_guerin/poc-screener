# markets-component-datepicker
markets-component-datepicker

<b>

## Install
#### `$ bower install  markets-components-datepicker --save `


<p style="font-weight:normal">Please add .bowerrc file with following content.</p>

    {
	    ...
	    "registry": {
		    "search": [
			    "http://bower.morningstar.com",
			    "https://bower.herokuapp.com"
		    ]
	    }
    }

</b>

## Usage
The component require a authentication process as below.

1. Email to <a href="mailto:marketsdevelopment@morningstar.com">markets development team</a> to get instid and account. if you already have these, please skip this step.
2. Use the account to do authentication.
#### Authentication

You can invoke below web service provided by Markets team in your server side. And you could parse the response JSON to get the session ID. This id will be used when you initializing this component.

***Note: There is a IP white list with your servers. Please provide them to Marktes team before you invoke the web service.***

**Web service**

    Production URL: https://quotespeed.morningstar.com/service/auth
	Staging URL: https://markets-uat.morningstar.com/service/auth

**Request parameters**

<table cellpadding=0 cellspacing=0 border="1">
<thead>
<tr class="header">
<th>key</th>
<th>Required</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td>instid</td>
<td>Yes</td>
<td>An unique ID assigned to you by Morningstar markets.</td>
</tr>
<tr>
<td>instuid</td>
<td>Yes</td>
<td>Unique identifier for the user provided by client side. We suggest to set is as same as email.</td>
</tr>
<tr>
<td>email</td>
<td>Yes</td>
<td>Unique identifier for the user provided by client side.</td>
</tr>
<tr>
<td>group</td>
<td>Yes</td>
<td>User group is provided by Morningstar.</td>
</tr>
<tr>
<td>e</td>
<td>Yes (internal only)</td>
<td>RTD credential, it will be used to pass the encrypted RTD account string. For how to get this string, please reference following links. <a gref="https://mswiki.morningstar.com/pages/viewpage.action?title=Entitlement+Integration&spaceKey=SWPAAS">https://mswiki.morningstar.com/pages/viewpage.action?title=Entitlement+Integration&spaceKey=SWPAAS</td>
</tr>
</tbody>
</table>

**Response JSON**


    {
	    "status": {
			"errorCode":"0",
			"subErrorCode":"",
			"errorMsg":"Sucessful"
		},
		"data":"95818CD5B0B2BC0E2F7B2577235F40E7",
		"attachment":{
			"SessionID":"95818CD5B0B2BC0E2F7B2577235F40E7",
			"sessionTimeout":1592
		}
    }

## Browser compatibility
Markets components are using browser native number formatting function (Intl) which is a ES6(ES2015) feature. It may not be supported in some lower browsers such as IE 10. If you want your product to compatible with these browsers, please add below polyfill to your page.

    <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Intl.~locale.en,Intl.~locale.fr"></script>

## Sample codes
    
    <div class="container"></div>
    
    <script>
        var params = {
            element: $('.container'),
            initialConfiguration: {
                instid: 'xxx',
                env: 'stage', //change to use production when going to production
                sessionKey: 'xxx',
                component: {
                    configuration: {
                    },
                    callbacks: {
                    }
            }
            }
        }
        var component = morningstar.components['markets-components-datepicker'].createComponent(params);
    </script>
	