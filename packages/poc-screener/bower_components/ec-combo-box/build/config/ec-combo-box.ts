// startref
/// <reference path='../../.references.ts'/>
// endref
module EcComboBox {
    export enum _Type { ecComboBox }

    /**
     * @title ComboBox
     */
    export interface EcComboBox {
        /** Component type.
         * @default ecComboBox
         * @options.hidden true
         * @type string
         * @readonly true
         */
        type: _Type;

        settings: Settings;

        labels: Labels;
    }
}
