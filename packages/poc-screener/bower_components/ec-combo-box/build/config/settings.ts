// startref
/// <reference path='../../.references.ts'/>
// endref
module EcComboBox {
    export interface Settings extends AsterixNg.ComponentSettings {
        _model: {
            listItems: AsterixCore.DataSourceSettings;
        }

        _parameters: {
            disabled: AsterixNg.Parameter;
            displaySettings: AsterixNg.Parameter;
            groupByField: AsterixNg.Parameter;
            runtimeSelectLabel: AsterixNg.Parameter;
        }

        _templates: Templates;

        /* Settings for combo-box select2 plugin */
        displaySettings: any;

        /**
         * Set the value to true, if you want to disable select
         * @default false
         */
        disabled?: boolean;

        /**
         * Key field in Listitems for Select
         * Default value of keyField is set as key
         * @default key
         */
        keyField: string;

        /** The namespaceClass
         * @default ec-combo-box
         * @type string
         */
        namespaceClass: string;

        /** runtimeSelectLabel, load labels statically or dynamically
         * @default true
         */
        runtimeSelectLabel: boolean;

        /**
         * Sets the value of the aria-labelledby attribute for screen readers.
         * Use label to use the label element or option to use the first option in the list.
         * @type string
         * @default label
         */
        ariaLabelledBy: AriaLabelledBy;

        /**
         * Value label in Listitems for select.
         * Default value of valueField is set as value.
         * @default value
         */
        valueField: string;

        /**
         * groupByField, indicated the grouping field
         * @default false
         */
        groupByField: any;
    }

    export enum AriaLabelledBy {
        label,
        option
    }

    export interface Templates {
        /** The main template
         * @format html
         * @default <div class=\"{{ ::namespaceClass('__wrapper') }}\">\r\n    <label id=\"{{ ::namespaceId('label') }}\" class=\"{{ ::namespaceClass('__label') }}\" for=\"{{ ::namespaceId('select') }}\">{{labels.get(\"caption\")}}<\/label>\r\n    <select data-ng-model=\"ngModel\"\r\n            data-ng-disabled=\"parameters.disabled\" id=\"{{ ::namespaceId('select') }}\"\r\n            class=\"{{ ::namespaceClass('__select') }}\"\r\n            data-ng-options=\"parameters.runtimeSelectLabel && labels.contains(listItem[settings.get('valueField')]) ?\r\n             labels.get(listItem[settings.get('valueField')]) : listItem[settings.get('valueField')]\r\n            for listItem in model.listItems track by listItem[settings.get('keyField')]\">\r\n        <option data-ng-if=\"labels.contains('placeholder') || (ngConfig && ngConfig.placeholder)\" value=\"\"><\/option>\r\n    <\/select>\r\n<\/div>\r\n
         */
        main: string;

         /** The multi-select combo-box template
         * @format html
         * @default <div class=\"{{ ::namespaceClass('__wrapper') }}\">\r\n    <label id=\"{{ ::namespaceId('label') }}\" class=\"{{ ::namespaceClass('__label') }}\" for=\"{{ ::namespaceId('select') }}\">{{labels.get(\"caption\")}}<\/label>\r\n    <select data-ng-model=\"ngModel\"\r\n            data-ng-disabled=\"parameters.disabled\" id=\"{{ ::namespaceId('select') }}\"\r\n            class=\"{{ ::namespaceClass('__select') }}\"\r\n            data-ng-options=\"parameters.runtimeSelectLabel && labels.contains(listItem[settings.get('valueField')]) ?\r\n             labels.get(listItem[settings.get('valueField')]) : listItem[settings.get('valueField')]\r\n            for listItem in model.listItems track by listItem[settings.get('keyField')]\"\r\n            multiple=\"multiple\">\r\n    <\/select>\r\n<\/div>\r\n
         */
        multiSelect: string;

        /** The grouped-multi-select combo-box template
        * @format html
        * @default <div class=\"{{ ::namespaceClass('__wrapper') }}\">\r\n    <label data-ng-if=\"labels.contains('caption')\" class=\"{{ ::namespaceClass('__label') }}\" for=\"{{ ::namespaceId() }}\">{{labels.get(\"caption\")}}<\/label>\r\n    <select data-ng-model=\"ngModel\"\r\n            data-ng-disabled=\"parameters.disabled\" id=\"{{ ::namespaceId() }}\"\r\n            class=\"{{ ::namespaceClass('__select') }}\"\r\n            data-ng-options=\"parameters.runtimeSelectLabel && labels.contains(listItem[settings.get('valueField')]) ?\r\n             labels.get(listItem[settings.get('valueField')]) : listItem[settings.get('valueField')] group by listItem[parameters.groupByField]\r\n            for listItem in model.listItems track by listItem[settings.get('keyField')]\"\r\n            multiple=\"multiple\">\r\n    <\/select>\r\n<\/div>\r\n
        */
       groupedMultiSelect: string;

       /** The grouped-single-select combo-box template
       * @format html
       * @default <div class=\"{{ ::namespaceClass('__wrapper') }}\">\r\n    <label data-ng-if=\"labels.contains('caption')\" class=\"{{ ::namespaceClass('__label') }}\" for=\"{{ ::namespaceId() }}\">{{labels.get(\"caption\")}}<\/label>\r\n    <select data-ng-model=\"ngModel\"\r\n            data-ng-disabled=\"parameters.disabled\" id=\"{{ ::namespaceId() }}\"\r\n            class=\"{{ ::namespaceClass('__select') }}\"\r\n            data-ng-options=\"parameters.runtimeSelectLabel && labels.contains(listItem[settings.get('valueField')]) ?\r\n             labels.get(listItem[settings.get('valueField')]) : listItem[settings.get('valueField')] group by listItem[parameters.groupByField]\r\n            for listItem in model.listItems track by listItem[settings.get('keyField')]\">\r\n        <option data-ng-if=\"labels.contains('placeholder') || (ngConfig && ngConfig.placeholder)\" value=\"\"><\/option>\r\n    <\/select>\r\n<\/div>\r\n
       */
      groupedSingleSelect: string;
    }
}
