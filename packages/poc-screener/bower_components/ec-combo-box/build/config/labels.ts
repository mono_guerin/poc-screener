// startref
/// <reference path='../../.references.ts'/>
// endref
module EcComboBox {
    export interface Labels {
        caption: AsterixCore.Label;
        placeholder: AsterixCore.Label;
    }
}