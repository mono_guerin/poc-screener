define(['morningstar'], function(morningstar) {
    'use strict';
    var QSAPI = morningstar.components["markets-components-core"];
    return QSAPI.components.register('svgchart', require('./js/svgchart.js'));
});