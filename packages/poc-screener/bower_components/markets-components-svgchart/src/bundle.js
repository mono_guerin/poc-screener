define([], function() {
    require('d3');
    require('moment');
    require('handlebars/runtime');
    require('es5-shim');
    require('jquery');
    require('asterix-core');
    require('markets-components-core');
    require('markets-components-datepicker');
    require('markets-components-chartdatamanager');
    require('../build/markets-components-svgchart.js');
});
