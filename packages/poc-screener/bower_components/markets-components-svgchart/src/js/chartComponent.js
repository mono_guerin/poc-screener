define(['./manager/layout/layoutManager', "./manager/chartConfig/chartConfigManager", './manager/data/dataAdapter', './manager/event/eventManager'], function(layoutManagerModel, chartConfigManagerModel, dataAdapterModel, eventManagerModel) {
    'use strict';
    return function() {
        var managers = {};
        managers.eventManager = eventManagerModel(managers);
        managers.dataAdapter = dataAdapterModel(managers);
        managers.chartConfigManager = chartConfigManagerModel(managers);
        managers.layoutManager = layoutManagerModel(managers);
        var chartConfigManager = managers.chartConfigManager;
        var layoutManager = managers.layoutManager;
        return {
            init: function(extendConfig, callbacks) {
                chartConfigManager.createConfig(extendConfig, layoutManager.createView);
            },
            getSaveConfig: chartConfigManager.getSaveConfig,
            changeInterval: chartConfigManager.changeInterval,
            destroy: chartConfigManager.destroyChart,
            changeMainTicker: chartConfigManager.changeMainTicker,
            addCompareTickers: chartConfigManager.addCompareTickers,
            removeCompareTickers: chartConfigManager.removeCompareTickers,
            changeSize: layoutManager.changeSize,
            hidePopover: layoutManager.hidePopover,
            changeGrowthBaseValue: chartConfigManager.changeGrowthBaseValue,
            updateCompareTickers: chartConfigManager.updateCompareTickers
        };
    };
});