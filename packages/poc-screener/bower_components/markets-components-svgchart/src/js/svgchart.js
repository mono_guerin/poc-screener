define(['./core/core', './chartComponent', '../js/manager/chartConfig/config.js'], function(core, chartComponent, config) {
    "use strict";
    var $ = core.$;
    var clone = core.Util.clone;


    function init(container, component, actionHandler) {
        var configuration = component.configuration || {},
            chartDataType = '';
        if (configuration.charts && configuration.charts.mainChart && configuration.charts.mainChart.dataType) {
            chartDataType = configuration.charts.mainChart.dataType;
        }
        if (configuration.investment) {
            configuration.mainTicker = configuration.investment;
        }
        configuration.container = container;
        configuration.actionHandler = actionHandler;
        $.extend(configuration, component.callbacks);
        $.extend(configuration, { callbacks: component.callbacks });
        var instance = chartComponent();
        // Used to control the init step, we can rebuild configuration before component init.
        // will be used in the API component
        if (!configuration.skipInit) {
            instance.init(configuration);
        }
        instance.setInvestment = function(investment) {
            instance.changeMainTicker(clone(investment));
        };
        return instance;
    }

    return {
        init: init,
        properties: {
            needTrack: true,
            setter: {
                'parentSize': 'changeSize',
                'investment': 'setInvestment',
                'growthBaseValue': 'changeGrowthBaseValue',
                'compareTickers': 'updateCompareTickers'
            },
            getter: {
                'configuration': 'getSaveConfig'
            },
            events: {
                'load': 'onLoad',
                'mainTickerDrawEnd': 'onMainTickerDrawEnd',
                'close': 'onClose',
                'click': 'triggerClickCallback',
                'addCompareTickers': 'onAddCompareTickers',
                'removeCompareTickers': 'onRemoveCompareTickers',
                'heightChange': 'onHeightChange',
                'tickerRemoveBtnClicked': 'onTickerRemoveBtnClicked',
                'configChange': 'onConfigChange',
                'fullyRendered': 'onFullyRendered',
                'error': 'onError',
                'beforeZoomIn': 'onbeforeZoomIn',
                'dateRangeChange': 'onDateRangeChange',
                'frequencyChange': 'onChangeFrequency'
            }
        }
    };
});