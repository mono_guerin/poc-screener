﻿define(["../core/core"], function(core) {
    "use strict";
    var $ = core.$;

    /**
     * Base View Model
     *
     * It's the base of all view of the components.
     * It defines common interface that all views should have.
     *
     * @constructor
     */
    var baseView = function(config) {
        this.config = config;
        this.$el = $(this.config.container);
        this._init(config);
    }

    baseView.prototype = {
        _init: function(config) {
            $.extend(true, this.config, config);
            this._renderView();
        },
        _renderView: function() {},
        show: function() {
            this.$el.show();
            return this;
        },
        hide: function() {
            this.$el.hide();
            return this;
        },
        remove: function() {
            this.$el.remove();
        }
    };

    return baseView;
});