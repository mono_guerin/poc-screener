define(["../../../core/core"], function (core) {
    "use strict";
    var $ = core.$;
    var d3 = core.d3;
    function barSeries() {
        var xScale, yScale;
        var tickSize = 5;
        var width, viewHeight, halfViewHeight;
        var baseValue;
        var xValueFunc, yValueFunc, heightFunc;
        var isUp, isDown;
        var xAttributeName, yAttributeName = "volume";
        var dataIndex = 0;
        var barColor;
        var distinguish = false;
        var getElementsMapping;

        var drawRectangle = function (bar) {
            var rect = bar.select("rect");
            if (rect.empty()) {
                rect = bar.append('rect');
            }

            rect.attr("x", xValueFunc)
                .attr("y", yValueFunc)
                .attr("width", tickSize)
                .attr("height", heightFunc);
        };

        /*var zoom = d3.behavior.zoom()
            .x(xScale)
            .scaleExtent([1, 50])
            .on('zoom', zoomed)
            .on('zoomend', zoomend);*/

        var bar = function (selection) {
            var series, bar;
            xValueFunc = function (d) {
                return xScale(d[xAttributeName]) - tickSize / 2;
            };

            if ($.isNumeric(baseValue)) {
                isUp = function (d) {
                    return d[yAttributeName] > baseValue;
                };

                isDown = function (d) {
                    return !isUp(d);
                };

                var baseY = yScale(baseValue);
                yValueFunc = function (d) {
                    var yValue;
                    if (isUp(d)) {
                        yValue = yScale(d[yAttributeName]);
                    }
                    else {
                        yValue = baseY;
                    }

                    return yValue;
                };

                heightFunc = function (d) {
                    var height;
                    if (isUp(d)) {
                        height = baseY - yScale(d[yAttributeName]);
                    }
                    else {
                        height = yScale(d[yAttributeName]) - baseY;
                    }

                    return height;
                };
            }
            else {
                isUp = function (d) {
                    return d.upFlag;
                };

                isDown = function (d) {
                    return d.upFlag;
                };

                yValueFunc = function (d) {
                    return yScale(d[yAttributeName]);
                };

                heightFunc = function (d) {
                    return viewHeight - yScale(d[yAttributeName]);
                };
            }

            var isDown = function (d) {
                return !isUp(d);
            };

            selection.each(function (dataArr) {
                if (!xAttributeName) {
                    xAttributeName = "_x";
                    dataArr.forEach(function (dataItem, index) {
                        dataItem[xAttributeName] = index;
                    });
                }

                var d3_this = d3.select(this);
                var outerElem = d3_this.select(".bar-series");
                if (outerElem.empty()) {
                    outerElem = d3_this.append("g").classed("bar-series", true);
                }

                var barGraphs = outerElem.selectAll(".bar")[0];
                var graphIndex = 0;
                var indexElementsMapping = {};
                dataArr.forEach(function (dataItem, dataIndex) {
                    if (!isNaN(dataItem[yAttributeName]) && (dataItem[yAttributeName] !== 0)) {
                        bar = barGraphs[graphIndex++];
                        if (bar) {
                            bar = d3.select(bar);
                        } else {
                            bar = outerElem.append("g").classed("bar", true);
                        }

                        bar.data([dataItem]);
                        if (distinguish || $.isNumeric(baseValue)) {
                            bar.classed({ "up": isUp, "down": isDown });
                        } else {
                            bar.classed({ "up": false, "down": false });
                        }

                        drawRectangle(bar);
                        indexElementsMapping[dataIndex] = bar;
                    }
                });

                barGraphs = outerElem.selectAll(".bar")[0];
                if (barGraphs.length > graphIndex) {
                    var nativeParentDom = outerElem[0][0];
                    for (var i = graphIndex; i < barGraphs.length; i++) {
                        nativeParentDom.removeChild(barGraphs[i]);
                    }
                }

                getElementsMapping && getElementsMapping(indexElementsMapping);
            });

            barColor && selection.selectAll("rect").style({ 'fill': barColor });
        };

        //bar.zoom = function (selection) {
        //    var xDomain = xScale.domain();
        //    var xRange = xScale.range();
        //    var translate = zoom.translate()[0];
        //}

        bar.xScale = function (value) {
            if (!arguments.length) {
                return xScale;
            }
            xScale = value;
            return bar;
        };

        bar.yScale = function (value) {
            if (!arguments.length) {
                return yScale;
            }
            yScale = value;
            return bar;
        };

        bar.tickSize = function (value) {
            if (!arguments.length) {
                return tickSize;
            }
            tickSize = value;
            return bar;
        };

        bar.width = function (value) {
            if (!arguments.length) {
                return width;
            }
            width = value;
            return bar;
        };

        bar.height = function (value) {
            if (!arguments.length) {
                return viewHeight;
            }
            viewHeight = value;
            halfViewHeight = viewHeight / 2;
            return bar;
        };

        bar.baseValue = function (value) {
            if (!arguments.length) {
                return baseValue;
            }
            baseValue = +value;
            return bar;
        };

        bar.color = function (value) {
            if (!arguments.length) {
                return barColor;
            }
            barColor = value;
            return bar;
        };

        bar.xAttributeName = function (value) {
            if (!arguments.length) {
                return xAttributeName;
            }
            xAttributeName = value;
            return bar;
        };

        bar.yAttributeName = function (value) {
            if (!arguments.length) {
                return yAttributeName;
            }
            yAttributeName = value;
            return bar;
        };

        bar.distinguish = function (value) {
            if (!arguments.length) {
                return distinguish;
            }
            distinguish = value;
            return bar;
        };

        bar.getElementsMapping = function (value) {
            if (!arguments.length) {
                return getElementsMapping;
            }
            getElementsMapping = value;
            return bar;
        };

        return bar;
    }
    return barSeries;
});
