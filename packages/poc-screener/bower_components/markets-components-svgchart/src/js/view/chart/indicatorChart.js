define(["./basicModule/commonPlotter", "../../manager/chartConfig/graphTypes", "../../core/core", "./baseChart", "../../util/util",
        "./basicModule/barSeries", "../../manager/data/request.js"
    ],
    function(commonPlotter, graphTypes, core, baseChartModel, util, barSeries, request) {
        "use strict";
        var $ = core.$;
        var d3 = core.d3;

        /**
         * indicator chart Model
         *
         */
        var model = function(managers) {
            var eventManager = managers.eventManager;
            var chartConfigManager = managers.chartConfigManager;
            var baseChart = baseChartModel(managers);
            var dataAdapter = managers.dataAdapter;
            var rootCfg;

            var indicatorChartExtend = {
                _init: function(config) {
                    this._innerClass = innerClass;
                    rootCfg = chartConfigManager.getConfig();
                    this.langData = chartConfigManager.getLabels('markets-components-svgchart', config.lang);
                    this._chartTypes = chartConfigManager.IndicatorChartTypes;
                    this._bindEvents();
                    this._decideMinMaxAttrbute();
                    if (rootCfg.indicators && rootCfg.indicators.length > 0) {
                        this._updateViewData();
                    }
                },
                _reviseData: function(data) {
                    data.forEach(function(d) {
                        d.x = d.index;
                        d.y = d.value;
                    });
                    return data;
                },
                _bindEvents: function() {
                    var eventTypes = eventManager.EventTypes
                    eventManager.bind(eventTypes.UpdateConfig, this._updateConfig, this);
                    eventManager.bind(eventTypes.ChangeIndicators, this._chartConfigChanged, this);
                    eventManager.bind(eventTypes.ChangeFundamental, this._chartConfigChanged, this);
                    eventManager.bind(eventTypes.ChangeXAxisDomain, this._changeLastDateRange, this);
                    eventManager.bind(eventTypes.ToggleTrackballIndicators, this._toggleTrackball, this);
                    eventManager.bind(eventTypes.DestroyChart, this._processDestroyChart, this);
                    eventManager.bind(eventTypes.ChangeStartDate, this._chartConfigChanged, this);
                },
                _bindAfterOpenIndicatorEvents: function() {
                    if (!this._isInited) {
                        var eventTypes = eventManager.EventTypes
                        eventManager.bind(eventTypes.ChangeXAxisDomain, this._changeXAxisScaleDomain, this);
                        eventManager.bind(eventTypes.UpdateCompareTickers, this._chartConfigChanged, this);
                        eventManager.bind(eventTypes.LayoutChange, this._changeLayout, this);
                        eventManager.bind(eventTypes.ChangeYAxis, this._changeYaxis, this);
                        eventManager.bind(eventTypes.FetchPreloadData, this._fetchPreloadData, this);
                        eventManager.bind(eventTypes.ChangeIntervalFrequency, this._changDataRange, this);
                        eventManager.bind(eventTypes.ChangeEvents, this._chartConfigChanged, this);
                        eventManager.bind(eventTypes.DrawNewData, this._chartConfigChanged, this);
                        eventManager.bind(eventTypes.ShowTrackball, this._showTrackball, this);
                        eventManager.bind(eventTypes.ChangeChartType, this._decideMinMaxAttrbute, this);
                        this._isInited = true;
                    }
                },
                _toggleTrackball: function(trackballData) {
                    var cfg = this.config;
                    var runningIndex = 0,
                        chartTypeName = '';
                    var view = this;
                    trackballData.forEach(function(d) {
                        if (!d.data) {
                            return;
                        }
                        var newChartTypeName = d.name;
                        newChartTypeName = newChartTypeName.replace(' ', '');
                        if (chartTypeName !== newChartTypeName) {
                            runningIndex = 0;
                            chartTypeName = newChartTypeName;
                        }
                        var tickerValueElement = view.$el.find('.' + innerClass.indicatorsValue + chartTypeName + runningIndex);
                        if (tickerValueElement) {
                            tickerValueElement.css('color', d.color);
                            tickerValueElement.text((d.data && !isNaN(d.data.y)) ? util.formatNotZeroNum(d.data.y, 2) : '--');
                        }
                        runningIndex += 1;
                    });
                },
                _updateConfig: function() {
                    this.config = chartConfigManager.getChartConfig(chartConfigManager.ChartTypes.IndicatorChart);
                },
                _chartConfigChanged: function() {
                    this._updateViewData();
                },
                _changeYaxis: function(position) {
                    if (this.config.yAxis.position == "outer") {
                        var translate = this._getGElemTranslate();
                        this._outerGElem && this._outerGElem.attr("transform", translate);
                    }
                    this._chartConfigChanged();
                },
                _updateViewData: function(adjustedDateRange) {
                    var view = this;
                    var cfg = this.config;
                    view._showLoading();
                    view._loading = true;
                    view._hideDataNotAvailable();
                    this._cancelNotRunedPreload();
                    chartConfigManager.fetchMainTickerData(function(res, preloadInfo) {
                        view._clearAllData();
                        view._clearView();
                        if (res.status().errorCode === "0") {
                            view._preloadInfo = preloadInfo;
                            var resDataArr = res.data();
                            var mainTickerData = resDataArr[0];
                            view._indicatorsData = mainTickerData.Indicators;
                            view.mainTickerObject = mainTickerData.tickerObject;
                            view._lastClosePrice = mainTickerData.Price.spos;
                            view._data = view._reviseData(mainTickerData.Price.data);
                            view._eventsData = mainTickerData.Events;
                            if (chartConfigManager.getMainDataType() === chartConfigManager.MainDataTypes.DividendEffect) {
                                //removing indicators that are shown in main chart
                                var indicators = null;
                                for (var indicatorName in view._indicatorsData) {
                                    if (cfg.indicatorProperties.hasOwnProperty(indicatorName)) {
                                        indicators = indicators || {};
                                        indicators[indicatorName] = view._indicatorsData[indicatorName];
                                    }
                                }
                                view._indicatorsData = indicators ? indicators : null;
                                var fundamentalType = dataAdapter.Fundamental.MReturnIndex;
                                if (mainTickerData.Fundamentals && mainTickerData.Fundamentals[fundamentalType] &&
                                    mainTickerData.Fundamentals[fundamentalType].data) {
                                    view._dividendEffectData = view._reviseDividendEffectData(mainTickerData.Fundamentals[fundamentalType].data);
                                }
                            } else {
                                view._getCompareData(res.data());
                                view._createDummyData(view._compareData);
                                preloadInfo.loadedCompareData = view._compareData;
                            }
                            view._rebaseCompareData();
                            view._rebaseFundamentalData();
                            view._changeChartGraph();
                            if (preloadInfo.intervalType !== chartConfigManager.IntervalTypes.Max && !chartConfigManager.getConfig().useEUTimeSeriesData) {
                                setTimeout(function() {
                                    view._preloading = true;
                                    view._loadingData = true;
                                    view._fetchPreloadData();
                                }, view.config.preloadDelay);
                            }
                        }

                        view._hideLoading();
                        view._loading = false;
                    }, adjustedDateRange);
                },
                _showDataNotAvailable: function(type) {
                    if (!this._svg) {
                        return;
                    }
                    this._clearGElem();
                    var elem = this._svg.append('g').append('text').attr("transform", "translate(" + this._validWidth / 2 + "," + this.config.height / 2 + ")");
                    var message = this.langData['datanotavailable'];
                    if (type == "changeToDailyFreq") {
                        message = this.langData['changetodailyfrequency'];
                    }
                    var textWidth = this._getTextWidthHeight(message).width;
                    //text need to be wraped when too long
                    if (textWidth > (this._validWidth - 20)) {
                        var mesArr = message.split('|');
                        for (var i in mesArr) {
                            elem.append('tspan')
                                .attr('class', 'dataNotAvailable')
                                .attr('dy', i * 1.5 + 'em')
                                .attr('x', '0')
                                .text(mesArr[i]);
                        }
                    } else {
                        message = message.replace('|', '');
                        elem.append('tspan')
                            .attr('class', 'dataNotAvailable')
                            .text(message);
                    }
                    this._$DataNotAvailableElem = elem;

                },
                _clearGElem: function(chartType) {
                    if (this._svg) {
                        this._svg.selectAll("g").remove();
                    }
                    this._clearTrackball();
                },
                _changeLastDateRange: function(newXDomainValue, lastDateRange) {
                    this._lastDateRange = lastDateRange;
                },
                _displayViewAfterChangeDomain: function() {
                    this._clearView();
                    this._changeChartGraph();
                },
                _changeChartGraph: function() {
                    //this._changeScaleDomain();
                    this._draw_Indicators();
                    eventManager.trigger(eventManager.EventTypes.HeightChanged);
                },
                _draw_Indicators: function() {
                    var indicatorsData = this._indicatorsData;
                    if (!indicatorsData) {
                        return;
                    }
                    this._caculateValidSize();
                    var view = this;
                    var cfg = this.config;
                    for (var indicatorName in indicatorsData) {
                        if (cfg.indicatorProperties.hasOwnProperty(indicatorName)) {
                            indicatorsData[indicatorName].forEach(function(s, chartIndex) {
                                var index = 0;
                                s.data.forEach(function(d) {
                                    d.x = index++;
                                    d.y = d.value;
                                });
                                if (chartIndex === 0) {
                                    view._renderView(indicatorName, s.data);
                                }
                                if (s['DailyDataPoint']) {
                                    view._showDataNotAvailable('changeToDailyFreq');
                                    return;
                                }
                                if (cfg.indicatorProperties[indicatorName].chartTypes) {
                                    var typeOfChart = cfg.indicatorProperties[indicatorName].chartTypes[chartIndex];
                                    view["_draw_" + typeOfChart + "_Indicators"](s.data, indicatorName, 'lineChart', chartIndex);
                                } else {
                                    view._draw_lineChart_Indicators(s.data, indicatorName, 'lineChart', chartIndex);
                                }
                            });
                        }
                    }
                },
                _draw_histogram_Indicators: function(data, indicatorName, chartType, chartIndex) {
                    var cfg = this.config;
                    var view = this;
                    var baseValue = 0; // MACD
                    // var baseValue = d3.min(data, function(d) {
                    //     return d.value;
                    // });
                    // baseValue = baseValue >= 0 ? 0 : baseValue;
                    var graphConfig = {
                        data: data,
                        graphCss: innerClass.indicators + " " + indicatorName + "-" + chartIndex + "-class",
                        color: this.config.indicatorProperties[indicatorName].color[chartIndex],
                        insertBeforeCss: innerClass.indicators,
                        yAttributeName: "value",
                        graphType: graphTypes.Histogram,
                        baseValue: baseValue,
                        tickSize: 1,
                        getElementsMapping: function(elementsMapping) {
                            var mapping = view._elementsMapping || [];
                            mapping.push(elementsMapping);
                            view._elementsMapping = mapping;
                        }
                    };

                    this["_elem_" + chartType] = this._commonDraw(graphConfig);
                },
                _createScale: function(indicatorName) {
                    this._xScale = d3.scale.linear()
                        .range([0, this._validWidth])
                        .domain([0, this._data.length - 1]);

                    var fixedTickValeus = this.config.indicatorProperties[indicatorName].fixedTickValeus;
                    var yScaleData;
                    if (fixedTickValeus) {
                        yScaleData = {
                            domain: [fixedTickValeus[0], fixedTickValeus[fixedTickValeus.length - 1]],
                            tickValues: fixedTickValeus
                        };
                    } else {
                        var minYValue = [];
                        var maxYValue = [];

                        this._indicatorsData[indicatorName].forEach(function(s) {
                            minYValue.push(d3.min(s.data, function(d) {
                                return d.value;
                            }));
                            maxYValue.push(d3.max(s.data, function(d) {
                                return d.value;
                            }));
                        });

                        minYValue = d3.min(minYValue);
                        maxYValue = d3.max(maxYValue);
                        isNaN(minYValue) && (minYValue = 0);
                        isNaN(maxYValue) && (maxYValue = 0);

                        yScaleData = this._calculateYScaleDomainTickValues(minYValue, maxYValue);
                    }
                    this._yTickValues = yScaleData.tickValues;
                    this._yScale = d3.scale.linear().range([this._validHeight, 0])
                        .domain(yScaleData.domain);
                },
                _setScaleDomain: function() {},
                _clearView: function() {
                    rootCfg.$componentContainer.find("." + this.config.chartClass).empty();
                    this._clearTrackball();
                },
                _changeLayout: function() {
                    this._rebaseCompareData();
                    this._clearView();
                    this._changeChartGraph();
                },
                _processDestroyChart: function() {
                    this._svg && this._svg.on('.indicatorchart', null);
                },
                _renderView: function(indicatorName) {
                    //when open a save config, maybe we need draw indicators.
                    var view = this;
                    this._renderHeader(indicatorName);
                    this._createScale(indicatorName);
                    this._createSvgWrapper(indicatorName);
                    this._createCommonPlotter();
                    this._renderContainedViews();
                    this._renderFooter(indicatorName);
                    this._bindAfterOpenIndicatorEvents();
                    //when mouse move in the indicatorchart, we will show crosshairs, waiting to get main chart data
                    this._svg.on('mousemove.indicatorchart', function() {
                        view._mousemove(d3.event, d3.mouse(this), view._headerHeight);
                    });
                },
                _renderContainedViews: function() {
                    this._drawXAxis();
                    this._drawYAxis();
                    this._drawGridLines();
                },
                _createCommonPlotter: function() {
                    var graphConfig = this._defaultGraphConfig = {
                        containerGElem: this._outerGElem,
                        yAxisContainerGElem: this._yAxisOuterGElem,
                        xScale: this._xScale,
                        yScale: this._yScale,
                        graphType: graphTypes.Histogram,
                        width: this._validWidth,
                        height: this._validHeight,
                        tickSize: this.config.indicatorBarWidth
                    };
                    this._getDefaultGraphConfig = function() {
                        return this._defaultGraphConfig;
                    };
                    this._commonPlotter = new commonPlotter(graphConfig);
                },
                _renderHeader: function(indicatorName) {
                    var cfg = this.config,
                        margin = this.config.margin;
                    var tickerInfoContainer = this._headerContainer = d3.select(cfg.container)
                        .append("div").classed(innerClass.chartHeader, true)
                        .classed(indicatorName + '-class', true);
                    tickerInfoContainer.append("span").classed(innerClass.chartLegend, true)
                        .style("background", "#2F4880");
                    tickerInfoContainer.append("span")
                        .style("margin-left", margin.left + "px")
                        .style("width", (this.config.width - margin.left - margin.right) + "px")
                        .classed(innerClass.chartLegendText, true)
                        .text(cfg.chartLegendText);
                    this._headerContainer.append("span").classed(innerClass.compare, true);
                    this._headerContainer.append("span").classed(innerClass.indicators, true);
                    this._appendHeaderForIndicators(indicatorName);
                },
                _renderFooter: function(indicatorName) {
                    var cfg = this.config,
                        margin = this.config.margin;
                    d3.select(cfg.container)
                        .append("div").classed(innerClass.chartFooter, true)
                        .classed(indicatorName + '-class', true)
                        .style("margin-left", margin.left + "px")
                        .style("width", (this.config.width - margin.left - margin.right) + "px")
                }
            };

            return util.extendClass(baseChart, indicatorChartExtend);
        };

        var innerClass = {
            gridLines: "indicatorChart-gridLines",
            indicatorChart: "chart-indicator",
            chartHeader: "indicatorChart-header",
            chartLegend: "indicatorChart-legend",
            chartLegendText: "indicatorChart-legend-text",
            chartBody: 'indicatorChart-body',
            chartFooter: "indicatorChart-footer",
            indicators: "indicators",
            indicatorsLegend: "indicators-legend",
            indicatorsValue: "indicatorsValue-",
            indicatorsLegendText: "indicators-legend-text",
            closeButton: "icon chart close-button",
            yAxis: "indicatorChart-yAxis"
        };

        return model;
    });