define(["../../../core/core", "../../../manager/chartConfig/graphTypes"], function (core, graphTypes) {
    "use strict";
    var d3 = core.d3;

    return function () {
        var xScale, yScale;
        var tickSize = 5;
        var barType;
        var isHLC = false;
        var xAttributeName, yAttributeName = "close";
        var getElementsMapping;
        var isUpDay = function (d) {
            return d.close > d.open;
        };

        var isDownDay = function (d) {
            return !isUpDay(d);
        };

        var line = d3.svg.line()
            .x(function (d) { return d.x; })
            .y(function (d) { return d.y; });

        var highLowLines = function (bar) {
            bar.append("path")
                .classed("high-low-line", true)
                .attr("d", function (d) {
                    var x = xScale(d[xAttributeName]);
                    return line([
                        { x: x, y: yScale(d.high) },
                        { x: x, y: yScale(d.low) }
                    ]);
                });
        };

        var openTicks = function (bar) {
            bar.append("path").classed("open-tick", true)
                .attr("d", function (d) {
                    var x = xScale(d[xAttributeName]);
                    var x1, x2;
                    x1 = x - tickSize;
                    x2 = x;
                    var y = yScale(d.open);
                    return line([
                        { x: x1, y: y },
                        { x: x2, y: y }
                    ]);
                });
        };

        var closeTicks = function (bar) {
            var close = bar.append("path").classed("close-tick", true)
                .attr("d", function (d) {
                    var x = xScale(d[xAttributeName]);
                    var x1, x2;
                    x1 = x;
                    x2 = x + tickSize;
                    var y = yScale(d.close);
                    return line([
                        { x: x1, y: y },
                        { x: x2, y: y }
                    ]);
                });
        }

        var ohlc = function (selection) {
            var series, bar;

            selection.each(function (dataArr) {
                if (!xAttributeName) {
                    xAttributeName = "_x";
                    dataArr.forEach(function (dataItem,index) {
                        dataItem[xAttributeName] = index;
                    });
                }
                var outerElem = d3.select(this).append("g").classed("ohlc-series", true);
                var indexElementsMapping = {};
                dataArr.forEach(function (dataItem, dataIndex) {
                    if (!isNaN(dataItem[yAttributeName])) {
                        bar = outerElem.append("g").data([dataItem])
                            .classed({
                                "bar": true,
                                "up-day": isUpDay,
                                "down-day": isDownDay
                            });

                        highLowLines(bar);

                        if (barType === graphTypes.OHLC) {
                            openTicks(bar);
                        }
                        closeTicks(bar);
                        indexElementsMapping[dataIndex] = bar;
                    }
                });

                getElementsMapping && getElementsMapping(indexElementsMapping);
            });
        };

        ohlc.xScale = function (value) {
            if (!arguments.length) {
                return xScale;
            }
            xScale = value;
            return ohlc;
        };

        ohlc.yScale = function (value) {
            if (!arguments.length) {
                return yScale;
            }
            yScale = value;
            return ohlc;
        };

        ohlc.tickSize = function (value) {
            if (!arguments.length) {
                return tickSize;
            }
            tickSize = value/2;
            return ohlc;
        };

        ohlc.type = function (value) {
            if (!arguments.length) {
                return barType;
            }
            barType = value;
            return ohlc;
        };

        ohlc.xAttributeName = function (value) {
            if (!arguments.length) {
                return xAttributeName;
            }
            xAttributeName = value;
            return ohlc;
        };

        ohlc.yAttributeName = function (value) {
            if (!arguments.length) {
                return yAttributeName;
            }
            yAttributeName = value;
            return ohlc;
        };

        ohlc.getElementsMapping = function (value) {
            if (!arguments.length) {
                return getElementsMapping;
            }
            getElementsMapping = value;
            return ohlc;
        };

        return ohlc;
    }
});
