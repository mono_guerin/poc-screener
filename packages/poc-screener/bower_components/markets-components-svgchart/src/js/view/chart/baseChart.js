﻿define(["../../manager/chartConfig/graphTypes", "./basicModule/commonPlotter", "../../core/core",
        "../baseView", "../../util/util", "./basicModule/gridLines", "../../manager/data/export", "moment"
    ],
    function(graphTypes, commonPlotter, core, baseView, util, gridLines, exportService, moment) {
        "use strict";
        var $ = core.$;
        var d3 = core.d3;
        var coreUtil = core.Util;
        var dateUtil = core.Widget.Chart.Util;
        var model = function(managers) {
            var eventManager = managers.eventManager;
            var chartConfigManager = managers.chartConfigManager;
            var IntervalTypes = chartConfigManager.IntervalTypes;
            var CursorType = chartConfigManager.CursorType;
            var FrequencyTypes = chartConfigManager.FrequencyTypes;
            var VerticalScaleTypes = chartConfigManager.VerticalScaleTypes;
            var MainChartTypes = chartConfigManager.MainChartTypes;
            var MainDataTypes = chartConfigManager.MainDataTypes;
            var dataAdapter = managers.dataAdapter;
            var SelectionModes = chartConfigManager.SelectionModes;
            var StartDateTypes = chartConfigManager.StartDateTypes;
            var isMobile = util.deviceType.mobile();
            var getDateFormat = function() {
                var format;
                var rootConfig = chartConfigManager.getConfig();
                if (chartConfigManager.isIntraday()) {
                    format = rootConfig.intradayDateFormater;
                } else {
                    format = rootConfig.dailyDateFormatter;
                }
                return format;
            };
            /**
             *   Base chart Model
             *   It extended from baseView.
             */
            var baseChartExtend = {
                _init: function() {
                    this._bindEvents();
                    this.decimalPlaces = this._getDecimalPlaces();
                },
                _exportToCsv: function(rowData) {
                    rowData = rowData || [];
                    var csvString = '';
                    var dataLength = 0;
                    var rootConfig = chartConfigManager.getConfig();
                    for (var i = 0; i < rowData.length; i++) {
                        dataLength = rowData[i].length > dataLength ? rowData[i].length : dataLength;
                    }
                    for (var i = 0; i < dataLength; i++) {
                        var rowString = '';
                        for (var r = 0; r < rowData.length; r++) {
                            var tempStr = rowData[r][i];
                            if (util.endwith(tempStr, rootConfig.columnDelimiter)) {
                                if (r !== rowData.length - 1) {
                                    tempStr += rootConfig.columnDelimiter;
                                }
                            } else if (r !== rowData.length - 1) {
                                tempStr += (rootConfig.columnDelimiter + rootConfig.columnDelimiter);
                            }
                            rowString += tempStr;
                        }
                        rowString += '\r\n';
                        csvString += rowString;
                    }
                    exportService.createFile(this.config.container, csvString, this._getLegendText(this.mainTickerObject) + ".csv");
                },
                _generateRowArray: function(tickerObject, dataPoints, priceData, indicatorsData, fundamentalsData, eventsData) {
                    var header = this._getExportColumnHeaders(dataPoints, tickerObject);
                    var rowArray = [header];
                    var rootConfig = chartConfigManager.getConfig();
                    for (var i = 0; i < this._data.length; i++) {
                        var row = '';
                        for (var j = 0; j < dataPoints.length; j++) {
                            var dp = dataPoints[j];
                            var value = '';
                            if (priceData[i]) {
                                switch (dp.toLowerCase()) {
                                    case 'close':
                                        value = priceData[i].close;
                                        break;
                                    case 'open':
                                        value = priceData[i].open;
                                        break;
                                    case 'high':
                                        value = priceData[i].high;
                                        break;
                                    case 'low':
                                        value = priceData[i].low;
                                        break;
                                    case 'volume':
                                        value = priceData[i].volume;
                                        break;
                                    case 'growth':
                                    case 'price':
                                    case 'nav':
                                        value = priceData[i][this.yAttr];
                                        break;
                                    case '10kgrowth':
                                        value = priceData[i].value || priceData[i].close;
                                        break;
                                    case 'originalprice':
                                        value = priceData[i].originalPrice || priceData[i].close;
                                        break;
                                    default:
                                        {
                                            break;
                                        }
                                }
                            }
                            if (/^indicator/.test(dp)) {
                                var indicatorName = dp.split(':')[1];
                                var parameter = dp.match(/(\S*)_(\S*):(\S*)/) && dp.match(/(\S*)_(\S*):(\S*)/)[2];
                                var indicatorData;
                                if (!parameter) {
                                    indicatorData = indicatorsData[indicatorName][0].data;
                                } else {
                                    $.each(indicatorsData[indicatorName], function(index, dataObj) {
                                        if (parameter == dataObj.parameter) {
                                            indicatorData = dataObj.data;
                                        }
                                    });
                                }
                                value = indicatorData[i] && indicatorData[i].value;
                            } else if (/^fundamental/.test(dp)) {
                                var fundamentalName = dp.split(':')[1];
                                if (fundamentalsData && fundamentalsData[fundamentalName]) {
                                    value = fundamentalsData[fundamentalName].data && fundamentalsData[fundamentalName].data[i] && fundamentalsData[fundamentalName].data[i].value;
                                }
                            }
                            value = isNaN(value) ? '' : util.numberFormatter(this.config.lang, value, this._decimalPlaces, " ", false, rootConfig.isFormat, true);
                            if (dp === 'time') {
                                value = moment(this._data[i].date).format(getDateFormat());
                            } else if (dp === 'name') {
                                value = tickerObject['customTickerName'] || tickerObject['Name'] || '';
                            }
                            if (value.indexOf(',') > -1 || value.indexOf('\n') > -1 || value.indexOf('\r') > -1) {
                                value = '"' + value + '"';
                            }
                            row += value + rootConfig.columnDelimiter;
                        }
                        rowArray.push(row);
                    }
                    return rowArray;
                },
                _exportDataToExcel: function() {
                    var dataPoints = this._getExportDataPoints()
                    var priceData = [],
                        indicatorsData = [],
                        fundamentalsData = [],
                        eventsData = [],
                        rowData = [];
                    var mainDataPoints = dataPoints[this._getUniqueKey(this.mainTickerObject)];
                    priceData = this._data;
                    indicatorsData = this._indicatorsData;
                    fundamentalsData = this._fundamentalsData;
                    eventsData = this._eventsData;
                    rowData.push(this._generateRowArray(this.mainTickerObject, mainDataPoints, priceData, indicatorsData, fundamentalsData, eventsData));
                    if (this._compareData && chartConfigManager.getConfig().growthType !== 'priceGrowth') {
                        for (var i = 0; i < this._compareData.length; i++) {
                            var compareData = this._compareData[i];
                            var compareDataPoints = dataPoints[this._getUniqueKey(compareData.tickerObject)];
                            priceData = this._compareData[i].Price.data;
                            indicatorsData = this._compareData[i].Indicators;
                            fundamentalsData = this._compareData[i].Fundamentals;
                            eventsData = this._compareData[i].eventsData;
                            rowData.push(this._generateRowArray(compareData.tickerObject, compareDataPoints, priceData, indicatorsData, fundamentalsData, eventsData));
                        }
                    }
                    this._exportToCsv(rowData);
                },
                _getExportDataPoints: function() {
                    var exportDPMap = {
                        'price': ['time', 'name', 'close', 'high', 'low', 'open'],
                        'nav': ['time', 'name', 'nav'],
                        'return': ['time', 'name', 'growth'],
                        'return_k': ['time', 'name', '10KGrowth']
                    };
                    var dataPoints = {};
                    var tickerCollection = chartConfigManager.getTickerCollection();
                    var type = parseInt(this.mainTickerObject.type, 10);
                    var mType = this.mainTickerObject.mType;
                    var priceType = chartConfigManager.getPriceType(tickerCollection);
                    var mainDps = exportDPMap[priceType] || [];
                    var config = chartConfigManager.getConfig();
                    if (config.showCrosshairOriginalData && !(chartConfigManager.isITFund(this.mainTickerObject) && priceType === 'return') || config.growthType == "performancesGrowth" || config.growthType === 'priceGrowth') {
                        mainDps = ['time', 'name', 'originalPrice'];
                    }
                    if (type !== 20 && !chartConfigManager.isMutualFund(this.mainTickerObject)) {
                        mainDps.push('volume');
                    }
                    for (var indic in this._indicatorsData) {
                        var indicData = this._indicatorsData[indic] || [];
                        switch (indic) {
                            case 'RSI':
                            case 'PChannel':
                            case 'DMI':
                                indicData = indicData.slice(0, 1);
                                break;
                            case 'SStochastic':
                            case 'FStochastic':
                                indicData = indicData.slice(0, 2);
                                break;
                        }
                        for (var p = 0; p < indicData.length; p++) {
                            var para = indicData[p].parameter;
                            if (para) {
                                mainDps.push('indicator_' + indicData[p].parameter + ':' + indic);
                            } else {
                                mainDps.push('indicator:' + indic);
                            }
                        }
                    }
                    for (var fundamental in this._fundamentalsData) {
                        mainDps.push('fundamental:' + fundamental);
                    }
                    dataPoints[this._getUniqueKey(this.mainTickerObject)] = mainDps;
                    if (this._compareData) {
                        for (var i = 0; i < this._compareData.length; i++) {
                            var compareDps = ['name'];
                            var uniqueKey = this._getUniqueKey(this._compareData[i].tickerObject);
                            if (dataPoints[uniqueKey]) {
                                continue;
                            }
                            if (config.showCrosshairOriginalData && !(chartConfigManager.isITFund(this.mainTickerObject) && priceType === 'return') || config.growthType == "performancesGrowth") {
                                compareDps.push('originalPrice');
                            } else {
                                compareDps.push('growth');
                            }
                            for (var fundamentalName in (this._compareData[i].Fundamentals || {})) {
                                if (chartConfigManager.isMutualFund(this._compareData[i].tickerObject)) {
                                    if (this.config.fundamentalProperties.hasOwnProperty(fundamentalName) && this.config.fundamentalProperties[fundamentalName].showWithCompare) {
                                        compareDps.push('fundamental:' + fundamentalName);
                                    }
                                }
                            }
                            for (var indicatorName in (this._compareData[i].Indicators || {})) {
                                if (this.config.indicatorProperties.hasOwnProperty(indicatorName) && this.config.indicatorProperties[indicatorName].showWithCompare) {
                                    var indicData = this._indicatorsData[indic] || [];
                                    for (var p = 0; p < indicData.length; p++) {
                                        var para = indicData[p].parameter;
                                        if (para) {
                                            compareDps.push('indicator_' + indicData[p].parameter + ':' + indicatorName);
                                        } else {
                                            compareDps.push('indicator:' + indicatorName);
                                        }
                                    }
                                }
                            }
                            dataPoints[uniqueKey] = compareDps;
                        }
                    }
                    return dataPoints;
                },
                _getUniqueKey: function(tickerObject) {
                    if (tickerObject) {
                        return [tickerObject.gkey, tickerObject.secId, tickerObject.performanceId].join(',').replace(/[_:]/g, '');
                    }
                    return null;
                },
                _getDataByUniqueKey: function(key) {
                    if (this._getUniqueKey(this.mainTickerObject) === key) {
                        return this._data;
                    } else {
                        for (var i = 0; i < this._compareData.length; i++) {
                            if (this._getUniqueKey(this._compareData[i].tickerObject) === key) {
                                return this._compareData[i].Price.data;
                            }
                        }
                    }
                    return [];
                },
                _getTickerObjectByUniqueKey: function(key) {
                    var tickerObjects = chartConfigManager.getTickerCollection();
                    for (var i = 0; i < tickerObjects.length; i++) {
                        var tickerObject = tickerObjects[i];
                        var uniqueKey = this._getUniqueKey(tickerObject);
                        if (uniqueKey === key) {
                            return tickerObject;
                        }
                    }
                    return null;
                },
                _getExportColumnHeaders: function(keys, tickerObject) {
                    var headers = [];
                    var config = chartConfigManager.getConfig();
                    for (var i = 0, l = keys.length, key, lkey; i < l; i++) {
                        key = lkey = keys[i];
                        var parameter = '';
                        if (key === 'time') {
                            lkey = isNaN(config.frequency) ? 'date' : key;
                        } else {
                            if (key === 'originalPrice') {
                                lkey = chartConfigManager.isMutualFund(tickerObject) ? 'nav' : 'price';
                                if (tickerObject.mType === 'XI') {
                                    lkey = 'price';
                                }
                                if (config.growthType == "performancesGrowth" || config.growthType === 'priceGrowth') {
                                    lkey = 'value'
                                }
                            } else if (key.indexOf(':') > -1) {
                                lkey = key.split(':')[1];
                                lkey = lkey.replace(/\b(\w)|\s(\w)/g, function(m) {
                                    return m.toLowerCase();
                                });
                                if (key.indexOf('_') > -1) {
                                    parameter = key.match(/(\S*)_(\S*):(\S*)/)[2];
                                }
                            }
                        }
                        var header = this.langData[lkey] || lkey.replace(/\b(\w)|\s(\w)/g, function(m) {
                            return m.toUpperCase();
                        });
                        if (parameter !== '') {
                            header += parameter;
                        }
                        headers.push(header);
                    }
                    return headers.join(config.columnDelimiter);
                },
                _createCommonPlotter: function() {
                    this._defaultGraphConfig = {};

                    this._getDefaultGraphConfig = function() {
                        return this._defaultGraphConfig;
                    };
                    this._commonPlotter = new commonPlotter(this._defaultGraphConfig);
                },
                _caculateValidSize: function() {
                    var cfg = this.config;
                    if (cfg.yAxis.position == "outer") {
                        this._validWidth = cfg.width - cfg.margin.left - cfg.margin.right - (cfg.yAxis.width + cfg.yAxisMargin || 0);
                    } else {
                        this._validWidth = cfg.width - cfg.margin.left - cfg.margin.right;
                    }

                    this._validHeight = cfg.height - cfg.margin.top - cfg.margin.bottom;
                },
                _updateViewAfterDataChanged: function(preloadInfo, offsetDomain, res) {
                    if (this._changeScaleDomain()) {
                        this._displayViewAfterChangeDomain();
                    }
                },
                _getCompareData: function(dataArr) {
                    var compareItem, compareDataArr = [];
                    for (var i = 1, len = dataArr.length; i < len; i++) {
                        compareItem = dataArr[i];
                        compareItem && compareItem.Price && dataArr[i].Price.data && compareDataArr.push(dataArr[i]);
                    }
                    this._compareData = (compareDataArr.length > 0) ? compareDataArr : null;
                },
                _reviseData: function(data) {
                    this._decideMinMaxAttrbute();
                    var yAttr = this.yAttr || 'close';
                    data.forEach(function(d) {
                        d.x = d.index;
                        d.y = d[yAttr];
                    });
                    return data;
                },
                _reviseDividendEffectData: function(data) {
                    var revisedData = data.slice();
                    revisedData.forEach(function(d) {
                        d.x = d.index;
                        d.y = d.value;
                        d.value = d.value;
                    });
                    return revisedData;
                },
                _reviseMainDividendEffectData: function(data) {
                    var revisedData = data.slice();
                    revisedData.forEach(function(d) {
                        d.x = d.index;
                        d.y = d.close;
                        d.value = d.close;
                    });
                    return revisedData;
                },
                _cancelNotRunedPreload: function() {
                    clearTimeout(this._timer);
                },
                _preloadData: function(domainStart, domainEnd) {
                    domainStart = domainStart || this._xScale.domain()[0];
                    domainEnd = domainEnd || this._xScale.domain()[1];
                    var view = this;
                    if ((domainStart <= 0 || domainStart < (domainEnd - domainStart)) && !this._loadingData && (this._preloadInfo.intervalType != IntervalTypes.Max)) {
                        clearTimeout(this._timer);
                        this._timer = setTimeout(function() {
                            if (!this._loadingData) {
                                view._preloading = true;
                                view._loadingData = true;
                                eventManager.trigger(eventManager.EventTypes.FetchPreloadData);
                            }
                        }, this.config.preloadDelay);
                    }
                },
                _throttle: function(fn, delay) {
                    var timer = null;
                    return function() {
                        var context = this,
                            args = arguments;

                        clearTimeout(timer);
                        timer = setTimeout(function() {
                            fn.apply(context, args);
                        }, delay)
                    };
                },
                _loadDataForPanning: function() {
                    var self = this;
                    return this._throttle(function() {
                        if (self._loadingData && self._preloading) {
                            self._showLoading();
                            self._preloading = false;
                        } else if (!self._loadingData && (self._preloadInfo.intervalType != IntervalTypes.Max)) {
                            self._preloading = true;
                            clearTimeout(self._timer);
                            eventManager.trigger(eventManager.EventTypes.FetchPreloadData);
                        }
                    }, 500)();
                },
                _fetchPreloadData: function() {
                    if (!this._xScale) {
                        return;
                    }
                    var view = this;
                    this._loadingData = true;
                    chartConfigManager.preloadMainTickerData(this._preloadInfo,
                        function(res, preloadInfo) {
                            if (res && res.status().errorCode === "0") {
                                var resDataArr = res.data();
                                var mainTickerData = resDataArr[0];
                                var newData = mainTickerData.Price.data;
                                var newDataLength = newData.length;
                                if (newDataLength > 0) {
                                    newData = view._reviseData(newData);
                                    var renderedData = preloadInfo.loadedData || view._data;
                                    var currentDomain = view._xScale.domain();
                                    var startIndex, endIndex;
                                    var offsetDomain = 0; //preloadInfo.offsetDomain || 0;
                                    var renderedDateLength = renderedData.length;
                                    if (renderedDateLength === 0) {
                                        return;
                                    }

                                    var newDataLastDateTime = newData[newDataLength - 1].date.getTime();
                                    if (renderedData[renderedDateLength - 1].date.getTime() === newDataLastDateTime) {
                                        offsetDomain += newDataLength - renderedDateLength;
                                    } else {
                                        var renderedDate = renderedData[0].date.getTime();
                                        if (renderedDate > newDataLastDateTime) {
                                            offsetDomain += newDataLength;
                                        } else if (renderedDate === newDataLastDateTime) {
                                            newData.pop();
                                            offsetDomain += newDataLength - 1;
                                        } else if (renderedDate < newDataLastDateTime) {
                                            for (var i = 0; i < renderedData.length; i++) {
                                                if (renderedData[i].date.getTime() > newDataLastDateTime) {
                                                    break;
                                                }
                                            }
                                            offsetDomain += newDataLength - i;
                                            renderedData = renderedData.slice(i);
                                        } else {
                                            alert("Error!");
                                        }

                                        renderedData.forEach(function(d) {
                                            d.x = d.x + offsetDomain;
                                        });

                                        newData = newData.concat(renderedData);
                                    }

                                    preloadInfo.offsetDomain = preloadInfo.offsetDomain || 0;
                                    preloadInfo.offsetDomain += offsetDomain;

                                    //Why put it on preloadInfo?
                                    //Because preloadInfo can reset all those parameters when change date range or indicator.
                                    view._loadedData = preloadInfo.loadedData = newData;
                                    view._preloadInfo = preloadInfo;
                                }
                            }

                            view._hideLoading();
                            view._loadingData = false;
                        });
                },
                _decideMinMaxAttrbute: function() {
                    var comConfig = chartConfigManager.getConfig();
                    if (/\w+\-(mainchart)/ig.test(this.$el[0].className) && comConfig.chartGenre === chartConfigManager.ChartGenre.FundChart && chartConfigManager.getMainDataType() === chartConfigManager.MainDataTypes.PriceWithDividendGrowth) {
                        this.minAttr = "growthWithNav";
                        this.maxAttr = "growthWithNav";
                        this.yAttr = "growthWithNav";
                    } else if (comConfig.chartGenre === chartConfigManager.ChartGenre.FundChart && !chartConfigManager.isCompareOrPercentChart() && !comConfig.useDataApiTimeseriesData) {
                        this.minAttr = "value";
                        this.maxAttr = "value";
                        this.yAttr = "value";
                        this._convertToPrice = this._convertToFundPrice; // This avoid judge chartGenre
                    } else if ($.inArray(comConfig.charts.mainChart.chartType, [MainChartTypes.OHLCChart, MainChartTypes.HLCChart, MainChartTypes.CandlestickChart]) > -1) {
                        this.minAttr = "low";
                        this.maxAttr = "high";
                        this.yAttr = "close";
                    } else {
                        this.minAttr = "close";
                        this.maxAttr = "close";
                        this.yAttr = "close";
                    }
                },
                _createScale: function() {
                    var view = this;
                    this._xScale = d3.scale.linear()
                        .range([0, this._validWidth])
                        .domain(this._getAjustedDomain([0, this._data.length - 1]));

                    this._yScale = this._createYScale().range([this._validHeight, 0]);
                    this._setYScaleDomain();
                },
                _getAjustedDomain: function(xScaleDomain) {
                    var domain = xScaleDomain[1] - xScaleDomain[0];
                    if (domain === 0) {
                        var data = this._data[0];
                        var dataArr = []; // Avoid affect original data.
                        var data1 = {};
                        var data2 = {};
                        data1.date = data2.date = data.date;
                        data1.dateIndex = data2.dateIndex = data.dateIndex;
                        dataArr.push(data1);
                        data.x = 1;
                        dataArr.push(data);
                        dataArr.push(data2);
                        domain = 2;
                        this._data = dataArr;
                        this._needAjustLine = true;
                    } else {
                        this._needAjustLine = false;
                    }

                    return [0, domain];
                },
                _setYScaleDomain: function() {
                    var view = this;
                    //d3.min/max can work on NaN, but Math.min/max can't.
                    var minYValueArr = [d3.min(this._data, function(d) {
                        return d[view.minAttr];
                    })];
                    var maxYValueArr = [d3.max(this._data, function(d) {
                        return d[view.maxAttr];
                    })];
                    for (var indicatorName in this._indicatorsData) {
                        if (this.config.indicatorProperties.hasOwnProperty(indicatorName) && indicatorName !== dataAdapter.IndicatorName.VBP) {
                            if (indicatorName === dataAdapter.IndicatorName.PrevClose &&
                                (this.config.intervalType !== IntervalTypes.OneDay || chartConfigManager.isCompareOrPercentChart())) {
                                continue;
                            }

                            this._indicatorsData[indicatorName].forEach(function(s) {
                                minYValueArr.push(d3.min(s.data, function(d) {
                                    return d.value;
                                }));
                                maxYValueArr.push(d3.max(s.data, function(d) {
                                    return d.value;
                                }));
                            });
                        }
                    }

                    if (this._compareData && !this._needHideBenchmark()) {
                        this._compareData.forEach(function(d) {
                            minYValueArr.push(d3.min(d.Price.data, function(d) {
                                return d[view.minAttr];
                            }));
                            maxYValueArr.push(d3.max(d.Price.data, function(d) {
                                return d[view.maxAttr];
                            }));
                        });
                    }

                    var isPriceChart = this._checkIfPriceChart();
                    if (this._fundamentalsData && isPriceChart) {
                        for (var fundamentalName in this._fundamentalsData) {
                            if (this.config.fundamentalProperties.hasOwnProperty(fundamentalName) && !this.config.fundamentalProperties[fundamentalName].skipDrawGraph) {
                                var filteredFundamentals = view._fundamentalsData[fundamentalName].data;
                                if (filteredFundamentals && filteredFundamentals.length > 0) {
                                    minYValueArr.push(d3.min(filteredFundamentals, function(d) {
                                        return d.value;
                                    }));
                                    maxYValueArr.push(d3.max(filteredFundamentals, function(d) {
                                        return d.value;
                                    }));
                                }
                            }
                        }
                    }

                    if (this._dividendEffectData && this._dividendEffectData.length > 0) {
                        minYValueArr.push(d3.min(this._dividendEffectData, function(d) {
                            return d.value;
                        }));
                        maxYValueArr.push(d3.max(this._dividendEffectData, function(d) {
                            return d.value;
                        }));
                    }

                    var yScaleData = this._calculateYScaleDomainTickValues(d3.min(minYValueArr), d3.max(maxYValueArr));
                    this._yTickValues = yScaleData.tickValues;
                    if (yScaleData.domain) {
                        this._yScale.domain(yScaleData.domain);
                    }
                },
                _calculateYScaleDomainTickValues: function(minYValue, maxYValue) {
                    var maxTickCount = this._caculateMaxTickCount();
                    return this._ajustDomainByAlgorithm([minYValue, maxYValue], maxTickCount);
                },
                _caculateMaxTickCount: function() {
                    var height = this._validHeight;
                    var textHeight = this._getTextWidthHeight("1").height;
                    var space = 8;
                    var maxTickCount = parseInt(height / (textHeight * 2 + space), 10) + 1;
                    if (maxTickCount <= 1) {
                        maxTickCount = 2; //at least 2 ticks
                    }
                    if (this.config.yAxis.maxTickCount && this.config.yAxis.maxTickCount < maxTickCount) {
                        maxTickCount = this.config.yAxis.maxTickCount;
                    }
                    return maxTickCount;
                },
                _ajustDomainByAlgorithm: function(domain, maxTickCount) {
                    var _factors = [0.01, 0.015, 0.02, 0.025, 0.03, 0.04, 0.05, 0.075];
                    var min = domain[0];
                    var max = domain[1];
                    var range = util.Math.subtract(max, min);
                    var adjustedMax, adjustedMin;
                    var maxExponent = 20;
                    var tickCount;
                    if (range == 0) {
                        //max==min==factor
                        factor = max = min;
                        tickCount = (maxTickCount % 2) ? maxTickCount : (maxTickCount - 1);
                        var multiplyFactor = Math.floor(tickCount / 2) * factor;
                        adjustedMax = util.Math.add(max, multiplyFactor);
                        adjustedMin = util.Math.subtract(min, multiplyFactor);
                        domain[0] = adjustedMin;
                        domain[1] = adjustedMax;
                        return {
                            domain: domain,
                            tickValues: this._generateYTickValues(domain, tickCount, factor)
                        };
                    } else if (range <= 0.01) {
                        var temp = 10;
                        maxExponent = 3;
                        for (var i = maxExponent; i > 0; i--) {
                            for (var f in _factors) {
                                var factor = util.Math.multiply(_factors[f], Math.pow(10, -i));
                                if (util.Math.multiply((maxTickCount - 1), factor) > range) {
                                    var offset = util.Math.subtract(factor, Math.abs(util.Math.mod(max, factor)));
                                    adjustedMax = util.Math.add(max, offset);
                                    tickCount = parseInt(util.Math.divide(util.Math.subtract(adjustedMax, min), factor) + 1, 10);
                                    tickCount += util.Math.mod(util.Math.subtract(adjustedMax, min), factor) === 0 ? 0 : 1;
                                    adjustedMin = util.Math.subtract(adjustedMax, (tickCount - 1) * factor);
                                    if (tickCount < maxTickCount) {
                                        domain[0] = adjustedMin;
                                        domain[1] = adjustedMax;
                                        return {
                                            domain: domain,
                                            tickValues: this._generateYTickValues(domain, tickCount, factor)
                                        };
                                    }
                                }
                            }
                        }
                    } else {
                        for (var i = 0; i < maxExponent; i++) {
                            for (var f in _factors) {
                                var factor = util.Math.multiply(_factors[f], Math.pow(10, i));
                                if (util.Math.subtract((maxTickCount - 1) * factor, range) > util.Math.multiply(0.01, range)) {
                                    var offset = util.Math.subtract(factor, Math.abs(util.Math.mod(max, factor)));
                                    adjustedMax = util.Math.add(max, offset);
                                    tickCount = parseInt(util.Math.divide(util.Math.subtract(adjustedMax, min), factor) + 1, 10);
                                    tickCount += util.Math.mod(util.Math.subtract(adjustedMax, min), factor) === 0 ? 0 : 1;
                                    adjustedMin = util.Math.subtract(adjustedMax, (tickCount - 1) * factor);
                                    if (tickCount <= maxTickCount) {
                                        domain[0] = adjustedMin;
                                        domain[1] = adjustedMax;
                                        return {
                                            domain: domain,
                                            tickValues: this._generateYTickValues(domain, tickCount, factor)
                                        };
                                    }
                                }
                            }
                        }
                    }
                },
                _generateYTickValues: function(domain, tickCount, factor) {
                    var tickValues = [];
                    this._yAxisDecimal = this._getYAxisDecimalPlaces(domain, tickCount, factor);
                    if (this._getCurrentScaleType() == VerticalScaleTypes.Logarithmic) {
                        if (domain[0] < 1) {
                            var adjustOffset = 1 - domain[0];
                            domain[0] = 1;
                            domain[1] = util.Math.add(domain[1], adjustOffset);
                        }
                    }
                    for (var i = 0; i < tickCount; i++) {
                        tickValues.push(domain[0] + factor * i);
                    }
                    // this._yTickValues = tickValues;
                    return tickValues;
                },
                _createYScale: function() {
                    var yScale;
                    var scaleType = this._getCurrentScaleType();
                    switch (scaleType) {
                        case VerticalScaleTypes.Logarithmic:
                            yScale = d3.scale.log();
                            break;
                        case VerticalScaleTypes.Linear:
                        default:
                            yScale = d3.scale.linear();
                            break;
                    }

                    return yScale;
                },
                _getCurrentScaleType: function() {
                    return VerticalScaleTypes.Linear;
                },
                _fetchDateRangeData: function(dateRange) {
                    var view = this;
                    chartConfigManager.fetchDisplayData(dateRange, view._preloadInfo.frequency, function(res) {
                        view._clearAllData();
                        if (res.status().errorCode === "0") {
                            var resDataArr = res.data();
                            var mainTickerData = resDataArr[0];
                            var data = mainTickerData.Price.data;
                            if (data.length > 0) {
                                data = view._reviseData(data);
                                view._data = data;
                                view._lastClosePrice = mainTickerData.Price.spos;
                                view._lastOpenPrice = mainTickerData.Price.epos;
                                view._fundamentalsData = mainTickerData.Fundamentals;
                                view._eventsData = mainTickerData.Events;
                                var mainDataType = chartConfigManager.getMainDataType();
                                if (mainDataType === MainDataTypes.DividendEffect) {
                                    var fundamentalType = dataAdapter.Fundamental.MReturnIndex;
                                    if (mainTickerData.Fundamentals && mainTickerData.Fundamentals[fundamentalType] &&
                                        mainTickerData.Fundamentals[fundamentalType].data) {
                                        view._dividendEffectData = view._reviseDividendEffectData(mainTickerData.Fundamentals[fundamentalType].data);
                                    }
                                } else {
                                    view._getCompareData(res.data());
                                    view._indicatorsData = mainTickerData.Indicators;
                                    view._createDummyData(view._compareData);
                                }

                                var startIndex = view._getDataIndexByDate(dateRange[0], null, null, view._data);
                                var endIndex = view._getDataIndexByDate(dateRange[1], null, null, view._data);
                                var xScaleDomain = [startIndex, endIndex];
                                view._getDisplayData(xScaleDomain);
                                view._setScaleDomain(xScaleDomain);
                            }
                        }
                        view._displayViewAfterChangeDomain();

                    });
                },
                _changeScaleDomain: function(xScaleDomain, isZoomIn) {
                    if (!this._xScale) {
                        return;
                    }
                    if (xScaleDomain) {
                        if (isZoomIn) {
                            this._getDisplayData(xScaleDomain);
                        } else {
                            var loadedData = this._loadedData;
                            if (loadedData) {
                                var startDate = loadedData[xScaleDomain[0]].date;
                                var endIndex = (xScaleDomain[1] < loadedData.length) ? xScaleDomain[1] : (loadedData.length - 1);
                                var endDate = loadedData[endIndex].date;
                                this._fetchDateRangeData([startDate, endDate]);
                            }
                            return;
                        }
                    } else {
                        var lastDateRange = this._lastDateRange;
                        if (lastDateRange) {
                            var data = this._data;
                            var start = this._getDataIndexByDate(lastDateRange[0], null, null, data);
                            var end = this._getDataIndexByDate(lastDateRange[1], null, null, data);
                            if (!start || !end) {
                                this._fetchDateRangeData(lastDateRange);
                                return;
                            } else {
                                xScaleDomain = [start, end];
                                this._getDisplayData(xScaleDomain);
                            }
                        } else {
                            xScaleDomain = [0, this._data.length - 1];
                        }
                    }
                    this._setScaleDomain(xScaleDomain);
                    return true;
                },
                _displayViewAfterChangeDomain: function() {
                    this._clearView();
                    this._renderContainedViews();
                },
                _setScaleDomain: function(xScaleDomain) {
                    var view = this;
                    this._xScale.domain(this._getAjustedDomain(xScaleDomain));
                    this._setYScaleDomain();
                },
                _checkIfPriceChart: function() {
                    var mainDataType = chartConfigManager.getMainDataType();
                    var isPriceChart = mainDataType !== MainDataTypes.Growth &&
                        mainDataType !== MainDataTypes.DividendEffect &&
                        (!this._compareData || this._compareData.length == 0);
                    var rootCfg = chartConfigManager.getConfig();
                    if (chartConfigManager.getMainDataType() === MainDataTypes.Growth && chartConfigManager.isITFund() && chartConfigManager.getConfig().growthType == "priceGrowth" || rootCfg.drawNavAndPriceChart) {
                        isPriceChart = true;
                    }
                    return isPriceChart;
                },
                _getDataIndexByDate: function(dateTime, startIndex, endIndex, data) {
                    var data = data || this._data;
                    var result;
                    if (data) {
                        startIndex = startIndex || 0;
                        endIndex = endIndex || data.length - 1;
                        if (dateTime < data[startIndex].date || dateTime > data[endIndex].date) {
                            result = null;
                        } else if (dateTime.getTime() === data[startIndex].date.getTime()) {
                            result = startIndex;
                        } else if (dateTime.getTime() === data[endIndex].date.getTime()) {
                            result = endIndex;
                        } else {
                            while (true) {
                                if ((endIndex - startIndex) >= 2) {
                                    var middleIndex = Math.floor((startIndex + endIndex) / 2);
                                    var middleIndexDate = data[middleIndex].date;
                                    if (dateTime < middleIndexDate) {
                                        endIndex = middleIndex;
                                    } else if (dateTime > middleIndexDate) {
                                        startIndex = middleIndex;
                                    } else {
                                        result = middleIndex;
                                        break;
                                    }
                                } else {
                                    var millisecond;
                                    var startDate, endDate;
                                    startDate = data[startIndex].date;
                                    if (endIndex === data.length) {
                                        millisecond = data[endIndex - 1].date - data[endIndex - 2].date;
                                    } else {
                                        endDate = data[endIndex].date;
                                        millisecond = endDate - startDate
                                    }
                                    result = startIndex + (dateTime - startDate) / millisecond;
                                    break;
                                }
                            }
                        }
                    }

                    return result;
                },
                _changDataRange: function(adjustedDateRange) {
                    this._lastDateRange = null;
                    this._updateViewData(adjustedDateRange);
                },
                _getDisplayPriceData: function(xScaleDomain) {
                    var data = this._data;
                    this._data = data.slice(xScaleDomain[0], xScaleDomain[1] + 1);
                },
                _getDisplayDividendEffectData: function(xScaleDomain) {
                    var dividendData = this._dividendEffectData;

                    this._dividendEffectData = dividendData && dividendData.slice(xScaleDomain[0], xScaleDomain[1] + 1);
                },
                _getDisplayCompareData: function(xScaleDomain) {
                    var loadedCompareData = this._loadedCompareData;
                    var renderedCompareData = this._compareData;
                    var compareData = renderedCompareData;
                    if (renderedCompareData && compareData && compareData.length > 0) {
                        for (var i = 0, len = compareData.length; i < len; i++) {
                            renderedCompareData[i].Price.data = compareData[i].Price.data.slice(xScaleDomain[0], xScaleDomain[1] + 1);
                        }

                        this._compareData = renderedCompareData;
                    }
                },
                _getDisplayIndicatorsData: function(xScaleDomain) {
                    var loadedIndicatorsData = this._loadedIndicatorsData;
                    var renderedIndicatorsData = this._indicatorsData;
                    var indecatorsData = renderedIndicatorsData;
                    var indicatorsItem, renderedIndicatorItem;
                    if (indecatorsData) {
                        for (var indicatorName in indecatorsData) {
                            indicatorsItem = indecatorsData[indicatorName];
                            renderedIndicatorItem = renderedIndicatorsData[indicatorName];
                            for (var i = 0, len = indicatorsItem.length; i < len; i++) {
                                renderedIndicatorItem[i].data = indicatorsItem[i].data.slice(xScaleDomain[0], xScaleDomain[1] + 1);
                            }
                        }
                    }

                    this._indicatorsData = renderedIndicatorsData;
                },
                _getDisplayData: function(xScaleDomain) {
                    this._getDisplayPriceData(xScaleDomain);
                    this._getDisplayIndicatorsData(xScaleDomain);
                    this._getDisplayCompareData(xScaleDomain);
                    this._getDisplayDividendEffectData(xScaleDomain);
                },
                _formatXAxisLabels: function(dataIndex, tickIndex) {
                    return this._xTickInfos.tickLabels[dataIndex].format;
                },
                _getOneDayTickInfos: function() {
                    var tickValues = [],
                        tickLabels = {};
                    var currentXScaleDomain = this._xScale.domain();
                    var tickSpan;
                    var intervalType = IntervalTypes.OneDay;
                    var frequency = chartConfigManager.getConfig().frequency || chartConfigManager.getDefaultfrequencyByIntervalType(intervalType);
                    switch (frequency) {
                        case FrequencyTypes.OneMinute:
                            tickSpan = this._validWidth <= 600 ? 120 : 60; //exception:x-small should have 2hrs tick (QS-7776)
                            break;
                        case FrequencyTypes.FiveMinute:
                            tickSpan = 12;
                            break;
                        case FrequencyTypes.TenMinute:
                            tickSpan = 6;
                            break;
                        case FrequencyTypes.FifteenMinute:
                            tickSpan = 4;
                            break;
                        case FrequencyTypes.ThirtyMinute:
                            tickSpan = 2;
                            break;
                    }

                    var lang = this.config.lang;
                    var data = this._data;
                    var d3Locale = chartConfigManager.getD3Locale();
                    var xLabelDateTimeFormat = d3Locale.XLabelDateTimeFormat;
                    var d3TimeFormat = d3Locale.timeFormat;
                    var xLabelFormatter;
                    var start = data[0].date;
                    var d;
                    for (var i = currentXScaleDomain[0]; i < currentXScaleDomain[1]; i = (i + tickSpan)) {
                        d = data[i].date;
                        tickValues.push(i);
                        if (start.getFullYear() != d.getFullYear()) {
                            xLabelFormatter = d3TimeFormat(xLabelDateTimeFormat.YearMonthDay);
                        } else if (start.getMonth() != d.getMonth() || start.getDate() != d.getDate()) {
                            xLabelFormatter = d3TimeFormat(xLabelDateTimeFormat.MonthDay);
                        } else {
                            xLabelFormatter = d3TimeFormat(xLabelDateTimeFormat.HourMinutes);
                        }
                        start = d;
                        tickLabels[i] = {
                            format: xLabelFormatter(d),
                            x: this._xScale(i)
                        };
                    }

                    return {
                        tickValues: tickValues,
                        tickLabels: tickLabels
                    };
                },
                _createXAxisTickInfos: function() {
                    var tickValues = [];
                    var tickLabels = {};
                    var data = this._data;
                    var dlen = data.length;
                    if (!dlen) {
                        return;
                    }
                    var start = data[0]['date'],
                        end = data[dlen - 1]['date'];
                    var diff = (end.getTime() - start.getTime()) / chartConfigManager.Constants.DAY_IN_MILLISECONDS;
                    if (this.config.intervalType === IntervalTypes.OneDay || diff < 1 && chartConfigManager.getConfig().chartGenre !== chartConfigManager.ChartGenre.FundChart) {
                        var tickInfos = this._getOneDayTickInfos();
                        tickValues = tickInfos.tickValues;
                        tickLabels = tickInfos.tickLabels;
                    } else {
                        var d3Locale = chartConfigManager.getD3Locale();
                        var xLabelDateTimeFormat = d3Locale.XLabelDateTimeFormat;
                        var d3TimeFormat = d3Locale.timeFormat;
                        var formatMD = d3TimeFormat(xLabelDateTimeFormat.MonthDay),
                            formatM = d3TimeFormat(xLabelDateTimeFormat.Month),
                            formatY = d3TimeFormat(xLabelDateTimeFormat.Year),
                            formatYM = d3TimeFormat(xLabelDateTimeFormat.YearMonth);
                        var ty = 'd';
                        var format;
                        if (diff < 8) {
                            ty = 'd';
                            format = formatMD;
                        } else if (diff <= 31) {
                            ty = 'avg';
                            format = formatMD;
                        } else if (diff <= 61) {
                            ty = 'w';
                            format = formatMD;
                        } else if (diff <= 240) {
                            ty = 'm';
                            format = start.getMonth() === 0 ? formatYM : formatM;
                        } else if (diff <= 730) {
                            ty = 'q';
                            format = start.getMonth() === 0 ? formatYM : formatM;
                        } else if (diff <= 2920) {
                            ty = 'y';
                            format = formatY;
                        } else {
                            ty = '5y';
                            format = formatY;
                        }

                        tickValues.push(0);
                        tickLabels[0] = {
                            format: format(start),
                            x: this._xScale(0)
                        }

                        for (var i = 1, d, flag, temp = start; i < dlen; i++) {
                            d = data[i]['date'];
                            flag = false;
                            if (ty === 'd' || ty === 'avg') {
                                flag = temp.getDate() !== d.getDate();
                                flag && (format = formatMD);
                            } else if (ty === 'w') {
                                flag = temp.getDate() !== d.getDate();
                                flag && (format = formatMD);
                            } else if (ty === 'm') {
                                flag = temp.getMonth() !== d.getMonth();
                                flag && (format = d.getMonth() === 0 ? formatYM : formatM);
                            } else if (ty === 'q') {
                                flag = (d.getMonth() + 1) % 3 === 1 && temp.getMonth() !== d.getMonth();
                                flag && (format = d.getMonth() === 0 ? formatYM : formatM);
                            } else if (ty === 'y') {
                                flag = temp.getFullYear() !== d.getFullYear();
                                flag && (format = formatY);
                            } else {
                                flag = temp.getFullYear() !== d.getFullYear() && d.getFullYear() % 5 === 0;
                                flag && (format = formatY);
                            }

                            if (flag) {
                                temp = d;
                                tickValues.push(i);
                                tickLabels[i] = {
                                    format: format(d),
                                    x: this._xScale(i)
                                };
                            }
                        }
                    }

                    var maxTickCount = this._validWidth / 70;
                    var tempTickValues;
                    while (tickValues.length > maxTickCount) {
                        tempTickValues = [];
                        for (var i = 0; i < tickValues.length; i++) {
                            (i % 2 === 0) && tempTickValues.push(tickValues[i]);
                        }
                        tickValues = tempTickValues;
                    }

                    var newTickValues = [];
                    var newTickLabels = {};
                    for (var j = 0, jl = tickValues.length, tickValue, tickLabel, tickWidth, lastTickWidth, nextX, lastX, formerX; j < jl; j++) {
                        tickValue = tickValues[j];
                        tickLabel = tickLabels[tickValue];
                        nextX = j === jl - 1 ? this._validWidth : tickLabels[tickValues[j + 1]].x;
                        formerX = j === 0 ? 0 : tickLabels[tickValues[j - 1]].x;
                        tickWidth = this._getTickWidth(tickLabel.format, this._innerClass.xAxis);
                        if (tickWidth <= nextX - tickLabel.x) {
                            tickLabel.textAnchor = 'start';
                        } else if ((tickLabel.x + tickWidth) > nextX) {
                            tickLabel.format = "";
                            tickLabel.textAnchor = 'start';
                        } else if (tickWidth <= tickLabel.x - formerX) {
                            if (lastTickWidth && (tickWidth + lastTickWidth) >= (tickLabel.x - formerX)) {
                                tickLabel.format = "";
                            }
                            tickLabel.textAnchor = 'end';
                        }
                        newTickValues.push(tickValue);
                        newTickLabels[tickValue] = tickLabel;
                        lastTickWidth = tickWidth;
                    }
                    this._xTickInfos = {
                        tickValues: newTickValues,
                        tickLabels: newTickLabels
                    };
                },
                _drawXAxis: function() {
                    this._createXAxisTickInfos();
                    var view = this;
                    if (this.config.xAxis.show) {
                        var config = {
                            tickSize: this.config.xAxis.tickSize || 0,
                            tickPadding: 5,
                            tickValues: this._xTickInfos.tickValues,
                            orient: this.config.xAxis.orient,
                            graphCss: this._innerClass.xAxis,
                            graphType: graphTypes.Axis,
                            scale: this._xScale,
                            width: this._validWidth,
                            height: this._validHeight,
                            xTickInfos: this._xTickInfos,
                            tickFormatFunc: function() {
                                return view._formatXAxisLabels.apply(view, arguments);
                            }
                        };
                        this._commonDraw(config);
                    }
                },
                _setYAxisStyle: function(selection) {
                    selection.select(".domain").remove(),
                        selection.selectAll("text").attr("dy", "1em");
                },
                _formatYAxisLabels: function() {

                },
                _getTickWidth: function(textArray, cls) {
                    var width = 0,
                        text,
                        temp;
                    var $textSpan = $('<span style="display:inline-block;"></span>').appendTo(this.config.container);
                    if (cls) {
                        $textSpan.addClass(cls);
                    }
                    if (!$.isArray(textArray)) {
                        textArray = [textArray];
                    }
                    textArray.forEach(function(text) {
                        temp = $textSpan.html(text).outerWidth(true);
                        if (temp > width) {
                            width = temp;
                        }
                    });
                    $textSpan.remove();
                    return width;
                },
                _changeYaxis: function() {},
                _drawYAxis: function() {
                    var view = this;
                    var formatValue;
                    var yAxisConfig = this.config.yAxis;
                    formatValue = function(d) {
                        return util.numberFormatter(view.config.lang, d, view._yAxisDecimal, " ", yAxisConfig.withUnit, false);
                    }
                    if (yAxisConfig.show) {
                        var comConfig = chartConfigManager.getConfig();
                        var yAxisTickValues = this._yTickValues;
                        var config = {
                            containerGElem: view._yAxisOuterGElem,
                            tickSize: yAxisConfig.tickSize || 0,
                            tickPadding: 5,
                            tickValues: yAxisTickValues,
                            orient: yAxisConfig.orient,
                            position: yAxisConfig.position,
                            yAxisWidth: yAxisConfig.width,
                            ticks: yAxisConfig.ticks,
                            graphCss: this._innerClass.yAxis,
                            setAxisStyleFunc: this._setYAxisStyle,
                            graphType: graphTypes.Axis,
                            scale: this._yScale,
                            width: this._validWidth,
                            height: this._validHeight,
                            tick: yAxisConfig.tick,
                            tickFormatFunc: function(d) {
                                return formatValue(d)
                            },
                            componentContainer: comConfig.$componentContainer,
                        };

                        if (comConfig.yAxisMargin) {
                            config.yAxisMargin = comConfig.yAxisMargin;
                        }
                        this._commonDraw(config);
                        //var yElem = this._commonDraw(config);
                        //var scaleType = this._getCurrentScaleType();
                        //this._processTicks(yElem);
                    }
                },
                _getGElemTranslate: function(type) {
                    var cfg = this.config;
                    var translate = {
                        x: cfg.margin.left,
                        y: cfg.margin.top
                    }
                    if (cfg.yAxis.show && cfg.yAxis.position == "outer") {
                        if (cfg.yAxis.orient == "left") {
                            translate.x = parseInt(cfg.margin.left + cfg.yAxis.width + cfg.yAxisMargin);
                            translate.y = parseInt(cfg.margin.top);
                        }
                    }
                    return "translate(" + translate.x + "," + translate.y + ")";
                },
                _processTicks: function(yElem) {
                    var lastTick = yElem.select(".tick");
                    if (lastTick.size()) {
                        var lastTickTransform = lastTick.attr("transform");
                        //TODO: use regex.
                        var transformValues = lastTickTransform.replace("translate(", "").replace(")", "");
                        if (transformValues.indexOf(",") > 0) {
                            transformValues = transformValues.split(",");
                        } else {
                            transformValues = transformValues.split(" ");
                        }

                        var lastTickYPosition = parseInt(transformValues[1], 10);
                        var marginValue = 17;
                        if ((this._validHeight - lastTickYPosition) < marginValue && this.config.yAxis.position == "inner") {
                            lastTick.remove();
                        }
                    }
                },
                _createDummyData: function(dataToCheck) {
                    var view = this;
                    if (dataToCheck) {
                        dataToCheck.forEach(function(d) {
                            if (d && d.Price.data.length === 0 && view._data && view._data.length > 0) {
                                var dummyData = [];
                                view._data.forEach(function(dum) {
                                    dummyData.push({
                                        close: NaN,
                                        index: dum.index,
                                        x: dum.x,
                                        open: NaN,
                                        high: NaN,
                                        low: NaN,
                                        volume: NaN
                                    });
                                });
                                d.Price.data = dummyData;
                            }
                        });
                    }
                },
                _commonDraw: function(config) {
                    this._commonPlotter.setConfig(this._getDefaultGraphConfig());
                    this._commonPlotter.extendConfig(config);
                    return this._commonPlotter.draw();
                },
                _commonDrawTrackball: function(config) {
                    !config.trackType && (config.trackType = 'circle');
                    !config.data && (config.data = this._data);
                    !config.trackContainer && (config.trackContainer = this._getDefaultGraphConfig().containerGElem);
                    config.elem = this._commonPlotter.drawTrackball(config);
                    (this._trackBalls || (this._trackBalls = [])).push(config);
                },
                _showTrackball: function(dataIndex) {
                    var view = this,
                        yValue, elem;
                    this._elementsMapping && this._elementsMapping.forEach(function(mapping) {
                        elem = mapping[dataIndex];
                        mapping.lastTrackElem && mapping.lastTrackElem.classed('track-high-light', false);
                        elem && elem.classed('track-high-light', true);
                        mapping.lastTrackElem = elem;
                    });

                    this._trackBalls && this._trackBalls.forEach(function(trackballInfo) {
                        if (trackballInfo.data) {
                            if (trackballInfo.data[dataIndex] && !isNaN(yValue = trackballInfo.data[dataIndex].y)) {
                                if (trackballInfo.showFunc) {
                                    trackballInfo.showFunc(trackballInfo, dataIndex);
                                } else {
                                    var elem = trackballInfo.elem;
                                    elem.attr('cx', view._xScale(dataIndex))
                                    elem.attr('cy', (trackballInfo.yScale || view._yScale)(yValue))
                                    elem.style("display", "");
                                }
                            } else {
                                trackballInfo.elem.style("display", "none");
                            }
                        }
                    });
                },
                _clearTrackball: function() {
                    if (!this._needTrackball) {
                        this._hideTrackball();
                        this._elementsMapping = undefined;
                        this._trackBalls = undefined;
                    }
                },
                _hideTrackball: function() {
                    this._elementsMapping && this._elementsMapping.forEach(function(mapping) {
                        mapping.lastTrackElem && mapping.lastTrackElem.classed('track-high-light', false);
                        mapping.lastTrackElem = undefined;
                    });

                    this._trackBalls && this._trackBalls.forEach(function(trackballInfo) {
                        trackballInfo.elem.style("display", "none");
                    });
                },
                _drawGridLines: function() {
                    if (this.config.showGridLines) {
                        var graphConfig = {
                            graphCss: this._innerClass.gridLines,
                            graphType: graphTypes.GridLines,
                            xTicks: this._xTickInfos && this._xTickInfos.tickValues,
                            yTicks: this._yTickValues,
                            showGridBlocks: this.config.showGridBlocks,
                            showSolidGridLines: this.config.showSolidGridLines
                        };

                        this._commonDraw(graphConfig);
                    }
                },
                _changeXAxisScaleDomain: function(newXDomainValue, lastDateRange, isZoomIn) {
                    this._lastDateRange = lastDateRange;
                    if (this._changeScaleDomain(newXDomainValue, isZoomIn)) {
                        this._displayViewAfterChangeDomain();
                    }
                },
                _changeChartType: function(chartType) {
                    var processFunc = this["_draw_" + chartType];
                    if (processFunc) {
                        this._clearChartGraphs();
                        processFunc.call(this, chartType);
                    }
                },
                _createSvgWrapper: function(elemFlag) {
                    var cfg = this.config;
                    var svg = this._svg = d3.select(cfg.container).append('div')
                        .classed(this._innerClass.chartBody, true)
                        .classed(elemFlag + '-class', true)
                        .style("width", cfg.width + "px").style("height", cfg.height + "px")
                        .append("svg").attr("width", cfg.width).attr("height", cfg.height);
                    this._yAxisOuterGElem = svg.append("g").attr("class", this._innerClass.yAxis);
                    this._outerGElem = svg.append("g")
                        .attr("transform", this._getGElemTranslate()).attr("class", "chart-gelm");
                    this._containerOffset = $(cfg.container).offset();
                },
                _clearChartGraphs: function() {
                    for (var key in MainChartTypes) {
                        var elem = this["_elem_" + chartTypes[key]];
                        elem && elem.remove();
                    }
                },
                _clearView: function(chartType) {
                    if (this._svg) {
                        this._svg.selectAll("g").remove();
                    }

                    this._clearTrackball();
                },
                _showLoading: function() {
                    eventManager.trigger(eventManager.EventTypes.StartLoading);
                },
                _hideLoading: function() {
                    eventManager.trigger(eventManager.EventTypes.EndLoading);
                },
                _removeIndicators: function(event) {
                    eventManager.trigger(eventManager.EventTypes.ChangeIndicators, true, event.data.toRemove, event.data.indicatorParameters, event.data.removeAll, event.data.isFromCloseBtn);
                },
                _rebaseFundamentalData: function() {
                    var view = this;
                    if (!view._fundamentalsData || view._fundamentalsData.length <= 0) {
                        return;
                    }

                    var mainTickerXLength = view._data ? view._data.length : 0;
                    for (var fundamental in view._fundamentalsData) {
                        if (view._fundamentalsData.hasOwnProperty(fundamental)) {
                            if (view._fundamentalsData[fundamental].data && view._fundamentalsData[fundamental].data.length > 0) {
                                var fundaData = view._fundamentalsData[fundamental].data;
                                if (fundaData.length !== mainTickerXLength) {
                                    fundaData = view._baselineXCordinates(fundaData, view._data);
                                }
                                fundaData.forEach(function(d) {
                                    d.x = d.index;
                                    d.value = d.value;
                                    d.y = d.value;
                                });
                                view._fundamentalsData[fundamental].data = fundaData;
                            }
                        }
                    }
                },
                _rebaseCompareData: function() {
                    var view = this;
                    var mainTickerData = view._data;
                    if (!mainTickerData || mainTickerData.length === 0) {
                        return;
                    }
                    var mainTickerXLength = view._data ? view._data.length : 0;
                    var rootConfig = chartConfigManager.getConfig();
                    var startDate = mainTickerData[0].date;
                    var startIndex = dateUtil.toDateIndex(startDate);
                    var endDate = mainTickerData[mainTickerXLength - 1].date;
                    var endIndex = dateUtil.toDateIndex(endDate);
                    var changeDateRange = false;
                    var rootCfg = chartConfigManager.getConfig();
                    var baseData = view._data; //line with the oldest history data 
                    var newestData = view._data; //line with the latest update data
                    var baseDataChanged = false;
                    view._rebaseCompareTime = view._rebaseCompareTime || 0;
                    view._compareData && view._compareData.forEach(function(c, cIndex) {
                        if (c.Price.data.length > 0) {
                            var compareData = c.Price.data,
                                compareDataLength = compareData.length,
                                cStartIndex = dateUtil.toDateIndex(compareData[0].date),
                                cEndIndex = dateUtil.toDateIndex(compareData[compareDataLength - 1].date);
                            var startDateCfg = rootCfg.startDate;
                            if (rootConfig.AlignToCompare && chartConfigManager.getMainDataType() === MainDataTypes.Growth10K && chartConfigManager.isITFund()) {
                                startDateCfg = StartDateTypes.Common;
                            }
                            if (compareData[0].date) {
                                if (startDateCfg === StartDateTypes.Common) {
                                    if (cStartIndex !== startIndex) {
                                        startIndex = Math.max(cStartIndex, startIndex);
                                        startDate = dateUtil.toDateFromIndex(startIndex);
                                        baseData = compareData;
                                        baseDataChanged = true;
                                        changeDateRange = true;
                                    }
                                    if (cEndIndex !== endIndex) {
                                        endIndex = Math.min(cEndIndex, endIndex);
                                        endDate = dateUtil.toDateFromIndex(endIndex);
                                        baseData = compareData;
                                        baseDataChanged = true;
                                        changeDateRange = true;
                                    }
                                } else if (startDateCfg === StartDateTypes.Earliest) {
                                    if (cStartIndex < startIndex) {
                                        startIndex = cStartIndex;
                                        baseData = compareData;
                                        baseDataChanged = true;
                                    }
                                    if (cEndIndex.dateIndex > endIndex) {
                                        endIndex = cEndIndex;
                                        newestData = compareData;
                                        // baseDataChanged = true;
                                    }
                                } else { // "mainTicker"
                                    if (cStartIndex < startIndex || cEndIndex > endIndex) {
                                        changeDateRange = true;
                                    }
                                }
                            }
                        }
                    });
                    if (changeDateRange && view._rebaseCompareTime <= 2) {
                        view._rebaseCompareTime++;
                        var dataRange = [startDate, endDate];
                        view._updateViewData(dataRange);
                    } else {
                        view._rebaseCompareTime > 0 && view._rebaseCompareTime--;
                        changeDateRange = false;
                        var toBeBasedData = view._compareData || [];
                        for (var i = 0; i < toBeBasedData.length; i++) {
                            var basedData = view._baselineXCordinates(toBeBasedData[i].Price.data, baseData, newestData);
                            basedData.forEach(function(d) {
                                d.x = d.index;
                                d.y = d[view.yAttr];
                            });
                            toBeBasedData[i].Price.data = basedData;
                        }
                        view._compareData = toBeBasedData;
                        if (baseDataChanged) {
                            view._data = view._baselineXCordinates(view._data, baseData, newestData);
                        }
                    }

                    return changeDateRange; //Abort current draw graph thread
                },
                _getWeekInYear: function(date) {
                    var date2 = new Date(date.getFullYear(), 0, 1);
                    var day1 = date.getDay();
                    var day2 = date2.getDay();
                    var d = Math.round((date.getTime() - date2.getTime() + (day2 - day1) * (24 * 60 * 60 * 1000)) / 86400000);
                    return Math.ceil(d / 7) + 1;
                },

                _baselineXCordinates: function(toBaseline, baseData, newestData) {
                    var baseLen = baseData.length;
                    var baseDataPos = 0; //because data is sorted by date. we can optimize it.
                    var toBaseLen = toBaseline.length;
                    var rootCfg = chartConfigManager.getConfig();
                    var mainDateIndex;
                    var basedResult = [];
                    var lastValidY = NaN;
                    if (newestData && newestData[newestData.length - 1].dateIndex > baseData[baseLen - 1].dateIndex) {
                        for (var i = 0; i < newestData.length; i++) {
                            if (newestData[i].dateIndex > baseData[baseLen - 1].dateIndex) {
                                baseData.push({
                                    'dateIndex': newestData[i].dateIndex,
                                    'date': newestData[i].date,
                                    'index': i,
                                    'close': NaN,
                                    'y': NaN,
                                    'value': NaN,
                                    'lastValidY': baseData[baseLen - 1][this.yAttr]
                                });
                            }
                        }
                        baseLen = baseData.length;
                    }
                    if (toBaseLen > 0) {
                        for (var i = 0, mainItem, baseItem; i < baseLen; i++) {
                            mainItem = baseData[i];
                            for (var j = baseDataPos; j < toBaseLen; j++) {
                                baseItem = toBaseline[j];
                                mainDateIndex = mainItem.dateIndex;
                                var match = false;
                                if (this.frequency === 'w') {
                                    if (('' + baseItem.date.getFullYear() + this._getWeekInYear(baseItem.date)) === ('' + mainItem.date.getFullYear() + this._getWeekInYear(mainItem.date))) {
                                        match = true;
                                    }
                                } else if (baseItem.dateIndex === mainDateIndex) {
                                    match = true;
                                }
                                if (match) {
                                    baseItem.index = i; //Not use baseData[w].index.
                                    basedResult.push(baseItem);
                                    lastValidY = baseItem[this.yAttr];
                                    baseDataPos++;
                                    break;
                                } else if (baseItem.dateIndex > mainDateIndex) {
                                    basedResult.push({
                                        'dateIndex': mainDateIndex,
                                        'date': mainItem.date,
                                        'index': i,
                                        'close': NaN,
                                        'y': NaN,
                                        'value': NaN,
                                        'lastValidY': lastValidY
                                    });
                                    break;
                                }
                            }
                        }
                    }

                    return basedResult;
                },
                _appendHeaderForIndicators: function(currentIndicatorName) {
                    if (!this._headerContainer) {
                        return;
                    }

                    var cfg = this.config;
                    var view = this;
                    var $headerContainer = $(this._headerContainer[0]);
                    var parentSpan = $headerContainer.find("." + this._innerClass.indicators);
                    if (parentSpan) {
                        parentSpan.empty();
                    }

                    if (!view._indicatorsData) {
                        return;
                    }

                    chartConfigManager.getConfig().indicators.forEach(function(indicatorObj) {
                        if (cfg.indicatorProperties.hasOwnProperty(indicatorObj.name)) {
                            view._constructIndicatorLegend(parentSpan, indicatorObj, currentIndicatorName);
                        }
                    });
                    var _headerHeight = $headerContainer.outerHeight(true);
                    if (this._headerHeight !== _headerHeight) {
                        this._headerHeight = _headerHeight;
                    }
                },
                _createLegendSpan: function(parentSpan, indicatorName, indicatorLabel, pIndex, globalGraphClass, parameter) {
                    var cfg = this.config;
                    var view = this;
                    var legendClass = "legend" + indicatorName + pIndex;
                    var legendSpan = parentSpan.find("." + legendClass);

                    var $closeElem;
                    if (legendSpan.length === 0) {
                        var legendCon = $("<span class='indicator-legend-" + pIndex + "'>");
                        globalGraphClass && legendCon.addClass(globalGraphClass);
                        if (cfg.showLegends) {
                            legendCon.append("<span class='" + this._innerClass.indicatorsLegend + "'>");
                        }

                        legendCon.append("<span class='" + legendClass + "'>");
                        legendCon.append("<span class='" + chartConfigManager.getConfig().cursorValueClass + ' ' + this._innerClass.indicatorsValue + legendClass.replace('legend', '') + "'>");
                        if (cfg.showIndicatorCloseButton && indicatorLabel) {
                            $closeElem = $("<span class='" + this._innerClass.closeButton + "'>&#215</span>").appendTo(legendCon);
                        }

                        legendCon.appendTo(parentSpan);
                    }

                    legendSpan = parentSpan.find("." + legendClass);
                    if (legendSpan && indicatorLabel) {
                        legendSpan.addClass(this._innerClass.indicatorsLegendText)
                            .css("color", cfg.indicatorProperties[indicatorName].color[pIndex])
                            .text(indicatorLabel);
                        if (cfg.showLegends) {
                            legendSpan.prev().css("background", cfg.indicatorProperties[indicatorName].color[pIndex]);
                        }
                        if (cfg.showIndicatorCloseButton) {
                            $closeElem && $closeElem.bind('click', {
                                toRemove: indicatorName,
                                indicatorParameters: parameter,
                                removeAll: cfg.indicatorProperties[indicatorName].removeAll === true,
                                isFromCloseBtn: true
                            }, view._removeIndicators);
                        }
                    }
                },
                /**
                 * @function Create indicator legend
                 * @param parentSpan    Indicatoe legend parent dom
                 * @param indicatorObj  A object contains name/parameters
                 * @param currentIndicatorName Identified current indicator in loop
                 * 
                 * @return void
                 */
                _constructIndicatorLegend: function(parentSpan, indicatorObj, currentIndicatorName, globalGraphClassList) {
                    var cfg = this.config;
                    var view = this;
                    var indicatorName = indicatorObj.name;
                    var parameters = indicatorObj.parameters;
                    if (cfg.indicatorProperties.hasOwnProperty(indicatorName)) {
                        if ((typeof(currentIndicatorName) === 'undefined' || currentIndicatorName === indicatorName) &&
                            (view._indicatorsData[indicatorName])) {

                            var indicatorLabelArr = view._getIndicatorLabelsArr(indicatorObj);
                            if ($.isArray(indicatorLabelArr)) {
                                indicatorLabelArr.forEach(function(indicatorLabel, pIndex) {
                                    if (globalGraphClassList && globalGraphClassList.length > 0) {
                                        view._createLegendSpan(parentSpan, indicatorName, indicatorLabel, pIndex, globalGraphClassList[pIndex], parameters[pIndex]);
                                    } else {
                                        var globalGraphClass = view._getGlobalClass();
                                        view._createLegendSpan(parentSpan, indicatorName, indicatorLabel, pIndex, globalGraphClass, parameters[pIndex]);
                                    }
                                });
                            }
                        }
                    }
                },
                /**
                 * @function Get indicator labels array, which are define in languageConfig.js
                 * @param indicatorObj  A object contains name/parameters
                 * 
                 * @return lableTextArr
                 */
                _getIndicatorLabelsArr: function(indicatorObj, mainIndicatorProperties) {
                    mainIndicatorProperties = mainIndicatorProperties || chartConfigManager.getConfig().charts.mainChart.indicatorProperties;
                    var cfg = this.config;
                    var lableTextArr = [];

                    var indicatorName = indicatorObj.name;
                    var parameters = indicatorObj.parameters;
                    var indicatorLabels = this.langData[indicatorName];
                    indicatorLabels = $.isArray(indicatorLabels) ? indicatorLabels : [indicatorLabels];

                    // Legend parse function are defined in config.js
                    // if has not, return label in array directly
                    var getLegendArrParse = null;
                    if (cfg.indicatorProperties && cfg.indicatorProperties[indicatorName] && cfg.indicatorProperties[indicatorName].legend) {
                        getLegendArrParse = cfg.indicatorProperties[indicatorName].legend;
                    } else if (mainIndicatorProperties[indicatorName] && mainIndicatorProperties[indicatorName].legend) {
                        getLegendArrParse = mainIndicatorProperties[indicatorName].legend;
                    }

                    if ($.isFunction(getLegendArrParse)) {
                        lableTextArr = getLegendArrParse(indicatorLabels, parameters);
                    } else {
                        lableTextArr = indicatorLabels;
                    }

                    return lableTextArr;
                },
                _clearAllData: function() {
                    this._indicatorsData = null;
                    this._compareData = null;
                    this._dividendEffectData = null;
                    this._eventsData = null;
                    this._data = null;
                },
                _getCurrency: function(tickerObject, currencyField) {
                    if (currencyField === 'currency') { //for cbnca, QS-12844
                        return coreUtil.assignCurrency(tickerObject, chartConfigManager.getConfig());
                    } else {
                        return tickerObject[currencyField];
                    }
                },
                _updateViewData: function(adjustedDateRange) {
                    var view = this;
                    var cfg = this.config;
                    var chartconfig = chartConfigManager.getConfig();
                    var $chartContainer = chartconfig.$container;
                    view._showLoading();
                    view._loading = true;
                    view._hideDataNotAvailable();
                    this._cancelNotRunedPreload();
                    chartConfigManager.fetchMainTickerData(function(res, preloadInfo) {
                        view._clearAllData();
                        view._clearView();
                        if (res.status().errorCode === "0") {
                            view._preloadInfo = preloadInfo;
                            var resData = res.data()[0];
                            var data = resData.Price.data;
                            view.frequency = res.req().frequency();
                            view.mainTickerObject = resData.tickerObject;
                            if (data.length > 0) {
                                data = view._reviseData(data);
                                preloadInfo.loadedData = view._data = data;
                                view._lastClosePrice = resData.Price.spos;
                                view._lastOpenPrice = resData.Price.epos;
                                view._lastTradeTime = resData.Price.lastTradeTime;
                                view._priceCurrency = view._getCurrency(resData.tickerObject, resData.Price.currencyField);
                                //view._timeDiff = resData.Price.timeDiff;
                                view._fundamentalsData = resData.Fundamentals;
                                view._eventsData = resData.Events;
                                if (chartConfigManager.getMainDataType() === MainDataTypes.DividendEffect) {
                                    var fundamentalType = dataAdapter.Fundamental.MReturnIndex;
                                    if (resData.Fundamentals && resData.Fundamentals[fundamentalType] &&
                                        resData.Fundamentals[fundamentalType].data) {
                                        view._dividendEffectData = view._reviseDividendEffectData(resData.Fundamentals[fundamentalType].data);
                                    }
                                } else {
                                    view._getCompareData(res.data());
                                    preloadInfo.loadedIndicatorsData = view._indicatorsData = resData.Indicators;
                                    view._createDummyData(view._compareData);
                                    preloadInfo.loadedCompareData = view._compareData;
                                }

                                var changeDateRange = view._rebaseCompareData();
                                if (changeDateRange) {
                                    view._hideLoading();
                                    return;
                                }

                                view._rebaseFundamentalData();
                                if (!view._viewInited) {
                                    view._renderView();
                                } else {
                                    view._updateViewAfterDataChanged();
                                }

                                view._createSelectionMode && view._createSelectionMode();
                                view._loadedData = view._data.slice(); //This because preload may no data, otherwise chart can't pan.

                                if (preloadInfo.intervalType !== IntervalTypes.Max && !chartconfig.useEUTimeSeriesData) {
                                    setTimeout(function() {
                                        view._preloading = true;
                                        view._loadingData = true;
                                        view._fetchPreloadData();
                                    }, view.config.preloadDelay);
                                }
                            }
                        }

                        //this logic just suit main and volume chart.
                        if (!view._data || view._data.length === 0) {
                            view._showDataNotAvailable();
                        }
                        view._hideLoading();
                        view._loading = false;
                        chartconfig = chartConfigManager.getConfig();
                        if (chartconfig.isFullyRendered && !chartconfig.triggeredFullyRendered) {
                            if (typeof chartconfig.onFullyRendered === 'function') {
                                clearTimeout(view._callbackTimer);
                                view._callbackTimer = setTimeout(function() {
                                    chartconfig.onFullyRendered({
                                        component: 'markets-components-svgchart',
                                        width: $chartContainer.width(),
                                        height: $chartContainer.height()
                                    });
                                    chartConfigManager.setConfig({
                                        triggeredFullyRendered: true
                                    });
                                }, 0);
                            }
                        }
                    }, adjustedDateRange);
                },
                _draw_Indicators: function() {
                    var indicatorsData = this._indicatorsData;
                    if (!indicatorsData) {
                        return;
                    }
                    var view = this;
                    var cfg = this.config;
                    this._needTrackball = false;
                    for (var indicatorName in indicatorsData) {
                        if (cfg.indicatorProperties.hasOwnProperty(indicatorName)) {
                            var indicatorData = indicatorsData[indicatorName];
                            if (indicatorName === dataAdapter.IndicatorName.PrevClose &&
                                (cfg.intervalType !== IntervalTypes.OneDay || chartConfigManager.isCompareOrPercentChart())) {
                                continue;
                            }

                            if (!$.isArray(indicatorData)) {
                                indicatorData = [indicatorData];
                            }

                            indicatorData.forEach(function(s, chartIndex) {
                                view._draw_Indicators_graph(s, chartIndex, indicatorName);
                            });
                        }
                    }
                },
                _draw_Indicators_graph: function(s, chartIndex, indicatorName, globalGraphClass) {
                    var cfg = this.config;
                    var index = 0;
                    s.data.forEach(function(d) {
                        d.x = index++;
                        d.y = d.value;
                    });
                    var graphTypes = cfg.indicatorProperties[indicatorName].chartTypes;
                    if (graphTypes) {
                        var typeOfChart;
                        if ($.isArray(graphTypes)) {
                            typeOfChart = graphTypes[chartIndex];
                        } else {
                            typeOfChart = graphTypes;
                        }

                        this["_draw_" + typeOfChart + "_Indicators"](s.data, indicatorName, typeOfChart, chartIndex, globalGraphClass, s);
                    } else {
                        this._draw_lineChart_Indicators(s.data, indicatorName, 'lineChart', chartIndex, globalGraphClass);
                    }

                    this._needTrackball = true;
                    if (this.config.indicatorProperties[indicatorName].showLegend === true) {
                        var legendLineProperties = this.config.indicatorProperties[indicatorName];
                        var legendPostText = ' | ';
                        if ($.isFunction(legendLineProperties.legend)) {
                            legendPostText += legendLineProperties.legend([this.langData[indicatorName]]);
                        } else {
                            legendPostText += legendLineProperties.legend;
                        }
                        this._draw_LineLegend(s.data, 'lineChart', legendLineProperties.color[0], legendPostText);
                    }
                },
                _draw_dotChart_Indicators: function(data, indicatorName, chartType, chartIndex, globalGraphClass) {
                    var cfg = this.config;
                    var colorValue = this.config.indicatorProperties[indicatorName].color[chartIndex];
                    var graphCss = this._innerClass[chartType + "-indicators"] + " " + indicatorName + "-" + chartIndex + "-class";
                    var graphConfig = {
                        data: data,
                        color: colorValue,
                        graphCss: graphCss,
                        insertBeforeCss: this._innerClass.xAxis,
                        graphType: graphTypes.Dot,
                        yAttributeName: "value",
                        dotRadius: cfg.dotChartRadius,
                        globalGraphClass: globalGraphClass

                    };

                    this["_elem_" + chartType] = this._commonDraw(graphConfig);

                    var trackballConfig = {
                        graphCss: graphCss,
                        color: colorValue,
                        data: data,
                        globalGraphClass: globalGraphClass
                    };

                    this._commonDrawTrackball(trackballConfig);

                },
                _draw_LineLegend: function(data, graphType, legendColor, postText) {
                    var cfg = this.config;
                    var graphConfig = {
                        xPosition: this._xScale(data.length * 0.95),
                        yPosition: this._yScale(data[0].y),
                        tagText: data[0].y + postText,
                        graphCssText: this._innerClass.lineLegendTextClass,
                        graphCss: this._innerClass.lineLegendClass,
                        fillColor: legendColor,
                        arrowWidth: cfg.tagProperties.arrowWidth,
                        arrowHeight: cfg.tagProperties.arrowHeight,
                        sideMargin: cfg.tagProperties.sideMargin,
                        textTopMargin: cfg.tagProperties.textTopMargin,
                        graphType: graphTypes.LineLegend,
                        componentContainer: chartConfigManager.getConfig().$componentContainer,
                    };

                    this["_elem_" + graphType] = this._commonDraw(graphConfig);
                },
                _draw_lineChart_Indicators: function(data, indicatorName, graphType, chartIndex, globalGraphClass) {
                    var colorValue = this.config.indicatorProperties[indicatorName].color[chartIndex];
                    var graphCss = this._innerClass.indicators + " " + indicatorName + "-" + chartIndex + "-class";
                    var graphConfig = {
                        data: data,
                        color: colorValue,
                        graphCss: graphCss,
                        insertBeforeCss: this._innerClass.xAxis,
                        yAttributeName: "value",
                        graphType: graphTypes.Line,
                        globalGraphClass: globalGraphClass
                    };

                    this["_elem_" + graphType] = this._commonDraw(graphConfig);

                    var trackballConfig = {
                        graphCss: graphCss,
                        color: colorValue,
                        data: data,
                        yScale: this._yScale,
                        globalGraphClass: globalGraphClass
                    };

                    this._commonDrawTrackball(trackballConfig);
                },
                _mouseDown: function(position) {
                    //d3.event.preventDefault();
                    var view = this;
                    var item = this._findNearItem(position);
                    if (item) {
                        // if the y axis is left outer, need reduce the translate left
                        var translateLeft = 0;
                        if (this.config.yAxis.orient == "left" && this.config.yAxis.position == "outer") {
                            translateLeft = this.config.margin.left + this.config.yAxis.width + this.config.yAxisMargin;
                        }
                        item.mPosition[0] -= translateLeft;
                        this.drawObj.drawType(this._drawingType);
                        this.drawObj.start(item, function(drawType) {
                            view._changeDrawing(drawType);
                        });
                    }
                },
                _getDecimalPlaces: function() {
                    var decimal;
                    var rootCfg = chartConfigManager.getConfig();
                    if ($.isNumeric(this.config.decimalPlaces)) {
                        decimal = this.config.decimalPlaces;
                    } else if ($.isNumeric(rootCfg.decimalPlaces)) {
                        decimal = rootCfg.decimalPlaces;
                    } else {
                        decimal = rootCfg.defaultDecimal;
                    }
                    return decimal;
                },
                _getYAxisDecimalPlaces: function(domain, tickCount, factor) {
                    var decimal;
                    var yAxisCfg = this.config.yAxis,
                        rootCfg = chartConfigManager.getConfig();
                    if ($.isNumeric(yAxisCfg.decimalPlaces)) {
                        decimal = yAxisCfg.decimalPlaces;
                    } else if ($.isNumeric(rootCfg.yAxisDecimal)) {
                        decimal = rootCfg.yAxisDecimal;
                    } else {
                        decimal = rootCfg.defaultDecimal;
                    }
                    var factorDecimal = factor.toString().split(".")[1] && factor.toString().split(".")[1].length;
                    if (factorDecimal > decimal) {
                        decimal = factorDecimal;
                    }
                    return decimal;
                },
                _mouseUp: function(position) {
                    //d3.event.preventDefault();
                    if (this._drawingType) {
                        this.drawObj.end(this._data.length);
                    }
                    this._changeDrawing();
                },
                _toApplyPercentage: function() {
                    var rootCfg = chartConfigManager.getConfig();
                    var isCompare = rootCfg.compareTickers.length > 0 &&
                        chartConfigManager.getMainDataType() !== MainDataTypes.PerformancesGrowth &&
                        chartConfigManager.getMainDataType() !== MainDataTypes.Growth10K && !rootCfg.drawNavAndPriceChart;

                    var isPrice = this._checkIfPriceChart();
                    return isCompare && isPrice;
                },
                _getLegendText: function(tickerObj) {
                    var text = tickerObj['customTickerName'];
                    if (!text) {
                        var defalutField = 'clientTicker';
                        var legendInfoLabel = chartConfigManager.getConfig().legendInfoLabel || defalutField;
                        text = this.getTickerLabelByLegendInfoModel(legendInfoLabel, tickerObj, defalutField) || tickerObj['clientTicker'] || tickerObj['ticker'] || tickerObj['symbol'];
                    }

                    return text;
                },
                /**
                 * @param "clientTikcer", "ticker", "{clientTicker}({MIC-CODE})"
                 * @param tickerObj 
                 * if some one no match, return defalutField,just like clientTicker
                 */
                getTickerLabelByLegendInfoModel: function(fields, tickerObj, defalutField) {
                    var pattern = (/\{[\s\S]+?\}/g);
                    var isAllFieldsExist = true;
                    if (!pattern.test(fields)) {
                        return tickerObj[fields]
                    }
                    var text = fields.replace(pattern, function(word) {
                        name = word.substring(1, word.length - 1);
                        if (tickerObj[name] && tickerObj[name] !== '') {
                            return tickerObj[name];
                        } else {
                            isAllFieldsExist = false;
                        }

                    })
                    return isAllFieldsExist ? text : tickerObj[defalutField];
                },
                _mousemove: function(e, position, offsetTop) {
                    d3.event.preventDefault();
                    if (!this.mainTickerObject || this._panning || this._zooming) {
                        return;
                    }
                    offsetTop = offsetTop || 0;
                    var rootCfg = chartConfigManager.getConfig();
                    var cursorType = rootCfg.cursorType;
                    if (rootCfg.cursorType === 'off' && !this._drawingType) {
                        return;
                    }
                    var yAxisCfg = this.config.yAxis;
                    var indexPosition = [];
                    for (var i = 0; i < position.length; i++) {
                        indexPosition[i] = position[i];
                    }
                    if (yAxisCfg.position == "outer" && yAxisCfg.show && yAxisCfg.orient == "left") {
                        if (position[0] - this.config.yAxisMargin - yAxisCfg.width < 0) {
                            if (cursorType === CursorType.Crosshair) {
                                eventManager.trigger(eventManager.EventTypes.ToggleCrossharis);
                            }
                            return;
                        }
                        indexPosition[0] = position[0] - this.config.yAxisMargin - yAxisCfg.width;
                    }
                    if (rootCfg.hightLightHoveredLine && this._compareData && this._compareData.length > 0 && chartConfigManager.getConfig().growthType !== "priceGrowth") {
                        if ($.isFunction(this._hightLightHoveredLine)) {
                            this._hightLightHoveredLine(indexPosition);
                        };
                    }
                    var dataIndex = this._currentDataIndex = this._findNearIndex(indexPosition)
                    var item = this._findNearItem(indexPosition);
                    var isPrice = this._checkIfPriceChart();

                    var popoverType = isPrice ? chartConfigManager.PopoverTypes.OHLCV : chartConfigManager.PopoverTypes.Growth;
                    var mainDataType = chartConfigManager.getMainDataType();
                    if (mainDataType === MainDataTypes.Growth10K) {
                        popoverType = chartConfigManager.PopoverTypes.TenKGrowth;
                    }
                    if (mainDataType === MainDataTypes.PostTax) {
                        popoverType = chartConfigManager.PopoverTypes.PostTax;
                    }
                    if (rootCfg.crosshairMode === 's') {
                        if (rootCfg.chartGenre === chartConfigManager.ChartGenre.FundChart || !isPrice || mainDataType === MainDataTypes.Growth10K) {
                            popoverType = chartConfigManager.PopoverTypes.Simple;
                        }
                    }
                    this._crossGlobalIndex = -1;
                    var isPercent = this._toApplyPercentage();
                    var nearest = this._findNearestData(item, isPercent, this._lastClosePrice); //this._findNearest(xMouse, isPercent, this._lastClosePrice);
                    if (nearest !== null) {
                        if (this._drawingType) {
                            var d = $.isNumeric(this._decimalPlaces) ? this._decimalPlaces : 2;
                            this.drawObj && this.drawObj.move(item, d);
                        } else if (rootCfg.cursorType !== 'off') {
                            var top = offsetTop,
                                offset = rootCfg.$innerContainer.offset(),
                                left;
                            if (rootCfg.fixHeight) {
                                var chartoffset = offset.top;
                                top += chartoffset;
                            }
                            left = position[0] + $(this._svg[0]).offset().left - offset.left;
                            var args = {
                                'position': {
                                    'top': e.pageY,
                                    'left': left
                                },
                                'data': {
                                    'main': {
                                        'ticker': this._getLegendText(this.mainTickerObject), //.name || this.mainTickerObject.clientTicker,
                                        'globalClass': this._getGlobalClass(this.mainTickerObject, '_crossGlobalIndex'),
                                        'color': this.mainTickerObject['customTickerColor'] || rootCfg.mainTickerColor,
                                        'data': nearest,
                                        'tickerObject': this.mainTickerObject,
                                        'currency': this._priceCurrency || this.mainTickerObject.currency || this.mainTickerObject.customCurrency,
                                        'frequency': chartConfigManager.getConfig().frequency
                                    },
                                    'compare': [],
                                    'indicators': [],
                                    'fundamentals': [],
                                    'events': [],
                                },
                                'popoverType': popoverType
                            };
                            if (offsetTop > 0) {
                                args['offsetTop'] = offsetTop;
                            }
                            //hideBenchmark from user
                            if (this._compareData &&
                                this._compareData.length &&
                                !(chartConfigManager.getMainDataType() === MainDataTypes.Growth && chartConfigManager.isITFund() && rootCfg.hideBenchmark)) {

                                args.data.compare.push.apply(args.data.compare, this._getCompareArray(position, isPercent));
                            }
                            if (this._indicatorsData) {
                                args.data.indicators.push.apply(args.data.indicators, this._getIndicatorsArray(position, isPercent));
                            }
                            if (mainDataType === MainDataTypes.DividendEffect) {
                                args.data.compare.push.apply(args.data.compare, this._getDividendEffectArray(position, isPercent));
                            }
                            if (this._fundamentalsData) {
                                args.data.fundamentals.push.apply(args.data.fundamentals, this._getFundamentalsArray(position, isPercent));
                            }
                            if (this._eventsData) {
                                args.data.events.push.apply(args.data.events, this._getEventsArray(position, isPercent));
                            }

                            if (cursorType === CursorType.Crosshair) {
                                eventManager.trigger(eventManager.EventTypes.ToggleCrossharis, args);
                            } else if (cursorType === CursorType.Trackball) {
                                eventManager.trigger(eventManager.EventTypes.ToggleTrackball, args);
                            }

                            eventManager.trigger(eventManager.EventTypes.ShowTrackball, dataIndex);
                            var volumeCharts = {
                                'main': args.data.main,
                                indicators: []
                            };
                            var indicatorsOnVolumeChart = this._getIndicatorsArray(position, isPercent, rootCfg.charts.volumeChart);
                            if (indicatorsOnVolumeChart) {
                                volumeCharts.indicators = indicatorsOnVolumeChart;
                            }
                            eventManager.trigger(eventManager.EventTypes.ToggleTrackballVolume, volumeCharts);

                            if (this._indicatorsData) {
                                var indicatorsOnIndicatorChart = this._getIndicatorsArray(position, isPercent, rootCfg.charts.indicatorChart);
                                if (indicatorsOnIndicatorChart) {
                                    eventManager.trigger(eventManager.EventTypes.ToggleTrackballIndicators, indicatorsOnIndicatorChart);
                                }
                            }

                            if (this._fundamentalsData) {
                                var fundamentalsChart = this._getFundamentalsArray(position, isPercent, rootCfg.charts.fundamentalChart);
                                if (fundamentalsChart) {
                                    eventManager.trigger(eventManager.EventTypes.ToggleTrackballFundamentals, fundamentalsChart);
                                }
                            }


                            if (this.drawObj && item && (this.drawObj.isEnter(item.mPosition) || this.drawObj.isCloseButton(item.mPosition))) {
                                this._disablePan && this._disablePan();
                                this._disableZoomIn && this._disableZoomIn();
                            } else {
                                if (this.config.selectionMode === SelectionModes.Pan) {
                                    if (!isMobile) {
                                        this._enablePan();
                                    }
                                } else if (this.config.selectionMode === SelectionModes.ZoomIn) {
                                    this._enableZoomIn();
                                }
                            }

                            this._VBPTipProcess && this._VBPTipProcess(position, offsetTop);
                        }
                    }
                    this._currentDataIndex = null;
                },
                _validData: function(xMouse) {
                    var yAttr = this.yAttr;
                    var allData = [this._data];
                    var compareLen = this._compareData && this._compareData.length;
                    for (var i = 0; i < compareLen; i++) {
                        if (this._compareData[i].Price && this._compareData[i].Price.data) {
                            allData.push(this._compareData[i].Price.data);
                        }
                    }
                    for (var i = 0; i < allData.length; i++) {
                        var d = allData[i] && allData[i][xMouse] && allData[i][xMouse][yAttr];
                        if (!isNaN(d)) {
                            return true;
                        }
                    }
                    return false;
                },
                _findNearIndex: function(position) {
                    var dataList = this._data;
                    var allData = [this._data].concat(this._compareData || []);
                    if (!dataList || dataList.length === 0) {
                        return;
                    }
                    var dataLen = dataList.length;
                    var view = this;
                    var oriXMouse = this._xScale.invert(position[0]);
                    var xMouse = Math.round((oriXMouse - this._xScale.domain()[0]));
                    if (xMouse < 0) {
                        xMouse = 0;
                    }
                    if (xMouse >= dataLen) {
                        xMouse = dataLen - 1;
                    }
                    if (!this._validData(xMouse)) {
                        var factor;
                        if (oriXMouse >= xMouse) {
                            factor = -1;
                        } else {
                            factor = 1;
                        }
                        var loop = 1;
                        var leftSearched = false,
                            rightSearched = false;
                        do {
                            factor *= -1;
                            xMouse = xMouse + factor * (loop++);
                            if (xMouse < 0) {
                                leftSearched = true;
                                continue;
                            } else if (xMouse >= dataLen) {
                                rightSearched = true;
                                continue;
                            }
                        } while (!this._validData(xMouse) && (!leftSearched || !rightSearched));
                    }

                    return xMouse;
                },
                _findNearItem: function(position, targetDataList, forceFind) {
                    forceFind = forceFind === true ? true : false; //it is used by drawings
                    targetDataList = targetDataList || this._data;
                    if (targetDataList) {
                        var dataIndex = this._currentDataIndex;
                        (forceFind || isNaN(dataIndex) || dataIndex === null) && (dataIndex = this._findNearIndex(position));
                        var dataItem = targetDataList[dataIndex];
                        return {
                            datapoint: dataItem,
                            mPosition: position
                        };
                    }
                },
                _convertToPrice: function(item, spos) {
                    if (item && item.datapoint) {
                        var datapoint = item.datapoint;
                        return {
                            open: spos * (1 + datapoint.open / 100),
                            close: spos * (1 + datapoint.close / 100),
                            high: spos * (1 + datapoint.high / 100),
                            low: spos * (1 + datapoint.low / 100),
                            y: spos * (1 + datapoint.y / 100),
                            x: datapoint.x,
                            date: datapoint.date,
                            volume: datapoint.volume
                        };
                    }
                },
                _convertToFundPrice: function(item, spos) {
                    if (item && item.datapoint) {
                        var datapoint = item.datapoint;
                        return {
                            value: spos * (1 + datapoint.value / 100),
                            x: datapoint.x,
                            date: datapoint.date,
                        };
                    }
                },
                _convertPercentageToPrice: function(valueToConvert, baseValue) {
                    return baseValue * (1 + valueToConvert / 100);
                },
                _getGlobalClass: function(tickerObj, globalIndexAttr, globalIndex) {
                    var globalClass = '';
                    globalIndexAttr = globalIndexAttr || '_globalGraphIndex';
                    var index;
                    if ($.isNumeric(globalIndex)) {
                        index = globalIndex;
                    } else if ($.isNumeric(this[globalIndexAttr])) {
                        index = this[globalIndexAttr]++;
                    }
                    if ($.isNumeric(index)) {
                        if (!tickerObj || !tickerObj['customTickerColor']) {
                            globalClass = " globalIndex-" + index;
                        }
                    }
                    return globalClass;
                },
                _getCompareArray: function(position, isPercent) {
                    var compareColors = chartConfigManager.getConfig().compareColors;
                    var compareArray = []
                    var view = this;
                    this._compareData.forEach(function(compare, cIndex) {
                        var legendColorIndex = (cIndex >= compareColors.length) ? cIndex % compareColors.length : cIndex;
                        var legendColor = compare.tickerObject['customTickerColor'] || compareColors[legendColorIndex];
                        var compareItem = {
                            ticker: view._getLegendText(compare.tickerObject),
                            data: view._findNearestData(view._findNearItem(position, compare.Price.data), isPercent, compare.Price.spos),
                            color: legendColor,
                            currency: compare.tickerObject.currency || compare.tickerObject.customCurrency,
                            tickerObject: compare.tickerObject,
                            globalClass: view._getGlobalClass(compare.tickerObject, '_crossGlobalIndex'),
                        };
                        var fundamentalsData = {};
                        for (var fundamentalName in compare.Fundamentals) {
                            if (view.config.fundamentalProperties && view.config.fundamentalProperties[fundamentalName] && view.config.fundamentalProperties[fundamentalName].showWithCompare) {
                                fundamentalsData[fundamentalName] = compare.Fundamentals[fundamentalName];
                            }
                        }
                        if (!$.isEmptyObject(fundamentalsData)) {
                            compareItem.fundamentals = view._getFundamentalsArray(position, isPercent, null, fundamentalsData);
                        }
                        compareArray.push(compareItem);
                    });
                    return compareArray;
                },
                _getDividendEffectArray: function(position, isPercent) {
                    var cfg = this.config;
                    var rootCfg = chartConfigManager.getConfig();
                    var dividendEffectArray = []
                    var view = this;
                    var dividendEffectData = view._dividendEffectData ? view._dividendEffectData : view._reviseMainDividendEffectData(view._data);
                    dividendEffectArray.push({
                        ticker: view.mainTickerObject.ticker + this.langData['mainChartHeaderDividendEffectLabel'],
                        data: view._findNearestData(view._findNearItem(position, dividendEffectData), isPercent, view._lastClosePrice),
                        color: rootCfg.charts.mainChart.dividendEffectProperties.color
                    });
                    return dividendEffectArray;
                },
                _normalizeEventsData: function(eventsData) {
                    if (eventsData && eventsData.length > 0 && this._data && this._data.length > 0) {
                        var baseData = this._data;
                        var baseDataLength = baseData.length;
                        var baseDateIndex;
                        eventsData.forEach(function(item) {
                            item.x = -100; //will not draw it.
                            for (var i = 0; i < baseDataLength; i++) {
                                baseDateIndex = baseData[i].dateIndex;
                                if (item.dateIndex === baseDateIndex) {
                                    item.x = i;
                                    break;
                                } else if (item.dateIndex < baseDateIndex) {
                                    if (i === 0) {
                                        item.x = -100;
                                        break;
                                    } else {
                                        item.x = i;
                                        break;
                                    }
                                }
                            }

                            item.y = item.value;
                        });
                    }
                },
                _getEventsArray: function(position, isPercent) {
                    var mainChartConfig = chartConfigManager.getConfig().charts.mainChart;
                    var limitSpan = isNaN(mainChartConfig.eventProperties.xRadious) ? 5 : mainChartConfig.eventProperties.xRadious;
                    var xMouse = this._xScale.invert(position[0]);
                    var eventsArray = [];
                    var view = this;
                    for (var eventName in this._eventsData) {
                        var eventPropertyName = Object.keys(dataAdapter.Events).filter(function(e) {
                            return dataAdapter.Events[e] === eventName
                        })[0];
                        var eventsCopy = view._eventsData[eventName].data.slice();
                        view._normalizeEventsData(eventsCopy);
                        if (mainChartConfig.eventProperties.hasOwnProperty(eventPropertyName)) {
                            if (eventName === dataAdapter.Events.Dividend && !mainChartConfig.showDividend) {
                                continue;
                            }
                            var eventProperty = mainChartConfig.eventProperties[eventPropertyName];
                            var eventLabel = eventProperty.labels || '';
                            var nearestEventItem = view._findNearestEvent(position, eventsCopy, limitSpan);
                            if (eventLabel && nearestEventItem) {
                                var eventData = {
                                    name: this.langData[eventLabel],
                                    data: {
                                        yDate: nearestEventItem.date,
                                        yValue: nearestEventItem.y,
                                        x: nearestEventItem.x
                                    },
                                    color: nearestEventItem.upFlag === 0 ? eventProperty.downColor : eventProperty.color,
                                }
                                if (eventName === dataAdapter.Events.Earnings) {
                                    eventData.data.yFrequency = this.langData[nearestEventItem.frequency];
                                }
                                eventsArray.push(eventData);
                            }
                        }
                    }
                    return eventsArray;
                },
                _findNearestEvent: function(position, eventsData, limitSpan) {
                    var view = this;
                    var nearestEvent = null;
                    if (!eventsData || eventsData.length === 0 || !position || !limitSpan) {
                        return null;
                    }
                    var xMouse = Math.floor(this._xScale.invert(position[0]));
                    var nearestEvents = eventsData.filter(function(e) {
                        var xDataPoint = view._xScale(e.x);
                        if (view.config.yAxis && view.config.yAxis.position == 'outer' && view.config.yAxis.orient == 'left') {
                            var offset = isNaN(view.config.yAxis.width + (view.config.yAxisMargin || 0)) ? 0 : (view.config.yAxis.width + (view.config.yAxisMargin || 0));
                            xDataPoint += offset;
                        }
                        return position[0] >= (xDataPoint - limitSpan) && position[0] <= (xDataPoint + limitSpan);
                    });
                    if (nearestEvents && nearestEvents.length > 0) {
                        nearestEvent = nearestEvents[0];
                    }
                    return nearestEvent;
                },
                _getFundamentalsArray: function(position, isPercent, chartConfigToUse, fundamentalsData) {
                    var chartCfg = chartConfigToUse ? chartConfigToUse : chartConfigManager.getConfig().charts.mainChart;
                    var fundamentalsArray = [];
                    var view = this;
                    fundamentalsData = fundamentalsData || this._fundamentalsData;
                    for (var fundamentalName in fundamentalsData) {
                        if (chartCfg.fundamentalProperties.hasOwnProperty(fundamentalName)) {
                            var fundamentalConfig = chartConfigManager.getConfig().fundamentals.filter(function(f) {
                                return f === fundamentalName;
                            });
                            var fundamentalLabel = view.langData[util.lowerFirstChar(fundamentalName)] || fundamentalName;
                            if (fundamentalLabel) {
                                var fundamentalData = fundamentalsData[fundamentalName].data;
                                fundamentalData.forEach(function(d, i) {
                                    d.y = d.value;
                                });
                                fundamentalsArray.push({
                                    decimalPlaces: chartCfg.fundamentalProperties[fundamentalName].decimalPlaces,
                                    ticker: fundamentalLabel,
                                    name: fundamentalName,
                                    showLegend: chartCfg.fundamentalProperties[fundamentalName].showLegend,
                                    data: view._findNearestData(view._findNearItem(position, fundamentalData), false, fundamentalsData[fundamentalName].spos),
                                    color: chartCfg.fundamentalProperties[fundamentalName].color,
                                    globalClass: !chartCfg.fundamentalProperties[fundamentalName].skipDrawGraph && view._getGlobalClass(null, '_crossGlobalIndex')
                                });
                            }
                        }
                    }
                    return fundamentalsArray;
                },

                _getIndicatorsArray: function(position, isPercent, chartConfigToUse) {
                    var mainChartConfig = chartConfigManager.getConfig().charts.mainChart;
                    var chartCfg = chartConfigToUse ? chartConfigToUse : mainChartConfig;
                    var indicatorsArray = [];
                    var view = this;
                    for (var indicatorName in this._indicatorsData) {
                        if (chartCfg.indicatorProperties.hasOwnProperty(indicatorName)) {
                            var indicatorConfig = chartConfigManager.getConfig().indicators.filter(function(i) {
                                return i.name === indicatorName;
                            });

                            // code for drawing legend, and fill data
                            var indicatorObj = indicatorConfig.length > 0 ? indicatorConfig[0] : [];
                            var indicatorLabelArr = view._getIndicatorLabelsArr(indicatorObj, chartCfg.indicatorProperties);
                            if ($.isArray(indicatorLabelArr)) {
                                indicatorLabelArr.forEach(function(indicatorLabel, pIndex) {
                                    view._prepareIndicatorsArray(indicatorsArray, indicatorName, pIndex, indicatorLabel, position, isPercent, chartCfg);
                                });
                            }
                        }
                    }
                    return indicatorsArray;
                },
                _prepareIndicatorsArray: function(indicatorsArray, indicatorName, chartIndex, indicatorLabel, position, isPercent, chartCfg) {
                    if (indicatorName === dataAdapter.IndicatorName.VBP) {
                        return;
                    }
                    var view = this;
                    var indicatorData = view._indicatorsData[indicatorName][chartIndex].data;
                    indicatorData.forEach(function(d, i) {
                        d.y = d.value;
                    });
                    indicatorsArray.push({
                        ticker: indicatorLabel,
                        name: indicatorName,
                        data: view._findNearestData(view._findNearItem(position, indicatorData), isPercent, view._indicatorsData[indicatorName][chartIndex].spos),
                        color: chartCfg.indicatorProperties[indicatorName].color[chartIndex],
                        globalClass: view._getGlobalClass(null, '_crossGlobalIndex') //' globalIndex-' + ($.isNumeric(this._crossGlobalIndex) ? this._crossGlobalIndex++ : '')
                    });
                },
                _findNearestData: function(item, isPercentValue, spos) {
                    if (item) {
                        return isPercentValue ? this._convertToPrice(item, spos) : item.datapoint;
                    }
                },
                _showDataNotAvailable: function(type) {
                    if (!this._svg) {
                        return;
                    }
                    this._clearView();
                    var elem = this._svg.append('g').append('text').attr("transform", "translate(" + this._validWidth / 2 + "," + this.config.height / 2 + ")");
                    var message = this.langData['datanotavailable'];
                    if (type == "changeToDailyFreq") {
                        message = this.langData['changetodailyfrequency'];
                    }
                    var textWidth = this._getTextWidthHeight(message).width;
                    //text need to be wraped when too long
                    if (textWidth > (this._validWidth - 20)) {
                        var mesArr = message.split('|');
                        for (var i in mesArr) {
                            elem.append('tspan')
                                .attr('class', 'dataNotAvailable')
                                .attr('dy', i * 1.5 + 'em')
                                .attr('x', '0')
                                .text(mesArr[i]);
                        }
                    } else {
                        message = message.replace('|', '');
                        elem.append('tspan')
                            .attr('class', 'dataNotAvailable')
                            .text(message);
                    }
                    this._$DataNotAvailableElem = elem;
                },
                _getTextWidthHeight: function(text) {
                    var $textSpan = $('<span style="display:inline-block;">' + text + '</span>').appendTo(this.config.container);
                    var width = $textSpan.outerWidth(true);
                    var height = $textSpan.outerHeight(true);
                    $textSpan.remove();
                    return {
                        width: width,
                        height: height
                    };
                },
                _hideDataNotAvailable: function() {
                    this._$DataNotAvailableElem && this._$DataNotAvailableElem.remove();
                },
                _draw_Fundamentals: function(renderView, fundamentalStartIndex) {
                    var fundamentalsData = this._fundamentalsData;
                    var view = this;
                    var cfg = this.config;
                    this._needTrackball = false;
                    chartConfigManager.getConfig().fundamentals.forEach(function(fundamentalName, chartIndex) {
                        if (cfg.fundamentalProperties.hasOwnProperty(fundamentalName) && !cfg.fundamentalProperties[fundamentalName].skipDrawGraph) {
                            if (renderView) {
                                view._renderView(fundamentalName);
                            }

                            var fundamental = fundamentalsData && fundamentalsData[fundamentalName];
                            if (fundamental && !fundamental.DailyDataPoint) {
                                var fundamental = fundamentalsData[fundamentalName];
                                if (fundamental && fundamental.data && fundamental.data.length > 0) {
                                    var index = 0;
                                    fundamental.data.forEach(function(d) {
                                        d.x = index++;
                                        d.y = d.value;
                                    });
                                    view._draw_lineChart_Fundamentals(fundamental.data, fundamentalName, 'lineChart', chartIndex, fundamentalStartIndex++);
                                    view._needTrackball = true;
                                }
                            } else {
                                eventManager.trigger(eventManager.EventTypes.FundamentalNotAvailable, true);
                                if (view._isFundamentalChart) {
                                    if (fundamental && fundamental.DailyDataPoint) {
                                        view._showDataNotAvailable('changeToDailyFreq');
                                    } else {
                                        view._showDataNotAvailable();
                                    }
                                }
                            }
                        }
                    });
                },
                _draw_lineChart_Fundamentals: function(data, fundamentalName, chartType, chartIndex, globalIndex) {
                    var graphCss = this._innerClass.fundamentals + " " + fundamentalName +
                        "-" + chartIndex + "-class" + " globalIndex-" + globalIndex;

                    var graphConfig = {
                        data: data,
                        color: this.config.fundamentalProperties[fundamentalName].color,
                        graphCss: graphCss,
                        insertBeforeCss: this._innerClass.xAxis,
                        xAttributeName: "x",
                        yAttributeName: "y",
                        graphType: graphTypes.Line,
                    };

                    this["_elem_" + chartType] = this._commonDraw(graphConfig);

                    var trackballConfig = {
                        graphCss: graphCss,
                        color: this.config.fundamentalProperties[fundamentalName].color,
                        data: data,
                        yScale: this._yScale
                    };

                    this._commonDrawTrackball(trackballConfig);
                },
                _appendHeaderForFundamentals: function(fundamentalName) {
                    if (!this._headerContainer) {
                        return;
                    }

                    var cfg = this.config;
                    var view = this;
                    var $headerContainer = $(this._headerContainer[0]);
                    var parentSpan = $headerContainer.find("." + this._innerClass.fundamentals);
                    if (parentSpan) {
                        parentSpan.empty();
                    }

                    chartConfigManager.getConfig().fundamentals.forEach(function(f) {
                        if (cfg.fundamentalProperties.hasOwnProperty(f)) {
                            if (fundamentalName) { // drawing fundmental chart header legend
                                view._constructFundamentalLegend(parentSpan, fundamentalName);
                            } else if (view._fundamentalsData && view._fundamentalsData[f] && view._fundamentalsData[f].data && view._fundamentalsData[f].data.length) { //drawing main chart header legend when fundamental data is available
                                view._constructFundamentalLegend(parentSpan, f);
                            }
                        }
                    });
                    var _headerHeight = $headerContainer.outerHeight(true);
                    if (this._headerHeight !== _headerHeight) {
                        this._headerHeight = _headerHeight;
                    }
                },
                _constructFundamentalLegend: function(parentSpan, fundamentalName) {
                    var cfg = this.config;
                    var view = this;
                    if (parentSpan && fundamentalName && !cfg.fundamentalProperties[fundamentalName].skipDrawGraph) {
                        var legendClass = "legend" + fundamentalName;
                        var legendSpan = parentSpan.find("." + legendClass);
                        var fundamentalLable = this.langData[fundamentalName];
                        if (legendSpan.length === 0) {
                            var $legendCon;
                            if ($.isNumeric(view._globalGraphIndex)) {
                                $legendCon = $("<span class='globalIndex-" + (view._globalGraphIndex++) + "'>").appendTo(parentSpan);
                            } else {
                                $legendCon = parentSpan;
                            }

                            if (cfg.showLegends) {
                                $legendCon.append("<span class='" + view._innerClass.fundamentalsLegend + "'>");
                            }
                            $legendCon.append("<span class='" + legendClass + "'>");
                            $legendCon.append("<span class='" + chartConfigManager.getConfig().cursorValueClass + ' ' + view._innerClass.fundamentalsValue + legendClass.replace('legend', '') + "'>");
                            if (cfg.showFundamentalCloseButton && fundamentalLable) {
                                $legendCon.append("<span class='" + view._innerClass.closeButton + "'>&#215</span>");
                            }
                        }

                        legendSpan = parentSpan.find("." + legendClass);
                        if (legendSpan && fundamentalLable) {
                            legendSpan.addClass(view._innerClass.fundamentalsLegendText)
                                .css("color", cfg.fundamentalProperties[fundamentalName].color)
                                .text(fundamentalLable);
                            if (cfg.showLegends) {
                                legendSpan.prev().css("background", cfg.fundamentalProperties[fundamentalName].color);
                            }
                            if (cfg.showFundamentalCloseButton) {
                                legendSpan.next().next().bind('click', {
                                    toRemove: fundamentalName
                                }, view._removeFundamentals);
                            }
                        }
                    }
                },
                _removeFundamentals: function(event) {
                    eventManager.trigger(eventManager.EventTypes.ChangeFundamental, event.data.toRemove, true);
                }
            };

            return util.extendClass(baseView, baseChartExtend);
        }

        return model;
    });