define(["../../../core/core"], function (core) {
    "use strict";
    var d3 = core.d3;

    function candlestickSeries() {
        var xScale, yScale;
        var cacheData, cacheScale;
        var tickSize = 5;
        var width;
        var xAttributeName, yAttributeName = "close";
        var getElementsMapping;
        var isUpDay = function (d) {
            return d.close > d.open;
        };

        var isDownDay = function (d) {
            return !isUpDay(d);
        };

        var line = d3.svg.line()
            .x(function (d) {
                return d.x;
            })
            .y(function (d) {
                return d.y;
            });

        var highLowLines = function (bar) {
            var paths = bar.append("path")
                .classed({ "high-low-line": true })
                .attr("d", function (d) {
                    var x = xScale(d[xAttributeName]);
                    return line([
                        { x: x, y: yScale(d.high) },
                        { x: x, y: yScale(d.low) }
                    ]);
                });
        };

        var rectangles = function (bar) {
            var rect = bar.append('rect')
                .attr("x", function (d) { return xScale(d[xAttributeName]) - tickSize / 2; })
                .attr("y", function (d) { return isUpDay(d) ? yScale(d.close) : yScale(d.open); })
                .attr("width", tickSize)
                .attr("height", function (d) {
                    return Math.max(0.5, isUpDay(d) ? (yScale(d.open) - yScale(d.close)) : (yScale(d.close) - yScale(d.open)));
                });
        };

        /*var zoom = d3.behavior.zoom()
            .x(xScale)
            .scaleExtent([1, 50])
            .on('zoom', zoomed)
            .on('zoomend', zoomend);*/

        var candlestick = function (selection) {
            var outerElem, bar;

            selection.each(function (dataArr) {
                //cacheData = data;  ??
                //cacheScale = yScale.copy();
                if (!xAttributeName) {
                    xAttributeName = "_x";
                    dataArr.forEach(function (dataItem, index) {
                        dataItem[xAttributeName] = index;
                    });
                }

                outerElem = d3.select(this).append("g").classed("candlestick-series", true);
                var indexElementsMapping = {};
                dataArr.forEach(function (dataItem, dataIndex) {
                    if (!isNaN(dataItem[yAttributeName])) {
                        var bar = outerElem.append("g").data([dataItem])
                            .classed({
                                "bar": true,
                                "up": isUpDay,
                                "down": isDownDay
                            });

                        highLowLines(bar);
                        rectangles(bar);
                        indexElementsMapping[dataIndex] = bar;
                    }
                });

                getElementsMapping && getElementsMapping(indexElementsMapping);
            });
        };

        //candlestick.zoom = function (selection) {
        //    var xDomain = xScale.domain();
        //    var xRange = xScale.range();
        //    var translate = zoom.translate()[0];
        //}

        candlestick.xScale = function (value) {
            if (!arguments.length) {
                return xScale;
            }
            xScale = value;
            return candlestick;
        };

        candlestick.yScale = function (value) {
            if (!arguments.length) {
                return yScale;
            }
            yScale = value;
            return candlestick;
        };

        candlestick.tickSize = function (value) {
            if (!arguments.length) {
                return tickSize;
            }
            tickSize = value;
            return candlestick;
        };

        candlestick.width = function (value) {
            if (!arguments.length) {
                return width;
            }
            width = value;
            return candlestick;
        };


        candlestick.xAttributeName = function (value) {
            if (!arguments.length) {
                return xAttributeName;
            }
            xAttributeName = value;
            return candlestick;
        };

        candlestick.yAttributeName = function (value) {
            if (!arguments.length) {
                return yAttributeName;
            }
            yAttributeName = value;
            return candlestick;
        };

        candlestick.getElementsMapping = function (value) {
            if (!arguments.length) {
                return getElementsMapping;
            }
            getElementsMapping = value;
            return candlestick;
        };

        return candlestick;
    }

    return candlestickSeries;
});
