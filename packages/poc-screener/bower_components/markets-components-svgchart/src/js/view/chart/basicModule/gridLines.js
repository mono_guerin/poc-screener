define(["../../../core/core"], function (core) {
    "use strict";
    var d3 = core.d3;

    return function () {
        var xScale, yScale, xTicks, yTicks;
        var showGridBlocks = false;
        var showSolidGridLines = false;
        var blockXArray = [], blockYArray=[];

        var drawRectangle = function (bar, x, width, height) {
            var rect = bar.select("rect");
            if (rect.empty()) {
                rect = bar.append('rect');
            }
            rect.attr("x", x)
                .attr("y", 0)
                .attr("width", width)
                .attr("height", height);
        };

        var drawBackgroudByxTickData = function(scope, xTickData, yTickData){
            var outerElem, bar;

            for (var i = 0; i < xTickData.length; i++) {
                outerElem = d3.select(scope);
                bar = outerElem.append("g").data(xTickData)
                    .classed({
                        "gridBlock": true,
                    });

                var x = blockXArray[i];
                var width = (blockXArray[i+1]?blockXArray[i+1]:10000) - blockXArray[i];
                var height = blockYArray[i];
                drawRectangle(bar, x, width, height);
            };
        };


        var xLines = function (data, grid) {
            var xlines = grid.selectAll(".grid-x").data(data);
            xlines.enter().append("line");
            xlines.attr({
                "class": "grid-x",
                "x1": function (d) {
                    var xValue = xScale(d);
                    blockXArray.push(xValue);
                    return xValue;
                },
                "x2": function (d) {
                    return xScale(d);
                },
                "y1": function(){
                    var yValue = yScale.range()[0];
                    blockYArray.push(yValue);
                    return yValue;
                },
                "y2": yScale.range()[1]
            });

            xlines.exit().remove();
        };

        var yLines = function (data, grid) {
            var ylines = grid.selectAll('.grid-y').data(data);

            ylines.enter()
                .append('line')
                .attr({
                    'class': 'grid-y',
                    'x1': xScale.range()[0],
                    'x2': xScale.range()[1],
                    'y1': function (d) {
                        return yScale(d);
                    },
                    'y2': function (d) {
                        return yScale(d);
                    }
                });
        };

        var gridlines = function (selection) {
            var grid, xTickData, yTickData;
            selection.each(function () {
                xTickData = xTicks ? (Array.isArray(xTicks) ? xTicks : xScale.ticks(xTicks)) : xScale.ticks();
                yTickData = yTicks ? (Array.isArray(yTicks) ? yTicks : yScale.ticks(yTicks)) : yScale.ticks();
                grid = d3.select(this).selectAll('.gridlines').data([[xTickData, yTickData]]);
                grid.enter().append('g').classed('gridlines', true);
                xLines(xTickData, grid);
                yLines(yTickData, grid);
                if( showGridBlocks ){
                    grid.classed('hideXGrids', true);
                    if( xTickData.length >3) {
                        drawBackgroudByxTickData(this, xTickData, yTickData);
                    }
                } else {
                    grid.classed('gridlines', true);
                }
                if(showSolidGridLines){
                    grid.classed('solidGridLines', true);
                }
            });
            
        };

        gridlines.xScale = function (value) {
            if (!arguments.length) {
                return xScale;
            }
            xScale = value;
            return gridlines;
        };

        gridlines.yScale = function (value) {
            if (!arguments.length) {
                return yScale;
            }
            yScale = value;
            return gridlines;
        };

        gridlines.xTicks = function (value) {
            if (!arguments.length) {
                return xTicks;
            }
            xTicks = value;
            return gridlines;
        };

        gridlines.yTicks = function (value) {
            if (!arguments.length) {
                return yTicks;
            }
            yTicks = value;
            return gridlines;
        };

        gridlines.setShowGridBlocks = function (value) {
            if (!arguments.length) {
                return showGridBlocks;
            }
            showGridBlocks = value;
            return gridlines;
        };

        gridlines.setShowSolidGridLines = function (value) {
            if (!arguments.length) {
                return showSolidGridLines;
            }
            showSolidGridLines = value;
            return gridlines;
        };

        return gridlines;
    }

});
