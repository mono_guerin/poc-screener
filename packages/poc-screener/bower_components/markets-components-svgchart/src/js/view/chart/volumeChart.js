﻿define(["./basicModule/commonPlotter", "../../manager/chartConfig/graphTypes", "../../core/core", "./baseChart", "../../util/util",
        "./basicModule/barSeries", "../../manager/data/request.js"
    ],
    function(commonPlotter, graphTypes, core, baseChartModel, util, barSeries, request) {
        "use strict";
        var $ = core.$;
        var d3 = core.d3;

        /**
         * Volume chart Model
         *
         */
        var model = function(managers) {
            var eventManager = managers.eventManager;
            var chartConfigManager = managers.chartConfigManager;
            var baseChart = baseChartModel(managers);
            var dataAdapter = managers.dataAdapter;
            var rootConfig;

            var volumeChartExtend = {
                _init: function(config) {
                    this._innerClass = innerClass;
                    rootConfig = chartConfigManager.getConfig();
                    this.langData = chartConfigManager.getLabels('markets-components-svgchart', config.lang);
                    this._chartTypes = chartConfigManager.VolumeChartTypes;
                    this._bindEvents();
                    this._renderDomFrame();
                    this._decideMinMaxAttrbute();
                    this._updateViewData();
                    if (this.config.show === false) {
                        this._changeVolumeChart();
                    }
                },
                _reviseData: function(data) {
                    data.forEach(function(d) {
                        d.x = d.index;
                        d.y = d.volume;
                    });
                    return data;
                },
                _bindEvents: function() {
                    var eventTypes = eventManager.EventTypes;
                    eventManager.bind(eventTypes.UpdateConfig, this._updateConfig, this);
                    eventManager.bind(eventTypes.ChangeChartType, this._changeChartType, this);
                    eventManager.bind(eventTypes.ChangeXAxisDomain, this._changeXAxisScaleDomain, this);
                    eventManager.bind(eventTypes.ChangeIntervalFrequency, this._changDataRange, this);
                    eventManager.bind(eventTypes.ChangeIndicators, this._chartConfigChanged, this);
                    eventManager.bind(eventTypes.ChangeFundamental, this._chartConfigChanged, this);
                    eventManager.bind(eventTypes.FetchPreloadData, this._fetchPreloadData, this);
                    eventManager.bind(eventTypes.LayoutChange, this._changeLayout, this);
                    eventManager.bind(eventTypes.ChangeYAxis, this._changeYaxis, this);
                    eventManager.bind(eventTypes.ChangeEvents, this._chartConfigChanged, this);
                    eventManager.bind(eventTypes.UpdateCompareTickers, this._chartConfigChanged, this);
                    eventManager.bind(eventTypes.ToggleTrackballVolume, this._toggleTrackball, this);
                    eventManager.bind(eventTypes.ChangeVolumeChart, this._changeVolumeChart, this);
                    eventManager.bind(eventTypes.DrawNewData, this._chartConfigChanged, this);
                    eventManager.bind(eventTypes.DestroyChart, this._processDestroyChart, this);
                    eventManager.bind(eventTypes.MouseOutChart, this._hideCursorValue, this);
                    eventManager.bind(eventTypes.ShowTrackball, this._showTrackball, this);
                    eventManager.bind(eventTypes.ChangeStartDate, this._chartConfigChanged, this);
                    eventManager.bind(eventTypes.ChangeChartType, this._decideMinMaxAttrbute, this);
                },
                _processDestroyChart: function() {
                    this._svg && this._svg.on('.volumechart', null);
                },
                _changeYaxis: function(position) {
                    if (this.config.yAxis.position == "outer") {
                        this._caculateValidSize();
                        this._createScale();
                        this._createCommonPlotter();
                        var translate = this._getGElemTranslate();
                        this._outerGElem && this._outerGElem.attr("transform", translate);
                    }
                    this._chartConfigChanged();
                },
                _hideCursorValue: function() {
                    rootConfig.$componentContainer.find('.' + chartConfigManager.getConfig().cursorValueClass).text('');
                },
                _changeVolumeChart: function() {
                    var volumeChartElement = rootConfig.$componentContainer.find('.' + chartConfigManager.getConfig().volumeChartClass);
                    if (this.config.show) {
                        if (volumeChartElement) {
                            volumeChartElement.css('display', 'block');
                        }
                        this._chartConfigChanged(this.config.chartType);
                    } else {
                        if (volumeChartElement) {
                            volumeChartElement.css('display', 'none');
                        }
                        this._clearView();
                    }
                    eventManager.trigger(eventManager.EventTypes.HeightChanged);
                },
                _toggleTrackball: function(trackballData) {
                    var cfg = this.config;
                    var rootCfg = chartConfigManager.getConfig();
                    for (var chartType in trackballData) {
                        if (trackballData.hasOwnProperty(chartType)) {
                            switch (chartType) {
                                case 'main':
                                    var volumeFormat = isNaN(rootCfg.frequency) ? ".3s" : ",";
                                    var volumeToDisplay = !trackballData[chartType].data || isNaN(trackballData[chartType].data.volume) ? this.langData['crosshairsPopoverNoData'] : d3.format(volumeFormat)(trackballData[chartType].data.volume);
                                    var mainValueLable = rootConfig.$componentContainer.find('.' + innerClass.mainValue);
                                    if (mainValueLable) {
                                        mainValueLable.css('color', trackballData[chartType].color);
                                        mainValueLable.text(': ' + volumeToDisplay);
                                    }
                                    break;
                                case 'indicators':
                                    var runningIndex = 0,
                                        chartTypeName = '';
                                    trackballData[chartType].forEach(function(d) {
                                        var newChartTypeName = d.name;
                                        newChartTypeName = newChartTypeName.replace(' ', '');
                                        if (chartTypeName !== newChartTypeName) {
                                            runningIndex = 0;
                                            chartTypeName = newChartTypeName;
                                        }
                                        var tickerValueElement = rootConfig.$componentContainer.find('.' + innerClass[chartType + "Value"] + chartTypeName + runningIndex);
                                        if (tickerValueElement) {
                                            tickerValueElement.css('color', d.color);
                                            tickerValueElement.text(!isNaN(d.data.y) ? d.data.y.toFixed(2) : '--');
                                        }
                                        runningIndex += 1;
                                    });
                                    break;
                                default:
                                    break;
                            }
                        }
                    };
                },
                _showDataNotAvailable: function() {
                    if (!this._svg) {
                        return;
                    }

                    if (this._outerGElem) {
                        this._outerGElem.selectAll("g").remove();
                    }
                    if (this.config && this.config.container) {
                        $(this.config.container).hide();
                    }
                },
                _hideDataNotAvailable: function() {
                    if (this.config.show) {
                        $(this.config.container).show();
                    }
                },
                _updateConfig: function() {
                    this.config = chartConfigManager.getChartConfig(chartConfigManager.ChartTypes.VolumeChart);
                },
                _changeChartType: function(chartType) {
                    if (this["_draw_" + chartType]) {
                        this.config.chartType = this._currentChartType = chartType;
                        //TODO: Not every chart type need reload data. We need change this in future.
                        this._chartConfigChanged();
                    }
                },
                _chartConfigChanged: function(chartType) {
                    this._updateViewData();
                },
                _clearView: function(chartType) {
                    if (this._outerGElem) {
                        this._outerGElem.selectAll("." + innerClass.indicators).remove();
                        this._outerGElem.selectAll(".gridBlock").remove();
                    }

                    this._clearTrackball();
                },
                _displayViewAfterChangeDomain: function() {
                    this._clearView();
                    this._renderContainedViews();
                    this._appendHeaderForIndicators();
                },
                _createScale: function() {
                    var view = this;
                    this._xScale = d3.scale.linear()
                        .range([0, this._validWidth])
                        .domain(this._getAjustedDomain([0, this._data.length - 1]));

                    this._yScale = d3.scale.linear().range([this._validHeight, 0]);
                    this._setYScaleDomain();
                },
                _setYScaleDomain: function() {
                    var minYValueArr = [d3.min(this._data, function(d) { return d.y; })];
                    var maxYValueArr = [d3.max(this._data, function(d) { return d.y; })];
                    var yScaleData = this._calculateYScaleDomainTickValues(d3.min(minYValueArr), d3.max(maxYValueArr));
                    this._yTickValues = yScaleData.tickValues;
                    if (yScaleData.domain) {
                        this._yScale.domain(yScaleData.domain);
                    }
                },
                _changeLayout: function() {
                    d3.select(this.config.container).selectAll("div").remove();
                    this._renderDomFrame();
                    if (this._data && (this._data.length > 0)) {
                        this._renderView();
                    } else {
                        this._showDataNotAvailable();
                        this._viewInited = false;
                    }
                },
                _caculateValidSize: function() {
                    var cfg = this.config;
                    if (cfg.yAxis.position == "outer") {
                        this._validWidth = cfg.width - cfg.margin.left - cfg.margin.right - cfg.yAxis.width - cfg.yAxisMargin;
                    } else {
                        this._validWidth = cfg.width - cfg.margin.left - cfg.margin.right;
                    }
                    this._validHeight = cfg.height - cfg.margin.top - cfg.margin.bottom - this._headerHeight;
                },
                _renderDomFrame: function() {
                    this._renderHeader();
                    this._caculateValidSize();
                    this._createSvgWrapper();
                    this._renderFooter();
                    //eventManager.trigger(eventManager.EventTypes.HeightChanged, this);
                },
                _createSvgWrapper: function() {
                    var cfg = this.config;
                    var svg = this._svg = d3.select(cfg.container).append('div').classed(innerClass.chartBody, true)
                        .style("width", cfg.width + cfg.margin.left + cfg.margin.right + "px").style("height", this._validHeight + "px")
                        .append("svg").attr("width", cfg.width).attr("height", this._validHeight);
                    this._outerGElem = svg.append("g")
                        .attr("transform", this._getGElemTranslate());
                    this._yAxisOuterGElem = svg.append("g").attr("class", innerClass.yAxis);
                },
                _renderView: function() {
                    var view = this;
                    this._containerOffset = 0;
                    this._createScale();
                    this._createCommonPlotter();
                    this._renderContainedViews();
                    //when mouse move in the mainchart, we will show crosshairs
                    this._svg.on('mousemove.volumechart', function() {
                        view._containerOffset = view._containerOffset ? view._containerOffset : $(view.config.container).offset();
                        view._mousemove(d3.event, d3.mouse(this), view._headerHeight);
                    });
                    this._viewInited = true;
                },
                _renderContainedViews: function() {
                    if (!this._data || this._data.length === 0) {
                        this._showDataNotAvailable();
                        return;
                    } else {
                        this._hideDataNotAvailable();
                    }
                    this._decimalPlaces = this._getDecimalPlaces();
                    this._drawXAxis();
                    this._drawYAxis();
                    this._drawGridLines();
                    this._draw_Indicators();
                    this._drawChartGraph();

                },
                _createCommonPlotter: function() {
                    var view = this;
                    var graphConfig = this._defaultGraphConfig = {
                        yAttributeName: "volume",
                        containerGElem: this._outerGElem,
                        data: this._data,
                        xScale: this._xScale,
                        yScale: this._yScale,
                        graphType: graphTypes.Histogram,
                        width: this._validWidth,
                        height: this._validHeight,
                        tickSize: this.config.volumeBarWidth,
                        yAxisContainerGElem: this._yAxisOuterGElem,
                        getElementsMapping: function(elementsMapping) {
                            var mapping = view._elementsMapping || [];
                            mapping.push(elementsMapping);
                            view._elementsMapping = mapping;
                        }
                    };
                    this._getDefaultGraphConfig = function() {
                        this._defaultGraphConfig.data = this._data;
                        return this._defaultGraphConfig;
                    };
                    this._commonPlotter = new commonPlotter(graphConfig);
                },
                _renderHeader: function() {
                    var cfg = this.config,
                        margin = this.config.margin;
                    var tickerInfoContainer = this._headerContainer = d3.select(cfg.container)
                        .append("div").classed(innerClass.chartHeader, true);
                    if (cfg.showLegend) {
                        tickerInfoContainer.append("span").classed(innerClass.chartLegend, true)
                            .style("background", "#2F4880");
                    }
                    tickerInfoContainer.append("span")
                        .style("margin-left", margin.left + "px")
                        .style("width", (this.config.width - margin.left - margin.right) + "px")
                        .classed(innerClass.chartLegendText, true)
                        .text(this.langData[cfg.chartLegendText] || cfg.chartLegendText)
                        .append("span")
                        .classed(chartConfigManager.getConfig().cursorValueClass, true)
                        .classed(innerClass.mainValue, true);
                    this._headerContainer.append("span").classed(innerClass.indicators, true);
                    this._headerHeight = $(tickerInfoContainer[0]).outerHeight(true);
                },
                _renderFooter: function() {
                    var cfg = this.config,
                        margin = this.config.margin;
                    d3.select(cfg.container)
                        .append("div").classed(innerClass.chartFooter, true)
                        .style("margin-left", margin.left + "px")
                        .style("width", (this.config.width - margin.left - margin.right) + "px")
                },
                _drawChartGraph: function() {
                    var chartType = this._currentChartType = this.config.chartType;
                    this["_draw_" + chartType](chartType);
                },
                _draw_volumeChart: function(chartType) {
                    var graphConfig = {
                        graphCss: innerClass.volumeChart
                    };

                    this["_elem_" + chartType] = this._commonDraw(graphConfig);
                },
                _draw_volumePlusChart: function(chartType) {
                    var graphConfig = {
                        graphCss: innerClass.volumeChart, //we should use volumePlusChart, I change it for performance.
                        distinguish: true
                    };

                    this["_elem_" + chartType] = this._commonDraw(graphConfig);
                }
            };

            return util.extendClass(baseChart, volumeChartExtend);
        };

        var innerClass = {
            gridLines: "volumeChart-gridLines",
            volumeChart: "chart-volume",
            volumePlusChart: "chart-volumePlus",
            chartHeader: "volumeChart-header",
            chartLegend: "volumeChart-legend",
            chartLegendText: "volumeChart-legend-text",
            chartBody: 'volumeChart-body',
            chartFooter: "volumeChart-footer",
            xAxis: "volumeChart-xAxis",
            yAxis: "volumeChart-yAxis",
            indicators: "indicators",
            indicatorsLegend: "indicators-legend",
            indicatorsLegendText: "indicators-legend-text",
            mainValue: "volumeChart-volumeValue",
            indicatorsValue: "volumeChart-indicatorsValue-",
            closeButton: "icon chart close-button"
        };

        return model;
    });