define(["./basicModule/commonPlotter", "../../manager/chartConfig/graphTypes", "../../core/core", "./baseChart", "../../util/util",
        "./basicModule/barSeries", "../../manager/data/request.js"
    ],
    function(commonPlotter, graphTypes, core, baseChartModel, util, barSeries, request) {
        "use strict";
        var $ = core.$;
        var d3 = core.d3;

        /**
         * fundamental chart Model
         *
         */
        var model = function(managers) {
            var eventManager = managers.eventManager;
            var chartConfigManager = managers.chartConfigManager;
            var baseChart = baseChartModel(managers);
            var dataAdapter = managers.dataAdapter;
            var rootCfg;

            var fundamentalChartExtend = {
                _init: function(config) {
                    this._innerClass = innerClass;
                    rootCfg = chartConfigManager.getConfig();
                    this.langData = chartConfigManager.getLabels('markets-components-svgchart', config.lang);
                    this._isFundamentalChart = true;
                    this._chartTypes = chartConfigManager.FundamenatlChartTypes
                    this._bindEvents();
                    this._decideMinMaxAttrbute();
                    if (rootCfg.fundamentals && rootCfg.fundamentals.length > 0) {
                        this._updateViewData();
                    }
                },
                _reviseData: function(data) {
                    data.forEach(function(d) {
                        d.x = d.index;
                        d.y = d.value;
                    });
                    return data;
                },
                _bindEvents: function() {
                    var eventTypes = eventManager.EventTypes
                    eventManager.bind(eventTypes.UpdateConfig, this._updateConfig, this);
                    eventManager.bind(eventTypes.ChangeFundamental, this._chartConfigChanged, this);
                    eventManager.bind(eventTypes.ChangeXAxisDomain, this._changeLastDateRange, this);
                    eventManager.bind(eventTypes.DestroyChart, this._processDestroyChart, this);
                    eventManager.bind(eventTypes.ChangeStartDate, this._chartConfigChanged, this);
                },
                _bindAfterOpenFundamentalEvents: function() {
                    if (!this._isInited) {
                        var eventTypes = eventManager.EventTypes
                        eventManager.bind(eventTypes.ChangeIndicators, this._chartConfigChanged, this);
                        eventManager.bind(eventTypes.ChangeXAxisDomain, this._changeXAxisScaleDomain, this);
                        eventManager.bind(eventTypes.UpdateCompareTickers, this._chartConfigChanged, this);
                        eventManager.bind(eventTypes.LayoutChange, this._changeLayout, this);
                        eventManager.bind(eventTypes.ChangeYAxis, this._chartConfigChanged, this);
                        eventManager.bind(eventTypes.FetchPreloadData, this._fetchPreloadData, this);
                        eventManager.bind(eventTypes.ChangeIntervalFrequency, this._changDataRange, this);
                        eventManager.bind(eventTypes.ChangeEvents, this._changeYaxis, this);
                        eventManager.bind(eventTypes.ToggleTrackballFundamentals, this._toggleTrackball, this);
                        eventManager.bind(eventTypes.DrawNewData, this._chartConfigChanged, this);
                        eventManager.bind(eventTypes.ShowTrackball, this._showTrackball, this);
                        eventManager.bind(eventTypes.ChangeChartType, this._decideMinMaxAttrbute, this);
                        this._isInited = true;
                    }
                },
                _changeLastDateRange: function(newXDomainValue, lastDateRange) {
                    this._lastDateRange = lastDateRange;
                },
                _toggleTrackball: function(trackballData) {
                    var cfg = this.config;
                    trackballData.forEach(function(d) {
                        var tickerValueElement = rootCfg.$componentContainer.find('.' + innerClass.fundamentalsValue + d.name);
                        if (tickerValueElement) {
                            tickerValueElement.css('color', d.color);
                            tickerValueElement.text((d.data && !isNaN(d.data.y)) ? util.formatNotZeroNum(d.data.y, 2) : '--');
                        }
                    });
                },
                _updateConfig: function() {
                    this.config = chartConfigManager.getChartConfig(chartConfigManager.ChartTypes.FundamentalChart);
                },
                _chartConfigChanged: function() {
                    this._updateViewData();
                },
                _changeYaxis: function(position) {
                    if (this.config.yAxis.position == "outer") {
                        var translate = this._getGElemTranslate();
                        this._outerGElem && this._outerGElem.attr("transform", translate);
                    }
                    this._chartConfigChanged();
                },
                _updateViewData: function(adjustedDateRange) {
                    var view = this;
                    var cfg = this.config;
                    chartConfigManager.fetchMainTickerData(function(res, preloadInfo) {
                        view._clearAllData();
                        view._clearView();
                        if (res.status().errorCode === "0") {
                            view._preloadInfo = preloadInfo;
                            var mainTickerData = res.data()[0];
                            view._fundamentalsData = mainTickerData.Fundamentals;
                            view._indicatorsData = mainTickerData.Indicators;
                            view._eventsData = mainTickerData.Events;
                            view.mainTickerObject = mainTickerData.tickerObject;
                            view._lastClosePrice = mainTickerData.Price.spos;
                            view._data = view._reviseData(mainTickerData.Price.data);
                            if (chartConfigManager.getMainDataType() === chartConfigManager.MainDataTypes.DividendEffect) {
                                //removing indicators that are shown in main chart
                                var indicators = null;
                                for (var indicatorName in view._indicatorsData) {
                                    if (!rootCfg.charts.mainChart.indicatorProperties.hasOwnProperty(indicatorName)) {
                                        indicators = indicators || {};
                                        indicators[indicatorName] = view._indicatorsData[indicatorName];
                                    }
                                }
                                view._indicatorsData = indicators ? indicators : null;
                                var fundamentalType = dataAdapter.Fundamental.MReturnIndex;
                                if (mainTickerData.Fundamentals && mainTickerData.Fundamentals[fundamentalType] &&
                                    mainTickerData.Fundamentals[fundamentalType].data) {
                                    view._dividendEffectData = view._reviseDividendEffectData(mainTickerData.Fundamentals[fundamentalType].data);
                                }
                            } else {
                                view._getCompareData(res.data());
                                view._createDummyData(view._compareData);
                                preloadInfo.loadedCompareData = view._compareData;
                            }
                            view._rebaseCompareData();
                            view._rebaseFundamentalData();
                            view._changeChartGraph();
                            if (preloadInfo.intervalType !== chartConfigManager.IntervalTypes.Max && !chartConfigManager.getConfig().useEUTimeSeriesData) {
                                setTimeout(function() {
                                    view._preloading = true;
                                    view._loadingData = true;
                                    view._fetchPreloadData();
                                }, view.config.preloadDelay);
                            }
                        }
                    }, adjustedDateRange);
                },
                _setScaleDomain: function() {},
                _displayViewAfterChangeDomain: function() {
                    this._rebaseCompareData();
                    this._changeChartGraph();
                },
                _changeChartGraph: function() {
                    rootCfg.$componentContainer.find("." + this.config.chartClass).empty();
                    var fundamentalsData = this._fundamentalsData;
                    if (rootCfg.fundamentals.length > 0) {
                        this._draw_Fundamentals(true);
                    }
                    this._bindAfterOpenFundamentalEvents();
                    eventManager.trigger(eventManager.EventTypes.HeightChanged);
                },
                _createScale: function(fundamentalName) {
                    this._xScale = d3.scale.linear()
                        .range([0, this._validWidth])
                        .domain([0, this._data.length - 1]);

                    var minYValue = 0;
                    var maxYValue = 0;
                    if (this._fundamentalsData && this._fundamentalsData[fundamentalName]) {
                        var fundamental = this._fundamentalsData[fundamentalName];
                        if (fundamental.data && fundamental.data.length > 0) {
                            minYValue = d3.min(fundamental.data, function(d) {
                                return d.value;
                            });
                            maxYValue = d3.max(fundamental.data, function(d) {
                                return d.value;
                            });
                        }
                    }
                    var yScaleData = this._calculateYScaleDomainTickValues(minYValue, maxYValue);
                    this._yTickValues = yScaleData.tickValues;
                    this._decimalPlaces = this._getDecimalPlaces(yScaleData.domain);
                    this._yScale = d3.scale.linear().range([this._validHeight, 0])
                        .domain(yScaleData.domain);
                },
                _changeLayout: function() {
                    this._changeChartGraph();
                },
                _processDestroyChart: function() {
                    this._svg && this._svg.on('.fundamentalchart', null);
                },
                _renderView: function(fundamentalName) {
                    //when open a save config, maybe we need draw fundamentals.
                    var view = this;
                    this._caculateValidSize();
                    this._renderHeader(fundamentalName);
                    this._createScale(fundamentalName);
                    this._createSvgWrapper();
                    this._createCommonPlotter();
                    this._renderContainedViews();
                    this._renderFooter();
                    //when mouse move in the fundamentalchart, we will show crosshairs, waiting to get main chart data
                    this._svg.on('mousemove.fundamentalchart', function() {
                        view._mousemove(d3.event, d3.mouse(this), view._headerHeight);
                    });
                },
                _renderContainedViews: function() {
                    this._drawXAxis();
                    this._drawYAxis();
                    this._drawGridLines();
                },
                _createCommonPlotter: function() {
                    var graphConfig = this._defaultGraphConfig = {
                        containerGElem: this._outerGElem,
                        xScale: this._xScale,
                        yScale: this._yScale,
                        graphType: graphTypes.Histogram,
                        width: this._validWidth,
                        height: this._validHeight,
                        tickSize: this.config.indicatorBarWidth,
                        yAxisContainerGElem: this._yAxisOuterGElem
                            // yAxisContainerGElem: this._svg,
                    };

                    this._getDefaultGraphConfig = function() {
                        return this._defaultGraphConfig;
                    };
                    this._commonPlotter = new commonPlotter(graphConfig);
                },
                _renderHeader: function(fundamentalName) {
                    var cfg = this.config,
                        margin = this.config.margin;
                    var tickerInfoContainer = this._headerContainer = d3.select(cfg.container)
                        .append("div").classed(innerClass.chartHeader, true)
                        .classed(fundamentalName + '-class', true);
                    tickerInfoContainer.append("span").classed(innerClass.chartLegend, true)
                        .style("background", "#2F4880");
                    tickerInfoContainer.append("span")
                        .style("margin-left", margin.left + "px")
                        .style("width", (this.config.width - margin.left - margin.right) + "px")
                        .classed(innerClass.chartLegendText, true)
                        .text(cfg.chartLegendText);
                    this._headerContainer.append("span").classed(innerClass.fundamentals, true);
                    this._appendHeaderForFundamentals(fundamentalName);
                },
                _renderFooter: function() {
                    var cfg = this.config,
                        margin = this.config.margin;
                    d3.select(cfg.container)
                        .append("div").classed(innerClass.chartFooter, true)
                        .style("margin-left", margin.left + "px")
                        .style("width", (this.config.width - margin.left - margin.right) + "px")
                }
            };

            return util.extendClass(baseChart, fundamentalChartExtend);
        };

        var innerClass = {
            gridLines: "fundamentalChart-gridLines",
            fundamentalChart: "chart-fundamental",
            chartHeader: "fundamentalChart-header",
            chartLegend: "fundamentalChart-legend",
            chartLegendText: "fundamentalChart-legend-text",
            chartBody: 'fundamentalChart-body',
            chartFooter: "fundamentalChart-footer",
            fundamentals: "fundamentals",
            fundamentalsValue: "fundamentalsValue-",
            fundamentalsLegend: "fundamentals-legend",
            fundamentalsLegendText: "fundamentals-legend-text",
            closeButton: "icon chart close-button",
            yAxis: "fundamentalChart-yAxis",
        };

        return model;
    });