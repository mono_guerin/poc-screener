﻿define(["./basicModule/commonPlotter", "../../core/core", "./baseChart", "../../util/util", "../../manager/data/request", "../../manager/chartConfig/graphTypes", "./basicModule/drawings", "moment"],
    function(commonPlotter, core, baseChartModel, util, request, graphTypes, drawings, moment) {
        "use strict";
        var $ = core.$;
        var d3 = core.d3;

        /**
         * Main chart Model
         *
         */
        var model = function(managers) {
            var eventManager = managers.eventManager;
            var chartConfigManager = managers.chartConfigManager;
            var dataAdapter = managers.dataAdapter;
            var baseChart = baseChartModel(managers);
            var Constants = chartConfigManager.Constants;
            var IntervalTypes = chartConfigManager.IntervalTypes;
            var CursorType = chartConfigManager.CursorType;
            var SelectionModes = chartConfigManager.SelectionModes;
            var MainChartTypes = chartConfigManager.MainChartTypes;
            var MainDataTypes = chartConfigManager.MainDataTypes;
            var rootConfig;
            var isFundChart = false;
            var legendSpanColor;
            var needMergeHeaderIndicators = ['MAE', 'BBands', 'PChannel', "Keltner"];
            var isMobile = util.deviceType.mobile();
            var mainChartExtend = {
                _init: function(config) {
                    this._innerClass = innerClass;
                    rootConfig = chartConfigManager.getConfig();
                    isFundChart = (rootConfig.chartGenre === chartConfigManager.ChartGenre.FundChart);
                    this.langData = chartConfigManager.getLabels('markets-components-svgchart', config.lang);
                    this._decimalPlaces = this.config.crossHairDecimal = this.config.drawingsDecimal = this._getDecimalPlaces();
                    this._bindEvents();
                    this._decideMinMaxAttrbute();
                    this._updateViewData();
                    this._panResponseCount = 0;
                },
                _createCommonPlotter: function() {
                    var view = this;
                    var graphConfig = this._defaultGraphConfig = {
                        containerGElem: this._outerGElem,
                        data: this._data,
                        xScale: this._xScale,
                        yScale: this._yScale,
                        width: this._validWidth,
                        yAttributeName: this.yAttr,
                        yAxisContainerGElem: this._yAxisOuterGElem,
                        getElementsMapping: function(elementsMapping) {
                            var mapping = view._elementsMapping || [];
                            mapping.push(elementsMapping);
                            view._elementsMapping = mapping;
                        }
                    };
                    this._getDefaultGraphConfig = function() {
                        this._defaultGraphConfig.data = this._data;
                        return this._defaultGraphConfig;
                    };
                    this._commonPlotter = new commonPlotter(graphConfig);
                },
                _bindEvents: function() {
                    var eventTypes = eventManager.EventTypes;
                    eventManager.bind(eventTypes.ExportData, this._exportDataToExcel, this);
                    eventManager.bind(eventTypes.UpdateConfig, this._updateConfig, this);
                    eventManager.bind(eventTypes.ChangeChartType, this._changeChartType, this);
                    eventManager.bind(eventTypes.ChangeIntervalFrequency, this._changDataRange, this);
                    eventManager.bind(eventTypes.UpdateCompareTickers, this._compareTicker, this);
                    eventManager.bind(eventTypes.GetMainChartDateRange, this._setSliderDateRange, this);
                    eventManager.bind(eventTypes.ChangeXAxisDomain, this._changeXAxisScaleDomain, this);
                    eventManager.bind(eventTypes.ChangeIndicators, this._chartConfigChanged, this);
                    eventManager.bind(eventTypes.ChangeFundamental, this._chartConfigChanged, this);
                    eventManager.bind(eventTypes.ChangeEvents, this._chartConfigChanged, this);
                    eventManager.bind(eventTypes.ChangeYAxis, this._changeYaxis, this);
                    eventManager.bind(eventTypes.FetchPreloadData, this._fetchPreloadData, this);
                    eventManager.bind([eventTypes.LayoutChange, eventTypes.ChangeMainChartLayout], this._changeLayout, this);
                    eventManager.bind(eventTypes.ChangeSelectionMode, this._changeSelectionMode, this);
                    eventManager.bind(eventTypes.ChangeDrawing, this._changeDrawing, this);
                    eventManager.bind(eventTypes.ChangeVerticalScale, this._changeVerticalScale, this);
                    eventManager.bind(eventTypes.ToggleTrackball, this._toggleTrackball, this);
                    eventManager.bind(eventTypes.DrawNewData, this._chartConfigChanged, this);
                    eventManager.bind(eventTypes.DestroyChart, this._processDestroyChart, this);
                    eventManager.bind(eventTypes.ShowTrackball, this._showTrackball, this);
                    eventManager.bind(eventTypes.ResetZoomIn, this._resetZoomIn, this);
                },
                _changeYaxis: function() {
                    if (this.config.yAxis.position == "outer") {
                        var translate = this._getGElemTranslate();
                        this._xAxisOuterGElem && this._xAxisOuterGElem.attr("transform", translate);
                        this._outerGElem && this._outerGElem.attr("transform", translate);
                        this._drawingsGElem && this._drawingsGElem.attr("transform", translate);
                    }
                    this._chartConfigChanged();
                },
                _drawYAxis: function() {
                    var view = this;
                    var formatValue;
                    var yAxisConfig = this.config.yAxis;
                    formatValue = function(d) {
                        return util.numberFormatter(view.config.lang, d, view._yAxisDecimal, " ", yAxisConfig.withUnit, true);
                    }
                    if (yAxisConfig.show) {
                        var yAxisTickValues = this._yTickValues;
                        if (yAxisConfig.position == "outer") {
                            var rootConfig = chartConfigManager.getConfig();
                            var yAxisTickText = [];
                            $.each(yAxisTickValues, function(key, val) {
                                yAxisTickText.push(formatValue(val));
                            });
                            var tickWidth = this._getTickWidth(yAxisTickText, this._innerClass.yAxis) + 2;
                            tickWidth = (tickWidth < rootConfig.yAxisWidth) ? rootConfig.yAxisWidth : tickWidth;
                            if (tickWidth != yAxisConfig.width) {
                                yAxisConfig.width = tickWidth;
                                var newConfig = rootConfig;
                                newConfig.charts.mainChart.yAxis.width = yAxisConfig.width;
                                newConfig.charts.volumeChart.yAxis.width = yAxisConfig.width;
                                newConfig.charts.sliderChart.yAxis.width = yAxisConfig.width;
                                newConfig.charts.indicatorChart.yAxis.width = yAxisConfig.width;
                                newConfig.charts.fundamentalChart.yAxis.width = yAxisConfig.width;
                                chartConfigManager.setConfig(newConfig);
                                eventManager.trigger(eventManager.EventTypes.ChangeYAxis);
                                return false;
                            }
                        }
                        var comConfig = chartConfigManager.getConfig();
                        var config = {
                            containerGElem: view._yAxisOuterGElem,
                            tickSize: yAxisConfig.tickSize || 0,
                            tickPadding: 5,
                            tickValues: yAxisTickValues,
                            orient: yAxisConfig.orient,
                            position: yAxisConfig.position,
                            yAxisWidth: yAxisConfig.width,
                            ticks: yAxisConfig.ticks,
                            graphCss: this._innerClass.yAxis,
                            setAxisStyleFunc: this._setYAxisStyle,
                            graphType: graphTypes.Axis,
                            scale: this._yScale,
                            width: this._validWidth,
                            height: this._validHeight,
                            tick: yAxisConfig.tick,
                            tickFormatFunc: function(d) {
                                return formatValue(d)
                            },
                            componentContainer: comConfig.$componentContainer,
                        };
                        if (comConfig.yAxisMargin) config.yAxisMargin = comConfig.yAxisMargin;
                        this._commonDraw(config);
                        // var yElem = this._commonDraw(config);
                        // var scaleType = this._getCurrentScaleType();
                        // this._processTicks(yElem);
                        return true;
                    }
                },

                _processDestroyChart: function() {
                    this._svg && this._svg.on('.mainchart', null);
                },
                _compareTicker: function() {
                    var mainChartConfig = rootConfig.charts.mainChart;
                    var eventTypes = eventManager.EventTypes;
                    // @todo remove duplication in chartConfigManager
                    if (rootConfig.compareTickers && rootConfig.compareTickers.length > rootConfig.compareChartsMaxAllowed) {
                        var toRemove = rootConfig.compareTickers.pop();
                        eventManager.trigger(eventTypes.TickerLimitReached);
                    } else {
                        this._chartConfigChanged();
                    }
                },
                _toggleTrackball: function(trackballData) {
                    var cfg = this.config;
                    var view = this;
                    for (var chartType in trackballData.data) {
                        if (trackballData.data.hasOwnProperty(chartType)) {
                            switch (chartType) {
                                case 'main':
                                    var mainValueLable = this.$el.find('.' + innerClass.mainValue);
                                    mainValueLable.css('color', trackballData.data[chartType].color);
                                    mainValueLable.text(this._constructMainTickerValues(trackballData.data[chartType].data));
                                    break;
                                case 'events':
                                    break;
                                case 'fundamentals':
                                    trackballData.data[chartType].forEach(function(d) {
                                        var tickerValueElement = view.$el.find('.' + innerClass[chartType + "Value"] + d.name);
                                        tickerValueElement.css('color', d.color);
                                        tickerValueElement.text(!isNaN(d.data.y) ? d.data.y.toFixed(view._decimalPlaces) : '--');
                                    });
                                    break;
                                default:
                                    var runningIndex = 0,
                                        chartTypeName = '';
                                    trackballData.data[chartType].forEach(function(d) {
                                        var newChartTypeName = d.ticker.indexOf('(') > -1 ? d.ticker.substr(0, d.ticker.indexOf('(')) : d.ticker
                                        newChartTypeName = newChartTypeName.replace(' ', '');
                                        if (chartTypeName !== newChartTypeName) {
                                            runningIndex = 0;
                                            chartTypeName = newChartTypeName;
                                        }

                                        chartTypeName = chartTypeName.replace(/[^\d\w]/g, '');
                                        var tickerValueElement = view.$el.find('.' + innerClass[chartType + "Value"] + chartTypeName + runningIndex);
                                        tickerValueElement.css('color', d.color);
                                        var textValue = d.data && !isNaN(d.data.y) ? d.data.y.toFixed(view._decimalPlaces) : '--';
                                        if (chartType != "compare") {
                                            tickerValueElement.text(' ' + textValue);
                                        }
                                        runningIndex += 1;
                                    });
                                    break;
                            }
                        }
                    };
                },
                _constructMainTickerValues: function(tickerData) {
                    var tickerValues = ''
                    if (tickerData) {
                        if (isFundChart) {
                            tickerValues = (!isNaN(tickerData[this.yAttr]) ? util.numberFormatter(this.config.lang, tickerData[this.yAttr], this._decimalPlaces, " ", false, true) : '') + ' ';
                        } else {
                            tickerValues += 'O:' + (!isNaN(tickerData.open) ? util.numberFormatter(this.config.lang, tickerData.open, this._decimalPlaces, " ", false, true) : '') + ' ';
                            tickerValues += 'H:' + (!isNaN(tickerData.high) ? util.numberFormatter(this.config.lang, tickerData.high, this._decimalPlaces, " ", false, true) : '') + ' ';
                            tickerValues += 'L:' + (!isNaN(tickerData.low) ? util.numberFormatter(this.config.lang, tickerData.low, this._decimalPlaces, " ", false, true) : '') + ' ';
                            tickerValues += 'C:' + (!isNaN(tickerData.close) ? util.numberFormatter(this.config.lang, tickerData.close, this._decimalPlaces, " ", false, true) : '') + ' ';
                            tickerValues += 'Volume:' + tickerData.volume + ' ';
                        }
                    }
                    return tickerValues;
                },
                _changeDrawing: function(drawingType) {
                    if (drawingType) {
                        this._disablePan();
                        this._disableZoomIn();
                        eventManager.trigger(eventManager.EventTypes.ToggleCrossharis);
                        this._drawingType = drawingType.toLowerCase();
                    } else {
                        if (this.config.selectionMode === SelectionModes.Pan) {
                            if (!isMobile) {
                                this._enablePan();
                            }
                        } else if (this.config.selectionMode === SelectionModes.ZoomIn) {
                            this._enableZoomIn();
                        }
                        this._drawingType = null;
                    }
                },
                _changeVerticalScale: function() {
                    this._changeYScale();
                    this._setYScaleDomain();
                    this._displayViewAfterChangeDomain();
                    this._createSelectionMode();
                },
                _setScaleDomain: function(xScaleDomain) {
                    var view = this;
                    this._xScale.domain(this._getAjustedDomain(xScaleDomain));
                    this._yScale = this._createYScale().range([this._validHeight, 0]);
                    this._decideMinMaxAttrbute();
                    this._setYScaleDomain();
                },
                _getCurrentScaleType: function() {
                    var mainDataType = chartConfigManager.getMainDataType();
                    var currentScaleType;
                    if ((this.config.verticalScaleType === chartConfigManager.VerticalScaleTypes.Logarithmic) &&
                        (this._compareData && this._compareData.length > 0 ||
                            mainDataType === MainDataTypes.Growth ||
                            mainDataType === MainDataTypes.DividendEffect)) {

                        currentScaleType = chartConfigManager.VerticalScaleTypes.Linear;
                    } else {
                        currentScaleType = this.config.verticalScaleType || chartConfigManager.VerticalScaleTypes.Linear;
                    }
                    return currentScaleType;
                },
                _changeYScale: function() {
                    var yScaleDomain = this._yScale.domain(),
                        yScaleRange = this._yScale.range();
                    this._yScale = this._createYScale().range(yScaleRange).domain(yScaleDomain);
                },
                _changeSelectionMode: function() {
                    if (this._lastSelectionMode && this._lastSelectionMode === this.config.selectionMode) {
                        return;
                    }
                    var selectionMode = this.config.selectionMode;
                    switch (selectionMode) {
                        case SelectionModes.Pan:
                            this._disableZoomIn();
                            this._createPan();
                            if (isMobile) {
                                this._disablePan();
                            }
                            var loadedData = this._loadedData;
                            if (loadedData) {
                                var offsetDomain = this._preloadInfo.offsetDomain || 0;
                                var start = this._getDataIndexByDate(this._data[0].date, null, null, loadedData);
                                start && (this._preloadInfo.offsetDomain = start);
                            }

                            this._lastSelectionMode = selectionMode;
                            break;
                        case SelectionModes.ZoomIn:
                            this._disablePan();
                            this._createZoomIn();
                            this._lastSelectionMode = selectionMode;
                            break;
                        case SelectionModes.Off:
                            this._disablePan();
                            this._disableZoomIn();
                            this._lastSelectionMode = selectionMode;
                            break;
                    }
                },
                _updateConfig: function() {
                    this.config = chartConfigManager.getChartConfig(chartConfigManager.ChartTypes.MainChart);
                },
                _setSliderDateRange: function() {
                    var dateRange = this.config.dateRange;
                    // TODO: why _xScale is null sometimes?
                    if (this._xScale && this._data && (!dateRange || dateRange.length !== 2)) {
                        var crurrentDomain = this._xScale.domain();
                        var startIndex, endIndex;
                        if (crurrentDomain[0] < 0) { //no preload data.
                            startIndex = 0;
                        } else {
                            startIndex = Math.floor(crurrentDomain[0]);
                        }

                        if (crurrentDomain[1] >= this._data.length) {
                            endIndex = this._data.length - 1;
                        } else {
                            endIndex = Math.floor(crurrentDomain[1] - 1);
                        }

                        if (endIndex < 0) {
                            endIndex = 0;
                        }

                        dateRange = [this._data[startIndex].date, this._data[endIndex].date];
                    }

                    dateRange && eventManager.trigger(eventManager.EventTypes.ChangeSliderDateRange, dateRange);
                    return dateRange;
                },
                _chartConfigChanged: function() {
                    //TODO: Calls are made twice one for main chart and again for volume
                    //Need to work on making sure that the call is made only once
                    this._updateViewData();
                },

                _changeChartType: function(chartType) {
                    if (chartType === MainChartTypes.TenKGrowthChart) {
                        chartConfigManager.setConfig({
                            growthBaseValue: 10000,
                            chartType: chartType,
                            dataType: chartConfigManager.getMainDataType()
                        });
                    } else {
                        chartConfigManager.setConfig({
                            growthBaseValue: rootConfig.rootGrowthBaseValue,
                            chartType: chartType,
                            dataType: chartConfigManager.getMainDataType()
                        });
                    }
                    if (chartType === MainChartTypes.pGrowthChart || chartType === MainChartTypes.TenKGrowthChart || chartType === MainChartTypes.DividendEffectChart || chartType === MainChartTypes.PostTaxReturnChart || chartType === MainChartTypes.PriceWithDividendGrowth) {
                        chartType = MainChartTypes.MountainChart;
                    }
                    this._decideMinMaxAttrbute();
                    if (this["_draw_" + chartType]) {
                        this._chartConfigChanged();
                    }
                },
                _hightLightHoveredLine: function(indexPosition) {
                    var hightLightIndex = this._findHoveredLineIndex(indexPosition);
                    var hightLightLine;
                    var d3Container = d3.select(this.config.container);
                    if (hightLightIndex == 0) {
                        hightLightLine = d3Container.select('.mkts-cmpt-svgcht-chart-lines.chart-line');
                        this._drawMainTag(true);
                    } else if (!isNaN(hightLightIndex)) {
                        var cIndex = hightLightIndex - 1;
                        var compareData = this._compareData[cIndex];
                        var lineSelector = '.mkts-cmpt-svgcht-chart-lines.compare-' + cIndex + '-class';
                        var tagSelector = '.compare-tag-' + cIndex;
                        var colorsLength = chartConfigManager.getConfig().compareColors.length;
                        hightLightLine = d3Container.select(lineSelector);
                        var colorIndex = (cIndex >= colorsLength && colorsLength) ? cIndex % colorsLength : cIndex;
                        var colorValue = compareData.tickerObject['customTickerColor'] || chartConfigManager.getConfig().compareColors[colorIndex];
                        var globalGraphClass = this._getGlobalClass(compareData.tickerObject, null, cIndex);
                        var upward = this._firstCompareTagUpward;
                        this._drawTag(compareData.Price.data, this.yAttr, colorValue, "compare-tag-" + colorIndex + globalGraphClass, upward, true);
                    }
                    d3Container.selectAll(".mkts-cmpt-svgcht-chart-lines").classed('mkts-cmpt-svgcht-hightlight-line', false);
                    hightLightLine && hightLightLine.classed('mkts-cmpt-svgcht-hightlight-line', true);
                },
                _isAcuteAngle: function(p1, p2, p3) { //p1 is the vertex,
                    var angel = 0,
                        radian;
                    var P12 = Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
                    var P13 = Math.sqrt(Math.pow(p1.x - p3.x, 2) + Math.pow(p1.y - p3.y, 2));
                    var P23 = Math.sqrt(Math.pow(p2.x - p3.x, 2) + Math.pow(p2.y - p3.y, 2));
                    if (P12 == 0 || P23 == 0 || P13 == 0) {
                        radian = 0;
                    } else {
                        var cosV = (Math.pow(P12, 2) + Math.pow(P13, 2) - Math.pow(P23, 2)) / (2 * P12 * P13);
                        if (cosV > 1) {
                            cosV = 1;
                        } else if (cosV < -1) {
                            cosV = -1;
                        }
                        radian = Math.acos(cosV);
                    }
                    var angle = Math.floor(180 / (Math.PI / radian));
                    return angle < 90;
                },
                _findHoveredLineIndex: function(position) {
                    var dataList = [],
                        disArray = [],
                        yAttr = this.yAttr;
                    var highlightDist = rootConfig.highlightDist;
                    dataList.push(this._data);
                    if (this._compareData) {
                        for (var i = 0; i < this._compareData.length; i++) {
                            dataList.push(this._compareData[i]['Price']['data']);
                        }
                    }
                    for (var i = 0; i < dataList.length; i++) {
                        var data = dataList[i],
                            dataLen = data.length;
                        var p0 = {
                                x: 0,
                                y: 0
                            },
                            p1 = {
                                x: 0,
                                y: 0
                            },
                            p = {
                                x: 0,
                                y: 0
                            };
                        var oriXMouse = this._xScale.invert(position[0]);
                        var oriXMouse1 = Math.floor(this._xScale.invert(position[0] - highlightDist) - this._xScale.domain()[0]);
                        var oriXMouse2 = Math.ceil(this._xScale.invert(position[0] + highlightDist));
                        oriXMouse2 = oriXMouse2 > (data.length - 1) ? (data.length - 1) : oriXMouse2;
                        oriXMouse1 = oriXMouse1 < 0 ? 0 : oriXMouse1;
                        var dpGroupArray = []; // all possible line segments between oriXMouse1 and oriXMouse2
                        for (var n = oriXMouse1; n < oriXMouse2; n++) {
                            var dpGroup = [n, n + 1]
                            dpGroupArray.push(dpGroup);
                        }
                        p.x = position[0];
                        p.y = position[1];
                        for (var g = 0; g < dpGroupArray.length; g++) { // caculate the distance of 'p to segments'
                            var xMouseArray = dpGroupArray[g]; // two datapoints of segment
                            for (var x = 0; x < xMouseArray.length; x++) {
                                var xMouse = xMouseArray[x];
                                if (xMouse < 0) {
                                    xMouse = 0;
                                }
                                if (xMouse >= dataLen) {
                                    xMouse = dataLen - 1;
                                }
                                var d = data[xMouse];
                                if (isNaN(d[yAttr]) && isNaN(d['lastValidY'])) { // find the nearest valid dataPoint for xMouse 
                                    var factor = (x == 0) ? -1 : 1;
                                    do {
                                        xMouse = xMouse + factor;
                                        if (xMouse <= 0 && x == 0 || xMouse >= dataLen - 1 && x == 1) {
                                            break;
                                        }
                                        d = data[xMouse];
                                    } while (isNaN(d[yAttr]) && isNaN(d['lastValidY']));
                                }
                                if (isNaN(d[yAttr]) && isNaN(d['lastValidY'])) {
                                    break;
                                }
                                var validY = isNaN(d[yAttr]) ? d['lastValidY'] : d[yAttr];
                                if (x == 0) {
                                    p0.x = this._xScale(xMouse);
                                    p0.y = this._yScale(validY);
                                } else if (x == 1) {
                                    p1.x = this._xScale(xMouse);
                                    p1.y = this._yScale(validY);
                                }
                            }
                            var dis;
                            if (p1.x == p0.x) { // p1 and p0 is the same points, caculate distance of 'point to point'
                                dis = Math.sqrt(Math.pow(p1.x - p.x, 2) + Math.pow(p1.y - p.y, 2));
                            } else {　
                                if (this._isAcuteAngle(p0, p, p1) && this._isAcuteAngle(p1, p, p0)) {
                                    //if ∠pp0p1 and ∠pp1p0 are both acute angle, caculate the distance of 'point to line'
                                    var k = ((p0.y - p1.y) / (p0.x - p1.x));
                                    var b = (p0.y * p1.x - p1.y * p0.x) / (p1.x - p0.x);
                                    dis = Math.abs(k * p.x - 1 * p.y + b) / Math.sqrt(1 + k * k);
                                } else {
                                    //else caculate the min distance of 'pp0' and 'pp1'
                                    dis = Math.min(Math.sqrt(Math.pow(p1.x - p.x, 2) + Math.pow(p1.y - p.y, 2)), Math.sqrt(Math.pow(p0.x - p.x, 2) + Math.pow(p0.y - p.y, 2)));
                                }
                            }　
                            disArray.push({
                                distance: dis,
                                index: i
                            });
                        }
                    }
                    var minIndex = 0,
                        minDis;
                    if (disArray.length > 0) {
                        minDis = disArray[0].distance;
                        for (var i = 0; i < disArray.length; i++) {
                            if (disArray[i].distance < minDis) {
                                minDis = disArray[i].distance;
                                minIndex = disArray[i].index;
                            }
                        }
                    }
                    if (minDis < highlightDist) {
                        return minIndex;
                    }
                },
                _draw_compareCharts: function() {
                    var view = this;
                    var cfg = this.config;
                    var parentSpan = this._headerContainer && $(this._headerContainer[0]).find("." + innerClass.compare).empty();
                    if (!(this._compareData && this._compareData.length > 0)) {
                        return;
                    }

                    var compareChartsData = this._compareData;
                    var upward = false;
                    compareChartsData && compareChartsData.forEach(function(c, cIndex) {
                        var compareData = c.Price.data;
                        var mainTickerXLength = view._data ? view._data.length : 0;
                        if (compareData.length !== mainTickerXLength) {
                            compareData = view._baselineXCordinates(compareData, view._data);
                        }
                        var index = 0;
                        compareData.forEach(function(d) {
                            d.x = d.index = index++;
                            d.y = d[view.yAttr];
                        });
                        c.Price.data = compareData;
                        var colorsLength = rootConfig.compareColors.length;
                        var colorIndex = (cIndex >= colorsLength && colorsLength) ? cIndex % colorsLength : cIndex;
                        var colorValue = c.tickerObject['customTickerColor'] || rootConfig.compareColors[colorIndex];
                        var globalGraphClass = view._getGlobalClass(c.tickerObject);
                        var graphCss = innerClass.compare + " compare" + "-" + colorIndex + "-class" + globalGraphClass + " " + innerClass.chartlines;
                        var graphConfig = {
                            data: compareData,
                            color: colorValue,
                            graphCss: graphCss,
                            graphType: graphTypes.Line
                        };

                        if (c.Price.data.length > 0) {
                            view._commonDraw(graphConfig);
                            //Just when cIndex = 0, check tag position if need ajust.
                            if (cIndex === 0) {
                                upward = view._firstCompareTagUpward;
                            }

                            view._drawTag(compareData, view.yAttr, colorValue,
                                "compare-tag-" + colorIndex + globalGraphClass, upward);

                            var trackballConfig = {
                                graphCss: graphCss,
                                color: colorValue,
                                data: compareData,
                                globalGraphClass: globalGraphClass
                            };

                            view._commonDrawTrackball(trackballConfig);
                        }

                        if (parentSpan) {
                            view._appendHeaderForComapre(parentSpan, c, cIndex, colorValue, globalGraphClass);
                        }

                    });

                },
                _needHideBenchmark: function() {
                    return chartConfigManager.getMainDataType() === MainDataTypes.Growth && chartConfigManager.isITFund() && rootConfig.hideBenchmark;
                },
                _removeCompare: function(event) {
                    event.data.tickers.length == 2 && rootConfig.showMTickerRemoveBtn && $(".mainChart-tickerCon").children().get(3).remove();
                    eventManager.trigger(eventManager.EventTypes.CompareTicker, event.data.toRemove, true, event.data.isFromRemoveBtn);
                },
                _removeMainTicker: function(event) {
                    event.data.tickers.splice(0, 1);
                    eventManager.trigger(eventManager.EventTypes.ChangeTicker, {
                        ticker: event.data.tickers[0]
                    }, true);
                },
                // exchange MainTicker and rebuild compare ticker
                _exchangeMainTicker: function(event) {
                    if (event.data.toChange && event.data.toChange.benchmarkType) {
                        return;
                    }
                    // first change mainTicker to compare ticker ,second use main ticker to replace compare ticker
                    var tickerCollection = event.data.tickers;
                    tickerCollection.splice(0, 1, event.data.toChange);
                    tickerCollection.splice(event.data.index, 1, rootConfig.mainTickerObject);

                    // value to the tickerCollection with new tickerCollection;
                    chartConfigManager.setTickerCollection(tickerCollection);

                    eventManager.trigger(eventManager.EventTypes.ChangeTicker, {
                        ticker: event.data.toChange
                    }, true);
                },
                _appendHeader: function() {
                    if (!this._headerContainer || !this.mainTickerObject) {
                        return;
                    }

                    var cfg = this.config;
                    var config = chartConfigManager.getConfig();
                    var $headerContainer = $(this._headerContainer[0]);
                    var yScaleDimensionElement = $headerContainer.find("." + innerClass.tickerCurrency);
                    if (yScaleDimensionElement && yScaleDimensionElement.length > 0) {
                        var ySclaleDimension;
                        var mainDataType = chartConfigManager.getMainDataType();
                        // if is pGrowthChart and is compare or is DividendEffectChart show cfg.yAxisDimensionForPercentage
                        if (mainDataType === MainDataTypes.Growth ||
                            mainDataType === MainDataTypes.DividendEffect ||
                            ((this._compareData && this._compareData.length > 0) && mainDataType !== MainDataTypes.Growth10K && !rootConfig.drawNavAndPriceChart && !config.growthBaseValue)) {
                            ySclaleDimension = cfg.yAxisDimensionForPercentage;
                        } else {
                            ySclaleDimension = rootConfig.currencyAliases[this._priceCurrency] || this._priceCurrency || 　this.mainTickerObject.currency;
                        }

                        $(yScaleDimensionElement[0]).text(ySclaleDimension);
                    }

                    if (rootConfig.legendChange !== false) {
                        var priceChangeElement = $headerContainer.find("." + innerClass.tickerNamePriceChange);
                        var priceLabelInfo = this._constructTickerPriceChangeLabel();
                        priceChangeElement.text(priceLabelInfo.label || '')
                            .removeClass(innerClass.upPriceChangeColor + ' ' + innerClass.downPriceChangeColor + ' ' + innerClass.zeroPriceChangeColor)
                            .addClass(priceLabelInfo.legendSpanColor);
                    }
                },
                _constructTickerPriceChangeLabel: function(tickerData) {
                    var legendLabel = '';
                    var lastPriceChange = Number.MIN_VALUE;
                    var lastOpenPrice = Number.MIN_VALUE;
                    var priceChange = Number.MIN_VALUE;
                    var percentageChange = Number.MIN_VALUE;
                    // The Growth10K chart value is not in percent.
                    var isPercentDataValues = chartConfigManager.isCompareOrPercentChart() && chartConfigManager.getMainDataType() !== MainDataTypes.Growth10K;

                    var dataToRefer = tickerData ? tickerData.Price.data : this._data; // Why use .slice()
                    var lastClosePrice = tickerData ? tickerData.Price.spos : this._lastClosePrice;
                    var yValue;
                    for (var len = dataToRefer.length, i = len - 1; i >= 0; i--) {
                        yValue = dataToRefer[i][this.yAttr];
                        if (!isNaN(yValue)) {
                            lastPriceChange = yValue;
                            break;
                        }
                    }
                    var growthBaseValue = chartConfigManager.getConfig().growthBaseValue;

                    if (isPercentDataValues) {
                        if (lastPriceChange !== Number.MIN_VALUE && !isNaN(lastClosePrice)) {
                            priceChange = lastClosePrice * (lastPriceChange / 100);
                            percentageChange = lastPriceChange;
                        }
                    } else {

                        if (growthBaseValue) {
                            lastClosePrice = growthBaseValue;
                        }
                        if (chartConfigManager.getConfig().chartGenre === chartConfigManager.ChartGenre.FundChart && chartConfigManager.getMainDataType() === chartConfigManager.MainDataTypes.PriceWithDividendGrowth && dataToRefer.length > 0) {
                            lastClosePrice = dataToRefer[0][this.yAttr];
                        }
                        if (lastPriceChange !== Number.MIN_VALUE && !isNaN(lastClosePrice)) {
                            if ((tickerData || (this._compareData && this._compareData.length)) && !growthBaseValue) {
                                priceChange = (lastPriceChange / 100) * lastClosePrice;
                                percentageChange = lastPriceChange;
                            } else {
                                priceChange = lastPriceChange - lastClosePrice;
                                percentageChange = (priceChange / lastClosePrice) * 100;
                            }
                        }
                    }
                    if (priceChange != Number.MIN_VALUE && percentageChange != Number.MIN_VALUE) {
                        var legendSpanColor = this._updatePriceChangeColor(priceChange.toFixed(this._decimalPlaces));
                        var _chgPDecimal = (rootConfig.chgPDecimal != undefined && rootConfig.chgPDecimal != null) ? rootConfig.chgPDecimal : rootConfig.defaultDecimal;
                        while (Math.abs(percentageChange) * Math.pow(10, _chgPDecimal) < 0.5 && percentageChange !== 0) {
                            _chgPDecimal++;
                        }
                        var changePositive = false;
                        if (percentageChange > 0) {
                            changePositive = true;
                        }
                        var _chg = util.numberFormatter(this.config.lang, priceChange, this._decimalPlaces, " ", false, true);
                        var _chgp = util.numberFormatter(this.config.lang, percentageChange, _chgPDecimal, " ", false, true);
                        if (rootConfig.showPricePlusSign && changePositive) {
                            _chg = '+' + _chg;
                            _chgp = '+' + _chgp;
                        }
                        // define two parameters to control chg || chgp
                        var _showChg = !!rootConfig.showLegendNetChange;
                        var _showChgp = !!rootConfig.showLegendPercentChange;
                        if (isPercentDataValues && rootConfig.useEUTimeSeriesData) {
                            _showChg = false;
                        }

                        if (_showChg && _showChgp) {
                            legendLabel += _chg + ' | ' + _chgp + '% ';
                        } else {
                            legendLabel = _showChg ? _chg : _showChgp ? _chgp + '% ' : '';
                        }
                    }

                    return {
                        label: legendLabel,
                        legendSpanColor: legendSpanColor
                    };
                },
                _updatePriceChangeColor: function(pricechange) {
                    var legendSpanColor;
                    if (pricechange > 0) {
                        legendSpanColor = innerClass.upPriceChangeColor;
                    } else if (pricechange < 0) {
                        legendSpanColor = innerClass.downPriceChangeColor;
                    } else {
                        legendSpanColor = innerClass.zeroPriceChangeColor;
                    }

                    return legendSpanColor;
                },
                _appendHeaderForComapre: function(parentSpan, c, cIndex, legendColor, globalGraphClass) {
                    if (parentSpan.length > 0) {
                        var view = this;
                        var cfg = this.config;
                        var legendClass = '';
                        var tickerCollection = chartConfigManager.getTickerCollection();
                        var closeEle = "<span class='" + innerClass.closeButton + "'>&#215</span>";
                        if (c.tickerObject.ticker) {
                            legendClass = "legend" + c.tickerObject.ticker.replace(/[^\d\w]/g, '') + cIndex;
                        } else {
                            legendClass = "legend" + c.tickerObject.symbol + cIndex;
                        }
                        var legendSpan = parentSpan.find("." + legendClass);
                        if (legendSpan.length === 0) {
                            var legendCon = $("<span class='compare-legend-con compare-legend-" + cIndex + "'>");
                            globalGraphClass = globalGraphClass || ' globalIndex-';
                            legendCon.addClass(globalGraphClass);
                            if (cfg.needLegendBackground) {
                                legendCon.css('background-color', c.tickerObject['customTickerColor'] || legendColor);
                            }
                            if (cfg.showLegend) {
                                legendCon.append("<span class='" + innerClass.compareLegend + "'>");
                            }

                            legendCon.append("<span class='" + legendClass + "'>");
                            legendCon.append("<span class='" + legendClass + "-price-change'>");
                            legendCon.append("<span class='" + rootConfig.cursorValueClass + ' ' + innerClass.compareValue + legendClass.replace('legend', '') + "'>");

                            var closeButton;
                            if (rootConfig.compareClose !== false) {
                                closeButton = $(closeEle).appendTo(legendCon);
                            }

                            legendCon.appendTo(parentSpan);
                        }

                        legendSpan = parentSpan.find("." + legendClass);
                        if (legendSpan) {
                            var legendText = this._getLegendText(c.tickerObject);
                            (rootConfig.legendDot) && (legendText = "●" + legendText);
                            legendSpan.addClass(innerClass.compareLegendText)
                                .css("color", legendColor)
                                .attr("title", legendText)
                                .text(legendText);

                            if (rootConfig.legendChange !== false) {
                                var legendLabelInfo = view._constructTickerPriceChangeLabel(c);
                                legendSpan.next().addClass(innerClass.comparePriceChangeText)
                                    .text(legendLabelInfo.label || '')
                                    .removeClass(innerClass.upPriceChangeColor + ' ' + innerClass.downPriceChangeColor + ' ' + innerClass.zeroPriceChangeColor)
                                    .addClass(legendLabelInfo.legendSpanColor);
                            }

                            if (cfg.showLegend) {
                                legendSpan.prev().css("background", legendColor);
                            }
                            var mainTickerInfo = this._headerContainer.find("." + innerClass.tickerInfoContainer);
                            var mainCloseBtn = mainTickerInfo.find('.close-button');

                            // add remove button for main ticker
                            if (rootConfig.showMTickerRemoveBtn && !mainCloseBtn[0]) {
                                $(closeEle).appendTo(mainTickerInfo);
                                mainTickerInfo.find('.close-button').bind('click', {
                                    tickers: tickerCollection
                                }, view._removeMainTicker);
                            }

                            // bind click event to change ticker function for every compare legend
                            if (rootConfig.exchangeMainTickerByLegend) {
                                legendCon.find('.compare-legend-text').bind('click', {
                                    toChange: c.tickerObject,
                                    index: cIndex + 1,
                                    tickers: tickerCollection
                                }, view._exchangeMainTicker);
                            }
                            closeButton && $(closeButton).bind('click', {
                                toRemove: c.tickerObject,
                                isFromRemoveBtn: true,
                                tickers: tickerCollection
                            }, view._removeCompare);
                        }
                    }
                },
                _dividendEffectHeaderProcess: function() {
                    if (!this._headerContainer || !this.mainTickerObject) {
                        return;
                    }

                    var cfg = this.config;
                    var parentSpan = $(this._headerContainer[0]).find("." + innerClass.compare);
                    var mainDataType = chartConfigManager.getMainDataType();
                    if (mainDataType === MainDataTypes.DividendEffect) {
                        if (cfg.showLegend) {
                            $("<span class='" + innerClass.dividendEffectLegend + "'>")
                                .css("background", cfg.dividendEffectProperties.color).appendTo(parentSpan);
                        }

                        var divEffectSpan = $("<span class='" + innerClass.dividendEffectLegendText + "'>").appendTo(parentSpan);
                        divEffectSpan.css("color", cfg.dividendEffectProperties.color)
                            .text(this.mainTickerObject.ticker + this.langData['mainChartHeaderDividendEffectLabel']);
                    }
                },
                _showDataNotAvailable: function() {
                    this._renderDomFrame();
                    this._hideDataNotAvailable();
                    this._outerGElem && this._outerGElem.selectAll("g").remove();
                    this._svg.select(".mainChart-zoomDrag").remove();
                    this._xAxisOuterGElem && this._xAxisOuterGElem.select("g").remove();
                    innerClass["xAxisSvg"] && this.$el.find("." + innerClass["xAxisSvg"]).hide();
                    innerClass["yAxis"] && this.$el.find("." + innerClass["yAxis"]).hide();
                    innerClass["chartHeader"] && this.$el.find("." + innerClass["chartHeader"]).hide();
                    innerClass["mainSvg"] && this.$el.find("." + innerClass["mainSvg"]).css("border-bottom", "none").css("height", this.config.minHeight);
                    innerClass["chartBody"] && this.$el.find("." + innerClass["chartBody"]).css("height", this.config.minHeight);
                    var elem = this._svg.append('g');
                    elem.append('text').attr('class', 'dataNotAvailable')
                        .text(this.langData['datanotavailable'])
                        .attr("transform", "translate(" + this._validWidth / 2 + ",40)");

                    this._$DataNotAvailableElem = elem;
                    eventManager.trigger(eventManager.EventTypes.HeightChanged);
                    chartConfigManager.setConfig({
                        isFullyRendered: true
                    });
                },

                _hideDataNotAvailable: function() {
                    this._$DataNotAvailableElem && this._$DataNotAvailableElem.remove();
                    innerClass["xAxisSvg"] && this.$el.find("." + innerClass["xAxisSvg"]).show();
                    innerClass["yAxis"] && this.$el.find("." + innerClass["yAxis"]).show();
                    innerClass["chartHeader"] && this.$el.find("." + innerClass["chartHeader"]).show();
                    innerClass["mainSvg"] && this.$el.find("." + innerClass["mainSvg"]).css({
                        "border-bottom": "none",
                        "height": this._validHeight
                    });

                    innerClass["chartBody"] && this.$el.find("." + innerClass["chartBody"]).css("height", this._validHeight + this.config.xAxisHeight);
                    eventManager.trigger(eventManager.EventTypes.HeightChanged);
                },
                _clearView: function(chartType) {
                    if (this._outerGElem) {
                        this._outerGElem.selectAll("g").remove();
                        this._outerGElem.selectAll(".track-ball").remove();
                        this._clearTrackball();
                    }

                    this._hideDataNotAvailable();
                },
                _changeLayout: function() {
                    this._clearView();
                    if (this._data && (this._data.length > 0)) {
                        this._renderView();
                        this._createSelectionMode();
                    } else {
                        this._showDataNotAvailable();
                        this._viewInited = false;
                    }
                },
                _getDrawingGContainer: function() {},
                _createSvgWrapper: function() {
                    var cfg = this.config;
                    var view = this;
                    var d3_container = d3.select(cfg.container);
                    var mainDiv = d3_container.select("." + innerClass.chartBody);
                    if (mainDiv.empty()) {
                        mainDiv = d3_container.append('div').classed(innerClass.chartBody, true)
                        var svg = this._svg = mainDiv.append('svg').classed(innerClass.mainSvg, true)
                        var yAxisGElem = d3_container.select('.' + innerClass.yAxis);
                        if (!this._yAxisOuterGElem || (this._yAxisOuterGElem && this._yAxisOuterGElem.empty())) {
                            yAxisGElem = d3_container.select('.' + innerClass.mainSvg)
                                .append("g").classed(innerClass.yAxis, true)
                                .style("width", cfg.yAxis.width + "px");

                            this._yAxisOuterGElem = yAxisGElem;
                        }
                        this._outerGElem = svg.append("g");
                        this._drawingsGElem = svg.append("g");
                        //when mouse move in the mainchart, we will show crosshairs
                        this._svg.on('mousedown.mainchart', function() {
                            d3.event.preventDefault();
                            view._mouseDown(d3.mouse(this));
                        }).on("touchstart.mainchart", function() {
                            view._mouseDown(d3.mouse(this));
                        }).on('mousemove.mainchart', function() {
                            view._mousemove(d3.event, d3.mouse(this), view._headerHeight);
                        }).on('mouseout.mainchart', function() {
                            view._mouseout(d3.mouse(this));
                        }).on("touchmove.mainchart", function() {
                            view._mousemove(d3.event, d3.mouse(this), view._headerHeight);
                        }).on('mouseup.mainchart', function() {
                            d3.event.preventDefault();
                            view._mouseUp(d3.mouse(this));
                        }).on('touchend.mainchart', function() {
                            view._mouseUp(d3.mouse(this));
                        });
                    }
                    var mainWidth;
                    if (cfg.yAxis.position == "outer") {
                        mainWidth = this._validWidth + cfg.margin.left + cfg.margin.right + cfg.yAxis.width + cfg.yAxisMargin + "px";
                    } else {
                        mainWidth = this._validWidth + cfg.margin.left + cfg.margin.right + "px";
                    }

                    mainDiv.style({
                        "width": mainWidth,
                        "height": this._validHeight + cfg.xAxisHeight + "px"
                    });

                    this._svg.style({
                        "width": mainWidth,
                        "height": (this._validHeight + 1) + "px"
                    });

                    this._outerGElem.attr("transform", this._getGElemTranslate());
                    this._drawingsGElem.attr("transform", this._getGElemTranslate());
                    this._containerOffset = $(cfg.container).offset();
                    var xAxisSvg = d3_container.select('.' + innerClass.xAxisSvg);

                    if (!this._xAxisOuterGElem || (this._xAxisOuterGElem && this._xAxisOuterGElem.empty())) {
                        xAxisSvg = d3_container.select('.' + innerClass.chartBody)
                            .append("svg").classed(innerClass.xAxisSvg, true)
                            .style("height", cfg.xAxisHeight + "px");
                        this._xAxisOuterGElem = xAxisSvg.append("g");
                        if (cfg.yAxis.position == "outer") {
                            this._xAxisOuterGElem.attr("transform", this._getGElemTranslate());
                        }
                    }
                    if (cfg.yAxis.position == "outer") {
                        xAxisSvg.style("width", this._validWidth + cfg.yAxis.width + cfg.yAxisMargin + "px");
                    } else {
                        xAxisSvg.style("width", this._validWidth + "px");
                    }

                },
                _mouseout: function(e) {
                    d3.select(this.config.container).selectAll(".mkts-cmpt-svgcht-chart-lines").classed('mkts-cmpt-svgcht-hightlight-line', false);
                },
                _caculateValidSize: function() {
                    var cfg = this.config;
                    if (cfg.yAxis.position == "outer") {
                        this._validWidth = cfg.width - cfg.margin.left - cfg.margin.right - (cfg.yAxis.width + cfg.yAxisMargin);
                    } else {
                        this._validWidth = cfg.width - cfg.margin.left - cfg.margin.right;
                    }
                    this._validHeight = cfg.height - cfg.margin.top - cfg.margin.bottom - cfg.xAxisHeight - this.$el.find("." + innerClass.chartHeader).outerHeight(true);
                },
                _drawXAxis: function() {
                    var cfg = this.config;
                    this._createXAxisTickInfos();
                    var view = this;
                    if (cfg.xAxis.show) {
                        this._xAxisOuterGElem.select("g").remove();
                        var config = {
                            containerGElem: this._xAxisOuterGElem,
                            tickSize: cfg.xAxis.tickSize || 0,
                            tickPadding: 5,
                            tickValues: this._xTickInfos.tickValues,
                            orient: cfg.xAxis.orient,
                            graphCss: innerClass.xAxis,
                            graphType: graphTypes.Axis,
                            scale: this._xScale,
                            width: this._validWidth,
                            height: 0,
                            xTickInfos: this._xTickInfos,
                            tickFormatFunc: function() {
                                return view._formatXAxisLabels.apply(view, arguments);
                            }
                        };
                        this._commonDraw(config);
                    }
                },
                _renderDomFrame: function() {
                    if (!rootConfig.hideSymbol) {
                        this._renderHeader();
                    }

                    this._caculateValidSize();
                    this._createSvgWrapper();
                },
                _renderView: function() {
                    var view = this;
                    this._renderDomFrame();
                    this._createScale();
                    if (!this.drawObj) {
                        this.drawObj = drawings(chartConfigManager)
                            .target(this._drawingsGElem)
                            .findByPosition(function(position) {
                                return view._findNearItem(position, null, true);
                            });
                    }

                    this.drawObj.width(this._validWidth)
                        .height(this._validHeight)
                        .xScale(this._xScale)
                        .yScale(this._yScale);

                    this._renderContainedViews();
                    this._viewInited = true;
                    eventManager.trigger(eventManager.EventTypes.HeightChanged);
                },
                _VBPTipProcess: function(position, offsetTop) {
                    if ($.isArray(rootConfig.indicators)) {
                        for (var i = 0, len = rootConfig.indicators.length; i < len; i++) {
                            if (rootConfig.indicators[i].name === dataAdapter.IndicatorName.VBP) {
                                var offsetLeft = 0;
                                if (this.config.yAxis.position == "outer") {
                                    offsetLeft = this.config.yAxis.width + this.config.yAxisMargin;
                                }
                                this._VBPDrawObj.tipProcess(position, offsetTop, offsetLeft);
                                break;
                            }
                        }
                    }
                },
                _displayViewAfterChangeDomain: function() {
                    this._clearView();
                    this._renderContainedViews();
                },
                _renderContainedViews: function() {
                    if (!this._data || this._data.length === 0) {
                        this._showDataNotAvailable();
                        return;
                    } else {
                        var formater = rootConfig.momentDateTime;
                        if (!$.isNumeric(rootConfig.frequency)) {
                            formater = rootConfig.momentDate;
                        }
                        var self = this;
                        self.mainTickerLastDateObj = {
                            date: moment(self._lastTradeTime).format(formater),
                            frequency: rootConfig.frequency,
                            rtTicker: this.mainTickerObject.gkey
                        }
                        eventManager.trigger(eventManager.EventTypes.OnMainTickerDrawEnd, self.mainTickerLastDateObj);
                        this._hideDataNotAvailable();
                    }

                    // Update calendar date value and frequency according returned data.
                    eventManager.trigger(eventManager.EventTypes.UpdateMenuIntervalForRender, [this._data[0].date, this._data[this._data.length - 1].date]);
                    this._createCommonPlotter();
                    this._drawXAxis();
                    if (!this._drawYAxis()) {
                        return;
                    }
                    this._caculateValidSize();
                    this._xScale.range([0, this._validWidth]);
                    this._setTagUpward();
                    this._drawGridLines();
                    this._drawMainGraph();
                    this._globalGraphIndex = 0;
                    //hideBenchmark from user
                    if (!this._needHideBenchmark()) {
                        this._draw_compareCharts();
                    } else {
                        this._headerContainer && $(this._headerContainer[0]).find("." + innerClass.compare).empty();
                    }

                    this._draw_Indicators();
                    this._dividendEffectHeaderProcess();
                    this._appendHeader();
                    var mainDataType = chartConfigManager.getMainDataType();
                    var isGrowthType = (mainDataType === MainDataTypes.Growth10K ||
                        mainDataType === MainDataTypes.Growth ||
                        mainDataType === MainDataTypes.DividendEffect)
                    if (!((this._compareData && this._compareData.length > 0) || isGrowthType)) {
                        var tempGlobalIndex = this._globalGraphIndex;
                        this._draw_Fundamentals(false, tempGlobalIndex);
                        this._appendHeaderForFundamentals();
                    } else {
                        this._clearHeaderForFundamentals();
                    }

                    this._drawEvents();
                    this._drawMainTag();
                    this._setSliderDateRange();
                    this.drawObj && this.drawObj.redraw(this._data);
                    chartConfigManager.setConfig({
                        isFullyRendered: true
                    });
                },
                _draw_Indicators: function() {
                    var view = this;
                    var cfg = this.config;
                    var parentSpan, $headerContainer;

                    if (this._headerContainer) {
                        $headerContainer = $(this._headerContainer[0]);
                        parentSpan = $headerContainer.find("." + innerClass.indicators).empty();
                    }
                    var indicatorsData = this._indicatorsData;
                    if (!indicatorsData) {
                        return;
                    }

                    for (var indicatorName in indicatorsData) {
                        if (cfg.indicatorProperties.hasOwnProperty(indicatorName)) {
                            var indiData = indicatorsData[indicatorName];
                            if (indicatorName === dataAdapter.IndicatorName.PrevClose &&
                                (cfg.intervalType !== IntervalTypes.OneDay || chartConfigManager.isCompareOrPercentChart())) {
                                continue;
                            }

                            if (!$.isArray(indiData)) {
                                indiData = [indiData];
                            }

                            var globalGraphClass;
                            var globalGraphClassList = [];
                            var needMergeHeader = (needMergeHeaderIndicators.indexOf(indicatorName) >= 0);
                            // code for drawing chart, no change
                            indiData.forEach(function(graphData, chartIndex) {
                                if (!needMergeHeader || (needMergeHeader && chartIndex === 0)) {
                                    globalGraphClass = view._getGlobalClass();
                                }
                                globalGraphClassList.push(globalGraphClass);

                                view._draw_Indicators_graph(graphData, chartIndex, indicatorName, globalGraphClass);
                            });

                            // code for drawing legend
                            var indicatorConfig = chartConfigManager.getConfig().indicators.filter(function(i) {
                                return i.name === indicatorName;
                            });
                            var indicatorObj = indicatorConfig.length > 0 ? indicatorConfig[0] : [];
                            view._constructIndicatorLegend(parentSpan, indicatorObj, indicatorName, globalGraphClassList);
                        }
                    }

                    var _headerHeight = $headerContainer.outerHeight(true);
                    if (this._headerHeight !== _headerHeight) {
                        this._headerHeight = _headerHeight;
                    }
                },
                _drawEvents: function() {
                    var view = this;
                    if (this._eventsData) {
                        for (var eventData in this._eventsData) {
                            if (eventData === dataAdapter.Events.Dividend && !view.config.showDividend) {
                                continue;
                            }
                            view._drawEvent(eventData, view._eventsData[eventData]);
                        }
                    }
                },
                _clearHeaderForFundamentals: function() {
                    if (!this._headerContainer) {
                        return;
                    }

                    var cfg = this.config;
                    var $headerContainer = $(this._headerContainer[0]);
                    var parentSpan = $headerContainer.find("." + innerClass.fundamentals);
                    if (parentSpan) {
                        parentSpan.empty();
                    }
                },
                _renderHeader: function() {
                    if (!this.mainTickerObject) {
                        return;
                    }
                    var cfg = this.config,
                        margin = this.config.margin,
                        chartClass = innerClass;
                    var headerContainer = $(cfg.container).find("." + chartClass.chartHeader);

                    if (!headerContainer.children().length) {
                        headerContainer = this._headerContainer = $("<div class=\"" + chartClass.chartHeader + "\"/>").prependTo(cfg.container);

                        headerContainer.append("<div class=\"" + chartClass.tickerCurrency + "\"/>");
                        if (rootConfig.hideCurrency) {
                            headerContainer.find("." + chartClass.tickerCurrency).hide();
                        }

                        var tickerInfoContainer = $("<span/>");
                        var globalClass, mainColor = this.mainTickerObject['customTickerColor'];
                        if (mainColor) {
                            globalClass = ' globalIndex-';
                        } else {
                            mainColor = rootConfig.mainTickerColor
                            globalClass = this._getGlobalClass(this.mainTickerObject, null, -1);
                        }

                        tickerInfoContainer.css({
                            'color': mainColor
                        });
                        if (cfg.needLegendBackground) {
                            tickerInfoContainer.css({
                                'background-color': mainColor
                            });
                        }
                        var containerClass = innerClass.tickerInfoContainer + globalClass;
                        tickerInfoContainer.addClass(containerClass);
                        if (cfg.showLegend) {
                            tickerInfoContainer.append("<span class=\"" + chartClass.tickerNameLegend + "\"/>");
                        }
                        if (!rootConfig.hideMainLegend) {
                            tickerInfoContainer.append("<span class=\"" + chartClass.tickerName + "\"/>");
                            tickerInfoContainer.append("<span class=\"" + chartClass.tickerNamePriceChange + "\"/>");
                        }
                        tickerInfoContainer.append("<span class=\"" + chartClass.mainValue + "\"/>");
                        tickerInfoContainer.appendTo(headerContainer);
                        headerContainer.append("<span class=\"" + chartClass.compare + "\"/>");
                        headerContainer.append("<span class=\"" + chartClass.indicators + "\"/>");
                        headerContainer.append("<span class=\"" + chartClass.fundamentals + "\"/>");
                        // error messages
                        headerContainer.append("<span class=\"" + chartClass.errorMessage + "\"/>");
                        if (rootConfig.hideCurrency) {
                            headerContainer.find("." + chartClass.tickerCurrency).hide();
                        }
                        if (rootConfig.hideLegend) {
                            var tempSelector = "." + chartClass.tickerInfoContainer + ",." + chartClass.compare + ",." + chartClass.fundamentals + ",." + chartClass.indicators;
                            headerContainer.find(tempSelector).hide();
                        }
                    }

                    var currency = "";
                    var legendText = "";
                    if (this.mainTickerObject) {
                        currency = rootConfig.currencyAliases[this._priceCurrency] || this._priceCurrency || this.mainTickerObject.currency;
                        legendText = this._getLegendText(this.mainTickerObject);
                    }

                    headerContainer.find("." + chartClass.tickerCurrency).text(currency);
                    tickerInfoContainer = tickerInfoContainer || headerContainer.find("." + chartClass.tickerInfoContainer);
                    (rootConfig.legendDot) && (legendText = "●" + legendText);
                    tickerInfoContainer.find("." + chartClass.tickerName).attr("title", legendText).text(legendText);
                    tickerInfoContainer.find("." + chartClass.mainValue).addClass(rootConfig.cursorValueClass);

                    var errorMessageContainer = headerContainer.find("." + chartClass.errorMessage);
                    errorMessageContainer.text(this.langData[rootConfig.compareChartsMaxAllowedErrorLabel]);

                    if (!errorMessageContainer.children().length) {
                        $('<span class="' + chartClass.errorMessageCloseButton + '">&#215</span>')
                            .appendTo(errorMessageContainer)
                            .bind('click', function() {
                                errorMessageContainer.hide();
                            });
                    }

                    headerContainer.css("margin-left", margin.left + "px");
                    this._headerHeight = $(headerContainer[0]).outerHeight(true);
                },
                _createSelectionMode: function() {
                    var selectionMode = this._lastSelectionMode = this.config.selectionMode;
                    var SelectionModes = chartConfigManager.SelectionModes;
                    switch (selectionMode) {
                        case SelectionModes.Pan:
                            this._createPan();
                            if (isMobile) {
                                this._disablePan();
                                this._disableZoomIn();
                            }
                            break;
                        case SelectionModes.ZoomIn:
                            this._createZoomIn();
                            break;
                    }
                },
                _resetZoomIn: function() {
                    if (this._lastSelectionMode === SelectionModes.ZoomIn && this._beforeZoomInDateRange) {
                        var dateRange = this._beforeZoomInDateRange.pop();
                        if (dateRange) {
                            var data = this._loadedData || this._data;
                            var startIndex = this._getDataIndexByDate(dateRange[0], null, null, data);
                            var endIndex = this._getDataIndexByDate(dateRange[1], null, null, data);
                            var eventTypes = eventManager.EventTypes;
                            eventManager.trigger([eventTypes.UpdateMenuInterval, eventTypes.ChangeDateRangeByZoom], [dateRange[0], dateRange[1]]);
                            if (startIndex == null || endIndex == null) {
                                var newDateRange = dateRange;
                                chartConfigManager.setConfig({
                                    dateRange: newDateRange
                                });
                                eventManager.trigger(eventTypes.ChangeIntervalFrequency);
                            } else {
                                eventManager.trigger(eventTypes.ChangeXAxisDomain, [startIndex, endIndex]);
                            }
                        }
                    }
                },
                _enableZoomIn: function() {
                    this._brushElem && this._brushElem.style("display", "");
                },
                _disableZoomIn: function() {
                    this._brushElem && this._brushElem.style("display", "none");
                },
                _createZoomIn: function() {
                    this._brushElem && this._brushElem.remove();
                    var cfg = this.config;
                    var view = this;
                    this._beforeZoomInDomain = null;
                    this._zoomInOffset = 0;
                    var config = {
                        containerGElem: this._svg,
                        graphType: graphTypes.Brush,
                        graphCss: innerClass.brush,
                        yValue: 0,
                        height: this._validHeight, //this._validHeight + 7,
                        insertBeforeCss: null,
                        brushStartResponseFunc: function(brushFunc) {
                            var xDomain = view._xScale.domain();
                            if (xDomain) {
                                var start = Math.round(xDomain[0]);
                                var end = Math.round(xDomain[1]);
                                var dataLength = view._data.length;
                                if (end >= dataLength) {
                                    end = dataLength - 1;
                                }
                                var beforeRange = view._beforeZoomInDateRange || [];
                                beforeRange.push([view._data[start].date, view._data[end].date]);
                                view._beforeZoomInDateRange = beforeRange;
                            }
                        },
                        brushResponseFunc: function() {
                            view._zooming = true;
                        },
                        brushEndResponseFunc: function(brushFunc) {
                            view._brushEndResponse(brushFunc);
                            view._zooming = false;
                        },
                        setBrushFunc: function(brushFunc) {
                            view._brushFunc = brushFunc;
                        }
                    };

                    this._brushElem = this._commonDraw(config);
                },
                _brushEndResponse: function(brushFunc) {
                    var brushExtentIsEmpty = brushFunc.empty();
                    var brushDomain = brushExtentIsEmpty ? this._xScale.domain() : brushFunc.extent();
                    var startIndex = Math.ceil(brushDomain[0]);
                    var endIndex = Math.floor(brushDomain[1]);
                    if (endIndex > (this._data.length - 1)) {
                        endIndex = this._data.length - 1;
                    }

                    if (startIndex < endIndex) {
                        var startDate = this._data[startIndex].date;
                        var endDate = this._data[endIndex].date;
                        var dateRange = [startDate, endDate];
                        var eventTypes = eventManager.EventTypes;
                        eventManager.trigger([eventTypes.UpdateMenuInterval, eventTypes.ChangeDateRangeByZoom], dateRange);
                        eventManager.trigger(eventTypes.ChangeXAxisDomain, [startIndex, endIndex], dateRange, true); //true is isZoonIn
                    }
                    brushFunc.clear();
                    this._brushElem.call(brushFunc);
                    this.drawObj && this.drawObj.redraw(this._data);

                    // add callback function before zoomIn
                    this._zooming && eventManager.trigger(eventTypes.onbeforeZoomIn, {
                        name: 'zoomIn',
                        value: {
                            startDate: moment(startDate).format(rootConfig.intradayDateFormater),
                            endDate: moment(endDate).format(rootConfig.intradayDateFormater)
                        }
                    });
                },
                _enablePan: function() {
                    this._zoomElem && this._zoomElem.style("display", "");
                },
                _disablePan: function() {
                    this._zoomElem && this._zoomElem.style("display", "none");
                },
                //this pan support zoom in by mouse wheel
                _createPan: function() {
                    this._beforeZoomInDateRange = null;
                    var cfg = this.config;
                    var view = this;
                    var zoomElem = this._svg.select("." + innerClass.zoomDrag);
                    zoomElem && zoomElem.remove();
                    var currentCursorType;
                    var graphConfig = {
                        view: this,
                        containerGElem: this._svg,
                        graphCss: innerClass.zoomDrag,
                        graphType: graphTypes.ZoomDrag,
                        insertBeforeCss: null,
                        margin: cfg.margin,
                        zoomDragMaskCss: innerClass.zoomDragMask,
                        width: this._validWidth,
                        height: this._validHeight,
                        panStartCallback: function(zoom) {
                            currentCursorType = rootConfig.cursorType;
                            view._panStartResponse(zoom);
                        },
                        panCallback: function(zoom) {
                            if (!view._panning) {
                                eventManager.trigger(eventManager.EventTypes.ToggleCrossharis);
                                eventManager.trigger(eventManager.EventTypes.ChangeChartCursor, 'off');
                                view._panning = true;
                            }
                            view._panResponse(zoom);
                        },
                        panEndCallback: function(zoom) {
                            view._panning = false;
                            eventManager.trigger(eventManager.EventTypes.ChangeChartCursor, currentCursorType);
                            view._panEndResponse(zoom);
                        }
                    };

                    if (cfg.yAxis.orient == "left" && cfg.yAxis.position == "outer") {
                        graphConfig.translateLeft = graphConfig.margin.left + cfg.yAxis.width + cfg.yAxisMargin;
                    }
                    this._zoomElem = this._commonDraw(graphConfig);
                    this._zoomOriData = this._data.slice(); //it will change when create zoom again.
                },
                _panStartResponse: function(zoom) {
                    this._panTranslate = null;
                },
                _panEndResponse: function(zoom) {
                    this._changeViewByPan();
                },
                _panResponse: function(zoom) {
                    var tx = zoom.translate()[0];
                    var ty = 0;
                    var domain = this._xScale.domain();
                    var range = this._xScale.range();
                    var oriDataLength = this._zoomOriData.length;
                    var offsetDomain = this._preloadInfo.offsetDomain || 0;
                    if (domain[1] > (oriDataLength - 1)) {
                        tx = tx - this._xScale(oriDataLength) + range[1];
                    } else if (offsetDomain + domain[0] < 0) {
                        tx = tx - this._xScale(-offsetDomain) + range[0];
                    }

                    zoom.translate([tx, ty]);
                    if (this._panResponseCount !== this.config.panResponseSpan) {
                        this._panResponseCount++;
                        this._panTranslate = tx;
                    } else {
                        this._panResponseCount = 0;
                        this._changeViewByPan();
                        this._panTranslate = null;
                    }
                },
                _changeViewByPan: function() {
                    var tx = this._panTranslate;
                    if (!tx) {
                        return;
                    }
                    //Because the this domain not changed as the chart. we need revise it.
                    var newDomain = this._xScale.domain();
                    var offsetDomain = this._preloadInfo.offsetDomain || 0;
                    var revisedStartIndex = offsetDomain + Math.ceil(newDomain[0]);
                    var revisedEndIndex = offsetDomain + Math.ceil(newDomain[1]);
                    var data = this._loadedData || this._data;
                    if (revisedStartIndex <= 0) {
                        this._xScale.domain([0, this._data.length - 1]);
                        this._loadDataForPanning();
                    } else if (revisedStartIndex > 0) {
                        this._preloadData(revisedStartIndex, revisedEndIndex); //Check if need load more date and do it if need.
                        var startDate = data[revisedStartIndex].date; //need to check if need -1
                        var loadedDataLen = data.length;
                        var endIndex = revisedEndIndex < loadedDataLen ? revisedEndIndex : loadedDataLen - 1;
                        var endDate = data[endIndex].date;
                        var dateRange = [startDate, endDate];
                        var eventTypes = eventManager.EventTypes;
                        this._lastDateRange = dateRange;
                        eventManager.trigger([eventTypes.UpdateMenuInterval, eventTypes.ChangeDateRangeByZoom], dateRange);
                        eventManager.trigger(eventTypes.ChangeXAxisDomain, [revisedStartIndex, revisedEndIndex], dateRange);
                    }
                },
                _drawMainGraph: function() {
                    var chartType = chartConfigManager.getMainChartType();
                    var globalClass = this._getGlobalClass(this.mainTickerObject, null, -1);
                    this["_draw_" + chartType] && this["_draw_" + chartType](chartType, globalClass);
                    if (chartConfigManager.getMainDataType() === MainDataTypes.DividendEffect) {
                        this._draw_dividendEffectChart(MainChartTypes.DividendEffectChart, globalClass);
                    }
                },
                _drawEvent: function(eventType, eventData) {
                    var cfg = this.config;
                    var chartType = chartConfigManager.getMainChartType();
                    var eventPropertyName = Object.keys(dataAdapter.Events).filter(function(e) {
                        return dataAdapter.Events[e] === eventType
                    })[0];
                    var eventProperty = cfg.eventProperties[eventPropertyName];
                    this._normalizeEventsData(eventData.data);
                    var graphConfig = {
                        data: eventData.data,
                        xRadious: cfg.eventProperties.xRadious,
                        yRadious: cfg.eventProperties.yRadious,
                        color: eventProperty.color,
                        downColor: eventProperty.downColor,
                        innerText: this.langData[eventProperty.text],
                        y0Value: this._validHeight - cfg.eventProperties.yRadious - 2,
                        dy: cfg.eventProperties.dy,
                        textAnchor: cfg.eventProperties.textAnchor,
                        textColor: cfg.eventProperties.textColor,
                        graphCss: innerClass.events,
                        textClass: innerClass.eventsText,
                        graphType: graphTypes.Event,
                    };
                    this["_elem_" + chartType] = this._commonDraw(graphConfig);
                },
                _getTagData: function(dataArr, attName) {
                    var result = [];
                    for (var len = dataArr.length, i = len - 1; i >= 0; i--) {
                        if (!isNaN(dataArr[i][attName])) {
                            break;
                        }
                    }
                    return dataArr.slice(0, ++i);
                },
                _setTagUpward: function() {
                    this._mainTagUpward = false;
                    if (this._compareData && this._compareData.length > 0 && this._compareData[0].Price.data.length > 0) {
                        var tagData = this._getTagData(this._data, this.yAttr);
                        var tickerPrice = tagData[tagData.length - 1] && tagData[tagData.length - 1][this.yAttr];
                        var yPos = this._yScale(tickerPrice);
                        var firstCompareTagData = this._getTagData(this._compareData[0].Price.data, this.yAttr);
                        if (firstCompareTagData.length > 0) {
                            var firstCompareTickerPrice = firstCompareTagData[firstCompareTagData.length - 1][this.yAttr];
                            var firstCompareTickerPriceYPos = this._yScale(firstCompareTickerPrice);
                            if (Math.abs(firstCompareTickerPriceYPos - yPos) < (this.config.tagProperties.tagHeight * 1.5)) {
                                if (firstCompareTickerPriceYPos < yPos) {
                                    this._firstCompareTagUpward = true;
                                } else {
                                    this._mainTagUpward = true;
                                    this._firstCompareTagUpward = false;
                                }
                            }
                        } else {
                            this._firstCompareTagUpward = false;
                        }
                    }
                },
                _drawMainTag: function(highLight) {
                    var mainTagClass = 'main-tag' + this._getGlobalClass(this.mainTickerObject, null, -1);
                    var mainColor = this.config.tagProperties.tagFillColor || (this.mainTickerObject['customTickerColor'] || rootConfig.mainTickerColor);
                    this._drawTag(this._data, this.yAttr, mainColor, mainTagClass, this._mainTagUpward, highLight);
                },

                _drawTag: function(dataArr, yAttr, tagColor, tagClass, upward, highLight) {
                    var tagData = this._getTagData(dataArr, yAttr);
                    var tagDataLength = tagData.length;
                    if (tagDataLength > 0) {
                        var tickerPrice = tagData[tagDataLength - 1][yAttr];
                        var decimalPlaces = this._decimalPlaces;
                        if (this.config.dataType === MainDataTypes.Growth10K && rootConfig.growth10KDecimal) {
                            decimalPlaces = rootConfig.growth10KDecimal;
                        }
                        var tagX = tagDataLength - 1,
                            tagY = tickerPrice,
                            tagText = util.numberFormatter(this.config.lang, tickerPrice, decimalPlaces, " ", false, true),
                            rightSpace = dataArr.length - tagDataLength;
                        var cfg = this.config;
                        var chartType = chartConfigManager.getMainChartType();
                        var xPosition = this._xScale(tagX);
                        var yPosition = this._yScale(tagY);
                        if (isNaN(xPosition) || isNaN(yPosition)) {
                            return;
                        }
                        var tagClass = innerClass.tagClass + " " + tagClass;
                        var tagTextClass = innerClass.tagTextClass;
                        if (cfg.tagProperties.tagMode) {
                            tagClass = tagClass + ' mkts-components-chart-tag-mode-' + cfg.tagProperties.tagMode;
                            tagTextClass = tagTextClass + ' ' + tagTextClass + '-mode-' + cfg.tagProperties.tagMode;
                        }
                        if(!rootConfig.showTag){
                            tagClass= tagClass + ' hide';
                        }
                        var graphConfig = {
                            xPosition: xPosition,
                            yPosition: yPosition,
                            tagHeight: cfg.tagProperties.tagHeight,
                            tagWidth: cfg.tagProperties.tagWidth,
                            tagText: tagText,
                            graphCssText: tagTextClass,
                            graphCss: tagClass,
                            spaceOnRight: this._xScale(rightSpace),
                            fillColor: tagColor,
                            arrowWidth: cfg.tagProperties.arrowWidth,
                            arrowHeight: cfg.tagProperties.arrowHeight,
                            sideMargin: cfg.tagProperties.sideMargin,
                            textTopMargin: cfg.tagProperties.textTopMargin,
                            tagMode: cfg.tagProperties.tagMode,
                            graphType: graphTypes.Tag,
                            height: this._validHeight,
                            upward: upward,
                            componentContainer: rootConfig.$componentContainer,
                            yVerticalMargin: cfg.tagProperties.yVerticalMargin
                        };

                        if (highLight) {
                            this._outerGElem.selectAll('.mkts-components-chart-highlight-tag').remove();
                            graphConfig.containerGElem = this._outerGElem.append('g').attr('class', 'mkts-components-chart-highlight-tag');
                        }
                        return this._commonDraw(graphConfig);
                    }
                },
                _draw_lineChart: function(chartType, globalClass, notMain) {
                    if (this._needAjustLine) {
                        this._draw_dotChart(MainChartTypes.DotChart, globalClass);
                    } else {
                        var graphConfig = {
                            graphCss: innerClass[chartType] + globalClass + " " + innerClass.chartlines,
                            graphType: graphTypes.Line
                        };

                        var mainColor = this.mainTickerObject['customTickerColor'] || rootConfig.mainTickerColor;
                        !notMain && mainColor && (graphConfig.color = mainColor);
                        this["_elem_" + chartType] = this._commonDraw(graphConfig);

                        var trackballConfig = {
                            trackContainer: this._outerGElem,
                            graphCss: globalClass,
                            color: graphConfig.color,
                            trackType: 'circle', //default
                            data: this._data //default
                        };

                        this._commonDrawTrackball(trackballConfig);
                    }
                },
                _draw_mountainChart: function(chartType, globalClass) {
                    var dataType = chartConfigManager.getMainDataType();
                    var _chartType = chartType;
                    switch (dataType) { // compatibility old version when chartType is pGrowthChart, TenKGrowthChart, DividendEffectChart and PostTax
                        case MainDataTypes.Growth:
                            _chartType = MainChartTypes.pGrowthChart;
                            break;
                        case MainDataTypes.Growth10K:
                            _chartType = MainChartTypes.TenKGrowthChart;
                            break;
                        case MainDataTypes.DividendEffect:
                            _chartType = MainChartTypes.DividendEffectChart;
                            break;
                        case MainDataTypes.PostTax:
                            _chartType = MainChartTypes.PostTaxReturnChart;
                            break;
                    }
                    var cfg = this.config;
                    var graphConfig = {
                        graphCss: innerClass[_chartType] + globalClass,
                        graphType: graphTypes.Area,
                        y0Value: this._validHeight
                    };

                    graphConfig.color = this.mainTickerObject['customTickerColor'] || rootConfig.mainTickerColor;
                    this["_elem_" + chartType] = this._commonDraw(graphConfig);
                    this._draw_lineChart(MainChartTypes.LineChart, globalClass);
                },
                _draw_dotChart: function(chartType, globalClass) {
                    var cfg = this.config;

                    var graphConfig = {
                        graphCss: innerClass[chartType] + globalClass,
                        graphType: graphTypes.Dot,
                        dotRadius: cfg.dotChartRadius
                    };

                    graphConfig.color = this.mainTickerObject['customTickerColor'] || rootConfig.mainTickerColor;
                    this["_elem_" + chartType] = this._commonDraw(graphConfig);

                    var trackballConfig = {
                        graphCss: globalClass,
                        color: graphConfig.color
                    };

                    this._commonDrawTrackball(trackballConfig);
                },
                _draw_abChart: function(chartType, globalClass) {
                    var cfg = this.config;
                    var referValue, yAtrr = this.yAttr;
                    for (var i = 0, len = this._data.length; i < len; i++) {
                        referValue = this._data[i][yAtrr];
                        if ($.isNumeric(referValue)) {
                            break;
                        }
                    }

                    var graphConfig = {
                        graphCss: innerClass[chartType] + globalClass + ' ' + innerClass.chartlines,
                        graphType: graphTypes.AboveBelow,
                        //referValue: this._lastClosePrice,
                        definedFunc: function(d) {
                            return !isNaN(d.y);
                        },
                        height: this._validHeight,
                        aboveClipID: cfg.abChartAboveClipID,
                        belowClipID: cfg.abChartBelowClipID,
                        aboveAreaCss: innerClass.aboveArea,
                        belowAreaCss: innerClass.belowArea,
                        ASColor: cfg.ASColor,
                        AFColor: cfg.AFColor,
                        BSColor: cfg.BSColor,
                        BFColor: cfg.BFColor,
                        referValue: referValue
                    };

                    this["_elem_" + chartType] = this._commonDraw(graphConfig);

                    var trackballConfig = {
                        graphCss: globalClass,
                        color: this.mainTickerObject['customTickerColor'] || rootConfig.mainTickerColor
                    };

                    this._commonDrawTrackball(trackballConfig);
                },
                _draw_ohlcChart: function(chartType, globalClass) {
                    var cfg = this.config;
                    var graphConfig = {
                        graphCss: innerClass[chartType] + globalClass,
                        graphType: graphTypes.OHLC,
                        tickSize: cfg.ohlcTickSize
                    };

                    this["_elem_" + chartType] = this._commonDraw(graphConfig);
                },
                _draw_candlestickChart: function(chartType, globalClass) {
                    var cfg = this.config;
                    var graphConfig = {
                        graphCss: innerClass[chartType] + globalClass,
                        graphType: graphTypes.Candlestick,
                        tickSize: cfg.candlestickTickSize
                    };

                    this["_elem_" + chartType] = this._commonDraw(graphConfig);
                },
                _draw_dividendEffectChart: function(chartType, globalClass) {
                    var cfg = this.config;
                    if (this._dividendEffectData) {
                        var dividendGraphConfig = {
                            data: this._dividendEffectData,
                            color: cfg.dividendEffectProperties.color,
                            graphCss: innerClass.dividendEffectCompareChart + globalClass,
                            yAttributeName: "value",
                            graphType: graphTypes.Line,
                        };

                        this["_elem_" + chartType] = this._commonDraw(dividendGraphConfig);

                    }
                },
                _draw_volumeByPrice_Indicators: function(data, indicatorName, graphType, chartIndex, globalGraphClass, indicatorData) {
                    eventManager.bind(eventManager.EventTypes.MouseOutChart, function() {
                        this._VBPDrawObj && this._VBPDrawObj.hideTipElement();
                    }, this);
                    var view = this;
                    var graphConfig = {
                        $container: rootConfig.$componentContainer.find('.chart-body'),
                        data: data,
                        totalVolume: indicatorData.totalVolume,
                        graphCss: innerClass.indicators,
                        yAttributeName: "value",
                        graphType: graphType,
                        downColor: "red",
                        upColor: "green",
                        width: this._validWidth,
                        langData: this.langData,
                        setDrawObj: function(drawObj) {
                            view._VBPDrawObj = drawObj;
                        }
                    };

                    this["_elem_" + graphType] = this._commonDraw(graphConfig);
                },
            };

            return util.extendClass(baseChart, mainChartExtend);
        };

        var innerClass = {
            chartlines: 'mkts-cmpt-svgcht-chart-lines',
            lineChart: "chart-line",
            mountainChart: "chart-mountain",
            postTaxReturnChart: "chart-postTax",
            dotChart: "chart-dot",
            "dotChart-indicators": "dotChart-indicators",
            abChart: "chart-aboveBelow",
            aboveArea: "chart-aboveArea",
            belowArea: "chart-belowArea",
            ohlcChart: "chart-ohlcChart",
            hlcChart: "chart-hlcChart",
            candlestickChart: "chart-candlestick",
            priceGrowth: "chart-priceGrowth",
            pGrowthChart: "chart-tenKGrowthChart",
            performancesGrowth: "chart-performancesGrowth",
            tenKGrowthChart: "chart-tenKGrowthChart",
            dividendEffectChart: "chart-dividendEffectChart",
            dividendEffectCompareChart: "dividendEffectCompareChart",
            gridLines: "mainChart-gridLines",
            chartHeader: "mainChart-header",
            chartBody: "mainChart-body",
            tickerName: "mainChart-tickerName",
            tickerNamePriceChange: "mainChart-tickerName-price-change",
            tickerNameLegend: "mainChart-tickerName-legend",
            tickerPice: "mainChart-tickerPrice",
            tickerCurrency: "mainChart-tickerCurrency",
            mainValue: "mainValue",
            compareValue: "compareValue-",
            indicatorsValue: "indicatorsValue-",
            fundamentalsValue: "fundamentalsValue-",
            zoomDrag: "mainChart-zoomDrag",
            zoomDragMask: "mainChart-zoomDragMask",
            brush: "mainChart-brush",
            mainSvg: "mainChart-mainSVG",
            xAxisSvg: "mainChart-xAxisSvg",
            xAxis: "mainChart-xAxis",
            yAxis: "mainChart-yAxis",
            fundamentals: "fundamentals",
            fundamentalsLegend: "fundamentals-legend",
            fundamentalsLegendText: "fundamentals-legend-text",
            indicators: "indicators",
            indicatorsLegend: "indicators-legend",
            indicatorsLegendText: "indicators-legend-text",
            events: "events",
            eventsText: "events-text",
            compare: "compare",
            compareLegend: "compare-legend",
            compareLegendText: "compare-legend-text",
            comparePriceChangeText: "compare-price-change-text",
            upPriceChangeColor: "upPriceChangeColor",
            downPriceChangeColor: "downPriceChangeColor",
            zeroPriceChangeColor: 'zeroPriceChangeColor',
            dividendEffectLegend: "dividend-effect-legend",
            dividendEffectLegendText: "dividend-effect-legend-text",
            closeButton: "icon chart close-button",
            errorMessage: "error-message",
            errorMessageCloseButton: "icon chart error-message-close-button",
            tagClass: "tag",
            tagTextClass: "tag-text",
            lineLegendClass: "line-legend",
            lineLegendTextClass: "line-legend-text",
            tickerInfoContainer: "mainChart-tickerCon"
        };

        return model;
    });