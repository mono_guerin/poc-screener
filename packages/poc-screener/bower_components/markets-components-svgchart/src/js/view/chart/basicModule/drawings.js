﻿define(['../../../core/core'], function(core) {
    var $ = core.$;
    var d3 = core.d3;

    return function(chartConfigManager) {
        var _id = 0;
        var _last
        var target;
        var width;
        var height;
        var drawType;
        var findByPosition;
        var xScale;
        var yScale;
        var maskElem;

        var labelBox = { height: 20, width: 80, spaceFromLine: 2, leftMargin: 4, topMargin: 4, closeIconMargin: 4, closeIconWidth: 12, closeIconHeight: 12 };
        var drawing = {
            'trendline': {
                'features': {
                    movingDraw: true,
                    dbClickDelete: true,
                    clickReDraw: false,
                    dragReDraw: true
                },
                'count': 0,
                'depot': {},
                'move': {
                    moving: false,
                    cp: [],
                    sp: [],
                    ep: []
                }
            },
            'rectangle': {
                'features': {
                    movingDraw: true, //support move
                    clickRedraw: false, //support draw when click event
                    dragReDraw: true //support drag and draw
                },
                'count': 0,
                'depot': {},
                'move': {
                    moving: false,
                    cp: [],
                    sp: [],
                    ep: []
                }
            },
            'ellipse': {
                'features': {
                    movingDraw: true, //support move
                    clickRedraw: false, //support draw when click event
                    dragReDraw: true //support drag and draw
                },
                'count': 0,
                'depot': {},
                'move': {
                    moving: false,
                    cp: [],
                    sp: [],
                    ep: []
                }
            },
            'levelline': {
                'features': {
                    dbClickDeleteLine: true,
                    movingDraw: true
                },
                'count': 0,
                'depot': {},
                'move': {
                    moving: false,
                    cp: [],
                    sp: [],
                    ep: []
                }
            },
            'fibonacciretracementlevels': {
                'features': {
                    movingDraw: true,
                    clickRedraw: false,
                    dragReDraw: true //support drag and draw
                },
                'count': 0,
                'depot': {},
                'move': {
                    moving: false,
                    cp: [],
                    sp: [],
                    ep: []
                }
            }
        };

        var store = function(dw, id, position, datapoint, dom) {
            var current = {
                'start': {
                    'x': position[0],
                    'y': position[1],
                    'datapoint': datapoint
                },
                'end': {},
                'dom': dom
            };
            dw.depot[id] = current;
            dw.count++;
            dw.currentId = id;
        };

        var storeBeforeDraging = function(current) {
            current.oldStart = $.extend(true, {}, current.start);
            current.oldEnd = $.extend(true, {}, current.end);
        };

        var storeBeforeMoving = function(dw, current, position, datapoint) {
            storeBeforeDraging(current);
            dw.move.moving = true;
            target.style({ 'cursor': 'move' });
            dw.move.cp = [position[0], position[1], datapoint];
            dw.move.sp = [current.start.x, current.start.y, current.start.datapoint];
            dw.move.ep = [current.end.x, current.end.y, current.end.datapoint];
        };

        var mouseOverLine = function(position, sePosition) {
            var spos = sePosition['spos'],
                epos = sePosition['epos'];
            var minx = Math.min(spos[0], epos[0]);
            var maxx = Math.max(spos[0], epos[0]);
            var miny = Math.min(spos[1], epos[1]);
            var maxy = Math.max(spos[1], epos[1]);
            if (!(Math.round(position[0]) > minx - 2 && Math.round(position[0]) < maxx + 2 && Math.round(position[1]) > miny - 2 && Math.round(position[1]) < maxy + 2)) {
                return false;
            }
            k = (epos[1] - spos[1]) / (epos[0] - spos[0]);
            b = spos[1] - k * spos[0];
            var r = k * position[0] + b;
            if (Math.abs(r - position[1]) > 3) {
                return false;
            }
            return true;
        };

        var exchangeStartEnd = function(current, item) {
            if (!item) return;
            var position = item.mPosition;
            if (current.oldStart) {
                if (drawing.selectPoint === 'nw') {
                    current.start = {
                        'x': position[0],
                        'y': position[1],
                        'datapoint': item.datapoint
                    };
                } else if (drawing.selectPoint === 'sw') {
                    current.start = {
                        'x': position[0],
                        'y': current.oldStart.y,
                        'datapoint': item.datapoint
                    };
                    current.end = {
                        'x': current.oldEnd.x,
                        'y': position[1],
                        'datapoint': current.oldEnd.datapoint
                    };
                } else if (drawing.selectPoint === 'ne') {
                    current.start = {
                        'x': current.oldStart.x,
                        'y': position[1],
                        'datapoint': current.oldStart.datapoint
                    };
                    current.end = {
                        'x': position[0],
                        'y': current.oldEnd.y,
                        'datapoint': item.datapoint
                    };
                } else if (drawing.selectPoint === 'n') {
                    var sx = current.oldStart.x,
                        sy = position[1],
                        ex = current.oldEnd.x,
                        ey = current.oldEnd.y + (current.oldStart.y - sy);

                    if (ey > height) {
                        sy -= ey - height;
                        ey = 0;
                    }

                    current.start = {
                        'x': sx,
                        'y': sy,
                        'datapoint': findByPosition([sx, sy]).datapoint
                    };
                    current.end = {
                        'x': ex,
                        'y': ey,
                        'datapoint': findByPosition([ex, ey]).datapoint
                    };
                } else if (drawing.selectPoint === 's') {
                    var ex = current.oldEnd.x,
                        ey = position[1],
                        sx = current.oldStart.x,
                        sy = current.oldStart.y - (ey - current.oldEnd.y);

                    if (sy < 0) {
                        ey -= -sy;
                        sy = 0;
                    }

                    current.start = {
                        'x': sx,
                        'y': sy,
                        'datapoint': findByPosition([sx, sy]).datapoint
                    };
                    current.end = {
                        'x': ex,
                        'y': ey,
                        'datapoint': findByPosition([ex, ey]).datapoint
                    };
                } else if (drawing.selectPoint === 'w') {
                    var sx = position[0],
                        sy = current.oldStart.y,
                        ex = current.oldEnd.x + (current.oldStart.x - sx),
                        ey = current.oldEnd.y;

                    if (ex > width) {
                        sx += ex - width;
                        ex = width;
                    }

                    current.start = {
                        'x': sx,
                        'y': sy,
                        'datapoint': findByPosition([sx, sy]).datapoint
                    };
                    current.end = {
                        'x': ex,
                        'y': ey,
                        'datapoint': findByPosition([ex, ey]).datapoint
                    };
                } else if (drawing.selectPoint === 'e') {
                    var ex = position[0],
                        ey = current.oldEnd.y,
                        sx = current.oldStart.x - (ex - current.oldEnd.x),
                        sy = current.start.y;

                    if (sx < 0) {
                        ex -= -sx;
                        sx = 0;
                    }

                    current.start = {
                        'x': sx,
                        'y': sy,
                        'datapoint': findByPosition([sx, sy]).datapoint
                    };
                    current.end = {
                        'x': ex,
                        'y': ey,
                        'datapoint': findByPosition([ex, ey]).datapoint
                    };
                } else {
                    current.end = {
                        'x': position[0],
                        'y': position[1],
                        'datapoint': item.datapoint
                    };
                }
            } else {
                current.end = {
                    'x': position[0],
                    'y': position[1],
                    'datapoint': item.datapoint
                };
            }
        };

        var verify = function(range, v) {
            if (v < range[0]) {
                v = range[0];
            } else if (v > range[1]) {
                v = range[1];
            }
            return v;
        };

        var calculateStartEnd = function(current, item, move, drawType) {
            if (!item) return;
            var position = item.mPosition;
            var moveX = position[0] - move.cp[0],
                moveY = position[1] - move.cp[1];
            var sx = move.sp[0],
                sy = move.sp[1] + moveY,
                ex = NaN,
                ey = NaN;

            if (drawType !== 'levelline') {
                sx = move.sp[0] + moveX;
                ex = move.ep[0] + moveX;
                ey = move.ep[1] + moveY;
            }

            if (sx < 0) {
                ex += -sx;
                sx = 0;
            }

            if (sy < 0) {
                ey += -sy;
                sy = 0;
            }

            if (ex > width) {
                sx -= ex - width;
                ex = width;
            }

            if (ey > height) {
                sy -= ey - height;
                ey = height;
            }
            //console.log('x=' + sx);
            //console.log('y=' + sy);
            var temp = !isNaN(sx) && !isNaN(sy) ? findByPosition([sx, sy], true) : undefined;
            if (temp) {
                //console.log("start=" + temp.datapoint['date']);
                current.start = {
                    'x': sx,
                    'y': sy,
                    'datapoint': temp.datapoint
                };
            }
            temp = !isNaN(ex) && !isNaN(ey) ? findByPosition([ex, ey]) : undefined;
            if (temp) {
                current.end = {
                    'x': ex,
                    'y': ey,
                    'datapoint': temp.datapoint
                };
            }
        };

        var findSelectPoint = function(drawType, current, position) {
            var t = {
                'spos': [current.start.x, current.start.y],
                'epos': [current.end.x, current.end.y]
            };
            var selectPoint;
            switch (drawType) {
                case 'trendline':
                case 'fibonacciretracementlevels':
                    if (Math.abs(t.spos[0] - position[0]) < 4 && Math.abs(t.spos[1] - position[1]) < 4) {
                        selectPoint = 'nw';
                    } else if (Math.abs(t.epos[0] - position[0]) < 4 && Math.abs(t.epos[1] - position[1]) < 4) {
                        selectPoint = 'se';
                    } else {
                        selectPoint = null;
                    }
                    break;
                case 'rectangle':
                    if (Math.abs(t.spos[0] - position[0]) < 4 && Math.abs(t.spos[1] - position[1]) < 4) {
                        selectPoint = 'nw';
                    } else if (Math.abs(t.epos[0] - position[0]) < 4 && Math.abs(t.epos[1] - position[1]) < 4) {
                        selectPoint = 'se';
                    } else if (Math.abs(t.epos[0] - position[0]) < 4 && Math.abs(t.spos[1] - position[1]) < 4) {
                        selectPoint = 'ne';
                    } else if (Math.abs(t.spos[0] - position[0]) < 4 && Math.abs(t.epos[1] - position[1]) < 4) {
                        selectPoint = 'sw';
                    } else {
                        selectPoint = null;
                    }
                    break;
                case 'ellipse':
                    var rx = Math.abs(t.epos[0] - t.spos[0]) / 2;
                    var ry = Math.abs(t.epos[1] - t.spos[1]) / 2;
                    var cx = rx + Math.min(t.spos[0], t.epos[0]);
                    var cy = ry + Math.min(t.spos[1], t.epos[1]);
                    if (Math.abs(cx - position[0]) < 4 && Math.abs(cy - ry - position[1]) < 4) {
                        selectPoint = 'n';
                    } else if (Math.abs(cx + rx - position[0]) < 4 && Math.abs(cy - position[1]) < 4) {
                        selectPoint = 'e';
                    } else if (Math.abs(cx - position[0]) < 4 && Math.abs(cy + ry - position[1]) < 4) {
                        selectPoint = 's';
                    } else if (Math.abs(cx - rx - position[0]) < 4 && Math.abs(cy - position[1]) < 4) {
                        selectPoint = 'w';
                    } else {
                        selectPoint = null;
                    }
                    break;
            }
            return selectPoint;
        };

        var isChooseDrawings = function(drawType, current, position) {
            var t = {
                'spos': [current.start.x, current.start.y],
                'epos': [current.end.x, current.end.y]
            };
            var isChoose = false;
            switch (drawType) {
                case 'trendline':
                    isChoose = mouseOverLine(position, t);
                    break;
                case 'rectangle':
                case 'ellipse':
                    isChoose = position[0] > (t.spos[0] - 3) && position[0] < (t.epos[0] + 3) && position[1] > (t.spos[1] - 3) && position[1] < (t.epos[1] + 3);
                    break;
                case 'levelline':
                    isChoose = Math.abs(position[1] - t.spos[1]) < 4;
                    break;
                case 'fibonacciretracementlevels':
                    var _height = Math.abs(t.epos[1] - t.spos[1]);
                    var c2 = { spos: [t.spos[0], t.epos[1]], epos: [t.epos[0], t.epos[1]] },
                        c3 = { spos: [t.spos[0], t.spos[1]], epos: [t.epos[0], t.spos[1]] },
                        c4 = { spos: [t.spos[0], t.epos[1] - _height * 0.618], epos: [t.epos[0], t.epos[1] - _height * 0.618] },
                        c5 = { spos: [t.spos[0], t.epos[1] - _height * 0.5], epos: [t.epos[0], t.epos[1] - _height * 0.5] },
                        c6 = { spos: [t.spos[0], t.epos[1] - _height * 0.382], epos: [t.epos[0], t.epos[1] - _height * 0.382] };
                    isChoose = mouseOverLine(position, c2) ||
                        mouseOverLine(position, c3) ||
                        mouseOverLine(position, c4) ||
                        mouseOverLine(position, c5) ||
                        mouseOverLine(position, c6);

                    break;
            }
            return isChoose;
        };

        var isChooseDrawingsForClose = function(drawType, current, position) {
            var t = {
                'spos': [current.start.x, current.start.y],
                'epos': [current.end.x, current.end.y]
            };
            var isChoose = false;
            switch (drawType) {
                case 'trendline':
                    isChoose = (t.epos[0] + labelBox.width - labelBox.closeIconWidth < position[0]) && (position[0] < t.epos[0] + labelBox.width) &&
                        (t.epos[1] - labelBox.height < position[1]) && (position[1] < t.epos[1]);
                    break;
                case 'rectangle':
                    break;
                case 'ellipse':
                    isChoose = position[0] > (t.spos[0] - 3) && position[0] < (t.epos[0] + 3) && position[1] > (t.spos[1] - 3) && position[1] < (t.epos[1] + 3);
                    break;
                case 'levelline':
                    isChoose = (width - labelBox.leftMargin - labelBox.closeIconWidth - labelBox.closeIconMargin < position[0]) &&
                        (position[0] < width - labelBox.leftMargin - labelBox.closeIconMargin) &&
                        (t.spos[1] - labelBox.height / 2 < position[1]) && (position[1] < t.spos[1] + labelBox.height / 2);
                    break;
                case 'fibonacciretracementlevels':
                    isChoose = (t.epos[0] < position[0]) && (position[0] < t.epos[0] + 15) && (t.spos[1] < position[1]) && (position[1] < t.spos[1] + 15);
                    break;
            }
            return isChoose;
        };

        var removeDrawings = function(drawType, drawId) {
            var dw = drawType && drawing[drawType] ? drawing[drawType] : {};
            var current = dw.depot[drawId];
            if (!current) return;
            //remove close buttom and legend dom

            //remove drawing dom
            var drawEl = findDrawingDom(drawType, drawId);
            drawEl && drawEl.remove();
            //remove drawing data
            dw.depot[drawId] = null;
            delete dw.depot[drawId];
            dw.count--;
        };

        var validDrawing = function(drawType, current) {
            var flag = false;
            var diffX = Math.abs(current.end.x - current.start.x),
                deffY = Math.abs(current.end.y - current.start.y),
                minDistance = 3;
            if (drawType === 'trendline') {
                flag = diffX <= minDistance && deffY <= minDistance;
            } else if (drawType === 'fibonaccitimezones') {
                flag = diffX <= minDistance;
            } else if (drawType === 'fibonaccifans' || drawType === 'fibonacciarcs') {
                flag = diffX <= minDistance;
            } else {
                flag = diffX <= minDistance || deffY <= minDistance;
            }
            return flag;
        };

        var genDrawingId = function() {
            return _id++;
        };

        var findDrawingDom = function(drawType, drawId) {
            var array = target.selectAll('.' + drawType);
            array = array.length > 0 ? array[0] : [];
            for (var i = 0, l = array.length; i < l; i++) {
                var dom = d3.select(array[i]);
                if (dom.length > 0 && dom.attr('drawId') == drawId) {
                    return dom;
                }
            }
            return null;
        };

        var drawings = function() {};
        drawings.target = function(value) {
            if (!arguments.length) {
                return target;
            }
            target = value;
            maskElem = target.attr('class', 'drawings').append('rect').attr('class', 'mask');
            return drawings;
        };
        drawings.width = function(value) {
            if (!arguments.length) {
                return width;
            }
            width = value;
            !maskElem.empty() && maskElem.attr('width', width)
            return drawings;
        };
        drawings.height = function(value) {
            if (!arguments.length) {
                return height;
            }
            height = value;
            !maskElem.empty() && maskElem.attr('height', height);
            return drawings;
        };
        drawings.drawType = function(value) {
            if (!arguments.length) {
                return drawType;
            }
            drawType = value;
            return drawings;
        };
        drawings.findByPosition = function(value) {
            if (!arguments.length) {
                return findByPosition;
            }
            findByPosition = value;
            return drawings;
        };
        drawings.xScale = function(value) {
            if (!arguments.length) {
                return xScale;
            }
            xScale = value;
            return drawings;
        };
        drawings.yScale = function(value) {
            if (!arguments.length) {
                return yScale;
            }
            yScale = value;
            return drawings;
        };
        drawings.isEnter = function(position) {
            var isChoose = false;
            var temp;
            for (var t in drawing) {
                var dw = drawing[t];
                if (!dw || !dw.features) continue;
                if (!isChoose && (dw.features.movingDraw || dw.features.dragReDraw)) {
                    for (var id in dw.depot) {
                        if (!dw.depot.hasOwnProperty(id)) continue;
                        temp = dw.depot[id];
                        isChoose = isChooseDrawings(t, temp, position);
                        if (isChoose) break;
                    }
                }
                if (isChoose) break;
            }
            return isChoose;
        };
        drawings.isCloseButton = function(position) {
            var isChoose = false;
            var temp;
            for (var t in drawing) {
                var dw = drawing[t];
                if (!dw || !dw.features) continue;
                if (!isChoose && (dw.features.movingDraw || dw.features.dragReDraw)) {
                    for (var id in dw.depot) {
                        if (!dw.depot.hasOwnProperty(id)) continue;
                        temp = dw.depot[id];
                        isChoose = isChooseDrawingsForClose(t, temp, position);
                        if (isChoose) break;
                    }
                }
                if (isChoose) break;
            }
            return isChoose;
        };
        drawings.start = function(item, callback) {
            var position = item.mPosition;
            var datapoint = item.datapoint;
            var decimal = chartConfigManager.getConfig().charts.mainChart.drawingsDecimal;
            var closeImage = chartConfigManager.getConfig().StaticFilesPath + "morningstar/images/close-button.svg";
            var dw = drawType ? drawing[drawType] : null;
            if (dw) {
                var _id = genDrawingId();
                store(dw, _id, position, datapoint);
                switch (drawType) {
                    case 'trendline':
                        var g = target.append('g').attr('class', 'trendline').attr('drawId', _id);
                        g.append('line').attr('class', 'tl-line');
                        g.append('rect').attr('class', 'tl-text-rect').attr('fill', '#dedede').attr('stroke', '#dedede');
                        g.append('text').attr('class', 'tl-text');
                        g.append("image")
                            .attr('class', 'close-trendline')
                            .attr({
                                "width": 12,
                                "height": 12,
                                "xlink:href": function(d, i) {
                                    return closeImage;
                                }
                            }).on('mousedown.drawings', function() {
                                d3.event.stopPropagation();
                            }).on('click.drawings', function() {
                                removeDrawings('trendline', _id);
                            });
                        break;
                    case 'rectangle':
                        var g = target.append('g').attr('class', 'rectangle').attr('drawId', _id);
                        g.append('rect').attr('class', 'r-rectangle');
                        g.append("image")
                            .attr('class', 'close-rectangle')
                            .attr({
                                "width": 12,
                                "height": 12,
                                "xlink:href": function(d, i) {
                                    return closeImage;
                                }
                            }).on('mousedown.drawings', function() {
                                d3.event.stopPropagation();
                            }).on('click.drawings', function() {
                                removeDrawings('rectangle', _id);
                            });
                        break;
                    case 'ellipse':
                        var g = target.append('g').attr('class', 'ellipse').attr('drawId', _id);
                        g.append('ellipse').attr('class', 'e-ellipse');
                        g.append("image")
                            .attr('class', 'close-ellispe')
                            .attr({
                                "width": 12,
                                "height": 12,
                                "xlink:href": function(d, i) {
                                    return closeImage;
                                }
                            }).on('mousedown.drawings', function() {
                                d3.event.stopPropagation();
                            }).on('click.drawings', function() {
                                removeDrawings('ellipse', _id);
                            });
                        break;
                    case 'levelline':
                        var start = dw.depot[_id].start;
                        var g = target.append('g').attr('class', 'levelline').attr('drawId', _id);
                        start.yValue = yScale.invert(start.y);
                        g.append('line').attr('class', 'll-line')
                            .attr('x1', 0)
                            .attr('y1', start.y)
                            .attr('x2', width)
                            .attr('y2', start.y);
                        g.append('rect').attr('class', 'll-text-rect').attr('fill', '#dedede').attr('stroke', '#dedede')
                            .attr({
                                'x': width - labelBox.width - labelBox.leftMargin,
                                'y': start.y - labelBox.height / 2,
                                'width': labelBox.width,
                                'height': labelBox.height
                            });

                        g.append('text').attr('class', 'll-text')
                            .attr({
                                'x': width - labelBox.width + labelBox.leftMargin,
                                'y': start.y + labelBox.height / 2 - labelBox.topMargin
                            }).text(start.yValue.toFixed(decimal));

                        g.append("image")
                            .attr('class', 'close-levelline')
                            .attr({
                                'x': width - labelBox.leftMargin - labelBox.closeIconWidth - labelBox.closeIconMargin,
                                'y': start.y - labelBox.height / 2 + labelBox.closeIconMargin
                            })
                            .attr({
                                "width": 12,
                                "height": 12,
                                "xlink:href": function(d, i) {
                                    return closeImage;
                                }
                            }).on('mousedown.drawings', function() {
                                d3.event.stopPropagation();
                            }).on('click.drawings', function() {
                                removeDrawings('levelline', _id);
                            });

                        break;
                    case 'fibonacciretracementlevels':
                        var g = target.append('g').attr('class', 'fibonacciretracementlevels').attr('drawId', _id);
                        g.append('line').attr('class', 'l-100').attr("stroke-width", 2);
                        g.append('circle').attr('class', 'l-100-tip');
                        g.append('text').attr('class', 'l-100-left-text').text('100.00%');
                        g.append('text').attr('class', 'l-100-right-text');
                        g.append('line').attr('class', 'l-618');
                        g.append('text').attr('class', 'l-618-left-text').text('61.80%');
                        g.append('text').attr('class', 'l-618-right-text');
                        g.append('line').attr('class', 'l-50');
                        g.append('text').attr('class', 'l-50-left-text').text('50.00%');
                        g.append('text').attr('class', 'l-50-right-text');
                        g.append('line').attr('class', 'l-382');
                        g.append('line').attr('class', 'l-0').attr("stroke-width", 2);
                        //g.append('line').attr('class', 'l-se');
                        g.append('text').attr('class', 'l-382-left-text').text('38.20%');
                        g.append('text').attr('class', 'l-382-right-text');
                        g.append('text').attr('class', 'l-0-left-text').text('0.00%');
                        g.append('text').attr('class', 'l-0-right-text');
                        g.append('circle').attr('class', 'l-0-tip');
                        g.append('line').attr('class', 'l-100-0');

                        g.append("image")
                            .attr('class', 'close-drawing')
                            .attr({
                                "width": 12,
                                "height": 12,
                                "xlink:href": function(d, i) {
                                    return closeImage;
                                }
                            }).on('mousedown.drawings', function() {
                                d3.event.stopPropagation();
                            }).on('click.drawings', function() {
                                removeDrawings('fibonacciretracementlevels', _id);
                            });
                        break;
                }

            } else {
                var isChoose = false;
                var selectPoint;
                var temp;
                //trendline
                for (var t in drawing) {
                    var dw = drawing[t];
                    if (!dw || !dw.features) continue;
                    if (!isChoose && (dw.features.movingDraw || dw.features.dragReDraw)) {
                        for (var id in dw.depot) {
                            if (!dw.depot.hasOwnProperty(id)) continue;
                            temp = dw.depot[id];
                            selectPoint = findSelectPoint(t, temp, position);
                            if (dw.features.dragReDraw && selectPoint) { //drag the start or end point
                                target.style({
                                    'cursor': selectPoint + '-resize'
                                });
                                storeBeforeDraging(temp);
                                isChoose = true;
                            } else { //move 
                                isChoose = isChooseDrawings(t, temp, position);
                                if (isChoose) {
                                    storeBeforeMoving(dw, temp, position, datapoint);
                                }
                            }
                            if (isChoose) {
                                drawType = t;
                                dw.currentId = id;
                                break;
                            }
                        }
                    }
                    if (isChoose) {
                        if (typeof callback === 'function') {
                            callback(drawType);
                        }
                        break;
                    }
                }

                drawing.selectPoint = selectPoint;
            }
        };
        drawings.move = function(item, decimals) {
            var dw = drawType ? drawing[drawType] : null;
            if (dw) {
                var _id = dw.currentId;
                var _drawEle = findDrawingDom(drawType, _id);
                if (!_drawEle) return;
                var end;
                var current = dw.depot[_id];
                var move = dw.move;
                if (move.moving) {
                    calculateStartEnd(current, item, move, drawType);
                } else {
                    exchangeStartEnd(current, item);
                }
                current.start.yValue = yScale.invert(current.start.y);
                current.end.yValue = yScale.invert(current.end.y);
                var start = current.start;
                var end = current.end;
                switch (drawType) {
                    case 'trendline':
                        _drawEle.select('.tl-line').attr('x1', start.x)
                            .attr('y1', start.y)
                            .attr('x2', end.x)
                            .attr('y2', end.y);
                        _drawEle.select('.tl-text').attr({
                            'x': end.x + labelBox.leftMargin + labelBox.spaceFromLine,
                            'y': end.y - labelBox.leftMargin
                        }).text((((current.end.yValue - current.start.yValue) / current.start.yValue) * 100).toFixed(2) + '%');
                        _drawEle.select('.tl-text-rect').attr({
                            'x': end.x + labelBox.spaceFromLine,
                            'y': end.y - labelBox.height,
                            'width': labelBox.width,
                            'height': labelBox.height
                        });
                        _drawEle.select('.close-trendline').attr({
                            'x': end.x + labelBox.width - (labelBox.closeIconWidth + labelBox.closeIconMargin),
                            'y': end.y - (labelBox.height - labelBox.closeIconMargin)
                        });
                        break;
                    case 'rectangle':
                        var x = Math.min(end.x, start.x);
                        var y = Math.min(end.y, start.y);
                        var _width = Math.abs(end.x - start.x);
                        var _height = Math.abs(end.y - start.y);
                        _drawEle.select('.r-rectangle').attr('x', x)
                            .attr('y', y)
                            .attr('width', _width)
                            .attr('height', _height)
                        _drawEle.select('.close-rectangle').attr({
                            'x': x + _width - labelBox.closeIconWidth,
                            'y': y
                        });
                        break;
                    case 'ellipse':
                        var rx = Math.abs(end.x - start.x) / 2;
                        var ry = Math.abs(end.y - start.y) / 2;
                        var cx = rx + Math.min(end.x, start.x);
                        var cy = ry + Math.min(end.y, start.y);
                        _drawEle.select('.e-ellipse').attr('cx', cx)
                            .attr('cy', cy)
                            .attr('rx', rx)
                            .attr('ry', ry);
                        _drawEle.select('.close-ellispe').attr({
                            'x': cx + rx - labelBox.closeIconWidth,
                            'y': cy - labelBox.closeIconHeight / 2
                        });
                        break;
                    case 'levelline':
                        _drawEle.select('.ll-line').attr('x1', 0)
                            .attr('y1', start.y)
                            .attr('x2', width)
                            .attr('y2', start.y);
                        _drawEle.select('.ll-text-rect').attr({
                            'x': width - labelBox.width - labelBox.leftMargin,
                            'y': start.y - labelBox.height / 2,
                            'width': labelBox.width,
                            'height': labelBox.height
                        });
                        _drawEle.select('.ll-text').attr({
                            'x': width - labelBox.width + labelBox.leftMargin,
                            'y': start.y + labelBox.height / 2 - labelBox.topMargin
                        }).text(start.yValue.toFixed(decimals));

                        _drawEle.select('.close-levelline').attr({
                            'x': width - labelBox.leftMargin - labelBox.closeIconWidth - labelBox.closeIconMargin,
                            'y': start.y - labelBox.height / 2 + labelBox.closeIconMargin
                        });
                        break;
                    case 'fibonacciretracementlevels':
                        var _height = Math.abs(end.y - start.y);
                        var x1 = Math.min(end.x, start.x);
                        var y1 = Math.min(end.y, start.y);
                        var x2 = Math.max(end.x, start.x);
                        var y2 = Math.max(end.y, start.y);
                        /*_drawEle.select('.l-se').attr({
                            'x1': x1,
                            'y1': y1,
                            'x2': x2,
                            'y2': y2
                        });*/
                        _drawEle.select('.l-0').attr({
                            'x1': x1,
                            'y1': y2,
                            'x2': x2,
                            'y2': y2
                        });
                        _drawEle.select('.l-0-tip').attr({
                            'cx': x2,
                            'cy': y2,
                            'r': 2
                        });
                        _drawEle.select('.l-0-left-text').attr({
                            'x': x1,
                            'y': y2 - 10
                        });
                        _drawEle.select('.l-0-right-text').attr({
                            'x': x2 - 45,
                            'y': y2 - 10
                        }).text(current.end.yValue.toFixed(decimals));

                        _drawEle.select('.l-100-left-text').attr({
                            'x': x1,
                            'y': y1 + 20
                        });
                        _drawEle.select('.l-100-tip').attr({
                            'cx': x1,
                            'cy': y1,
                            'r': 2
                        });
                        _drawEle.select('.l-100-right-text').attr({
                            'x': x2 - 45,
                            'y': y1 + 20
                        }).text(current.start.yValue.toFixed(decimals));
                        _drawEle.select('.l-100').attr({
                            'x1': x1,
                            'y1': y1,
                            'x2': x2,
                            'y2': y1
                        });
                        _drawEle.select('.l-100-0').attr({
                            'x1': x1,
                            'y1': y1,
                            'x2': x2,
                            'y2': y2
                        });


                        _drawEle.select('.l-618').attr({
                            'x1': x1,
                            'y1': y2 - _height * 0.618,
                            'x2': x2,
                            'y2': y2 - _height * 0.618
                        });
                        _drawEle.select('.l-618-left-text').attr({
                            'x': x1,
                            'y': y2 - _height * 0.618 + 20
                        });
                        _drawEle.select('.l-618-right-text').attr({
                            'x': x2 - 45,
                            'y': y2 - _height * 0.618 + 20
                        }).text((current.end.yValue - (current.end.yValue - current.start.yValue) * 0.618).toFixed(decimals));


                        _drawEle.select('.l-50').attr({
                            'x1': x1,
                            'y1': y2 - _height * 0.5,
                            'x2': x2,
                            'y2': y2 - _height * 0.5
                        });
                        _drawEle.select('.l-50-left-text').attr({
                            'x': x1,
                            'y': y2 - _height * 0.5 + 20
                        });
                        _drawEle.select('.l-50-right-text').attr({
                            'x': x2 - 45,
                            'y': y2 - _height * 0.5 + 20
                        }).text((current.end.yValue - (current.end.yValue - current.start.yValue) * 0.5).toFixed(decimals));

                        _drawEle.select('.l-382').attr({
                            'x1': x1,
                            'y1': y2 - _height * 0.382,
                            'x2': x2,
                            'y2': y2 - _height * 0.382
                        });
                        _drawEle.select('.l-382-left-text').attr({
                            'x': x1,
                            'y': y2 - _height * 0.382 + 20
                        });
                        _drawEle.select('.l-382-right-text').attr({
                            'x': x2 - 45,
                            'y': y2 - _height * 0.382 + 20
                        }).text((current.end.yValue - (current.end.yValue - current.start.yValue) * 0.382).toFixed(decimals));

                        _drawEle.select('.close-drawing').attr({
                            'x': x2 + 3,
                            'y': y1
                        });

                        break;
                }
            }
        };
        drawings.end = function(dataPointSize) {
            var dw = drawType ? drawing[drawType] : null;
            if (dw) {
                var id = dw.currentId;
                var current = dw.depot[id];
                if (current.oldStart) {
                    current.oldStart = null;
                    delete current.oldStart;
                }
                if (current.oldEnd) {
                    current.oldEnd = null;
                    delete current.oldEnd;
                }
                dw.move = {
                    moving: false,
                    cp: [],
                    sp: [],
                    ep: []
                };
                if (dataPointSize < 2 && drawType !== 'levelline') { //when data point is less than 2, we don't draw any except levelline
                    removeDrawings(drawType, id);
                } else {
                    if (typeof current.end === 'undefined' || !current.end.x) {
                        if (!dw.features.clickRedraw && drawType !== 'levelline') {
                            removeDrawings(drawType, id);
                        }
                    } else if (drawType !== 'levelline' && validDrawing(drawType, current)) {
                        removeDrawings(drawType, id);
                    } else {
                        if (drawType !== 'trendline' && (current.start.x > current.end.x || current.start.y > current.end.y)) {
                            var start = $.extend(true, {}, current.start),
                                end = $.extend(true, {}, current.end);
                            current.start = {
                                'x': Math.min(start.x, end.x),
                                'y': Math.min(start.y, end.y),
                                'yValue': Math.max(start.yValue, end.yValue),
                                'datapoint': start.x > end.x ? end.datapoint : start.datapoint
                            };
                            current.end = {
                                'x': Math.max(start.x, end.x),
                                'y': Math.max(start.y, end.y),
                                'yValue': Math.min(start.yValue, end.yValue),
                                'datapoint': start.x > end.x ? start.datapoint : end.datapoint
                            };
                        }
                    }
                }
                dw.currentId = null;
                delete dw.currentId;
            }
            drawType = null;
            drawing.selectPoint = null;
            delete drawing.selectPoint;
            target.style({ 'cursor': 'default' });
        };
        drawings.redraw = function(dataList) {
            drawing.selectPoint = null;
            var _map = {};
            for (var i = 0, l = dataList.length, d; i < l; i++) {
                d = dataList[i]['date'].getTime();
                _map[d] = i;
            }
            for (var t in drawing) {
                var dw = drawing[t];
                if (!dw || dw.count <= 0) continue;
                drawType = t;
                for (var id in dw.depot) {
                    var drawEl = findDrawingDom(drawType, id);
                    var current = dw.depot[id];
                    dw.currentId = id;
                    var startY = yScale(current.start.yValue);
                    if (startY >= 0 && startY <= height) {
                        if (drawType !== 'levelline') {
                            var startIndex = _map[current.start.datapoint['date'].getTime()],
                                endIndex = _map[current.end.datapoint['date'].getTime()],
                                endY = yScale(current.end.yValue);
                            if (startIndex > -1 && endIndex > -1 && endY >= 0 && endY <= height) {
                                drawEl.style({ 'display': '' });
                                var sitem = findByPosition([xScale(startIndex), startY]),
                                    eitem = findByPosition([xScale(endIndex), endY]),
                                    sposition = sitem.mPosition;
                                current.start['x'] = sposition[0];
                                current.start['y'] = sposition[1];
                                this.move(eitem);
                            } else {
                                drawEl.style({ 'display': 'none' });
                            }
                        } else {
                            drawEl.style({ 'display': '' });
                            current.start['y'] = startY;
                            this.move({
                                mPosition: [0, startY]
                            });
                        }

                    } else {
                        drawEl.style({ 'display': 'none' });
                    }
                }
            }
            drawType = null;
        };
        return drawings;
    };
});