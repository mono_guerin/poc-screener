define(["./basicModule/commonPlotter", "../../manager/chartConfig/graphTypes", "../../core/core", "./baseChart", "../../util/util", "moment"], function(commonPlotter, graphTypes, core, baseChartModel, util, moment) {
    "use strict";
    var $ = core.$;
    var d3 = core.d3;

    /**
     * Slider chart Model
     *
     */

    var model = function(managers) {
        var eventManager = managers.eventManager;
        var chartConfigManager = managers.chartConfigManager;
        var baseChart = baseChartModel(managers);
        var dataAdapter = managers.dataAdapter;

        var sliderChartExtend = {
            _init: function(config) {
                this._innerClass = innerClass;
                this._bindEvents();
                this.langData = chartConfigManager.getLabels('markets-components-svgchart', config.lang);
                this._renderDomFrame();
                this._decideMinMaxAttrbute();
                this._getData();
            },
            _bindEvents: function() {
                var eventTypes = eventManager.EventTypes
                eventManager.bind(eventTypes.UpdateConfig, this._updateConfig, this);
                eventManager.bind(eventTypes.ChangeIntervalFrequency, this._chartConfigChanged, this);
                eventManager.bind(eventTypes.LayoutChange, this._changeLayout, this);
                eventManager.bind(eventTypes.ChangeYAxis, this._chartConfigChanged, this);
                eventManager.bind([eventTypes.ChangeSliderDateRange, eventTypes.ChangeDateRangeByZoom], this._changeSelectedDateRange, this);
                eventManager.bind(eventTypes.ChangeIntervalFrequency, this._getData, this);
                eventManager.bind(eventTypes.ChangeChartType, this._decideMinMaxAttrbute, this);
            },
            _updateConfig: function() {
                this.config = chartConfigManager.getChartConfig(chartConfigManager.ChartTypes.SliderChart);
            },
            _getData: function(extendDateRange) {
                var view = this;
                var cfg = this.config;
                view._showLoading();
                view._loading = true;
                view._hideDataNotAvailable();
                view._decideMinMaxAttrbute();
                chartConfigManager.fetchSliderChartData(extendDateRange ? this._preloadInfo : null,
                    function(res, preloadInfo) {
                        view._clearView();
                        view._data = null;
                        if (res.status().errorCode === "0") {
                            view._preloadInfo = preloadInfo;
                            //view._dynamicValues = {};
                            var data = res.data()[0].Price.data;
                            if (data.length > 0) {
                                data = view._dataInterpolation(data, preloadInfo);
                                data.forEach(function(d) {
                                    d.x = d.index;
                                    d.y = d.close;
                                });
                                view._data = data;
                                view._renderView();
                            }
                        }

                        if (!view._data || view._data.length === 0) {
                            view._showDataNotAvailable();
                        }
                        view._hideLoading();
                        view._loading = false;
                    });
            },
            _chartConfigChanged: function() {
                ////TODO: Check if just need change brush status.
            },
            _clearView: function(chartType) {
                this._outerGElem && this._outerGElem.selectAll("*").remove();
            },
            _dataInterpolation: function(data, preloadInfo) {
                var frequencyTypes = chartConfigManager.FrequencyTypes;
                var lastDataItem = data[data.length - 1];
                var appendedData = $.extend(true, {}, lastDataItem);
                var today = new Date();
                var y = today.getFullYear(),
                    m = today.getMonth(),
                    d = today.getDate();
                appendedData.date = new Date(y, m, d + 1);
                appendedData.index += 1;
                data.push(appendedData);
                return data;
            },
            _createScale: function() {
                var yAttr = this.yAttr;
                this._xScale = this._xScale = d3.scale.linear().range([0, this._validWidth]).domain([0, this._data.length - 1]);

                var minYValue = d3.min(this._data, function(d) { return d[yAttr]; });
                var maxYValue = d3.max(this._data, function(d) { return d[yAttr]; });
                var domainScaleup = this.config.domainScaleup
                var scaleUpValue = (maxYValue - minYValue) * domainScaleup;
                var yScaleDomain = [minYValue - scaleUpValue, maxYValue + scaleUpValue];

                this._yScale = d3.scale.linear().range([this._validHeight, 0])
                    .domain(yScaleDomain);
            },
            _changeLayout: function() {
                d3.select(this.config.container).selectAll("div").remove();
                this._renderDomFrame();
                if (this._data && (this._data.length > 0)) {
                    this._renderView();
                } else {
                    this._showDataNotAvailable();
                }
            },
            _renderDomFrame: function() {
                this._caculateValidSize();
                this._createSvgWrapper();
                //eventManager.trigger(eventManager.EventTypes.HeightChanged, this);
            },
            _renderView: function() {
                this._createScale();
                this._decimalPlaces = this._getDecimalPlaces();
                this._createCommonPlotter();
                this._drawXAxis();
                this._drawYAxis();
                this._drawChart();
                this._createChartMask();
                this._createBrush();
                this._initBrushExtent();
                //when mouse in the slider chart, we will hide the crossharis
                d3.select(this.config.container).on('mouseover.sliderchart', function() {
                    eventManager.trigger(eventManager.EventTypes.ToggleCrossharis);
                });
            },
            _setXAxisStyle: function(selection) {
                selection.selectAll("text").attr("y", "6").attr("x", "1");
            },
            _createCommonPlotter: function() {
                var graphConfig = this._defaultGraphConfig = {
                    containerGElem: this._outerGElem,
                    data: this._data,
                    xScale: this._xScale,
                    yScale: this._yScale,
                    yAttributeName: this.yAttr
                };
                this._getDefaultGraphConfig = function() {
                    this._defaultGraphConfig.data = this._data;
                    return this._defaultGraphConfig;
                };
                this._commonPlotter = new commonPlotter(graphConfig);
            },
            _drawChart: function(chartType) {
                var cfg = this.config;
                var lineConfig = {
                    graphType: graphTypes.Line,
                    graphCss: innerClass.lineChart
                };

                this._commonDraw(lineConfig);

                var areaConfig = {
                    graphType: graphTypes.Area,
                    graphCss: innerClass.mountainChart,
                    y0Value: this._validHeight,
                };

                this._commonDraw(areaConfig);
            },
            _createChartMask: function() {
                var brushMaskAttrValue = {
                    "width": this._validWidth,
                    "class": innerClass.brushMask,
                    "height": this._validHeight
                };
                this._brushMaskLeftElem = this._outerGElem.append("rect").attr(brushMaskAttrValue);
                this._brushMaskRightElem = this._outerGElem.append("rect").attr(brushMaskAttrValue).attr("x", this._validWidth);
            },
            _createBrush: function() {
                var cfg = this.config;
                var view = this;

                var config = {
                    graphType: graphTypes.Brush,
                    graphCss: innerClass.brush,
                    yValue: 0,
                    height: this._validHeight, //this._validHeight + 7,
                    brushImages: cfg.brushImages,
                    brushImagesXAdjust: -11,
                    brushImagesWidth: 22,
                    brushResponseFunc: function() {
                        view._brushResponse.apply(view, arguments);
                    },
                    brushEndResponseFunc: function() {
                        view._brushEndResponse.apply(view, arguments);
                    },
                    setExtentFunc: function(callback) { view._changeBrushExtent = callback; }
                };

                this._commonDraw(config);
            },
            //Will be overwrite by brush function
            _changeBrushExtent: function() {},
            _changeSelectedDateRange: function(dateRange) {
                if ($.isArray(this._data) && dateRange && dateRange.length === 2) {
                    var startDate = dateRange[0];
                    var endDate = dateRange[1];
                    if (startDate < this._data[0].date && !this._loading) {
                        this._getData();
                    } else {
                        startDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());
                        endDate = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate() + 1);
                        var startIndex = this._getDataIndexByDate(startDate);
                        var endIndex = this._getDataIndexByDate(endDate);
                        startIndex = Math.floor(startIndex);
                        endIndex = Math.ceil(endIndex);
                        this._setBrushExtent(startIndex, endIndex);
                    }
                }
            },
            _setBrushExtent: function(startIndex, endIndex) {
                this._changeBrushExtent([startIndex, endIndex]);
                this._brushResponse(null, null, [startIndex, endIndex]);
                this._checkIfNeedLoadMoreData(startIndex);
            },
            _checkIfNeedLoadMoreData: function(startIndex) {
                if (startIndex === 0 &&
                    !this._loading &&
                    (this._preloadInfo.intervalType !== chartConfigManager.IntervalTypes.Max)) {
                    this._getData(true); //get more data.
                }
            },
            _initBrushExtent: function() {
                var dateRange = this.config.dateRange;
                if (!dateRange || dateRange.length !== 2) {
                    eventManager.trigger(eventManager.EventTypes.GetMainChartDateRange);
                } else {
                    this._changeSelectedDateRange(dateRange);
                }
            },
            _brushResponse: function(brushFunc, eventTarget, brushDomain) {
                if (brushFunc && eventTarget) {
                    var s = d3.event.target.extent();
                    var miniRange = 0.5;
                    if (s[1] - s[0] <= miniRange) {
                        var extentValue;
                        if (s[1] > (this._data.length / 2)) {
                            extentValue = [s[1] - miniRange, s[1]];
                        } else {
                            extentValue = [s[0], s[0] + miniRange];
                        }
                        d3.event.target.extent(extentValue);
                        d3.event.target(d3.select(eventTarget));
                        brushDomain = extentValue;
                    }
                }

                var brushExtentIsEmpty;
                if (!brushDomain && brushFunc) {
                    brushExtentIsEmpty = brushFunc.empty();
                    brushDomain = brushExtentIsEmpty ? this._xScale.domain() : brushFunc.extent();
                } else {
                    brushExtentIsEmpty = false;
                }

                if (brushDomain) {
                    var startIndex = Math.floor(brushDomain[0]);
                    var startDate = this._data[startIndex].date;
                    var endIndex = Math.ceil(brushDomain[1]);
                    var endDate, now = new Date();
                    if (endIndex >= this._data.length) {
                        endDate = now;
                    } else {
                        endDate = this._data[endIndex].date;
                    }

                    if (endDate > now) {
                        endDate = now;
                    }
                    brushFunc && eventManager.trigger(eventManager.EventTypes.UpdateMenuInterval, [startDate, endDate]);
                }

                var view = this;
                this._brushMaskLeftElem.attr("x", function() {
                    return brushExtentIsEmpty ? 0 : (view._xScale(brushDomain[0]) - view._validWidth);
                });
                this._brushMaskRightElem.attr("x", function() {
                    return brushExtentIsEmpty ? view._validWidth : view._xScale(brushDomain[1]);
                });
            },
            _brushEndResponse: function(brushFunc) {
                var brushExtentIsEmpty = brushFunc.empty();
                var brushDomain = brushExtentIsEmpty ? this._xScale.domain() : brushFunc.extent();
                var startIndex = Math.floor(brushDomain[0]);
                var startDate = this._data[startIndex].date;
                var endIndex = Math.ceil(brushDomain[1]);
                var endDate, now = new Date();
                if (endIndex >= this._data.length) {
                    endDate = now;
                } else {
                    endDate = this._data[endIndex].date;
                }

                if (endDate > now) {
                    endDate = now;
                }

                var dateRange = [startDate, endDate];
                var rootConfig = chartConfigManager.getConfig();
                eventManager.trigger(eventManager.EventTypes.RequestFrequencyFromMenu, dateRange, true);
                this._checkIfNeedLoadMoreData(startIndex);
                eventManager.trigger(eventManager.EventTypes.onDateRangeChange, {
                    name: 'sliderChartRange',
                    value: {
                        startDate: moment(startDate).format(rootConfig.intradayDateFormater),
                        endDate: moment(endDate).format(rootConfig.intradayDateFormater)
                    }
                });
                //eventManager.trigger(eventManager.EventTypes.UpdateMenuInterval, [startDate, endDate]);
            },
            _showDataNotAvailable: function() {
                if (!this._svg) {
                    return;
                }
                if (this._outerGElem) {
                    this._outerGElem.selectAll("g").remove();
                }
                if (this.config && this.config.container) {
                    $(this.config.container).hide();
                }
            },

            _hideDataNotAvailable: function() {
                $(this.config.container).show();
            }

        };

        return util.extendClass(baseChart, sliderChartExtend);
    }
    var innerClass = {
        xAxis: "sliderChart-xAxis",
        lineChart: "sliderChart-line",
        mountainChart: "sliderChart-mountain",
        brush: "sliderChart-brush",
        brushMask: "sliderChart-brushMask",
        chartBody: 'sliderChart-body',
    };

    return model;
});