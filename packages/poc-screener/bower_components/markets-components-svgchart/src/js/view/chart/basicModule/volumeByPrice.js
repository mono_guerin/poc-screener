define(["../../../core/core"], function (core) {
    "use strict";
    function barSeries() {
        var $ = core.$;
        var d3 = core.d3;
        var xScale, yScale;
        var margeValue = 1;
        var width, volumeByPriceScale = 0.4;
        var totalVolume;
        var xValueFunc, yValueFunc, heightFunc;
        var yAttributeName = "volume";
        var dataIndex = 0;
        var downColor, upColor;
        var data, $tipElement, $tipContent, tipHeight = 20;
        var langData, $container;

        var drawRectangle = function (barGraph) {
            var rect = barGraph.append('rect')
                .classed("up", true)
                .attr("x", 0)
                .attr("y", function (d) {
                    return yScale(d.range[1]) - margeValue;
                })
                .attr("width", function (d) {
                    return xScale(d.upVolume);
                })
                .attr("height", function (d) {
                    return yScale(d.range[0]) - yScale(d.range[1]) - 2 * margeValue;
                });

            var rect = barGraph.append('rect')
                .classed("down", true)
                .attr("x", function (d) {
                    return xScale(d.upVolume);
                })
                .attr("y", function (d) {
                    return yScale(d.range[1]) - margeValue;
                })
                .attr("width", function (d) {
                    return xScale(d.downVolume);
                })
                .attr("height", function (d) {
                    return yScale(d.range[0]) - yScale(d.range[1]) - 2 * margeValue;
                });
        };

        var createTipElement = function () {
            $tipElement = $container.find(".chart-vbp-tip");
            if ($tipElement.length === 0) {
                $tipElement = $('<div class="chart-vbp-tip"><div class="chart-vbp-content"></div></div>').appendTo($container);
            }

            $tipContent = $tipElement.find(".chart-vbp-content").css("height", tipHeight);
        }

        var displayTip = function (dataItem, offsetTop, offsetLeft) {
            $tipElement || createTipElement();
            var priceRange = dataItem.range
            var tipViticalCenter = yScale((priceRange[1] - priceRange[0]) / 2 + priceRange[0]);
            $tipElement.css("top", (tipViticalCenter - tipHeight / 2 + offsetTop));
            var totalVolume = dataItem.totalVolume;
            $tipElement.css("left", (xScale(totalVolume) + 2 + offsetLeft));


            $tipContent.text(langData['volume'] + ": " + totalVolume + " | " + langData['range'] + ": " + priceRange[0].toFixed(2) + " - " + priceRange[1].toFixed(2));
            $tipElement.show();
        }

        var bar = function (selection) {
            data = selection.datum();
            xScale = d3.scale.linear()
                    .range([0, width * volumeByPriceScale])
                    .domain([0, totalVolume]);

            var barGraph;
            selection.each(function (dataArr) {
                var outerElem = d3.select(this).append("g").classed("bar-series", true);

                dataArr.forEach(function (dataItem) {
                    barGraph = outerElem.append("g").data([dataItem])
                        .classed("bar", true);

                    drawRectangle(barGraph);
                });
            });

            downColor && selection.selectAll("rect.down").style({ 'fill': downColor });
            upColor && selection.selectAll("rect.up").style({ 'fill': upColor });
        };

        bar.hideTipElement = function () {
            $tipElement && $tipElement.hide();
        };

        bar.tipProcess = function (position, offsetTop, offsetLeft) {
            offsetLeft = offsetLeft || 0;
            var x = position[0] - (offsetLeft || 0 ) ;
            if (x > (width * volumeByPriceScale + 3)) {
                bar.hideTipElement();
                return;
            }

            var mousePrice = yScale.invert(position[1]);
            var dataItem, priceRange;
            for (var i = 0, len = data.length; i < len; i++) {
                priceRange = data[i].range;
                if (mousePrice > priceRange[0] && mousePrice < priceRange[1]) {
                    dataItem = data[i];
                    break;
                }
            }

            if (dataItem && (xScale.invert(x) < dataItem.totalVolume)) {
                displayTip(dataItem, offsetTop, offsetLeft);
            }
            else {
                bar.hideTipElement();
            }
        };

        bar.yScale = function (value) {
            if (!arguments.length) {
                return yScale;
            }
            yScale = value;
            return bar;
        };

        bar.width = function (value) {
            if (!arguments.length) {
                return width;
            }
            width = value;
            return bar;
        };

        bar.height = function (value) {
            return bar;
        };

        bar.downColor = function (value) {
            if (!arguments.length) {
                return downColor;
            }
            downColor = value;
            return bar;
        };

        bar.upColor = function (value) {
            if (!arguments.length) {
                return upColor;
            }
            upColor = value;
            return bar;
        };

        bar.yAttributeName = function (value) {
            if (!arguments.length) {
                return yAttributeName;
            }
            yAttributeName = value;
            return bar;
        };

        bar.totalVolume = function (value) {
            if (!arguments.length) {
                return totalVolume;
            }
            totalVolume = value;
            return bar;
        };

        bar.langData = function (value) {
            if (!arguments.length) {
                return langData;
            }
            langData = value;
            return bar;
        };

        bar.$container = function (value) {
            if (!arguments.length) {
                return $container;
            }
            $container = value;
            return bar;
        };

        return bar;
    }
    return barSeries;
});
