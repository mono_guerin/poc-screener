define(["../../../manager/event/eventManager", "../../../core/core", "../../../manager/chartConfig/graphTypes", "./barSeries", "./volumeByPrice", "./candlestickSeries", "./gridLines", "./ohlcSeries"],
    function(eventManager, core, GraphTypes, barSeries, volumeByPrice, candlestickSeries, gridLines, ohlcSeries) {
        "use strict";
        var $ = core.$;
        var d3 = core.d3;
        var minTickSize = 1,
            maxTickSize = 30;

        var commonPlotter = function(config) {
            var plotter = this;
            this.defaultConfig = {
                containerGElem: "",
                componentContainer: "",
                data: [],
                yAxisContainerGElem: "",
                graphCss: "",
                insertBeforeCss: "",
                graphType: GraphTypes.Line,
                xScale: null,
                yScale: null,
                xAttributeName: "",
                yAttributeName: "close",
                xFunc: function(d) {
                    return plotter.config.xScale(d[plotter.config.xAttributeName]);
                },
                yFunc: function(d) {
                    return plotter.config.yScale(d[plotter.config.yAttributeName]);
                },
                definedFunc: function(d) {
                    return !isNaN(d[plotter.config.yAttributeName]);
                },
                y0Value: 0
            };
            this._init(config);
        };

        commonPlotter.prototype = {
            _init: function(config) {
                this.setConfig(config);
            },
            setConfig: function(config) {
                this.config = $.extend(true, {}, this.defaultConfig);
                this.config = $.extend(this.config, config);
            },
            extendConfig: function(config) {
                $.extend(this.config, config);
            },
            draw: function() {
                var plotter = this;
                if (!this.config.xAttributeName) {
                    this.config.xFunc = function(d, i) {
                        return plotter.config.xScale(i);
                    };
                }
                return this._showElem(this["_draw_" + this.config.graphType]());
            },
            drawTrackball: function(config) {
                config.graphCss && (config.graphCss = config.graphCss + ' trackball');
                var graphElem = this._hideElem(this["_draw_track_" + config.trackType](config).classed("track-ball", true));
                config.globalGraphClass && graphElem.classed(config.globalGraphClass, true);
                return graphElem
            },
            _draw_track_circle: function(config) {
                var elem = config.trackContainer.append("circle")
                    .attr("r", config.circleRadius || 4)
                    .attr("cx", -100)
                    .attr("cy", -100);

                config.graphCss && elem.classed(config.graphCss, true);
                config.color && elem.style("stroke", config.color);
                return elem;
            },
            _draw_track_OHLC: function() {

            },
            _draw_track_candlestick: function() {

            },
            _caculateTickSize: function() {
                var width = this.config.width;
                var tickSize;
                if (width) {
                    tickSize = width / this.config.data.length - 1;
                    tickSize = Math.min(tickSize, maxTickSize);
                    tickSize = Math.max(tickSize, minTickSize);
                }
                return tickSize;
            },
            _commonDrawOuterElement: function() {
                var cfg = this.config;
                var newGElem;
                if (cfg.graphCss) {
                    newGElem = cfg.containerGElem.select("." + cfg.graphCss);
                }

                if (!newGElem || newGElem.empty()) {
                    if (cfg.insertBeforeCss) {
                        newGElem = cfg.containerGElem.insert("g", "." + cfg.insertBeforeCss);
                    } else {
                        newGElem = cfg.containerGElem.append("g");
                    }

                    newGElem.classed(cfg.graphCss, true);
                    cfg.globalGraphClass && newGElem.classed(cfg.globalGraphClass, true);
                }
                return this._hideElem(newGElem);
            },
            _commonDrawOHLC: function() {
                var cfg = this.config;
                var ohlcFunc = ohlcSeries().xScale(cfg.xScale)
                    .yScale(cfg.yScale).getElementsMapping(cfg.getElementsMapping)
                    .tickSize(this._caculateTickSize()).type(cfg.graphType);

                var dataInDomain = this._commonGetDataInDomain(cfg.data, cfg.xScale);
                var outerElem = this._commonDrawOuterElement();
                outerElem.datum(dataInDomain).call(ohlcFunc);

                return outerElem;
            },
            _commonGetDataInDomain: function(data, scale) {
                var domain = scale.domain();
                return data.slice(domain[0], domain[1] + 1);
            },
            _hideElem: function(elem) { elem.style("display", "none"); return elem },
            _showElem: function(elem) { elem.style("display", ""); return elem },
            _draw_line: function() {
                var cfg = this.config;
                //console.log(cfg.data[cfg.data.length-1].date);
                var lineFunc = d3.svg.line()
                    .x(cfg.xFunc)
                    .y(cfg.yFunc)
                    .defined(cfg.definedFunc);

                var outerElem = this._commonDrawOuterElement();

                var line = outerElem.append("path")
                    .datum(this._processData(cfg.data, cfg.definedFunc, cfg.xAttributeName))
                    .attr("d", lineFunc);

                cfg.color && line.style("stroke", cfg.color);
                return outerElem;
            },
            _draw_area: function() {
                var cfg = this.config;
                var area = d3.svg.area()
                    .x(cfg.xFunc)
                    .y0(cfg.y0Value)
                    .y1(cfg.yFunc)
                    .defined(cfg.definedFunc);

                var outerElem = this._commonDrawOuterElement();

                var graph = outerElem.append("path")
                    .datum(this._processData(cfg.data, cfg.definedFunc, cfg.xAttributeName))
                    .attr("d", area);

                cfg.color && graph.style("fill", cfg.color);
                return outerElem;
            },
            _draw_dot: function() {
                var cfg = this.config;
                var outerElem = this._commonDrawOuterElement();
                var dataInDomain = this._commonGetDataInDomain(cfg.data, cfg.xScale);
                var dataIndex = 0;

                if (!cfg.xAttributeName) {
                    cfg.xFunc = function() {
                        return cfg.xScale(dataIndex);
                    };
                }

                dataInDomain.forEach(function(d) {
                    if (!isNaN(d[cfg.yAttributeName])) {
                        var elem = outerElem.append("circle")
                            .attr("r", cfg.dotRadius)
                            .attr("cx", cfg.xFunc(d))
                            .attr("cy", cfg.yFunc(d));

                        cfg.color && elem.style('fill', cfg.color);
                    }
                    dataIndex++;
                });

                return outerElem;
            },
            _getTickTextSize: function(textArray, cls, font) {
                var width = 0,
                    height = 0,
                    temp,
                    css = {
                        display: 'inline-block'
                    };
                if (font) {
                    css['font'] = font;
                }
                var span = $('<span></span>').appendTo(this.config.componentContainer).css(css).addClass(cls);
                textArray.forEach(function(text) {
                    span.html(text);
                    var tempW, tempH;
                    tempW = span.width();
                    tempH = span.height();
                    if (tempW > width) {
                        width = tempW;
                    }
                    if (tempH > height) {
                        height = tempH;
                    }
                })
                span.remove();
                return {
                    width: width,
                    height: height
                };
            },
            _draw_lineLegend: function() {
                var cfg = this.config;
                var outerElem = this._commonDrawOuterElement();

                var downArrow = [{ x: 0, y: 0 }, { x: cfg.arrowWidth, y: 0 }, { x: cfg.arrowWidth, y: cfg.arrowHeight }];

                var tagArrow = outerElem.append('polygon')
                    .attr('class', cfg.graphCss);
                var tagRectangle = outerElem.append('rect')
                    .attr('class', cfg.graphCss)
                    .style('stroke', cfg.fillColor);
                var tagText = outerElem.append('text')
                    .attr("x", cfg.xPosition + cfg.sideMargin + cfg.arrowWidth)
                    .text(cfg.tagText)
                    .attr('class', cfg.graphCssText);
                if (tagText[0][0]) {
                    var legendTextSize = this._getTickTextSize([cfg.tagText], cfg.graphCssText);
                    var legendTextWidth = legendTextSize.width;
                    var legendTextHeight = legendTextSize.height;
                    var tagWidth = legendTextWidth + cfg.sideMargin * 2 + cfg.arrowWidth;

                    tagRectangle.attr("x", cfg.xPosition)
                        .attr("y", cfg.yPosition - cfg.arrowHeight - legendTextHeight)
                        .style('fill', cfg.fillColor)
                        .style('stroke', cfg.fillColor)
                        .attr("width", legendTextWidth + cfg.sideMargin * 2)
                        .attr("height", legendTextHeight);

                    var legendRectHeight = parseFloat(tagRectangle[0][0].attributes.height.value);
                    if (legendRectHeight == 0) {
                        legendRectHeight = 20;
                    }

                    tagText.attr("y", cfg.yPosition - legendRectHeight + cfg.textTopMargin);
                    tagText.attr("x", cfg.xPosition + cfg.sideMargin - tagWidth);

                    tagRectangle.attr("x", cfg.xPosition - tagWidth);

                    downArrow.forEach(function(l) {
                        l.x += cfg.xPosition - 2 * cfg.arrowWidth + 1;
                        l.y += cfg.yPosition - cfg.arrowHeight;
                    });

                    tagArrow.style('fill', cfg.fillColor)
                        .attr("points", downArrow.map(function(a) {
                            return a.x + ',' + a.y;
                        }).join(' '));
                }

                return outerElem;

            },
            _draw_tag: function() {
                var cfg = this.config;
                var outerElem = this._commonDrawOuterElement();
                var tagArrow = outerElem.append('polygon')
                    .attr('class', cfg.graphCss);

                var tagRectangle = outerElem.append('rect')
                    .attr('class', cfg.graphCss)
                    .style('stroke', cfg.fillColor);

                var tagRectangleWidth, sideMargin;
                var tagText = outerElem.append('text')
                    .text(cfg.tagText)
                    .attr('class', cfg.graphCssText);
                if (tagText[0][0]) {
                    //Hardcoded width and height for FF and IE
                    var legendTextSize = this._getTickTextSize([cfg.tagText], cfg.graphCssText);
                    var legendTextWidth = legendTextSize.width;
                    var legendTextHeight = legendTextSize.height;

                    var tagRectWidth, sideMargin;
                    if (cfg.tagWidth && cfg.tagWidth > legendTextWidth) {
                        tagRectWidth = cfg.tagWidth;
                        sideMargin = (tagRectWidth - legendTextWidth) / 2;
                    } else {
                        tagRectWidth = legendTextWidth + cfg.sideMargin * 2;
                        sideMargin = cfg.sideMargin;
                    }
                    tagText.attr("x", cfg.xPosition + cfg.sideMargin + cfg.arrowWidth);
                    var tagWidth = tagRectWidth + cfg.arrowWidth;
                    var rightArrow, leftArrow;
                    var rectangleYPos = cfg.yPosition;
                    var rectangleHeight = cfg.tagHeight || legendTextHeight;
                    var yVerticalMargin = cfg.yVerticalMargin || 3;
                    var yPosAjust = 0;
                    if (cfg.upward) {
                        rectangleYPos -= rectangleHeight;
                        if (rectangleYPos < yVerticalMargin) {
                            yPosAjust = yVerticalMargin - rectangleYPos;
                        }

                        rectangleYPos += yPosAjust;
                        rightArrow = [{ x: 0, y: 0 }, { x: cfg.arrowWidth, y: 0 }, { x: 0, y: -cfg.arrowHeight }];
                        leftArrow = [{ x: 0, y: 0 }, { x: cfg.arrowWidth, y: 0 }, { x: cfg.arrowWidth, y: -cfg.arrowHeight }];
                    } else {
                        var rectangleBottomPos = (rectangleYPos + rectangleHeight + yVerticalMargin);
                        if (rectangleBottomPos > cfg.height) {
                            yPosAjust = rectangleBottomPos - cfg.height;
                        }

                        rectangleYPos -= yPosAjust;
                        rightArrow = [{ x: 0, y: 0 }, { x: cfg.arrowWidth, y: 0 }, { x: 0, y: cfg.arrowHeight }];
                        leftArrow = [{ x: 0, y: 0 }, { x: cfg.arrowWidth, y: 0 }, { x: cfg.arrowWidth, y: cfg.arrowHeight }];
                    }

                    tagRectangle.attr("x", cfg.xPosition + cfg.arrowWidth)
                        .attr("y", rectangleYPos)
                        .style('fill', cfg.fillColor)
                        .style('stroke', cfg.fillColor)
                        .attr("width", tagRectWidth)
                        .attr("height", cfg.tagHeight || legendTextHeight);

                    var legendRectHeight = parseFloat(tagRectangle[0][0].attributes.height.value);
                    if (legendRectHeight == 0) {
                        legendRectHeight = 20;
                    }

                    tagText.attr("y", rectangleYPos + legendRectHeight - cfg.textTopMargin + 3);
                    if (tagWidth >= cfg.spaceOnRight) {
                        tagText.attr("x", cfg.xPosition + sideMargin - tagWidth);
                        tagRectangle.attr("x", cfg.xPosition - tagWidth);
                        rightArrow.forEach(function(l) {
                            l.x += cfg.xPosition - cfg.arrowWidth;
                            l.y += cfg.yPosition;
                        });

                        tagArrow.style('fill', cfg.fillColor)
                            .attr("points", rightArrow.map(function(a) {
                                return a.x + ',' + a.y;
                            }).join(' '));
                    } else {
                        leftArrow.forEach(function(l) {
                            l.x += cfg.xPosition;
                            l.y += cfg.yPosition;
                        });

                        tagArrow.style('fill', cfg.fillColor)
                            .attr("points", leftArrow.map(function(a) {
                                return a.x + ',' + a.y;
                            }).join(' '));
                    }
                }
                return outerElem;
            },
            _draw_event: function() {
                var cfg = this.config;
                var outerElem = this._commonDrawOuterElement();
                if (cfg.data && cfg.data.length > 0) {
                    cfg.data.forEach(function(d) {
                        if (d.x >= 0) {
                            var xPosition = cfg.xScale(d.x);

                            outerElem
                                .append("ellipse")
                                .attr("cx", xPosition)
                                .attr("cy", cfg.y0Value)
                                .attr("rx", cfg.xRadious)
                                .attr("ry", cfg.yRadious)
                                .style('fill', d.upFlag === 0 ? cfg.downColor : cfg.color)
                                .attr("class", cfg.graphCss);

                            outerElem
                                .append('text')
                                .attr("x", xPosition)
                                .attr("y", cfg.y0Value)
                                .text(cfg.innerText)
                                .attr('dy', cfg.dy)
                                .attr('text-anchor', cfg.textAnchor)
                                .style('fill', cfg.textColor)
                                .attr("class", cfg.textClass);
                        }
                    });
                }
                return outerElem;
            },
            _draw_aboveBelow: function() {
                var cfg = this.config;
                var yClipValue = cfg.yScale(cfg.referValue);
                var areaFunc = d3.svg.area()
                    .x(cfg.xFunc)
                    .y1(cfg.yFunc)
                    .defined(cfg.definedFunc);

                var outerElem = this._commonDrawOuterElement();
                //Create above area
                outerElem.append("clipPath")
                    .attr("id", cfg.aboveClipID)
                    .append('rect')
                    .attr('x', 0)
                    .attr('y', 0)
                    .attr('width', cfg.width)
                    .attr('height', yClipValue);

                var datumData = this._processData(cfg.data, cfg.definedFunc, cfg.xAttributeName);

                outerElem.append("path").datum(datumData)
                    .attr("class", cfg.aboveAreaCss)
                    .style({ stroke: "#" + cfg.ASColor, fill: "#" + cfg.AFColor })
                    .attr("clip-path", "url(#" + cfg.aboveClipID + ")")
                    .attr("d", areaFunc.y0(cfg.height));
                //Create below area
                outerElem.append("clipPath")
                    .attr("id", cfg.belowClipID)
                    .append('rect')
                    .attr('x', 0)
                    .attr('y', yClipValue)
                    .attr('width', cfg.width)
                    .attr('height', cfg.height - yClipValue);

                outerElem.append("path").datum(datumData)
                    .attr("class", cfg.belowAreaCss)
                    .style({ stroke: "#" + cfg.BSColor, fill: "#" + cfg.BFColor })
                    .attr("clip-path", "url(#" + cfg.belowClipID + ")")
                    .attr("d", areaFunc.y0(0));

                return outerElem;
            },
            _draw_ohlc: function() {
                return this._commonDrawOHLC();
            },
            _draw_hlc: function() {
                return this._commonDrawOHLC();
            },
            _draw_candlestick: function() {
                var cfg = this.config;
                var dataInDomain = this._commonGetDataInDomain(cfg.data, cfg.xScale);

                var candlestickFunc = candlestickSeries().xScale(cfg.xScale)
                    .yScale(cfg.yScale).getElementsMapping(cfg.getElementsMapping)
                    .tickSize(this._caculateTickSize());

                var outerElem = this._commonDrawOuterElement();
                outerElem.datum(dataInDomain).call(candlestickFunc);

                return outerElem;
            },
            _draw_gridLines: function() {
                var cfg = this.config;
                var grid = gridLines().setShowGridBlocks(cfg.showGridBlocks).setShowSolidGridLines(cfg.showSolidGridLines).xScale(cfg.xScale).yScale(cfg.yScale).yTicks(cfg.yTicks).xTicks(cfg.xTicks);
                var outerElem = this._commonDrawOuterElement();
                outerElem.call(grid);
                return outerElem;
            },
            _draw_zoomDrag: function() {
                var cfg = this.config;
                var zoom = d3.behavior.zoom()
                    .center([cfg.width, 0])
                    .x(cfg.xScale)
                    .on("zoomstart", function() {
                        cfg.panStartCallback && cfg.panStartCallback(zoom);
                    })
                    .on("zoom", function() {
                        cfg.panCallback && cfg.panCallback(zoom);
                    })
                    .on("zoomend", function() {
                        cfg.panEndCallback && cfg.panEndCallback(zoom);
                    });

                var maxScale = cfg.data.length / 3;
                var minScale = 0.5;
                zoom.scaleExtent([minScale, maxScale]);

                var outerElem = this._commonDrawOuterElement();

                outerElem.append("rect")
                    .attr("transform", "translate(" + (cfg.translateLeft || cfg.margin.left) + "," + cfg.margin.top + ")")
                    .attr("class", cfg.zoomDragMaskCss)
                    .attr("width", cfg.width)
                    .attr("height", cfg.height)
                    .call(zoom);

                return outerElem;
            },
            _draw_histogram: function() {
                var cfg = this.config;
                var dataInDomain = this._commonGetDataInDomain(cfg.data, cfg.xScale);
                var bars = barSeries().xScale(cfg.xScale)
                    .yScale(cfg.yScale).width(cfg.width).height(cfg.height)
                    .tickSize(this._caculateTickSize());

                $.isNumeric(cfg.baseValue) && bars.baseValue(cfg.baseValue);
                cfg.color && bars.color(cfg.color);
                cfg.xAttributeName && bars.xAttributeName(cfg.xAttributeName);
                cfg.yAttributeName && bars.yAttributeName(cfg.yAttributeName);
                cfg.distinguish && bars.distinguish(cfg.distinguish);
                cfg.getElementsMapping && bars.getElementsMapping(cfg.getElementsMapping);
                var outerElem = this._commonDrawOuterElement();
                outerElem.datum(dataInDomain).call(bars);
                if (outerElem.selectAll("rect") &&
                    outerElem.selectAll("rect")[0] &&
                    outerElem.selectAll("rect")[0][0] &&
                    $(outerElem.selectAll("rect")[0][0]).attr('x') < 0) {
                    var xvalue = parseFloat($(outerElem.selectAll("rect")[0][0]).attr('x'));
                    var width = parseFloat($(outerElem.selectAll("rect")[0][0]).attr('width'));
                    $(outerElem.selectAll("rect")[0][0]).attr('x', 0).attr('width', width + xvalue);
                }
                return outerElem;
            },
            _draw_brush: function() {
                var cfg = this.config;
                var self = this;

                var brushFunc = d3.svg.brush().x(cfg.xScale)
                    .on("brushstart", brushStart).on("brush", onBrush).on("brushend", brushEnd);

                cfg.setBrushFunc && cfg.setBrushFunc(brushFunc, this);

                function brushStart() {
                    cfg.brushStartResponseFunc && cfg.brushStartResponseFunc(brushFunc);
                };

                function onBrush() {
                    cfg.brushResponseFunc && cfg.brushResponseFunc(brushFunc, this);
                }

                function brushEnd() {
                    cfg.brushEndResponseFunc && cfg.brushEndResponseFunc(brushFunc);
                };

                var outerElem = this._commonDrawOuterElement();

                outerElem.classed("x brush", true)
                    .call(brushFunc);

                outerElem.selectAll("rect")
                    .attr("y", cfg.yValue)
                    .attr("height", cfg.height);

                function changeBrushExtent(newDomainValue, sender) {
                    if (sender !== this) {
                        outerElem.call(brushFunc.extent(newDomainValue));
                        //.call(brushFunc.event);
                    }
                };

                var brushImages = cfg.brushImages;
                if (brushImages) {
                    var brushImageIDPrefix = new Date().getTime();
                    var resizeElems = outerElem.selectAll(".resize");

                    resizeElems.append("image")
                        .attr("id", function(d, i) {
                            return brushImageIDPrefix + "-" + i;
                        }).attr({
                            "width": cfg.brushImagesWidth,
                            "height": cfg.height,
                            "x": cfg.brushImagesXAdjust,
                            "y": 0,
                            "xlink:href": function(d, i) {
                                return brushImages[i];
                            }
                        });

                    cfg.setExtentFunc && cfg.setExtentFunc(changeBrushExtent);
                }

                return outerElem;
            },
            _draw_axis: function() {
                var cfg = this.config;

                var axisFunc = d3.svg.axis().scale(cfg.scale).orient(cfg.orient);
                cfg.tickSize && axisFunc.tickSize(cfg.tickSize);
                cfg.ticks && axisFunc.ticks(cfg.ticks);
                cfg.tickPadding && axisFunc.tickPadding(cfg.tickPadding);
                cfg.tickValues && axisFunc.tickValues(cfg.tickValues);
                cfg.tickFormatFunc && axisFunc.tickFormat(cfg.tickFormatFunc);

                var outerElem = this._commonDrawOuterElement();
                var translateValues;

                function leftStyleFunc(elem) {
                    if (cfg.tick.line.show) {
                        elem.selectAll("line")
                            .attr("x1", 0)
                            .attr("x2", cfg.yAxisWidth)
                            .style(cfg.tick.line.style);
                    } else {
                        elem.selectAll("line").style("display", "none");
                    }
                    elem.selectAll("text").style("text-anchor", "start")
                        .attr("x", 0).attr("dy", "1em");
                    return elem;
                };

                function topStyleFunc(elem) {
                    elem.selectAll("text").attr("x", 1).attr("y", -6);
                    elem.call(xAxisStyleFunc);
                    return elem;
                };

                function rightStyleFunc(elem) {
                    if (cfg.tick.line.show) {
                        elem.selectAll("line")
                            .attr("x1", -cfg.yAxisWidth)
                            .attr("x2", 0)
                            .style(cfg.tick.line.style);
                    } else {
                        elem.selectAll("line").style("display", "none");
                    }
                    elem.selectAll("text").style("text-anchor", "end")
                        .attr("x", 0).attr("dy", "1em");

                    return elem;
                };

                function bottomStyleFunc(elem) {
                    elem.selectAll("text").attr("x", 1).attr("y", 1);
                    elem.call(xAxisStyleFunc);
                    return elem;
                };

                function xAxisStyleFunc(elem) {
                    var textEl = elem.selectAll("text");
                    if (cfg.xTickInfos && cfg.xTickInfos.tickLabels) {
                        var count = 0;
                        for (var x in cfg.xTickInfos.tickLabels) {
                            d3.select(textEl[0][count]).style("text-anchor", cfg.xTickInfos.tickLabels[x].textAnchor);
                            count++;
                        }
                    }
                    return elem;
                }
                var defaultStyleFunc;
                var yAxisMargin = (isNaN(cfg.yAxisWidth) || !cfg.yAxisMargin) ? 0 : cfg.yAxisMargin;
                var yAxisWidth = (isNaN(cfg.yAxisWidth) || !cfg.yAxisWidth) ? 0 : cfg.yAxisWidth;
                yAxisMargin = parseInt(yAxisMargin, 10);

                switch (cfg.orient) {
                    case "left":
                        if (cfg.position == "outer") {
                            translateValues = [0, 0];
                        } else {
                            translateValues = [0 + yAxisMargin, 0];
                        }
                        defaultStyleFunc = leftStyleFunc;
                        outerElem = cfg.yAxisContainerGElem || outerElem;
                        break;
                    case "top":
                        translateValues = [0, 0];
                        defaultStyleFunc = topStyleFunc;
                        break;
                    case "right":
                        if (cfg.position == "outer") {
                            translateValues = [cfg.width + yAxisWidth + yAxisMargin, 0]
                        } else {
                            translateValues = [cfg.width - yAxisMargin, 0];
                        }
                        defaultStyleFunc = rightStyleFunc;
                        outerElem = cfg.yAxisContainerGElem || outerElem;
                        break;
                    case "bottom":
                        translateValues = [0, cfg.height];
                        defaultStyleFunc = bottomStyleFunc;
                        break;
                }

                outerElem.classed("axis", true)
                    .attr("transform", "translate(" + translateValues[0] + "," + translateValues[1] + ")");

                outerElem.style("display", "none");
                outerElem.call(axisFunc).call(defaultStyleFunc);

                if (cfg.orient == "right" || cfg.orient == "left") {
                    var ticksElems = outerElem.selectAll(".tick")[0];
                    if (ticksElems.length > 0) {
                        var $firstTickText = $(ticksElems[0]).find("text");
                        var textHeight = this._getTickTextSize([$firstTickText.text()], null, $firstTickText.css('font')).height;
                        $firstTickText.attr("y", -textHeight);
                        if (ticksElems.length > 1) {
                            var $secondTick = $(ticksElems[1]);
                            var firstTickY = d3.transform(d3.select(ticksElems[0]).attr("transform")).translate[1];
                            var secondTickY = d3.transform(d3.select(ticksElems[1]).attr("transform")).translate[1];
                            if (firstTickY - secondTickY < textHeight * 2) {
                                $secondTick.find("text").attr("y", -textHeight);
                            }
                        }
                    }
                }
                outerElem.style("display", "block");
                return outerElem;
            },
            _draw_volumeByPrice: function() {
                var cfg = this.config;
                var bars = volumeByPrice().yScale(cfg.yScale)
                    .width(cfg.width).height(cfg.height)
                    .totalVolume(cfg.totalVolume)
                    .downColor(cfg.downColor)
                    .upColor(cfg.upColor)
                    .$container(cfg.$container);

                cfg.langData && bars.langData(cfg.langData);
                cfg.yAttributeName && bars.yAttributeName(cfg.yAttributeName);
                var outerElem = this._commonDrawOuterElement();
                outerElem.datum(cfg.data).call(bars);
                cfg.setDrawObj && cfg.setDrawObj(bars);
                return outerElem;
            },
            _processData: function(data, defined, xAttributeName) {
                var ret = [];
                var validDataPoint = {};
                var l = data.length;
                var maxValidIndex = l;
                for (var i = l - 1; i >= 0; i--) {
                    if (defined(data[i])) {
                        maxValidIndex = i;
                        break;
                    }
                }
                for (var j = 0, d; j < l; j++) {
                    d = data[j];
                    if (defined(d)) {
                        validDataPoint = d;
                        ret.push(d);
                    } else if (defined(validDataPoint) && j <= maxValidIndex) {
                        var extDataPoint = {};
                        extDataPoint[xAttributeName] = j;
                        ret.push($.extend({}, validDataPoint, extDataPoint));
                    } else {
                        ret.push(d);
                    }
                }
                return ret;
            }
        };

        return commonPlotter;
    });