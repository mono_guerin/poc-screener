define(["../../core/core",
        "../../manager/data/request",
        "../../../html/menuBar/autocompleteContainer.html",
        "../../../html/menuBar/autocompleteContainerFullScreen.html",
        "../../../html/menuBar/autocompleteCategory.html",
        "../../../html/menuBar/autocompleteCategoryFullScreen.html",
        "../../../html/menuBar/autoCompleteCatHeaderFullScreen.html",
        "../../../html/menuBar/autocompleteRow.html",
        "../../../html/menuBar/autocompleteNoresults.html",
        "morningstar",
        "../../util/util"
    ],
    function(
        core,
        Request,
        autocompleteContainer,
        autocompleteContainerFullScreen,
        autocompleteCategory,
        autocompleteCategoryFullScreen,
        autoCompleteCatHeaderFullScreen,
        autocompleteRow,
        autocompleteNoresults,
        morningstar,
        util) {
        "use strict";

        /**
         *   AutoComplete Model
         *
         */
        var $ = core.$;
        var asterixUtil = morningstar.asterix.util;

        var model = function(managers, Popover) {
            var eventManager = managers.eventManager;
            var CCM = managers.chartConfigManager;
            var dataAdapter = managers.dataAdapter;
            var rootConfig;
            var $clearButton;
            var $menuBarContainer;
            var AutoComplete = function(config) {
                this.config = {};
                rootConfig = CCM.getConfig();
                this._init(config);
            };

            AutoComplete.prototype = {
                /* test-code */
                __private: {
                    CCM: CCM,
                    dataAdapter: dataAdapter,
                    eventManager: eventManager,
                },
                /* end-test-code */
                _init: function(config) {
                    $.extend(true, this.config, config);
                    $menuBarContainer = this.config.$menuBarContainer;
                    var obj = {
                        'autoCompleteChangeMainTicker': rootConfig.autoComplete.action === 'main' ? true : false,
                        'add-comparison': CCM.getLabel('addComparison'),
                        'change-ticker': CCM.getLabel('changeTicker')
                    }
                    this.$mobileSearch = $(autocompleteContainerFullScreen(obj));
                    this._bindEvents();

                },
                _bindEvents: function() {
                    var self = this;

                    function onMobileSearchIcon(event) {
                        var $mobileSearch = self.$mobileSearch;
                        var el = self.$mobileSearch;
                        var popObj = { $el: el };
                        var popConfig = {
                            $container: rootConfig.$container,
                            $target: $menuBarContainer.find('.mkts-cmpt-svgcht-menuicn--search'),
                            toggle: false,
                            styleClass: 'mkts-cmpt-svgcht-popover--search-mobile',
                            $componentContainer: rootConfig.$componentContainer,
                            $menuBarContainer: $menuBarContainer,
                            sync: false
                        };
                        $mobileSearch.find('.mkts-cmpt-svgcht-search-input--mobile').val('').focus();
                        $mobileSearch.find('.mkts-cmpt-svgcht-search-results-container').empty();
                        Popover.show(popObj, popConfig);
                        event.stopPropagation();
                    }

                    function makeAutocompleteMarkup(d, ss, isMobile) {
                        function makeCat(catData) {
                            function makeEachExchangeHeader() {
                                var rowObj = {
                                    name: catObj.catName,
                                    ticker: "Identifier",
                                    exchange: "exchange",
                                    type: "Type",
                                    isShowRelateTickerAndMajorIndex: true,
                                    headerClassName: "mkts-cmpt-svgcht-search-header"
                                }
                                var rowStr = autocompleteRow(rowObj);
                                var row = $(rowStr);
                                return row;
                            };

                            function makeRow(rowData) {
                                var needBoldReplace = rootConfig.autoComplete.highlight;
                                var tickerPriority = rootConfig.autoComplete.tickerPriority ? rootConfig.autoComplete.tickerPriority.split(',') : ['ticker'];
                                var rowObj = {
                                    name: util.boldSearch(rowData.name, ss, needBoldReplace),
                                    exchange: rowData.exch,
                                    type: rowData.type,
                                    cols: {}
                                };

                                if (rootConfig.autoComplete.removeExch !== true) {
                                    rowObj.cols.exchange = true;
                                }

                                tickerPriority.forEach(function(element) {
                                    if (!rowObj.ticker && rowData[element]) {
                                        rowObj.ticker = util.boldSearch(rowData[element], ss, needBoldReplace);
                                    }
                                });

                                if (rootConfig.isShowRelateTickerAndMajorIndex) {
                                    rowObj.isShowRelateTickerAndMajorIndex = true;
                                }
                                var rowStr = autocompleteRow(rowObj);
                                var row = $(rowStr);
                                var li = row[0];

                                li.ticker = rowData.ticker; // this should be used to display in the UI
                                li.identifier = rowData.identifier; // this should be used to transfer in each modules like dataAdapter, chart and so on.
                                li.name = rowData.name;
                                li.secId = rowData.secId;
                                li.currency = rowData.currency;
                                return row;
                            };
                            var catObj = { catName: CCM.getLabel(formatCategory(catData.catName)) || catData.catName };
                            var $td = $('<td>');
                            var rows = catData.results;
                            var rowLen = rows.length || 0;
                            var catStr, $cat, i, $table, header;

                            /* test-code */
                            self.__private._bindEvents.makeAutocompleteMarkup.makeCat.makeRow = makeRow;
                            /* end-test-code */

                            if (isMobile) {
                                catStr = autocompleteCategoryFullScreen();
                                header = autoCompleteCatHeaderFullScreen(catObj);
                            } else {
                                catObj.isShowRelateTickerAndMajorIndex = rootConfig.isShowRelateTickerAndMajorIndex;
                                catStr = autocompleteCategory(catObj);
                            }

                            $cat = $(catStr);
                            for (i = 0; i < rowLen; i++) {
                                $td.append(makeRow(rows[i]));
                            }
                            if (rootConfig.isShowRelateTickerAndMajorIndex) {
                                $td.prepend(makeEachExchangeHeader());
                            }

                            $cat.append($td);
                            if (isMobile) {
                                $table = $('<table>');
                                $table.append(header);
                                $table.append($cat);
                                return $table;
                            }

                            return $cat;
                        }

                        function formatCategory(str) {
                            if (str) {
                                return str.replace(/\s+\&\s+|\-|(\b\s+\b)/g, "").replace(/\b\w/g, function($0) { return $0.toLocaleLowerCase(); });
                            }
                            return str;
                        }

                        function loopData() {
                            var len = results.length;
                            var i;
                            var tableInEl = $el.find('.mkts-cmpt-svgcht-search-table');
                            var appendElement = tableInEl.length > 0 ? tableInEl : $el;

                            if (len > 0) {
                                for (i = 0; i < len; i++) {
                                    if (results[i].results.length) {
                                        appendElement.append(makeCat(results[i]));
                                    }
                                }
                            } else {
                                appendElement.append(autocompleteNoresults());
                            }
                        }
                        var results = d.data() || [];
                        var req = d.req();
                        var reqKey = req.keyword();
                        var $el = null;

                        /* test-code */
                        self.__private._bindEvents.makeAutocompleteMarkup.makeCat = makeCat;
                        self.__private._bindEvents.makeAutocompleteMarkup.loopData = loopData;
                        /* end-test-code */

                        if (asterixUtil.isString(reqKey) && reqKey === self.autoCompleteKeyword) {
                            var _obj = {};
                            if (rootConfig.isShowRelateTickerAndMajorIndex) {
                                _obj.isShowRelateTickerAndMajorIndex = true;
                            }
                            $el = $(autocompleteContainer(_obj));
                            loopData();
                        }

                        return $el;
                    }

                    function search(searchStr, target, isMobile) {
                        function displayResults(data) {
                            var $el = makeAutocompleteMarkup(data, searchStr, isMobile);

                            if ($el) {
                                if (asterixUtil.isObj(target)) {
                                    $(target).empty().append($el);
                                }
                                showAutocompletePopup($el);
                            }
                        }
                        var request = new Request();
                        var cats = rootConfig.autocompleteCategories;

                        self.autoCompleteKeyword = searchStr;
                        request.keyword(searchStr);

                        request.category(cats);
                        request.nonFilterCategory(rootConfig.autoComplete.nonFilterCategory);
                        dataAdapter.fetchAutocompleteData(request, displayResults);
                    }

                    function showAutocompletePopup(el) {
                        var popObj = { $el: el };
                        var popConfig = {
                            $container: rootConfig.$container,
                            $target: $menuBarContainer.find('.mkts-cmpt-svgcht-search'),
                            toggle: false,
                            styleClass: 'mkts-cmpt-svgcht-popover--search',
                            $componentContainer: rootConfig.$componentContainer,
                            $menuBarContainer: $menuBarContainer
                        };

                        if (rootConfig.$container.width() <= 600) {
                            popObj = { $el: $menuBarContainer.find('.mkts-cmpt-svgcht-popover--search-mobile') };
                            popConfig.styleClass = 'mkts-cmpt-svgcht-popover--search-mobile';
                            popConfig.sync = true;
                        }

                        $menuBarContainer.find('.mkts-cmpt-svgcht-popover--search, mkts-cmpt-svgcht-popover--search-mobile').remove();
                        Popover.show(popObj, popConfig);
                    }

                    function onMainSearchKey(e) {

                        if (isActionKey(e)) {
                            onActionKey(e.keyCode);
                        } else {
                            var ss,
                                __$this = $(e.target),
                                __isMobile = __$this.hasClass('mkts-cmpt-svgcht-search-input--mobile'),
                                __$target = !__isMobile ? undefined : $menuBarContainer.find('.mkts-cmpt-svgcht-search-results-container');
                            if (asterixUtil.isObj(e) && asterixUtil.isObj(e.target) && asterixUtil.isString(e.target.value) && e.target.value !== '') {
                                ss = e.target.value; //make closure for boldSearch function
                                clearButtonShow(true);
                                if (CCM.getConfig().autoComplete.dataSource && CCM.getConfig().autoComplete.dataSource.toLocaleLowerCase() === 'eu' && $.trim(ss).length < 2) {
                                    $menuBarContainer.find('.mkts-cmpt-svgcht-popover--search').remove();
                                    return;
                                }
                                search(ss, __$target, __isMobile);
                            } else {
                                $menuBarContainer.find('.mkts-cmpt-svgcht-popover--search').remove();
                                clearButtonShow(false);
                            }
                        }

                    }

                    function isActionKey(e) {
                        var keyList = [13, 27, 37, 38, 39, 40];
                        var keyCode;

                        if (asterixUtil.isObj(e) && asterixUtil.isNumber(e.keyCode)) {
                            keyCode = e.keyCode;
                        }

                        return keyList.indexOf(keyCode) > -1;
                    }

                    function onActionKey(key) {
                        function clearActive() {
                            $results.removeClass('active');
                        }
                        var action = {
                            close: function() {
                                $menuBarContainer.find('.mkts-cmpt-svgcht-popover--search').remove();
                                $menuBarContainer.find('.mkts-cmpt-svgcht-search-results-container').empty();
                            },
                            select: function() {
                                if ($target) {
                                    $target.click();
                                }
                            },
                            prev: function() {
                                if ($target && targetIndex !== 0) {
                                    $target = $($results[targetIndex - 1]);
                                    clearActive();
                                    $target.addClass('active');
                                } else {
                                    $target = $($results[lastIndex]);
                                    clearActive();
                                    $target.addClass('active');
                                }
                                $container.scrollTop($target.position().top);
                            },
                            next: function() {
                                if ($target && targetIndex !== lastIndex) {
                                    $target = $($results[targetIndex + 1]);
                                    clearActive();
                                    $target.addClass('active');
                                } else {
                                    $target = $($results[0]);
                                    clearActive();
                                    $target.addClass('active');
                                }
                                $container.scrollTop($target.position().top);
                            }
                        };
                        var $container = $menuBarContainer.find('.mkts-cmpt-svgcht-search-table');
                        var $results = $menuBarContainer.find('.mkts-cmpt-svgcht-search-result');
                        var $active = $results.filter('.active');
                        var $target, targetIndex, lastIndex;

                        if (asterixUtil.isNumber(key)) {
                            key = key.toString();
                            key = key.replace('27', 'close').replace('13', 'select').replace('37', 'prev').replace('38', 'prev').replace('39', 'next').replace('40', 'next');

                            if (asterixUtil.isFunction(action[key])) {
                                if ($results.length > 0) {
                                    if ($active.length > 0) {
                                        $target = $($active[0]);
                                        targetIndex = $results.index($active[0]);
                                        lastIndex = $results.length - 1;
                                    }
                                }
                                action[key]();
                            }
                        }
                    }

                    function onResultClick() {
                        var autoCompleteAction = rootConfig.autoComplete.action;

                        if (asterixUtil.isObj(this) && asterixUtil.isString(this.ticker)) {
                            $menuBarContainer.find('.mkts-cmpt-svgcht-search-input').val('').blur();
                            clearButtonShow(false);
                            if (autoCompleteAction === 'main') {
                                if (rootConfig.autoComplete.changeTicker) {
                                    eventManager.trigger(eventManager.EventTypes.ChangeTicker, this.identifier);
                                    console.debug(eventManager.EventTypes.ChangeTicker, this.identifier);
                                }
                                if ($.isFunction(rootConfig.onAutoCompleteClick)) {
                                    rootConfig.onAutoCompleteClick(this.identifier);
                                }
                            } else {
                                var compareTicker = this.identifier;
                                if (rootConfig.autoComplete.dataSource === 'EU' && rootConfig.useEUTimeSeriesData) {
                                    compareTicker = {
                                        id: rootConfig.useEUSecurityData ? this.secId : this.identifier,
                                        securityToken: this.secId + ']',
                                        name: this.name,
                                        currency: this.currency
                                    };
                                } else if (rootConfig.useEUSecurityData) {
                                    compareTicker = this.secId;
                                }
                                eventManager.trigger(eventManager.EventTypes.CompareTicker, compareTicker);
                                console.debug(eventManager.EventTypes.CompareTicker, this.identifier);
                            }

                            /*
                             * this.identifier is like 126.1.IBM
                             */
                            CCM.triggerClickCallback({
                                name: 'autoComplete',
                                value: autoCompleteAction === 'main' ? 'changeMainTicker' : 'addComparison'
                            });

                            $(this).parents('.mkts-cmpt-svgcht-popover--search').remove();
                            $(this).parents('.mkts-cmpt-svgcht-popover--search-mobile').remove();
                        }
                    }

                    function onSearchClick(event, isShowPopover) {
                        // hide date range
                        eventManager.trigger(eventManager.EventTypes.HideDateRange);

                        if (event) {
                            event.stopPropagation();
                        }

                        function displayResults(data) {
                            var $el = makeAutocompleteMarkup(data, searchStr, isMobile);

                            if ($el) {
                                if (asterixUtil.isObj(target)) {
                                    $(target).empty()
                                        .append($el);
                                } else {
                                    if (isShowPopover) {
                                        showAutocompletePopup($el);
                                    }
                                }
                            }
                        }

                        // trigger click event first
                        CCM.triggerClickCallback({
                            name: 'autoComplete'
                        });

                        if (!rootConfig.isShowRelateTickerAndMajorIndex) {
                            return;
                        }
                        var isShowPopover = isShowPopover == false ? false : true;
                        var target;
                        var ifInputBlack = $menuBarContainer.find('.mkts-cmpt-svgcht-search-input').val();
                        if (ifInputBlack !== '') {
                            return;
                        }

                        var searchStr = "relateTickerAndMajorIndex";
                        var isMobile = false;

                        self.autoCompleteKeyword = searchStr;

                        var request = new Request();
                        request.tickerObjects([rootConfig.mainTickerObject]);
                        var cats = rootConfig.autocompleteCategories;
                        request.keyword(searchStr);
                        request.category(cats);
                        request.nonFilterCategory(rootConfig.autoComplete.nonFilterCategory);

                        dataAdapter.fetchRelatedTickersAndMajorIndexData(request, displayResults);
                    }

                    function onSearchClear() {
                        $menuBarContainer.find('.mkts-cmpt-svgcht-search-input').val('').focus();
                        clearButtonShow(false);
                    }
                    //true or false
                    function clearButtonShow(show) {
                        $clearButton = $clearButton || $menuBarContainer.find('.mkts-cmpt-svgcht-search-clear');
                        $clearButton[show ? 'show' : 'hide']();
                    }

                    function onMobileSearchClear() {
                        $menuBarContainer.find('.mkts-cmpt-svgcht-search-input--mobile').val('').focus();
                        $menuBarContainer.find('.mkts-cmpt-svgcht-search-results-container').empty();
                        clearButtonShow(false);
                    }

                    var self = this;

                    function onMouseOver(event) {
                        $(event.currentTarget).addClass("active");
                    }

                    function onMouseLeave(event) {
                        $(event.currentTarget).removeClass("active");
                    }

                    /* test-code */
                    self.__private._bindEvents = {
                        self: self,
                        onMobileSearchIcon: onMobileSearchIcon,
                        makeAutocompleteMarkup: makeAutocompleteMarkup,
                        search: search,
                        showAutocompletePopup: showAutocompletePopup,
                        onMainSearchKey: onMainSearchKey,
                        onMouseOver: onMouseOver,
                        onMouseLeave: onMouseLeave,
                        isActionKey: isActionKey,
                        onActionKey: onActionKey,
                        onResultClick: onResultClick

                    };
                    /* end-test-code */

                    if (rootConfig.isShowRelateTickerAndMajorIndex) {
                        eventManager.bind(eventManager.EventTypes.FetchPreloadRelatedAndIndexData, onSearchClick);
                    }

                    rootConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-menuicn--search', onMobileSearchIcon);
                    rootConfig.$componentContainer.on('keyup.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-search-input', onMainSearchKey);
                    rootConfig.$componentContainer.on('mouseover.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-search-result', onMouseOver);
                    rootConfig.$componentContainer.on('mouseleave.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-search-result', onMouseLeave);

                    rootConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-search-input', onSearchClick);
                    rootConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-search-input--mobile', onSearchClick);

                    rootConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-search-clear', onSearchClear);
                    rootConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-popover--search-mobile .mkts-cmpt-svgcht-search-clear', onMobileSearchClear);

                    rootConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-search-result', onResultClick);

                }
            };

            return AutoComplete;
        }
        return model;
    });