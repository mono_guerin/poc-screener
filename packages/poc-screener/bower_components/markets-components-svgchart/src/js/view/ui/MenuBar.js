define(["../../../html/menuBar/menuBarTemplate.html",
        "../../../html/menuBar/popoverEventsIndicatorsTemplate.html",
        "../../../html/menuBar/popoverFundamentalTemplate.html",
        "../../../html/menuBar/popoverDrwaingTemplate.html",
        "../../../html/menuBar/popoverDisplayTemplate.html",
        "../../../html/menuBar/gear-drill-down.html",
        "../../core/core",
        "../../util/util",
        "morningstar",
        "./MenuBarInterval.js",
        "./autoComplete.js",
        "./popover.js"
    ],
    function(menuBarTemplate,
        popoverEventsIndicatorsTemplate,
        popoverFundamentalTemplate,
        popoverDrwaingTemplate,
        popoverDisplayTemplate,
        settingsTemplate,
        core,
        util,
        morningstar,
        MenuBarIntervalModel,
        AutoCompleteModel,
        PopoverModel) {
        "use strict";

        /**
         * Menubar for main chart type view
         *
         * @constructor
         */

        var asterixUtil = morningstar.asterix.util;
        var $ = core.$;
        var invalidClass = 'mkts-cmpt-svgcht-invalid';
        var invalidLabelClass = 'mkts-cmpt-svgcht-invalid-label';

        function model(managers) {
            var eventManager = managers.eventManager;
            var CCM = managers.chartConfigManager;
            var MenuBarInterval = MenuBarIntervalModel(managers);
            var Popover = PopoverModel();
            var AutoComplete = AutoCompleteModel(managers, Popover);
            var $menuBarTemplate;
            var langObj;
            var appConfig;

            /*TEMPORARY UNTIL THIS ASTERIX PULL REQUEST IS MERGED*/
            asterixUtil.isObj = function(o) {
                return typeof o === 'object' && o !== null;
            };

            var MenuBar = function(config) {
                this.config = {
                    container: '',
                    popObj: {},
                    popConfig: '',
                    popName: '',
                };
                this._init(config);
            };

            MenuBar.prototype = {
                /* test-code */
                __private: {
                    CCM: CCM,
                    eventManager: eventManager
                },
                /* end-test-code */
                _init: function(config) {
                    function makePopOvers() {
                        var templates = {
                            eventsIndicators: popoverEventsIndicatorsTemplate, //eventsIndicatorsTemplate,
                            fundamentals: popoverFundamentalTemplate, //fundamentalTemplate,
                            drawings: popoverDrwaingTemplate, //drawingTemplate,
                            display: popoverDisplayTemplate, //displayTemplate
                        };
                        var popovers = {};
                        var buttons = $menuBarTemplate.find('.mkts-cmpt-svgcht-menubtn');
                        var buttonLen = buttons.length;
                        var name, button, tempFun, markupStr, i;

                        for (i = 0; i < buttonLen; i++) {
                            button = buttons[i];
                            name = button.getAttribute('data-menuid');
                            name = util.dashesToCamel(name);
                            button.popName = name;
                            tempFun = templates[name];
                            var tempData = langObj;
                            if (name === 'display') {
                                tempData.showStartDate = CCM.getConfig().showStartDate;
                                tempData.showPriceWithDividendGrowth = CCM.getConfig().showPriceWithDividendGrowth;
                            }
                            markupStr = tempFun(tempData);
                            var $markupStr = $(markupStr);
                            if (name === 'drawings') {
                                button.resetOnOpen = true;
                            } else if (name === 'display') {
                                var configArr = [
                                    (function(mainChart) {
                                        var MainChartTypes = CCM.MainChartTypes;
                                        var MainDataTypes = CCM.MainDataTypes;
                                        var dataType = mainChart.dataType;
                                        if (dataType === MainDataTypes.Growth) {
                                            return MainChartTypes.pGrowthChart;
                                        } else if (dataType === MainDataTypes.Growth10K) {
                                            return MainChartTypes.TenKGrowthChart;
                                        } else if (dataType === MainDataTypes.DividendEffect) {
                                            return MainChartTypes.DividendEffectChart;
                                        } else if (dataType === MainDataTypes.PostTax) {
                                            return MainChartTypes.PostTaxReturnChart;
                                        } else if (dataType === MainDataTypes.PriceWithDividendGrowth) {
                                            return MainDataTypes.PriceWithDividendGrowth;
                                        } else {
                                            return mainChart.chartType;
                                        }
                                    })(appConfig.charts.mainChart),
                                    appConfig.cursorType,
                                    appConfig.verticalScaleType,
                                    appConfig.charts.mainChart.selectionMode,
                                    appConfig.charts.mainChart.yAxis.orient,
                                    appConfig.charts.volumeChart.chartType,
                                    appConfig.startDate
                                ];
                                if (appConfig.showPreAfter) {
                                    configArr.push("extendedMarketHour");
                                }

                                configArr.forEach(function(item) { setOptionsStatus(item, $markupStr) });
                            } else if (name === 'eventsIndicators' || name === 'fundamentals') {
                                $markupStr.find(".mkts-cmpt-svgcht-toggle--off,.mkts-cmpt-svgcht-toggle--on").each(function() {
                                    if ($(this).find("div").length === 0) {
                                        var wraper = '<div class="mkts-cmpt-svgcht-indicator-name" title="' + $(this).text() + '"></div>';
                                        $(this).wrapInner(wraper);
                                    }
                                    var onoffBtn = '<div class="mkts-cmpt-svgcht-onoff-btn"> <span class="onoff-left"></span><span class="onoff-right"></span></div>';
                                    $(this).append(onoffBtn);
                                });
                                var defaultOptions = {};
                                if (name === 'eventsIndicators') {
                                    defaultOptions["events"] = appConfig.events || [];
                                    defaultOptions["indicators"] = appConfig.indicators || [];
                                    //handle if the volume is active
                                    if (appConfig.charts.volumeChart.show === false) {
                                        $markupStr.find(".volume-chart").removeClass("mkts-cmpt-svgcht-toggle--on").addClass("mkts-cmpt-svgcht-toggle--off");
                                    }
                                } else if (name === 'fundamentals') {
                                    defaultOptions["fundamentals"] = appConfig.fundamentals ? appConfig.fundamentals.slice() : [];
                                    appConfig.indicators.forEach(function(indicator) {
                                        if (CCM.isIndicatorsFundamentals(indicator.name)) {
                                            defaultOptions["fundamentals"].push(indicator.name);
                                        }
                                    });
                                }
                                setDefaultIndictFundm(defaultOptions, $markupStr);
                            }
                            popovers[name] = $markupStr;
                        }

                        return popovers;
                    }

                    function setOptionsStatus(optionValue, $markupStr) {
                        if (!optionValue) return;
                        var eid = util.camelToDashes(optionValue);
                        var elem = $markupStr.find("." + eid);
                        if (optionValue !== appConfig.charts.volumeChart.chartType && optionValue !== "extendedMarketHour") {
                            elem.addClass('active');
                        } else {
                            elem.removeClass('mkts-cmpt-svgcht-toggle--off').addClass('mkts-cmpt-svgcht-toggle--on');
                        }
                    }

                    function setDefaultIndictFundm(defaultOptions, $markupStr) {
                        var items = [];
                        var eid;
                        var events = defaultOptions.events ? defaultOptions.events : [];
                        for (var i = 0, eventsLength = events.length; i < eventsLength; i++) {
                            eid = events[i];
                            if (appConfig.charts.mainChart.showDividend === false && eid === 'eventDividend') {
                                continue;
                            }

                            $markupStr.find("." + eid).removeClass("mkts-cmpt-svgcht-toggle--off").addClass("mkts-cmpt-svgcht-toggle--on");
                            self.activeIndicators[events[i]] = {
                                el: $markupStr.find("." + eid)[0]
                            }
                        }

                        var indicators = defaultOptions.indicators ? defaultOptions.indicators : [];
                        var fundamentals = defaultOptions.fundamentals ? defaultOptions.fundamentals : [];
                        for (var i = 0, indicatorsLength = indicators.length, indicator; i < indicatorsLength; i++) {
                            indicator = indicators[i];
                            if (CCM.isIndicatorsFundamentals(indicator.name)) {
                                fundamentals.push(indicator.name);
                                continue;
                            }
                            eid = util.camelToDashes(indicator.name);
                            var $elm = $markupStr.find("." + eid).removeClass("mkts-cmpt-svgcht-toggle--off").addClass("mkts-cmpt-svgcht-toggle--on");
                            var $drawer = $markupStr.find("." + eid).next('.mkts-cmpt-svgcht-drawer, .mkts-cmpt-svgcht-drawer--expand');
                            $drawer.removeClass('mkts-cmpt-svgcht-drawer').addClass('mkts-cmpt-svgcht-drawer--expand');
                            var $input = $drawer.find('.mkts-cmpt-svgcht-dropdown input');
                            if (indicator.parameters) {
                                indicator.parameters.forEach(function(item, idx) {
                                    $input.eq(idx).val(item);
                                });
                            }
                            $elm[0].toRemove = false;
                            self.activeIndicators[indicators[i].name] = {
                                el: $elm[0],
                                options: indicators[i].parameters
                            }
                        }

                        for (var i = 0, fundamentalsLength = fundamentals.length; i < fundamentalsLength; i++) {
                            eid = util.camelToDashes(fundamentals[i]);
                            $markupStr.find("." + eid).removeClass("mkts-cmpt-svgcht-toggle--off").addClass("mkts-cmpt-svgcht-toggle--on");
                            self.activeFundamentals[fundamentals[i]] = {
                                el: $markupStr.find("." + eid)[0],
                            }
                        }
                    }
                    var self = this;

                    /* test-code */
                    self.__private._init = {
                        makePopOvers: makePopOvers
                    };
                    /* end-test-code */

                    appConfig = CCM.getConfig();
                    var locale = appConfig.lang || 'en-US';
                    var isAutoCompleteActMainTicker = appConfig.autoComplete.action === 'main' ? true : false;

                    langObj = CCM.getLabels('markets-components-svgchart', locale);
                    langObj.autoCompleteChangeMainTicker = isAutoCompleteActMainTicker;
                    langObj.hideGear = appConfig.hideIndicator && appConfig.hideFundamental && appConfig.hideDrawing && appConfig.hideDisplay;
                    langObj.showExportMenuTab = appConfig.showExportMenuTab;
                    $.extend(true, self.config, config);

                    $menuBarTemplate = $(menuBarTemplate(langObj));

                    if (appConfig.enterTipsLabelID) {
                        var placeholder = isAutoCompleteActMainTicker ? langObj['changeTicker'] : langObj['searchAssets'];
                        $menuBarTemplate.find(".mkts-cmpt-svgcht-search-input").attr("placeholder", placeholder);
                    }


                    self._conditionalLoad();
                    self.activeIndicators = {};
                    self.activeFundamentals = {};
                    self.popovers = makePopOvers();
                    /* test-code */
                    self.__private.activeIndicators = self.activeIndicators;
                    self.__private.activeFundamentals = self.activeFundamentals;
                    /* end-test-code */

                    self._renderView();
                    self._hideMenuItemsAndSetPremium();
                    self._hideMenuIcons();
                    self._hideMenuButtons();
                    self._bindEvents();
                },
                hidePopover: function() {
                    Popover.hide();
                },
                _conditionalLoad: function() {
                    var testDiv = document.createElement('div');

                    //conditionally load overrides for browsers without flexbox (ie9)
                    $('body').append(testDiv);
                    if (testDiv.style.flexDirection === undefined && testDiv.style.webkitFlexDirection === undefined && testDiv.style.msFlexDirection === undefined) {
                        CCM.getConfig().$componentContainer.find(".mkts-cmpt-svgcht-menubar").addClass("mkts-cmpt-svgcht-noFlex");
                    }
                    $(testDiv).remove();

                },
                _hideMenuItemsAndSetPremium: function() {
                    function hideElements() {
                        var key = 'hide' + util.capFirstChar(curPop);
                        var hides = menubarCfg[key];
                        var i, l;

                        if (hides && $.isArray(hides)) {
                            l = hides.length;
                            for (i = 0; i < l; i++) {
                                $el.find('.' + hides[i]).hide();
                                //hide Title if all subItem is hidden
                                var $pEl = $el.find('.' + hides[i]).parent();
                                if ($pEl.prev().hasClass('mkts-cmpt-svgcht-popover-sub-title')) {
                                    var allHidden = true;
                                    $.each($pEl.children(".mkts-cmpt-svgcht-popover-body"), function(o, e) {
                                        if ($(e).css('display') !== 'none') {
                                            allHidden = false;
                                            return false;
                                        }
                                    });
                                }
                                if (allHidden) {
                                    $pEl.prev().hide();
                                }
                            }
                        }
                    }

                    function findPremium() {
                        function disablePremium(el) {
                            var $parEl = $(el.parentNode);

                            $parEl.removeClass('mkts-cmpt-svgcht-popover-body mkts-cmpt-svgcht-toggle--off')
                                .addClass('mkts-cmpt-svgcht-popover-body-disabled')
                                .attr('data-linkbinding', 'history-fairvalue');
                        }

                        function hidePremiumMark(el) {
                            var $el = $(el);
                            $el.css({
                                'background': 'none',
                                'padding-left': '0px'
                            });
                        }

                        var premItms = $el.find('.mkts-cmpt-svgcht-premium-chic');
                        var premLen = premItms.length;
                        var i;

                        if (!appConfig.premiumUser) {
                            for (i = 0; i < premLen; i++) {
                                disablePremium(premItms[i]);
                            }
                        } else {
                            for (i = 0; i < premLen; i++) {
                                hidePremiumMark(premItms[i]);
                            }
                        }

                    }
                    var self = this;
                    var menubarCfg = appConfig.menubar;
                    var popovers = self.popovers;
                    var curPop, $el;


                    //conditionally hide items for listed in config
                    for (curPop in popovers) {
                        if (popovers.hasOwnProperty(curPop)) {
                            $el = self.popovers[curPop];
                            hideElements();
                            findPremium();
                        }
                    }

                    //hide interval items
                    curPop = 'Interval';
                    $el = $menuBarTemplate;
                    hideElements();

                    if (appConfig.showCloseBtn) {
                        $menuBarTemplate.find('.mkts-cmpt-svgcht-menuicn--remove').show();
                    }
                },
                _hideMenuIcons: function() {
                    var resetFlag = appConfig.hideResetIcon;
                    var hideExport = appConfig.hideExport;
                    var hidePrint = appConfig.hidePrint;
                    if (resetFlag) {
                        $menuBarTemplate.find('.mkts-cmpt-svgcht-menuicn--reset').hide();
                    }
                    if (hideExport) {
                        $menuBarTemplate.find('.mkts-cmpt-svgcht-menuicn--export').hide();
                    }
                    if (hidePrint) {
                        $menuBarTemplate.find('.mkts-cmpt-svgcht-menuicn--print').hide();
                    }
                },
                _hideMenuButtons: function() {
                    var buttons = appConfig.menubar.hideButtons;
                    var l, i;

                    if (asterixUtil.isArray(buttons) && buttons.length > 0) {
                        l = buttons.length;
                        for (i = 0; i < l; i++) {
                            $menuBarTemplate.find('.' + buttons[i]).hide();
                        }
                    }
                },
                _renderView: function(defaultRange) {
                    var intervalInst;
                    this.$el = this.config.container;
                    $menuBarTemplate.appendTo(this.$el);
                    intervalInst = new MenuBarInterval({
                        container: this.$el.find('.mkts-cmpt-svgcht-menubar-bottom')
                    });

                    this.intervalInst = intervalInst;
                    /* test-code */
                    this.__private.menuIntervalInst = intervalInst;
                    /* test-code */

                    new AutoComplete({ $menuBarContainer: this.$el });
                },
                _bindEvents: function() {
                    var self = this;
                    var popObj = this.config.popObj;
                    var popConfig = this.config.popConfig;
                    var popName = this.config.popName;
                    var timer;

                    function onMenuButtonClick(e) {
                        e.preventDefault();
                        e.stopPropagation();

                        if (asterixUtil.isString(this.popName)) {

                            popName = this.popName;
                            popObj.$el = self.popovers[this.popName];
                            if (this.resetOnOpen) {
                                popObj.$el.find('.mkts-cmpt-svgcht-popover-body').removeClass('active');
                            }

                            popConfig = {
                                $container: appConfig.$container,
                                $target: $(this),
                                styleClass: 'mkts-cmpt-svgcht-popover--' + this.popName,
                                $componentContainer: appConfig.$componentContainer,
                                $menuBarContainer: self.$el,
                                sync: appConfig.$container.width() < 601 //todo: need to find the unify process to check mobile size
                            };
                            Popover.show(popObj, popConfig);

                            /*
                             * popName is eventsIndicators|fundamental|drawing|display
                             */
                            eventManager.trigger(eventManager.EventTypes.triggerClickCallback, {
                                name: popName
                            });
                        }
                    }

                    function changeLayout() {
                        if (popObj != null && popConfig != "") {
                            if (asterixUtil.isString(popName)) {
                                popConfig = {
                                    $container: self.$el,
                                    $target: $(this),
                                    styleClass: 'mkts-cmpt-svgcht-popover--' + popName
                                };
                                if (popConfig.$container.find('.' + popConfig.styleClass).length > 0) {
                                    Popover.adjustPosition();
                                }
                            }
                        }
                    }

                    function onLayoutChange() {
                        clearTimeout(timer);
                        timer = setTimeout(changeLayout, 100);
                    }

                    function onSettingsButtonClick(e) {
                        var name = this.getAttribute('data-menuid');
                        var target = self.$el.find('.mkts-cmpt-svgcht-menuicn--gear');

                        if (target.length > 0) {
                            name = util.dashesToCamel(name);
                            target.popName = name;
                            onMenuButtonClick.call(target, e);
                        }
                        /*
                         * popName is eventsIndicators|fundamental|drawing|display
                         */
                        eventManager.trigger(eventManager.EventTypes.triggerClickCallback, {
                            name: popName
                        });
                    }

                    function onSettingsClick(e) {
                        var popObj = {};
                        var popConfig;

                        e.preventDefault();
                        e.stopPropagation();

                        $.extend(langObj, {
                            hideIndicatorInGear: appConfig.hideIndicator,
                            hideFundamentalInGear: appConfig.hideFundamental,
                            hideDrawingInGear: appConfig.hideDrawing,
                            hideDisplayInGear: appConfig.hideDisplay
                        });

                        popObj.$el = $(settingsTemplate(langObj));
                        popConfig = {
                            $container: appConfig.$container,
                            $target: $(this),
                            styleClass: 'mkts-cmpt-svgcht-popover--settings',
                            $componentContainer: appConfig.$componentContainer,
                            $menuBarContainer: self.$el,
                            sync: false
                        };
                        Popover.show(popObj, popConfig);
                        eventManager.trigger(eventManager.EventTypes.triggerClickCallback, {
                            name: 'gear'
                        });
                    }

                    function makeArrayFromInputs(el) {
                        var ar = [];
                        var inpts, inptLen, i;

                        if (asterixUtil.isObj(el) && el.length > 0) {
                            inpts = $(el).find('input');
                            inptLen = inpts.length;
                            for (i = 0; i < inptLen; i++) {
                                if (asterixUtil.isString(inpts[i].value) && inpts[i].value !== '') {
                                    var numValue = parseFloat(inpts[i].value, 10);
                                    if (!isNaN(numValue)) {
                                        ar.push(parseFloat(numValue));
                                    }
                                } else {
                                    ar.push(null);
                                }
                            }
                        }

                        return ar;
                    }

                    function toggleDrawer(el) {
                        var drawer = $(el).next('.mkts-cmpt-svgcht-drawer, .mkts-cmpt-svgcht-drawer--expand');

                        if (el.toRemove === undefined || el.toRemove) {
                            el.toRemove = false;
                        } else {
                            el.toRemove = true;
                        }

                        $(el).toggleClass('mkts-cmpt-svgcht-toggle--on mkts-cmpt-svgcht-toggle--off');
                        $(drawer).toggleClass('mkts-cmpt-svgcht-drawer mkts-cmpt-svgcht-drawer--expand');
                        if (drawer.length > 0) {
                            if (el.className.indexOf('on') > 0) {
                                $(el).addClass('noBorder');
                            } else {
                                $(el).removeClass('noBorder');
                            }
                        }
                    }

                    function onMenuItemClick(e) {
                        function manageVolumeState() {
                            var popovers = self.popovers;
                            var toggleString = 'mkts-cmpt-svgcht-toggle--on mkts-cmpt-svgcht-toggle--off';
                            var $vol = popovers.eventsIndicators.find('.volume-chart');
                            var $volPlus = popovers.display.find('.volume-plus-chart');

                            if (eventType === 'ChangeChartVolume') {
                                if ($vol.hasClass('mkts-cmpt-svgcht-toggle--off') && $volPlus.hasClass('mkts-cmpt-svgcht-toggle--on')) {
                                    $volPlus.toggleClass(toggleString);
                                    eventManager.trigger(eventManager.EventTypes.ChangeChartVolumePlus, false);
                                    console.debug(eventManager.EventTypes.ChangeChartVolumePlus, false);
                                }
                            } else if (eventType === 'ChangeChartVolumePlus') {
                                if ($volPlus.hasClass('mkts-cmpt-svgcht-toggle--on') && $vol.hasClass('mkts-cmpt-svgcht-toggle--off')) {
                                    $vol.toggleClass(toggleString);
                                    eventManager.trigger(eventManager.EventTypes.ChangeChartVolume, true);
                                    console.debug(eventManager.EventTypes.ChangeChartVolume, true);
                                }
                            }
                        }

                        function manageVolumeValue() {
                            eventVal = false;
                            if (el.className.indexOf('--on') > 0) {
                                eventVal = true;
                            }
                        }

                        function processElement() {
                            if ($(el).attr("id") === "reset") { return; }

                            var items = $(parent).find('.mkts-cmpt-svgcht-popover-body');
                            if (toggle) {
                                toggleDrawer(el);
                            } else {
                                $(items).removeClass('active');
                                $(el).addClass('active');
                            }
                        }

                        function assignActiveIndicators() {
                            inputContainer = $(el).next('.mkts-cmpt-svgcht-drawer, .mkts-cmpt-svgcht-drawer--expand');
                            if (el.toRemove === false) {
                                indicatorArgs = makeArrayFromInputs(inputContainer);
                                self.activeIndicators[eventVal] = {
                                    el: el,
                                    options: indicatorArgs
                                };
                            }
                        }

                        function assingActiveFundamentals() {
                            if (el.toRemove === false) {
                                self.activeFundamentals[eventVal] = {
                                    el: el
                                };
                            }
                        }

                        function fireNormalEvent(flag) {
                            if (window._satellite) {
                                try {
                                    _satellite.setVar('CategoryName', eventManager.EventTypes[eventType]);
                                    _satellite.setVar('EventVal', eventVal);
                                    _satellite.track('DropDown');
                                } catch (e) {}
                            }
                            eventManager.trigger(eventManager.EventTypes[eventType], eventVal);
                            console.debug(eventManager.EventTypes[eventType], eventVal);
                            eventVal = flag ? el.getAttribute('data-menuid').replace(/-chart/, '').replace(/-(\w)/g, function($0, $1) { return $1.toUpperCase() }) : eventVal;
                            eventManager.trigger(eventManager.EventTypes.triggerClickCallback, {
                                name: popName,
                                value: eventVal
                            });
                        }

                        function fireParameteredEvent() {
                            inputContainer = $(el).next('.mkts-cmpt-svgcht-drawer, .mkts-cmpt-svgcht-drawer--expand');
                            if (el.toRemove === false) {
                                indicatorArgs = makeArrayFromInputs(inputContainer);
                                self.activeIndicators[eventVal] = {
                                    el: el,
                                    options: indicatorArgs
                                };
                            }
                            if (window._satellite) {
                                try {
                                    _satellite.setVar('CategoryName', eventManager.EventTypes.ChangeIndicators);
                                    _satellite.setVar('EventVal', eventVal);
                                    _satellite.track('DropDown');
                                } catch (e) {}
                            }
                            eventManager.trigger(eventManager.EventTypes.ChangeIndicators, el.toRemove, eventVal, indicatorArgs);
                            console.debug(eventManager.EventTypes.ChangeIndicators, el.toRemove, eventVal, indicatorArgs);

                            eventManager.trigger(eventManager.EventTypes.triggerClickCallback, {
                                name: popName,
                                value: eventVal,
                                params: indicatorArgs
                            });
                        }

                        function hidePopoverAfterSelect() {
                            if (appConfig.isHidePopoverAfterSelect) {
                                self.hidePopover();
                            }
                        }

                        function processEventType() {
                            switch (eventType) {
                                case 'ChangeFundamental':
                                    eventVal = util.capFirstChar(eventVal);
                                    assingActiveFundamentals();
                                    fireNormalEvent();
                                    hidePopoverAfterSelect();
                                    break;
                                case 'ChangeIndicators':
                                    eventVal = util.capFirstChar(eventVal);
                                    assignActiveIndicators();
                                    fireParameteredEvent();
                                    hidePopoverAfterSelect();
                                    break;
                                case 'ChangeChartVolumePlus':
                                    //intentional fall through, both event types should be handled the same
                                case 'ChangeChartVolume':
                                    manageVolumeState();
                                    manageVolumeValue();
                                    hidePopoverAfterSelect();
                                    fireNormalEvent(true);
                                    break;
                                default:
                                    fireNormalEvent();
                                    hidePopoverAfterSelect();
                            }
                        }
                        var el = this;
                        var parent = el.parentNode;
                        var eventType;
                        var eventVal;
                        var inputContainer;
                        var toggle = el.className.indexOf('mkts-cmpt-svgcht-toggle') > 0;
                        var indicatorArgs = [];

                        e.preventDefault();
                        e.stopPropagation();

                        if (asterixUtil.isString(el.getAttribute('data-menuid')) && asterixUtil.isObj(parent) && asterixUtil.isString(parent.getAttribute('data-menuid'))) {
                            eventType = util.dashesToCamel(parent.getAttribute('data-menuid'));
                            eventType = util.capFirstChar(eventType);
                            eventVal = util.dashesToCamel(el.getAttribute('data-menuid'));
                            processElement();
                            processEventType();
                        }
                    }

                    function onIndicatorChagne(remove, indicator, options, removeAll, isFromCloseBtn) {
                        function allNull(array) {
                            var isNull = true;
                            var len = array.length || 0;
                            var i;

                            for (i = 0; i < len; i++) {
                                if (array[i] !== null) {
                                    isNull = false;
                                    break;
                                }
                            }

                            return isNull;
                        }

                        function updateActiveIndicators() {
                            indicatorArray.splice(index, 1);
                            if (indicatorArray.length === 0 || allNull(indicatorArray) || removeAll) {
                                toggleDrawer(indicatorEl);
                            }
                        }

                        function removeValue() {
                            if (asterixUtil.isObj(self.activeIndicators[indicator])) {
                                indicatorArray = self.activeIndicators[indicator].options;
                                indicatorEl = self.activeIndicators[indicator].el;
                                if (isFromCloseBtn && asterixUtil.isNumber(options)) {
                                    index = indicatorArray.indexOf(options);
                                    if (index > -1) {
                                        //if clearing individual fields as they're cleared from the chart is requested, this is where that function should be called
                                        //get an array of the input fields, and find the field from this index, then set it to ''
                                        updateActiveIndicators();
                                    }
                                } else if (isFromCloseBtn) {
                                    updateActiveIndicators();
                                }
                            }
                        }
                        var indicatorArray, indicatorEl, index;
                        if (remove) {
                            removeValue();
                        };
                    }

                    function onFundamentalChagne(fun, remove) {
                        var funEl;

                        if (remove === true) {
                            if (asterixUtil.isObj(self.activeFundamentals[fun])) {
                                funEl = self.activeFundamentals[fun].el;
                                toggleDrawer(funEl);
                            }
                        }
                    }

                    function onUpdateIndicator() {
                        var inputContainer = $(this).closest('.mkts-cmpt-svgcht-drawer--expand');
                        var el = inputContainer.prev('.mkts-cmpt-svgcht-popover-body')[0];
                        var parent = el.parentNode;
                        var eventType;
                        var eventVal;
                        var indicatorArgs;
                        var $invalidInputs = inputContainer.find('.' + invalidClass);
                        if ($invalidInputs.length > 0) {
                            $invalidInputs.animate({ borderWidth: '5px' }, 'fast').animate({ borderWidth: '1px' }, 'fast').focus();
                        } else if (asterixUtil.isString(el.getAttribute('data-menuid')) &&
                            asterixUtil.isObj(parent) &&
                            asterixUtil.isString(parent.getAttribute('data-menuid'))) {
                            eventType = util.dashesToCamel(parent.getAttribute('data-menuid'));
                            eventType = util.capFirstChar(eventType);
                            eventVal = util.dashesToCamel(el.getAttribute('data-menuid'));
                            eventVal = util.capFirstChar(eventVal);
                            indicatorArgs = makeArrayFromInputs(inputContainer);
                            self.activeIndicators[eventVal] = {
                                el: el,
                                options: indicatorArgs
                            };

                            eventManager.trigger(eventManager.EventTypes.ChangeIndicators, false, eventVal, indicatorArgs);
                            console.debug(eventManager.EventTypes.ChangeIndicators, false, eventVal, indicatorArgs);
                        }
                    }

                    function onInputKeyPress(event) {
                        return;
                        /*var $target = $(event.currentTarget);
                        var inputType = $target.attr('data-inputtype');
                        var inputValid = false;
                        var inputValue = $target.val();
                        switch (inputType) {
                            case CCM.indicatorInputTypes.NecessaryPositiveInteger:
                                inputValid = /^[0-9]*[1-9][0-9]*$/.test(inputValue);
                                break;
                            default:
                                inputValid = (inputValue === '' || /^[0-9]*[1-9][0-9]*$/.test(inputValue));
                                break;
                        }

                        var invalidLabel = $target.parent().next('div.' + invalidLabelClass);
                        if (inputValid) {
                            $target.removeClass(invalidClass);
                            invalidLabel.remove();
                        } else {
                            $target.addClass(invalidClass);
                            if (invalidLabel.length === 0) {
                                $('<div>').addClass(invalidLabelClass)
                                    .html(langObj['invalid-input-' + (inputType || 'default')])
                                    .insertAfter($target.parent());
                            }
                        }*/
                    }

                    function onAutoCompleteClear() {
                        self.$el.find('.mkts-cmpt-svgcht-search-input').val('');
                        self.$el.find('.mkts-cmpt-svgcht-popover--search').remove();
                    }

                    function onMenuIconClick() {
                        if (asterixUtil.isString(this.getAttribute('data-menuid'))) {
                            var eventType = util.capFirstChar(this.getAttribute('data-menuid').replace('mkts-cmpt-svgcht-menuicn--', ''));

                            if (eventManager.EventTypes[eventType]) {

                                eventManager.trigger(eventManager.EventTypes[eventType]);

                                eventManager.trigger(eventManager.EventTypes.triggerClickCallback, {
                                    name: eventType
                                });
                            }
                        }
                    }

                    function onCloseButton() {
                        eventManager.trigger(eventManager.EventTypes.DestroyChart);
                        if ($.isFunction(appConfig.onClose)) {
                            appConfig.onClose({
                                component: 'markets-components-svgchart'
                            });
                        }
                    }

                    function destroyMenubar() {
                        appConfig.$componentContainer.off('.mkts-cmpt-svgcht-menu');
                    }

                    function onGrowthTypeClick(e) {
                        var el = this;
                        var $el = $(el);
                        var parent = el.parentNode;
                        var eventType;
                        var eventVal;
                        var inputContainer;
                        var indicatorArgs = [];

                        e.preventDefault();
                        e.stopPropagation();

                        if ($el.hasClass("active")) {
                            return;
                        }

                        var et = el.attributes['data-eventtype'].value;
                        eventType = util.dashesToCamel(et);
                        eventType = util.capFirstChar(eventType);
                        var ev = el.attributes['data-eventvalue'].value;
                        eventVal = util.dashesToCamel(ev);
                        if(eventVal!== eventManager.EventTypes.Export){
                            $el.siblings().removeClass('active');
                            $el.addClass('active');
                        }
                        eventManager.trigger(eventManager.EventTypes[eventType], eventVal);
                        console.debug(eventManager.EventTypes[eventType], eventVal);
                    }

                    function print() {
                        window.print();
                    }

                    function onResetRoomIn(e) {
                        e.preventDefault();
                        e.stopPropagation();
                        eventManager.trigger(eventManager.EventTypes.ResetZoomIn);
                    }

                    eventManager.bind(eventManager.EventTypes.ChangeIndicators, onIndicatorChagne);
                    eventManager.bind(eventManager.EventTypes.ChangeFundamental, onFundamentalChagne);
                    eventManager.bind(eventManager.EventTypes.LayoutChange, onLayoutChange);
                    eventManager.bind(eventManager.EventTypes.DestroyChart, destroyMenubar);
                    eventManager.bind(eventManager.EventTypes.Print, print);
                    appConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-menubtn', onMenuButtonClick);
                    appConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-menuicn--gear', onSettingsClick);
                    appConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-popover-menubtn', onSettingsButtonClick);
                    appConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-menuicn--remove', onAutoCompleteClear);
                    appConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-popover-body', onMenuItemClick);
                    appConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-popover-text.zoom-in-reset', onResetRoomIn);
                    appConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-menutab', onGrowthTypeClick);
                    appConfig.$componentContainer.on('keyup.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-text-box', onInputKeyPress);
                    appConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-btn-update', onUpdateIndicator);
                    appConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-menuicn', onMenuIconClick);
                    appConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-menuicn--remove', onCloseButton);
                }
            };

            return MenuBar;
        }

        return model;
    });