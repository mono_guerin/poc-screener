define(["../../../../html/menuBar/mui/intervalTemplate.html",
    "../../../../html/menuBar/mui/intervalSelectOption.html",
    "../../../core/core",
    "../../../util/util",
    "morningstar",
    'moment'],
    function (intervalTemplate,
        intervalSelectOption,
        core,
        util,
        morningstar,
        moment) {
    "use strict";

    /**
    * Menubar for main chart type view
    *
    * @constructor
    */
    var $ = core.$;
    var asterixUtil = morningstar.asterix.util;
    var isMobile = util.deviceType.mobile();
    var intervalsMapping = {
        '1D': ".one-day",
        '5D': ".five-day",
        '15D': ".fifteen-day",
        '1M': ".one-month",
        '3M': ".three-month",
        '6M': ".six-month",
        'YTD': ".ytd",
        '1Y': ".one-year",
        '3Y': ".three-year",
        '5Y': ".five-year",
        '10Y': ".ten-year",
        'MAX': ".max"
    };

    var model = function myfunction(managers) {
        var eventManager = managers.eventManager;
        var CCM = managers.chartConfigManager;
        var $intervalTemplate;
        var freqConfig = CCM.freqencyConfig;
        var IntervalTypes = CCM.IntervalTypes;
        var datePickerFixativeFormater = 'YYYY-MM-DD';
        var appConfig, isFundChart;


        var Interval = function (config) {
            this.LastIntervalType; //remain last interval type to change the frequency of "Max"
            this.config = {
                container: ''
            };
            this._init(config);
        };

        Interval.prototype = {
            /* test-code */
            __private: {
                CCM: CCM,
                eventManager: eventManager
            },
            /* end-test-code */
            _init: function (config) {
                function setDefaultRange() {
                    if (isFundChart) {
                        freqConfig['OneMonth'].fc = freqConfig['ThreeMonth'].fc = freqConfig['SixMonth'].fc;
                        freqConfig['OneMonth'].def = freqConfig['ThreeMonth'].def = freqConfig['SixMonth'].def;
                        $intervalTemplate.find('.one-day, .five-day, .fifteen-day').remove();
                    }

                    self.rangePreset = appConfig.intervalType;
                    var selector, hideIntervals = [];
                    for (var i = 0, len = appConfig.hideIntervals.length; i < len; i++) {
                        selector = intervalsMapping[appConfig.hideIntervals[i]];
                        selector && hideIntervals.push(selector);
                    }
                    $intervalTemplate.find(hideIntervals.join(',')).remove();
                    $intervalTemplate.find('.' + util.camelToDashes(self.rangePreset)).addClass('active');
                    self._updateFreq(self.rangePreset);
                }

                var self = this;
                appConfig = CCM.getConfig();
                isFundChart = appConfig.chartGenre === CCM.ChartGenre.FundChart;
                var locale = appConfig.lang || 'en-US';
                var langObj = CCM.getLabels('markets-components-svgchart', locale);

                self._clearRange = function () {
                    self.rangePreset = null;
                    this.$el.find('.mkts-cmpt-svgcht-segment-option').removeClass('active');
                };

                self._displayDate = function (date) {
                    var _m = date;
                    if (moment.isDate(_m)) {
                        _m = moment(_m);
                    }

                    var formater;
                    if ($.browser.mozilla || $.browser.msie) {
                        formater = appConfig.dailyDateFormatter;
                    }

                    return _m.format(formater || datePickerFixativeFormater);
                };

                self.updateDateButtonText = function(startDate,endDate){
                    var startDate = startDate;
                    var endDate = endDate;
                    if(!startDate){
                        startDate = self._displayDate(self.startMoment);
                        endDate = self._displayDate(self.endMoment);
                    }
                    var dateText = startDate+" - "+endDate;
                    self.$container.find('.mkts-cmpt-svgcht-menubtn--date .mkts-cmpt-svgcht-menubtn-text').html(dateText);
                }

                self._emitIntervalFreq = function () {
                    var freq = self.intervalFreq;
                    var interval, dateRange;

                    if (asterixUtil.isString(self.rangePreset)) {
                        interval = self.rangePreset;
                    } else {
                        dateRange = [self.startMoment.toDate(), self.endMoment.toDate()];
                    }
                    eventManager.trigger(eventManager.EventTypes.ChangeIDFConfig, interval, dateRange, freq);
                    console.debug(eventManager.EventTypes.ChangeIDFConfig, interval, dateRange, freq);
                };
                self._freqFromDates = function (start, end) {
                    var freqByDate = CCM.getFreqencyByDateRange([start, end], true);

                    //update frequency
                    self._clearRange();
                    self._updateFreq(freqByDate);
                };
                self._updateFreq = function (interval, innerCall) {
                    function makeOptions(config) {
                        var options = document.createDocumentFragment();
                        var option;
                        var conLen = config.length;
                        var i;
                        for (i = 0; i < conLen; i++) {
                            var templateCfg = {
                                isMobile: isMobile,
                                fc: config[i]
                            }
                            option = intervalSelectOption(templateCfg);
                            options.appendChild($(option)[0]);
                        }

                        return options;
                    }
                    var interval = interval || self.rangePreset;
                    var freqSelect = $intervalTemplate.find('.mkts-cmpt-svgcht-freq-input');
                    var fc = freqConfig[interval].fc;
                    var def = freqConfig[interval].def;
                    var options;
                    var defOpt;
                    var nfc = [];
                    var validFre = [];
                    for (var i = 0, l = fc.length, c; i < l; i++) {
                        c= fc[i];
                        if (isFundChart) {
                            if(isNaN(c.value)){
                                nfc.push(c);
                                validFre.push(c.value);
                            }
                        } else {
                            nfc.push(c);
                            validFre.push(c.value);
                        }
                    }

                    if ($.isArray(fc[def].value, validFre) < 0) {
                        def = nfc.length > 0 ? nfc[0].value : 'd';
                    }

                    if (self.interval !== interval) {
                        self.interval = interval;
                        options = makeOptions(nfc);
                        
                        if (isMobile) {
                            $(freqSelect).empty().append(options);
                            defOpt = $(freqSelect).find('option')[def];
                            $(defOpt).attr('selected', 'selected');
                        } else {
                            var $ulFreq = $intervalTemplate.find(".ul-freq-dropdown-list");
                            $ulFreq.empty().append(options);
                            var $selectli = $($ulFreq.find('li')[def]);
                            $(freqSelect).text($selectli.find('span').text()).data("value",$selectli.data("value"));
                            $selectli.attr('data-selected', 'selected');
                        }
                    }

                    eventManager.trigger(eventManager.EventTypes.ChangeFrequencyItems, nfc, def);
                    console.debug(eventManager.EventTypes.ChangeFrequencyItems, nfc, def);

                    self.onChangeFrequency(null, innerCall); //innerCall
                };
                langObj.isMobile = isMobile;
                $intervalTemplate = $(intervalTemplate(langObj));

                $.extend(true, self.config, config);

                self.lastMoment = self.startMoment = self.endMoment = moment().hours(0).minutes(0).seconds(0);
                self._bindEvents();
                setDefaultRange();
                self._renderView();
                self.updateDateButtonText();
            },

            _renderView: function () {
                this.$el = this.config.container;
                $intervalTemplate.appendTo(this.$el);
                this.$container = this.$el.parent();
            },
            _bindEvents: function () {
                var self = this;

                /**
                 * date string 2016-07-06
                 * return monment obj
                 */
                function momentFromDateString(dateString) {
                    var _momentObj;

                    if(dateString){
                        _momentObj = moment(dateString).hours(0).minutes(0).seconds(0);
                        if (_momentObj.isAfter(self.lastMoment)) {
                            _momentObj = self.lastMoment;
                        }
                    }
                    return _momentObj;
                }

                function datesHaveChanged(start, end) {
                    var _startMoment = start,
                        _endMoment = end,
                        _flag = false;
                    if (_startMoment) {
                        if (moment.isDate(_startMoment)) {
                            _startMoment = moment(_startMoment).hours(0).minutes(0).seconds(0);
                        }
                        if (!_startMoment.isSame(self.startMoment)) {
                            self.startMoment = _startMoment;
                            _flag = true;
                        }
                    }
                    if (_endMoment) {
                        if (moment.isDate(_endMoment)) {
                            _endMoment = moment(_endMoment).hours(0).minutes(0).seconds(0);
                        }
                        if (!_endMoment.isSame(self.endMoment)) {
                            self.endMoment = _endMoment;
                            _flag = true;
                        }
                    }

                    if (self.startMoment.isAfter(self.endMoment)) {//switch start end if start < end
                        var temp = self.endMoment;
                        self.endMoment = self.startMoment;
                        self.startMoment = temp;
                        self.updateDateButtonText(self._displayDate(self.startMoment), self._displayDate(self.endMoment));
                    }

                    return _flag;
                }

                function changeStartEnd(start, end) {
                    if (datesHaveChanged(start, end)) {
                        self._freqFromDates(self.startMoment.toDate(), self.endMoment.toDate());
                    }
                }

                function changeCalendars(start, end) {
                    if (datesHaveChanged(start, end)) {
                        self.updateDateButtonText(self._displayDate(self.startMoment), self._displayDate(self.endMoment));
                    }

                    if (self.rangePreset === 'Max') {
                        var freqByDate = CCM.getFreqencyByDateRange([self.startMoment.toDate(), self.endMoment.toDate()], true);
                        //update frequency
                        self._updateFreq(freqByDate);
                    }
                }

                function onChangeFrequency(e, innerCall) {
                    var value, $target, $container = self.$container || $intervalTemplate;
                    if (e) {
                        $target = $(e.currentTarget);
                    } else {
                        if (isMobile) {
                            $target = $container.find('.mkts-cmpt-svgcht-freq-input').eq(0);
                        } else {
                            $target = $container.find("li[data-selected='selected']");
                        }
                    }

                    if (!isMobile) {
                        $container.find(".ul-freq-dropdown-list").hide();
                        if ($target && $target.find("span")) {
                            var targetText = $target.find("span").text();
                            $container.find('.mkts-cmpt-svgcht-freq-input')
                                .data("value", targetText)
                                .text(targetText);
                        }
                        value = $target.data("value") || '';
                    } else {
                        value = $target.val() || '';
                        $target.parent().removeClass('open'); //easiest way to clear down arrow when an option is selected
                        $target.blur();
                    }

                    if (typeof (value) !== "object" && value !== '') {
                        if (!innerCall && self.rangePreset === IntervalTypes.OneDay) {
                            self.oneDayCanAdjust = false;
                        }
                        self.intervalFreq = value;
                        self._emitIntervalFreq();
                    }
                }

                function filterRenderEvents(dates) {
                    updateDatesFromMainChart(dates, true);
                }

                function updateDatesFromMainChart(dates, suppressClearRange) {
                    if (datesHaveChanged(dates[0], dates[1])) {
                        self.updateDateButtonText(self._displayDate(self.startMoment), self._displayDate(self.endMoment));
                        if (!suppressClearRange) {
                            self._clearRange();
                        }
                    }
                }

                function updateFreqAndMainChart(dates) {
                    self._freqFromDates(dates[0], dates[1]);
                }

                function onDateIntervalClick(innerCall, el) {
                    function clearActive() {
                        self.$container.find('.mkts-cmpt-svgcht-segment-option').removeClass('active');
                    }
                    var id, eventVal;
                    var el = this || el;
                    if (asterixUtil.isObj(el) && asterixUtil.isString(el.getAttribute('data-menuid'))) {
                        id = el.getAttribute('data-menuid');
                        eventVal = util.dashesToCamel(id);
                        eventVal = util.capFirstChar(eventVal);

                        updateDateIntervalByRange(innerCall, eventVal);
                        
                        clearActive();
                        $(el).addClass('active');

                    }
                    $(el).blur();
                }

                function changeDateByDateRange(data){
                    var innerCall = false;
                    var isCustomDate = false;
                    if( data["startDate"] || data["endDate"]) {
                        isCustomDate = true;
                    }
                    // appConfig.maPaapiPath = "//admadevwb8005:3080/paapi";

                    // custom date, user input
                    if(isCustomDate) {
                        var startDateValue = data["startDate"]["value"];
                        var endDateValue = data["endDate"]["value"];

                        var isStartDateIsStatic = util.isStaticDate(startDateValue);
                        var isEndDateIsStatic = util.isStaticDate(endDateValue);

                        if(isStartDateIsStatic&&isEndDateIsStatic){
                            changeStartEndByDateRange(startDateValue, endDateValue);
                        } else {
                            var dataJson = JSON.stringify({
                                15: isStartDateIsStatic ? '' : startDateValue,
                                16: isEndDateIsStatic ? '' : endDateValue,
                            });
                            $.ajax({
                                type: 'POST',
                                url: appConfig.maPaapiPath+"/v1/dateRange",
                                data: dataJson,
                                contentType : 'application/json',
                                dataType: 'json',
                                success: function(data){
                                    var startDate = isStartDateIsStatic? startDateValue : data.data["startDate"];
                                    var endDate = isEndDateIsStatic? endDateValue : data.data["endDate"];
                                    changeStartEndByDateRange(startDate, endDate);
                                }
                            })
                        };
                        
                    } else { // date range, 1D,5D,1Y..
                        var eventVal = util.dashesToCamel(data["data-menuid"]);
                        eventVal = util.capFirstChar(eventVal);
                        updateDateIntervalByRange(innerCall, eventVal);
                    }

                    function changeStartEndByDateRange(startDate, endDate){
                        var startDateMomentObj = momentFromDateString(startDate);
                        var endDateMomentObj = momentFromDateString(endDate);
                        changeStartEnd(startDateMomentObj, endDateMomentObj);
                    }
                    
                }

                function updateDateIntervalByRange(innerCall, eventVal){
                    if (asterixUtil.isObj(freqConfig[eventVal])) {
                        if (!innerCall && eventVal === IntervalTypes.OneDay) {
                            self.oneDayCanAdjust = true;
                        }

                        self.rangePreset = eventVal;
                        //self.forcePickerUpdate = true;
                        if (eventVal !== 'Max') {
                            self.LastIntervalType = eventVal;
                            self._updateFreq(eventVal, true);
                        } else { //max should not update frequency options until after it gets the max date range from mainchart
                            CCM.getRealDateRange(eventVal, undefined, 'm', function (dates) {
                                if (self.rangePreset === 'Max') {
                                    if (datesHaveChanged(dates[0], dates[1]) || self.LastIntervalType != eventVal) {
                                        self.LastIntervalType = eventVal;
                                        self.updateDateButtonText(self._displayDate(self.startMoment), self._displayDate(self.endMoment));
                                        var _interval = CCM.getFreqencyByDateRange([self.startMoment.toDate(), self.endMoment.toDate()], true);
                                        self._updateFreq(_interval);
                                    }
                                }
                            });
                        }
                    }
                }

                function onGotoCustomDateClick(){
                    var $dateRange = self.$container.find('.mkts-cmpt-svgcht-popover-dateRange');
                    var $customDate = self.$container.find('.mkts-cmpt-svgcht-popover-customdate');

                    $dateRange.hide();
                    $customDate.show();
                }

                function onCustomDateBackClick(){
                    var $dateRange = self.$container.find('.mkts-cmpt-svgcht-popover-dateRange');
                    var $customDate = self.$container.find('.mkts-cmpt-svgcht-popover-customdate');

                    $dateRange.show();
                    $customDate.hide();
                }

                function changeOneDayFrequency(isChange) {
                    //if user have manuly change frequency, we can't change again.
                    if (self.oneDayCanAdjust !== false) {
                        if (isChange) {
                            freqConfig.OneDay.def = 1;
                            changeView(1);
                        } else {
                            freqConfig.OneDay.def = 0;
                            changeView(5);
                        }
                    }
                }

                /*
                *   @freqValue: the frequency value before change.
                */
                function changeView(freqValue) {
                    if (self.rangePreset === IntervalTypes.OneDay && (!self.intervalFreq || self.intervalFreq === freqValue)) {
                        self.interval = null;   //otherwise, it don't update the view.
                        var target = self.$container.find(".one-day")[0];
                        onDateIntervalClick.call(target, true);  //innerCall 
                    }
                }

                function changeInterval(interval) {
                    var target = intervalsMapping[interval] && self.$container.find(intervalsMapping[interval])[0];
                    if (target)
                        onDateIntervalClick.call(target, true);
                }

                self.onChangeFrequency = onChangeFrequency;
                // if (!isMobile) {
                //     eventManager.bind(eventManager.EventTypes.LayoutChange, hideFrequencyList);
                // }
                eventManager.bind(eventManager.EventTypes.ChangeOneDayFrequency, changeOneDayFrequency, self);
                eventManager.bind(eventManager.EventTypes.UpdateMenuInterval, updateDatesFromMainChart);
                eventManager.bind(eventManager.EventTypes.UpdateMenuIntervalForRender, filterRenderEvents);
                eventManager.bind(eventManager.EventTypes.RequestFrequencyFromMenu, updateFreqAndMainChart);
                eventManager.bind(eventManager.EventTypes.ChangeInterval, changeInterval);
                eventManager.bind(eventManager.EventTypes.ChangeDateByDateRange, changeDateByDateRange);
                eventManager.bind(eventManager.EventTypes.ChangeFrequencyFromMenu, onChangeFrequency);

                // appConfig.$componentContainer.on('change.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-freq-input', onChangeFrequency);
                // appConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.change-time-range .mkts-cmpt-svgcht-segment-option', onDateIntervalClick);
                // appConfig.$componentContainer.on("click.mkts-cmpt-svgcht-menu", ".ul-freq-dropdown-list li", onChangeFrequency);
                appConfig.$componentContainer.on("click.mkts-cmpt-svgcht-menu", ".mkts-cmpt-svgcht-popover-body.customdate", onGotoCustomDateClick);
                appConfig.$componentContainer.on("click.mkts-cmpt-svgcht-menu", ".mkts-cmpt-svgcht-popover-customdate-back", onCustomDateBackClick);
            }
        };

        return Interval;
    }

    return model;
});

