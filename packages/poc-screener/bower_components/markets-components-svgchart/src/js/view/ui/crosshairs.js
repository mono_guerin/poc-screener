﻿define(['moment', '../../core/core', 'morningstar', '../../util/util', '../../../html/crosshairs/popoverOHLCVTemplate.html', '../../../html/crosshairs/popoverGrowthTemplate.html', '../../../html/crosshairs/popoverPerformancesGrowthTemplate.html', '../../../html/crosshairs/popoverPriceGrowthTemplate.html', '../../../html/crosshairs/popoverSimpleTemplate.html'],
    function(moment, core, morningstar, util, popoverOHLCVTemplate, popoverGrowthTemplate, popoverPerformancesGrowthTemplate, popoverPriceGrowthTemplate, popoverSimpleTemplate) {
        'use strict';

        var d3 = core.d3;
        var $ = core.$;
        var Util = core.Util;
        var model = function(managers) {
            var chartConfigManager = managers.chartConfigManager;
            var eventManager = managers.eventManager;
            var target;
            var locale = 'en-US';
            var chartGenre, decimalPlaces;
            var rootConfig, ChartGenre;
            var isFundChart = false;
            var MainDataTypes = chartConfigManager.MainDataTypes;
            var PopoverTypes = chartConfigManager.PopoverTypes;
            var numberFormat = function(d, formatLabel, isVolume, decimal, showOriginalDecimal) {
                var _dec = $.isNumeric(decimal) ? decimal : ($.isNumeric(decimalPlaces) ? decimalPlaces : rootConfig.defaultDecimal);
                var needMark = false;
                var isFormat = rootConfig.isFormat;
                while (Math.abs(d) * Math.pow(10, _dec) < 0.5 && d != 0) {
                    _dec++;
                }
                if (isVolume) {
                    _dec = (d > 1000000) ? rootConfig.volumeDecimal : 0;
                    needMark = (d > 1000000) ? true : false;
                    isFormat = (d > 1000000) ? true : isFormat;
                }
                if (rootConfig.crosshairs && rootConfig.crosshairs[formatLabel]) {
                    needMark = rootConfig.crosshairs[formatLabel].needMark || needMark;
                    _dec = rootConfig.crosshairs[formatLabel].decimal || _dec;
                }
                d = util.numberFormatter(locale, d, _dec, ' ', needMark, isFormat, showOriginalDecimal);
                return d;
            };
            var _off = false,
                _lineH,
                _lineV,
                _popover,
                _maxWidth = 0,
                _maxHeight = 0,
                _popoverWidth = 0,
                _popoverHeight = 0,
                _lineHHeight = 0,
                _lineVWidth = 0,
                _lineVOffsetTop = 0,
                _lang;

            var assembleOHLCV = function(data, lang, chartType) {
                var frequency = data.frequency;
                var valueChange = data['valueChange'];
                var valueChangePercent = data['valueChangePercent'];
                valueChange = isNaN(valueChange) ? lang['crosshairsPopoverNoData'] : valueChange.toFixed(decimalPlaces);
                valueChangePercent = isNaN(valueChangePercent) ? lang['crosshairsPopoverNoData'] : valueChangePercent.toFixed(decimalPlaces);

                var frequencyKey = util.getKeyByValueInObj(chartConfigManager.FrequencyTypes, frequency);
                frequencyKey = util.lowerFirstChar(frequencyKey);
                var valueChangeLabel = lang[frequencyKey + 'ValueChange'];
                var valueChangePercentLabel = lang[frequencyKey + 'ValueChangePercent'];;

                if (isFundChart) {
                    var label = lang['nav'];
                    var dataType = chartConfigManager.getMainDataType();
                    var ischartDataType = dataType === MainDataTypes.TotalReturn || dataType === MainDataTypes.MarketValue || dataType === MainDataTypes.PriceWithDividendGrowth;
                    if (ischartDataType) {
                        label = lang[dataType];
                    }

                    var value = data['value'] || data['close'];
                    if (dataType === MainDataTypes.PriceWithDividendGrowth && (chartType === 'main' || chartType === 'compare')) {
                        value = data['growthWithNav'];
                    }
                    var val = isNaN(value) ? lang['crosshairsPopoverNoData'] : numberFormat(value, ischartDataType ? dataType : 'nav', false, data.decimalPlaces);
                    if (rootConfig.crossCurrency && data['currency']) {
                        var currency;
                        if (rootConfig.crossCurrency === 'symbol') {
                            currency = Util.getCurrency(data['currency']);
                            if (currency.orient === 'right') {
                                val += ' ' + currency.symbol;
                            } else {
                                val = currency.symbol + ' ' + val;
                            }

                        } else {
                            currency = rootConfig.currencyAliases[data['currency']] || data['currency'];
                            val = currency + " " + val;
                        }

                    }
                    popoverConfig = {
                        'navValue': {
                            'label': label,
                            'value': val
                        }
                    }
                    if (rootConfig.showCrosshairChange) {
                        popoverConfig['valueChange'] = {
                            'label': valueChangeLabel,
                            'value': valueChange,
                            'upOrDownclass': valueChange >= 0 ? 'upPriceChangeColor' : 'downPriceChangeColor'
                        };
                    }
                    if (rootConfig.showCrosshairChangePercent) {
                        popoverConfig['valueChangePercent'] = {
                            'label': valueChangePercentLabel,
                            'value': valueChangePercent,
                            'upOrDownclass': valueChange >= 0 ? 'upPriceChangeColor' : 'downPriceChangeColor'
                        };
                    }
                } else {
                    var popoverConfig = {
                        'open': {
                            'label': lang['open'],
                            'value': isNaN(data['open']) ? lang['crosshairsPopoverNoData'] : numberFormat(data['open'], 'open')
                        },
                        'close': {
                            'label': lang['close'],
                            'value': isNaN(data['close']) ? lang['crosshairsPopoverNoData'] : numberFormat(data['close'], 'close')
                        },
                        'high': {
                            'label': lang['high'],
                            'value': isNaN(data['high']) ? lang['crosshairsPopoverNoData'] : numberFormat(data['high'], 'high')
                        },
                        'low': {
                            'label': lang['low'],
                            'value': isNaN(data['low']) ? lang['crosshairsPopoverNoData'] : numberFormat(data['low'], 'low')
                        },
                        'y': {
                            'value': isNaN(data['y']) ? lang['crosshairsPopoverNoData'] : numberFormat(data['y'], data.name)
                        }
                    };
                    if (rootConfig.showCrosshairChange) {
                        popoverConfig['valueChange'] = {
                            'label': valueChangeLabel,
                            'value': valueChange,
                            'upOrDownclass': valueChange >= 0 ? 'upPriceChangeColor' : 'downPriceChangeColor'
                        };
                    }
                    if (rootConfig.showCrosshairChangePercent) {
                        popoverConfig['valueChangePercent'] = {
                            'label': valueChangePercentLabel,
                            'value': valueChangePercent,
                            'upOrDownclass': valueChange >= 0 ? 'upPriceChangeColor' : 'downPriceChangeColor'
                        };
                    }

                    if (rootConfig.chartGenre === ChartGenre.StockChart && rootConfig.showCrosshairVolume) {
                        popoverConfig['volume'] = {
                            'label': lang['volume'],
                            'value': isNaN(data['volume']) ? lang['crosshairsPopoverNoData'] : numberFormat(data['volume'], 'volume', true)
                        }
                    }
                }

                return popoverConfig;
            };

            var getLabelNameFromLanguage = function(lang, popoverType) {
                var _popoverType = popoverType.substring(0, 1).toLowerCase() + popoverType.substring(1);
                return lang[_popoverType];
            };

            // show plus sign
            var isShowPlusSign = function(data) {
                var showPricePlusSign = rootConfig.showPricePlusSign;
                var _data = data;
                if (showPricePlusSign) {
                    _data = parseFloat(data, 10) > 0 ? "+" + data : data;
                }
                return _data;
            };

            var assembleGrowthOrTenKGrowthOrPostTax = function(data, lang, chartType, popoverType, labelname) {
                var label = getLabelNameFromLanguage(lang, popoverType);
                var gValue = isNaN(data['close']) ? data['value'] : data['close'];
                if (chartConfigManager.getMainDataType() === chartConfigManager.MainDataTypes.PriceWithDividendGrowth && isFundChart) {
                    label = lang[chartConfigManager.MainDataTypes.PriceWithDividendGrowth];
                    gValue = data['growthWithNav'];
                } else if (rootConfig.growthBaseValue && rootConfig.growthBaseValue != 10000 && rootConfig.charts.mainChart.dataType === MainDataTypes.Growth10K) {
                    label = rootConfig.growthBaseValue + " " + lang["growth"];
                }
                if (!data) {
                    data = {
                        close: '-',
                        value: '-',
                        y: '-'
                    }
                }

                if (isNaN(gValue)) {
                    gValue = lang['crosshairsPopoverNoData'];
                } else {
                    gValue = numberFormat(gValue, util.lowerFirstChar(popoverType), false, data.decimalPlaces, false);
                    if (chartConfigManager.getMainDataType() !== chartConfigManager.MainDataTypes.PriceWithDividendGrowth) {
                        gValue = isShowPlusSign(gValue);
                    }
                }
                if (chartType === 'compare' && popoverType === PopoverTypes.TenKGrowth) {
                    labelname = util.lowerFirstChar(popoverType);
                }
                if (chartType === 'compare' && !data['y']) {
                    data['y'] = gValue;
                }
                var yVal = isShowPlusSign(numberFormat(data['y'], labelname, false, data.decimalPlaces));
                var popoverConfig = {
                    'growth': {
                        'label': label,
                        'value': gValue
                    },
                    'y': {
                        'value': isNaN(data['y']) ? lang['crosshairsPopoverNoData'] : yVal
                    }
                };

                if (rootConfig.chartGenre === ChartGenre.StockChart && rootConfig.showCrosshairVolume) {
                    popoverConfig['volume'] = {
                        'label': lang['volume'],
                        'value': isNaN(data['volume']) ? lang['crosshairsPopoverNoData'] : numberFormat(data['volume'], 'volume', true)
                    };
                }
                return popoverConfig;
            };

            var assemblePerformanceGrowth = function(data, lang, chartType) {
                var performanceValue;
                var value = data['close'] || data['value'];
                if (isNaN(value)) {
                    performanceValue = lang['crosshairsPopoverNoData'];
                } else {
                    switch (rootConfig.crossPerformanceVal) {
                        case MainDataTypes.Growth:
                            performanceValue = numberFormat((value - 10000) / 100, chartType + 'GrowthLabel') + '%'; // devide 10000 multiply 100 so is 100;
                            break;
                        case MainDataTypes.Growth10K:
                            performanceValue = numberFormat(value, chartType + 'GrowthLabel');
                            break;
                        case "original":
                            performanceValue = numberFormat(data.originalPrice, chartType + 'GrowthLabel');
                            break;
                    }
                    if (rootConfig.crossCurrency && data['currency']) {
                        if (rootConfig.crossCurrency === 'symbol') {
                            var currencyInfo = Util.getCurrency(data['currency']);
                            if (currencyInfo.orient === 'right') {
                                performanceValue += ' ' + currencyInfo.symbol;
                            } else {
                                performanceValue = currencyInfo.symbol + ' ' + performanceValue;
                            }
                        } else if (rootConfig.crossCurrency === 'code') {
                            performanceValue = (rootConfig.currencyAliases[data['currency']] || data['currency']) + ' ' + performanceValue;
                        }
                    }
                }

                var popoverConfig = {
                    'growth': {
                        'label': lang[chartType + 'GrowthLabel'],
                        'value': performanceValue
                    }
                };

                return popoverConfig;
            };

            var renderPopover = function(container, chartData, popoverType, lang) {
                var mainCfg = rootConfig.charts.mainChart;
                decimalPlaces = $.isNumeric(mainCfg.crossHairDecimal) ? mainCfg.crossHairDecimal : 2;
                if (rootConfig.charts.mainChart.dataType === MainDataTypes.Growth10K && rootConfig.growth10KDecimal ||
                    (chartConfigManager.getMainDataType() === chartConfigManager.MainDataTypes.PriceWithDividendGrowth && isFundChart)) {
                    _popover.addClass("mkts-components-svgchart-crosshair-long");
                } else {
                    _popover.removeClass("mkts-components-svgchart-crosshair-long");
                }
                var parseData = {};
                var pooverTemplate;
                var isPerformancesGrowth = rootConfig.growthType === "performancesGrowth";
                var isPriceGrowth = rootConfig.growthType === "priceGrowth";
                if (popoverType === PopoverTypes.Simple) {
                    pooverTemplate = popoverSimpleTemplate;
                    _popover.addClass("mkts-cmpt-svgcht-crosshair-mode-" + rootConfig.crosshairMode);
                } else if (isPerformancesGrowth) {
                    pooverTemplate = popoverPerformancesGrowthTemplate;
                } else if (isPriceGrowth) {
                    pooverTemplate = popoverPriceGrowthTemplate;
                } else if (popoverType === PopoverTypes.OHLCV) {
                    pooverTemplate = popoverOHLCVTemplate
                } else {
                    pooverTemplate = popoverGrowthTemplate; //popoverGrowthTemplate;
                }

                var popoverAssembleFunction;
                if (isPerformancesGrowth) {
                    popoverAssembleFunction = assemblePerformanceGrowth;
                } else {
                    switch (popoverType) {
                        case PopoverTypes.Growth:
                        case PopoverTypes.TenKGrowth:
                        case PopoverTypes.PostTax:
                        case PopoverTypes.Simple:
                            popoverAssembleFunction = assembleGrowthOrTenKGrowthOrPostTax;
                            break;
                        case PopoverTypes.OHLCV:
                        default:
                            popoverAssembleFunction = assembleOHLCV;
                            break;
                    };
                }

                for (var chartType in chartData.data) {
                    if (chartData.data.hasOwnProperty(chartType)) {
                        parseData[chartType] = parseData[chartType] || [];
                        switch (chartType) {
                            case 'events':
                                parseData.events = parseData.events || [];
                                chartData.data.events.forEach(function(e) {
                                    var eventValue = e.data.yValue;
                                    if ($.isNumeric(eventValue)) {
                                        eventValue = numberFormat(+eventValue);
                                    }
                                    var y = {
                                        'value': eventValue,
                                        'date': moment(e.data.yDate).format(getDateFormat())
                                    };
                                    if (e.data.yFrequency) {
                                        y.frequency = e.data.yFrequency;
                                    }
                                    parseData.events.push({
                                        'showLegend': e.showLegend === false ? false : true,
                                        'name': e.name,
                                        'color': e.color,
                                        'y': y
                                    });
                                });
                                break;
                            case 'main':
                                var mainData = chartData.data[chartType].data;
                                if (mainData) {
                                    var popData = {
                                        'showLegend': chartData.data[chartType].showLegend === false ? false : true,
                                        'datetime': moment(mainData.date).format(getDateFormat())
                                    };

                                    if (isPerformancesGrowth || isPriceGrowth) {
                                        popData['closeDateLabel'] = lang['closeDateLabel'];
                                    }

                                    if (isPerformancesGrowth || isPriceGrowth) {
                                        popData['valueLabel'] = lang['value'];
                                        var v;
                                        if (isPerformancesGrowth && rootConfig.crossValue === 'original') {
                                            v = mainData.originalPrice;
                                        } else if (rootConfig.crossValue === MainDataTypes.Growth) {
                                            v = ((mainData.value || mainData.y) - 10000) / 100;
                                        } else {
                                            v = mainData.value || mainData.y;
                                        }
                                        popData['value'] = numberFormat(v);
                                    } else {
                                        popData['ticker'] = chartData.data[chartType].ticker;
                                        popData['color'] = chartData.data[chartType].color;
                                    }
                                    if (rootConfig.showCrosshairOriginalData) {
                                        var originalPrice = isNaN(mainData['originalPrice']) ? (mainData['close'] || d['value']) : mainData['originalPrice'];
                                        if (isNaN(originalPrice)) {
                                            originalPrice = lang['crosshairsPopoverNoData'];
                                        }
                                        var flag = (chartConfigManager.isMutualFund(chartData.data[chartType].tickerObject) && chartData.data[chartType].tickerObject.mType !== 'XI');
                                        popData['originalPrice'] = {
                                            'label': flag ? lang['nav'] : lang['price'],
                                            'value': isShowPlusSign(numberFormat(originalPrice, flag ? 'nav' : 'price', false, rootConfig.growth10KDecimal, true))
                                        };
                                    }
                                    mainData['currency'] = chartData.data[chartType].currency;
                                    mainData['frequency'] = chartData.data[chartType].frequency;
                                    popData['globalClass'] = chartData.data[chartType].globalClass;
                                    parseData[chartType] = $.extend(popData, popoverAssembleFunction(mainData, lang, chartType, popoverType));
                                }
                                break;
                            default:
                                parseData[chartType] = parseData[chartType] || [];
                                chartData.data[chartType].forEach(function(d) {
                                    var defaultData = $.extend(true, {
                                        'decimalPlaces': d.decimalPlaces,
                                        'name': d.name
                                    }, d.data);
                                    var crossData = {
                                        'showLegend': d.showLegend === false ? false : true,
                                        'ticker': lang[d.ticker] || d.ticker,
                                        'color': d.color,
                                        'datetime': defaultData ? moment(defaultData.date).format(getDateFormat()) : 0
                                    };
                                    if (chartType === 'compare') {
                                        defaultData.currency = d.currency;
                                        if (rootConfig.showCrosshairOriginalData) {
                                            var originalPrice = isNaN(defaultData['originalPrice']) ? (defaultData['close'] || d['value']) : defaultData['originalPrice'];
                                            if (isNaN(originalPrice)) {
                                                originalPrice = lang['crosshairsPopoverNoData'];
                                            }
                                            crossData.originalPrice = {
                                                'label': (chartConfigManager.isMutualFund(d.tickerObject) && d.tickerObject.mType !== 'XI') ? lang['nav'] : lang['price'],
                                                'value': isShowPlusSign(numberFormat(originalPrice, '', false, rootConfig.growth10KDecimal, true))
                                            }
                                        }
                                        if (d.fundamentals) {
                                            crossData['fundamentals'] = [];
                                            for (var f = 0; f < d.fundamentals.length; f++) {
                                                var fundamentalValue = d.fundamentals[f].data && d.fundamentals[f].data.value;
                                                if (isNaN(fundamentalValue)) {
                                                    fundamentalValue = lang['crosshairsPopoverNoData'];
                                                }
                                                fundamentalValue = numberFormat(fundamentalValue, util.lowerFirstChar(d.fundamentals[f].name));
                                                crossData['fundamentals'].push({
                                                    'showLegend': d.fundamentals[f].showLegend === false ? false : true,
                                                    'label': lang[d.fundamentals[f].ticker] || d.fundamentals[f].ticker,
                                                    'color': d.fundamentals[f].color,
                                                    'value': isShowPlusSign(fundamentalValue),
                                                    'datetime': defaultData ? moment(defaultData.date).format(getDateFormat()) : 0
                                                });
                                            }
                                        }
                                    }

                                    if (d.globalClass) {
                                        crossData.globalClass = d.globalClass;
                                    }
                                    parseData[chartType].push($.extend(crossData, popoverAssembleFunction(defaultData, lang, chartType, popoverType, d.name ? util.lowerFirstChar(d.name) : '')));
                                });
                                break;
                        }
                    }
                };

                var html = pooverTemplate(parseData);
                container.html(html);
            };

            var getDateFormat = function() {
                var format;
                if (chartConfigManager.isIntraday()) {
                    format = rootConfig.intradayDateFormater;
                } else {
                    format = rootConfig.dailyDateFormatter;
                }
                return format;
            }

            var position = function(pos, offsetTop) {
                _maxWidth = target.width();
                _maxHeight = target.height();
                if (offsetTop > 0) {
                    _lineVOffsetTop = offsetTop;
                }
                var top = pos.top - target.offset().top;
                var left = pos.left;
                var lineOffSet = 0;

                if ($.browser.msie) { //For ie not support pointer-events: none; otherwise it cause zoom and drag can't work.
                    (left > (_maxWidth / 2)) ? (lineOffSet = -1) : (lineOffSet = 1)
                }

                _lineH.css({
                    'top': top + lineOffSet
                }).show();

                _lineV.css({
                    'left': left + lineOffSet,
                    'top': _lineVOffsetTop,
                    'height': _maxHeight - _lineVOffsetTop
                }).show();

                _popover.show();

                _lineHHeight <= 0 && (_lineHHeight = _lineH.outerHeight(true));
                _lineVWidth <= 0 && (_lineVWidth = _lineV.outerWidth(true));
                _popoverWidth = _popover.outerWidth(true);
                _popoverHeight = _popover.outerHeight(true);
                var direction = [];
                var halfPopoverWidth = _popoverWidth / 2;
                if (rootConfig.crosshairJustify) {
                    if (left + halfPopoverWidth > _maxWidth) {
                        left = left - _popoverWidth - _lineVWidth;
                        direction.push('right');
                    } else if (left < halfPopoverWidth) {
                        left = left + _lineVWidth;
                        direction.push('left');
                    } else {
                        left = left - halfPopoverWidth;
                        direction.push('middle');
                    }
                } else {
                    if (left + _popoverWidth > _maxWidth) {
                        left = left - _popoverWidth - _lineVWidth;
                        direction.push('right');
                    } else {
                        left = left + _lineVWidth;
                        direction.push('left');
                    }
                }

                if (top + _popoverHeight > _maxHeight) {
                    top = top - _popoverHeight - _lineHHeight;
                    direction.push('down');
                } else {
                    top = top + _lineHHeight;
                    direction.push('up');
                }
                _popover.css({
                    left: left,
                    top: top
                }).removeClass(function(index, css) {
                    return (css.match(/\bpopover-tip-\S+/g) || []).join(' ')
                }).addClass('popover-tip-' + direction.join(''));
            };

            var displayCrosshairs = function(e) {
                renderPopover(_popover, e, e.popoverType, _lang);
                position(e.position, e.offsetTop);
            };

            var hideCrosshairs = function() {
                _lineH.hide();
                _lineV.hide();
                _popover.hide();
            };

            var bindEvent = function() {
                var dom = d3.select(target[0]);
                d3.select(target[0]).on('mouseleave.crosshairs', function() {
                    var target = d3.event.relatedTarget;
                    if (util.contains(_lineH[0], target) || util.contains(_lineV[0], target) || util.contains(_popover[0], target)) {
                        return false;
                    }
                    hideCrosshairs();
                    eventManager.trigger(eventManager.EventTypes.MouseOutChart);
                });

                eventManager.bind(eventManager.EventTypes.ToggleCrossharis, function(e) {
                    if (e) { //display crosshairs
                        displayCrosshairs(e);
                    } else {
                        hideCrosshairs();
                    }
                }, crosshairs);
            };

            var crosshairs = function(tar) {
                rootConfig = chartConfigManager.getConfig();
                ChartGenre = chartConfigManager.ChartGenre;
                locale = rootConfig.lang;
                target = tar;
                target.append('<div class="crosshairs line horizontal"></div><div class="crosshairs line vertical"></div><div class="crosshairs popover-tip popover-tip-leftup"></div>');
                _lineH = target.find('.horizontal');
                _lineV = target.find('.vertical');
                _popover = target.find('.popover-tip');
                if (rootConfig.chartGenre === ChartGenre.FundChart) {
                    isFundChart = true;
                    _popover.addClass("nav-tip");
                } else {
                    isFundChart = false;
                    _popover.removeClass("nav-tip");
                }
                _lang = chartConfigManager.getLabels('markets-components-svgchart', chartConfigManager.getConfig().lang);
                bindEvent();
            };

            return crosshairs;
        }
        return model;
    });