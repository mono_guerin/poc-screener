define(["../../../html/menuBar/intervalTemplate.html",
        "../../../html/menuBar/intervalSelectOption.html",
        "../../core/core",
        "../../util/util",
        "morningstar",
        'moment'
    ],
    function(intervalTemplate,
        intervalSelectOption,
        core,
        util,
        morningstar,
        moment) {
        "use strict";

        /**
         * Menubar for main chart type view
         *
         * @constructor
         */
        var $ = core.$;
        var asterixUtil = morningstar.asterix.util;
        var isMobile = util.deviceType.mobile();
        var intervalsMapping = {
            '1D': ".one-day",
            '5D': ".five-day",
            '15D': ".fifteen-day",
            '1M': ".one-month",
            '3M': ".three-month",
            '6M': ".six-month",
            'YTD': ".y-t-d",
            '1Y': ".one-year",
            '3Y': ".three-year",
            '5Y': ".five-year",
            '10Y': ".ten-year",
            'MAX': ".max"
        };

        var model = function myfunction(managers) {
            var eventManager = managers.eventManager;
            var CCM = managers.chartConfigManager;
            var $intervalTemplate;
            var freqConfig = CCM.frequencyConfig;
            var freqTypes = CCM.FrequencyTypes;
            var IntervalTypes = CCM.IntervalTypes;
            var datePickerFixativeFormater = 'YYYY-MM-DD';
            var $startInput, $endInput;
            var appConfig, isFundChart;
            var datePicker;

            var Interval = function(config) {
                this.LastIntervalType; //remain last interval type to change the frequency of "Max"
                this.config = {
                    container: ''
                };
                this._init(config);
            };

            Interval.prototype = {
                /* test-code */
                __private: {
                    CCM: CCM,
                    eventManager: eventManager
                },
                /* end-test-code */
                _init: function(config) {
                    this.setupCalendars = function() {
                        $startInput = $intervalTemplate.find('.mkts-cmpt-svgcht-start-input');
                        $endInput = $intervalTemplate.find('.mkts-cmpt-svgcht-end-input');
                        if (!isMobile) {
                            var datePickerConfig = $.extend(true, {}, appConfig.datePicker, {
                                needTrack: false,
                                dataFields: [$startInput[0], $endInput[0]],
                                startDate: self.startMoment.toDate(),
                                endDate: self.endMoment.toDate(),
                                lang: appConfig.lang,
                                format: this.displayFomater,
                                triggerClass: 'mkts-cmpt-svgcht-cal-picker',
                                labels: CCM.getD3Locale()
                            });
                            var params = {
                                element: appConfig.$componentContainer,
                                initialConfiguration: {
                                    component: {
                                        configuration: datePickerConfig,

                                    }
                                }
                            };
                            datePicker = morningstar.components['markets-components-datepicker'].createComponent(params);
                        }
                    }
                    this.displayFomater = function(date) {
                        if ($.isFunction(appConfig.datePicker.format)) {
                            return appConfig.datePicker.format(date);
                        }
                        var formater = appConfig.dailyDateFormatter;
                        if (moment.isDate(date)) {
                            date = moment(date);
                        }
                        return date.format(formater || datePickerFixativeFormater);
                    };

                    function setDefaultRange() {
                        if (isFundChart || appConfig.drawNavAndPriceChart) {
                            freqConfig['OneMonth'].fc = freqConfig['ThreeMonth'].fc = freqConfig['SixMonth'].fc;
                            freqConfig['OneMonth'].def = freqConfig['ThreeMonth'].def = freqConfig['SixMonth'].def;
                            $intervalTemplate.find('.one-day, .five-day, .fifteen-day').remove();
                        }
                        self.rangePreset = appConfig.intervalType;
                        if (self.rangePreset) {
                            $intervalTemplate.find('.' + util.camelToDashes(self.rangePreset)).addClass('active');
                        }
                        self._updateFreq(self.rangePreset || CCM.getfrequencyByDateRange(appConfig.dateRange, true));
                    }

                    var self = this;
                    appConfig = CCM.getConfig();
                    isFundChart = appConfig.chartGenre === CCM.ChartGenre.FundChart;
                    var locale = appConfig.lang || 'en-US';
                    var langObj = CCM.getLabels('markets-components-svgchart', locale);

                    self._clearRange = function() {
                        self.rangePreset = null;
                        this.$container.find('.mkts-cmpt-svgcht-segment-option').removeClass('active');
                    };



                    self._emitIntervalFreq = function() {
                        var freq = self.intervalFreq;
                        var interval, dateRange;

                        if (asterixUtil.isString(self.rangePreset)) {
                            interval = self.rangePreset;
                        } else {
                            dateRange = [self.startMoment.toDate(), self.endMoment.toDate()];
                        }

                        eventManager.trigger(eventManager.EventTypes.ChangeIDFConfig, interval, dateRange, freq);
                    };
                    self._freqFromDates = function(start, end, isBrush) {
                        // add date change callback
                        var freqByDate = CCM.getfrequencyByDateRange([start, end], true);

                        //update frequency
                        self._clearRange();
                        self._updateFreq(freqByDate);
                        !isBrush && eventManager.trigger(eventManager.EventTypes.onDateRangeChange, {
                            name: 'calendarDateRange',
                            value: {
                                startDate: moment(start).format(appConfig.intradayDateFormater),
                                endDate: moment(end).format(appConfig.intradayDateFormater)
                            }
                        })
                    };
                    self._updateFreq = function(interval, innerCall, isDateRange) {
                        function makeOptions(config) {
                            var options = [];
                            var option;
                            var conLen = config.length;
                            var i;
                            for (i = 0; i < conLen; i++) {
                                var templateCfg = {
                                    isMobile: isMobile,
                                    fc: {
                                        text: langObj[config[i].text] || config[i].text,
                                        value: config[i].value
                                    }
                                }
                                option = intervalSelectOption(templateCfg);
                                options.push(option);
                            }

                            return options.join('');
                        }
                        var $freqSelect = $intervalTemplate.find('.mkts-cmpt-svgcht-freq-input');
                        var fc = freqConfig[interval].fc;
                        var def = freqConfig[interval].def;
                        var options;
                        var $defOpt;
                        var nfc = [];
                        var validFre = [];
                        for (var i = 0, l = fc.length, c; i < l; i++) {
                            c = fc[i];
                            if (isFundChart) {
                                if (isNaN(c.value)) {
                                    nfc.push(c);
                                    validFre.push(c.value);
                                }
                            } else {
                                nfc.push(c);
                                validFre.push(c.value);
                            }
                        }
                        var idx = -1,
                            reRender = false,
                            appFreIndex;
                        if (appConfig.defaultFrequency || appConfig.lastFrequency) {
                            idx = $.inArray((appConfig.defaultFrequency || appConfig.lastFrequency) + "", validFre);
                            appConfig.lastFrequency = null;
                        }
                        if (appConfig.frequencyPriority === "manually") {
                            idx = $.inArray(appConfig.frequency + "", validFre);
                        }
                        var defIndex = idx >= 0 ? idx : $.inArray(def + "", validFre);

                        /** when chose custom date and the frequency in the frequency list
                         *  then the frequency do not change,Otherwise the change
                         */
                        appFreIndex = $.inArray(appConfig.frequency + "", validFre);

                        if (appFreIndex > -1) {
                            if (!isDateRange && defIndex >= 0) {
                                defIndex = appFreIndex;
                                reRender = true;
                            }
                        }

                        if (self.interval !== interval || reRender) {
                            self.interval = interval;
                            options = makeOptions(nfc);
                            if (isMobile) {
                                $freqSelect.empty().append(options);
                                $defOpt = $freqSelect.find('option').eq(defIndex);
                                $defOpt.attr('selected', 'selected');
                            } else {
                                var $ulFreq = $intervalTemplate.find(".ul-freq-dropdown-list");
                                $ulFreq.empty().append(options);
                                var $selectli = $ulFreq.find('li').eq(defIndex);
                                $freqSelect.text($selectli.find('span').text()).data("value", $selectli.data("value"));
                                $selectli.attr('data-selected', 'selected');
                            }
                        }

                        self.onChangeFrequency(null, innerCall); //innerCall
                    };
                    self.datesHaveChanged = function(start, end) {
                        var _startMoment = start,
                            _endMoment = end,
                            _flag = false;
                        if (_startMoment) {
                            if (moment.isDate(_startMoment)) {
                                _startMoment = moment(_startMoment).hours(0).minutes(0).seconds(0);
                            }
                            if (!_startMoment.isSame(self.startMoment)) {
                                self.startMoment = _startMoment;
                                _flag = true;
                            }
                        }
                        if (_endMoment) {
                            if (moment.isDate(_endMoment)) {
                                _endMoment = moment(_endMoment).hours(0).minutes(0).seconds(0);
                            }
                            if (!_endMoment.isSame(self.endMoment)) {
                                self.endMoment = _endMoment;
                                _flag = true;
                            }
                        }

                        if (self.startMoment.isAfter(self.endMoment)) { //switch start end if start < end
                            var temp = self.endMoment;
                            self.endMoment = self.startMoment;
                            self.startMoment = temp;
                            if (isMobile) {
                                $startInput.val(self.startMoment.format(datePickerFixativeFormater));
                                $endInput.val(self.endMoment.format(datePickerFixativeFormater));
                            } else {
                                datePicker.setProperty("date", {
                                    date: self.startMoment.toDate(),
                                    which: 'start'
                                });
                                datePicker.setProperty("date", {
                                    date: self.endMoment.toDate(),
                                    which: 'end'
                                });
                            }

                        }

                        return _flag;
                    };
                    langObj.isMobile = isMobile;
                    $intervalTemplate = $(intervalTemplate(langObj));

                    $.extend(true, self.config, config);
                    var dateRange = appConfig.dateRange;
                    if (dateRange && dateRange.length === 2) {
                        self.startMoment = moment(dateRange[0]);
                        self.lastMoment = self.endMoment = moment(dateRange[1]);
                    } else {
                        self.lastMoment = self.startMoment = self.endMoment = moment().hours(0).minutes(0).seconds(0);
                    }
                    self._bindEvents();
                    setDefaultRange();
                    self.setupCalendars();
                    self._renderView();
                },

                _renderView: function() {
                    this.$el = this.config.container;
                    $intervalTemplate.appendTo(this.$el);
                    this.$container = this.$el.parent();
                },
                _bindEvents: function() {
                    var self = this;

                    function momentFromEvent(e) {
                        var target;
                        var _momentObj;
                        if (asterixUtil.isObj(e.originalEvent) && asterixUtil.isObj(e.originalEvent.target) && asterixUtil.isString(e.originalEvent.target.value) && e.originalEvent.target.value !== '') {
                            target = e.originalEvent.target;
                        } else if (asterixUtil.isObj(e.currentTarget) && asterixUtil.isString(e.currentTarget.value) && e.currentTarget.value !== '') {
                            target = e.currentTarget;
                        }
                        if (target) {
                            $(target).parent().removeClass('open'); //easiest way to clear down arrow when an option is selected
                            $(target).blur();
                            var dateVal = isMobile ? target.value : target.dateValue;
                            _momentObj = moment(dateVal).hours(0).minutes(0).seconds(0);
                            if (_momentObj.isAfter(self.lastMoment)) {
                                _momentObj = self.lastMoment;
                            }
                        }
                        return _momentObj;
                    }

                    function changeStartEnd(start, end) {
                        if (self.datesHaveChanged(start, end)) {
                            self._freqFromDates(self.startMoment.toDate(), self.endMoment.toDate());
                        }
                    }

                    function onChangeStartDate(e) {
                        changeStartEnd(momentFromEvent(e));
                    }

                    function onChangeEndDate(e) {
                        changeStartEnd(undefined, momentFromEvent(e));
                    }

                    function onChangeFrequency(e, innerCall) {
                        var value, $target, $container = self.$container || $intervalTemplate;
                        if (e) {
                            $target = $(e.currentTarget);
                        } else {
                            if (isMobile) {
                                $target = $container.find('.mkts-cmpt-svgcht-freq-input').eq(0);
                            } else {
                                $target = $container.find("li[data-selected='selected']");
                            }
                        }

                        if (!isMobile) {
                            $container.find(".ul-freq-dropdown-list").hide();
                            if ($target && $target.find("span")) {
                                var targetText = $target.find("span").text();
                                $container.find('.mkts-cmpt-svgcht-freq-input')
                                    .data("value", targetText)
                                    .text(targetText);
                            }
                            value = $target.data("value") || '';
                        } else {
                            value = $target.val() || '';
                            $target.parent().removeClass('open'); //easiest way to clear down arrow when an option is selected
                            $target.blur();
                        }

                        if (typeof(value) !== "object" && value !== '') {
                            if (!innerCall && self.rangePreset === IntervalTypes.OneDay) {
                                self.oneDayCanAdjust = false;
                            }
                            self.intervalFreq = value;
                            self._emitIntervalFreq();
                            e && eventManager.trigger(eventManager.EventTypes.triggerClickCallback, {
                                name: 'frequency',
                                value: value
                            });
                        }
                    }

                    function hideFrequencyList() {
                        self.$container.find(".ul-freq-dropdown-list").hide();
                        self.$container.find(".mkts-cmpt-svgcht-freq").removeClass('open');
                    }

                    function onClickFrequency(e) {
                        var $dropdownList = self.$container.find(".ul-freq-dropdown-list");
                        var $target = $(e.target || e.srcElement);
                        var $pEl = $target;
                        if ($dropdownList.css("display") == "none") {
                            var $freqDiv = self.$container.find(".mkts-cmpt-svgcht-freq");
                            var postion = $freqDiv.position();
                            var top = postion.top + $freqDiv.height();
                            var left = postion.left;
                            var width = self.$container.find(".mkts-cmpt-svgcht-freq-container").width();
                            $dropdownList.css({
                                "top": top,
                                "left": left,
                                "width": width
                            }).show();
                        } else {
                            $dropdownList.hide();
                        }
                        CCM.triggerClickCallback({
                            name: 'frequency'
                        });
                    }

                    function filterRenderEvents(dates) {
                        updateDatesFromMainChart(dates, true);
                    }

                    function updateDatesFromMainChart(dates, suppressClearRange) {
                        if (self.datesHaveChanged(dates[0], dates[1])) {
                            if (isMobile) {
                                $startInput.val(self.startMoment.format(datePickerFixativeFormater));
                                $endInput.val(self.endMoment.format(datePickerFixativeFormater));
                            } else {
                                datePicker.setProperty("date", {
                                    date: self.startMoment.toDate(),
                                    which: 'start'
                                });
                                datePicker.setProperty("date", {
                                    date: self.endMoment.toDate(),
                                    which: 'end'
                                });
                            }
                            if (!suppressClearRange) {
                                self._clearRange();
                            }
                        }
                        updateChartTitle();
                    }

                    function updateChartTitle(startDate, endDate) {
                        startDate = startDate ? startDate : self.displayFomater(self.startMoment.toDate());
                        endDate = endDate ? endDate : self.displayFomater(self.endMoment.toDate());
                        if (!appConfig.actionHandler) return;
                        var value = {};
                        var mainTickerObject = appConfig.mainTickerObject;
                        var name = mainTickerObject.Name !== "" ? mainTickerObject.Name : mainTickerObject.performanceId;
                        var MainDataTypes = CCM.MainDataTypes;
                        var dataType = CCM.getMainDataType();
                        dataType = dataType === MainDataTypes.Growth10K ? "Wealth" : dataType === MainDataTypes.Growth ? "Return" : "NAV";
                        value = {
                            name: name,
                            startDate: startDate,
                            endDate: endDate,
                            dataType: dataType,
                            timePeriod: appConfig.intervalType
                        };
                        var actionHandler = appConfig.actionHandler;
                        if (appConfig.setMarketValueTitle) {
                            actionHandler.send("setTitle", "Market Value " + value.startDate + "-" + value.endDate + "-" + value.name);
                        } else if (appConfig.setReturnChartTitle) {
                            actionHandler.send("setTitle", dataType + " over " + value.timePeriod);
                        }
                    }

                    function updateFreqAndMainChart(dates, isBrush) {
                        self._freqFromDates(dates[0], dates[1], isBrush);
                    }

                    function onDateIntervalClick(innerCall) {
                        function clearActive() {
                            self.$container.find('.mkts-cmpt-svgcht-segment-option').removeClass('active');
                        }
                        var id, eventVal;
                        if (asterixUtil.isObj(this) && asterixUtil.isString(this.getAttribute('data-menuid'))) {
                            id = this.getAttribute('data-menuid');
                            eventVal = util.dashesToCamel(id);
                            eventVal = util.capFirstChar(eventVal);
                            if (eventVal === self.rangePreset) {
                                return;
                            }
                            if (asterixUtil.isObj(freqConfig[eventVal])) {
                                if (!innerCall && eventVal === IntervalTypes.OneDay) {
                                    self.oneDayCanAdjust = true;
                                }
                                self.rangePreset = eventVal;
                                self.LastIntervalType = eventVal;
                                self._updateFreq(eventVal, true, true);
                            }
                            clearActive();
                            $(this).addClass('active');

                            CCM.triggerClickCallback({
                                name: 'date range',
                                value: eventVal
                            });
                        }
                        $(this).blur();
                    }

                    function onDdUnfocus(e) {
                        var $dd = self.$container.find('.mkts-cmpt-svgcht-dropdown');
                        var ddLen = $dd.length;
                        var tar = e.target;
                        var contained = false;
                        var i;
                        for (i = 0; i < ddLen; i++) {
                            if ($dd[i] === tar || $.contains($dd[i], tar)) {
                                contained = true;
                                $($dd[i]).toggleClass('open');
                            } else {
                                $($dd[i]).removeClass('open');
                            }
                        }
                    }

                    function changeOneDayFrequency(isChange) {
                        //if user have manuly change frequency, we can't change again.
                        if (self.oneDayCanAdjust !== false) {
                            if (isChange) {
                                freqConfig.OneDay.def = 1;
                                changeView(1);
                            } else {
                                freqConfig.OneDay.def = 0;
                                changeView(5);
                            }
                        }
                    }

                    /*
                     *   @freqValue: the frequency value before change.
                     */
                    function changeView(freqValue) {
                        if (self.rangePreset === IntervalTypes.OneDay && (!self.intervalFreq || self.intervalFreq === freqValue)) {
                            self.interval = null; //otherwise, it don't update the view.
                            var target = self.$container.find(".one-day")[0];
                            onDateIntervalClick.call(target, true); //innerCall 
                        }
                    }

                    function changeInterval(interval) {
                        var intervalMap = CCM.intervalTypesMapping;
                        if (self.rangePreset === intervalMap[interval]) {
                            return;
                        }
                        var target = intervalsMapping[interval] && self.$container.find(intervalsMapping[interval])[0];
                        if (target)
                            onDateIntervalClick.call(target, true);
                    }

                    function deployIntervalBreakPoint(width) {
                        var intervalArr;
                        var breakPointArr = Object.keys(appConfig.intervalBreakPointMap).sort();
                        for (var b in breakPointArr) {
                            var breakpoint = breakPointArr[b];
                            if (breakpoint >= width || breakpoint === "default") {
                                if (breakpoint === self.intervalBreakPoint) {
                                    return;
                                } else {
                                    if (!self.intervalBreakPoint) self.intervalBreakPoint = "default";
                                    // preIntervalArr = appConfig.intervalBreakPointMap[self.intervalBreakPoint];
                                    intervalArr = appConfig.intervalBreakPointMap[breakpoint];
                                    self.intervalBreakPoint = breakpoint;
                                    var showSelector = [],
                                        needChangeDefaultInterval = true;
                                    for (var i = 0; i < intervalArr.length; i++) {
                                        var interval = intervalArr[i];
                                        showSelector.push(intervalsMapping[interval]);
                                        if (CCM.intervalTypesMapping[interval] === self.rangePreset) {
                                            needChangeDefaultInterval = false;
                                        }
                                    }
                                    if (needChangeDefaultInterval) {
                                        eventManager.trigger(eventManager.EventTypes.ChangeInterval, intervalArr[0]);
                                    }
                                    if (showSelector) {
                                        var showSelector = showSelector.join(','),
                                            showEle = $intervalTemplate.find(showSelector);
                                        showEle.show().removeClass('mkts-cmpt-svgcht-interval-last mkts-cmpt-svgcht-interval-first');
                                        showEle.eq(0).addClass('mkts-cmpt-svgcht-interval-first');
                                        showEle.eq(showEle.length - 1).addClass('mkts-cmpt-svgcht-interval-last');
                                        $intervalTemplate.find('.mkts-cmpt-svgcht-segment-option').not(showSelector).hide();
                                    }
                                }
                                return;
                            }
                        }
                    }
                    self.onChangeFrequency = onChangeFrequency;

                    if (!isMobile) {
                        eventManager.bind(eventManager.EventTypes.LayoutChange, hideFrequencyList);
                    }

                    eventManager.bind(eventManager.EventTypes.ChangeOneDayFrequency, changeOneDayFrequency, self);
                    eventManager.bind(eventManager.EventTypes.UpdateMenuInterval, updateDatesFromMainChart);
                    eventManager.bind(eventManager.EventTypes.UpdateMenuIntervalForRender, filterRenderEvents);
                    eventManager.bind(eventManager.EventTypes.RequestFrequencyFromMenu, updateFreqAndMainChart);
                    eventManager.bind(eventManager.EventTypes.ChangeInterval, changeInterval);
                    eventManager.bind(eventManager.EventTypes.DeployIntervalBreakPoint, deployIntervalBreakPoint);

                    appConfig.$componentContainer.on('change.mkts-cmpt-svgcht-menu', 'input.mkts-cmpt-svgcht-start-input', onChangeStartDate);
                    appConfig.$componentContainer.on('change.mkts-cmpt-svgcht-menu', 'input.mkts-cmpt-svgcht-end-input', onChangeEndDate);
                    appConfig.$componentContainer.on('change.mkts-cmpt-svgcht-menu', 'input.mkts-cmpt-svgcht-freq-input', onChangeFrequency);
                    appConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.change-time-range .mkts-cmpt-svgcht-segment-option', onDateIntervalClick);
                    appConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', onDdUnfocus); //clearing the open state from morningstar style dropdowns is unfortunately complicated
                    appConfig.$componentContainer.on("click.mkts-cmpt-svgcht-menu", ".ul-freq-dropdown-list li", onChangeFrequency);
                    appConfig.$componentContainer.on("click.mkts-cmpt-svgcht-menu", ".mkts-cmpt-svgcht-freq", onClickFrequency);
                    appConfig.$componentContainer.on('mouseleave.mkts-cmpt-svgcht-menu', ".ul-freq-dropdown-list", hideFrequencyList);
                }
            };

            return Interval;
        }

        return model;
    });