define(["../../core/core"], function(core) {
    "use strict";
    var $ = core.$;

    function popover() {
        var $el, option, shown;

        function show(view, config) {
            $el = view.$el;
            var $menuBarContainer;
            var defaults = {
                $container: config.$container,
                $menuBarContainer: config.$menuBarContainer,
                $pageContainer: config.$componentContainer,
                $target: null,
                toggle: true, // hide the popover when hitting the same target twice
                styleClass: "", // custom style class
                sizeForMobile: true,
                center: false,
                autohide: true,
                blurhide: true,
                sync: true
            };

            option = $.extend(defaults, config);
            $menuBarContainer = option.$menuBarContainer;

            var $popElem = $menuBarContainer.find('.' + option.styleClass);
            if ($popElem.length < 1) {
                $menuBarContainer.find('.mkts-cmpt-svgcht-popover').remove(); //if a different popover is shown, remove it
                $el.addClass('mkts-cmpt-svgcht-popover').addClass(option.styleClass);
                $menuBarContainer.append($el);
                adjustPosition();
            } else if (option.sync) {
                adjustPosition();
            } else {
                if (shown) {
                    if (option.toggle) {
                        $popElem.remove();
                        hide(); //will call $(document).off('.popover') to remove click events.
                        return false;
                    }
                }
            }

            // If popover should auto hide on blur
            if (option.blurhide === true) {
                addBlurListener();
            }
            $el.on('click.popover', '.mkts-cmpt-svgcht-popover-close', function() {
                hide();
            });

            shown = true;
        };

        function hide() {
            if ($el) {
                $el.remove();
                $(document).off('.popover');
                $($el).off('.popover');
                option.$menuBarContainer.find('.mkts-cmpt-svgcht-menubtn').each(function() {
                    $(this).removeClass('active');
                });
                shown = false;
            }
        };

        function adjustPosition() {
            function setHeightAndTop($el, $target) {
                var position = $target.position();
                var popTop = position.top + $target.outerHeight();
                var heightPlusPosition = popTop + $el.outerHeight();

                $el.css({ top: popTop });
                if (heightPlusPosition > containerHeight) {
                    $el.height(containerHeight - popTop - 10);
                }
            }

            function setLeft($el, $target) {
                var position = $target.position();
                var popLeft = position.left;

                if (option.center) { //center if config'd
                    popLeft = (popLeft + ($target.outerWidth() / 2) - ($el.outerWidth() / 2));
                }
                if ((popLeft + $el.outerWidth()) > containerWidth) { //scootch if no room
                    popLeft = containerWidth - $el.outerWidth();
                }

                $el.css({ left: popLeft });
            }

            function setArrowPostion($el, $target) {
                var $arrow = $el.find('.mkts-cmpt-svgcht-popover-arrow');
                if ($arrow.length < 1) {
                    return;
                }
                var _left = $target.position().left - $el.position().left;
                _left += 30;
                $arrow.css("left", _left);
            }
            var $pageContainer = option.$pageContainer;
            var containerWidth = $pageContainer.outerWidth();
            var containerHeight = $pageContainer.outerHeight();
            var $target = option.$target;

            $el.css({ visibility: 'hidden' });
            if (option.sizeForMobile === true && option.$container && option.$container.width() < 601) {
                $el.height('auto');
                var style = {
                    'top': 0,
                    'left': 0,
                    'width': '100%',
                    'max-width': '100%',
                    'height': height,
                    'overflow': 'hidden'
                };
                if (option.sync) {
                    var height = $el.outerHeight(true);
                    if (containerHeight < height) {
                        height = containerHeight;
                    }
                    style.height = height + 'px';
                }
                $el.css(style);
            } else {
                $el.css({
                    'width': '330px', //when it display on mobil, it will limited by max-width.
                    'height': 'auto',
                    'max-height': '100%'
                });
                setHeightAndTop($el, $target);
                setLeft($el, $target);
                setArrowPostion($el, $target);
            }
            $el.css({ visibility: 'visible' });
        };

        function addBlurListener() {
            $(document).on('click.popover', function(e) {
                var el = $el[0];
                var tar = e.target;
                var autohide = option.autohide;
                var contained = $.contains(el, tar);

                if (autohide === false || (autohide === true && !contained)) {
                    hide();
                }
            });
        }

        return {
            show: show,
            adjustPosition: adjustPosition,
            hide: hide
        }
    }
    return popover;
});