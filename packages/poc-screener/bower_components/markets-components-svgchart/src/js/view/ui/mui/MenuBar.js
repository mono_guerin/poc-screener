define(["../../../../html/menuBar/mui/menuBarTemplate.html",
    "../../../../html/menuBar/mui/popoverDateTemplate.html",
    "../../../../html/menuBar/mui/popoverFrequencyTemplate.html",
    "../../../../html/menuBar/mui/popoverEventsTemplate.html",
    "../../../../html/menuBar/mui/popoverEventsIndicatorsTemplate.html",
    "../../../../html/menuBar/mui/popoverFundamentalTemplate.html",
    "../../../../html/menuBar/mui/popoverDrwaingTemplate.html",
    "../../../../html/menuBar/mui/popoverDisplayTemplate.html",
    "../../../../html/menuBar/mui/gear-drill-down.html",
    "../../../core/core",
    "../../../util/util",
    "morningstar",
    "./MenuBarInterval.js",
    "../autoComplete.js",
    "../popover.js"],
    function (menuBarTemplate,
        popoverDateTemplate,
        popoverFrequencyTemplate,
        popoverEventsTemplate,
        popoverEventsIndicatorsTemplate,
        popoverFundamentalTemplate,
        popoverDrwaingTemplate,
        popoverDisplayTemplate,
        settingsTemplate,
        core,
        util,
        morningstar,
        MenuBarIntervalModel,
        AutoCompleteModel,
		PopoverModel) {
        "use strict";

        /**
        * Menubar for main chart type view
        *
        * @constructor
        */

        var asterixUtil = morningstar.asterix.util;
        var $ = core.$;
        var invalidClass = 'mkts-cmpt-svgcht-invalid';
        var invalidLabelClass = 'mkts-cmpt-svgcht-invalid-label';

        var portfolioCommon = morningstar.components.portfolioCommon;
        var DatePicker = null;
        var IconButton = null;
        if(portfolioCommon){
            DatePicker = portfolioCommon.ui.DatePicker;
            IconButton = portfolioCommon.ui.IconButton;
        }

        function model(managers) {
            var eventManager = managers.eventManager;
            var CCM = managers.chartConfigManager;
            var MenuBarInterval = MenuBarIntervalModel(managers);
            var Popover = PopoverModel();
            var AutoComplete = AutoCompleteModel(managers, Popover);
            var $menuBarTemplate;
            var langObj;
            var appConfig;

            /*TEMPORARY UNTIL THIS ASTERIX PULL REQUEST IS MERGED*/
            asterixUtil.isObj = function (o) {
                return typeof o === 'object' && o !== null;
            };

            var MenuBar = function (config) {
                this.config = {
                    container: '',
                    popObj: {},
                    popConfig: '',
                    popName: '',
                };
                this._init(config);
            };

            MenuBar.prototype = {
                /* test-code */
                __private: {
                    CCM: CCM,
                    eventManager: eventManager
                },
                /* end-test-code */
                _init: function (config) {
                    function makePopOvers() {
                        var templates = {
                            frequency: popoverFrequencyTemplate,//Frequency,
                            events: popoverEventsTemplate,//eventsIndicatorsTemplate,
                            eventsIndicators: popoverEventsIndicatorsTemplate,//eventsIndicatorsTemplate,
                            fundamental: popoverFundamentalTemplate,//fundamentalTemplate,
                            drawing: popoverDrwaingTemplate,//drawingTemplate,
                            display: popoverDisplayTemplate,//displayTemplate
                        };
                        var popovers = {};
                        var buttons = null;
                        if( appConfig.initMenubarmui ){
                            buttons = $menuBarTemplate.find('.mkts-cmpt-svgcht-menubtn').not('.mkts-cmpt-svgcht-menubtn--date');
                        } else {
                            buttons = $menuBarTemplate.find('.mkts-cmpt-svgcht-menubtn');
                        }
                        
                        var buttonLen = buttons.length;
                        var name, button, tempFun, markupStr, i;

                        for (i = 0; i < buttonLen; i++) {
                            button = buttons[i];
                            name = button.getAttribute('data-menuid');
                            name = util.dashesToCamel(name);
                            button.popName = name;
                            tempFun = templates[name];
                            markupStr = tempFun(langObj);
                            var $markupStr = $(markupStr);
                            if (name === 'drawing') {
                                button.resetOnOpen = true;
                            }
                            else if (name === 'display') {
                                var configArr = [
                                    appConfig.charts.mainChart.chartType,
                                    appConfig.cursorType,
                                    appConfig.charts.mainChart.selectionMode
                                ];

                                configArr.forEach(function (item) { setOptionsStatus(item, $markupStr) });
                            } else if (name === 'eventsIndicators' || name === 'fundamental') {
                                $markupStr.find(".mkts-cmpt-svgcht-toggle--off,.mkts-cmpt-svgcht-toggle--on").each(function () {
                                    if ($(this).find("div").length === 0) {
                                        var wraper = '<div class="mkts-cmpt-svgcht-indicator-name" title="' + $(this).text() + '"></div>';
                                        $(this).wrapInner(wraper);
                                    }
                                    var onoffBtn = '<div class="mkts-cmpt-svgcht-onoff-btn"> <span class="onoff-left"></span><span class="onoff-right"></span></div>';
                                    $(this).append(onoffBtn);
                                });
                                var defaultOptions = {};
                                if (name === 'eventsIndicators') {
                                    defaultOptions["indicators"] = appConfig.indicators;
                                } else if (name === 'fundamental') {
                                    defaultOptions["fundamentals"] = appConfig.fundamentals;
                                }
                                setDefaultIndictFundm(defaultOptions, $markupStr);
                            }
                            popovers[name] = $markupStr;
                        }

                        return popovers;
                    }

                    function setOptionsStatus(optionValue, $markupStr) {
                        var eid = util.camelToDashes(optionValue);
                        var elem = $markupStr.find("." + eid);
                        var classValue = elem.attr('class');
                        elem.attr('class', classValue + ' active');
                    }
                    function setDefaultIndictFundm(defaultOptions, $markupStr) {
                        var items = [];
                        var eid;
                        var indicatorsLength = defaultOptions.indicators && defaultOptions.indicators.length ||0;
                        var fundamentalsLength = defaultOptions.fundamentals && defaultOptions.fundamentals.length ||0;

                        if( indicatorsLength>0 ){
                            for (var i=0; i<indicatorsLength; i++) {
                                eid = util.camelToDashes(defaultOptions.indicators[i].name);
                                $markupStr.find("." + eid).removeClass("mkts-cmpt-svgcht-toggle--off").addClass("mkts-cmpt-svgcht-toggle--on");
                                var drawer = $markupStr.find("." + eid).next('.mkts-cmpt-svgcht-drawer, .mkts-cmpt-svgcht-drawer--expand');
                                $(drawer).removeClass('mkts-cmpt-svgcht-drawer').addClass('mkts-cmpt-svgcht-drawer--expand');
                                var elm = $markupStr.find("." + eid)[0];
                                elm.toRemove = false;
                                self.activeIndicators[defaultOptions.indicators[i].name] = {
                                    el: elm,
                                    options: defaultOptions.indicators[i].parameters
                                }
                            }
                        }
                        
                        if( fundamentalsLength.length>0 ){
                            for (var i=0; i<fundamentalsLength.length; i++) {
                                eid = util.camelToDashes(defaultOptions.fundamentals[i]);
                                $markupStr.find("." + eid).removeClass("mkts-cmpt-svgcht-toggle--off").addClass("mkts-cmpt-svgcht-toggle--on");
                                self.activeFundamentals[defaultOptions.fundamentals[i]] = {
                                    el: $markupStr.find("." + eid)[0],
                                }
                            }
                        }
                    }
                    var self = this;

                    /* test-code */
                    self.__private._init = {
                        makePopOvers: makePopOvers
                    };
                    /* end-test-code */

                    appConfig = CCM.getConfig();
                    var locale = appConfig.lang || 'en-US';

                    langObj = CCM.getLabels('markets-components-svgchart', locale);

                    $.extend(true, self.config, config);

                    $menuBarTemplate = $(menuBarTemplate(langObj));

                    if (appConfig.enterTipsLabelID) {
                        $menuBarTemplate.find(".mkts-cmpt-svgcht-search-input").attr("placeholder", langObj['search-assets']);
                    }

                    self._conditionalLoad();
                    self.activeIndicators = {};
                    self.activeFundamentals = {};
                    self.popovers = makePopOvers();
                    /* test-code */
                    self.__private.activeIndicators = self.activeIndicators;
                    self.__private.activeFundamentals = self.activeFundamentals;
                    /* end-test-code */

                    self._renderView();
                    self._hideMenuItemsAndSetPremium();
                    self._hideMenuIcons();
                    self._hideMenuButtons();
                    self._bindEvents();
                },
                hidePopover: function () {
                    Popover.hide();
                },
                _conditionalLoad: function () {
                    var testDiv = document.createElement('div');

                    //conditionally load overrides for browsers without flexbox (ie9)
                    $('body').append(testDiv);
                    if (testDiv.style.flexDirection === undefined && testDiv.style.webkitFlexDirection === undefined && testDiv.style.msFlexDirection === undefined) {
                        var $container = $(CCM.getConfig().container);
                        $container.addClass("mkts-cmpt-svgcht-noFlex");
                    }
                    $(testDiv).remove();

                },
                _hideMenuItemsAndSetPremium: function () {
                    function hideElements() {
                        var key = 'hide' + util.capFirstChar(curPop);
                        var hides = menubarCfg[key];
                        var i, l;

                        if (hides && $.isArray(hides)) {
                            l = hides.length;

                            for (i = 0; i < l; i++) {
                                $el.find('.' + hides[i]).hide();
                            }
                        }
                    }
                    function findPremium() {
                        function disablePremium(el) {
                            var $parEl = $(el.parentNode);

                            $parEl.removeClass('mkts-cmpt-svgcht-popover-body mkts-cmpt-svgcht-toggle--off')
                                .addClass('mkts-cmpt-svgcht-popover-body-disabled')
                                .attr('data-linkbinding', 'history-fairvalue');
                        }

                        function hidePremiumMark(el) {
                            var $el = $(el);
                            $el.css({
                                'background': 'none',
                                'padding-left': '0px'
                            });
                        }

                        var premItms = $el.find('.mkts-cmpt-svgcht-premium-chic');
                        var premLen = premItms.length;
                        var i;

                        if (!appConfig.premiumUser) {
                            for (i = 0; i < premLen; i++) {
                                disablePremium(premItms[i]);
                            }
                        } else {
                            for (i = 0; i < premLen; i++) {
                                hidePremiumMark(premItms[i]);
                            }
                        }

                    }
                    var self = this;
                    var menubarCfg = appConfig.menubar;
                    var popovers = self.popovers;
                    var curPop, $el;


                    //conditionally hide items for listed in config
                    for (curPop in popovers) {
                        if (popovers.hasOwnProperty(curPop)) {
                            $el = self.popovers[curPop];
                            hideElements();
                            findPremium();
                        }
                    }

                    //hide interval items
                    curPop = 'Interval';
                    $el = $menuBarTemplate;
                    hideElements();

                    if (appConfig.showCloseBtn) {
                        $menuBarTemplate.find('.mkts-cmpt-svgcht-menuicn--remove').show();
                    }
                },
                _hideMenuIcons: function () {
                    var resetFlag = appConfig.hideResetIcon;
                    var hideExport = appConfig.hideExport;

                    if (resetFlag) {
                        $menuBarTemplate.find('.mkts-cmpt-svgcht-menuicn--reset').hide();
                    }
                    if (hideExport) {
                        $menuBarTemplate.find('.mkts-cmpt-svgcht-menuicn--export').hide();
                    }
                },
                _hideMenuButtons: function () {
                    var buttons = appConfig.menubar.hideButtons;
                    var l, i;

                    if (asterixUtil.isArray(buttons) && buttons.length > 0) {
                        l = buttons.length;
                        for (i = 0; i < l; i++) {
                            $menuBarTemplate.find('.' + buttons[i]).hide();
                        }
                    }
                },
                _renderView: function (defaultRange) {
                    var intervalInst;
                    this.$el = this.config.container;
                    $menuBarTemplate.appendTo(this.$el);

                    // date picker
                    this.createDatePicker();

                    intervalInst = new MenuBarInterval({
                        container: this.$el.find('.mkts-cmpt-svgcht-menubar-bottom')
                    });

                    this.intervalInst = intervalInst;
                    /* test-code */
                    this.__private.menuIntervalInst = intervalInst;
                    /* test-code */

                    new AutoComplete({ $menuBarContainer: this.$el });
                    
                },
                createDatePicker: function(){
                    var self = this;
                    var $container = self.$el.find('.mkts-cmpt-svgcht-menubtn.mkts-cmpt-svgcht-menubtn--date');
                    var $dropdownArrow = $container.find('.mkts-cmpt-svgcht-menubtn-arrow');
                    var nowItemShowValue = "1D";
                    
                    if(DatePicker && IconButton){
                        self.datePickerRange = new DatePicker();
                        self.datePickerRange.bind("change", function(e, d) {
                            nowItemShowValue = d.value;
                            eventManager.trigger(eventManager.EventTypes.ChangeDateByDateRange, d);
                            console.debug(eventManager.EventTypes.ChangeDateByDateRange, d);
                        });

                        self.buttonRange = new IconButton({
                            container: $('<div></div>').addClass("pull-left").appendTo($container),
                            data: {
                                label: "",
                                value: ""
                            }
                        });


                        $container.bind("click", function(e) {
                            showDateRangePopover(e);
                        });
                        $dropdownArrow.on("click", function(e){
                            showDateRangePopover(e);
                        });

                        // bind the hover button color change event
                        $container.on({
                            mouseover: function(e){
                                $container.find('.mkts-cmpt-svgcht-menubtn-text').css("color", "#fff");
                                $container.find('.mkts-cmpt-svgcht-menubtn-arrow').css("color", "#fff");
                            },
                            mouseleave: function(e){
                                $container.find('.mkts-cmpt-svgcht-menubtn-text').css("color", "#151515");
                                $container.find('.mkts-cmpt-svgcht-menubtn-arrow').css("color", "#151515");
                            }
                        });

                        var addBlurListener = function(){
                            $(document).on('click.mapopover', function (e) {
                                e.preventDefault();
                                e.stopPropagation();

                                $container.removeClass('active');
                                offBlurListener();
                            });
                        }
                        var offBlurListener = function(){
                            $(document).off('.mapopover');
                        }

                        var showDateRangePopover = function(e){
                            self.hidePopover();
                            self.datePickerRange.show({
                                target: $container,
                                type: "range",
                                start_date: {
                                    value: 29544
                                },
                                end_date: {
                                    value: 34
                                },
                                maxHeight: 420,
                                // style: {
                                //     width: 160,
                                //     height: 140
                                // },
                                range: {
                                    title: "Date Range",
                                    value: nowItemShowValue,
                                    bt_back: false,
                                    bt_done: false,
                                    list: [{
                                        label: "1D",
                                        value: "1D",
                                        "data-menuid": "one-day"
                                    }, {
                                        label: "5D",
                                        value: "5D",
                                        "data-menuid": "five-day"
                                    }, {
                                        label: "15D",
                                        value: "15D",
                                        "data-menuid": "fifteen-day"
                                    }, {
                                        label: "1M",
                                        value: "1M",
                                        "data-menuid": "one-month"
                                    }, {
                                        label: "3M",
                                        value: "3M",
                                        "data-menuid": "three-month"
                                    }, {
                                        label: "6M",
                                        value: "6M",
                                        "data-menuid": "six-month"
                                    }, {
                                        label: "YTD",
                                        value: "YTD",
                                        "data-menuid": "y-t-d"
                                    }, {
                                        label: "1Y",
                                        value: "1Y",
                                        "data-menuid": "one-year"
                                    }, {
                                        label: "3Y",
                                        value: "3Y",
                                        "data-menuid": "three-year"
                                    }, {
                                        label: "5Y",
                                        value: "5Y",
                                        "data-menuid": "five-year"
                                    }, {
                                        label: "10Y",
                                        value: "10Y",
                                        "data-menuid": "ten-year"
                                    }, {
                                        label: "MAX",
                                        value: "MAX",
                                        "data-menuid": "max"
                                    }, {
                                        label: "Custom",
                                        value: "range_custom"
                                    }]
                                },
                            });
                            addBlurListener();
                        }
                    }
                },
                _bindEvents: function () {
                    var self = this;
                    var popObj = this.config.popObj;
                    var popConfig = this.config.popConfig;
                    var popName = this.config.popName;
                    var timer;
                    function onMenuButtonClick(e) {
                        self.$el.find('.mkts-cmpt-svgcht-menubtn').each(function(index, element){
                            $(element).removeClass('active');  
                        });

                        e.preventDefault();
                        e.stopPropagation();
                        $(e.currentTarget).addClass('active');

                        if(this.popName){
                            self.datePickerRange.close();
                        }

                        if (asterixUtil.isString(this.popName)) {

                            popName = this.popName;
                            popObj.$el = self.popovers[this.popName];
                            if (this.resetOnOpen) {
                                popObj.$el.find('.mkts-cmpt-svgcht-popover-body').removeClass('active');
                            }

                            popConfig = {
                                $target: $(this),
                                styleClass: 'mkts-cmpt-svgcht-popover--' + this.popName,
                                $componentContainer: appConfig.$componentContainer,
                                $menuBarContainer: self.$el
                            };
                            Popover.show(popObj, popConfig);
                        }
                    }

                    function changeLayout() {
                        if (popObj != null && popConfig != "") {
                            if (asterixUtil.isString(popName)) {
                                popConfig = {
                                    $container: self.$el,
                                    $target: $(this),
                                    styleClass: 'mkts-cmpt-svgcht-popover--' + popName
                                };
                                if (popConfig.$container.find('.' + popConfig.styleClass).length > 0) {
                                    Popover.adjustPosition();
                                }
                            }
                        }
                    }
                    function onLayoutChange() {
                        clearTimeout(timer);
                        timer = setTimeout(changeLayout, 100);
                    }

                    function makeArrayFromInputs(el) {
                        var ar = [];
                        var inpts, inptLen, i;

                        if (asterixUtil.isObj(el) && el.length > 0) {
                            inpts = $(el).find('input');
                            inptLen = inpts.length;
                            for (i = 0; i < inptLen; i++) {
                                if (asterixUtil.isString(inpts[i].value) && inpts[i].value !== '') {
                                    var numValue = parseFloat(inpts[i].value, 10);
                                    if (!isNaN(numValue)) {
                                        ar.push(parseFloat(numValue));
                                    }
                                } else {
                                    ar.push(null);
                                }
                            }
                        }

                        return ar;
                    }

                    function toggleDrawer(el) {
                        var drawer = $(el).next('.mkts-cmpt-svgcht-drawer, .mkts-cmpt-svgcht-drawer--expand');

                        if (el.toRemove === undefined || el.toRemove) {
                            el.toRemove = false;
                        } else {
                            el.toRemove = true;
                        }

                        $(el).toggleClass('mkts-cmpt-svgcht-toggle--on mkts-cmpt-svgcht-toggle--off');
                        $(drawer).toggleClass('mkts-cmpt-svgcht-drawer mkts-cmpt-svgcht-drawer--expand');
                        if (drawer.length > 0) {
                            if (el.className.indexOf('on') > 0) {
                                $(el).addClass('noBorder');
                            } else {
                                $(el).removeClass('noBorder');
                            }
                        }
                    }

                    function onMenuItemClick(e) {
                        function manageVolumeState() {
                            var popovers = self.popovers;
                            var toggleString = 'mkts-cmpt-svgcht-toggle--on mkts-cmpt-svgcht-toggle--off';
                            var $vol = popovers.eventsIndicators.find('.volume-chart');
                            var $volPlus = popovers.display.find('.volume-plus-chart');

                            if (eventType === 'ChangeChartVolume') {
                                if ($vol.hasClass('mkts-cmpt-svgcht-toggle--off') && $volPlus.hasClass('mkts-cmpt-svgcht-toggle--on')) {
                                    $volPlus.toggleClass(toggleString);
                                    eventManager.trigger(eventManager.EventTypes.ChangeChartVolumePlus, false);
                                    console.debug(eventManager.EventTypes.ChangeChartVolumePlus, false);
                                }
                            } else if (eventType === 'ChangeChartVolumePlus') {
                                if ($volPlus.hasClass('mkts-cmpt-svgcht-toggle--on') && $vol.hasClass('mkts-cmpt-svgcht-toggle--off')) {
                                    $vol.toggleClass(toggleString);
                                    eventManager.trigger(eventManager.EventTypes.ChangeChartVolume, true);
                                    console.debug(eventManager.EventTypes.ChangeChartVolume, true);
                                }
                            }
                        }
                        function manageVolumeValue() {
                            eventVal = false;
                            if (el.className.indexOf('--on') > 0) {
                                eventVal = true;
                            }
                        }
                        function processElement() {
                            if ($(el).attr("id") === "reset") { return; }

                            var items = $(parent).find('.mkts-cmpt-svgcht-popover-body');
                            if (toggle) {
                                toggleDrawer(el);
                            } else {
                                $(items).removeClass('active');
                                $(el).addClass('active');
                            }
                        }
                        function assignActiveIndicators() {
                            inputContainer = $(el).next('.mkts-cmpt-svgcht-drawer, .mkts-cmpt-svgcht-drawer--expand');
                            if (el.toRemove === false) {
                                indicatorArgs = makeArrayFromInputs(inputContainer);
                                self.activeIndicators[eventVal] = {
                                    el: el,
                                    options: indicatorArgs
                                };
                            }
                        }
                        function assingActiveFundamentals() {
                            if (el.toRemove === false) {
                                self.activeFundamentals[eventVal] = {
                                    el: el
                                };
                            }
                        }
                        function fireNormalEvent() {
                            if (window._satellite){
                                try{
                                    _satellite.setVar('CategoryName', eventManager.EventTypes[eventType]);
                                    _satellite.setVar('EventVal', eventVal);
                                    _satellite.track('DropDown');
                                }catch(e){}
                            }
                            showSelectedItemsNumber();
                            eventManager.trigger(eventManager.EventTypes[eventType], eventVal);
                            console.debug(eventManager.EventTypes[eventType], eventVal);
                        }
                        function fireParameteredEvent() {
                            inputContainer = $(el).next('.mkts-cmpt-svgcht-drawer, .mkts-cmpt-svgcht-drawer--expand');
                            if (el.toRemove === false) {
                                indicatorArgs = makeArrayFromInputs(inputContainer);
                                self.activeIndicators[eventVal] = {
                                    el: el,
                                    options: indicatorArgs
                                };
                            }
                            if (window._satellite){
                                try{
                                    _satellite.setVar('CategoryName', eventManager.EventTypes.ChangeIndicators);
                                    _satellite.setVar('EventVal', eventVal);
                                    _satellite.track('DropDown');
                                }catch(e){}
                            }
                            showSelectedItemsNumber();
                            eventManager.trigger(eventManager.EventTypes.ChangeIndicators, el.toRemove, eventVal, indicatorArgs);
                            console.debug(eventManager.EventTypes.ChangeIndicators, el.toRemove, eventVal, indicatorArgs);
                        }
                        function showSelectedItemsNumber(){
                            var $target = popConfig["$target"];
                            var activeNumber = $(parent).find('.mkts-cmpt-svgcht-toggle--on').length;
                            if(activeNumber==0){
                                $target.find('.toggle-on-number').addClass('invisible');
                            } else {
                                $target.find('.toggle-on-number').removeClass('invisible');
                                $target.find('.toggle-on-number').html('('+activeNumber+')');
                            }
                        }
                        function fireFrequencyParameteredEvent() {
                            var innerCall = e;
                            changeFrequencyButtonText($(el).html());
                            eventManager.trigger(eventManager.EventTypes.ChangeFrequencyFromMenu, innerCall, el, eventVal);
                            console.debug(eventManager.EventTypes.ChangeFrequencyFromMenu, innerCall, el, eventVal);
                        }
                        function hidePopoverAfterSelect(){
                            if(appConfig.isHidePopoverAfterSelect){
                                self.hidePopover();
                            }
                        }
                        function processEventType() {
                            switch (eventType) {
                                case 'ChangeFrequency':
                                    eventVal = util.capFirstChar(eventVal);
                                    fireFrequencyParameteredEvent();
                                    hidePopoverAfterSelect();
                                    break;
                                case 'ChangeFundamental':
                                    eventVal = util.capFirstChar(eventVal);
                                    assingActiveFundamentals();
                                    fireNormalEvent();
                                    hidePopoverAfterSelect();
                                    break;
                                case 'ChangeIndicators':
                                    eventVal = util.capFirstChar(eventVal);
                                    assignActiveIndicators();
                                    fireParameteredEvent();
                                    hidePopoverAfterSelect();
                                    break;
                                case 'ChangeChartVolumePlus':
                                    //intentional fall through, both event types should be handled the same
                                case 'ChangeChartVolume':
                                    manageVolumeState();
                                    manageVolumeValue();
                                    fireNormalEvent();
                                    hidePopoverAfterSelect();
                                    break;
                                default:
                                    fireNormalEvent();
                                    hidePopoverAfterSelect();
                            }
                        }
                        var el = this;
                        var parent = el.parentNode;
                        var eventType;
                        var eventVal;
                        var inputContainer;
                        var toggle = el.className.indexOf('mkts-cmpt-svgcht-toggle') > 0;
                        var indicatorArgs = [];

                        e.preventDefault();
                        e.stopPropagation();

                        if (asterixUtil.isString(el.getAttribute('data-menuid')) && asterixUtil.isObj(parent) && asterixUtil.isString(parent.getAttribute('data-menuid'))) {
                            eventType = util.dashesToCamel(parent.getAttribute('data-menuid'));
                            eventType = util.capFirstChar(eventType);
                            eventVal = util.dashesToCamel(el.getAttribute('data-menuid'));
                            processElement();
                            processEventType();
                        }
                    }

                    function onIndicatorChagne(remove, indicator, options, removeAll, isFromCloseBtn) {
                        function allNull(array) {
                            var isNull = true;
                            var len = array.length || 0;
                            var i;

                            for (i = 0; i < len; i++) {
                                if (array[i] !== null) {
                                    isNull = false;
                                    break;
                                }
                            }

                            return isNull;
                        }
                        function updateActiveIndicators() {
                            indicatorArray.splice(index, 1);
                            if (indicatorArray.length === 0 || allNull(indicatorArray) || removeAll) {
                                toggleDrawer(indicatorEl);
                            }
                        }
                        function removeValue() {
                            if (asterixUtil.isObj(self.activeIndicators[indicator])) {
                                indicatorArray = self.activeIndicators[indicator].options;
                                indicatorEl = self.activeIndicators[indicator].el;
                                if (isFromCloseBtn && asterixUtil.isNumber(options)) {
                                    index = indicatorArray.indexOf(options);
                                    if (index > -1) {
                                        //if clearing individual fields as they're cleared from the chart is requested, this is where that function should be called
                                        //get an array of the input fields, and find the field from this index, then set it to ''
                                        updateActiveIndicators();
                                    }
                                } else if (isFromCloseBtn) {
                                    updateActiveIndicators();
                                }
                            }
                        }
                        var indicatorArray, indicatorEl, index;
                        if (remove) {
                            removeValue();
                        };
                    }
                    function onFundamentalChagne(fun, remove) {
                        var funEl;

                        if (remove === true) {
                            if (asterixUtil.isObj(self.activeFundamentals[fun])) {
                                funEl = self.activeFundamentals[fun].el;
                                toggleDrawer(funEl);
                            }
                        }
                    }

                    function onUpdateIndicator() {
                        var inputContainer = $(this).closest('.mkts-cmpt-svgcht-drawer--expand');
                        var el = inputContainer.prev('.mkts-cmpt-svgcht-popover-body')[0];
                        var parent = el.parentNode;
                        var eventType;
                        var eventVal;
                        var indicatorArgs;
                        var $invalidInputs = inputContainer.find('.' + invalidClass);
                        if ($invalidInputs.length > 0) {
                            $invalidInputs.animate({ borderWidth: '5px' }, 'fast').animate({ borderWidth: '1px' }, 'fast').focus();
                        } else if (asterixUtil.isString(el.getAttribute('data-menuid'))
                                && asterixUtil.isObj(parent)
                                && asterixUtil.isString(parent.getAttribute('data-menuid'))) {
                            eventType = util.dashesToCamel(parent.getAttribute('data-menuid'));
                            eventType = util.capFirstChar(eventType);
                            eventVal = util.dashesToCamel(el.getAttribute('data-menuid'));
                            eventVal = util.capFirstChar(eventVal);
                            indicatorArgs = makeArrayFromInputs(inputContainer);
                            self.activeIndicators[eventVal] = {
                                el: el,
                                options: indicatorArgs
                            };

                            eventManager.trigger(eventManager.EventTypes.ChangeIndicators, false, eventVal, indicatorArgs);
                            console.debug(eventManager.EventTypes.ChangeIndicators, false, eventVal, indicatorArgs);
                        }
                    }

                    function onInputKeyPress(event) {
                        return;
                        var $target = $(event.currentTarget);
                        var inputType = $target.attr('data-inputtype');
                        var inputValid = false;
                        var inputValue = $target.val();
                        switch (inputType) {
                            case CCM.indicatorInputTypes.NecessaryPositiveInteger:
                                inputValid = /^[0-9]*[1-9][0-9]*$/.test(inputValue);
                                break;
                            default:
                                inputValid = (inputValue === '' || /^[0-9]*[1-9][0-9]*$/.test(inputValue));
                                break;
                        }

                        var invalidLabel = $target.parent().next('div.' + invalidLabelClass);
                        if (inputValid) {
                            $target.removeClass(invalidClass);
                            invalidLabel.remove();
                        } else {
                            $target.addClass(invalidClass);
                            if (invalidLabel.length === 0) {
                                $('<div>').addClass(invalidLabelClass)
                                        .html(langObj['invalid-input-' + (inputType || 'default')])
                                        .insertAfter($target.parent());
                            }
                        }
                    }

                    function onMenuIconClick() {
                        if (asterixUtil.isString(this.getAttribute('data-menuid'))) {
                            var eventType = util.capFirstChar(this.getAttribute('data-menuid').replace('mkts-cmpt-svgcht-menuicn--', ''));

                            if (eventManager.EventTypes[eventType]) {

                                eventManager.trigger(eventManager.EventTypes[eventType]);
                                console.debug(eventManager.EventTypes[eventType]);
                            }
                        }
                    }

                    function destroyMenubar() {
                        appConfig.$componentContainer.off('.mkts-cmpt-svgcht-menu');
                    }

                    function changeFrequencyItems(nfc, defalutIndex) {
                        var $el = self.$el;
                        var $frequencyPopover = self.popovers["frequency"];
                        var valueArray = [];
                        for (var i = 0; i < nfc.length; i++) {
                            if(nfc[i].value){
                                valueArray.push(nfc[i].value);
                            }
                        };
                        var defalutActiveItemId = valueArray[defalutIndex];

                        $frequencyPopover.find('.mkts-cmpt-svgcht-frequency-option').each(function(e){
                            var menuid = $(this).attr('data-menuid');
                            $(this).removeClass('hide').addClass('hide');
                            $(this).removeClass('active');
                            if( $.inArray(menuid, valueArray)>-1){
                                $(this).removeClass('hide');
                            }
                            if( menuid === defalutActiveItemId){
                                $(this).addClass('active');
                                changeFrequencyButtonText($(this).html());
                            }
                        });
                    }

                    function changeFrequencyButtonText(text){
                        self.$el.find('.mkts-cmpt-svgcht-menubtn--frequency .mkts-cmpt-svgcht-menubtn-text').html(text);
                    }

                    eventManager.bind(eventManager.EventTypes.ChangeIndicators, onIndicatorChagne);
                    eventManager.bind(eventManager.EventTypes.ChangeFundamental, onFundamentalChagne);
                    eventManager.bind(eventManager.EventTypes.LayoutChange, onLayoutChange);
                    eventManager.bind(eventManager.EventTypes.ChangeFrequencyItems, changeFrequencyItems);
                    eventManager.bind(eventManager.EventTypes.DestroyChart, destroyMenubar);
                    self.intervalInst._updateFreq();

                    appConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-menubtn', onMenuButtonClick);
                    appConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-popover-body', onMenuItemClick);
                    appConfig.$componentContainer.on('keyup.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-text-box', onInputKeyPress);
                    appConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-btn-update', onUpdateIndicator);
                    appConfig.$componentContainer.on('click.mkts-cmpt-svgcht-menu', '.mkts-cmpt-svgcht-menuicn', onMenuIconClick);
                }
            };

            return MenuBar;
        }

        return model;
    });
