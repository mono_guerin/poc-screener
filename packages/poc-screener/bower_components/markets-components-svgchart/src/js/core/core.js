﻿define(['morningstar', 'd3'], function (morningstar, d3) {
    'use strict';
    /**
    * Define the component name for namespace.
    */
    var marketscore = 'markets-components-core';
    /**
    * Registar the global namespace name for the whole component
    */
    var QSAPI = morningstar.components[marketscore];

    QSAPI.d3 = d3;

    return QSAPI;
});
