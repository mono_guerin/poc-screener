define(['../../core/core', './graphTypes', './config', '../../util/util', 'moment', 'd3', './locale'],
    function(core, GraphTypes, toolConfig, util, moment, d3, locale) {
        'use strict';
        /**
         * Chart Config Manage Model
         */

        var $ = core.$;
        var asterixUtil = morningstar.asterix.util;
        var ChartTypes = toolConfig.ChartTypes;
        var MainChartTypes = toolConfig.MainChartTypes;
        var MainDataTypes = toolConfig.MainDataTypes;
        var VolumeChartTypes = toolConfig.VolumeChartTypes;
        var FundamenatlChartTypes = toolConfig.FundamenatlChartTypes;
        var IndicatorChartTypes = toolConfig.IndicatorChartTypes;
        var IntervalTypesMapping = toolConfig.IntervalTypesMapping;
        var ExternalTypesMapping = toolConfig.ExternalTypesMapping;
        var IntervalTypes = toolConfig.IntervalTypes;
        var FrequencyTypes = toolConfig.FrequencyTypes;
        var FrequencyTexts = toolConfig.FrequencyTexts;
        var CursorType = toolConfig.CursorType;
        var Constants = toolConfig.Constants;
        var frequencyConfig = toolConfig.frequencyConfig;
        var TimezoneMap = toolConfig.TimezoneMap;
        var intervalTypeSortedArray = toolConfig.IntervalTypeSortedArray;
        var SelectionModes = toolConfig.SelectionModes;
        var PopoverTypes = toolConfig.PopoverTypes;
        var VerticalScaleTypes = toolConfig.VerticalScaleTypes;
        var ChartGenre = toolConfig.ChartGenre;
        var ChartTypeCategoryMap = toolConfig.ChartTypeCategoryMap;
        var AutocompleteCategories = toolConfig.AutocompleteCategories;
        var DataPoints = core.Widget.Chart.DataPoints;
        var appConfig = {
            'settings': {},
            'components': {
                'markets-components-svgchart': {
                    'type': 'svgchart',
                    'settings': {},
                    'labels': {},
                }
            },
            'labels': {}
        };

        var baseLabels = {
            'en': {}
        };

        var throttle = function(fn, delay) {
            var timer = null;
            return function() {
                var context = this,
                    args = arguments;

                clearTimeout(timer);
                timer = setTimeout(function() {
                    fn.apply(context, args);
                }, delay)
            }
        }

        function configManager(managers) {
            var eventManager = managers.eventManager;
            var eventTypes = eventManager.EventTypes;
            var lastCursorType;
            var mainTickerObject, tickerCollection = [];
            var config;
            var initedConfig;
            var dataAdapter = managers.dataAdapter;
            var initCallback;
            var customTickerNameField = 'customTickerName';
            var customTickerColorField = 'customTickerColor';
            var customTickerCurrencyField = 'customCurrency';
            var checkList = ['mType', 'type'];
            var labelsValues = {};

            function createConfig(extendConfig, callback) {
                initCallback = callback;
                lastCursorType = undefined;
                extendConfig = extendConfig || {};
                if (extendConfig.frequency) {
                    extendConfig.defaultFrequency = extendConfig.frequency;
                }
                extendConfig.charts = extendConfig.charts || {};
                extendConfig.charts.mainChart = extendConfig.charts.mainChart || {};
                var savedConfig = extendConfig.savedConfig;
                if (savedConfig) {
                    delete extendConfig.savedConfig;
                }
                // if invoke createConfig from internal, not need extend.
                if (!extendConfig.createConfigInternal) {
                    initedConfig = $.extend(true, {}, extendConfig);
                }
                extendConfig = $.extend(true, {}, extendConfig, savedConfig);
                extendConfig.width && (extendConfig.width = parseInt(extendConfig.width, 10));
                extendConfig.height && (extendConfig.height = parseInt(extendConfig.height, 10));
                if (!extendConfig.solutionKey) {
                    extendConfig.useEUTimeSeriesData = false;
                    extendConfig.useEUSecurityData = false;
                    if (extendConfig.autoComplete && extendConfig.autoComplete.dataSource === 'EU') {
                        extendConfig.autoComplete.dataSource = 'US';
                    }
                }
                if (extendConfig.premiumUser === undefined) {
                    var instID = core.getInstID().toUpperCase();
                    if (instID === 'QSDEMO' || instID === 'QUEST') {
                        extendConfig.premiumUser = true;
                    }
                }
                var oriConfig = toolConfig.defaultConfig['svgchart'].settings;
                config = $.extend(true, {}, oriConfig);
                if (extendConfig.lang) {
                    extendConfig.lang = core.Util.standardizeLangSymbol(extendConfig.lang);
                }
                //configuration for NavAndPrice chart
                if (extendConfig.drawNavAndPriceChart) {
                    extendConfig.legendInfoLabel = "tenforeTicker";
                    extendConfig["intervalType"] = IntervalTypes.OneMonth;
                    extendConfig["compareClose"] = false;
                }

                if (extendConfig.startPeriod) {
                    var transforedIntervalType = IntervalTypesMapping[extendConfig.startPeriod];
                    if (transforedIntervalType) {
                        extendConfig.intervalType = transforedIntervalType;
                    }
                    delete extendConfig.startPeriod;
                }
                var theme = (extendConfig.theme || config.theme) + '';
                config.brushImages = extendConfig.brushImages;
                config.compareColors = extendConfig.compareColors;
                config.tagMode = extendConfig.tagMode;
                extendConfig.menubar = extendConfig.menubar || {};
                for (var menu in extendConfig.menubar) {
                    var hides = [];
                    if ($.inArray(menu), ['hideEventsIndicators', 'hideFundamentals', 'hideDisplay', 'hideDrawings']) {
                        var hides = extendConfig.menubar[menu];
                        for (var i = 0; i < hides.length; i++) {
                            hides[i] = util.camelToDashes(hides[i]);
                        }
                        config.menubar[menu] = util.arrConcat(config.menubar[menu], hides || []);
                    }
                }
                delete extendConfig.menubar;
                loadTheme(theme);
                if (extendConfig.intervalBreakPointMap) {
                    for (var bp in extendConfig.intervalBreakPointMap) {
                        config.intervalBreakPointMap[bp] = extendConfig.intervalBreakPointMap[bp];
                    }
                    delete extendConfig.intervalBreakPointMap;
                }
                if (extendConfig.intervalFrequencyMap) {
                    for (var key in extendConfig.intervalFrequencyMap) {
                        extendConfig.intervalFrequencyMap[IntervalTypesMapping[key] || key] = extendConfig.intervalFrequencyMap[key];
                    }
                }
                extendConfig.showGridBlocks && changeShowGridBlocks(extendConfig.showGridBlocks);
                extendConfig.showSolidGridLines && changeShowSolidGridLines(extendConfig.showSolidGridLines);
                if (extendConfig.lang) {
                    config.lang = extendConfig.lang;
                }
                var d3Locale = getD3Locale();
                if (d3Locale.dailyDateFormatter) {
                    config.dailyDateFormatter = d3Locale.dailyDateFormatter;
                }
                if (d3Locale.intradayDateFormater) {
                    config.intradayDateFormater = d3Locale.intradayDateFormater;
                }
                if (d3Locale.momentDateTime) {
                    config.momentDateTime = d3Locale.momentDateTime;
                }
                if (d3Locale.momentDate) {
                    config.momentDate = d3Locale.momentDate;
                }
                if (d3Locale.columnDelimiter) {
                    config.columnDelimiter = d3Locale.columnDelimiter;
                }
                $.extend(true, config, extendConfig);
                if (config.useEUTimeSeriesData && !extendConfig.legendInfoLabel) {
                    config.legendInfoLabel = 'Name';
                }
                adapteMainChartType(extendConfig.charts.mainChart.chartType, extendConfig.charts.mainChart.dataType);
                menuBarConfigProcess();
                config.charts.sliderChart.show = (config.charts.sliderChart.show === false || config.hideSlider === true) ? false : true;
                config.charts.volumeChart.show = (config.charts.volumeChart.show === false || config.hideVolume === true) ? false : true;

                //adjust autocomplete config but make it backward compatibility
                if (typeof config.autoComplete.highlight === 'undefined') {
                    config.autoComplete.highlight = config.autoCompleteHighLight === true ? true : false;
                }
                if (typeof config.autoComplete.action === 'undefined') {
                    config.autoComplete.action = config.autoCompleteAction === 'main' ? 'main' : 'compare';
                }
                if (typeof config.autoComplete.changeTicker === 'undefined') {
                    config.autoComplete.changeTicker = config.autoChangeTicker === false ? false : true;
                }
                if (typeof config.autoComplete.nonFilterCategory === 'undefined') {
                    config.autoComplete.nonFilterCategory = config.autocompleteNonFilterCategories && config.autocompleteNonFilterCategories.length > 0 ? config.autocompleteNonFilterCategories : [];
                }
                if (config.autoComplete.nonFilterCategory === "All") {
                    config.autoComplete.nonFilterCategory = ["All"];
                }
                if (typeof config.autoComplete.group === 'undefined') {
                    config.autoComplete.group = config.isAutocompleteGroup === false ? false : true;
                }

                if (config.yAxisOrient && (config.yAxisOrient === 'right' || config.yAxisOrient === 'left')) {
                    config.charts.mainChart.yAxis.orient = config.charts.volumeChart.yAxis.orient = config.charts.indicatorChart.yAxis.orient = config.charts.fundamentalChart.yAxis.orient = config.yAxisOrient;
                }
                if (config.showYAxisTickLine == false) {
                    config.charts.mainChart.yAxis.tick.line.show = config.charts.volumeChart.yAxis.tick.line.show = config.charts.indicatorChart.yAxis.tick.line.show = config.charts.fundamentalChart.yAxis.tick.line.show = config.showYAxisTickLine;
                }
                if (config.yAxisWidth && !isNaN(config.yAxisWidth)) {
                    config.charts.mainChart.yAxis.width = config.charts.volumeChart.yAxis.width = config.charts.indicatorChart.yAxis.width = config.charts.fundamentalChart.yAxis.width = config.minYAxisWidth = config.yAxisWidth = Number(config.yAxisWidth);
                }

                if (config.yAxisPosition && (config.yAxisPosition === 'inner' || config.yAxisPosition === 'outer')) {
                    config.charts.mainChart.yAxis.position = config.charts.volumeChart.yAxis.position = config.charts.indicatorChart.yAxis.position = config.charts.fundamentalChart.yAxis.position = config.yAxisPosition;
                }

                if (config.yAxisPosition === 'inner') {
                    config.charts.indicatorChart.yAxis.ticks = 4;
                    config.charts.fundamentalChart.yAxis.ticks = 4;
                }
                if (config.yAxisMargin) {
                    config.charts.mainChart.yAxisMargin = config.charts.volumeChart.yAxisMargin = config.charts.indicatorChart.yAxisMargin = config.charts.fundamentalChart.yAxisMargin = config.yAxisMargin = Number(config.yAxisMargin);
                }
                if (config.hideMainYAxis) {
                    config.charts.mainChart.yAxis.show = false;
                }
                if (config.hideMainXAxis) {
                    config.charts.mainChart.xAxis.show = false;
                }
                if (config.hideVolumeYAxis) {
                    config.charts.volumeChart.yAxis.show = false;
                }
                if (config.hideSliderXAxis) {
                    config.charts.sliderChart.xAxis.show = false;
                }
                if (!config.showDiscountCumFair) {
                    config.menubar.hideFundamentals = util.arrConcat(config.menubar.hideFundamentals, ['discount-cum-fair']);
                }
                if (!config.showNAV) {
                    config.menubar.hideFundamentals = util.arrConcat(config.menubar.hideFundamentals, ['daily-cum-fair-nav']);
                }
                if (!config.showPremiumDiscount) {
                    config.menubar.hideFundamentals = util.arrConcat(config.menubar.hideFundamentals, ['premium-discount']);
                }
                if (config.fundamentals) {
                    if (typeof config.fundamentals == "string") {
                        config.fundamentals = [config.fundamentals];
                    }
                }
                if (config.chartCursor && (config.chartCursor === CursorType.Crosshair || config.chartCursor === CursorType.Trackball || config.chartCursor === CursorType.Off)) {
                    config.cursorType = config.chartCursor;
                } else {
                    config.cursorType = CursorType.Crosshair;
                }
                if (config.hideCrossHair) config.cursorType = CursorType.Off;
                if (extendConfig.charts.mainChart.dataType === MainDataTypes.Growth10K) {
                    config.growthBaseValue = config.growthBaseValue || 10000;
                } else {
                    delete config.growthBaseValue;
                }

                if (config.growthBaseValue) {
                    if (typeof config.growthBaseValue != "number") {
                        console.info("GrowthBaseValue should be a number")
                        if (!isNaN(Number(config.growthBaseValue))) {
                            config.rootGrowthBaseValue = config.growthBaseValue = Number(config.growthBaseValue);
                        }
                    } else {
                        config.rootGrowthBaseValue = config.growthBaseValue;
                    }
                }

                var selectionMode = config.SMode;
                if (selectionMode) {
                    config.charts.mainChart.selectionMode = selectionMode;
                }

                if (config.dateRange && config.dateRange.length === 2) {
                    if (typeof config.dateRange[0] === 'string') {
                        config.dateRange[0] = new Date(config.dateRange[0]);
                    }
                    if (typeof config.dateRange[1] === 'string') {
                        config.dateRange[1] = new Date(config.dateRange[1]);
                    }
                }
                for (var i = 0; i < config.events.length; i++) {
                    if (config.events[i] == 'DD001') {
                        config.events[i] = 'dividend';
                    }
                    if (config.events[i] == 'HS030') {
                        config.events[i] = 'earnings';
                    }
                    if (config.events[i] == 'HSB05:HSB04') {
                        config.events[i] = 'split'
                    }
                }
                if (config.showTSDividend) {
                    config.charts.mainChart.showDividend = true;
                    config.events.push("dividend");
                }
                if (config.useEUTimeSeriesData) {
                    config.charts.volumeChart.needInit = false;
                }
                if (config.autoComplete.category) {
                    config.category = config.autoComplete.category;
                }
                for (var f in frequencyConfig) {
                    frequencyConfig[f].def = config.intervalFrequencyMap[f];
                }
                if (config.rangeFrequency) {
                    for (var i in config.rangeFrequency) {
                        var currentInterval = IntervalTypesMapping[i];
                        var currentIntervalValues = config.rangeFrequency[i];
                        var fcObject = [];
                        if (currentIntervalValues && currentIntervalValues.length > 0) {
                            for (var j = 0; j < currentIntervalValues.length; j++) {
                                fcObject.push({
                                    "value": currentIntervalValues[j],
                                    "text": FrequencyTexts[currentIntervalValues[j]]
                                });
                            }
                        }
                        frequencyConfig[currentInterval].fc = fcObject;
                    }
                }
                //morningstar.asterix.config.init(appConfig, toolConfig.defaultConfig, baseLabels);
                moment.locale(config.lang);
                bindEvents();
                initChartGenreConfig();
            };

            function getD3Locale() {
                return locale.getD3Locale(config.lang);
            };

            function processTickerCustom(tickers, tickerInfos) {
                if (!tickers || !tickerInfos || tickers.length !== tickerInfos.length) {
                    return;
                }
                if (tickerInfos) {
                    for (var m = 0, infoLen = tickerInfos.length; m < infoLen; m++) {
                        tickers[m][customTickerColorField] = tickerInfos[m].color;
                        tickers[m][customTickerNameField] = tickerInfos[m].name;
                        tickers[m][customTickerCurrencyField] = tickerInfos[m].currency || config.country;
                        if (config.solutionKey) {
                            tickers[m].solutionKey = config.solutionKey;
                        }
                    }
                }
            };

            function createTickerObjects(tickers) {
                var deferred = $.Deferred();
                dataAdapter.createTickerObjects(tickers, function(tickerObjects) {
                    deferred.resolve(tickerObjects);
                });
                return deferred;
            };

            /**
             * tickerInfo is a object or a string , like below:
             *  {
             *   ticker: '126.1.ibm',	// or a tickerObject
             *   color: #F00,	// or red
             *   name: 'AAAA'
             *  }
             *
             *  or
             *
             *  '126.1.ibm'
             */
            var changeMainTicker = throttle(function changeMainTicker(tickerInfo) {
                if (!tickerInfo || tickerInfo == "" || tickerInfo == []) {
                    return;
                }
                var ticker = _buildTickers(tickerInfo);
                if (typeof tickerInfo === 'string') {
                    tickerInfo = {
                        ticker: tickerInfo,
                        color: null,
                        name: null
                    };
                }
                if (typeof ticker === 'string') {
                    createTickerObjects([ticker]).done(function(tickerObj) {
                        tickerInfo.ticker = tickerObj[0];
                        _changeMainTicker(tickerInfo);
                    });
                } else {
                    _changeMainTicker(tickerInfo);
                }
            }, 500);

            function _changeMainTicker(tickerInfo) {
                var tickerObj = tickerInfo.ticker;
                var oriChartGenre = config.chartGenre;
                if (tickerObj && tickerObj.valid) {
                    processTickerCustom([tickerObj], [tickerInfo]);
                    if (isSameTicker(mainTickerObject, tickerObj)) {
                        return;
                    }
                    var oriMainTickerObject = mainTickerObject;
                    mainTickerObject = tickerObj;
                    setChartGenre();
                    if (oriChartGenre !== config.chartGenre) {
                        dataAdapter.unregisterChartTickers(tickerCollection);
                        dataAdapter.registerChartTickers(tickerObj);
                        tickerCollection = [tickerObj];
                    } else {
                        var isCompare = false;
                        //Note: start at 1
                        for (var i = 1; i < tickerCollection.length; i++) {
                            if (isSameTicker(tickerCollection[i], tickerObj)) {
                                tickerCollection.splice(i, 1);
                                isCompare = true;
                                break; //Because no duplicate ticker object.
                            }
                            //clear benchmark and category
                            if (tickerCollection[i].benchmarkType) {
                                tickerCollection.splice(i, 1);
                                i--;
                            }
                        }

                        if (!isCompare) {
                            dataAdapter.registerChartTickers(tickerObj);
                        }

                        tickerCollection[0] = tickerObj;
                    }
                    var cfg = {
                        startDate: config.startDate
                    };
                    if (oriChartGenre === config.chartGenre) {
                        cfg.intervalType = config.intervalType;
                        cfg.lastFrequency = config.frequency;
                    }
                    updateChart(false, cfg); //Don't need clear tickers. if chart type doesn't change, keep the original frequency and interval
                }
            };


            function updateChart(clearTicker, cfg) {
                var _config = getInitConfig();
                var newConfig = $.extend({}, _config, cfg);
                newConfig.width = config.width;
                newConfig.height = config.height;
                newConfig.createConfigInternal = true;

                destroyChart(clearTicker);
                createConfig(newConfig, initCallback);
                eventManager.trigger(eventTypes.triggerClickCallback, {
                    name: 'reset',
                    value: newConfig
                });
            }

            function _updateCompareTickers(updateInfo) {
                if (typeof updateInfo == 'undefined') {
                    return;
                }
                switch (updateInfo.action) {
                    case 'add':
                        {
                            addCompareTickers(updateInfo.tickerInfo);
                            break;
                        }
                    case 'remove':
                        {
                            removeCompareTickers(updateInfo.tickerInfo);
                            break;
                        }
                    case 'set':
                    default:
                        {
                            var updateTickerInfo = [],
                                updateTickers = [],
                                addTickers = [],
                                removeTickers = [];
                            if ($.isArray(updateInfo.tickerInfo)) {
                                updateTickerInfo = updateInfo.tickerInfo;
                            } else if ($.isArray(updateInfo)) {
                                updateTickerInfo = updateInfo;
                            }
                            for (var i = 0; i < updateTickerInfo.length; i++) {
                                updateTickers.push(_buildTickers(updateTickerInfo[i]));
                            }
                            createTickerObjects(updateTickers).done(function(updateTickerObjects) {
                                for (var i = 0; i < updateTickerObjects.length; i++) {
                                    var updateTickerObj = updateTickerObjects[i];
                                    var needAdd = true;
                                    if (updateTickerObj.valid) {
                                        for (var t = 0; t < tickerCollection.length; t++) {
                                            if (isSameTicker(updateTickerObj, tickerCollection[t])) {
                                                needAdd = false;
                                            }
                                        }
                                    } else {
                                        needAdd = false;
                                    }
                                    if (needAdd) {
                                        if (config.country) {
                                            updateTickerObj.currency = config.country;
                                        }
                                        addTickers.push({
                                            ticker: updateTickerObj,
                                            color: updateTickerInfo[i].color,
                                            name: updateTickerInfo[i].name,
                                            currency: updateTickerInfo[i].currency
                                        });
                                    }
                                }
                                for (var i = 0; i < tickerCollection.length; i++) {
                                    if (isSameTicker(mainTickerObject, tickerCollection[i])) {
                                        continue;
                                    }
                                    var needRemove = true;
                                    for (var j = 0; j < updateTickerObjects.length; j++) {
                                        if (isSameTicker(tickerCollection[i], updateTickerObjects[j])) {
                                            needRemove = false;
                                        }
                                    }
                                    if (needRemove) {
                                        removeTickers.push(tickerCollection[i]);
                                    }
                                }
                                if (removeTickers.length > 0) {
                                    removeCompareTickers(removeTickers);
                                }
                                if (addTickers.length > 0) {
                                    addCompareTickers(addTickers);
                                }
                            });
                        }
                }
            }
            var updateCompareTickers = throttle(_updateCompareTickers, 500);
            /**
             *  tickerInfo is a object array or a object or a string , like below:
             *  [{
             *   ticker: '126.1.ibm',	// or a tickerObject, but not support string and object at same time.
             *   color: #F00,	// or red
             *   name: 'BBBB'
             *  }]
             *
             *  or
             *
             *  ['126.1.ibm']
             */
            function addCompareTickers(tickerInfos) {
                if (!$.isArray(tickerInfos)) { // Only support array.
                    return;
                }

                // Note: Pay attention to remove duplication in mainChart.js
                if (tickerCollection.length > config.compareChartsMaxAllowed) {
                    eventManager.trigger(eventTypes.TickerLimitReached);
                    return false;
                }

                var newCompareTickers = [];
                for (var item, i = 0, len = tickerInfos.length; i < len; i++) {
                    item = tickerInfos[i];
                    newCompareTickers.push(_buildTickers(item));
                }

                // Only support same type: ticker objects or symbols.
                if (typeof newCompareTickers[0] === 'string') {
                    createTickerObjects(newCompareTickers).done(function(tickerObjects) {
                        _addCompareTickers(tickerObjects, tickerInfos);
                    });
                } else {
                    _addCompareTickers(newCompareTickers, tickerInfos);
                }
            }

            function _addCompareTickers(newObjects, tickerInfos) {
                var hasSame, hasChanged = false,
                    needRegisterTickers = [],
                    comparePosition;
                processTickerCustom(newObjects, tickerInfos);
                for (var w = 0, newTicker, newLen = newObjects.length; w < newLen; w++) {
                    newTicker = newObjects[w];
                    hasSame = false;
                    if (newTicker.valid) {
                        // need calculate collectionLen after change tickerCollection.
                        for (var i = 0, collectionLen = tickerCollection.length; i < collectionLen; i++) {
                            if (isSameTicker(newTicker, tickerCollection[i])) {
                                //config.compareColors[i] = newColors[w] || config.compareColors[i];
                                hasChanged = true;
                                hasSame = true;
                                break;
                            }
                        }

                        if (!hasSame) { // no same ticker
                            if (collectionLen <= config.compareChartsMaxAllowed) {
                                comparePosition = tickerCollection.push(newTicker);
                                config.compareTickers.push(newTicker.symbol);
                                needRegisterTickers.push(newTicker);
                                hasChanged = true;
                            } else {
                                eventManager.trigger(eventTypes.TickerLimitReached);
                                break; //we should drop rest tickers.
                            }
                        }
                    }
                }

                if (needRegisterTickers.length > 0) {
                    dataAdapter.registerChartTickers(needRegisterTickers);
                }

                if (hasChanged) {
                    eventManager.trigger(eventTypes.UpdateCompareTickers);
                    executeAddOrRemoveCompareTicker(needRegisterTickers);
                }
            }

            /**
             *  tickers is a string or a array of objects or strings , like below:
             *  '126.1.ibm' or ['126.1.ibm'] or [tickerObject]
             */
            function removeCompareTickers(tickers, isFromRemoveBtn) {
                if (!$.isArray(tickers)) {
                    tickers = [tickers];
                }

                if (typeof tickers[0] === 'string') {
                    createTickerObjects(tickers).done(function(tickerObjects) {
                        _removeCompareTickers(tickerObjects, isFromRemoveBtn);
                    });
                } else {
                    _removeCompareTickers(tickers, isFromRemoveBtn);
                }
            }

            function _removeCompareTickers(removeTickers, isFromRemoveBtn) {
                var needUnregisterTickers = [],
                    hasChanged = false;
                for (var w = 0, removeTicker, removeLen = removeTickers.length; w < removeLen; w++) {
                    removeTicker = removeTickers[w];
                    //Note: start from 1 to cross main ticker.
                    for (var i = 1, len = tickerCollection.length; i < len; i++) {
                        var tickerObj = tickerCollection[i];
                        if (tickerObj && isSameTicker(tickerObj, removeTicker)) {
                            needUnregisterTickers.push(tickerObj);
                            tickerCollection.splice(i, 1);
                            config.compareTickers.splice(i - 1, 1);
                            hasChanged = true;
                            break;
                        }
                    }
                }

                if (hasChanged) {
                    dataAdapter.unregisterChartTickers(needUnregisterTickers);
                    eventManager.trigger(eventManager.EventTypes.UpdateCompareTickers);
                    executeAddOrRemoveCompareTicker(needUnregisterTickers, true, isFromRemoveBtn);
                }
            }

            function executeAddOrRemoveCompareTicker(tickers, toRemove, isFromRemoveBtn) {
                if (toRemove) {
                    !isFromRemoveBtn && eventManager.trigger(eventManager.EventTypes.onRemoveCompareTickers, tickers);
                    isFromRemoveBtn && eventManager.trigger(eventManager.EventTypes.onTickerRemoveBtnClicked, tickers);
                } else {
                    eventManager.trigger(eventManager.EventTypes.onAddCompareTickers, tickers);
                }
                setConfig(config.compareTickers);
            }

            function isSameTicker(tickerA, tickerB) {
                return tickerA.listTicker === tickerB.listTicker && tickerA.secId === tickerB.secId;
            }

            function menuBarConfigProcess() {
                var hideInterval;
                for (var bp in config.intervalBreakPointMap) {
                    var intervalArr = config.intervalBreakPointMap[bp];
                    for (var i = 0, len = intervalArr.length; i < len; i++) {
                        if (config[intervalArr[i]] === false) {
                            intervalArr.splice(i, 1);
                            len--;
                            i--;
                        }
                    }
                }
            }

            function initChartGenreConfig() {
                if (tickerCollection && tickerCollection.length > 0) {
                    _initChartGenreConfig(tickerCollection);
                } else {
                    var tickerInfos = [config.mainTicker];
                    if (config.compareTickers && config.compareTickers.length > 0) {
                        tickerInfos = tickerInfos.concat(config.compareTickers);
                    }
                    for (var i = 0, item, tickers = [], tickerLen = tickerInfos.length; i < tickerLen; i++) {
                        item = tickerInfos[i];
                        var ticker = _buildTickers(item);
                        if (ticker) {
                            tickers.push(ticker);
                        } else {
                            tickerInfos.splice(i, 1);
                            tickerLen--;
                        }
                    }
                    if (typeof tickers[0] === 'string') {
                        createTickerObjects(tickers).done(function(tickerObjects) {
                            if (tickerInfos.length > 1) {
                                _uniqueTickersAfterIdService(tickerObjects, tickerInfos, tickers);
                            }
                            dataAdapter.registerChartTickers(tickerObjects);
                            _initChartGenreConfig(tickerObjects, tickerInfos);
                        });
                    } else {
                        dataAdapter.registerChartTickers(tickers);
                        _initChartGenreConfig(tickers, tickerInfos);
                    }
                }
            }

            /** 
             * @function unique tickerObjects,TickerInfos
             * @params tickerObjects From initChartGenreConfig()
             * @params tickerInfos From initChartGenreConfig()
             * @params tickers From initChartGenreConfig()
             * @params compareTickers on config
             */
            function _uniqueTickersAfterIdService(tickerObjects, tickerInfos, tickers) {
                for (var j = 0; j < tickerObjects.length; j++) {
                    for (var k = j + 1; k < tickerObjects.length; k++) {
                        var currentTickerObject = tickerObjects[j];
                        if (isSameTicker(currentTickerObject, tickerObjects[k])) {
                            tickerObjects.splice(k, 1);
                            tickerInfos.splice(k, 1);
                            tickers.splice(k, 1);
                            config.compareTickers.splice(k - 1, 1);
                            // k--, because array length will reduce
                            k--;
                        }
                    }
                }
            }

            function _buildTickers(tickerInfo) {
                var ticker;
                var chartDataType = config.charts.mainChart.dataType;
                if (tickerInfo.portfolioId || chartDataType === MainDataTypes.TotalReturn || chartDataType === MainDataTypes.MarketValue) {
                    ticker = tickerInfo.id + ";PO|" + tickerInfo.name;
                } else if (tickerInfo.type === 'fund') {
                    ticker = tickerInfo.securityId + ";PO|" + tickerInfo.name;
                } else if (tickerInfo.ticker) {
                    ticker = tickerInfo.ticker;
                } else if (tickerInfo.id && tickerInfo.id.indexOf(']') > -1) {
                    config.useEUTimeSeriesData = true;
                    tickerInfo.securityToken = tickerInfo.securityToken || tickerInfo.id;
                    tickerInfo.id = tickerInfo.id.split(']')[0];
                } else if (tickerInfo.id) {
                    ticker = tickerInfo.id;
                } else {
                    ticker = tickerInfo;
                }
                if (config.useEUTimeSeriesData && tickerInfo.securityToken) {
                    ticker = tickerInfo.securityToken;
                    ticker += '|' + (tickerInfo.idType || 'Morningstar');
                    ticker += '|' + (tickerInfo.name || '');
                    if (tickerInfo.id) {
                        ticker += '@_' + tickerInfo.id;
                    }
                } else if (config.useEUTimeSeriesData && typeof tickerInfo === "string" && tickerInfo.indexOf(']') > 0) {
                    ticker += tickerInfo + '@_' + tickerInfo.split(']')[0];
                }
                return ticker;
            }

            function _initChartGenreConfig(tickerObjects, tickerInfos) {
                tickerCollection = tickerObjects;
                mainTickerObject = config.mainTickerObject = tickerObjects[0];
                processTickerCustom(tickerCollection, tickerInfos);
                config.securityTypeClass = "mkts-cmpt-svgcht-mtype-" + mainTickerObject.mType || '';
                setChartGenre();
                if (config.chartGenre === ChartGenre.ForexChart) {
                    initForexChartConfig();
                } else if (config.chartGenre === ChartGenre.FundChart) {
                    initFundChartConfig();
                } else if (config.chartGenre === ChartGenre.CommodityChart) {
                    initCommodityChartConfig();
                } else {
                    initStockChartConfig();
                }
                initRelatedBenchmarks().done(initCallback);
                setAutocompleteCategories();
            }

            function setAutocompleteCategories() {
                var conCategorys = [];
                var defaultCategoryType = ChartTypeCategoryMap[config.chartGenre];
                if ($.isArray(config.category)) {
                    config.autocompleteCategories = config.category;
                } else {
                    if (asterixUtil.isObj(config.category)) {
                        var _category = _getMainTickerTypeCategory();
                        if (_category) {
                            config.autocompleteCategories = _category; // get config category
                        } else if (config.category[defaultCategoryType]) {
                            config.autocompleteCategories = config.category[defaultCategoryType]; //get chartType category
                        } else {
                            config.autocompleteCategories = AutocompleteCategories[defaultCategoryType]; //get default category
                        }
                    } else {
                        config.autocompleteCategories = AutocompleteCategories[defaultCategoryType]; //get default category
                    }
                }
            }

            // get config Category by mainTicker of type
            function _getMainTickerTypeCategory() {
                for (var i = 0; i < checkList.length; i++) {
                    if (mainTickerObject[checkList[i]]) {
                        var type = mainTickerObject[checkList[i]];
                        if (config.category[type]) {
                            return config.category[type];
                        }
                    }
                }
                return null;
            }

            function initStockChartConfig() {
                config.menubar.hideDisplay = util.arrConcat(config.menubar.hideDisplay, ['post-tax-return-chart']);
                setDefaultInterval(IntervalTypes.OneDay);
            }

            function initFundChartConfig() {
                if (mainTickerObject.exch == "MSIT") {
                    config.charts.mainChart.dataType = config.charts.mainChart.dataType || MainDataTypes.Growth10K;
                }
                if (isITFund()) {
                    config.legendInfoLabel = 'Name';
                }
                var fundCategories = AutocompleteCategories.fund;
                if ($.isArray(config.funds) && config.funds.length > 0) {
                    for (var i = 0, f; i < config.funds.length; i++) {
                        f = config.funds[i];
                        if (f === 'FC') {
                            f = 'Closed-End Fund';
                        } else if (f === 'FE') {
                            f = 'ETF';
                        } else {
                            f = '';
                        }
                        if (!f || $.inArray(f, fundCategories) >= 0) {
                            continue;
                        }
                        fundCategories.push(f);
                    }
                }
                config.charts.volumeChart.needInit = false;

                config.menubar.hideButtons = util.arrConcat(config.menubar.hideButtons, ['mkts-cmpt-svgcht-menubtn--fundamental']);
                config.menubar.hideEventsIndicators = util.arrConcat(config.menubar.hideEventsIndicators, ['splits', 'earnings', 'change-chart-volume', 's-m-a-v', 'p-s-a-r', 'will-r', 's-stochastic', 'f-stochastic', 'p-channel', 'u-l-t', 'd-m-i', 'm-f-i', 'o-b-v', 'v-acc', 'u-d-ratio', 'v-b-p', 'f-volatility', 'acc-dis', 'mass', 'volatility', 'force-index', 'a-t-r', 'keltner', 'prev-close']);
                config.menubar.hideDisplay = util.arrConcat(config.menubar.hideDisplay, ['ohlc-chart', 'candlestick-chart', 'dividend-effect-chart', 'change-chart-volume-plus', 'change-extended-market-hour', 'title-other']);
                config.hideFundamental = true;
                setDefaultInterval(IntervalTypes.OneMonth);
                //config.intervalType = config.intervalType || IntervalTypes.OneMonth;
            }

            function initForexChartConfig() {
                setDefaultInterval(IntervalTypes.OneDay);
                //config.intervalType = config.intervalType || IntervalTypes.OneDay;
                config.charts.mainChart.yAxis.formatPrefix = "f";
                config.charts.volumeChart.show = false;
                config.menubar.hideButtons = util.arrConcat(config.menubar.hideButtons, ['mkts-cmpt-svgcht-menubtn--fundamental']);
                config.menubar.hideEventsIndicators = util.arrConcat(config.menubar.hideEventsIndicators, ['mkts-cmpt-svgcht-popover1-subtitle1', 'change-events', 'change-chart-volume', 's-m-a-v', 'p-s-a-r', 'will-r', 's-stochastic', 'f-stochastic', 'p-channel', 'u-l-t', 'd-m-i', 'm-f-i', 'o-b-v', 'v-acc', 'u-d-ratio', 'v-b-p', 'f-volatility', 'acc-dis', 'mass', 'volatility', 'force-index', 'a-t-r', 'keltner', 'prev-close']);
                config.menubar.hideDisplay = util.arrConcat(config.menubar.hideDisplay, ['dividend-effect-chart', 'change-extended-market-hour', 'change-chart-volume-plus', 'post-tax-return-chart']);
            }

            function initCommodityChartConfig() {
                config.menubar.hideButtons = util.arrConcat(config.menubar.hideButtons, ['mkts-cmpt-svgcht-menubtn--fundamental']);
                config.menubar.hideEventsIndicators = util.arrConcat(config.menubar.hideEventsIndicators, ['mkts-cmpt-svgcht-popover1-subtitle1', 'change-events']);
                config.menubar.hideDisplay = util.arrConcat(config.menubar.hideDisplay, ['p-growth-chart', 'ten-k-growth-chart', 'dividend-effect-chart', 'post-tax-return-chart']);
                setDefaultInterval(IntervalTypes.OneDay);
            }

            function setChartGenre() {
                if (mainTickerObject.type === 20) {
                    config.chartGenre = ChartGenre.ForexChart;
                } else if ((isMutualFund(mainTickerObject) || isPortfolio(mainTickerObject) || config.useDataApiTimeseriesData) && !config.drawNavAndPriceChart) {
                    config.chartGenre = ChartGenre.FundChart;
                } else if (isCommodity(mainTickerObject)) {
                    config.chartGenre = ChartGenre.CommodityChart;
                } else {
                    config.chartGenre = ChartGenre.StockChart;
                }
            }

            function isPortfolio(tickerObj) {
                var mType = tickerObj.mType;
                return mType === 'PO';
            }

            function isCommodity(tickerObj) {
                var mType = tickerObj.mType;
                return mType === "FU";
            }

            function isMutualFund(tickerObj) {
                if (!tickerObj) {
                    tickerObj = mainTickerObject;
                }
                var type = tickerObj.type;
                var mType = tickerObj.mType;
                var isFundType = false;
                if (typeof config.funds === 'function') {
                    isFundType = config.funds(tickerObj);
                } else if ($.isArray(config.funds)) {
                    for (var i = 0; i < config.funds.length; i++) {
                        if (mType == config.funds[i]) {
                            isFundType = true;
                            break;
                        }
                    }
                }
                if (isFundType) {
                    tickerObj.isCustomFund = true;
                }
                if (tickerObj.tsfund && tickerObj.tenforeCode && tickerObj.tenforeCode.indexOf('9999') > -1 || (tickerObj.tsfund && !tickerObj.tenforeCode)) {
                    isFundType = true;
                }
                return isFundType || mType === "FO" || (type == 8 && (!mType || mType == "SA" || mType === "FV"));
            }

            function loadTheme(theme) {
                var leftSliderBrushImageUrl, rightSliderBrushImageUrl;
                config.hideTab = true;
                switch (theme) {
                    case '2':
                        config.minWidth = 260;
                        config.charts.volumeChart.height = 65;
                        config.charts.volumeChart.yAxis.show = false;
                        config.applyAspectRatio = false;
                        config.charts.sliderChart.height = 50;
                        config.yAxisOrient = "right";
                        config.yAxisPosition = "inner";
                        config.showYAxisTickLine = false;
                        leftSliderBrushImageUrl = rightSliderBrushImageUrl = config.StaticFilesPath + 'quest/images/brush-q.png';
                        config.mainTickerColor = config.mainTickerColor || '#339bb6';
                        config.compareColors = config.compareColors || ['#FF5621', '#B967C7', '#19227D', '#2095F2', '#548A2E', '#AEB32A', '#4D332D'];
                        config.menubar.hideDisplay = ['change-extended-market-hour'];
                        config.intervalBreakPointMap['600'] = ['1D', '3M', '1Y', '5Y', 'MAX'];
                        break;
                    case '3':
                        leftSliderBrushImageUrl = rightSliderBrushImageUrl = config.StaticFilesPath + 'msn/images/brush-msn.png';
                        config.mainTickerColor = config.mainTickerColor || '#0090b9';
                        config.closeImage = config.StaticFilesPath + 'msn/images/msn_close.svg';
                        config.compareColors = config.compareColors || ['#e45765', '#068743', '#f89a30', '#234e67', '#ffc216', '#625387', '#231f20'];
                        config.menubar.hideDisplay = ['change-extended-market-hour'];
                        config.yAxisOrient = "right";
                        config.yAxisPosition = "inner";
                        config.showYAxisTickLine = false;
                        break;
                    case '4':
                        leftSliderBrushImageUrl = rightSliderBrushImageUrl = config.StaticFilesPath + 'arca/images/brush-q.png';
                        //config.maxWidth = 960;
                        config.hideMenu = true;
                        config.hideTab = false;
                        config.menubar.hideInterval = ['mkts-cmpt-svgcht-frequency-picker'];
                        config.applyAspectRatio = false;
                        config.charts.sliderChart.height = 45;
                        config.mainTickerColor = config.mainTickerColor || '#a11140';
                        config.compareColors = config.compareColors || ['#333333', '#B967C7', '#19227D', '#2095F2', '#548A2E', '#AEB32A', '#4D332D'];
                        config.charts.mainChart.yAxis.formatPrefix = "CUSTOM";
                        config.charts.mainChart.dataType = MainDataTypes.Growth10K;
                        config.dailyDateFormatter = 'DD/MM/YYYY';
                        config.yAxisOrient = "left";
                        config.yAxisPosition = "inner";
                        config.showYAxisTickLine = false;
                        config.growthType = "performancesGrowth";
                        config.useEUTimeSeriesData = true;
                        config.intervalBreakPointMap = {
                            'default': ['1M', '3M', 'YTD', '1Y', '3Y', '5Y', '10Y'],
                            '600': ['1M', '1Y', '5Y']
                        };
                        break;
                    case '5':
                        config.lang = 'en-GB';
                        config.autoComplete.highlight = true;
                        leftSliderBrushImageUrl = rightSliderBrushImageUrl = config.StaticFilesPath + 'fidelity/images/fidelity.png';
                        config.margin = {
                            left: 0,
                            top: 0,
                            right: 0,
                            bottom: 0
                        };
                        config.crosshairJustify = true;
                        config.theme = 5;
                        config.enterTipsLabelID = 'search-assets';
                        config.showDiscountCumFair = true;
                        config.showNAV = true;
                        config.yAxisOrient = "right";
                        config.yAxisPosition = "inner";
                        config.showYAxisTickLine = false;
                        config.charts = $.extend(true, config.charts, {
                            volumeChart: {
                                height: 98,
                                yAxis: {
                                    show: true
                                }
                            },
                            sliderChart: {
                                height: 85
                            }
                        });
                        config.needLegendBackground = true;
                        config.mainTickerColor = [];
                        config.compareColors = [];
                        config.yAxisMargin = 16;
                        var tagProperties = {
                            tagHeight: 32,
                            sideMargin: 12,
                            textTopMargin: 13
                        };
                        if (config.tagMode === 's') {
                            tagProperties.tagHeight = 25;
                            tagProperties.tagWidth = 56;
                            tagProperties.textTopMargin = 12;
                            tagProperties.tagMode = config.tagMode;
                        }
                        config.charts.mainChart.tagProperties = $.extend(true, config.charts.mainChart.tagProperties, tagProperties);
                        config.intervalBreakPointMap = {
                            'default': ['1D', '5D', '1M', '3M', '6M', 'YTD', '1Y', '5Y', '10Y'],
                            '600': ['1D', '1M', '1Y', '10Y']
                        };
                        resetColorConfig();
                        break;
                    default:
                        var url = config.StaticFilesPath;
                        leftSliderBrushImageUrl = url + "morningstar/images/brush-m-1.png";
                        rightSliderBrushImageUrl = url + "morningstar/images/brush-m-2.png";
                        break;
                }

                if (!config.brushImages) {
                    config.brushImages = [leftSliderBrushImageUrl, rightSliderBrushImageUrl];
                }
                config.compareColors = config.compareColors || ['#FD5516', '#008E91', '#709900', '#5050CC', '#FFB600', '#AA008A', '#037F56'];
                config.mainTickerColor = config.mainTickerColor || '#0039B7';
                config.menubar.hideDrawings = util.arrConcat(config.menubar.hideDrawings, ['fibonacci-fans', 'fibonacci-time-zones', 'fibonacci-arcs']);
            }

            function resetColorConfig() {
                var fundamentalCfg = config.charts.mainChart.fundamentalProperties;
                var indicatorCfg = config.charts.mainChart.indicatorProperties;
                for (var i in fundamentalCfg) {
                    fundamentalCfg[i].color = [];
                }
                for (var i in indicatorCfg) {
                    indicatorCfg[i].color = [];
                }
            }

            function reduceMonth(date, months) {
                var preM = date.getMonth();
                var m = date.getMonth() - (months || 1);
                date.setMonth(m);
                if (preM !== 0 && date.getMonth() !== (m % 11)) { // special for one month
                    date.setDate(0);
                }
                return date;
            }

            function getDateRangeByInterval(intervalType) {
                var today = new Date(),

                    startDate = new Date();
                var y = startDate.getFullYear(),
                    m = startDate.getMonth(),
                    d = startDate.getDate();
                switch (intervalType) {
                    case IntervalTypes.FiveDay:
                        startDate = new Date(y, m, d - 5);
                        break;
                    case IntervalTypes.FifteenDay:
                        startDate = new Date(y, m, d - 15);
                        break;
                    case IntervalTypes.OneMonth:
                        startDate = reduceMonth(startDate, 1);
                        break;
                    case IntervalTypes.ThreeMonth:
                        startDate = reduceMonth(startDate, 3);
                        break;
                    case IntervalTypes.SixMonth:
                        startDate = reduceMonth(startDate, 6);
                        break;
                    case IntervalTypes.YTD:
                        startDate = new Date(y, 0, 1);
                        break;
                    case IntervalTypes.OneYear:
                        startDate = new Date(y - 1, m, d);
                        break;
                    case IntervalTypes.ThreeYear:
                        startDate = new Date(y - 3, m, d);
                        break;
                    case IntervalTypes.FiveYear:
                        startDate = new Date(y - 5, m, d);
                        break;
                    case IntervalTypes.TenYear:
                        startDate = new Date(y - 10, m, d);
                        break;
                    case IntervalTypes.FifteenYear:
                        startDate = new Date(y - 15, m, d);
                        break;
                    case IntervalTypes.Max:
                        startDate = config.compareNewstartDate || config.maxRangeStartDate;
                        break;
                }
                return [startDate, today];
            }

            function getDefaultfrequencyByIntervalType(intervalType) {
                if (intervalType) {
                    return config.intervalFrequencyMap[intervalType];
                }
            }

            function getPriceType(tickerObjects) {
                var priceType = getMainDataType();
                if (priceType === MainDataTypes.PriceWithDividendGrowth) {
                    priceType = dataAdapter.PriceType.PriceWithDividendGrowth;
                } else if (priceType === MainDataTypes.Growth10K) {
                    priceType = dataAdapter.PriceType.Growth10K;
                } else if ((((tickerObjects.length > 1) && !config.drawNavAndPriceChart)) && !config.growthBaseValue) {
                    priceType = dataAdapter.PriceType.Growth;
                } else if ((tickerObjects.length > 1) && config.drawNavAndPriceChart) {
                    priceType = dataAdapter.PriceType.PriceAndNav;
                } else {
                    if (config.chartGenre === ChartGenre.FundChart) {
                        priceType = priceType === MainDataTypes.Price ? MainDataTypes.Nav : priceType;
                    }
                }
                switch (priceType) {
                    case MainDataTypes.Growth:
                    case MainDataTypes.DividendEffect:
                        priceType = dataAdapter.PriceType.Growth;
                        break;
                    case MainDataTypes.Growth10K:
                        priceType = dataAdapter.PriceType.Growth10K;
                        break;
                    case MainDataTypes.PostTax:
                        priceType = dataAdapter.PriceType.PostTax;
                        break;
                    case MainDataTypes.TotalReturn:
                        priceType = dataAdapter.PriceType.TotalReturn;
                        break;
                    case MainDataTypes.MarketValue:
                        priceType = dataAdapter.PriceType.MarketValue;
                        break;
                    case MainDataTypes.PriceAndNav:
                        priceType = dataAdapter.PriceType.PriceAndNav;
                        break;
                    case MainDataTypes.Price:
                    case MainDataTypes.Nav:
                        if (config.growthBaseValue) {
                            priceType = dataAdapter.PriceType.Growth10K;
                        }
                        break;
                }
                return priceType;
            }

            function createRequestObj(tickerObjects, adjustedDateRange) {
                var req = new dataAdapter.Request();
                var intervalType = config.intervalType;
                var frequency;
                var dateRange = adjustedDateRange || config.dateRange;
                req.indicators(getIndicators()).fundamentals(getFundamentals()).events(config.events);
                if (intervalType && !adjustedDateRange) {
                    frequency = config.frequency || (config.frequency = getDefaultfrequencyByIntervalType(intervalType));
                    dateRange = setRequestByInterval(intervalType, req, frequency) || dateRange;
                    dateRange = _changeDateRangeByFrequency(frequency, dateRange, false); // change dateRange to get full data
                    dateRange && req.dateRange(dateRange);
                } else {
                    frequency = config.frequency || (config.frequency = getfrequencyByDateRange(dateRange));
                    dateRange = _changeDateRangeByFrequency(frequency, dateRange, true); // change dateRange to get full data
                    req.dateRange(dateRange);
                }
                req.isPreAfter(config.showPreAfter);
                req.priceType(getPriceType(tickerObjects)).tickerObjects(tickerObjects).frequency(frequency).keepDateRange(true);
                req.growthBaseValue(config.growthBaseValue);
                if (config.startDate === toolConfig.StartDateTypes.Earliest) {
                    req.alignMainTicker(false);
                }
                var preloadInfo = {}; // this reset all other parameters, like loadedData.
                preloadInfo.intervalType = intervalType;
                preloadInfo.frequency = frequency;
                preloadInfo.dateRange = dateRange;
                req.preloadInfo = preloadInfo;
                return req;
            }

            function createDisplayRequestObj(tickerObjects, dateRange, frequency) {
                var req = new dataAdapter.Request();
                var intervalType = config.intervalType;
                req.indicators(getIndicators()).events(config.events);
                if (!config.fundamentalNotAvailable) {
                    req.fundamentals(getFundamentals())
                }
                req.dateRange(dateRange);
                req.isPreAfter(config.showPreAfter);
                config.growthType && req.chartType(config.growthType);
                req.growthBaseValue(config.growthBaseValue);
                req.priceType(getPriceType(tickerObjects)).tickerObjects(tickerObjects).frequency(frequency).keepDateRange(true);
                return req;
            }

            function getFundamentals() {
                var fundamentals = config.fundamentals ? config.fundamentals.slice() : [];
                if (getMainDataType() === MainDataTypes.DividendEffect) {
                    fundamentals.push(dataAdapter.Fundamental.MReturnIndex);
                }
                return fundamentals;
            }

            function getPreloadIntervalType(intervalType) {
                var preloadIntervalType;
                switch (intervalType) {
                    case IntervalTypes.OneDay:
                        preloadIntervalType = IntervalTypes.FiveDay;
                        break;
                    case IntervalTypes.FiveDay:
                        preloadIntervalType = IntervalTypes.FifteenDay;
                        break;
                    case IntervalTypes.FifteenDay:
                        preloadIntervalType = IntervalTypes.OneMonth;
                        break;
                    case IntervalTypes.OneMonth:
                        preloadIntervalType = IntervalTypes.ThreeMonth;
                        break;
                    case IntervalTypes.ThreeMonth:
                        preloadIntervalType = IntervalTypes.SixMonth;
                        break;
                    case IntervalTypes.SixMonth:
                        preloadIntervalType = IntervalTypes.OneYear;
                        break;
                    case IntervalTypes.OneYear:
                        preloadIntervalType = IntervalTypes.ThreeYear;
                        break;
                    case IntervalTypes.ThreeYear:
                        preloadIntervalType = IntervalTypes.FiveYear;
                        break;
                    case IntervalTypes.FiveYear:
                        preloadIntervalType = IntervalTypes.TenYear;
                        break;
                }
                return preloadIntervalType;
            }

            function getPreloadDateRange(dateRange) {
                var newDateRange;
                var oriEndDate = dateRange[1];
                var oriStartDate = dateRange[0];
                var timeSpan = oriEndDate - oriStartDate;
                if (timeSpan / (3600 * 24 * 1000) < 4) { //less than 4 days
                    timeSpan += (3600 * 24 * 1000 * 4);
                } else {
                    timeSpan = 2 * timeSpan;
                }
                var newStartDate = oriStartDate - timeSpan;
                return [new Date(newStartDate), oriStartDate];
            }

            function createPreloadRequestObj(tickerObjects, preloadInfo) {
                var req = new dataAdapter.Request();
                preloadInfo = preloadInfo || {};
                var intervalType = preloadInfo.intervalType || config.intervalType;
                var frequency = preloadInfo.frequency || config.frequency || getDefaultfrequencyByIntervalType(intervalType);
                req.indicators(getIndicators()).fundamentals(getFundamentals());
                var dateRange = preloadInfo.dateRange || config.dateRange;
                if (dateRange && dateRange.length === 2) {
                    frequency = frequency || getfrequencyByDateRange(dateRange);
                    dateRange = getPreloadDateRange(dateRange);
                    dateRange = _changeDateRangeByFrequency(frequency, dateRange);
                    req.dateRange(dateRange);
                } else {
                    intervalType = getPreloadIntervalType(intervalType);
                    frequency = frequency || getDefaultfrequencyByIntervalType(intervalType);
                    dateRange = setRequestByInterval(intervalType, req) || dateRange;
                    dateRange = _changeDateRangeByFrequency(frequency, dateRange);
                }
                req.isPreAfter(config.showPreAfter);
                req.priceType(getPriceType(tickerObjects)).tickerObjects(tickerObjects).frequency(frequency).keepDateRange(true);
                req.growthBaseValue(config.growthBaseValue);
                preloadInfo.intervalType = intervalType;
                preloadInfo.frequency = frequency;
                preloadInfo.dateRange = dateRange;
                req.preloadInfo = preloadInfo;
                return req;
            }

            function getfrequencyByDateRange(dateRange, returnIntervalType) {
                var startDate = dateRange[0];
                var endDate = dateRange[1];
                var daySpan = (endDate - startDate) / Constants.DAY_IN_MILLISECONDS;
                var intervalFrequencyMap = config.intervalFrequencyMap;
                var intervalType, frequency;

                daySpan = Math.abs(daySpan);

                if (daySpan >= 365 * 10) {
                    intervalType = IntervalTypes.Max;;
                } else if (daySpan >= 365 * 5 && daySpan < 365 * 10) {
                    intervalType = IntervalTypes.FiveYear;
                } else if (daySpan >= 365 * 3 && daySpan < 365 * 5) {
                    intervalType = IntervalTypes.ThreeYear;
                } else if (daySpan >= 365 * 1 && daySpan < 365 * 3) {
                    intervalType = IntervalTypes.OneYear;
                } else if (daySpan > 90 && daySpan < 365) {
                    intervalType = IntervalTypes.SixMonth; // rt can not query morn then 90 days of minutes data
                } else if (daySpan >= 30 && daySpan <= 90) {
                    intervalType = IntervalTypes.OneMonth;
                } else if (daySpan >= 15 && daySpan < 30) {
                    intervalType = IntervalTypes.FifteenDay;
                } else if (daySpan >= 5 && daySpan < 15) {
                    intervalType = IntervalTypes.FiveDay;
                } else if (daySpan < 5) {
                    intervalType = IntervalTypes.OneDay;
                }
                if (config.chartGenre === ChartGenre.FundChart && daySpan < 30) {
                    intervalType = IntervalTypes.OneMonth;
                }
                if (returnIntervalType) {
                    frequency = intervalType;
                } else {
                    frequency = intervalFrequencyMap[intervalType];
                }

                return frequency;
            }

            function getSliderChartfrequency() {
                var frequency = config.sliderChartfrequency;
                if (!frequency) {
                    if (config.intervalType) {
                        var sliderIntervalType = config.SliderChartIntervalTypeMap[config.intervalType];
                        frequency = config.intervalFrequencyMap[sliderIntervalType];
                    } else if (config.dateRange) {
                        frequency = getfrequencyByDateRange(config.dateRange);
                    }
                }
                return frequency;
            }

            function getSliderNextTntervalType(intervalType) {
                var pos = $.inArray(intervalType, intervalTypeSortedArray);
                var nextIntervalType, target, found = false;
                var sliderIntervalTypeMap = config.SliderChartIntervalTypeMap;
                for (var i = pos + 1, len = intervalTypeSortedArray.length; i < len; i++) {
                    nextIntervalType = intervalTypeSortedArray[i];
                    for (var key in sliderIntervalTypeMap) {
                        if (sliderIntervalTypeMap[key] === nextIntervalType) {
                            found = true;
                            break;
                        }
                    }
                    if (found) {
                        break;
                    }
                }
                return nextIntervalType;
            }

            function createSliderChartRequestObj(tickerObjects, preloadInfo) {
                var sliderChartIntervalType;
                if (preloadInfo) {
                    sliderChartIntervalType = getSliderNextTntervalType(preloadInfo.intervalType);
                } else {
                    var mainChartIntervalType = config.intervalType;
                    if (mainChartIntervalType) {
                        sliderChartIntervalType = config.SliderChartIntervalTypeMap[mainChartIntervalType];
                    } else {
                        var mainChartDateRange = config.dateRange;
                        var today = new Date();
                        var days = (today - mainChartDateRange[0]) / Constants.DAY_IN_MILLISECONDS;
                        if (days < 30 * 2.5) {
                            sliderChartIntervalType = IntervalTypes.ThreeMonth;
                        } else if (days < 30 * 5) {
                            sliderChartIntervalType = IntervalTypes.SixMonth;
                        } else if (days < 30 * 12 * 9.5) {
                            sliderChartIntervalType = IntervalTypes.TenYear;
                        } else {
                            sliderChartIntervalType = IntervalTypes.Max;
                        }
                    }
                }

                var req = new dataAdapter.Request();
                var frequency = getDefaultfrequencyByIntervalType(sliderChartIntervalType);
                var dateRange = getDateRangeByInterval(sliderChartIntervalType);
                if (config.chartGenre === ChartGenre.FundChart) {
                    var priceType = getMainDataType();
                    if (isITFund() && priceType !== MainDataTypes.PriceWithDividendGrowth) {
                        if (priceType === MainDataTypes.Price) {
                            priceType = dataAdapter.PriceType.Nav;
                        } else if (priceType == MainDataTypes.Growth) {
                            priceType = dataAdapter.PriceType.Growth;
                        } else if (priceType == MainDataTypes.Growth10K) {
                            priceType = dataAdapter.PriceType.Growth10K;
                        }
                    } else if (priceType !== MainDataTypes.PriceWithDividendGrowth) {
                        priceType = dataAdapter.PriceType.Nav;
                    }
                    req.priceType(priceType);
                }
                req.dateRange(dateRange);
                req.growthBaseValue(config.growthBaseValue);
                req.tickerObjects(tickerObjects).frequency(frequency).keepDateRange(true);
                req.isPreAfter(config.showPreAfter);
                preloadInfo = preloadInfo || {};
                preloadInfo.intervalType = sliderChartIntervalType;
                preloadInfo.frequency = frequency;
                preloadInfo.dateRange = dateRange;
                req.preloadInfo = preloadInfo;
                return req;
            }

            function isITFund(tickerObject) {
                var tickerObject = tickerObject || mainTickerObject;
                return tickerObject.tsfund === true;
            }

            function isSemiEarningsExch(tickerObject) {
                var tickerObject = tickerObject || mainTickerObject;
                var checkFields = ['MIC-CODE', 'CompositeExchangeID', 'tenforeCode', 'listMarket'];
                for (var i = 0; i < checkFields.length; i++) {
                    if (config.semiEarningsExch.indexOf(tickerObject[checkFields[i]]) > -1) {
                        return true;
                    }
                }
                return false;
            }

            function fetchMainTickerData(callback, adjustedDateRange) {
                var req = createRequestObj(tickerCollection, adjustedDateRange);
                dataAdapter.fetchTSData(req, function(res) {
                    dataPreprocess(req, res, callback);
                });
            }

            function fetchDisplayData(dateRange, frequency, callback) {
                var req = createDisplayRequestObj(tickerCollection, dateRange, frequency);
                dataAdapter.fetchTSData(req, function(res) {
                    callback(res, req.preloadInfo);
                });
            }

            function fetchSliderChartData(preloadInfo, callback) {
                var req = createSliderChartRequestObj([mainTickerObject], preloadInfo);
                dataAdapter.fetchTSData(req, function(res) {
                    dataPreprocess(req, res, callback);
                });
            }

            function preloadMainTickerData(preloadInfo, callback) {
                var maxStartDate = config.mainTickerObject.IPODate || config.maxRangeStartDate;
                if (!preloadInfo.dateRange || !preloadInfo.dateRange[0] || (preloadInfo.dateRange[0] > maxStartDate)) {
                    changeFundamentalStatus(false);
                    var req = createPreloadRequestObj(tickerCollection, preloadInfo);
                    if (req.dateRange()[0] < maxStartDate) {
                        req.dateRange()[0] = maxStartDate;
                    }

                    dataAdapter.fetchTSData(req, function(res) {
                        dataPreprocess(req, res, callback);
                    })
                } else {
                    callback(null, preloadInfo);
                }
            }

            function dataPreprocess(req, res, callback) {
                var indicatorArr = req.indicators();
                if (indicatorArr && indicatorArr.length > 0) {
                    for (var k = 0, reqLen = indicatorArr.length; k < reqLen; k++) {
                        var requestName = indicatorArr[k].name();
                        var reuestParameter = indicatorArr[k].parameter();
                        var indicatorsRes = res.data()[0].Indicators;
                        var resultArr, indicatorData;
                        for (var returnName in indicatorsRes) {
                            if ((requestName == "SMA" || requestName == "EMA") && requestName === returnName) {
                                resultArr = [];
                                indicatorData = indicatorsRes[returnName];
                                for (var i = 0, len = reuestParameter.length; i < len; i++) {
                                    for (var j = 0, dataLen = indicatorData.length; j < dataLen; j++) {
                                        if (reuestParameter[i] == indicatorData[j].parameter) {
                                            resultArr.push(indicatorData[j]);
                                            break;
                                        }
                                    }
                                }
                                indicatorsRes[returnName] = resultArr;
                                break;
                            }
                        }
                    }
                }
                IPODateProcess(req, res, callback);
            }

            function IPODateProcess(req, res, callback) {
                if (res && (res.status().errorCode === "0") && res.data().length > 0) {
                    if (res.data()[0].Price.data[0] && (req.preloadInfo.intervalType === IntervalTypes.Max)) {

                        var IPODate = config.mainTickerObject.IPODate;
                        var currentIPODate = res.data()[0].Price.data[0].date;
                        if (!IPODate || (currentIPODate < IPODate)) {
                            config.mainTickerObject.IPODate = currentIPODate;
                        }
                    }
                    callback(res, req.preloadInfo);
                }
            }

            function getIndicators() {
                var indicators = [];
                if (config.indicators && config.indicators.length > 0) {
                    config.indicators.forEach(function(indicator) {
                        indicators.push(new dataAdapter.Indicator(indicator.name, indicator.parameters));
                    });
                }
                return indicators;
            }

            function exportToExcel() {
                eventManager.trigger(eventTypes.ExportData);
            }

            function bindEvents() {
                eventManager.bind(eventTypes.ChangeIDFConfig, changeIntervalFrequency);
                eventManager.bind(eventTypes.ChangeIndicators, changeIndicators);
                eventManager.bind(eventTypes.ChangeFundamental, changeFundamental);
                eventManager.bind(eventTypes.ChangeEvents, changeEvents);
                eventManager.bind(eventTypes.ChangeTicker, changeMainTicker);
                eventManager.bind(eventTypes.CompareTicker, compareTicker);
                eventManager.bind(eventTypes.Export, exportToExcel);
                eventManager.bind(eventTypes.ChangeDateRangeByZoom, changeDateRangeByZoom);
                eventManager.bind(eventTypes.ChangeChartCursor, changeChartCursor);
                eventManager.bind(eventTypes.ChangeYAxis, changeYAxis);
                eventManager.bind(eventTypes.ChangeSelectionMode, changeSelectionMode);
                eventManager.bind(eventTypes.ChangeVerticalScale, changeVerticalScale);
                eventManager.bind(eventTypes.FundamentalNotAvailable, changeFundamentalStatus);
                eventManager.bind(eventTypes.ChangeChartVolume, changeChartVolume);
                eventManager.bind(eventTypes.ChangeChartVolumePlus, changeChartVolumePlus);
                eventManager.bind(eventTypes.ChartDataUpdate, processChartDataUpdate);
                eventManager.bind(eventTypes.DestroyChart, processDestroyChart);
                eventManager.bind(eventTypes.ChangeOneDayFrequency, changeOneDayFrequency);
                eventManager.bind(eventTypes.ChangeGrowthType, changeGrowthType);
                eventManager.bind(eventTypes.ChangeChartType, changeChartType);
                eventManager.bind(eventTypes.Reset, updateChart);
                eventManager.bind(eventTypes.ChangeGrowthBaseValue, _changGrowthBaseValue);
                eventManager.bind(eventTypes.ChangeExtendedMarketHour, _changeExtendedMarketHour);
                eventManager.bind(eventTypes.ChangeStartDate, _changeStartDate);

                // bind callback
                registerCallback(config.callbacks);
            }

            function _changeExtendedMarketHour() {
                var newConfig = {};
                newConfig.showPreAfter = !config.showPreAfter;
                setConfig(newConfig);
                eventManager.trigger(eventTypes.DrawNewData);
            }

            function changeGrowthType(growthType) {
                config.growthType = growthType;
                if (growthType == "performancesGrowth") {
                    config.charts.mainChart.dataType = MainDataTypes.Growth10K;
                } else if (growthType == "priceGrowth") {
                    config.charts.mainChart.dataType = MainDataTypes.Growth;
                }
                eventManager.trigger(eventTypes.DrawNewData);
            }

            function changeOneDayFrequency(isChange) {
                var frequencyMap = config.intervalFrequencyMap;
                if (isChange) {
                    if (!frequencyMap.OneDay_BackUp) {
                        frequencyMap.OneDay_BackUp = frequencyMap.OneDay;
                    }
                    frequencyMap.OneDay = frequencyMap.FiveDay;
                } else if (frequencyMap.OneDay_BackUp) {
                    frequencyMap.OneDay = frequencyMap.OneDay_BackUp;
                }
            }

            function processDestroyChart(clearTickers) {
                if (clearTickers !== false) {
                    dataAdapter.unregisterChartTickers(tickerCollection);
                    tickerCollection = [];
                }

                eventManager.clearBinders();
            }

            function isIntraday(frequency) {
                frequency = frequency || config.frequency;
                return !isNaN(frequency);
            }

            function processChartDataUpdate() {
                if (config.intervalType && isIntraday(config.frequency)) {
                    eventManager.trigger(eventTypes.DrawNewData);
                }
            }

            function changeChartVolume(status) {
                config.charts.volumeChart.show = status;
                eventManager.trigger(eventTypes.ChangeVolumeChart);
            }

            function changeChartVolumePlus(status) {
                var chartType = status ? VolumeChartTypes.VolumePlusChart : VolumeChartTypes.VolumeChart;
                setVolumeChartType(chartType);
                eventManager.trigger(eventTypes.ChangeVolumeChart);
            }

            function changeFundamentalStatus(status) {
                config.fundamentalNotAvailable = status;
            }

            function changeSelectionMode(selectionMode) {
                var newConfig = {};
                newConfig.selectionMode = config.selectionMode;
                newConfig.charts = $.extend(true, newConfig.charts, config.charts);
                if (selectionMode) {
                    newConfig.selectionMode = selectionMode;
                    newConfig.charts.mainChart.selectionMode = selectionMode;
                }
                setConfig(newConfig);
            }

            function changeVerticalScale(verticalScaleType) {
                var newConfig = {};
                newConfig.verticalScaleType = verticalScaleType;
                setConfig(newConfig);
            }

            function changeYAxis(position) {
                var newConfig = {};
                newConfig.yAxisOrient = config.yAxisOrient;
                newConfig.charts = $.extend(true, newConfig.charts, config.charts);
                if (position) {
                    newConfig.yAxisOrient = position;
                    newConfig.charts.mainChart.yAxis.orient = position;
                    newConfig.charts.volumeChart.yAxis.orient = position;
                    newConfig.charts.sliderChart.yAxis.orient = position;
                    newConfig.charts.indicatorChart.yAxis.orient = position;
                    newConfig.charts.fundamentalChart.yAxis.orient = position;
                }
                setConfig(newConfig);
            }

            function changeShowGridBlocks(showGridBlocks) {
                var newConfig = config;
                newConfig.charts.mainChart.showGridBlocks = showGridBlocks;
                newConfig.charts.volumeChart.showGridBlocks = showGridBlocks;
                newConfig.charts.sliderChart.showGridBlocks = showGridBlocks;
                newConfig.charts.indicatorChart.showGridBlocks = showGridBlocks;
                newConfig.charts.fundamentalChart.showGridBlocks = showGridBlocks;
                setConfig(newConfig);
            }

            function changeShowSolidGridLines(showSolidGridLines) {
                var newConfig = config;
                newConfig.charts.mainChart.showSolidGridLines = showSolidGridLines;
                newConfig.charts.volumeChart.showSolidGridLines = showSolidGridLines;
                newConfig.charts.sliderChart.showSolidGridLines = showSolidGridLines;
                newConfig.charts.indicatorChart.showSolidGridLines = showSolidGridLines;
                newConfig.charts.fundamentalChart.showSolidGridLines = showSolidGridLines;
                setConfig(newConfig);
            }

            function changeChartCursor(newCursorType) {
                if (lastCursorType !== newCursorType) {
                    var newConfig = {};
                    newConfig.cursorType = newCursorType;
                    setConfig(newConfig);
                    config.$componentContainer.find('.' + config.cursorValueClass).text('');
                    lastCursorType = newCursorType;
                }
            }

            function compareTicker(ticker, toRemove, isFromRemoveBtn) {
                if (toRemove) {
                    removeCompareTickers(ticker, isFromRemoveBtn);
                } else {
                    addCompareTickers([ticker]);
                }
            }

            function changeDateRangeByZoom(newDateRange) {
                if ($.isArray(newDateRange) && newDateRange.length === 2) {
                    var newConfig = {};
                    newConfig.dateRange = newDateRange;
                    newConfig.intervalType = null;
                    setConfig(newConfig);
                }
            }

            function changeEvents(eventType) {
                if (eventType === dataAdapter.Events.Dividend) {
                    config.charts.mainChart.showDividend = !config.charts.mainChart.showDividend;
                }

                var newConfig = {
                    events: config.events
                };
                var existingEvents = newConfig.events.filter(function(e) {
                    return e === eventType;
                });
                if (existingEvents.length > 0) {
                    newConfig.events = newConfig.events.filter(function(e) {
                        return e !== eventType;
                    });
                } else {
                    newConfig.events.push(eventType);
                }
                setConfig(newConfig);
            }

            function isIndicatorsFundamentals(name) {
                return dataAdapter.IndicatorName.DYield === name || dataAdapter.IndicatorName.RDividend === name;
            }

            function changeFundamental(fundamental) {
                var newConfig = {};
                if (isIndicatorsFundamentals(fundamental)) {
                    newConfig = {
                        indicators: config.indicators
                    };
                    var existingIndicator = newConfig.indicators.filter(function(cf) {
                        return cf.name === fundamental;
                    });
                    if (existingIndicator.length > 0) {
                        newConfig.indicators = newConfig.indicators.filter(function(cf) {
                            return cf.name !== fundamental;
                        });
                    } else {
                        newConfig.indicators.push({
                            name: fundamental,
                            parameters: []
                        });

                        //Add dividend if not exists
                        var eventsConfig = {
                            events: config.events
                        };
                        var dividendExists = eventsConfig.events.filter(function(e) {
                            return e === 'DD001';
                        });
                        if (dividendExists.length === 0) {
                            eventsConfig.events.push('DD001');
                        }
                        setConfig(eventsConfig);
                    }
                } else {
                    newConfig = {
                        fundamentals: config.fundamentals
                    };
                    var existingFundamental = newConfig.fundamentals.filter(function(f) {
                        return f === fundamental;
                    });

                    if (existingFundamental.length > 0) {
                        newConfig.fundamentals = newConfig.fundamentals.filter(function(f) {
                            return f !== fundamental;
                        });
                    } else {
                        newConfig.fundamentals.push(fundamental);
                    }
                }
                setConfig(newConfig);
            }

            function changeIndicators(removeIndicator, indicator, indicatorParameters, removeALL) {
                var newConfig = {
                    indicators: config.indicators
                };
                var existingIndicator = newConfig.indicators
                    .filter(function(cf) {
                        return cf.name === indicator;
                    });

                if (removeIndicator) {
                    if (existingIndicator.length > 0) {
                        if (removeALL) {
                            newConfig.indicators = newConfig.indicators.filter(function(cf) {
                                return cf.name !== indicator;
                            });
                        } else {
                            if (typeof(indicatorParameters) === 'undefined' ||
                                ($.isArray(indicatorParameters) && indicatorParameters.length === 0)) {
                                newConfig.indicators = newConfig.indicators.filter(function(cf) {
                                    return cf.name !== indicator;
                                });
                            } else {
                                existingIndicator[0].parameters = existingIndicator[0].parameters
                                    .filter(function(p) {
                                        return p !== indicatorParameters;
                                    });

                                if (existingIndicator[0].parameters.length === 0) {
                                    newConfig.indicators = newConfig.indicators.filter(function(cf) {
                                        return cf.name !== indicator;
                                    });
                                }
                            }
                        }
                    }

                } else {
                    indicatorParameters = indicatorParameters.filter(function(ip) {
                        return ip;
                    });
                    if (existingIndicator.length > 0) {
                        existingIndicator[0].parameters = indicatorParameters;
                    } else {
                        newConfig.indicators.push({
                            name: indicator,
                            parameters: indicatorParameters
                        });
                    }
                }

                setConfig(newConfig);
            }

            function changeIntervalFrequency(newInterval, newDateRange, newFrequency) {
                var oldIntervalType = config.intervalType;
                var oldFrequency = config.frequency;
                if (!newInterval || oldIntervalType !== newInterval || oldFrequency !== newFrequency) {
                    var newConfig = {};
                    newConfig.dateRange = newDateRange;
                    newConfig.frequency = isNaN(newFrequency) ? newFrequency : parseInt(newFrequency, 10);
                    newConfig.intervalType = newInterval;
                    setConfig(newConfig);
                    eventManager.trigger(eventTypes.ChangeIntervalFrequency);
                }
            }

            /**
             * @param {Array} dataRange
             * @param {string} frequency
             * @description
             *  when current data greater then end date
             *  if frequency equal weekly
             *  then change the start date to the first day of this week.
             *  if frequency equal monthly 
             *  then change the start date to the first day for this month. 
             * 
             *  Monday is begin of a week
             */
            function _changeDateRangeByFrequency(frequency, dataRange, isDatePicker) {
                var currDate = new Date(),
                    _dataRange;
                if (dataRange && 　dataRange.length > 0) {
                    _dataRange = [].slice.apply(dataRange);
                    var startMonth = _dataRange[0].getMonth(),
                        startYear = _dataRange[0].getFullYear(),
                        startDay = _dataRange[0].getDay(),
                        startDate = _dataRange[0].getDate(),
                        endMonth = _dataRange[1].getMonth(),
                        endYear = _dataRange[1].getFullYear(),
                        endDay = _dataRange[1].getDay(),
                        endDate = _dataRange[1].getDate();
                    switch (frequency) {
                        case FrequencyTypes.Weekly:
                            var firstWeekDay = startDate - (startDay || 7) + 1; // first week day
                            _dataRange[0] = _calcuateWeekDate(startYear, startMonth, firstWeekDay, startDay);

                            var lastweekDay = endDate + 5 - _dataRange[1].getDay(); // this week last day
                            _dataRange[1] = _calcuateWeekDate(endYear, endMonth, lastweekDay, endDay);
                            break;
                        case FrequencyTypes.Monthly:
                            _dataRange[0] = _calcuateMonthDate(startYear, startMonth, startDate, isDatePicker);
                            var lastDate = _lastWorkDayOfMonth(endYear, endMonth)
                            lastDate = isDatePicker ? Math.min(endDate, lastDate) : Math.max(endDate, lastDate)
                            _dataRange[1] = new Date(endYear, endMonth, lastDate);
                            break;
                        default:
                            break;
                    }
                }
                return _dataRange;
            }

            /**
             * @description calcuate week date
             */
            function _calcuateWeekDate(y, m, d, day) {
                var _lastWorkDay = _lastWorkDayOfMonth(y, m),
                    dateArry = [y, m];
                if (day === 5) {
                    d = 5 + d - 1;
                } else {
                    if (d < 0) {
                        d += _lastWorkDay;
                        dateArry = acrossMonthOrYears(y, m)['less']();
                    } else if (_lastWorkDay < d) {
                        d = d % _lastWorkDay;
                        dateArry = acrossMonthOrYears(y, m)['more']();

                    }
                }
                return new Date(dateArry[0], dateArry[1], d);
            }

            /**
             * @description calcuate month date
             */
            function _calcuateMonthDate(y, m, d, isDatePicker) {
                var dateArry = [y, m],
                    _lastWorkDay = _lastWorkDayOfMonth(y, m);
                if (d !== _lastWorkDay) {
                    dateArry = acrossMonthOrYears(y, m)['less'](); // out of the last workday every month
                    isDatePicker && (dateArry[1] += 1);
                }
                return new Date(dateArry[0], dateArry[1], 1);
            }

            /**
             * @description across the years
             */
            function acrossMonthOrYears(y, m) {
                return {
                    more: function() {
                        if (m === 11) {
                            ++y;
                            m = 0;
                        } else {
                            ++m;
                        }
                        return [y, m];
                    },
                    less: function() {
                        if (m === 0) {
                            --y;
                            m = 11;
                        } else {
                            --m;
                        }
                        return [y, m];
                    }
                }
            }

            /**
             * find workend from last day of one month
             */
            function _lastWorkDayOfMonth(y, m, date) {
                date = date || new Date(y, m + 1, 0).getDate();
                var day = new Date(y, m, date).getDay();
                if (/[1-5]/.test(day)) {
                    return date;
                } else {
                    return _lastWorkDayOfMonth(y, m, --date);
                }
            }


            function _changeStartDate(startDate) {
                var oriStartType = config.startDate;
                if (oriStartType === startDate) {
                    return;
                }
                setConfig({
                    startDate: startDate
                });
                eventManager.trigger(eventTypes.DrawNewData);
            }

            function setRequestByInterval(intervalType, req, frequency) {
                var dataRange;
                switch (intervalType) {
                    case IntervalTypes.OneDay:
                    case IntervalTypes.FiveDay:
                    case IntervalTypes.FifteenDay:
                        fetchDateRangeByfrequency(req, frequency, intervalType);
                        break;
                    default:
                        dataRange = getDateRangeByInterval(intervalType);
                        req.dateRange(dataRange);
                        break;
                }
                return dataRange;
            }

            function fetchDateRangeByfrequency(req, frequency, intervalType) {
                var intervalMap = {
                    OneDay: 1,
                    FiveDay: 5,
                    FifteenDay: 15
                };
                switch (frequency) {
                    case FrequencyTypes.Daily:
                        req.dateRange(getDateRangeByInterval(intervalType));
                        break;
                    default:
                        req.days(intervalMap[intervalType]);
                        break;
                }
            }

            function setConfig(newConfig, triggerEvent) {
                if (newConfig === config) return;
                // if newConfig.intervalType is undefined, we want set config to it. $.extend can't right work.
                for (var key in newConfig) {
                    config[key] = newConfig[key];
                }
                //$.extend(config, newConfig);
                triggerEvent = triggerEvent === false ? false : true;
                if (triggerEvent) {
                    eventManager.trigger(eventTypes.UpdateConfig);
                }
                if ($.isFunction(config.onConfigChange)) {
                    config.onConfigChange({
                        saveConfig: getSaveConfig(),
                        newConfig: newConfig
                    });
                }
            }

            function getChartConfig(chartName) {
                var chartConfig = config.charts[chartName];
                chartConfig.width = config.width - config.paddingLeftRight;
                chartConfig.mainTicker = config.mainTicker;
                if (chartName !== ChartTypes.SliderChart) {
                    chartConfig.preloadDelay = config.preloadDelay;
                    chartConfig.indicators = config.indicators;
                    chartConfig.preloadData = config.preloadData;
                    chartConfig.dateRange = config.dateRange;
                    chartConfig.intervalType = config.intervalType;
                }
                chartConfig.verticalScaleType = config.verticalScaleType;
                chartConfig.domainScaleup = config.domainScaleup;
                chartConfig.lang = config.lang;
                chartConfig.brushImages = config.brushImages;
                return chartConfig;
            }

            function getConfig() {
                return config;
            }

            function getSaveConfig() {
                //The date range's priority is higher than interval type when user open a saved config.
                if (config.dateRange && config.dateRange.length === 2) {
                    config.intervalType = null;
                }
                var index = $.inArray(dataAdapter.Events.Dividend, config.events);
                if (index < 0 && config.charts.mainChart.showDividend === true) {
                    config.events.push(dataAdapter.Events.Dividend);
                }
                var saveConfig = {
                    mainTicker: config.mainTicker,
                    compareTickers: config.compareTickers,
                    events: config.events,
                    indicators: config.indicators,
                    fundamentals: config.fundamentals,
                    charts: {
                        mainChart: {
                            chartType: config.charts.mainChart.chartType,
                            dataType: config.charts.mainChart.dataType,
                            showDividend: config.charts.mainChart.showDividend
                        },
                        volumeChart: {
                            show: config.charts.volumeChart.show,
                            chartType: config.charts.volumeChart.chartType
                        }
                    },
                    chartCursor: config.cursorType,
                    yAxisOrient: config.yAxisOrient,
                    verticalScaleType: config.verticalScaleType,
                    frequency: (config.frequency + '')
                };
                if (config.intervalType) {
                    saveConfig.intervalType = config.intervalType;
                }
                if (config.dateRange && config.dateRange.length === 2) {
                    saveConfig.dateRange = [core.Widget.Chart.Util.formatDateYMD(config.dateRange[0]), core.Widget.Chart.Util.formatDateYMD(config.dateRange[1])];
                }
                return saveConfig;
            }

            function getMainChartType() {
                return config.charts.mainChart.chartType;
            }

            function setMainChartType(chartType) {
                config.charts.mainChart.chartType = chartType;
            }

            function getMainDataType() {
                return config.charts.mainChart.dataType;
            }

            function setMainDataType(dataType) {
                config.charts.mainChart.dataType = dataType;
            }

            function getVolumeChartType() {
                return config.charts.volumeChart.chartType;
            }

            function setVolumeChartType(chartType) {
                var newConfig = {};
                newConfig.charts = $.extend(true, newConfig.charts, config.charts);
                if (chartType) {
                    newConfig.chartType = chartType;
                    newConfig.charts.volumeChart.chartType = chartType;
                }
                setConfig(newConfig);
            }

            function isMainChartType(chartType) {
                var result = false;
                for (var key in MainChartTypes) {
                    if (MainChartTypes[key] === chartType) {
                        result = true;
                        break;
                    }
                }
                return result;
            }

            function isVolumeChartType(chartType) {
                var result = false;
                for (var key in VolumeChartTypes) {
                    if (VolumeChartTypes[key] === chartType) {
                        result = true;
                        break;
                    }
                }
                return result;
            }

            function changeChartType(newChartType, newDataType) {
                if (isMainChartType(newChartType)) {
                    var dataType;
                    switch (newChartType) {
                        case MainChartTypes.DividendEffectChart:
                            newChartType = MainChartTypes.MountainChart;
                            dataType = MainDataTypes.DividendEffect;
                            break;
                        case MainChartTypes.pGrowthChart:
                            newChartType = MainChartTypes.MountainChart;
                            dataType = MainDataTypes.Growth;
                            break;
                        case MainChartTypes.TenKGrowthChart:
                            newChartType = MainChartTypes.MountainChart;
                            dataType = MainDataTypes.Growth10K;
                            break;
                        case MainChartTypes.PostTaxReturnChart:
                            newChartType = MainChartTypes.MountainChart;
                            dataType = MainDataTypes.PostTax;
                            break;
                        case MainChartTypes.PriceWithDividendGrowth:
                            newChartType = MainChartTypes.MountainChart;
                            dataType = MainDataTypes.PriceWithDividendGrowth;
                            break;
                        default:
                            if (newDataType) {
                                dataType = newDataType;
                            } else {
                                dataType = MainDataTypes.Price;
                            }
                            break;
                    }
                    adapteMainChartType(newChartType, dataType);
                } else if (isVolumeChartType(newChartType)) {
                    setVolumeChartType(newChartType);
                }
            }

            function destroyChart(clearTickers) { //true or false
                eventManager.trigger(eventTypes.DestroyChart, clearTickers);
            }

            function getLabels(name, locale) {
                //var asterixLabels = morningstar.asterix.config.getLabels;
                //var _lang = asterixLabels('markets-components-svgchart', config.lang);
                var labels = toolConfig.defaultConfig['svgchart'].labels.values || {};
                var langCode = locale.split('-')[0];
                labelsValues = labels[locale] || labels[langCode] || labels['en'];
                if (config.labels) {
                    labelsValues = $.extend(true, labelsValues, config.labels[locale] || config.labels[langCode]);
                }
                return labelsValues;

            }

            function getLabel(name) {
                return labelsValues[name] || '';
            }

            function changeInterval(interval) {
                eventManager.trigger(eventTypes.ChangeInterval, interval);
            }

            function registerPerformanceTickers(performanceTickers, performanceIds, deferred) {
                var benchmarkTickers = {};
                for (var i = 0, l = performanceTickers.length, tickerObj; i < l; i++) {
                    tickerObj = performanceTickers[i];
                    if (tickerObj.symbol == performanceIds.cid) {
                        tickerObj.setField("benchmarkType", "FundCategory");
                    } else if (tickerObj.symbol == performanceIds.rrid) {
                        tickerObj.setField("benchmarkType", "RelativeRiskBenchmark");
                    } else if (tickerObj.symbol == performanceIds.mIndex) {
                        tickerObj.setField("benchmarkType", "MstarIndex");
                    } else if (tickerObj.symbol == performanceIds.pbid) {
                        tickerObj.setField("benchmarkType", "ProspectusBenchmark");
                    }
                }
                if (performanceTickers.length === 0) {
                    deferred.resolve();
                } else {
                    var unregisterTickerObjects = [];
                    var registerTickers = [];
                    for (var i = 0, l = tickerCollection.length, _tickerObj; i < l; i++) {
                        _tickerObj = tickerCollection[i];
                        var _pid = _tickerObj.getField("performanceId") || _tickerObj.getField("secId");
                        var _benchmarkType = _tickerObj.getField("benchmarkType");
                        if (_benchmarkType) {
                            unregisterTickerObjects.push(_tickerObj);
                        }
                    }
                    for (var idx = 0; idx < performanceTickers.length; idx++) {
                        var isExisted = registerTickers.some(function(ticker) {
                            return ticker.secId && ticker.secId === performanceTickers[idx].secId || ticker.performanceId && ticker.performanceId === performanceTickers[idx].performanceId;
                        });
                        if (!isExisted) {
                            registerTickers.push(performanceTickers[idx]);
                        }
                    }
                    if (unregisterTickerObjects.length > 0) {
                        dataAdapter.unregisterChartTickers(unregisterTickerObjects);
                    }
                    if (registerTickers.length > 0) {
                        dataAdapter.registerChartTickers(registerTickers);
                        var ticker0 = tickerCollection.shift();
                        tickerCollection = [ticker0].concat(registerTickers.concat(tickerCollection));
                    }
                    deferred.resolve();
                }
            }

            function initRelatedBenchmarks() {
                var deferred = $.Deferred();
                var tickers = [];
                var cid = getIdWithCategoryId(mainTickerObject.getField("categoryId"));
                if (config.useEUTimeSeriesData) {
                    cid = mainTickerObject.getField("categoryId");
                }
                var rrid = getIdWithRiskBenchmarkId(mainTickerObject.getField("relativeRiskBenchmarkId"));
                var pbid = getIdForBenchmarks('ProspectusBenchmark');
                var mIndex = getIdForBenchmarks('MstarIndex');
                if (config.useEUTimeSeriesData && config.useEUSecurityData) {
                    rrid = mainTickerObject.getField("benchMarkId");
                } else if (config.useEUTimeSeriesData) {
                    rrid = mainTickerObject.getField("relativeRiskBenchmarkId");
                }
                var tickerObjects = [];
                if (config.showCategory && cid) {
                    if (config.useEUTimeSeriesData && config.useEUSecurityData && dataAdapter.getSecurityObjects(cid)) {
                        tickerObjects.push(dataAdapter.getSecurityObjects(cid));
                    } else if (config.useEUTimeSeriesData) {
                        cid = _buildTickers({
                            id: cid,
                            securityToken: cid + ']'
                        });
                        tickers.push(cid);
                    } else {
                        tickers.push(cid);
                    }
                }
                if (config.showRiskBenchmark && rrid) {
                    if (config.useEUTimeSeriesData && config.useEUSecurityData && dataAdapter.getSecurityObjects(rrid)) {
                        tickerObjects.push(dataAdapter.getSecurityObjects(rrid));
                    } else if (config.useEUTimeSeriesData) {
                        rrid = _buildTickers({
                            id: rrid,
                            securityToken: rrid + ']'
                        });
                        tickers.push(rrid);
                    } else {
                        tickers.push(rrid);
                    }
                }
                if (config.showProspectusBenchmark && pbid) {
                    tickers.push(pbid);
                }
                if (config.showMorningstarIndex && mIndex) {
                    tickers.push(mIndex);
                }
                if (tickers.length > 0) {
                    dataAdapter.createTickerObjects(tickers, function(performanceTickers) {
                        registerPerformanceTickers(performanceTickers, {
                            cid: cid,
                            rrid: rrid,
                            pbid: pbid,
                            mIndex: mIndex
                        }, deferred);
                    });
                } else if (tickerObjects.length > 0) {
                    registerPerformanceTickers(tickerObjects, {
                        cid: cid,
                        rrid: rrid,
                        pbid: pbid,
                        mIndex: mIndex
                    }, deferred);
                } else {
                    deferred.resolve();
                }
                return deferred;
            }

            function getIdForBenchmarks(type) {
                var id, name;
                if (type === 'MstarIndex') {
                    id = mainTickerObject.getField('mstarIndexId');
                    name = mainTickerObject.getField('mstarIndexName');
                } else if (type === 'ProspectusBenchmark') {
                    id = mainTickerObject.getField('prospectusBenchmarkId');
                    name = mainTickerObject.getField('prospectusBenchmarkName');
                }

                if (id) {
                    return id + ';benchmark_' + type + ';' + name;
                }
                return null;
            }

            function getIdWithCategoryId(cid) {
                var mType = mainTickerObject.getField("mType");
                var secId = getSecId(mainTickerObject);
                if (secId && cid && (mType == "FE" || mType == "FC" || mType == "FO")) {
                    return [secId, cid, mType, ''].join("|");
                } else {
                    return null;
                }
            }

            function getIdWithRiskBenchmarkId(rrmid) {
                var mType = mainTickerObject.getField("mType");
                var secId = getSecId(mainTickerObject);
                if (secId && rrmid && (mType == "FE" || mType == "FC" || mType == "FO")) {
                    return [secId, '', '', rrmid].join("|");
                } else {
                    return null;
                }
            }

            function getSecId(tickerObj) {
                return util.isFund(tickerObj) ? tickerObj.getField("secId") : tickerObj.getField("performanceId");
            }

            function adapteMainChartType(chartType, dataType) {
                var _validChartTypes = [];
                if (dataType) {
                    switch (dataType) {
                        case MainDataTypes.Price:
                            _validChartTypes = [MainChartTypes.LineChart, MainChartTypes.MountainChart, MainChartTypes.DotChart, MainChartTypes.ABChart, MainChartTypes.OHLCChart, MainChartTypes.HLCChart, MainChartTypes.CandlestickChart];
                            break;
                        default:
                            _validChartTypes = [MainChartTypes.LineChart, MainChartTypes.MountainChart, MainChartTypes.DotChart, MainChartTypes.ABChart];
                            break;
                    }
                }

                if (chartType && _validChartTypes.length > 0) {
                    if ($.inArray(chartType, _validChartTypes) === -1) {
                        chartType = MainChartTypes.MountainChart;
                    }
                }

                if (chartType) {
                    setMainChartType(chartType);
                }
                if (dataType) {
                    setMainDataType(dataType);
                }

            }

            function getInitConfig() {
                return initedConfig;
            }

            function setDefaultInterval(intervalType) {
                if (config.dateRange && config.dateRange.length === 2) {
                    config.intervalType = null;
                } else {
                    config.intervalType = config.intervalType || intervalType;
                }
            }

            function changeGrowthBaseValue(value) {
                if (typeof value != "number") {
                    console.info("GrowthBaseValue should be a number")
                    if (isNaN(Number(value))) {
                        return;
                    } else {
                        value = Number(value);
                    }
                }
                if (config.growthBaseValue != value && value > 0) {
                    _changGrowthBaseValue(value);
                }
            }

            function _changGrowthBaseValue(value) {
                var newConfig = {};
                newConfig.growthBaseValue = value;
                newConfig.rootGrowthBaseValue = value;
                setConfig(newConfig);
                setMainDataType(MainDataTypes.Growth10K);
                var chartType = getMainChartType();
                eventManager.trigger(eventTypes.ChangeChartType, chartType, MainDataTypes.Growth10K);
            }

            function _isCompareOrPercentChart() {
                var mainDataType = getMainDataType();
                return config.compareTickers.length > 0 ||
                    mainDataType === MainDataTypes.Growth10K ||
                    mainDataType === MainDataTypes.Growth ||
                    mainDataType === MainDataTypes.DividendEffect ||
                    config.showCategory === true ||
                    config.showRiskBenchmark === true;
            }

            function getTickerCollection() {
                return tickerCollection;
            }

            // bind all callbacks to eventManager
            function registerCallback(callbackList) {
                for (var i in callbackList) {
                    eventTypes[i] && eventManager.bind(eventTypes[i], callbackList[i]);
                }
            }

            function setTickerCollection(tickers) {
                tickerCollection = tickers;
            }

            function triggerClickCallback(data) {
                if (typeof config.onClick === 'function') {
                    config.onClick(data);
                }
            }
            return {
                updateCompareTickers: updateCompareTickers,
                addCompareTickers: addCompareTickers,
                ChartTypes: ChartTypes,
                PopoverTypes: PopoverTypes,
                Constants: Constants,
                createConfig: createConfig,
                ChartGenre: ChartGenre,
                fetchMainTickerData: fetchMainTickerData,
                fetchDisplayData: fetchDisplayData,
                fetchSliderChartData: fetchSliderChartData,
                frequencyConfig: frequencyConfig,
                intervalTypeSortedArray: intervalTypeSortedArray,
                FrequencyTypes: FrequencyTypes,
                CursorType: CursorType,
                changeMainTicker: changeMainTicker,
                getChartConfig: getChartConfig,
                getConfig: getConfig,
                getDefaultfrequencyByIntervalType: getDefaultfrequencyByIntervalType,
                getfrequencyByDateRange: getfrequencyByDateRange,
                getSliderChartfrequency: getSliderChartfrequency,
                getSaveConfig: getSaveConfig,
                getMainChartType: getMainChartType,
                getMainDataType: getMainDataType,
                getPriceType: getPriceType,
                IntervalTypes: IntervalTypes,
                IndicatorChartTypes: IndicatorChartTypes,
                ExternalTypesMapping: ExternalTypesMapping,
                MainChartTypes: MainChartTypes,
                MainDataTypes: MainDataTypes,
                preloadMainTickerData: preloadMainTickerData,
                removeCompareTickers: removeCompareTickers,
                setConfig: setConfig,
                SelectionModes: SelectionModes,
                StartDateTypes: toolConfig.StartDateTypes,
                getTickerCollection: getTickerCollection,
                setTickerCollection: setTickerCollection,
                TimezoneMap: TimezoneMap,
                VolumeChartTypes: VolumeChartTypes,
                VerticalScaleTypes: VerticalScaleTypes,
                isIntraday: isIntraday,
                isMutualFund: isMutualFund,
                destroyChart: destroyChart,
                getInitConfig: getInitConfig,
                getLabels: getLabels,
                getLabel: getLabel,
                changeInterval: changeInterval,
                indicatorInputTypes: toolConfig.indicatorInputTypes,
                isIndicatorsFundamentals: isIndicatorsFundamentals,
                changeGrowthBaseValue: changeGrowthBaseValue,
                isCompareOrPercentChart: _isCompareOrPercentChart,
                isITFund: isITFund,
                isSemiEarningsExch: isSemiEarningsExch,
                getD3Locale: getD3Locale,
                triggerClickCallback: triggerClickCallback,
                intervalTypesMapping: IntervalTypesMapping
            };
        }

        return configManager;
    });