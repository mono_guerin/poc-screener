define(['./autocomplete_us', './autocomplete_eu'], function(autocomplete_us, autocomplete_eu) {
    'use strict';

    var acDataManager = {
        dataDomain: function(value) {
            if (!arguments.length) {
                return autocomplete_us.dataDomain();
            }
            autocomplete_us.dataDomain(value);
            return this;
        },
        fetchData: function(params, callback) {
            if (params.autoComplete.dataSource.toUpperCase() === 'EU') {
                autocomplete_eu.fetchData(params, callback);
            } else {
                autocomplete_us.fetchData(params, callback);
            }
        },
        fetchRelatedTickersAndMajorIndexData: function(pId, mainTickerType, callback) {
            if (useEUAutoComplete) {
                autocomplete_eu.fetchRelatedTickersAndMajorIndexData(queryKey, category, nonFilterCategory, callback);
            } else {
                autocomplete_us.fetchRelatedTickersAndMajorIndexData(queryKey, category, nonFilterCategory, callback);
            }
        }
    };
    return acDataManager;
});