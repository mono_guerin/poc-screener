'use strict';

define(['../../core/core', 'd3'], function(core, d3) {
    var $ = core.$;
    var CommonXLabelDateTimeFormat = {
        'YearMonthDay': '%b %d, %Y',
        'Year': '%Y',
        'YearMonth': '%Y %b',
        'Month': '%b',
        'MonthDay': '%b %d',
        'HourMinutes': '%I:%M %p'
    };
    var ChinaXLabelDateTimeFormat = {
        'YearMonthDay': function(d) {
            return d.getFullYear() + '年' + (d.getMonth() + 1) + '月' + d.getDate() + '日';
        },
        'Year': function(d) {
            return d.getFullYear() + '年';
        },
        'YearMonth': function(d) {
            return d.getFullYear() + '年' + (d.getMonth() + 1) + '月';
        },
        'Month': function(d) {
            return (d.getMonth() + 1) + '月';
        },
        'MonthDay': function(d) {
            return (d.getMonth() + 1) + '月' + d.getDate() + '日';
        },
        'HourMinutes': '%I:%M %p'
    };
    var localeConfig = {
        'en': {
            'decimal': '.',
            'thousands': ',',
            'grouping': [3],
            'currency': ['$', ''],
            'dateTime': '%a %b %e %X %Y',
            'date': '%m/%d/%Y',
            'time': '%H:%M:%S',
            'periods': ['AM', 'PM'],
            'days': ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
            'shortDays': ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            'months': ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            'shortMonths': ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            'dailyDateFormatter': 'MM/DD/YYYY',
            'intradayDateFormater': 'MM/DD/YYYY hh:mm A',
            'momentDateTime': 'MMM DD YYYY hh:mm A',
            'momentDate': 'MMM DD YYYY',
            'columnDelimiter': ',',
            'XLabelDateTimeFormat': CommonXLabelDateTimeFormat
        },
        'en-GB': {
            'decimal': '.',
            'thousands': ',',
            'grouping': [3],
            'currency': ['$', ''],
            'dateTime': '%a %b %e %X %Y',
            'date': '%m/%d/%Y',
            'time': '%H:%M:%S',
            'periods': ['AM', 'PM'],
            'days': ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
            'shortDays': ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            'months': ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            'shortMonths': ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            'dailyDateFormatter': 'DD/MM/YYYY',
            'intradayDateFormater': 'DD/MM/YYYY hh:mm A',
            'momentDateTime': 'DD MMM YYYY hh:mm A',
            'momentDate': 'DD MMM YYYY',
            'columnDelimiter': ',',
            'XLabelDateTimeFormat': CommonXLabelDateTimeFormat
        },
        'fr': {
            'decimal': ',',
            'thousands': ' ',
            'grouping': [3],
            'dateTime': '%A, le %e %B %Y, %X',
            'date': '%d/%m/%Y',
            'time': '%H:%M:%S',
            'periods': ['AM', 'PM'],
            'days': ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'],
            'shortDays': ['dim.', 'lun.', 'mar.', 'mer.', 'jeu.', 'ven.', 'sam.'],
            'months': ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'],
            'shortMonths': ['janv.', 'févr.', 'mars', 'avr.', 'mai', 'juin', 'juil.', 'août', 'sept.', 'oct.', 'nov.', 'déc.'],
            'dailyDateFormatter': 'DD/MM/YYYY',
            'intradayDateFormater': 'DD/MM/YYYY hh:mm A',
            'momentDateTime': 'MMM DD YYYY hh:mm A',
            'momentDate': 'DD MMM YYYY',
            'columnDelimiter': ';',
            'XLabelDateTimeFormat': CommonXLabelDateTimeFormat
        },
        'zh': {
            'decimal': '.',
            'thousands': ',',
            'grouping': [3],
            'currency': ['$', ''],
            'dateTime': '%a %b %e %X %Y',
            'date': '%m/%d/%Y',
            'time': '%H:%M:%S',
            'periods': ['AM', 'PM'],
            'days': ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
            'shortDays': ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
            'months': ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
            'shortMonths': ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
            'dailyDateFormatter': 'YYYY/MM/DD',
            'intradayDateFormater': 'YYYY/MM/DD hh:mm A',
            'momentDateTime': 'YYYY MM DD hh:mm A',
            'momentDate': 'YYYY MMM DD',
            'columnDelimiter': ',',
            'XLabelDateTimeFormat': ChinaXLabelDateTimeFormat
        },
        'zh-TW': {
            'decimal': '.',
            'thousands': ',',
            'grouping': [3],
            'currency': ['$', ''],
            'dateTime': '%a %b %e %X %Y',
            'date': '%m/%d/%Y',
            'time': '%H:%M:%S',
            'periods': ['AM', 'PM'],
            'days': ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
            'shortDays': ['週日', '週一', '週二', '週三', '週四', '週五', '週六'],
            'months': ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
            'shortMonths': ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
            'dailyDateFormatter': 'YYYY/MM/DD',
            'intradayDateFormater': 'YYYY/MM/DD hh:mm A',
            'momentDateTime': 'YYYY MMM DD hh:mm A',
            'momentDate': 'YYYY MMM DD',
            'columnDelimiter': ',',
            'XLabelDateTimeFormat': ChinaXLabelDateTimeFormat
        },
        'zh-HK': {
            'decimal': '.',
            'thousands': ',',
            'grouping': [3],
            'currency': ['$', ''],
            'dateTime': '%a %b %e %X %Y',
            'date': '%m/%d/%Y',
            'time': '%H:%M:%S',
            'periods': ['AM', 'PM'],
            'days': ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
            'shortDays': ['週日', '週一', '週二', '週三', '週四', '週五', '週六'],
            'months': ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
            'shortMonths': ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
            'dailyDateFormatter': 'YYYY/MM/DD',
            'intradayDateFormater': 'YYYY/MM/DD hh:mm A',
            'momentDateTime': 'MMM DD YYYY hh:mm A',
            'momentDate': 'YYYY MMM DD',
            'columnDelimiter': ',',
            'XLabelDateTimeFormat': ChinaXLabelDateTimeFormat
        },
        'pt': {
            'decimal': ',',
            'thousands': '.',
            'grouping': [3],
            'currency': ['$', ''],
            'dateTime': '%A, le %e %B %Y, %X',
            'date': '%d/%m/%Y',
            'time': '%H:%M:%S',
            'periods': ['AM', 'PM'],
            'days': ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
            'shortDays': ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
            'months': ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            'shortMonths': ['Jan.', 'Fev.', 'Mar.', 'Abr.', 'Maio', 'Jun.', 'Jul.', 'Ago.', 'Set.', 'Out.', 'Nov.', 'Dez.'],
            'dailyDateFormatter': 'DD/MM/YYYY',
            'intradayDateFormater': 'DD/MM/YYYY hh:mm A',
            'momentDateTime': 'DD MMM YYYY hh:mm A',
            'momentDate': 'DD MMM YYYY',
            'columnDelimiter': ';',
            'XLabelDateTimeFormat': CommonXLabelDateTimeFormat
        },
        'ja': {
            'decimal': '.',
            'thousands': ',',
            'grouping': [3],
            'currency': ['$', ''],
            'dateTime': '%a %b %e %X %Y',
            'date': '%m/%d/%Y',
            'time': '%H:%M:%S',
            'periods': ['AM', 'PM'],
            'days': ['日曜日', '月曜日', '火曜日', '水曜日', '木曜日', '金曜日', '土曜日'],
            'shortDays': ['日', '月', '火', '水', '木', '金', '土'],
            'months': ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
            'shortMonths': ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
            'dailyDateFormatter': 'YYYY/MM/DD',
            'intradayDateFormater': 'YYYY/MM/DD hh:mm A',
            'momentDateTime': 'YYYY MM DD hh:mm A',
            'momentDate': 'YYYY MMM DD',
            'columnDelimiter': ',',
            'XLabelDateTimeFormat': ChinaXLabelDateTimeFormat
        },
        'it': {
            'decimal': ',',
            'thousands': '.',
            'grouping': [3],
            'currency': ['$', ''],
            'dateTime': '%A, le %e %B %Y, %X',
            'date': '%d/%m/%Y',
            'time': '%H:%M:%S',
            'periods': ['AM', 'PM'],
            'days': ['Domenica', 'Lunedì', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'],
            'shortDays': ['Dom', 'Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab'],
            'months': ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'],
            'shortMonths': ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'],
            'dailyDateFormatter': 'DD/MM/YYYY',
            'intradayDateFormater': 'DD/MM/YYYY hh:mm A',
            'momentDateTime': 'DD MMM YYYY hh:mm A',
            'momentDate': 'DD MMM YYYY',
            'columnDelimiter': ';',
            'XLabelDateTimeFormat': CommonXLabelDateTimeFormat
        },
        'da': {
            'decimal': ',',
            'thousands': '.',
            'grouping': [3],
            'currency': ['$', ''],
            'dateTime': '%A den %d %B %Y %X',
            'date': '%d-%m-%Y',
            'time': '%H:%M:%S',
            'periods': ['AM', 'PM'],
            'days': ['søndag', 'mandag', 'tirsdag', 'onsdag', 'torsdag', 'fredag', 'lørdag'],
            'shortDays': ['søn', 'man', 'tir', 'ons', 'tor', 'fre', 'lør'],
            'months': ['januar', 'februar', 'marts', 'april', 'maj', 'juni', 'juli', 'august', 'september', 'oktober', 'november', 'december'],
            'shortMonths': ['jan', 'feb', 'mar', 'apr', 'maj', 'jun', 'jul', 'aug', 'sep', 'okt', 'nov', 'dec'],
            'dailyDateFormatter': 'DD/MM/YYYY',
            'intradayDateFormater': 'DD/MM/YYYY hh:mm A',
            'momentDateTime': 'DD MMM YYYY hh:mm A',
            'momentDate': 'DD MMM YYYY',
            'columnDelimiter': ';',
            'XLabelDateTimeFormat': CommonXLabelDateTimeFormat
        },
        'sv': {
            'decimal': ',',
            'thousands': '.',
            'grouping': [3],
            'currency': ['$', ''],
            'dateTime': '%A den %d %B %Y %X',
            'date': '%Y-%m-%d',
            'time': '%H:%M:%S',
            'periods': ['fm', 'em'],
            'days': ['Söndag', 'Måndag', 'Tisdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lördag'],
            'shortDays': ['Sön', 'Mån', 'Tis', 'Ons', 'Tor', 'Fre', 'Lör'],
            'months': ['januari', 'februari', 'mars', 'april', 'maj', 'juni', 'juli', 'augusti', 'september', 'oktober', 'november', 'december'],
            'shortMonths': ['jan', 'feb', 'mars', 'apr', 'maj', 'jun', 'jul', 'aug', 'sep', 'okt', 'nov', 'dec'],
            'dailyDateFormatter': 'YYYY-MM-DD',
            'intradayDateFormater': 'YYYY-MM-DD HH:mm',
            'momentDateTime': 'YYYY-MM-DD HH:mm',
            'momentDate': 'YYYY-MM-DD',
            'columnDelimiter': ';',
            'XLabelDateTimeFormat': {
                'YearMonthDay': '%b %d, %Y',
                'Year': '%Y',
                'YearMonth': '%Y %b',
                'Month': '%b',
                'MonthDay': '%b %d',
                'HourMinutes': '%H:%M'
            }
        }
    };

    function getSpecifier(languageId) {
        var specifier;
        if (localeConfig[languageId]) {
            specifier = localeConfig[languageId];
        } else {
            languageId = languageId.split('-')[0];
            specifier = localeConfig[languageId] || localeConfig['en'];
        }
        return specifier;
    }

    function getD3Locale(languageId) {
        var localeObject = {};
        var specifier = getSpecifier(languageId);
        var d3_locale = d3.locale(specifier);
        var originD3LocaleTimeFormat = d3_locale.timeFormat;
        d3_locale.timeFormat = function(template) {
            if ($.isFunction(template)) {
                return template;
            } else {
                template = specifier[template] || template;
                return originD3LocaleTimeFormat(template);
            }
        }
        return $.extend(true, {}, d3_locale, specifier);
    }

    return {
        getD3Locale: getD3Locale
    };
});