﻿define(["../../../html/layoutTemplate.html", "./graphTypes", './staticConfig', './languageConfig'],
    function(layoutTemplate, GraphTypes, StaticConfig, LanguageConfig) {
        'use strict';

        var defaultConfig = {
            'svgchart': {
                'type': 'svgchart',
                'settings': {
                    autoComplete: {
                        dataSource: 'US',
                        universeIds: '',
                        sortOrder: 'Name+Asc',
                        //group:true,
                        //highlight: false,
                        //action: 'compare',//main
                        //changeTicker: true,
                        //nonFilterCategory: [],
                        //tickerPriority: 'ticker',
                        //removeExch: false
                    },
                    StaticFilesPath: '//quotespeed.morningstar.com/components/static/',
                    AlignToCompare: false,
                    hideMenu: false,
                    legendInfoLabel: 'clientTicker',
                    theme: '1',
                    columnDelimiter: ",",
                    showCategory: false,
                    showRiskBenchmark: false,
                    showProspectusBenchmark: false,
                    showMorningstarIndex: false,
                    yAxisMargin: 2,
                    yAxisWidth: 42,
                    dailyDateFormatter: "MM/DD/YYYY",
                    intradayDateFormater: "MM/DD/YYYY hh:mm A",
                    momentDateTime: "MMM DD YYYY hh:mm A",
                    momentDate: "MMM DD YYYY",
                    enterTipsLabelID: "",
                    crosshairJustify: false,
                    mainTickerObject: {},
                    crosshairs: {}, //format crosshairs label value
                    crossCurrency: null, // can be "symbol" => $ or "code" => USD.
                    legendChange: true,
                    showLegendNetChange: true,
                    showLegendPercentChange: true,
                    compareClose: true,
                    legendDot: false,
                    defaultDecimal: 2,
                    decimalPlaces: null, //for crosshair, change price value, default: config.defaultDecimal
                    yAxisDecimal: null, //for y-axis ticks, default: config.defaultDecimal
                    isFormat: true,
                    chgPDecimal: 2, // for change percent value 
                    volumeDecimal: 2, //for crosshair volume value, eg:1.21 mil
                    chartGenre: StaticConfig.ChartGenre.StockChart,
                    onLoad: function(loadInfo) { /*console.log(loadInfo);*/ },
                    onHeightChange: function(heightInfo) { /*console.log(heightInfo);*/ },
                    template: layoutTemplate(),
                    container: document.body,
                    mainChartClass: "chart-mainchart",
                    volumeChartClass: "chart-volumechart",
                    sliderChartClass: "chart-sliderchart",
                    paddingTopBottom: 0,
                    paddingLeftRight: 0,
                    exchangeMainTickerByLegend: true,
                    useEUTimeSeriesData: false,
                    showTag: true,
                    EUTimeSeriesApiSetting: {
                        "timeseries_price": {
                            forwardFill: true
                        },
                        "timeseries_cumulativereturn": {
                            restructureDateOptions: 'ignore'
                        },
                        "timeseries_dividend": {}
                    },
                    useEUSecurityData: false,
                    //margin: { left: 20, top: 20, right: 20, bottom: 40 },
                    //width: 800,
                    //height:900,
                    fixHeight: true,
                    showExportMenuTab: false,
                    crossPerformanceVal: StaticConfig.MainDataTypes.Growth, // growth or 10K growth; for performace growth chart in theme4, config the data type show in crosshair's performance value.
                    crossValue: StaticConfig.MainDataTypes.Growth10K,
                    showPreAfter: false,
                    applyAspectRatio: true,
                    minWidth: 240, //mainchart min width is 200, margin-left 20, margin-right 20
                    maxWidth: Number.MAX_VALUE,
                    intervalType: null,
                    frequency: null,
                    defaultFrequency: null,
                    frequencyPriority: 'default', // optional value: default
                    dateRange: [],
                    indicators: [ /*{ name: "SMA", parameters: [10, 50] }*/ ],
                    fundamentals: [],
                    events: [],
                    /*"dividend" "splits" "earnings"*/
                    //chartType: StaticConfig.MainChartTypes.MountainChart,
                    showTSDividend: false,
                    autocompleteCategories: StaticConfig.AutocompleteCategories.stock, //only internal use
                    //isAutocompleteGroup: true,
                    funds: null,
                    maxRangeStartDate: new Date(1900, 0, 1),
                    domainScaleup: 0.05,
                    lang: StaticConfig.LanguageTypes.EN_US,
                    exchangeTimezone: {}, //Need get from session server
                    showPricePlusSign: true,
                    timezone: "",
                    mainTickerColor: null,
                    growth10KDecimal: null,
                    verticalScaleType: StaticConfig.VerticalScaleTypes.Linear,
                    semiEarningsExch: ["146", "147"],
                    hightLightHoveredLine: true,
                    highlightDist: 5,
                    showCrosshairVolume: true,
                    showCrosshairChange: false,
                    showCrosshairChangePercent: false,
                    showCrosshairOriginalData: false,
                    isFullyRendered: false,
                    triggeredFullyRendered: false,
                    customBreakPoint: null, //[600,800]
                    showStartDate: false,
                    showPriceWithDividendGrowth: false,
                    startDate: StaticConfig.StartDateTypes.MainTicker, //optional value:commom, earliest,main
                    showMTickerRemoveBtn: false, // show main ticker remove button
                    datePicker: {
                        reposition: true,
                        highlightBetween: false,
                        highlightMutiSelected: false,
                        showDisable: false
                    },
                    intervalBreakPointMap: {
                        'default': ['1D', '5D', '15D', '1M', '3M', '6M', 'YTD', '1Y', '3Y', '5Y', '10Y', 'MAX'],
                        '600': ['1D', '1M', '1Y', '5Y', 'MAX']
                    },
                    intervalFrequencyMap: {
                        OneDay: "1",
                        FiveDay: "5",
                        FifteenDay: "5",
                        OneMonth: "d",
                        ThreeMonth: "d",
                        SixMonth: "d",
                        YTD: "d",
                        OneYear: "d",
                        ThreeYear: "w",
                        FiveYear: "m",
                        TenYear: "m",
                        FifteenYear: "m",
                        Max: "m",
                        Custom: "5"
                    },
                    SliderChartIntervalTypeMap: {
                        OneDay: "ThreeMonth",
                        FiveDay: "ThreeMonth",
                        FifteenDay: "ThreeMonth",
                        OneMonth: "ThreeMonth",
                        ThreeMonth: "SixMonth",
                        SixMonth: "TenYear",
                        YTD: "TenYear",
                        OneYear: "TenYear",
                        ThreeYear: "TenYear",
                        FiveYear: "TenYear",
                        TenYear: "FifteenYear",
                        Max: "Max"
                    },
                    mainTicker: "126.1.IBM",
                    compareTickers: [],
                    preloadDelay: 500,
                    cursorType: StaticConfig.CursorType.Crosshair,
                    cursorValueClass: 'cursor-value',
                    menubar: {},
                    isHidePopoverAfterSelect: false,
                    isShowRelateTickerAndMajorIndex: false,
                    compareChartsMaxAllowed: 7,
                    compareChartsMaxAllowedErrorLabel: 'compareChartsMaxAllowedErrorLabel',
                    compareChartsMaxAllowedErrorMessageTimeout: 8000,
                    compareColors: null,
                    currencyAliases: {}, // {"USD":"$"},
                    currencyType: "listCurrency", // currency type tradeCurrency(D204) , listCurrency(s9)
                    tradeCurrency: [], // support exchange display currency(D204)
                    listCurrency: [],

                    charts: {
                        mainChart: {
                            panResponseSpan: 4,
                            xAxisHeight: 18,
                            margin: {
                                top: 0,
                                right: 1,
                                bottom: 0,
                                left: 1
                            },
                            width: 960,
                            height: 432,
                            minHeight: 147,
                            dataFormat: "%Y%m%d",
                            container: null,
                            xAxis: {
                                show: true,
                                orient: "bottom",
                                tickSize: 1
                            },
                            yAxis: {
                                show: true,
                                orient: "left",
                                ticks: 5,
                                maxTickCount: 6,
                                position: "outer",
                                withUnit: false,
                                width: 42,
                                tick: {
                                    line: {
                                        show: true,
                                        style: {
                                            'stroke': '#cccccc',
                                            'stroke-width': '1px'
                                        }
                                    }
                                }
                            },
                            showGridLines: true,
                            showGridBlocks: false,
                            showSolidGridLines: false,
                            showLegend: false,
                            showIndicatorCloseButton: true,
                            showFundamentalCloseButton: true,
                            tagProperties: {
                                arrowWidth: 5,
                                arrowHeight: 7,
                                sideMargin: 6,
                                textTopMargin: 7,
                                tagHeight: 18,
                                yVerticalMargin: 3,
                                tagFillColor: ''
                            },
                            eventProperties: {
                                xRadious: 5,
                                yRadious: 10,
                                textColor: '#FFF',
                                textAnchor: 'middle',
                                dy: '0.3em',
                                Split: {
                                    color: '#5050CC',
                                    text: 'splitText',
                                    labels: 'eventsSplits'
                                },
                                Earnings: {
                                    color: '#008E91',
                                    downColor: '#AA008A',
                                    text: 'earningText',
                                    labels: 'eventsEarnings'
                                },
                                Dividend: {
                                    color: '#FD5516',
                                    text: 'dividendText',
                                    labels: 'eventsDividende'
                                }
                            },
                            fundamentalProperties: {
                                FairValue: {
                                    color: '#FD5516',
                                    labels: 'fairValue',
                                },
                                QuantitativeFairValue: {
                                    color: '#FD5516',
                                    labels: 'fairValue',
                                },
                                DailyCumFairNav: {
                                    color: '#FD5516',
                                    labels: 'NAV'
                                },
                                FundSize: {
                                    showLegend: false,
                                    labels: 'fundSize',
                                    skipDrawGraph: true,
                                    decimalPlaces: 0,
                                    showWithCompare: true
                                },
                                ShareholdersNumber: {
                                    showLegend: false,
                                    labels: 'shareholdersNumber',
                                    skipDrawGraph: true,
                                    decimalPlaces: 0,
                                    showWithCompare: true
                                }
                            },
                            indicatorProperties: {
                                SMA: {
                                    color: ['#008E91', '#AA008A', '#FD5516'],
                                    legend: function(labels, parameters) {
                                        var labelsArr = [];
                                        parameters.forEach(function(parameter) {
                                            labelsArr.push(labels[0] + '(' + parameter + ')')
                                        });
                                        return labelsArr;
                                    }
                                },
                                EMA: {
                                    color: ['#FD5516', '#5050CC', '#008E91'],
                                    legend: function(labels, parameters) {
                                        var labelsArr = [];
                                        parameters.forEach(function(parameter) {
                                            labelsArr.push(labels[0] + '(' + parameter + ')')
                                        });
                                        return labelsArr;
                                    }
                                },
                                BBands: {
                                    color: ['#5050CC', '#5050CC'],
                                    chartTypes: ['lineChart', 'lineChart'],
                                    removeAll: true,
                                    //example BBands(20, 2)
                                    legend: function(labels, parameters) {
                                        return [
                                            labels[0] + '(' + parameters[0] + ',' + parameters[1] + ')'
                                        ];
                                    }
                                },
                                PSAR: {
                                    color: ['#AA008A'],
                                    chartTypes: ['dotChart'],
                                    removeAll: true,
                                    //example PSAR(0.02, 0.2)
                                    legend: function(labels, parameters) {
                                        return [
                                            labels[0] + '(' + parameters[0] + ',' + parameters[1] + ')'
                                        ];
                                    }
                                },
                                WMA: {
                                    color: ['#FD5516', '#5050CC'],
                                    removeAll: true,
                                    //example WMA(15)
                                    legend: function(labels, parameters) {
                                        return [
                                            labels[0] + '(' + parameters[0] + ')'
                                        ];
                                    }
                                },
                                Keltner: {
                                    color: ['#FD5516', '#5050CC', '#008E91'],
                                    removeAll: true,
                                    //example Keltner Index(20, 2, 10)
                                    legend: function(labels, parameters) {
                                        return [
                                            labels[0] + '(' + parameters[0] + ',' + parameters[1] + ',' + parameters[2] + ')'
                                        ];
                                    }
                                },
                                VBP: {
                                    color: ['#c9cbcb'],
                                    chartTypes: GraphTypes.VBP
                                },
                                PrevClose: {
                                    color: ['#FD5516'],
                                    showLegend: true,
                                    legend: function(labels, parameters) {
                                        return labels[0];
                                    }
                                },
                                PChannel: {
                                    color: ['#AA008A', '#008E91'],
                                    //example PChannel(14)
                                    legend: function(labels, parameters) {
                                        return [
                                            labels[0] + '(' + parameters[0] + ')'
                                        ];
                                    }
                                },
                                MAE: {
                                    color: ['#FD5516', '#AA008A'],
                                    removeAll: true,
                                    //example PChannel(14)
                                    legend: function(labels, parameters) {
                                        return [
                                            labels[0] + '(' + parameters[0] + ',' + parameters[1] + '%)'
                                        ];
                                    }
                                }
                            },
                            dividendEffectProperties: {
                                color: '#293FA0'
                            },
                            abChartAboveClipID: "mainChart-aboveClip",
                            abChartBelowClipID: "mainChart-belowClip",
                            ASColor: null,
                            AFColor: null,
                            BSColor: null,
                            BFColor: null,
                            dotChartRadius: 2,
                            xAxisTickSize: 3,
                            ohlcTickSize: 3,
                            candlestickTickSize: 1,
                            show: true,
                            showDividend: false,
                            chartClass: "chart-mainchart",
                            yAxisDimensionForPercentage: "%",
                            chartType: StaticConfig.MainChartTypes.MountainChart, //.CandlestickChart
                            dataType: StaticConfig.MainDataTypes.Price,
                            selectionMode: StaticConfig.SelectionModes.Pan
                        },
                        volumeChart: {
                            margin: {
                                top: 0,
                                right: 1,
                                bottom: 0,
                                left: 1
                            },
                            width: 960,
                            height: 100,
                            dataFormat: "%Y%m%d",
                            container: null,
                            needInit: true,
                            xAxis: {
                                show: false,
                                orient: "bottom"
                            },
                            yAxis: {
                                show: true,
                                orient: "left",
                                ticks: 2,
                                maxTickCount: 3,
                                position: "outer",
                                width: 42,
                                decimalPlaces: 0,
                                tick: {
                                    line: {
                                        show: true,
                                        style: {
                                            'stroke': '#cccccc',
                                            'stroke-width': '1px'
                                        }
                                    }
                                }
                            },
                            showGridLines: true,
                            showGridBlocks: false,
                            showSolidGridLines: false,
                            showIndicatorCloseButton: true,
                            showLegend: false,
                            indicatorProperties: {
                                SMAV: {
                                    color: ['#AA008A'],
                                    //example SMAV(50)
                                    legend: function(labels, parameters) {
                                        return [
                                            labels[0] + '(' + parameters[0] + ')'
                                        ];
                                    }
                                }
                            },
                            volumeBarWidth: 5,
                            show: true,
                            chartClass: "chart-volumechart",
                            chartLegendText: "volume",
                            chartType: StaticConfig.VolumeChartTypes.VolumeChart, //VolumeChart   //VolumePlusChart
                        },
                        sliderChart: {
                            margin: {
                                top: 0,
                                right: 5,
                                bottom: 18,
                                left: 5
                            },
                            width: 960,
                            height: 65,
                            dataFormat: "%Y%m%d",
                            container: null,
                            xAxis: {
                                show: true,
                                orient: "bottom",
                                tickSize: 1
                            },
                            yAxis: {
                                show: false,
                                orient: "left",
                                tickSize: 0,
                                width: 42,
                                position: "outer",
                                tick: {
                                    line: {
                                        show: true,
                                        style: {
                                            'stroke': '#cccccc',
                                            'stroke-width': '1px'
                                        }
                                    }
                                }
                            },
                            showGridLines: false,
                            showGridBlocks: false,
                            showSolidGridLines: false,
                            show: true,
                            chartClass: "chart-sliderchart",
                        },
                        indicatorChart: {
                            margin: {
                                top: 0,
                                right: 0,
                                bottom: 0,
                                left: 0
                            },
                            width: 960,
                            height: 120,
                            dataFormat: "%Y%m%d",
                            container: null,
                            xAxis: {
                                show: false,
                                orient: "bottom"
                            },
                            yAxis: {
                                show: true,
                                orient: "left",
                                ticks: 3,
                                maxTickCount: 4,
                                width: 42,
                                position: "outer",
                                tick: {
                                    line: {
                                        show: true,
                                        style: {
                                            'stroke': '#cccccc',
                                            'stroke-width': '1px'
                                        }
                                    }
                                }
                            },
                            showGridLines: true,
                            showGridBlocks: false,
                            showSolidGridLines: false,
                            showLegend: false,
                            showIndicatorCloseButton: false,
                            indicatorProperties: {
                                DYield: {
                                    color: ['#008E91'],
                                },
                                RDividend: {
                                    color: ['#008E91'],
                                },
                                MACD: {
                                    color: ['#008E91', '#AA008A', '#FD5516'],
                                    chartTypes: ['lineChart', 'lineChart', 'histogram'],
                                    //example MACD(26,12) EMA(9) Divergence
                                    legend: function(labels, parameters) {
                                        return [
                                            labels[0] + '(' + parameters[0] + ',' + parameters[1] + ')',
                                            labels[1] + ' (' + parameters[2] + ') ',
                                            labels[2]
                                        ];
                                    }
                                },
                                RSI: {
                                    color: ['#AA008A'],
                                    //example RSI(14)
                                    legend: function(labels, parameters) {
                                        return [
                                            labels[0] + '(' + parameters[0] + ')'
                                        ];
                                    },
                                    fixedTickValeus: [0, 30, 70, 100]
                                },
                                ROC: {
                                    color: ['#008E91', '#AA008A'],
                                    //example ROC(12)
                                    legend: function(labels, parameters) {
                                        return [
                                            labels[0] + '(' + parameters[0] + ')'
                                        ];
                                    }
                                },
                                WillR: {
                                    color: ['#FD5516'],
                                    //example WillR(14)
                                    legend: function(labels, parameters) {
                                        return [
                                            labels[0] + '(' + parameters[0] + ')'
                                        ];
                                    },
                                    fixedTickValeus: [-100, -80, -20, 0]
                                },
                                SStochastic: {
                                    color: ['#008E91', '#AA008A'],
                                    //example SStochastic(14)SStochastic(3)
                                    legend: function(labels, parameters) {
                                        return [
                                            labels[0] + '(' + parameters[0] + ')',
                                            labels[0] + '(' + parameters[1] + ')',
                                        ];
                                    }
                                },
                                FStochastic: {
                                    color: ['#FD5516', '#008E91'],
                                    //example FStochastic(14)FStochastic(3)
                                    legend: function(labels, parameters) {
                                        return [
                                            labels[0] + '(' + parameters[0] + ')',
                                            labels[0] + '(' + parameters[1] + ')',
                                        ];
                                    }
                                },
                                Momentum: {
                                    color: ['#AA008A', '#008E91'],
                                    //example Momentum(2)
                                    legend: function(labels, parameters) {
                                        return [
                                            labels[0] + '(' + parameters[0] + ')'
                                        ];
                                    }
                                },
                                ULT: {
                                    color: ['#008E91']
                                },
                                DMI: {
                                    color: ['#AA008A', '#008E91', '#FD5516'],
                                    //example DMI+ DMI- ADX(14)
                                    legend: function(labels, parameters) {
                                        return [
                                            labels[0],
                                            labels[1],
                                            labels[2] + '(' + parameters[0] + ')'
                                        ];
                                    }
                                },
                                MFI: {
                                    color: ['#008E91', '#008E91'],
                                    //example MFI(14)
                                    legend: function(labels, parameters) {
                                        return [
                                            labels[0] + '(' + parameters[0] + ')'
                                        ];
                                    }
                                },
                                OBV: {
                                    color: ['#FD5516', '#008E91']
                                },
                                VAcc: {
                                    color: ['#FD5516', '#008E91']
                                },
                                UDRatio: {
                                    color: ['#FD5516', '#AA008A'],
                                    //example UDRatio(7,5)
                                    legend: function(labels, parameters) {
                                        return [
                                            labels[0] + '(' + parameters[0] + ',' + parameters[1] + ')'
                                        ];
                                    }
                                },
                                FVolatility: {
                                    color: ['#FD5516', '#008E91']
                                },
                                AccDis: {
                                    color: ['#FD5516', '#008E91']
                                },
                                Mass: {
                                    color: ['#FD5516', '#008E91']
                                },
                                Volatility: {
                                    color: ['#FD5516', '#008E91'],
                                    //example Volatility(7,5)
                                    legend: function(labels, parameters) {
                                        return [
                                            labels[0] + '(' + parameters[0] + ',' + parameters[1] + ')'
                                        ];
                                    }
                                },
                                ForceIndex: {
                                    color: ['#FD5516', '#008E91'],
                                    //example ForceIndex(13)
                                    legend: function(labels, parameters) {
                                        return [
                                            labels[0] + '(' + parameters[0] + ')'
                                        ];
                                    }
                                },
                                ATR: {
                                    color: ['#008E91']
                                }
                            },
                            show: true,
                            chartClass: "chart-indicatorchart",
                            chartType: StaticConfig.IndicatorChartTypes.LineChart, //LineChart
                        },
                        fundamentalChart: {
                            margin: {
                                top: 0,
                                right: 0,
                                bottom: 0,
                                left: 0
                            },
                            width: 960,
                            height: 120,
                            dataFormat: "%Y%m%d",
                            container: null,
                            xAxis: {
                                show: false,
                                orient: "bottom"
                            },
                            yAxis: {
                                show: true,
                                orient: "left",
                                isFormat: true,
                                ticks: 3,
                                maxTickCount: 4,
                                width: 42,
                                position: "outer",
                                tick: {
                                    line: {
                                        show: true,
                                        style: {
                                            'stroke': '#cccccc',
                                            'stroke-width': '1px'
                                        }
                                    }
                                }
                            },
                            showGridLines: true,
                            showGridBlocks: false,
                            showSolidGridLines: false,
                            showLegend: false,
                            showFundamentalCloseButton: false,
                            fundamentalProperties: {
                                PE: {
                                    color: '#008E91',
                                },
                                PS: {
                                    color: '#008E91',
                                },
                                PB: {
                                    color: '#008E91',
                                },
                                PC: {
                                    color: '#008E91',
                                },
                                SInterest: {
                                    color: '#008E91',
                                },
                                REPS: {
                                    color: '#008E91',
                                },
                                DiscountCumFair: {
                                    color: '#008E91',
                                },
                                PremiumDiscount: {
                                    color: '#FD5516',
                                }

                            },
                            show: true,
                            chartClass: "chart-fundamentalchart",
                            chartType: StaticConfig.FundamentalChartTypes.LineChart,
                        }
                    }
                },
                'labels': LanguageConfig,
                'components': {}
            }
        };

        return {
            AutocompleteCategories: StaticConfig.AutocompleteCategories,
            defaultConfig: defaultConfig,
            frequencyConfig: StaticConfig.frequencyConfig,
            frequencySetupConfig: StaticConfig.frequencySetupConfig,
            ChartTypes: StaticConfig.ChartTypes,
            ITFundGrowthTypes: StaticConfig.ITFundGrowthTypes,
            CursorType: StaticConfig.CursorType,
            PopoverTypes: StaticConfig.PopoverTypes,
            MainChartTypes: StaticConfig.MainChartTypes,
            MainDataTypes: StaticConfig.MainDataTypes,
            VolumeChartTypes: StaticConfig.VolumeChartTypes,
            FundamentalChartTypes: StaticConfig.FundamentalChartTypes,
            IndicatorChartTypes: StaticConfig.IndicatorChartTypes,
            IntervalTypesMapping: StaticConfig.IntervalTypesMapping,
            ExternalTypesMapping: StaticConfig.ExternalTypesMapping,
            IntervalTypes: StaticConfig.IntervalTypes,
            FrequencyTypes: StaticConfig.FrequencyTypes,
            FrequencyTexts: StaticConfig.FrequencyTexts,
            Constants: StaticConfig.Constants,
            TimezoneMap: StaticConfig.TimezoneMap,
            IntervalTypeSortedArray: StaticConfig.IntervalTypeSortedArray,
            SelectionModes: StaticConfig.SelectionModes,
            StartDateTypes: StaticConfig.StartDateTypes,
            VerticalScaleTypes: StaticConfig.VerticalScaleTypes,
            ChartGenre: StaticConfig.ChartGenre,
            indicatorInputTypes: StaticConfig.indicatorInputTypes,
            ChartTypeCategoryMap: StaticConfig.ChartTypeCategoryMap
        };
    });