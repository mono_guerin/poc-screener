﻿define([],
    function() {
        'use strict';

        var indicatorInputTypes = {
            NullOrPositiveInteger: '', //default
            NecessaryPositiveInteger: 'npi'
        }

        var ChartTypes = {
            MainChart: "mainChart",
            VolumeChart: "volumeChart",
            SliderChart: "sliderChart",
            FundamentalChart: "fundamentalChart",
            IndicatorChart: "indicatorChart",
        };


        var MainChartTypes = {
            LineChart: "lineChart",
            MountainChart: "mountainChart",
            DotChart: "dotChart",
            ABChart: "abChart",
            OHLCChart: "ohlcChart",
            HLCChart: "hlcChart",
            CandlestickChart: "candlestickChart",
            DividendEffectChart: "dividendEffectChart",
            pGrowthChart: "pGrowthChart",
            TenKGrowthChart: "tenKGrowthChart",
            PostTaxReturnChart: "postTaxReturnChart",
            PriceWithDividendGrowth: "priceWithDividendGrowth"
        };

        var MainDataTypes = {
            Price: "price",
            Nav: 'nav',
            DividendEffect: 'dividendEffect',
            Growth: 'growth',
            Growth10K: 'growth10K',
            PostTax: 'postTax',
            TotalReturn: 'totalReturn',
            MarketValue: 'marketValue',
            PriceAndNav: 'priceNav',
            PriceWithDividendGrowth: 'priceWithDividendGrowth'
        };

        var VolumeChartTypes = {
            VolumeChart: "volumeChart",
            VolumePlusChart: "volumePlusChart"
        };

        var FundamentalChartTypes = {
            LineChart: "lineChart"
        };

        var IndicatorChartTypes = {
            LineChart: "lineChart"
        };

        var IntervalTypes = {
            OneDay: "OneDay",
            FiveDay: "FiveDay",
            FifteenDay: "FifteenDay",
            OneMonth: "OneMonth",
            ThreeMonth: "ThreeMonth",
            SixMonth: "SixMonth",
            YTD: "YTD",
            OneYear: "OneYear",
            ThreeYear: "ThreeYear",
            FiveYear: "FiveYear",
            TenYear: "TenYear",
            FifteenYear: "FifteenYear",
            Max: "Max",
            Custom: "Oneday"
        };

        var IntervalTypesMapping = {
            "1D": "OneDay",
            "5D": "FiveDay",
            "15D": "FifteenDay",
            "1M": "OneMonth",
            "3M": "ThreeMonth",
            "6M": "SixMonth",
            "YTD": "YTD",
            "1Y": "OneYear",
            "3Y": "ThreeYear",
            "5Y": "FiveYear",
            "10Y": "TenYear",
            "MAX": "Max"
        };

        var ExternalTypesMapping = {
            "OneDay": "One Day",
            "FiveDay": "Five Days",
            "FifteenDay": "Fifteen Days",
            "OneMonth": "One Month",
            "ThreeMonth": "Three Months",
            "SixMonth": "Six Months",
            "YTD": "YTD",
            "OneYear": "One Year",
            "ThreeYear": "Three Years",
            "FiveYear": "Five Years",
            "TenYear": "Ten Years",
            "Max": "Max"
        };

        var IntervalTypeSortedArray = [
            "OneDay",
            "FiveDay",
            "FifteenDay",
            "OneMonth",
            "ThreeMonth",
            "SixMonth",
            "YTD",
            "OneYear",
            "ThreeYear",
            "FiveYear",
            "TenYear",
            "FifteenYear",
            "Max"
        ];

        var FrequencyTypes = {
            OneMinute: 1,
            FiveMinute: 5,
            TenMinute: 10,
            FifteenMinute: 15,
            ThirtyMinute: 30,
            SixtyMinute: 60,
            Daily: "d",
            Weekly: "w",
            Monthly: "m"
        };

        var CursorType = {
            Crosshair: 'crosshair',
            Trackball: 'trackball',
            Off: 'off'
        };

        var Constants = {
            MINUTE_IN_MILLISECOND: 60 * 1e3,
            DAY_IN_MINUTE: 60 * 24,
            DAY_IN_SECONDS: 60 * 24 * 60,
            DAY_IN_MILLISECONDS: 60 * 24 * 60 * 1e3
        };
        // available categories: ['Equities', 'ETF', 'Closed-End Fund', 'Mutual Fund', 'Index', 'Forex', 'Futures',
        //'Options', 'Debenture', 'Government Bonds', 'Strategies & Spreads', 'Unspecified Bonds',
        //'Unspecified Funds & Certificates', 'Warrents']
        var AutocompleteCategories = {
            "stock": ['Equities', 'ETF', 'Index', 'Closed-End Fund'],
            "forex": ['Forex'],
            "fund": ['Mutual Fund', 'Index']
        };

        //available category by chartType
        var ChartTypeCategoryMap = {
            "StockChart": "stock",
            "ForexChart": "forex",
            "FundChart": "fund"
        };
        var frequencySetupConfig = {
            f1: [
                { "value": "1", "text": "oneMinute" },
                { "value": "5", "text": "fiveMinute" },
                { "value": "10", "text": "tenMinute" },
                { "value": "15", "text": "fifteenMinute" },
                { "value": "30", "text": "thirtyMinute" }
            ],
            f2: [
                { "value": "5", "text": "fiveMinute" },
                { "value": "10", "text": "tenMinute" },
                { "value": "15", "text": "fifteenMinute" },
                { "value": "30", "text": "thirtyMinute" }
            ],
            f3: [
                { "value": "10", "text": "tenMinute" },
                { "value": "15", "text": "fifteenMinute" },
                { "value": "30", "text": "thirtyMinute" },
                { "value": "d", "text": "daily" },
                { "value": "w", "text": "weekly" }
            ],
            f4: [
                { "value": "d", "text": "daily" },
                { "value": "w", "text": "weekly" },
                { "value": "m", "text": "monthly" }
            ],
            f5: [
                { "value": "m", "text": "monthly" }
            ],
            f6: [
                { "value": "1", "text": "oneMinute" },
                { "value": "5", "text": "fiveMinute" },
                { "value": "10", "text": "tenMinute" },
                { "value": "15", "text": "fifteenMinute" },
                { "value": "30", "text": "thirtyMinute" },
                { "value": "d", "text": "daily" },
                { "value": "w", "text": "weekly" },
                { "value": "m", "text": "monthly" }
            ],
            f7: [
                { "value": "5", "text": "fiveMinute" },
                { "value": "10", "text": "tenMinute" },
                { "value": "15", "text": "fifteenMinute" },
                { "value": "30", "text": "thirtyMinute" },
                { "value": "60", "text": "sixtyMinute" }
            ],
            f8: [
                { "value": "10", "text": "tenMinute" },
                { "value": "15", "text": "fifteenMinute" },
                { "value": "30", "text": "thirtyMinute" },
                { "value": "60", "text": "sixtyMinute" },
                { "value": "d", "text": "daily" },
                { "value": "w", "text": "weekly" }
            ]
        };

        var PopoverTypes = {
            OHLCV: "OHLCV",
            Growth: "Growth",
            TenKGrowth: "TenKGrowth",
            PostTax: "PostTax",
            Simple: 'Simple'
        };

        var FrequencyTexts = {
            "1": "oneMinute",
            "5": "fiveMinute",
            "10": "tenMinute",
            "15": "fifteenMinute",
            "30": "thirtyMinute",
            "60": "sixtyMinute",
            "d": "daily",
            "w": "weekly",
            "m": "monthly"
        };

        var frequencyConfig = {
            'OneDay': { fc: frequencySetupConfig.f1, def: '1' },
            'FiveDay': { fc: frequencySetupConfig.f2, def: '5' },
            'FifteenDay': { fc: frequencySetupConfig.f7, def: '10' },
            'OneMonth': { fc: frequencySetupConfig.f8, def: 'd' },
            'ThreeMonth': { fc: frequencySetupConfig.f8, def: 'd' },
            'SixMonth': { fc: frequencySetupConfig.f4, def: 'd' },
            'YTD': { fc: frequencySetupConfig.f4, def: 'd' },
            'OneYear': { fc: frequencySetupConfig.f4, def: 'd' },
            'ThreeYear': { fc: frequencySetupConfig.f4, def: 'd' },
            'FiveYear': { fc: frequencySetupConfig.f4, def: 'd' },
            'TenYear': { fc: frequencySetupConfig.f4, def: 'd' },
            'Max': { fc: frequencySetupConfig.f4, def: 'm' }
        };

        var LanguageTypes = {
            EN_US: 'en-US',
        };

        var TimezoneMap = {
            "ADT": -180,
            "AKDT": -480,
            "AKST": -540,
            "AST": -240,
            "CDT": -300,
            "CST": -360,
            "EDT": -240,
            "EST": -300,
            "EGT": -60,
            "EGST": 0,
            "HADT": -540,
            "HAST": -600,
            "MDT": -360,
            "MST": -420,
            "NDT": -150,
            "NST": -210,
            "PDT": -420,
            "PST": -480,
            "WGT": -180,
            "WGST": -120,
            "CET": 60
        };

        var SelectionModes = {
            Pan: "pan",
            ZoomIn: "zoomIn",
            Off: "off"
        };

        var VerticalScaleTypes = {
            Linear: "linear",
            Logarithmic: "logarithmic"
        };

        var ChartGenre = {
            StockChart: "StockChart",
            ForexChart: "ForexChart",
            FundChart: "FundChart",
            CommodityChart: "CommodityChart"
        };

        var Securities = {
            'ST': 'Equities',
            "ST01": "Common Stocks",
            "ST02": "Preferred Stocks",
            "STADR": "ADRs",
            'XI': 'Index',
            'FE': 'ETF',
            'FC': 'Closed-End Fund',
            'FO': 'Mutual Fund',
            'C0': 'Cash',
            'CA': 'Category',
            'SA': 'Life & Pension',
            'OTHER': 'Others',
            'Bonds': 'Bonds',
            'FG': 'Euro Funds',
            'FH': 'Hedge Funds',
            'CB': 'Custom Benchmark',
            'MI': 'Miscellaneous',
            '1': 'Equities',
            '2': 'Options',
            '3': 'Futures',
            '7': 'Debenture',
            '8': 'Mutual Fund',
            '9': 'Government Bonds',
            '10': 'Index',
            '13': 'Strategies & Spreads',
            '16': 'Unspecified Bonds',
            '17': 'Unspecified Funds & Certificates',
            '18': 'Warrants',
            '20': 'Forex'
        };
        var StartDateTypes = {
            Common: "common",
            MainTicker: "main",
            Earliest: "earliest"
        }

        return {
            ChartTypes: ChartTypes,
            MainChartTypes: MainChartTypes,
            MainDataTypes: MainDataTypes,
            VolumeChartTypes: VolumeChartTypes,
            FundamentalChartTypes: FundamentalChartTypes,
            IndicatorChartTypes: IndicatorChartTypes,
            IntervalTypes: IntervalTypes,
            IntervalTypesMapping: IntervalTypesMapping,
            ExternalTypesMapping: ExternalTypesMapping,
            IntervalTypeSortedArray: IntervalTypeSortedArray,
            FrequencyTypes: FrequencyTypes,
            FrequencyTexts: FrequencyTexts,
            CursorType: CursorType,
            Constants: Constants,
            AutocompleteCategories: AutocompleteCategories,
            frequencySetupConfig: frequencySetupConfig,
            PopoverTypes: PopoverTypes,
            frequencyConfig: frequencyConfig,
            LanguageTypes: LanguageTypes,
            TimezoneMap: TimezoneMap,
            SelectionModes: SelectionModes,
            VerticalScaleTypes: VerticalScaleTypes,
            ChartGenre: ChartGenre,
            indicatorInputTypes: indicatorInputTypes,
            ChartTypeCategoryMap: ChartTypeCategoryMap,
            Securities: Securities,
            StartDateTypes: StartDateTypes
        };
    });