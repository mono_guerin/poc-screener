﻿define([],
    function() {
        'use strict';

        var GraphTypes = {
            Line: "line",
            Area: "area",
            Dot: "dot",
            AboveBelow: "aboveBelow",
            OHLC: "ohlc",
            HLC: "hlc",
            Candlestick: "candlestick",
            DividendEffect: "dividendEffect",
            Growth: "growth",
            TenKGrowth: "tenKGrowth",
            GridLines: "gridLines",
            GridBlocks: "gridBlocks",
            ZoomDrag: "zoomDrag",
            Histogram: "histogram",
            Brush: "brush",
            Axis: "axis",
            Event: "event",
            VBP: "volumeByPrice",
            Tag: "tag",
            LineLegend: "lineLegend"
        };

        return GraphTypes;
    });