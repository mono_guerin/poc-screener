define(["../../util/errorHandler", "../../view/ui/crosshairs", '../../view/ui/MenuBar',
        "../../view/ui/MenuBarInterval.js", "../../core/core", "../../util/util", "../../view/chart/mainChart", "../../view/chart/volumeChart",
        "../../view/chart/sliderChart", "../../view/chart/indicatorChart", "../../view/chart/fundamentalChart", "../../view/ui/autoComplete", "../../view/ui/popover", "morningstar"
    ],
    function(errorHandler, crosshairsModel, MenuBarModel, MenuBarInterval, core, util, mainChartModel, volumeChartModel, sliderChartModel, indicatorChartModel, fundamentalChartModel, AutoCompleteModel, PopoverModel, morningstar) {
        "use strict";

        var $ = core.$;


        /**
         * Layout manage Model
         *
         */
        function layoutManager(managers) {
            var CCM = managers.chartConfigManager;
            var eventManager = managers.eventManager;
            var MenuBar = MenuBarModel(managers);
            var menuBar = null;
            var crosshairs = crosshairsModel(managers);
            var ChartNameConstructorMapping = {
                mainChart: mainChartModel(managers),
                volumeChart: volumeChartModel(managers),
                sliderChart: sliderChartModel(managers),
                fundamentalChart: fundamentalChartModel(managers),
                indicatorChart: indicatorChartModel(managers)
            };

            var config;
            var viewMappings = {};
            var $container, $scrollEl, $innerContainer, $el, $menubarContainer, $subChartContainer;
            var $indicatorFoundmentalContainer;
            var $loadingElem;
            var callLoadingCount = 0;
            var componentName = "markets-components-svgchart";
            var heightChangeTimer, heightChangeSender = [];
            var autoHeightPoint = 600,
                autoRatio = 2.5;
            var lastCallbackHeight, viewLoaded = false;
            var cursorTypeChanged, frequencyChanged;
            var containerIsBody;
            var mainHeightBeforeAuto;
            var scrollBarInfo;
            var scrollbarStatus;
            var hideLoadingTimer;
            var breakpointArr, breakpointLength, breakpointStatus, printSizeConfig = {},
                printStyleClass;
            /**
             * Render the whole chart view
             *
             * @param {config} the configuration of the chart view layout
             */
            function createView() {
                breakpointStatus = Infinity;
                cursorTypeChanged = false;
                frequencyChanged = false;
                $loadingElem = null;
                var asterixUtil = morningstar.asterix.util;
                var instName = '';
                config = CCM.getConfig();

                var breakpointConfig = core.getBreakpointConfig(config.theme);
                if (config.customBreakPoint) {
                    breakpointConfig.breakPoint = config.customBreakPoint;
                }
                breakpointArr = (breakpointConfig.breakPoint || []).sort(function(a, b) {
                    return a - b;
                });

                breakpointLength = breakpointArr.length;
                if (scrollbarStatus) {
                    config.width += (scrollBarInfo = getScrollBarInfo(scrollBarInfo)).width;
                }

                scrollbarStatus = false;
                $container = $(config.container);
                $innerContainer = $('<div class="mkts-cmpt-svgcht-chart-wrap"></div>').appendTo($container);
                $scrollEl = $(config.template).addClass(config.securityTypeClass).appendTo($innerContainer);
                $el = $scrollEl.find(".mkts-cmpt-svgcht-chart");
                config.$componentContainer = $el;
                config.$container = $container;
                config.$innerContainer = $innerContainer;
                containerIsBody = $container.is(document.body);
                if (containerIsBody) {
                    if (config.fixHeight) {
                        $container.css('overflow', 'hidden');
                    } else {
                        $container.css('overflowX', 'hidden');
                    }
                }

                $subChartContainer = $el.find(".chart-subcharts");
                $indicatorFoundmentalContainer = $el.find(".chart-indicator-fundamental");
                bindEvents();

                if (config.MenubarModel && typeof config.MenubarModel === 'function') {
                    MenuBar = config.MenubarModel(managers, core, util, AutoCompleteModel, PopoverModel);
                }
                /*render menubar firstly, then we can get the height*/
                renderMenuBarView();

                /*calculate size*/
                var width, height;
                if (config.width > 0) {
                    width = config.width;
                } else {
                    width = getContainerWidth();
                }
                if (config.height > 0) {
                    height = config.height;
                } else {
                    height = getContainerHeight();
                }

                width = caculateContentWidth(width, false);
                height = caculateContentHeight(height, false);
                addOrRemoveBreakPointStyle(width);
                var newConfig = {
                    width: width,
                    height: height,
                    paddingTopBottom: padTopBottom(),
                    paddingLeftRight: padLeftRight()
                };

                CCM.setConfig(newConfig, newConfig.width !== config.width || newConfig.height !== config.height);

                widthRelatedProcess();
                renderSubCharts();
                config.charts.mainChart.height = caculateMainChartHeight(newConfig.width, newConfig.height - newConfig.paddingTopBottom).height;
                renderMainCharts();
                var style = {};
                if (config.fixHeight) {
                    style.overflowY = "auto";
                    style.height = newConfig.height;
                } else {
                    style.overflow = "hidden";
                }

                //if (containerIsBody) {
                //    style.width = caculateContentWidth(width);
                //}

                /*var marginConfig = config.margin;
                style.margin = marginConfig.top + "px " + marginConfig.right + "px " +
                    marginConfig.bottom + "px " + marginConfig.left + "px";*/
                $scrollEl.css(style);

                renderCrossHairs();

                $scrollEl.show();
                errorHandler(managers);
                eventManager.trigger("layout-rendered");
                addPrintListener();
            }

            function addPrintListener() {
                // support chrome for listen print function
                var currentWidth = 0,
                    _FixHeight;
                window.matchMedia("print").addListener(function(mql) {
                    if (mql.matches) {
                        caculatePrintWidth(_FixHeight);
                    }
                });
                //support IE & firefox for listen print function
                window.addEventListener("beforeprint", function(e) {
                    var userArge = navigator.userAgent;
                    currentWidth = getContainerWidth();
                    if (/Firefox/ig.test(userArge) || /(msie\s|Trident.*rv:)([\w.]+)/g.test(userArge)) {
                        caculatePrintWidth(_FixHeight);
                    }
                });
                window.addEventListener("afterprint", function(e) {
                    config.fixHeight = _FixHeight;
                    changeSize(currentWidth);
                    config.$componentContainer.find(".chart-body").removeClass(printStyleClass);
                });
            }

            // caculate print screen width and resize svgchart container
            function caculatePrintWidth(_FixHeight) {
                config.$componentContainer.find(".chart-body").removeClass(printStyleClass);
                config.fixHeight = [false, _FixHeight = config.fixHeight][0];
                var styles = config.$container.attr("style"),
                    hasWidthStyle = false;
                if (styles && styles.indexOf("width") > -1) {
                    hasWidthStyle = true;
                }
                var prefix = "mkts-cmpt-svgcht-less",
                    suffix = "printStyle",
                    sizeArry = [1155, 834, 634, 600],
                    resizewidth,
                    screenWidth = window.screen.width;

                if (!hasWidthStyle) {
                    var _breakpointArr = [].concat(breakpointArr).sort(function(a, b) { return b - a; });
                    for (var printPoint = 0; printPoint < _breakpointArr.length; printPoint++) {
                        if (printSizeConfig.width <= _breakpointArr[printPoint]) {
                            resizewidth = sizeArry[printPoint + 1];
                            printStyleClass = prefix + _breakpointArr[printPoint] + suffix;
                        } else if (printSizeConfig.width > 1000) {
                            resizewidth = sizeArry[0];
                            printStyleClass = screenWidth === printSizeConfig.width && containerIsBody ? "mkts-cmpt-svgcht-nobody-style" : "mkts-cmpt-svgcht-more1000printStyle";

                        }
                    }
                    if (screenWidth !== printSizeConfig.width || !containerIsBody) {
                        changeSize(resizewidth);
                    }
                    config.$componentContainer.find(".chart-body").addClass(printStyleClass);
                } else {
                    //svgchart container has width style
                    resizewidth = getContainerWidth();
                    changeSize(resizewidth);
                }

            }

            function getScrollBarInfo(scrollBarInfo) {
                if (scrollBarInfo) return scrollBarInfo;
                var scrollBarHelper = document.createElement("div");
                scrollBarHelper.style.cssText = "overflow:scroll;width:100px;height:100px;";
                document.body.appendChild(scrollBarHelper);
                var __scrollBarInfo = {};
                if (scrollBarHelper) {
                    __scrollBarInfo = {
                        height: scrollBarHelper.offsetHeight - scrollBarHelper.clientHeight,
                        width: scrollBarHelper.offsetWidth - scrollBarHelper.clientWidth
                    };
                }

                document.body.removeChild(scrollBarHelper);
                return __scrollBarInfo;
            }

            function checkIsSupportDateTypeElment() {
                var div = document.createElement("input");
                div.setAttribute("type", "date");
                if (div.type == "text") {
                    return false
                }
                return true;
            }

            function caculateContentWidth(width, needMinusPadding) {
                needMinusPadding = needMinusPadding === false ? false : true;
                var _width = width - (needMinusPadding ? padLeftRight() : 0);
                _width = Math.max(config.minWidth, _width);
                _width = Math.min(config.maxWidth, _width);
                return _width;
            }

            function caculateContentHeight(height, needMinusPadding) {
                needMinusPadding = needMinusPadding === false ? false : true;
                return height - (needMinusPadding ? padTopBottom() : 0);
            }

            function padTopBottom() {
                return parseInt($scrollEl.css('padding-top'), 10) + parseInt($scrollEl.css('padding-bottom'), 10);
            }

            function padLeftRight() {
                return parseInt($scrollEl.css('padding-left'), 10) + parseInt($scrollEl.css('padding-right'), 10);
            }

            function caculateMainChartHeight(width, height) {
                var needScrollBar = false,
                    menuHeight = $menubarContainer.is(':hidden') ? 0 : $menubarContainer.outerHeight(true),
                    volumeSliderHeight = $subChartContainer.outerHeight(true),
                    volumeSliderWidtht = $subChartContainer.outerWidth(true),
                    indicatorFoundmentalHeight = $indicatorFoundmentalContainer.outerHeight(true),
                    mainHeight = height - menuHeight - volumeSliderHeight,
                    minHeight = config.charts.mainChart.minHeight;
                if (config.fixHeight) {
                    if (indicatorFoundmentalHeight > 0) {
                        needScrollBar = true;
                    }
                } else {
                    mainHeight -= indicatorFoundmentalHeight;
                }

                if (mainHeight < minHeight) {
                    needScrollBar = true;
                    mainHeight = minHeight;
                }

                return {
                    height: mainHeight,
                    needScrollBar: needScrollBar
                };
            }

            function bindEvents() {
                var eventTypes = eventManager.EventTypes;
                eventManager.bind(eventTypes.StartLoading, startLoading);
                eventManager.bind(eventTypes.EndLoading, endLoading);
                eventManager.bind(eventTypes.HeightChanged, heightChanged);
                eventManager.bind(eventTypes.CallBackHeightChanged, callBackHeightChanged);
                eventManager.bind(eventTypes.DestroyChart, processDestroyChart);
                eventManager.bind(eventTypes.OnMainTickerDrawEnd, onMainTickerDrawEnd);
            }

            function heightChanged() {
                if (config.fixHeight) {
                    changeSize();
                    if (!viewLoaded) {
                        callBackHeightChanged();
                    }
                } else {
                    callBackHeightChanged();
                }
            }


            function processDestroyChart() {
                $container.removeData('morningstar-markets-container-event-data');
                $innerContainer.hide().remove();
                viewMappings = {};
                $el = null;
            }

            function createLoadingElem() {
                $loadingElem = $('<div class="mkts-cmpt-svgcht-loading-overlay"></div>')
                    .append($('<div class="mkts-cmpt-svgcht-loading-spinner"></div>'))
                    .appendTo($el);
            }

            function startLoading() {
                if (!$loadingElem) {
                    createLoadingElem();
                }
                callLoadingCount++;
                //console.log("startLoading callLoadingCount:" + callLoadingCount);
                //console.trace();
                $loadingElem.addClass("showing").show();
                clearTimeout(hideLoadingTimer);
                hideLoadingTimer = setTimeout(hideLoading, 10000);
            }

            function endLoading() {
                callLoadingCount--;
                //console.log("endLoading callLoadingCount:" + callLoadingCount);
                //console.trace();
                if (callLoadingCount <= 0) {
                    hideLoading();
                }
            }

            function hideLoading() {
                callLoadingCount = 0;
                $loadingElem && $loadingElem.hide().removeClass("showing");
                clearTimeout(hideLoadingTimer);
            }

            function changeSize(width, height) {
                // before init config, may be change browser size.
                if (config) {
                    width = width || (config.width + (scrollbarStatus ? (scrollBarInfo = getScrollBarInfo(scrollBarInfo)).width : 0)) || getContainerWidth();

                    var newConfig = {
                        width: width
                    };

                    width = caculateContentWidth(width, false);
                    var style = {};
                    var mainHeightChanged = false;
                    if (!config.fixHeight) {
                        height = null;
                    } else {
                        height = height || config.height || getContainerHeight();
                        newConfig.height = height;
                        height = caculateContentHeight(height);
                        style.height = newConfig.height;

                        var chartInfo = caculateMainChartHeight(width, height);
                        if (mainHeightChanged = (config.charts.mainChart.height != chartInfo.height)) {
                            config.charts.mainChart.height = chartInfo.height;
                        }

                        scrollbarStatus = chartInfo.needScrollBar;
                        if (scrollbarStatus) {
                            scrollBarInfo = getScrollBarInfo(scrollBarInfo);
                            width -= scrollBarInfo.width || 0;
                        }
                    }

                    $scrollEl.css(style);
                    widthRelatedProcess(width);
                    var widthChanged = (width !== config.width);
                    newConfig.width = width;
                    if ((newConfig.height && config.height !== newConfig.height) || widthChanged) {
                        CCM.setConfig(newConfig); //just update config, not update view.
                    }

                    addOrRemoveBreakPointStyle(width);

                    newConfig.paddingLeftRight = padLeftRight();
                    newConfig.paddingTopBottom = padTopBottom();
                    var paddingChanged = (newConfig.paddingLeftRight !== config.paddingLeftRight) || (newConfig.paddingTopBottom !== config.paddingTopBottom);
                    if (paddingChanged) {
                        CCM.setConfig(newConfig);
                    }
                    $.extend(true, printSizeConfig, newConfig);
                    if (widthChanged || paddingChanged) {
                        eventManager.trigger(eventManager.EventTypes.LayoutChange);
                    } else if (mainHeightChanged) {
                        eventManager.trigger(eventManager.EventTypes.ChangeMainChartLayout);
                    }
                }
            }

            function addOrRemoveBreakPointStyle(width) {
                var prefix = "mkts-cmpt-svgcht-less";
                var suffix = "style";
                if (CCM.getConfig().customBreakPoint) {
                    suffix += "-custom";
                }
                var lastStatus = breakpointStatus;
                for (var i = breakpointLength; i > 0; i--) {
                    if (width > breakpointArr[i - 1]) {
                        break;
                    }
                };
                breakpointStatus = i;
                if (lastStatus > breakpointStatus) {
                    for (var k = breakpointStatus; k < lastStatus && k < breakpointLength; k++) {
                        $scrollEl.addClass(prefix + breakpointArr[k] + suffix);
                    }
                } else if (lastStatus < breakpointStatus) {
                    for (var k = lastStatus; k < breakpointStatus && k < breakpointLength; k++) {
                        $scrollEl.removeClass(prefix + breakpointArr[k] + suffix);
                    }
                }
                eventManager.trigger(eventManager.EventTypes.DeployIntervalBreakPoint, width);
            }

            function widthRelatedProcess() {
                var crossType;
                if (!cursorTypeChanged && config.hideCross && (config.cursorType === CCM.CursorType.Crosshair) && (getContainerWidth() < 800)) {
                    crossType = CCM.CursorType.Trackball;
                    cursorTypeChanged = true;

                } else if (cursorTypeChanged && (getContainerWidth() > 800)) {
                    cursorTypeChanged = false;
                    crossType = CCM.CursorType.Crosshair;
                }

                if (crossType) {
                    eventManager.trigger(eventManager.EventTypes.ChangeChartCursor, crossType);
                    eventManager.trigger(eventManager.EventTypes.ToggleCrossharis);
                }

                if (!frequencyChanged && getContainerWidth() < 1000) {
                    eventManager.trigger(eventManager.EventTypes.ChangeOneDayFrequency, true);
                    frequencyChanged = true;
                } else if (frequencyChanged && getContainerWidth() > 1000) {
                    eventManager.trigger(eventManager.EventTypes.ChangeOneDayFrequency, false);
                    frequencyChanged = false;
                }
            }

            function callBackHeightChanged() {
                clearTimeout(heightChangeTimer);

                //Why use setTimeout?
                //Because main chart, volume chart, indicator chart use same data and data manager have

                heightChangeTimer = setTimeout(function() {
                    var height = $scrollEl.outerHeight(true),
                        width = $scrollEl.outerWidth(true);
                    if (!viewLoaded) {
                        lastCallbackHeight = height;
                        if (typeof config.onLoad == 'function') {
                            viewLoaded = true;
                            config.onLoad({
                                component: componentName,
                                width: width,
                                height: height
                            });
                        }
                    } else if (lastCallbackHeight !== height) {
                        lastCallbackHeight = height;
                        if (typeof config.onHeightChange == 'function') {
                            config.onHeightChange({
                                height: height,
                                component: componentName
                            });
                        }
                    }
                    var eventTypes = eventManager.EventTypes;
                    eventManager.trigger(eventTypes.FetchPreloadRelatedAndIndexData, null, false);
                }, 10);
            }

            function createSelfContainer(height) {
                return $("<div/>").addClass("chart-container").css("height", height);
            }

            function getContainer(className) {
                var chartContainer = $el.find("." + className)[0];
                if (!chartContainer) {
                    chartContainer = $("<div>").addClass(className).appendTo($el);
                }
                return chartContainer;
            }

            function renderSubCharts() {
                var chartTypes = CCM.ChartTypes;
                var chartName, chartConfig, chartConstructor;
                for (var key in chartTypes) {
                    chartName = chartTypes[key];
                    if (chartName === chartTypes.MainChart) {
                        continue;
                    }
                    chartConfig = CCM.getChartConfig(chartName);
                    chartConstructor = ChartNameConstructorMapping[chartName];
                    //fund chart does not need init volume chart

                    if ((chartConfig.show || chartName === chartTypes.VolumeChart) && chartConstructor) {
                        if (chartName === chartTypes.VolumeChart) {
                            if (!chartConfig.needInit) continue;
                        }
                        chartConfig.container = getContainer(chartConfig.chartClass);
                        viewMappings[chartName] = new chartConstructor(chartConfig);
                    }
                }
            }

            function renderMainCharts() {
                var chartName, chartConfig, chartConstructor;
                chartName = CCM.ChartTypes.MainChart;
                chartConfig = CCM.getChartConfig(chartName);
                chartConstructor = ChartNameConstructorMapping[chartName];
                if (chartConfig.show && chartConstructor) {
                    chartConfig.container = getContainer(chartConfig.chartClass);
                    viewMappings[chartName] = new chartConstructor(chartConfig);
                }
            }

            function renderMenuBarView() {
                $menubarContainer = $el.find('.mkts-cmpt-svgcht-menubar');

                menuBar = new MenuBar({
                    container: $menubarContainer
                });

                if (config.initMenubarmui) {
                    $scrollEl.addClass('mui');
                }

                if (config.hideAllMenuBar) {
                    $menubarContainer.hide();
                } else {
                    if (config.hideMenu) {
                        $menubarContainer.find('.mkts-cmpt-svgcht-menubar-top, .mkts-cmpt-svgcht-hr').hide();
                    }

                    if (config.hideTab) {
                        $menubarContainer.find('.mkts-cmpt-svgcht-menubar-tab, .mkts-cmpt-svgcht-hr').hide();
                    }
                    if (config.hideMenuBottom) {
                        $menubarContainer.find('.mkts-cmpt-svgcht-menubar-bottom').hide();
                    }
                    if (config.hideSearch) {
                        $menubarContainer.find('.mkts-cmpt-svgcht-menubar-top').addClass('mkts-cmpt-svgcht-hide-search');
                    }
                    if (config.hideIndicator) {
                        $menubarContainer.find(".mkts-cmpt-svgcht-menubtn--events-indicators").hide();
                    }
                    if (config.hideFundamental) {
                        $menubarContainer.find(".mkts-cmpt-svgcht-menubtn--fundamental").hide();
                    }
                    if (config.hideDrawing) {
                        $menubarContainer.find(".mkts-cmpt-svgcht-menubtn--drawing").hide();
                    }
                    if (config.hideDisplay) {
                        $menubarContainer.find(".mkts-cmpt-svgcht-menubtn--display").hide();
                    }

                    if (config.hideDateRange) {
                        $menubarContainer.find('.mkts-cmpt-svgcht-menuicn--label,.mkts-cmpt-svgcht-date-range').hide();
                    }
                    if (config.hideCalendar) {
                        $menubarContainer.find('.mkts-cmpt-svgcht-calendar-pickers').hide();
                    }
                    if (config.hideFrequency) {
                        $menubarContainer.find('.mkts-cmpt-svgcht-menuicn--label,.mkts-cmpt-svgcht-frequency-picker').hide();
                    }
                }
            }

            function renderCrossHairs() {
                new crosshairs($innerContainer);
            }

            function getContainerWidth() {
                return containerIsBody ? document.documentElement.clientWidth : $container.width();
            }

            function getContainerHeight() {
                return containerIsBody ? document.documentElement.clientHeight : $container.height();
            }

            function hidePopover() {
                menuBar.hidePopover();
            }

            function onMainTickerDrawEnd(date) {
                if (config && typeof config.onMainTickerDrawEnd == "function") {
                    config.onMainTickerDrawEnd(date);
                }
            }
            return {
                createView: createView,
                changeSize: changeSize,
                hidePopover: hidePopover
            };
        }

        return layoutManager;
    });