﻿define(['../../core/core', "./eventTypes"], function(core, EventTypes) {
    'use strict';
    /**
     * Event Manage Model
     */
    var $ = core.$;

    function eventManager(managers) {
        var callbackList = {};

        // Bind one or more space separated events, to a `callback`
        // function. Passing `"all"` will bind the callback to all events fired.
        function bind(events, callback, context) {
            var event, node, list, next;
            if (!callback) return this;
            events = $.isArray(events) ? events : [events];
            for (var i = 0, len = events.length; i < len; i++) {
                event = events[i];
                if (!event) {
                    throw new Error("Event name is undefined!");
                }
                list = callbackList[event];
                list = list ? list : [];
                node = {};
                node.context = context;
                node.callback = callback;
                list.push(node);
                callbackList[event] = list
            }
            return this;
        };

        // Remove one or many callbacks. If `context` is null, removes all callbacks
        // with that function. If `callback` is null, removes all callbacks for the
        // event. If `events` is null, removes all bound callbacks for all events.
        function unbind(events, callback, context) {
            var event;
            if (!events) {
                callbackList = {};
            } else {
                if (callbackList) {
                    events = $.isArray(events) ? events : [events];
                    for (var i = 0, len = events.length; i < len; i++) {
                        event = events[i];
                        list = callbackList[event];
                        deleteCallbackNodeByCallback(list, callback);
                        deleteCallbackNodeByContext(list, context);
                    }
                }
            }
            return this;
        };

        function deleteCallbackNodeByCallback(callbackList, callback) {
            deleteArrayItemByAttr(callbackList, "callback", callback);
        };

        function deleteCallbackNodeByContext(callbackList, context) {
            deleteArrayItemByAttr(callbackList, "context", context);
        };

        function deleteArrayItemByAttr(array, attrName, attrValue) {
            for (var i = 0, len = array.length; i < len; i++) {
                if (array[i][attrName] === attrValue) {
                    array.splice(i, 1);
                    break;
                }
            }
        };
        // Trigger one more many events, firing all bound callbacks. Callbacks are
        // passed the same arguments as `trigger` is, apart from the event name.
        function trigger(events) {
            var event, node, tail, args, all, eventData;
            if (!callbackList) {
                return this;
            }
            events = $.isArray(events) ? events : [events];
            eventData = [].slice.call(arguments, 1);
            if (checkEventsIfContainAll(events)) { //Need run all the callbacks
                for (event in callbackList) {
                    runCallbacksByEvent(event, eventData);
                }
            } else {
                events.push(EventTypes.All); //Need run the callbacks that bind on "all"
                for (var i = 0, len = events.length; i < len; i++) {
                    var event = events[i];
                    runCallbacksByEvent(event, eventData);
                }
            }
            return this;
        };

        function checkEventsIfContainAll(events) {
            var hasEvent_all = false;
            for (var i = 0, len = events.length; i < len; i++) {
                if (events[i] === EventTypes.All) {
                    hasEvent_all = true;
                }
            }
            return hasEvent_all;
        };

        function runCallbacksByEvent(eventName, args) {
            if (!eventName) {
                throw new Error("Event name is undefined!");
            }
            var node, nodeList = callbackList[eventName] || [];
            for (var i = 0, len = nodeList.length; i < len; i++) {
                node = nodeList[i];
                node.callback.apply(node.context || this, args);
            }
        };

        function clearBinders() {
            callbackList = {};
        };

        return {
            EventTypes: EventTypes,
            bind: bind,
            unbind: unbind,
            trigger: trigger,
            clearBinders: clearBinders
        };
    }

    return eventManager;
});