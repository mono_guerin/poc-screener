﻿define([], function() {
    "use strict";
    /**
     * Event Support Types Model
     */
    var EventTypes = {
        All: "all", //will be triggered by any event.
        LayoutChange: 'layoutChange',
        //BeginDrawTrandLine: "beginDrawTrandLine",
        ChangeChartCursor: 'changeChartCursor',
        ChangeChartVolume: 'changeChartVolume',
        ChangeChartVolumePlus: 'changeChartVolumePlus',
        ChangeVolumeChart: 'ChangeVolumeChart',
        ChangeYAxis: 'changeYAxis',
        ChangeVerticalScale: 'changeVerticalScale',
        ChangeSelectionMode: 'changeSelectionMode',
        ChangeExtendedMarketHour: 'changeExtendedMarketHour',
        ChangeDateByDateRange: 'changeDateByDateRange',
        ChangeEvents: 'changeEvents',
        ChangeIndicators: 'changeIndicators',
        ChangeFundamental: 'changeFundamental',
        ChangeDrawing: 'cangeDrawing',
        ChangeIntervalFrequency: 'changeIntervalFrequency',
        ChangeTicker: 'changeTicker',
        ChangeFrequencyItems: 'changeFrequencyItems',
        ChangeFrequencyFromMenu: 'changeFrequencyFromMenu',
        CompareTicker: 'compareTicker',
        ChangeChartType: "changeChartType", //Use for change chart type event, like change to line chart.
        ChangeXAxisDomain: "changeXAxisDomain", //Use for change chart x axis's scale when operate on slider chart.
        ChangeDateRangeByZoom: "ChangeDateRangeByZoom",
        ChartDataUpdate: 'chartDataUpdate', //Use for inform chart that data have updated.
        UpdateMenuInterval: 'updateMenuInterval',
        RequestFrequencyFromMenu: 'requestFrequencyFromMenu',
        GetMainChartDateRange: 'getMainChartDateRange',
        ChangeSliderDateRange: 'changeSliderDateRange',
        StartLoading: 'startLoading',
        EndLoading: 'endLoading',
        ToggleCrossharis: 'toggleCrossharis', //when mouse is moving in the chart,
        ToggleTrackball: 'toggleTrackball',
        ToggleTrackballVolume: 'toggleTrackballVolume',
        ToggleTrackballIndicators: 'toggleTrackballIndicators',
        ToggleTrackballFundamentals: 'toggleTrackballFundamentals',
        FetchPreloadData: 'fetchPreloadData',
        FetchPreloadRelatedAndIndexData: 'fetchPreloadRelatedAndIndexData',
        UpdateConfig: 'UpdateConfig',
        Export: 'export',
        "Reset": 'Reset',
        HeightChanged: 'onHeightChange',
        FundamentalNotAvailable: "FundamentalNotAvailable",
        MouseOutChart: "MouseOutChart",
        UpdateMenuIntervalForRender: 'UpdateMenuIntervalForRender',
        DrawNewData: "DrawNewData",
        DestroyChart: "DestroyChart",
        ChangeIDFConfig: "ChangeIntervalDateRangeFrequency",
        ChangeOneDayFrequency: "ChangeOneDayFrequency",
        ChangeGrowthType: "ChangeGrowthType",
        LayoutRendered: "layout-rendered",
        HideDateRange: "hideDateRange",
        ErrorShown: 'error-shown',
        ErrorHidden: 'error-hidden',
        UpdateCompareTickers: 'UpdateCompareTickers',
        ShowTrackball: 'ShowTrackball',
        ChangeMainChartLayout: 'ChangeMainChartLayout',
        CallBackHeightChanged: 'CallBackHeightChanged',
        TickerLimitReached: "error:ticker-limit-reached",
        ChangeInterval: "ChangeInterval",
        OnMainTickerDrawEnd: "onMainTickerDrawEnd",
        ChangeGrowthBaseValue: "ChangeGrowthBaseValue",
        DeployIntervalBreakPoint: "deployIntervalBreakPoint",
        ExportData: 'ExportData',
        Print: 'Print',
        ResetZoomIn: 'ResetZoomIn',
        ChangeStartDate: 'ChangeStartDate',


        /***callback */
        onbeforeZoomIn: 'onbeforeZoomIn',
        onDateRangeChange: 'onDateRangeChange',
        triggerClickCallback: 'triggerClickCallback',
        onRemoveCompareTickers: 'onRemoveCompareTickers',
        onTickerRemoveBtnClicked: 'onTickerRemoveBtnClicked',
        onAddCompareTickers: 'onAddCompareTickers'
    };

    return EventTypes;
});