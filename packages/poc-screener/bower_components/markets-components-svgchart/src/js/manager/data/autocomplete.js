define(['../chartConfig/staticConfig.js', './autocomplete_us', './autocomplete_eu'], function(staticConfig, autocomplete_us, autocomplete_eu) {
    'use strict';

    var acDataManager = {
        //Fields: autocomplete_us.Fields,
        //Securities: staticConfig.Securities,
        dataDomain: function(value) {
            if (!arguments.length) {
                return autocomplete_us.dataDomain();
            }
            autocomplete_us.dataDomain(value);
            return this;
        },
        fetchData: function(params, callback) {
            if (params.autoComplete.dataSource.toUpperCase() === 'EU') {
                autocomplete_eu.fetchData(params, callback);
            } else {
                autocomplete_us.fetchData(params, callback);
            }
        },
        fetchRelatedTickersAndMajorIndexData: autocomplete_us.fetchRelatedTickersAndMajorIndexData
    };
    return acDataManager;
});