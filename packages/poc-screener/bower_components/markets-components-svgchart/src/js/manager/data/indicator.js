﻿define(function() {
    'use strict';

    function Indicator(name, parameter) {
        var name = name;
        var parameter = parameter;

        this.name = function(value) {
            if (!arguments.length) {
                return name;
            }
            name = value;
            return this;
        };

        this.parameter = function(value) {
            if (!arguments.length) {
                return parameter;
            }
            parameter = value;
            return this;
        };
    }

    return Indicator;
});