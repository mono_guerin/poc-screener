define(['../../core/core.js', './indicator', './request', './response', './autocomplete', './screener_eu'], function(core, indicator, request, response, autoCompleteDataManager, screener_eu) {
    'use strict';
    var $ = core.$;
    var dateUtil = core.Widget.Chart.Util;
    var dataPoints = core.Widget.Chart.DataPoints;

    var model = function(managers) {
        var chartDataManager = core.DataManager.Chart;
        var eventManager = managers.eventManager;
        var chartObj = {
            id: 'markets-components-svgchart-' + Date.now(),
            refreshChart: function() {
                eventManager.trigger(eventManager.EventTypes.ChartDataUpdate);
            }
        };

        var checkRequest = function(req) {
            if (req instanceof request) {
                return true;
            }
            return false;
        };

        var isIntraday = function(req) {
            return !isNaN(req.frequency());
        };

        var isPortfolio = function(tickerObj) {
            var mType = tickerObj.mType;
            return mType === 'PO';
        };

        var isCustomBenchmark = function(tickerObj) {
            return tickerObj.getField("benchmarkType") && tickerObj.getField("benchmarkType").length > 0;
        };
        var extractDataPoints = function(tickerObj, req) {
            var chartConfigManager = managers.chartConfigManager;
            var isMutualFund = chartConfigManager.isMutualFund;
            var type = req.priceType() || dataAdapter.PriceType.Default;
            var events = req.events() || [];
            var indicators = req.indicators() || [];
            var chartCfg = chartConfigManager.getConfig();
            var useEUTimeSeriesData = chartCfg.useEUTimeSeriesData && isNaN(req.frequency());

            if (isMutualFund(tickerObj) && req.priceType() === 'pricenav') {
                type = 'p_nav';
            } else if (req.priceType() === 'pricenav') {
                type = 'p_price';
            } else if ((req.priceType() === dataAdapter.PriceType.Nav || req.priceType() === dataAdapter.PriceType.Default || req.priceType() === dataAdapter.PriceType.PriceWithDividendGrowth) && useEUTimeSeriesData) {
                type = 'tsp';
            } else if (req.priceType() === dataAdapter.PriceType.Growth && useEUTimeSeriesData) {
                type = (chartCfg.growthType === "priceGrowth") ? "return_tsp" : 'tsc';
            } else if (req.priceType() === dataAdapter.PriceType.Growth10K && useEUTimeSeriesData) {
                type = (chartCfg.growthType === "performancesGrowth") ? "return_k_tsp" : 'k_tsc';
            } else if (chartCfg.useDataApiTimeseriesData) {
                if (req.priceType() === dataAdapter.PriceType.Growth10K) {
                    type = isMutualFund(tickerObj) ? 'return_k_fact_nav' : 'return_k_fact_price';
                } else if (req.priceType() === dataAdapter.PriceType.Growth) {
                    type = isMutualFund(tickerObj) ? 'return_fact_nav' : 'return_fact_price';
                } else {
                    type = isMutualFund(tickerObj) ? 'fact_nav' : 'fact_price';
                }
            }
            var dps = {
                'p': type
            };
            if (req.priceType() === dataAdapter.PriceType.PriceWithDividendGrowth) {
                if (useEUTimeSeriesData) {
                    if (isMutualFund(tickerObj)) {
                        dps.p = 'tsc';
                        if (req.frequency() === 'm') {
                            dps.growthPrice = 'tsp';
                        }
                    } else {
                        dps.p = 'tsp';
                    }
                } else {
                    if (isMutualFund(tickerObj)) {
                        dps.p = dataAdapter.PriceType.Growth;
                        dps.growthPrice = dataAdapter.PriceType.Nav;
                    } else {
                        dps.p = dataAdapter.PriceType.Default;
                    }
                }
            }

            req.dpType(type);
            if (!isMutualFund(tickerObj) && !isCustomBenchmark(tickerObj) && !isPortfolio(tickerObj)) {
                dps['v'] = 'volume';
            }

            var hasDividendEvent = false;
            for (var i = 0, l = events.length, event; i < l; i++) {
                event = events[i];
                if (event === dataAdapter.Events.Dividend) {
                    if (!dps['ds']) {
                        dps['ds'] = {};
                    }
                    if (chartConfigManager.getConfig().showTSDividend || chartConfigManager.isITFund()) {
                        dps['ds']['tsd'] = true;
                    } else {
                        dps['ds']['d'] = true;
                    }
                    hasDividendEvent = true;
                } else if (event === dataAdapter.Events.Split) {
                    if (!dps['ds']) {
                        dps['ds'] = {};
                    }
                    dps['ds']['s'] = true;
                } else if (event === dataAdapter.Events.Earnings) {
                    if (chartConfigManager.isSemiEarningsExch()) {
                        dps['e'] = 'se';
                    } else {
                        dps['e'] = 'e';
                    }
                }
            }

            var indicatorSize = indicators.length;
            var needDividendEvent = false;
            if (indicatorSize > 0) {
                dps['indicators'] = [];
                var pType = type;
                for (var j = 0, ind, name, para, pType; j < indicatorSize; j++) {
                    ind = indicators[j];
                    name = ind.name();
                    para = ind.parameter();
                    if (name === dataAdapter.IndicatorName.EMA || name === dataAdapter.IndicatorName.SMA) {
                        for (var k = 0, kl = para.length; k < kl; k++) {
                            dps['indicators'].push({
                                name: name,
                                p: [para[k]],
                                pType: pType,
                                growthBaseValue: req.growthBaseValue()
                            });
                        }
                    } else {
                        if (name === dataAdapter.IndicatorName.RDividend || name === dataAdapter.IndicatorName.DYield) {
                            needDividendEvent = true;
                        }
                        dps['indicators'].push({
                            name: name,
                            p: para,
                            pType: pType
                        });
                    }
                }
            }

            if (needDividendEvent && !hasDividendEvent) {
                if (!dps['ds']) {
                    dps['ds'] = {};
                }
                dps['ds']['d'] = true;
            }

            var fundamentals = req.fundamentals() || [];
            for (var f = 0, fl = fundamentals.length, fd; f < fl; f++) {
                fd = fundamentals[f];
                if (fd === dataAdapter.Fundamental.MReturnIndex) {
                    dps[fd] = 'return';
                } else {
                    dps[fd] = 'fundamental';
                }
            }
            return dps;
        };

        var generaterInfoList = function(req, dataPoints) {
            var isMutualFund = managers.chartConfigManager.isMutualFund;
            var tickerObjects = req.tickerObjects();
            var infoList = [];
            for (var i = 0, l = tickerObjects.length, tickerObj, info; i < l; i++) {
                tickerObj = tickerObjects[i];
                info = {
                    type: isMutualFund(tickerObj) || isCustomBenchmark(tickerObj) || isPortfolio(tickerObj) ? '' : 'StockOrForex',
                    chartType: req.chartType(),
                    tickerObject: tickerObj,
                    datapoints: dataPoints || extractDataPoints(tickerObj, req),
                    growthBaseValue: req.growthBaseValue()
                };
                if (req.priceType() === dataAdapter.PriceType.PriceWithDividendGrowth) {
                    info.growthType = dataAdapter.PriceType.PriceWithDividendGrowth;
                }
                if (isIntraday(req)) {
                    info.days = req.days();
                } else {
                    info.fields = req.fundamentals();
                }
                var chartCfg = managers.chartConfigManager.getConfig();
                if (chartCfg.useEUTimeSeriesData) {
                    info.apiSetting = chartCfg.EUTimeSeriesApiSetting;
                    info.solutionKey = chartCfg.solutionKey;
                }
                infoList.push(info);
            }
            return infoList;
        };

        var genCallback = function(callback) {
            return typeof callback === 'function' ? callback : function() {};
        };

        var handleResponse = function(callback, req, data, status) {
            if (typeof callback === 'function') {
                var res = new response();
                res.req(req).data(data);
                if (status) {
                    res.status(status);
                }
                callback(res);
            }
        };

        var dispatchRequest = function(req, callback) {};

        var findDataItem = function(data, secId) {
            if (!data || data.length === 0) {
                return null;
            }
            for (var i = 0; i < data.length; i++) {
                if (data[i].secId === secId) {
                    return data[i];
                }
            }
            return null;
        }

        var buildTickerObjects = function(data, tickers) {
            var ticker = function() {
                return {
                    valid: false,
                    getField: function(field) {
                        return !field ? this : this[field];
                    },
                    setData: function(data) {
                        for (var d in data) {
                            if (d === 'name') {
                                this.Name = data[d];
                            } else {
                                this[d] = data[d];
                            }
                        }
                    },
                    setField: function(field, value) {
                        if (field) {
                            this[field] = typeof(value) == "undefined" ? null : value;
                        }
                    }
                }
            };

            var tickerObjects = [];
            $.each(tickers, function(i, ot) {
                var t = new ticker();
                //ot FOUSA06CGQ]7]0]IXALL$$ALL|MSID|FundName@_Ticker
                if (ot.indexOf(']') > -1) {
                    t.tsfund = true;
                }
                t.securityToken = index(ot.split('|'), 0);
                t.secId = index(t.securityToken.split(']'), 0);
                t.idType = index(ot.split('|'), 1) || 'Morningstar';
                t.Name = index(ot.split('|'), 2);
                var item = findDataItem(data, t.secId);
                if (item) {
                    t.valid = true;
                    t.setData(item);
                }
                $.extend(true, securityCacheMap[t.secId], t);
                tickerObjects.push(securityCacheMap[t.secId]);
                var performanceTickers = [];
                if (t.categoryId) {
                    performanceTickers.push({
                        id: t.categoryId,
                        type: 'category',
                        name: t.categoryName
                    });
                }
                if (t.benchMarkId) {
                    performanceTickers.push({
                        id: t.benchMarkId,
                        type: 'benchMark',
                        name: t.benchMarkName
                    });
                }
                $.each(performanceTickers, function(i, pt) {
                    var pticker = new ticker();
                    pticker.secId = pticker.symbol = pticker.ticker = pt.id;
                    pticker.Name = pt.name;
                    pticker.securityToken = pticker.secId + ']';
                    pticker.idType = 'Morningstar';
                    pticker.mType = pt.type === 'category' ? 'CA' : 'CB';
                    pticker.currency = t.currency;
                    pticker.valid = true;
                    securityCacheMap[pticker.secId] = $.extend(true, securityCacheMap[pticker.secId], pticker);
                });
            });
            return tickerObjects.sort(function(a, b) {
                return a.order - b.order;
            });
        }

        function index(arr, idx) {
            if (arr.length >= idx) {
                return arr[idx];
            }
            return null;
        }

        var formatPrice = function(jsonObject, d) {
            var isMutualFund = managers.chartConfigManager.isMutualFund;
            var isIntraday = !isNaN(d.fre);
            var dp = 'Price';
            var priceData = d.price && d.price.data ? d.price.data : [];
            var growthPriceData = d.growthPrice && d.growthPrice.data;
            var baseInfo = null

            var originalPriceData = d.price && d.price.originalPrice ? d.price.originalPrice : [];
            var volumeData = d.volume && d.volume.data ? d.volume.data : [];
            jsonObject[dp] = {
                'lastTradeTime': d.price && d.price.lastTradeTime,
                'epos': d.price && d.price.epos ? (+d.price.epos) : 0,
                'spos': d.price && d.price.spos ? (+d.price.spos) : 0,
                'data': [],
                'currencyField': d.currencyField
            };
            var isCompareOrPercentChart = managers.chartConfigManager.isCompareOrPercentChart();
            for (var j = 0, jl = priceData.length, pd, vd, temp; j < jl; j++) {
                pd = priceData[j];
                vd = volumeData[j] || [];
                var prevPdValue = j > 0 ? priceData[j - 1][1] : priceData[0][1];
                if (isCompareOrPercentChart || (!isMutualFund(d.tickerObject) || managers.chartConfigManager.getConfig().drawNavAndPriceChart) && !isCustomBenchmark(d.tickerObject) && !isPortfolio(d.tickerObject)) {
                    temp = {
                        'index': pd[0],
                        'open': pd[4],
                        'high': pd[2],
                        'low': pd[3],
                        'close': pd[1],
                        'upFlag': pd[5],
                        'updatedId': pd[pd.length - 2],
                        'date': dateUtil.toDateFromIndexOrTick(pd[pd.length - 1], isIntraday ? 0 : 1),
                        'dateIndex': pd[pd.length - 1],
                        'valueChange': pd[1] - prevPdValue,
                        'valueChangePercent': (pd[1] - prevPdValue) / prevPdValue * 100,
                        'originalPrice': originalPriceData[j] && originalPriceData[j].value,
                    };
                    if (vd.length > 1) {
                        temp['volume'] = vd[1];
                    }
                    isIntraday && (temp['fillFlag'] = pd[6]);
                } else {
                    temp = {
                        'index': pd[0],
                        'value': pd[1],
                        'valueChange': pd[1] - prevPdValue,
                        'valueChangePercent': (pd[1] - prevPdValue) / prevPdValue * 100,
                        'date': dateUtil.toDateFromIndexOrTick(pd[pd.length - 1]),
                        'dateIndex': pd[pd.length - 1],
                        'originalPrice': originalPriceData[j] && originalPriceData[j].value
                    };
                }
                if (growthPriceData) {
                    if (!baseInfo) {
                        baseInfo = getBasePriceData(priceData, growthPriceData);
                    }
                    if (pd[pd.length - 1] >= baseInfo.index) {
                        temp['growthWithNav'] = calculateGrowthWithNav(baseInfo.basePrice, pd[1] - baseInfo.baseGrowth);
                        if (temp['growthWithNav']) {
                            jsonObject[dp].data.push(temp);
                        }
                    }
                } else {
                    jsonObject[dp].data.push(temp);
                }
            }
        };

        function getBasePriceData(growthData, priceData) {
            var basePrice = null;
            var growthIndex = growthData[0][growthData[0].length - 1];
            var baseIndex, baseGrowth;
            for (var j = 0; j < priceData.length; j++) {
                baseIndex = priceData[j][priceData[j].length - 1];
                if (baseIndex >= growthIndex) {
                    basePrice = priceData[j][1];
                    if (baseIndex === growthIndex) {
                        baseGrowth = growthData[0][1];
                    }
                    break;
                }
            }
            if (!baseGrowth) {
                for (var i = 0; i < growthData.length; i++) {
                    if (growthData[i][growthData[i].length - 1] >= baseIndex) {
                        baseGrowth = growthData[i][1];
                        break;
                    }
                }
            }
            return {
                basePrice: basePrice,
                index: baseIndex,
                baseGrowth: baseGrowth
            };
        }

        function calculateGrowthWithNav(base, growth) {
            return growth === null ? NaN : (base + base * growth / 100);
        }
        var formatDividendAndSplitData = function(jsonObject, d) {
            var data = d.dividendAndSplit || [];
            var l = data.length;
            if (l > 0) {
                jsonObject['Events'] = jsonObject['Events'] || {};
                var dp;
                for (var i = 0, l = data.length, temp; i < l; i++) {
                    temp = {
                        'date': dateUtil.toDateFromIndexOrTick(data[i].DateIndex),
                        'value': data[i].Desc,
                        'dateIndex': data[i].DateIndex
                    }
                    dp = data[i].Type === 1 ? dataAdapter.Events.Dividend : dataAdapter.Events.Split;
                    if (!jsonObject['Events'][dp]) {
                        jsonObject['Events'][dp] = {
                            'data': []
                        };
                    }
                    jsonObject['Events'][dp].data.push(temp);
                }
            }
        };

        var formatEarnings = function(jsonObject, d) {
            var data = d.earningsData && d.earningsData.data ? d.earningsData.data : [],
                dp = dataAdapter.Events.Earnings;
            var l = data.length;
            if (l > 0) {
                jsonObject['Events'] = jsonObject['Events'] || {};
                jsonObject['Events'][dp] = {
                    'spos': d.earningsData && d.earningsData.spos ? (+d.earningsData.spos) : 0
                };
                var cdata = [];
                for (var i = 0, ed; i < l; i++) {
                    ed = data[i];
                    var md = {
                        'date': dateUtil.toDateFromIndexOrTick(ed[ed.length - 1]),
                        'upFlag': ed[2],
                        'value': ed[1],
                        'frequency': ed[3],
                        'dateIndex': ed[ed.length - 1],
                    }
                    cdata.push(md);
                }
                jsonObject['Events'][dp]['data'] = cdata;
            }
        };

        var formatIndicators = function(jsonObject, d, req) {
            var isIntraday = !isNaN(d.fre);
            var data = d.indicators ? d.indicators : {};
            var indData;
            var IndicatorName = dataAdapter.IndicatorName;
            for (var indName in data) {
                jsonObject['Indicators'] = jsonObject['Indicators'] || {};
                indData = data[indName];
                switch (indName) {
                    case IndicatorName.SMA:
                    case IndicatorName.EMA:
                        formatSMAEMA(jsonObject['Indicators'], indName, indData, isIntraday);
                        break;
                    case IndicatorName.BBands:
                    case IndicatorName.MACD:
                    case IndicatorName.RSI:
                    case IndicatorName.SStochastic:
                    case IndicatorName.FStochastic:
                    case IndicatorName.DMI:
                    case IndicatorName.PChannel:
                    case IndicatorName.MAE:
                    case IndicatorName.Keltner:
                        formatOther(jsonObject['Indicators'], indName, indData, isIntraday, req);
                        break;
                    case IndicatorName.ROC:
                    case IndicatorName.PSAR:
                    case IndicatorName.WillR:
                    case IndicatorName.SMAV:
                    case IndicatorName.Momentum:
                    case IndicatorName.ULT:
                    case IndicatorName.MFI:
                    case IndicatorName.VAcc:
                    case IndicatorName.UDRatio:
                    case IndicatorName.OBV:
                    case IndicatorName.DYield:
                    case IndicatorName.FVolatility:
                    case IndicatorName.WMA:
                    case IndicatorName.AccDis:
                    case IndicatorName.Mass:
                    case IndicatorName.Volatility:
                    case IndicatorName.ForceIndex:
                    case IndicatorName.ATR:
                    case IndicatorName.RDividend:
                    case IndicatorName.PrevClose:
                        formatOther(jsonObject['Indicators'], indName, [indData], isIntraday, req);
                        break;
                    case IndicatorName.VBP:
                        formatVBP(jsonObject['Indicators'], indData.data);
                        break;
                }
            };
        };

        var formatSMAEMA = function(jsonObject, indName, indData, isIntraday) {
            indData = indData || {};
            var spos, data, cdata, i = 0;
            for (var key in indData) {
                spos = indData[key].spos;
                data = indData[key].data;
                if (!jsonObject[indName]) {
                    jsonObject[indName] = [];
                }
                cdata = [];
                for (var i = 0, l = data.length, d; i < l; i++) {
                    d = data[i];
                    cdata.push({
                        'index': d[0],
                        'date': dateUtil.toDateFromIndexOrTick(d[d.length - 1], isIntraday ? 0 : 1),
                        'value': d[1],
                        'dateIndex': d[d.length - 1]
                    });
                }
                jsonObject[indName].push({
                    'data': cdata,
                    'spos': spos ? (+spos) : 0,
                    'parameter': +key
                })
            }
        };

        var formatVBP = function(jsonObject, data) {
            var indName = "VBP";
            if (data.length == 3) {
                jsonObject[indName] = {
                    'data': [],
                    'range': data[1],
                    'totalVolume': data[2]
                }
                for (var i = 0, l = data[0].length, d; i < l; i++) {
                    d = data[0][i];
                    if (d.length < 5) continue;
                    jsonObject[indName]['data'].push({
                        'range': [d[0], d[1]],
                        'totalVolume': d[2],
                        'upVolume': d[3],
                        'downVolume': d[4]
                    });
                }
            }
        };

        var formatOther = function(jsonObject, indName, indData, isIntraday, req) {
            indData = indData || [];
            var l = indData.length;
            if (l > 0) {
                var para = indicatorPara(indName, req.indicators()) || [];
                var spos, data, cdata, ind;
                jsonObject[indName] = [];
                for (var i = 0; i < l; i++) {
                    spos = indData[i].spos;
                    data = indData[i].data;
                    cdata = [];
                    for (var j = 0, jl = data.length, d, spos; j < jl; j++) {
                        d = data[j];
                        cdata.push({
                            'index': d[0],
                            'date': dateUtil.toDateFromIndexOrTick(d[d.length - 1], isIntraday ? 0 : 1),
                            'value': +d[1],
                            'dateIndex': d[d.length - 1]
                        });
                    }
                    ind = {
                        'data': cdata,
                        'spos': spos ? (+spos) : 0
                    };
                    if (typeof para[i] !== 'undefined') {
                        ind['parameter'] = +para[i];
                    }
                    if (indData[i]['DailyDataPoint']) {
                        ind['DailyDataPoint'] = true;
                    }
                    jsonObject[indName].push(ind);
                }
            }

        };

        var indicatorPara = function(indName, indicators) {
            var para = [];
            for (var i = 0, l = indicators.length, ind; i < l; i++) {
                ind = indicators[i];
                if (ind.name() === indName) {
                    para = ind.parameter();
                    break;
                }
            }
            return para;
        };

        var formatFundamental = function(jsonObject, d) {
            for (var key in dataAdapter.Fundamental) {
                if (!d[key]) continue;
                var fundData = d[key].data || [];
                if (fundData.length > 0 || d[key]['DailyDataPoint']) {
                    jsonObject['Fundamentals'] = jsonObject['Fundamentals'] || {};
                    jsonObject['Fundamentals'][key] = {};
                    if (d[key]['DailyDataPoint']) {
                        jsonObject['Fundamentals'][key]['DailyDataPoint'] = true;
                        jsonObject['Fundamentals'][key]['data'] = [];
                    } else {
                        var cdata = [];
                        for (var i = 0, l = fundData.length, fd; i < l; i++) {
                            fd = fundData[i];
                            cdata.push({
                                'index': fd[0],
                                'date': dateUtil.toDateFromIndexOrTick(fd[fd.length - 1]),
                                'value': fd[1],
                                'dateIndex': fd[fd.length - 1]
                            });
                        }
                        jsonObject['Fundamentals'][key] = {
                            'spos': d[key].spos ? (+d[key].spos) : 0,
                            'data': cdata
                        };
                    }
                }
            }
        };

        var formatTSData = function(data, req, growthPrice) {
            data = data || [];
            var ret = [],
                jsonArray, jsonObject, d, freq, priceData, volumeData;
            for (var i = 0, l = data.length; i < l; i++) {
                d = data[i];
                jsonArray = [];
                jsonObject = {
                    'tickerObject': d.tickerObject
                };
                if (growthPrice && growthPrice[i]) {
                    d.growthPrice = growthPrice[i].growthPrice;
                }
                formatPrice(jsonObject, d);
                formatDividendAndSplitData(jsonObject, d);
                formatEarnings(jsonObject, d);
                formatIndicators(jsonObject, d, req);
                formatFundamental(jsonObject, d);
                ret.push(jsonObject);
            }
            return ret;
        };

        var matchKeyWord = function(obj, keyword) {
            var reg = new RegExp(keyword, "i");
            return obj.ticker.match(reg) || obj.name.match(reg) || obj.ISIN.match(reg);
        };

        var formatAutocompleteData = function(data, params) {
            var config = managers.chartConfigManager.getConfig();
            var category = params.autoComplete.category;
            if (!params.autoComplete.needFilter) {
                category = [];
                for (var cat in data) {
                    category.push(cat);
                }
            }
            var nonFilterCategoryMap = params.autoComplete.nonFilterCategoryMap;
            var isAutocompleteGroup = config.autoComplete.group;
            var orgainzedArray = [];
            var max = isAutocompleteGroup ? 8 : 18;
            if (!isAutocompleteGroup) {
                var allData = [];
                $.each(category, function(idx, cate) {
                    var categoryData = data[cate] || [];
                    if (categoryData.length > 0) {
                        allData = allData.concat(categoryData);
                    }
                });
                category = ['Securities'];
                data = {
                    'Securities': allData
                };
            }
            $.each(category, function(idx, cate) {
                var categoryData = data[cate] || [];
                if (categoryData.length > 0) {
                    var json = {
                        catName: cate,
                        results: []
                    };
                    for (var i = 0, l = Math.min(max, categoryData.length), d; i < l; i++) {
                        var d = $.extend(true, {}, categoryData[i]);
                        if (nonFilterCategoryMap.All !== true && nonFilterCategoryMap[cate] !== true && !matchKeyWord(d, params.queryKey)) continue;
                        json.results.push(d);
                    }
                    orgainzedArray.push(json);
                }
            });

            return orgainzedArray;
        };


        var securityCacheMap = {};
        var dataAdapter = {
            ready: false,
            init: function() {
                if (!this.ready) {
                    chartDataManager.dataDomainValue(core.getQSPath());
                    chartDataManager.volumeDividend(1);
                    autoCompleteDataManager.dataDomain(core.getQSPath());
                    this.ready = true;
                }
            },
            getSecurityObjects: function(secId) {
                return securityCacheMap[secId];
            },
            createTickerObjects: function(tickers, callback) {
                var rootCfg = managers.chartConfigManager.getConfig();
                var self = this;
                if (rootCfg.useEUSecurityData) {
                    //FOUSA06CGQ]7]0]IXALL$$ALL|MSID|FundName@_Ticker
                    var filters = [];
                    $.each(tickers, function(i, ticker) {
                        var secId = ticker.split(']')[0];
                        securityCacheMap[secId] = {};
                        securityCacheMap[secId].originalTicker = ticker;
                        securityCacheMap[secId].order = i;
                        filters.push(secId);
                    });
                    filters.unshift('secId', 'IN');
                    filters = filters.join(':');
                    screener_eu.fetchData({
                        solutionKey: rootCfg.solutionKey,
                        languageId: rootCfg.lang,
                        filters: filters
                    }, {
                        onSuccess: function(data) {
                            genCallback(callback)(buildTickerObjects(data.tickers, tickers));
                        }
                    });
                } else {
                    core.TickerFactory.create(tickers, {
                        onSuccess: function(tickerObjects) {
                            for (var i = 0; i < tickerObjects.length; i++) {
                                managers.chartConfigManager.isMutualFund(tickerObjects[i]);
                            }
                            genCallback(callback)(tickerObjects);
                        }
                    });
                }
            },
            registerChartTickers: function(tickerObjects) {
                this.init();
                chartDataManager.registerSymbol(chartObj, tickerObjects);
            },
            unregisterChartTickers: function(tickerObjects) {
                this.init();
                chartDataManager.unregisterSymbol(chartObj, tickerObjects);
            },
            extractDataPoints: function(tickerObj, req) {
                this.init();
                if (checkRequest(req)) {
                    extractDataPoints(tickerObj, req);
                }
            },
            fetchTSData: function(req, callback) {
                this.init();
                if (checkRequest(req)) {
                    var infoList = generaterInfoList(req);
                    var dateRange = req.dateRange();
                    var startIndex = dateUtil.toDateIndex(dateRange[0]);
                    var endIndex = dateUtil.toDateIndex(dateRange[1]);
                    if (isIntraday(req)) {
                        chartDataManager.getIntradayData(infoList, req.frequency(), startIndex, endIndex, function(data) {
                            handleResponse(callback, req, formatTSData(data, req));
                        }, req.isPreAfter(), chartObj.id);
                    } else {
                        if (managers.chartConfigManager.getConfig().useEUTimeSeriesData && req.priceType() === dataAdapter.PriceType.PriceWithDividendGrowth && req.frequency() !== 'm') {
                            chartDataManager.getHistoricalData(generaterInfoList(req, {
                                growthPrice: 'tsp'
                            }), 'd', Math.max(startIndex - 7, 0), endIndex, function(growthPriceData) {
                                chartDataManager.getHistoricalData(infoList, req.frequency(), startIndex, endIndex, function(data) {
                                    handleResponse(callback, req, formatTSData(data, req, growthPriceData));
                                }, req.keepDateRange(), req.alignMainTicker());
                            }, req.keepDateRange(), req.alignMainTicker());
                        } else {
                            chartDataManager.getHistoricalData(infoList, req.frequency(), startIndex, endIndex, function(data) {
                                handleResponse(callback, req, formatTSData(data, req));
                            }, req.keepDateRange(), req.alignMainTicker());
                        }
                    }
                } else {
                    handleResponse(callback, req, null, response.Status.InvalidRequest);
                }
            },
            fetchRelatedTickersAndMajorIndexData: function(req, callback) {
                this.init();
                if (checkRequest(req)) {
                    var tickerObjects = req.tickerObjects();
                    if (tickerObjects.length > 0) {
                        var mainTicker = tickerObjects[0];
                        var mainTickerType = mainTicker["mType"] || "";
                        var pID = mainTicker.performanceId || mainTicker.secId;
                        if (pID) {
                            autoCompleteDataManager.fetchRelatedTickersAndMajorIndexData(pID, mainTickerType, function(data, pID) {
                                handleResponse(callback, req, data);
                            });
                        } else {
                            handleResponse(callback, req, null, response.Status.NoData);
                        }
                    } else {
                        handleResponse(callback, req, null, response.Status.InvalidRequest);
                    }
                } else {
                    handleResponse(callback, req, null, response.Status.InvalidRequest);
                }
            },
            fetchAutocompleteData: function(req, callback) {
                this.init();
                if (checkRequest(req)) {
                    var config = managers.chartConfigManager.getConfig();
                    if (typeof config.autoComplete.needFilter === "undefined") {
                        config.autoComplete.needFilter = config.autoComplete.dataSource === "EU" ? false : true;
                    }
                    var params = {
                        solutionKey: config.solutionKey,
                        autoComplete: $.extend(true, {}, config.autoComplete, {
                            category: req.category(),
                            nonFilterCategory: req.nonFilterCategory()
                        }),
                        lang: config.lang,
                        queryKey: req.keyword()
                    };
                    autoCompleteDataManager.fetchData(params, function(data, params) {
                        data = formatAutocompleteData(data, params);
                        if (data.length > 0) {
                            handleResponse(callback, req, data);
                        } else {
                            handleResponse(callback, req, null, response.Status.NoData);
                        }
                    });
                } else {
                    handleResponse(callback, req, null, response.Status.InvalidRequest);
                }
            },

            IndicatorName: {
                SMA: 'SMA',
                EMA: 'EMA',
                BBands: 'BBands',
                MACD: 'MACD',
                RSI: 'RSI',
                ROC: 'ROC',
                SStochastic: 'SStochastic',
                FStochastic: 'FStochastic',
                PSAR: 'PSAR',
                WillR: 'WillR',
                SMAV: 'SMAV',
                DMI: 'DMI',
                PChannel: 'PChannel',
                MAE: 'MAE',
                Momentum: 'Momentum',
                ULT: 'ULT',
                MFI: 'MFI',
                VAcc: 'VAcc',
                UDRatio: 'UDRatio',
                OBV: 'OBV',
                VBP: 'VBP',
                DYield: 'DYield',
                FVolatility: 'FVolatility',
                WMA: 'WMA',
                AccDis: 'AccDis',
                Mass: 'Mass',
                Volatility: 'Volatility',
                ForceIndex: 'ForceIndex',
                ATR: 'ATR',
                Keltner: 'Keltner',
                RDividend: 'RDividend',
                PrevClose: 'PrevClose'
            },
            PriceType: {
                Default: 'price',
                Nav: 'nav',
                Growth: 'return',
                Growth10K: 'return_k',
                PostTax: 'postTax',
                TotalReturn: 'total_return',
                MarketValue: 'market_value',
                PriceAndNav: 'pricenav',
                PriceWithDividendGrowth: 'priceWithDividendGrowth'
            },
            Events: {
                Dividend: 'dividend',
                Split: 'split',
                Earnings: 'earnings'
            },
            Fundamental: {
                PE: 'PE',
                PS: 'PS',
                PB: 'PB',
                PC: 'PC',
                FairValue: 'FairValue',
                QuantitativeFairValue: 'QuantitativeFairValue',
                SInterest: 'SInterest',
                REPS: 'REPS',
                MReturnIndex: 'MReturnIndex', // it is used for draw dividend Effect chart
                DiscountCumFair: 'DiscountCumFair',
                DailyCumFairNav: 'DailyCumFairNav',
                PremiumDiscount: 'PremiumDiscount',
                FundSize: 'FundSize',
                ShareholdersNumber: 'ShareholdersNumber'
            },
            Request: request,
            Response: response,
            Indicator: indicator
        };

        return dataAdapter;
    }

    return model;
});