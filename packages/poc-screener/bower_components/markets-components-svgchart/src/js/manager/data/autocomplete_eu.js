define(['../../core/core.js', '../chartConfig/staticConfig.js', './screener_eu'], function(core, staticConfig, screener_eu) {
    'use strict';
    var $ = core.$;
    var url = '//lt.morningstar.com/api/rest.svc/{0}/security/screener';
    var cache = {};
    var cacheCallback = {};

    var categoryholdingTypeIdMapping = (function() {
        var map = {};
        var holdingTypeId = {
            "2": "Mutual Fund",
            "3": "Equities",
            "7": "Index",
            "21": "Closed-End Fund",
            "22": "ETF"
        };
        for (var c in holdingTypeId) {
            var category = holdingTypeId[c];
            if (!map[category]) {
                map[category] = [];
            }
            map[category].push(c);
        }
        return map;
    })();

    function getCacheKey(key, condition) {
        return 'kw=' + key + '&condition=' + condition;
    }

    function getFilters(category) {
        var ret = [],
            unique = {};
        for (var i = 0, l = category.length, cat, conditions; i < l; i++) {
            cat = category[i];
            conditions = categoryholdingTypeIdMapping[cat];
            if (!categoryholdingTypeIdMapping[cat]) continue;
            for (var j = 0, jl = conditions.length, cond; j < jl; j++) {
                cond = conditions[j];
                if (unique[cond]) continue;
                unique[cond] = true;
                ret.push("'" + cond + "'");
            }
        }
        if (ret.length > 0) {
            ret.unshift('HoldingTypeId', 'IN');
        }
        return ret.join(':');
    }

    function getExchange(exchId, micCode) {
        var exch;
        if (micCode) {
            exch = core.DataManager._getMICExangeMap()[micCode] || micCode || '';
        }
        if (!exch) {
            exch = core.DataManager._getExchangeMap()[exchId] || '';
        }
        return exch.replace(/\$/ig, '');
    }

    function dataOnArrived(result) {
        var requestId = result.requestId,
            iData = {};
        $.each(result.tickers, function(i, item) {
            var securityName = staticConfig.Securities[item.mType];
            if (!iData[securityName]) {
                iData[securityName] = [];
            }
            iData[securityName].push(item);
        });
        cache[cacheCallback[requestId].cacheKey] = {
            data: iData,
            params: cacheCallback[requestId].params
        };
        if (typeof cacheCallback[requestId].callback === 'function') {
            cacheCallback[requestId].callback(iData, cacheCallback[requestId].params);
            cacheCallback[requestId].callback = null;
            delete cacheCallback[requestId].callback;
        }
    }

    function fetchData(params, callback) {
        var filters = '',
            autoComplete = params.autoComplete || {},
            universeIds = autoComplete.universeIds || '';
        if (params.needFilter && autoComplete.category && autoComplete.category.length > 0) {
            filters = getFilters(autoComplete.category)
        }
        var cacheKey = getCacheKey(params.queryKey, filters);
        var cacheObject = cache[cacheKey];
        if (!cacheObject) {
            autoComplete.nonFilterCategoryMap = {};
            for (var i = 0, l = autoComplete.nonFilterCategory.length; i < l; i++) {
                autoComplete.nonFilterCategoryMap[autoComplete.nonFilterCategory[i]] = true;
            }
            var requestId = new Date().getTime();
            cacheCallback[requestId] = {
                'callback': callback,
                'cacheKey': cacheKey,
                'params': params
            };
            screener_eu.fetchData({
                solutionKey: params.solutionKey,
                languageId: params.lang,
                filters: filters,
                requestId: requestId,
                universeIds: universeIds,
                sortOrder: autoComplete.sortOrder || 'Name+Asc',
                queryKey: params.queryKey
            }, {
                onSuccess: function(data) {
                    dataOnArrived(data);
                },
                onFailure: function(data) {
                    callback([], data);
                }
            });
        } else {
            callback(cacheObject.data, cacheObject.params);
        }
    }
    return {
        fetchData: fetchData
    };
});