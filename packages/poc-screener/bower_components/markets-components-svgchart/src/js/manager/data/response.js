﻿define(['./request'], function(request) {
    'use strict';

    function Response() {
        var req = new request();
        var data = null;
        var status = Response.Status.Success;

        this.req = function(value) {
            if (!arguments.length) {
                return req;
            }
            req = value;
            return this;
        };

        this.data = function(value) {
            if (!arguments.length) {
                return data;
            }
            data = value;
            return this;
        };

        this.status = function(value) {
            if (!arguments.length) {
                return status;
            }
            status = value;
            return this;
        };
    };

    Response.Status = {
        InvalidRequest: {
            errorCode: '-1',
            errorMessage: 'Invalid request'
        },
        NoData: {
            errorCode: '-2',
            errorMessage: 'No Data'
        },
        Success: {
            errorCode: '0',
            errorMessage: 'Success'
        }
    };

    return Response;
});