define(function() {
    'use strict';
    var createFile = function(container, result, fileName) {

        if (!Blob) {
            throw (new Error('Blob is not supported by your browser'));
        }
        result = '\ufeff' + result;

        var blob = new Blob([result], {
            type: 'text/csv;charset=utf-8;'
        });

        if (window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveBlob(blob, fileName);
        } else {
            var $downloadContainer = $('<div data-tap-disabled="true"><a id="dowload"></a></div>');
            var $downloadLink = $downloadContainer.find('a');
            $downloadLink.attr('href', window.URL.createObjectURL(blob));
            $downloadLink.attr('download', fileName);
            $downloadLink.attr('target', '_blank');
            var $container = container.jquery ? container : $(container);
            $container.append($downloadContainer);
            setTimeout(function() {
                $downloadLink[0].click();
                $downloadLink.remove();
                $downloadContainer.remove();
            }, 0);
        }
    };
    return {
        createFile: createFile
    }
});