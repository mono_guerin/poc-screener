define(['../../core/core.js', '../chartConfig/staticConfig.js'], function(core, staticConfig) {
    'use strict';

    var dataDomain = '';
    var url = 'acb.jsp';
    var cache = {};
    var cacheCallback = {};

    var getCacheKey = function(key, condition) {
        return 'kw=' + key + '&condition=' + condition;
    };

    var generaterCondition = function(category) {
        var ret = [],
            unique = {};
        for (var i = 0, l = category.length, cat, conditions; i < l; i++) {
            cat = category[i];
            conditions = categoryConditionMapping[cat];
            if (!categoryConditionMapping[cat]) continue;
            for (var j = 0, jl = conditions.length, cond; j < jl; j++) {
                cond = conditions[j];
                if (/^ST/.test(cond)) {
                    cond = 'ST';
                }
                if (unique[cond]) continue;
                unique[cond] = true;
                ret.push(cond);
            }
        }
        return ret.join(',');
        //return 'ST,FE,FC,XI,FO,1,2,7,8,9,10,20';
    };

    function getExchange(exchId, micCode) {
        var exch;
        if (exch) {
            exch = core.DataManager._getExchangeMap()[exchId] || '';
        }
        if (!exch) {
            exch = core.DataManager._getMICExangeMap()[micCode] || micCode || '';
        }
        return exch.replace(/\$/ig, '');
    }

    var dataOnArrived = function(data) {
        var unqiuePerformanceIds = {};
        var arrkey = '';
        var requestId = data.id,
            result = data.result || [],
            iData = {};
        //var GB_AC_FIELD = acDataManager.Fields;
        for (var i = 0, l = result.length, item, micCode, securityName, securityName2, r; i < l; i++) {
            item = result[i];
            if (!item) continue;
            if (item.AC003 === '20') { //forex
                item.OS001 = item.OS001 || item.AC009.substr(6);
                item.Name = item.Name || item.AC010;
                item.LS01Z = item.LS01Z || item.AC011;
                item.AC018 = item.AC018 === '200' ? '213' : item.AC018;
            }
            //securityType = item.OS010.toUpperCase();
            micCode = (item.LS01Z || '').substr(6).replace(/\$/ig, '');
            // securityType = item[GB_AC_FIELD["SecurityType"]].toUpperCase();
            // if (securityType === 'FOREX') {
            //     item[GB_AC_FIELD['Ticker']] = item[GB_AC_FIELD['TFForexTicker']].replace('FLIT', '');
            //     item[GB_AC_FIELD['Name']] = item[GB_AC_FIELD['TFForexName']];
            //     item[GB_AC_FIELD['MICCode']] = item[GB_AC_FIELD['TFForexMIC']]; //
            // } else {
            //     item[GB_AC_FIELD['MICCode']] = item[GB_AC_FIELD['MICCode']].substr(6).replace(/\$/ig, '');
            // }

            // if (item[GB_AC_FIELD['TFSecurityType']] == 20) { //forex
            //     if (item[GB_AC_FIELD['TFExchangeID']] == 200 && item[GB_AC_FIELD['TFForexExchangeID']] == 213) {
            //         item[GB_AC_FIELD['TFExchangeID']] = 213;
            //     }
            // }

            if (!core.DataManager._checkAutoCompletePermission(micCode, item.AC018, item.AC003)) {
                continue;
            }
            if ((item.AC003 === '8' && !item.OS001 && item.AC018 === '127') || item.AC003 === '18') {
                item.OS001 = 'ST';
            }
            if (item.AC018 && item.AC003 && item.AC001) {
                item.TenforeId = [item.AC018, item.AC003, item.AC001].join('.');
            }

            securityName = staticConfig.Securities[item.OS010] || staticConfig.Securities[item.AC003];

            if (!iData[securityName]) {
                iData[securityName] = [];
            }
            r = {
                identifier: item.TenforeId || item.OS06Y || item.SecId || '',
                ticker: item.OS001 || item.AC001 || item.ClientTicker || item.OS06Y || item.SecId || '',
                name: item.OS01W || item.AC020 || '',
                exch: getExchange(item.AC018, micCode),
                ISIN: item.OS05J || '',
                category: securityName,
                type: securityName,
                secId: item.SecId
            };
            iData[securityName].push(r);

            securityName2 = null;
            if (item.OS010 === 'ST' || item.AC003 === '1') {
                if (item.AA0A6 === '1') {
                    securityName2 = "STADR";
                } else if (item.ST816) {
                    securityName2 = "ST" + item.ST816;
                }
            }

            if (securityName2) {
                securityName2 = staticConfig.Securities[securityName2];
                if (securityName2) {
                    if (!iData[securityName2]) {
                        iData[securityName2] = [];
                    }
                    iData[securityName2].push($.extend({}, r, {
                        category: securityName2,
                        type: securityName2
                    }));
                }
            }

            // if (!ticker && item[GB_AC_FIELD['TFTicker']] && item[GB_AC_FIELD['TFTicker']]) {
            //     item[GB_AC_FIELD['Ticker']] = item[GB_AC_FIELD['TFTicker']];
            //     ticker = item[GB_AC_FIELD['TFTicker']];
            // }
            // if (!item[GB_AC_FIELD['UserTicker']]) {
            //     item[GB_AC_FIELD['UserTicker']] = ticker;
            // }
            // if (!item[GB_AC_FIELD['Name']] || item[GB_AC_FIELD['Name']] == '') { //TFName
            //     item[GB_AC_FIELD['Name']] = (item[GB_AC_FIELD['TFName']] || '');
            // }

            // if (!item.OS06Y) {
            //     if ((securityType == 2 || securityType == 7) && item[GB_AC_FIELD['TFTicker']]) {
            //         item[GB_AC_FIELD['PerformanceId']] = item[GB_AC_FIELD['TFTicker']];
            //     } else if (item[GB_AC_FIELD['TFExchangeID']] && item[GB_AC_FIELD['TFSecurityType']] && item[GB_AC_FIELD['TFTicker']]) {
            //         item[GB_AC_FIELD['PerformanceId']] = item[GB_AC_FIELD['TFExchangeID']] + '.' + item[GB_AC_FIELD['TFSecurityType']] + '.' + item[GB_AC_FIELD['TFTicker']];
            //     } else if (securityType === 'FOREX') {
            //         item[GB_AC_FIELD['PerformanceId']] = item[GB_AC_FIELD['TFForexTicker']]
            //     }
            // }

            // performanceId = item[GB_AC_FIELD['PerformanceId']] || item[GB_AC_FIELD['SecId']];

            // if (unqiuePerformanceIds[performanceId]) { //reduplicative remove 
            //     continue;
            // }

            // if ((item[GB_AC_FIELD['TFSecurityType']] == 8 && !item[GB_AC_FIELD['SecurityType']] && ticker[GB_AC_FIELD['TFExchangeID']] == 127) || item[GB_AC_FIELD['TFSecurityType']] == 18) {
            //     item[GB_AC_FIELD['SecurityType']] = 'ST';
            // }

            // !item[GB_AC_FIELD['SecurityType']] && (item[GB_AC_FIELD['SecurityType']] = item[GB_AC_FIELD['TFSecurityType']]);

            // if (!item[GB_AC_FIELD['SecurityType']] || item[GB_AC_FIELD['MICCode']] == 'XMEX') {
            //     continue;
            // }

            // securityName = staticConfig.Securities[item[GB_AC_FIELD['SecurityType']]];
            // securityName = securityName || staticConfig.Securities[item[GB_AC_FIELD['TFSecurityType']]];
            // if (!iData[securityName]) {
            //     iData[securityName] = [];
            // }

            // unqiuePerformanceIds[performanceId] = true;
            // iData[securityName].push({
            //     identifier: item.TenforeId || item.PerformanceId || item.SecId,
            //     ticker: item.Symbol || item.Ticker || index(tenforeInfo, 2) || item.PerformanceId || item.SecId,
            //     name: item.Name,
            //     exch: getExchange(index(tenforeInfo, 0), item.MIC),
            //     ISIN: item.ISIN,
            //     type: getMorningstarType(item.HoldingTypeId)
            // });

            // var securityName2 = null;
            // if (item[GB_AC_FIELD['SecurityType']].toUpperCase() === 'ST' || item[GB_AC_FIELD['TFSecurityType']] === '1') {
            //     if (item[GB_AC_FIELD["isADR"]] === '1') {
            //         securityName2 = "STADR";
            //     } else if (item[GB_AC_FIELD["SecurityType2"]]) {
            //         securityName2 = "ST" + item[GB_AC_FIELD["SecurityType2"]];
            //     }
            // }

            // if (securityName2) {
            //     securityName2 = staticConfig.Securities[securityName2];
            //     if (securityName2) {
            //         if (!iData[securityName2]) {
            //             iData[securityName2] = [];
            //         }
            //         iData[securityName2].push(item);
            //     }
            // }
        }
        cache[cacheCallback[requestId].cacheKey] = {
            data: iData,
            params: cacheCallback[requestId].params
        };

        if (typeof cacheCallback[requestId].callback === 'function') {
            cacheCallback[requestId].callback(iData, cacheCallback[requestId].params);
            cacheCallback[requestId].callback = null;
            delete cacheCallback[requestId].callback;
        }
    };

    var saveDataIntoCache = function(dataObj, requestId) {
        var RealtedRequestId = requestId;
        var queryKey = cacheCallback[requestId].queryKey || '';

        var iData = [];
        for (var name in dataObj) {
            var _dataObj = {};
            _dataObj["catName"] = name;
            _dataObj["results"] = [];
            var __data = dataObj[name].Data || [];
            for (var i = 0; i < __data.length; i++) {
                var _data = __data[i];
                var obj = {
                    'category': staticConfig.Securities[_data["SecurityType"]],
                    'ticker': _data["Ticker"],
                    'name': _data["Name"],
                    // 'exch': _data["Exch"],
                    'exch': staticConfig.Securities[_data["SecurityType"]],
                    'type': staticConfig.Securities[_data["SecurityType"]],
                    'identifier': _data["QueryKey"] || _data["TenforeInfo"][1] + "." + _data["TenforeInfo"][2] + "." + _data["Ticker"]
                };
                _dataObj["results"].push(obj);
            };
            iData.push(_dataObj);
        }

        cache[cacheCallback[requestId].cacheKey] = {
            data: iData,
            queryKey: queryKey,
        };

        if (typeof cacheCallback[RealtedRequestId].callback === 'function') {
            cacheCallback[RealtedRequestId].callback(iData, queryKey);
            cacheCallback[RealtedRequestId].callback = null;
            delete cacheCallback[RealtedRequestId].callback;
        }
    };

    var acDataManager = {
        // Fields: {
        //     'SecId': 'SecId',
        //     'Name': 'OS01W',
        //     'CUSIP': 'OS05K',
        //     'ISIN': 'OS05J',
        //     'Ticker': 'OS001',
        //     'SecurityType': 'OS010',
        //     'MICCode': 'LS01Z',
        //     'PerformanceId': 'OS06Y',
        //     'CountryName': 'XI018',
        //     'TFTicker': 'AC001',
        //     'TFName': 'AC020',
        //     'TFForexTicker': 'AC009',
        //     'TFForexName': 'AC010',
        //     'TFForexMIC': 'AC011',
        //     'TFForexExchangeID': 'AC030',
        //     'TFSecurityType': 'AC003',
        //     'TFExchangeID': 'AC018',
        //     'TFCurrencyMarket': 'AC019',
        //     'UserTicker': 'ClientTicker',
        //     "SecurityType2": "ST816",
        //     "isADR": "AA0A6"
        // },
        dataDomain: function(value) {
            if (!arguments.length) {
                return id;
            }
            dataDomain = value;
            return this;
        },
        fetchData: function(params, callback) {
            var autoComplete = params.autoComplete || {};
            var condition = generaterCondition(autoComplete.category);
            var cacheKey = getCacheKey(params.queryKey, condition);
            var cacheObject = cache[cacheKey];
            if (!cacheObject) {
                autoComplete.nonFilterCategoryMap = {};
                for (var i = 0, l = autoComplete.nonFilterCategory.length; i < l; i++) {
                    autoComplete.nonFilterCategoryMap[autoComplete.nonFilterCategory[i]] = true;
                }
                var requestId = new Date().getTime();
                cacheCallback[requestId] = {
                    'callback': callback,
                    'cacheKey': cacheKey,
                    'params': params
                };

                core.Util.getData({
                    url: dataDomain + url,
                    type: 'GET',
                    dataType: 'jsonp',
                    timeout: 100000,
                    data: {
                        'id': requestId,
                        'kw': params.queryKey,
                        'condition': condition,
                        'acbinstid': core.getInstID()
                    }
                }, {
                    onSuccess: function(result) {
                        dataOnArrived(result);
                    },
                    onFailure: function() {
                        callback([], cacheCallback[requestId].params);
                    }
                });
            } else {
                callback(cacheObject.data, cacheObject.params);
            }
        },
        fetchRelatedTickersAndMajorIndexData: function(pId, mainTickerType, callback) {
            var queryKey = pId;
            var mainTickerType = mainTickerType;
            var cacheKey = getCacheKey(queryKey, "relatedTickersAndMajorIndex");
            var cacheObject = cache[cacheKey];
            if (!cacheObject) {
                var requestId = "realtedAndMajorRequestId";
                cacheCallback[requestId] = {
                    'callback': callback,
                    'requestId': requestId,
                    'cacheKey': cacheKey,
                    "queryKey": queryKey
                };

                core.$.when(core.Util.ajax({
                    url: dataDomain + "ra/peerSecurities",
                    type: 'GET',
                    dataType: 'json',
                    timeout: 100000,
                    data: {
                        "mainSecurity": queryKey
                    }
                }), core.Util.ajax({
                    url: dataDomain + "ra/majorIndices",
                    type: 'GET',
                    dataType: 'json',
                    timeout: 100000,
                    data: {
                        "mainSecurity": queryKey
                    }
                })).done(function(result1, result2) {
                    if (result1[1] === 'success' && result2[1] === 'success') {
                        var _dataObj = {};
                        // only sotck has related tickers
                        if (result1[0].Data && result1[0].Data.length > 0 && (mainTickerType == 'ST' || mainTickerType == '1')) {
                            _dataObj["Related Securities"] = result1[0];
                        }
                        if (result2[0].Data && result2[0].Data.length > 0) {
                            _dataObj["Major Indexes"] = result2[0];
                        }
                        saveDataIntoCache(_dataObj, requestId);
                    }
                })

            } else {
                callback(cacheObject.data, cacheObject.queryKey);
            }
        }
    };

    var categoryConditionMapping = (function() {
        var securities = staticConfig.Securities;
        var map = {};
        for (var c in securities) {
            var category = securities[c];
            if (!map[category]) {
                map[category] = [];
            }
            map[category].push(c);
        }
        return map;
    })();

    return acDataManager;
});