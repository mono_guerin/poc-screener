﻿define(function() {
    'use strict';

    function Request() {
        var id;
        var priceType, chartType;
        var isPreAfter = false;
        var frequency;
        var dpType;
        var tickerObjects;
        var dateRange = [null, null];
        var days = -1;
        //var dataPoints;
        var events;
        var indicators;
        var fundamentals;
        var keyword;
        var category = ['Equities', 'ETF', 'Closed-End Fund', 'Mutual Fund', 'Index', 'Forex', 'Futures', 'Options', 'Debenture', 'Government Bonds', 'Strategies & Spreads', 'Unspecified Bonds', 'Unspecified Funds & Certificates', 'Warrents'];
        var headers;
        var keepDateRange = false;
        var nonFilterCategory = [];
        var alignMainTicker = true;
        //growthBaseValue
        var growthBaseValue;

        this.id = function(value) {
            if (!arguments.length) {
                return id;
            }
            id = value;
            return this;
        };

        this.priceType = function(value) {
            if (!arguments.length) {
                return priceType;
            }
            priceType = value;
            return this;
        };

        this.dpType = function(value) {
            if (!arguments.length) {
                return dpType;
            }
            dpType = value;
            return this;
        };
        this.chartType = function(value) {
            if (!arguments.length) {
                return chartType;
            }
            chartType = value;
            return this;
        };

        this.isPreAfter = function(value) {
            if (!arguments.length) {
                return isPreAfter;
            }
            isPreAfter = typeof value === 'boolean' ? value : true;
            return this;
        };

        this.frequency = function(value) {
            if (!arguments.length) {
                return frequency;
            }
            if (!isNaN(value)) {
                value = parseInt(value, 10);
            }
            frequency = value;
            return this;
        };

        this.tickerObjects = function(value) {
            if (!arguments.length) {
                return tickerObjects;
            }
            tickerObjects = value;
            return this;
        };

        this.dateRange = function(value) {
            if (!arguments.length) {
                return dateRange;
            }
            dateRange = value;
            return this;
        };

        this.alignMainTicker = function(value) {
            if (!arguments.length) {
                return alignMainTicker;
            }
            alignMainTicker = value;
            return this;
        }

        this.days = function(value) {
            if (!arguments.length) {
                return days;
            }
            days = value;
            return this;
        };

        /*this.dataPoints = function (value) {
            if (!arguments.length) {
                return dataPoints;
            }
            dataPoints = value;
            return this;
        };*/
        this.events = function(value) {
            if (!arguments.length) {
                return events;
            }
            events = value;
            return this;
        };

        this.indicators = function(value) {
            if (!arguments.length) {
                return indicators;
            }
            indicators = value;
            return this;
        };

        this.fundamentals = function(value) {
            if (!arguments.length) {
                return fundamentals;
            }
            fundamentals = value;
            return this;
        };

        this.keyword = function(value) {
            if (!arguments.length) {
                return keyword;
            }
            keyword = value;
            return this;
        };

        this.category = function(value) {
            if (!arguments.length) {
                return category;
            }
            category = value;
            return this;
        };

        this.headers = function(value) {
            if (!arguments.length) {
                return headers;
            }
            headers = value;
            return this;
        };

        this.keepDateRange = function(value) {
            if (!arguments.length) {
                return keepDateRange;
            }
            keepDateRange = value === true ? true : false;
            return this;
        };

        this.nonFilterCategory = function(value) {
            if (!arguments.length) {
                return nonFilterCategory;
            }
            nonFilterCategory = value;
            return this;
        };

        // default 10000
        this.growthBaseValue = function(value) {
            if (!arguments.length) {
                return growthBaseValue;
            }
            growthBaseValue = value;
            return this;
        };
    }

    return Request;
});