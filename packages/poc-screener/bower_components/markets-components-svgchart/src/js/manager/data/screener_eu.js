define(['../../core/core.js'], function(core) {
    'use strict';
    var $ = core.$;
    var url = 'https://lt.morningstar.com/api/rest.svc/{0}/security/screener';

    function getMorningstarType(holdingTypeId) {
        var mType;
        switch (holdingTypeId) {
            case 1:
                mType = 'C0';
                break;
            case 2:
                mType = 'FO';
                break;
            case 3:
                mType = 'ST';
                break;
            case 3:
                mType = 'Bonds';
                break;
            case 7:
                mType = 'XI';
                break;
            case 21:
                mType = 'FC';
                break;
            case 22:
                mType = 'FE';
                break;
            case 9:
                mType = 'SA';
                break;
            case 8:
                mType = 'CA';
                break;
            case 10:
                mType = 'MI';
                break;
            case 24:
                mType = 'FG';
                break;
            case 50:
                mType = 'FH';
                break;
            case 60:
                mType = 'CB';
                break;
            default:
                mType = 'OTHER';
                break;
        }
        return mType;
    }

    function dataOnArrived(result) {
        var requestId = result.requestTimestamp,
            tickers = [];
        $.each(result.rows, function(i, item) {
            var tenforeInfo = (item.TenforeId || '').split('\.');
            var securityType;
            if (item.HoldingTypeId) {
                securityType = getMorningstarType(item.HoldingTypeId);
            }
            tickers.push({
                identifier: item.TenforeId || item.PerformanceId || item.SecId || '',
                ticker: item.Symbol || item.Ticker || index(tenforeInfo, 2) || item.PerformanceId || item.SecId || '',
                name: item.Name || '',
                exch: getExchange(index(tenforeInfo, 0), item.MIC),
                ISIN: item.ISIN || '',
                CNPJ: item.CNPJ,
                secId: item.SecId,
                currency: item.Currency,
                mType: securityType,
                type: index(tenforeInfo, 1),
                gkey: item.TenforeId,
                performanceId: item.PerformanceId,
                securityId: item.SecId,
                categoryId: item.CategoryId,
                benchMarkId: item.PrimaryBenchmarkId,
                categoryName: item.CategoryName,
                benchMarkName: item.PrimaryBenchmarkName
            });
        });
        return {
            tickers: tickers,
            requestId: requestId
        }

    }

    function getExchange(exchId, micCode) {
        var exch;
        if (micCode) {
            exch = core.DataManager._getMICExangeMap()[micCode] || micCode || '';
        }
        if (!exch) {
            exch = core.DataManager._getExchangeMap()[exchId] || '';
        }
        return exch.replace(/\$/ig, '');
    }

    function index(tenforeInfo, idx) {
        if (tenforeInfo.length >= idx) {
            return tenforeInfo[idx];
        }
        return '';
    }

    function fetchData(params, callback) {
        var data = {
            'timestamp': params.requestId || new Date().getTime(),
            'page': 1,
            'pageSize': 100,
            'outputType': 'json',
            'version': 1,
            'languageId': params.languageId,
            'securityDataPoints': 'Symbol,Ticker,MIC,PerformanceId,SecId,TenforeId,Name,ExchangeCode,InvestmentType,HoldingTypeId,Universe,CNPJ,Currency,CategoryId,CategoryName,PrimaryBenchmarkId,PrimaryBenchmarkName',
            'term': params.queryKey || '',
            'sortOrder': params.sortOrder || 'Name+Asc',
            'universeIds': params.universeIds || '',
            'filters': params.filters || '',

        };
        core.Util.getData({
            third: true,
            url: core.Util.formatString(url, params.solutionKey),
            type: 'GET',
            dataType: 'jsonp',
            timeout: 100000,
            data: data
        }, {
            onSuccess: function(result) {
                if ($.isFunction(callback.onSuccess)) {
                    callback.onSuccess(dataOnArrived(result));
                }
            },
            onFailure: function() {
                if ($.isFunction(callback.onFailure)) {
                    callback.onFailure([], params);
                }

            }
        });
    }

    return {
        fetchData: fetchData
    };
});