define(["../core/core", './util'],
    function(core, util) {
        "use strict";

        function handler(managers) {
            var chartConfigManager = managers.chartConfigManager,
                eventManager = managers.eventManager,
                eventTypes = eventManager.EventTypes,
                errorContainer, config, baseConfig;

            function hideErrorMessage() {
                setTimeout(function() {
                    errorContainer.fadeOut(250, function() {
                        errorContainer.empty().removeClass('alert error');
                        eventManager.trigger([eventTypes.ErrorHidden, eventTypes.HeightChanged], this);
                    });
                }, config.errorTimeout);
            }

            function showErrorMessage(content, type, hide) {
                type = type || 'alert';
                errorContainer.html(content).addClass(type);
                errorContainer.fadeIn(250, function() {
                    if (hide) { hideErrorMessage() }
                    eventManager.trigger([eventTypes.ErrorShown, eventTypes.HeightChanged], this);
                });
            }

            var ErrorHandler = function() {
                baseConfig = chartConfigManager.getConfig();

                // bind all error events after inititialization is complete
                this.bindCollection.forEach(function(arr, i) {
                    eventManager.bind(arr[0], this[arr[1]], this);
                }, this);
            };

            ErrorHandler.prototype.bindCollection = [];

            /**
             * Adds an error to the factory
             * @param {string} name
             * @param {string} eventName - name of the event to watch for
             * @param {function} handler - callback function to fire for event
             */
            function addError(name, eventName, handler) {
                ErrorHandler.prototype[name] = handler

                ErrorHandler.prototype.bindCollection.push([eventName, name])
            }

            addError("blackberryWarningMessage", 'layout-rendered', function() {
                if (util.deviceType.blackberry()) {
                    showErrorMessage.call(this, config.langData['errorsMsg'], 'alert', false)
                }
            });

            addError("tooManyTickers", "error:ticker-limit-reached", function() {
                var errorSpan = baseConfig.$componentContainer.find('.error-message');
                if (errorSpan) {
                    errorSpan.show();
                    setTimeout(function() {
                        errorSpan.hide();
                    }, config.errorTimeout);
                }
            });

            // initialize after the layout is rendered and configuration is done
            eventManager.bind(eventTypes.LayoutRendered, function init() {
                errorContainer = baseConfig.$componentContainer.find('.mkts-cmpt-svgcht-errors');
                config = {
                    errorTimeout: baseConfig.compareChartsMaxAllowedErrorMessageTimeout,
                    langData: chartConfigManager.getLabels('markets-components-svgchart', baseConfig.lang)
                };
            }, this);

            return new ErrorHandler();
        }

        return handler;
    }
)