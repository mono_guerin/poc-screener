define(["../core/core"], function(core) {
    "use strict";

    var $ = core.$;
    var dateFmt, numFmt;

    /**
     *   Make sure console log/debug won't cause errors in unsupported browsers
     */
    if (typeof console !== 'object') {
        window.console = {};
    }

    if (typeof console.log !== 'function' && typeof console.log !== 'object') {
        console.log = function() {};
    }
    if (typeof console.debug !== 'function' && typeof console.debug !== 'object') {
        console.debug = console.log;
    }

    /**
     *   Helper function to correctly set up the prototype chain, for subclasses.
     *   Similar to `goog.inherits`, but uses a hash of prototype properties and
     *   class properties to be extended.
     */
    function extendClass(parentClass, protoProps, staticProps) {
        var child;

        // The constructor function for the new subclass is either defined by you
        // (the "constructor" property in your `extend` definition), or defaulted
        // by us to simply call the parent's constructor.
        if (protoProps && protoProps.hasOwnProperty('constructor')) {
            child = protoProps.constructor;
        } else {
            child = function() { return parentClass.apply(this, arguments); };
        }

        // Add static properties to the constructor function, if supplied.
        $.extend(true, child, parentClass, staticProps);

        // Set the prototype chain to inherit from `parent`, without calling
        // `parent`'s constructor function.
        var surrogate = function() { this.constructor = child; };
        surrogate.prototype = parentClass.prototype;
        child.prototype = new surrogate;

        // Add prototype properties (instance properties) to the subclass,
        // if supplied.
        if (protoProps) $.extend(true, child.prototype, protoProps);

        return child;
    }

    function isTouchCapable() {
        var ret = false;
        var test1 = 'ontouchstart' in window;
        var test2 = 'onmsgesturechange' in window;

        if (test1 || test2) {
            ret = true;
        }
        return ret;
    }

    function dashesToCamel(str) {
        return str.replace(/-([a-z])/g, function(g) { return g[1].toUpperCase(); });
    }

    function camelToDashes(str) {
        //return str.replace(/([a-z][A-Z])/g, function (g) { return g[0] + '-' + g[1]; }).toLowerCase();
        return str.replace(/([A-Z])/g, "-$1").replace(/^-/, "").toLowerCase();
    }

    function capFirstChar(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    function lowerFirstChar(str) {
        return str.charAt(0).toLowerCase() + str.slice(1);
    }

    var isStaticDate = function(str) {
        return /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/.test(str);
    };

    function boldSearch(input, search, needHighLight) {
        if (!needHighLight) {
            return input;
        }

        var specialChar = new RegExp("([\$|\!|\+|\-|\/|\\|\*|\.])", "i");
        search = search.replace(specialChar, "\\" + "$1");
        var rgx = new RegExp(search, "i");
        var tmatch = input.match(rgx);
        if (tmatch != null) {
            input = input.replace(rgx, "<em>" + tmatch[0] + "</em>");
        }

        return input;
    }

    function contains(f, c) {
        if (!f) {
            return false;
        }
        var bCB2F = false;
        if (f.contains) {
            bCB2F = f.contains(c);
        } else {
            bCB2F = (typeof f.compareDocumentPosition == "function" && f.compareDocumentPosition(c) == 20) ? true : false;
        }
        return bCB2F;
    }

    function arrConcat(obj, arr) {
        if ($.isArray(obj))
            obj = obj.concat(arr);
        else
            obj = arr;

        return obj;
    }

    /**
     * Class to handle user agent matching
     */
    function DeviceType() {
        var useragent = navigator.userAgent,
            device;

        if (useragent.match(/Android/i)) {
            device = 'android';
        } else if (useragent.match(/webOS/i)) {
            device = 'webos';
        } else if (useragent.match(/iPhone/i)) {
            device = 'iphone';
        } else if (useragent.match(/iPod/i)) {
            device = 'ipod';
        } else if (useragent.match(/iPad/i)) {
            device = 'ipad';
        } else if (useragent.match(/Windows Phone/i)) {
            device = 'windows phone';
        } else if (useragent.match(/windows ce/i)) {
            device = "windows ce";
        } else if (useragent.match(/SymbianOS/i)) {
            device = 'symbian';
        } else if (useragent.match(/BlackBerry|RIM|BB\d{2}/)) {
            device = 'blackberry';
        } else {
            device = undefined;
        }

        /** wrapper for device matching
         * @param String - a list of device strings
         * @return Function - the function that will run for the device method
         **/
        function isDevice() {
            var args = Array.prototype.slice.call(arguments);
            var matchDevice = function(obj) {
                return obj === device
            };

            return function() {
                return args.some(matchDevice);
            }
        }

        this.android = isDevice('android');
        this.iPhone = isDevice('iphone');
        this.iPad = isDevice('ipad');
        this.blackberry = isDevice('blackberry');
        this.windowsPhone = isDevice('windows phone');
        this.windowsCe = isDevice('windows ce');
        this.iOS = isDevice('iphone', 'ipad', 'ipod');
        this.mobile = function() {
            return device !== undefined;
        }
    }

    function numberFormatter(locale, n, f, sep, needmark, isFormat, showOriginalDecimal) {
        var MSize = [1e+12, 1e+9, 1e+6, 1e+3];
        var Mark = ['T', 'bil', 'mil', 'k'];
        var zs = ['000', '00', '0'];
        var sep = (sep != undefined) ? sep : " ";
        var ds = ".";
        var gs = ",";
        var lang = locale.split('-')[0];
        if (showOriginalDecimal) {
            var strn = n + '';
            f = strn.indexOf('.') === -1 ? 0 : (strn.length - strn.indexOf('.') - 1);
        }
        if ($.inArray(lang, ['fr', 'it', 'es', 'de', 'pt']) > -1) {
            Mark = ['T', 'G', 'M', 'K'];
            ds = ",";
            gs = ".";
        }
        var ne = 0;
        if (n == Infinity || isNaN(n)) {
            return '--';
        }
        if (needmark === false && isFormat === false) {
            return (f != undefined) ? parseFloat(n).toFixed(f < 0 ? 2 : f) : n;
        }
        if (f != undefined) {
            n = +parseFloat(n).toFixed(f < 0 ? 2 : f);
        }
        if (n < 0) {
            ne = 1;
            n = -n;
        }
        try {
            var t = n;
            var m = '';
            var ret = '';
            if (needmark !== false) {
                if (n >= MSize[MSize.length - 1]) { //divide the million , billion or thousand
                    for (var i = 0, size = MSize.length; i < size; i++) {
                        if (n >= MSize[i]) {
                            t = n / MSize[i];
                            m = sep + Mark[i];
                            break;
                        }
                    }
                }
            }
            if (f < 0) { // if f is invalid,it will be used the default value.
                f = m == '' ? 1 : 2;
            }
            if (isFormat === false) {
                ret = t.toFixed(f) + m;
                // ne is a negative flag, unit abbreviations format logic lack this flag
                if (ne == 1) {
                    ret = '-' + ret;
                }
                return ret;
            }
            var p1 = Math.floor(t);
            //integer
            var p2 = t - p1;
            var ar = [];
            var st;
            //toFixed
            if ((f < 0 && p2 != 0) || f > 0) {
                var k = new Number(p2);
                k = k.toFixed(f);
                if (k == 1)
                    p1++;
                m = ds + k.substr(2) + m;
            }

            //format
            do {
                t = p1 % 1000;
                p1 = Math.floor(p1 / 1000);
                st = '' + t;
                if (p1 > 0 && st.length < 3) {
                    st = zs[st.length] + st
                }
                ar.unshift(st);
            } while (p1 > 0);
            ret = ar.join(gs) + m;
        } catch (e) {
            ret = '' + n;
        }
        if (ne == 1) {
            ret = '-' + ret;
        }
        return ret;
    }

    function calculateDecimal(ticks, min, max) {
        var scale = 0,
            decimal = 2;
        if (min == max) {
            min = min / 2.0;
            max = min + max;
        }

        scale = (max - min) / ticks;
        if (scale > 0.25) {
            decimal = 2;
        } else if (scale > 0.0001) {
            decimal = 4;
        } else if (scale > 0.00001) {
            decimal = 5;
        } else {
            decimal = 6;
        }

        return decimal;
    }

    // Taken from underscore.js
    // Returns a function, that, as long as it continues to be invoked, will not
    // be triggered. The function will be called after it stops being called for
    // N milliseconds. If `immediate` is passed, trigger the function on the
    // leading edge, instead of the trailing.
    function debounce(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this,
                args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    }

    function isFund(tickerObj) {
        var tenforeCode = tickerObj.getField("tenforeCode"),
            tenforeType = tickerObj.getField("type"),
            mType = tickerObj.getField("mType")
        if (tenforeType == 8) {
            if (mType && mType != "FO") {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    function getKeyByValueInObj(obj, value) {
        var returnKey = '';
        for (var key in obj) {
            if (obj[key] === value) {
                returnKey = key;
                break;
            }
        }
        return returnKey;
    }

    //format number until find the effect number
    function formatNotZeroNum(count, decimal) {
        // judge count is 0/0.00/0.0..00
        if (parseFloat(count, 10) === 0) {
            return;
        }
        if (/^0(\.(0)+)?$/.test(count.toFixed(decimal))) {
            decimal++;
            return formatNotZeroNum(count, decimal);
        } else {
            return count.toFixed(decimal);
        }
    }

    function endwith(str, char) {
        var pos = str.lastIndexOf(char);
        if (pos === -1) {
            return false;
        } else {
            return pos + 1 === str.length;
        }
    }

    function isInteger(num) {
        return Math.floor(num) === num;
    }

    function toInteger(num) {
        if (isInteger(num)) {
            return {
                times: 1,
                intNum: num
            };
        }
        var numString = num + '';
        var precision = numString.replace(/^-?\d+.(\d)/, '$1').length;
        var times = Math.pow(10, precision);
        return {
            times: times,
            intNum: parseInt(numString.replace('.', ''), 10)
        };
    }

    function operation(a, b, op) {
        var ret,
            integerA = toInteger(a),
            integerB = toInteger(b),
            intNumA = integerA.intNum,
            intNumB = integerB.intNum,
            maxTimes = Math.max(integerA.times, integerB.times);

        switch (op) {
            case '+':
                if (integerA.times > integerB.times) {
                    intNumB = intNumB * integerA.times / integerB.times;
                } else if (integerA.times < integerB.times) {
                    intNumA = intNumA * integerB.times / integerA.times;
                }
                ret = (intNumA + intNumB) / maxTimes;
                break;
            case '*':
                ret = (intNumA * intNumB) / (integerA.times * integerB.times);
                break;
            case '/':
                ret = (intNumA / intNumB) * (integerB.times / integerA.times);
                break;
            case '%':
                if (integerA.times > integerB.times) {
                    intNumB = intNumB * integerA.times / integerB.times;
                } else if (integerA.times < integerB.times) {
                    intNumA = intNumA * integerB.times / integerA.times;
                }
                ret = (intNumA % intNumB) / maxTimes;
                break;
        }
        return ret;
    }

    function add(a, b) {
        if (isInteger(a) || isInteger(b)) {
            return a + b;
        }
        return operation(a, b, '+');
    }

    function subtract(a, b) {
        if (isInteger(b)) {
            return a - b;
        }
        return operation(a, -b, '+');
    }

    function multiply(a, b) {
        if (isInteger(a) && isInteger(b)) {
            return a * b;
        }
        return operation(a, b, '*');
    }

    function divide(a, b) {
        if (isInteger(a) || isInteger(b)) {
            return a / b;
        }
        return operation(a, b, '/');
    }

    function mod(a, b) {
        return operation(a, b, '%');
    }

    return {
        debounce: debounce,
        boldSearch: boldSearch,
        extendClass: extendClass,
        dashesToCamel: dashesToCamel,
        camelToDashes: camelToDashes,
        capFirstChar: capFirstChar,
        lowerFirstChar: lowerFirstChar,
        isStaticDate: isStaticDate,
        contains: contains,
        isTouchCapable: isTouchCapable,
        arrConcat: arrConcat,
        deviceType: new DeviceType(),
        numberFormatter: numberFormatter,
        calculateDecimal: calculateDecimal,
        isFund: isFund,
        getKeyByValueInObj: getKeyByValueInObj,
        formatNotZeroNum: formatNotZeroNum,
        endwith: endwith,
        Math: {
            add: add,
            subtract: subtract,
            multiply: multiply,
            divide: divide,
            mod: mod
        }
    };
});