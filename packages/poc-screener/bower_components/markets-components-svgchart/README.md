# markets-component-svgchart
markets-component-svgchart

<b>

## Install
#### `$ bower install  markets-components-svgchart --save `


<p style="font-weight:normal">Please add .bowerrc file with following content.</p>

     {
	    ...
	    "registry": {
		    "search": [
			   "https://msnexus.morningstar.com/repository/bower-all/",
			   "https://bower.herokuapp.com"
		    ]
	    },
	    "resolvers": ["bower-nexus3-resolver"],
	    "interactive": false,
	    "shallowCloneHosts": [
	        "msstash.morningstar.com"
	    ]
    }


</b>

## Usage
The component require a authentication process as below.

1. Email to <a href="mailto:marketsdevelopment@morningstar.com">markets development team</a> to get instid and account. if you already have these, please skip this step.
2. Use the account to do authentication.

#### Authentication

You can invoke below web service provided by Markets team in your server side. And you could parse the response JSON to get the session ID. This id will be used when you initializing this component.

***Note: There is a IP white list with your servers. Please provide them to Marktes team before you invoke the web service.***

**Web service**

    Production URL: https://quotespeed.morningstar.com/service/auth
	Staging URL: https://markets-uat.morningstar.com/service/auth

**Request parameters**

<table cellpadding=0 cellspacing=0 border="1">
<thead>
<tr class="header">
<th>key</th>
<th>Required</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td>instid</td>
<td>Yes</td>
<td>An unique ID assigned to you by Morningstar markets.</td>
</tr>
<tr>
<td>instuid</td>
<td>Yes</td>
<td>Unique identifier for the user provided by client side. We suggest to set is as same as email.</td>
</tr>
<tr>
<td>email</td>
<td>Yes</td>
<td>Unique identifier for the user provided by client side.</td>
</tr>
<tr>
<td>group</td>
<td>Yes</td>
<td>User group is provided by Morningstar.</td>
</tr>
<tr>
<td>e</td>
<td>Yes(internal only)</td>
<td>RTD credential, it will be used to pass the encrypted RTD account string. For how to get this string, please reference following links. <a gref="https://mswiki.morningstar.com/pages/viewpage.action?title=Entitlement+Integration&spaceKey=SWPAAS">https://mswiki.morningstar.com/pages/viewpage.action?title=Entitlement+Integration&spaceKey=SWPAAS</td>
</tr>
</tbody>
</table>

**Response JSON**


    {
	    "status": {
			"errorCode":"0",
			"subErrorCode":"",
			"errorMsg":"Sucessful"
		},
		"data":"95818CD5B0B2BC0E2F7B2577235F40E7",
		"attachment":{
			"SessionID":"95818CD5B0B2BC0E2F7B2577235F40E7",
			"sessionTimeout":1592
		}
    }

## Browser compatibility
Markets components are using browser native number formatting function (Intl) which is a ES6(ES2015) feature. It may not be supported in some lower browsers such as IE 10. If you want your product to compatible with these browsers, please add below polyfill to your page.

    <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Intl.~locale.en,Intl.~locale.fr"></script>

## Sample codes
    
	<div class="container"></div>

    <script>
		var params = {
			element: $(".container"),
			initialConfiguration:{
				instid:'xxxx'
				env:'stage', //Please change to production when going to production
				sessionKey: 'xxxxxx',
				component: {
					configuration:{}
				}
			}
		};
		var component = morningstar.components['markets-components-svgchart'].createComponent(params);
	</script>    

## Component Interface
 **create the svgchart component object instance.**

    createComponent({
        element: <el>,
        initialConfiguration: {}
    });

supported key-value pairs for above initialConfiguration JSON object
<table cellpadding=0 cellspacing=0 border="1">
<thead>
<tr class="header">
<th>key</th>
<th>value</th>
<th>default</th>
</tr>
</thead>
<tbody>
<tr>
<td>instid</td>
<td>given from Morningstar markets</td>
<td></td>
</tr>
<tr>
<td>env</td>
<td>production or stage</td>
<td>stage</td>
</tr>
<tr>
<td>sessionKey</td>
<td>the response with authenciation</td>
<td></td>
</tr>
<tr>
<td>component</td>
<td><pre>{
  configuration:{} //JSON object. please refrence below table for details
}</pre></td>
<td></td>
</tr>
</tbody>
</table>

supported key-value pairs for above configuration JSON object

<style>
table th:nth-of-type(2) {
    width: 100px;
}
</style>

<table cellpadding=0 cellspacing=0 border="1">
<thead>
<tr class="header">
<th>key</th>
<th>Optional Value</th>
<th>default</th>
<th>description</th>
</tr>
</thead>
<tbody>
<tr>
<td rowspan="2">mainTicker</td>
<td>"126.1.IBM" / "0P000002RH" / "XNAS:MSFT"</td>
<td rowspan="2">"126.1.IBM"</td>
<td>Used to set the main ticker of chart. It can be a string of realtime id, performanceId or exchange:ticker. </td>
</tr>

<tr>
<td><pre>
{
 id:"126.1.IBM",  
 name:"International Business Machines", 
 color:"#AA008A", 
 securityToken:"F0000008V0]",
 currency:"GBP" 
}</pre></td>

<td>Can be an object with customize features. 
<p>id: required. realtime id,performanceId or exchange:ticker</p>
<p>name: optional. custom name</p>
<p>color: optional. custom color</p>
<p>securityToken: need to apply this property when useEUTimeSeriesData set to true</p>
<p>currency: need to apply this property when useEUTimeSeriesData set to true</p>
</td>
</tr>

<tr>
<td>compareTickers</td>
<td>
<pre>[
 "126.1.AAPL",
 "0P000002RH",
 "XNAS:MSFT",
 {id:"126.1.IBM",color:"#AA008A",name:"IBM"}
]</pre></td>
<td>[]</td>
<td>Used to set compare tickers. An array of realtime id, performanceId, exchange:ticker and object reference with mainticker.</td>
</tr>

<tr>
<td>lang</td>
<td>"en-US" / "fr-CA" / "en-GB" / "pt-PT" / "de-DE" / "it-IT" / "zh-CN" / "zh-TW"</td>
<td>"en-US"</td>
<td>Used to configure which language is applied.</td>
</tr>

<tr>
<td>chartType</td>
<td>"mountainChart" / "lineChart" / "dotChart" / "ohlcChart" / "candlestickChart" / "abChart"</td>
<td>"mountainChart"</td>
<td>Used to set the graph of main chart, for example:
<pre>
var chartConfig = {
        charts:{
            mainchart:{
                chartType:"mountainChart"
            }
        }   
};
</pre>
</td>
</tr>
<td>dataType</td>
<td>"price" / "nav" / "dividendEffect" / "growth" / "growth10K" / "postTax" / "totalReturn" / "marketValue"</td>
<td>"price"</td>
<td>Used to set the data of main chart, for example:
<pre>
var chartConfig = {
    charts:{
        mainchart:{
            dataType:"price"
        }
    }   
}</pre>

<p>
price: Main chart will be drawn with the price data(OHLC). 	
</p>
<p>
nav: Main chart will be drawn with the NAV data.	It is available with fund.
</p>
<p>
dividendEffect: Main chart will be drawn with the dividendEffect data.
</p>
<p>
growth: Main chart will be drawn with the growth data.
</p>
<p>
growth10K: Main chart will be drawn with the 10K growth data.
</p>
<p>
postTax: Main chart will be drawn with the postTax data. It is available with fund.
</p>
<p>
totalReturn: Main chart will be drawn with the total return data. It is available with fund and portfolio.
</p>
<p>
marketValue: Main chart will be drawn with the market value data. It is available with fund and portfolio.
</p>
</td>
</tr>

<tr>
<td>showCategory</td>
<td>true / false</td>
<td>false</td>
<td>Used to set whether to display the related category of the fund.</td>
</tr>

<tr>
<td>showRiskBenchmark</td>
<td>true / false</td>
<td>false</td>
<td>Used to set whether to  display the related risk benchmark of the fund.</td>
</tr>

<tr>
<td>mainTickerColor</td>
<td>"#339bb6"</td>
<td>"#339bb6"</td>
<td>Used to set the main ticker color.</td>
</tr>

<tr>
<td>compareColors</td>
<td>An array of color string. </td>
<td>['#FF5621', '#B967C7', '#19227D', '#2095F2', '#548A2E', '#AEB32A', '#4D332D']</td>
<td>Used to set the compare tickers' color.</td>
</tr>

<tr>
<td>showCrosshairVolume</td>
<td>true / false </td>
<td>true</td>
<td>Used to control whether to show volume on crosshair. </td>
</tr>

<tr>
<td>currencyAliases</td>
<td>An object for currency alias maping. For example, {"USD":"$"} </td>
<td>{}</td>
<td>Used to show the currency alias instead of currency code.</td>
</tr>

<tr>
<td>intervalType</td>
<td>"OneDay" / "FiveDay" / "FifteenDay" / "OneMonth" / "ThreeMonth" / "SixMonth" / "YTD" / "OneYear" / "ThreeYear" / "FiveYear" / "TenYear" / "Max" </td>
<td>"OneDay"</td>
<td>Used to control which intervalType will be applied as default.</td>
</tr>
<tr>

<tr>
<td>frequency</td>
<td>"1" / "5" / "10" / "15" / "30" / "d" / "YTD" / "w" / "m" </td>
<td>"1"</td>
<td>Used to control which frequency will be applied as default.</td>
</tr>
<tr>
<td>theme</td>
<td>"1" / "5"</td>
<td>"1"</td>
<td>Used to control which theme will be applied.</td>
</tr>
<tr>
<td>growthBaseValue</td>
<td>200</td>
<td>null</td>
<td>Used to set the growth amount for the mainchart. Only works when chart dataType is growth10K.</td>
</tr>
<tr>
<td rowspan="2">category</td>
<td>A string array, for example:["Equities", "ETF", "Index"]
<p>Optional categories:
 "Equities", "Forex", "Debenture", "Bonds", "ETF","Closed-End Fund", "Mutual Fund", "Index" and "Option"</p></td>
<td rowspan="2">{
 "stock":['Equities', 'ETF', 'Index', 'Closed-End Fund'],
 "forex":['Forex'],
 "fund":['Mutual Fund']
}
</td>
<td>Used to configure the security type can be searched by search box. </td>
</tr>
<tr>
<td>A json object,for example:
<pre>{
	"stock":["Equities", "ETF"],
	 'FC': 'Closed-End Fund'
}</pre>
<p>Optional key of the json: "stock" / "forex" / "fund" / "FC" / "FO" / "FE" / "ST" / "XI". </p>
</td>
<td>
 Used to configure the security type can be searched by search box according to the main ticker's security type. 
</td>
<tr>
<td>fixHeight</td>
<td>true / false</td>
<td>true</td>
<td>Used to control the chart height is fixed or not.</td>
</tr>

<tr>
<td>chartCursor</td>
<td>"crosshair" / "trackball" / "off" </td>
<td>"crosshair"</td>
<td>Used to set crosshair type. 
<p>crosshair:show price information on tooltip when mousemove.</p>
<p>trackball:show price information on legend above the chart when mousemove.</p>
<p>off:turn off this feature.</p>
 </td>
</tr>

<tr>
<td>SMode</td>
<td>"pan" / "zoomIn" / "off"</td>
<td>"pan"</td>
<td>Used to configure the mode when drag the chart. <p>pan: Drag horizontally.</p> <p>zoomIn: Drag: Select a date range.</p> <p>off: Disable pan and zoom in.</p></td>
</tr>

<tr>
<td>legendInfoLabel</td>
<td>"Name" / "clientTicker" / "ticker", or can be customized.</td>
<td>"clientTicker"</td>
<td>Used to set which fields to be displayed in legend area above the main chart.<br> It also can be a string format expression. Use 126.1.IBM for an example:legendInfoLabel: {clientTicker}-{MIC-CODE} ({currency}) currency  would display as 'IBM-XNYS (USD)'</td>
</tr>
<tr>
<td>yAxisOrient</td>
<td>"left" / "right"</td>
<td>"right"</td>
<td>Used to set the y axis to be 'right' or 'left'</td>
</tr>

<tr>
<td>yAxisPosition</td>
<td>"inner" / "outer"</td>
<td>"outer"</td>
<td>Used to control the y-Axis is in or out of the chart. </td>
</tr>

<tr>
<td>compareClose</td>
<td>true / false</td>
<td>true</td>
<td>Used to set if display the close button of the compare button.</td>
</tr>

<tr>
<td>frequencyPriority</td>
<td>"manually" / "default"</td>
<td>"default"</td>
<td>Used to control the priority of frequency when change date intervals. 
<p>"default":  the frequency would change according to intervalFrequencyMap of chart. intervalFrequencyMap is also one of chart configuration, please reference below for detail.</p>
<p>"manually": the frequency would be what you selected last time. </p>
default frequency map setting as the first priority. </td>
</tr>

<tr>
<td>intervalFrequencyMap</td>
<td> 		    
	 { OneMonth: "d"}
</td>
<td>
	<pre>
{
    OneDay: "1",
    FiveDay: "5",
    FifteenDay: "5",
    OneMonth: "10",
    ThreeMonth: "d",
    SixMonth: "d",
    YTD: "d",
    OneYear: "d",
    ThreeYear: "d",
    FiveYear: "w",
    TenYear: "w",
    FifteenYear: "w",
    Max: "m",
    Custom: "5"
}</pre> </td>
<td>Used to configure the frequency of each date range</td>
</tr>

<tr>
<td>autoCompleteAction</td>
<td> 		    
	 "compare" / "main"
</td>
<td>
	"compare"
</td>
<td>Used to control search box behavior.
<p>compare: To add a compare ticker.</p>
<p>main: To change the main ticker.</p>
</td>
</tr>
<tr>
<td>autoComplete</td>
<td> 		    
<pre>{
  action:"main",
  dataSource:"EU",
  universeIds:"FOBRA$$ALL"
}</pre>
</td>
<td>
<pre>{
  action:"compare",
  dataSource:"",
  universeIds:""
}</pre>
</td>
<td>Used to control search box behavior.
<p>action: "main"/"compare". Used to config 'change a main ticker' or 'add a compare ticker' when choose a ticker from autocomplete </p>
<p>dataSource: "EU". Used to swtich the data source to IWT EU service.</p>
<p>universeIds: Provided by IWT team. Only works when dataSource set to "EU"
</td>
</tr>
<tr>
<td>intervalBreakPointMap</td>
<td>{'600': ['1D', '1M', '1Y', '5Y', 'MAX']}</td>
<td>
{ 
	'default': ['1D', '5D', '15D', '1M', '3M', '6M', 'YTD', '1Y', '3Y', '5Y', '10Y', 'MAX'],
	'600': ['1D', '1M', '1Y', '5Y', 'MAX']
 }
</td>
<td>Used to configure interval type buttons in different chart size. 
<P>The chart size can be any number or 'default'.</P> 
<P>Interval type should be an array of optional value in below array:<br> ['1D', '5D', '15D', '1M', '3M', '6M', 'YTD', '1Y', '3Y', '5Y', '10Y', 'MAX'].</P> </td>
</tr>

<tr>
<td>tagMode</td>
<td> 		    
   "s"
</td>
<td>
	""
</td>
<td>Used to draw price tag in a samller size.</td>
</tr>

<tr>
<td>crosshairMode</td>
<td> 		    
	 "s"
</td>
<td>
	""
</td>
<td>Used to render crosshair in a samller size.</td>
</tr>

<tr>
<td>rangeFrequency</td>
<td><pre>{'MAX':['w','m'],'10Y':['15','5']}</pre></td>
<td></td>
<td>Used to configure the options of frequency dropdown list for each data range. 
<p>The optional date range : '1D', '5D', '15D', '1M', '3M', '6M', 'YTD', '1Y', '3Y', '5Y', '10Y', 'MAX' </p>
<P>The optional frequency: '1', '5', '15', '30', '60', 'd', 'w', 'm'</P>
</td>
</tr>

<tr>
<td>defaultDecimal</td>
<td>number</td>
<td>2</td>
<td>Used to configure the decimal places for chart, including price on crosshair and yaxis ticks.</td>
</tr>

<tr>
<td>decimalPlaces</td>
<td>number</td>
<td>2</td>
<td>Used to configure the decimal places for chart, except for y-axis ticks and volume.</td>
</tr>

<tr>
<td>yAxisDecimal</td>
<td>number</td>
<td>It depends on the algorithm results.</td>
<td>Used to configure the decimal places for chart, except for y-axis ticks.</td>
</tr>

<tr>
<td>volumeDecimal</td>
<td>number</td>
<td>2</td>
<td>Used to configure the volume decimal places When volume is >1000000. </td>
</tr>

<tr>
<td>chgPDecimal</td>
<td>number</td>
<td>2</td>
<td></td>
</tr>

<tr>
<td>isFormat</td>
<td>true/false</td>
<td>true</td>
<td>Used to control whether format the number with thousand separater.</td>
</tr>

<tr>
<td>crossCurrency</td>
<td>true/false</td>
<td>true</td>
<td>Used to configure whether to show currency before price. This configuration works for fund chart only.</td>
</tr>

<tr>
<td>legendChange</td>
<td>true/false</td>
<td>true</td>
<td>Used to configure whether to show change legend above the chart.</td>
</tr>
<tr>
<td>isShowRelateTickerAndMajorIndex</td>
<td>true / false</td>
<td>false</td>
<td>Used to configure whether to show related tickers and major index in autocomplete without input</td>
</tr>

<tr>
<td>compareChartsMaxAllowed</td>
<td> 7 </td>
<td>7 </td>
<td>Used to set the max number of compare tickers can be added into chart.</td>
</tr>

<tr>
<td>hideAllMenuBar</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide the whole menu bar</td>
</tr>
<tr>
<td>hideMenu</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide first line menu bar sections</td>
</tr>
<tr>
<td>hideMenuBottom</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide the second line menu bar</td>
</tr>
<tr>
<td>hideSymbol</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide the main chart header(ticker and currency) of the chart</td>
</tr>
<tr>
<td>hideCurrency</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide the currency above the main chart</td>
</tr>
<tr>
<td>hideLegend</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide the tickerLegend above the main chart</td>
</tr>
<tr>
<td>hideVolume</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide the volume section at the bottom of the chart</td>
</tr>
<tr>
<td>hideSlider</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide the slider sections at the bottom of the chart</td>
</tr>
<tr>
<td>hideCrossHair</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide CrossHair when mousemove on chart</td>
</tr>

<tr>
<td>hideAllMenuBar</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide all menu bar.</td>
</tr>

<tr>
<td>hideMenu</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide the most top menu bar.(The search,indicators,fundamental,drawings,display,rest,export.)</td>
</tr>

<tr>
<td>hideMenuBottom</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide the second top menu bar above the chart.(The intervals,calendar,frequency)</td>
</tr>

<tr>
<td>hideExport</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide Export button on chart menu bar.</td>
</tr>

<tr>
<td>hideResetIcon</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide Reset button on chart menu bar.</td>
</tr>

<tr>
<td>hidePrint</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide print button on chart menu bar.</td>
</tr>

<tr>
<td>hideSearch</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide search on the chart menu bar.</td>
</tr>

<tr>
<td>hideIndicator</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide Indicator menu on the chart menu bar.</td>
</tr>

<tr>
<td>hideFundamental</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide Fundamental menu on the chart menu bar.</td>
</tr>

<tr>
<td>hideDrawing</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide Drawings menu on the chart menu bar.</td>
</tr>

<tr>
<td>hideDisplay</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide the Display menu on the chart menu bar.</td>
</tr>

<tr>
<td>hideDateRange</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide the date intervals buttons on the chart menu bar.</td>
</tr>

<tr>
<td>hideCalendar</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide the calendar on the chart menu bar.</td>
</tr>

<tr>
<td>hideFrequency</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide the frequency dropdown list on the chart menu bar.</td>
</tr>

<tr>
<td>showCloseBtn</td>
<td>true / false</td>
<td>false</td>
<td>Used to show/hide the close button on chart menu bar</td>
</tr>



<tr>
<td>hideMainYAxis</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide the y-Axis of main chart.</td>
</tr>


<tr>
<td>hideMainXAxis</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide the x-Axis of main chart.</td>
</tr>

<tr>
<td>hideVolumeYAxis</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide the x-Axis of volume chart.</td>
</tr>

<tr>
<td>hideSliderXAxis</td>
<td>true / false</td>
<td>false</td>
<td>Used to hide the x-Axis of slider chart.</td>
</tr>

<tr>
<td>hideMainLegend</td>
<td>true / false</td>
<td>false</td>
<td>Used to show/hide the mainTicker info legend above the chart.</td>
</tr>

<tr>
<td>showStartDate</td>
<td>true / false</td>
<td>false</td>
<td>Used to controle show startDate option on UI.</td>
</tr>

<tr>
<td>startDate</td>
<td>"common" / "earliest" / "main"</td>
<td>main</td>
<td>Used to indicate the base date when camparison added.</td>
</tr>
<tr>
<td>menubar</td>
<td>An object of menu buttons to hide.
	<pre>
menubar:{
    hideEventsIndicators:["SMA","EMA"],
    hideFundamentals:["FairValue"]
}</pre>

<p>The optional eventsIndicators to hide:<br/>
"SMA",  "EMA", "BBands", "PSAR", "WMA", "Keltner", "VBP", "PChannel", "MAE", "DYield", "RDividend", "MACD", "RSI", "ROC", "WillR", "SStochastic", "FStochastic", "Momentum", "ULT", "DMI", "MFI", "OBV", "VAcc", "UDRatio", "FVolatility", 
"AccDis", "Mass", "Volatility", "ForceIndex", "ATR", 'SMAV', 'volumeChart', 'PrevClose', 'Dividend', 'Earnings', 'Split'</p>
<p>The optional fundamentals to hide:<br/>
"FairValue", "QuantitativeFairValue", "DailyCumFairNav", "PE", "PS", 'PB', 'PC', 'FairValue', 'DYield', 'RDividend', 'REPS', 'PremiumDiscount', 'SInterest'</p>
</td>
<td>{}</td>
<td>Used to hide menu buttons.</td>
</tr>

<tr>
<td>indicators</td>
<td> An array of objects:<br> [{ name: "SMA", parameters: [10, 50] }] 
<p>Optional indicators to set:
"SMA", "EMA", "BBands", "PSAR", "WMA", "Keltner", "VBP", "PChannel", "MAE", "DYield", "RDividend", "MACD", "RSI", "ROC", "WillR", "SStochastic", "FStochastic", "Momentum", "ULT", "DMI", "MFI", "OBV", "VAcc",  "UDRatio", "FVolatility", "AccDis","Mass","Volatility","ForceIndex","ATR"</p></td>
<td>[]</td>
<td>Used to add some default indicators at the first load.The default indicator works depend on corresponding security type and frequency. </td>
</tr>

<tr>
<td>fundamentals</td>
<td> An array of string:<br>["FairValue"] 
<p>Optional fundamentals to set:<br>
"FairValue",<br> "QuantitativeFairValue",<br> "DailyCumFairNav",<br> "FundSize", <br>"ShareholdersNumber"</p></td>
<td>[]</td>
<td>Used to add some default fundamentals at the first load.The default fundamental works depend on corresponding security type and frequency. Mostly fundamentals work only when frequency is set to daily,weekly,monthly.</td>
</tr>


<tr>
<td>events</td>
<td> An array of string:<br> ["dividend"] 
<p>Optional events to set:<br>
"dividend", "splits", "earnings"</p></td>
<td>[]</td>
<td>Used to add some default events data points at the first load.</td>
</tr>

<tr>
<td>hightLightHoveredLine</td>
<td> true / false </td>
<td> true </td>
<td>Used to configure whether to high light a line when mouse over in the compare charts.</td>
</tr>

<tr>
<td>showCrosshairChange</td>
<td> true / false </td>
<td> false </td>
<td>Used to configure whether to show daily change on crosshair tooltip. This feature only works for fund.</td>
</tr>

<tr>
<td>showCrosshairChangePercent</td>
<td> true / false </td>
<td> false </td>
<td>Used to configure whether to show daily change percent on crosshair tooltip. This feature only works for fund.</td>
</tr>

<tr>
<td>showCrosshairOriginalData</td>
<td> true / false </td>
<td> false </td>
<td>Used to configure whether to show original nav or price on crosshair tooltip. This feature only works for growth or growth10K chart.</td>
</tr>

<tr>
<td>showGridBlocks</td>
<td> true / false </td>
<td> false </td>
<td>Used to configure whether to show grid blocks as chart background.</td>
</tr>

<tr>
<td>showSolidGridLines</td>
<td> true / false </td>
<td> false </td>
<td>Used to configure whether to show horizontal divide lines on the chart.</td>
</tr>

<tr>
<td>useEUTimeSeriesData</td>
<td> true / false </td>
<td> false </td>
<td>Used to configure to fetch chart data from IWT api service. This feature only works with the correct main ticker format. Please reference the mainTicker configuration for detail.</td>
</tr>

<tr>
<td>showYAxisTickLine</td>
<td> true / false </td>
<td> false </td>
<td>Used to configure whether to show the below the y-axis ticks.</td>
</tr>

<tr>
<td>yAxisWidth</td>
<td> Number. eg: 50 </td>
<td> 42 </td>
<td>Used to set y-axis width. This feature only works when yAxisPosition is set to outer.</td>
</tr>

<tr>
<td>yAxisMargin</td>
<td> Number. eg: 10 </td>
<td> 2 </td>
<td>Used to set the gap width between y-axis and chart. This feature only works when yAxisPosition is set to outer.</td>
</tr>

<tr>
<td>showMTickerRemoveBtn</td>
<td> true /false</td>
<td> true </td>
<td>show main ticker remove button</td>
</tr>

</tbody>
</table>


***This function will be return with an object after initializing the component that it will contains the following methods.***

### setProperty(name,value)

  this will provide information from the container.  
   
<table cellpadding=0 cellspacing=0 border="1">
<thead>
<tr class="header">
<th>name</th>
<th>value</th>
<th>value description</th>
</tr>
</thead>
<tbody>
<tr>
<td>parentSize</td>
<td>{width:800,height:600}</td>
<td>object with width and height</td>
</tr>
<tr>
<td>investment</td>
<td>{id:'126.1.IBM'}</td>
<td>realtime id</td>
</tr>
<tr>
<td>growthBaseValue</td>
<td>200</td>
<td>positive number</td>
</tr>
<tr>
<td>compareTickers</td>
<td>['126.1.AAPL']</td>
<td>An array of tickers</td>
</tr>
</tbody>
</table>

### destroy()

destroy the component.


### on(event, callback)

Register events and callbacks for the component.
All supported events:

<table cellpadding=0 cellspacing=0 border="1">
<thead>
<tr class="header">
	<th>event</th>
	<th>description</th>
</tr>
</thead>
<tbody>
<tr>
<td>load</td>
<td>will be triggered when component is loaded.</td>
</tr>
<tr>
<td>mainTickerDrawEnd</td>
<td>will be triggered after main ticker draw end.</td>
</tr>
<tr>
<td>click</td>
<td>will be triggered with click target infomation when menubar is clicked.</td>
</tr>
<tr>
<td>heightChange</td>
<td>will be triggered when component height is changed. It will only be fired when the fixHeight is setting to false.</td>
</tr>
<tr>
<td>addCompareTickers</td>
<td>will be triggered when compare tickers are added.</td>
</tr>
<td>close</td>
<td>will be triggered when close button is clicked</td>
</tr>
<tr>
<td>tickerRemoveBtnClicked</td>
<td>will be triggered when remove button of ticker legend is clicked.</td>
</tr>
<tr>
<td>configChange</td>
<td>will be triggered when configuration changed.</td>
</tr>
<tr>
<td>fullyRendered</td>
<td>will be triggered when component is fully rendered.</td>
</tr>
<tr>
<tr>
<td>error</td>
<td>will be triggered with the error code and message when session id is invalid or expired.</td>
</tr>
<tr>
</tbody>
</table>

Sample code:

    component.on('load',function(r){
    	console.log("component loaded.");
    });

### off(event)

unregister events for the component.


Sample code:

    component.off('load');