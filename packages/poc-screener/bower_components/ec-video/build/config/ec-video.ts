// startref
/// <reference path='../../.references.ts'/>
// endref
module EcVideo {
    export enum _Type { ecVideo }

    /**
     * @title Video
     */
    export interface EcVideo {
        /** Component type.
         * @default ecVideoModal
         * @options.hidden true
         * @type string
         * @readonly true
         */
        type: _Type;

        settings: Settings;

        labels: Labels;
        
    }
}
