// startref
/// <reference path='../../.references.ts'/>
// endref
module EcVideo {
    export interface Labels {
        ecVideoModalLoadText: AsterixCore.Label;
        html5VideoCompatibilityMessage: AsterixCore.Label;
        supportedMimeTypeNotFound: AsterixCore.Label;
    }
}
