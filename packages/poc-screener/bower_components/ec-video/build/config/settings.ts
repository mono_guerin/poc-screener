// startref
/// <reference path='../../.references.ts'/>
// endref
module EcVideo {
    export interface Settings extends AsterixNg.ComponentSettings {
        _templates: Templates;
        _parameters: {
            sources: AsterixNg.Parameter;
        };

        /** Specify if the video playback is a modal
         * @default false
         */
        modalPlayback: boolean;

        /** Specify modal video aspect ratio, for example, 0.5625 will be the 16 : 9 video ratio
         * @default 0.5625
         */
        modalVideoAspectRatio: number;

        /** The namespaceClass
         * @default ec-video
         */
        namespaceClass: string;

        /** Specify usage of browser native controls
         * @default true
         */
        showNativeControls: boolean;

        sources: any;
    }

    export interface Templates {
        /** The main template.
         * @format html
         * @default <div data-ng-if=\"::!settings.get('modalPlayback')\" mstar-include=\"templates.videoElement\"><\/div>\r\n<div data-ng-if=\"::settings.get('modalPlayback')\" class=\"{{::namespaceClass()}} {{::namespaceClass('--modal')}}\">\r\n    {{labels.get(\"ecVideoModalLoadText\")}}\r\n<\/div>\r\n
         */
        main: string;

        /** The video-element template.
         * @format html
         * @default <video class=\"{{ ::namespaceClass('__video') }}\" data-ng-class=\"{ '{{::namespaceClass('__video--modal')}}':settings.get('modalPlayback')}\" data-ng-attr-controls=\"{{::settings.get('showNativeControls')}}\">\r\n    <source data-ng-repeat=\"source in parameters.sources track by $index\" data-ng-src=\"{{getTrustedMediaUrl(source.url)}}\" type=\"{{source.type}}\" \/>\r\n    <track data-ng-repeat=\"track in ::settings.get('tracks') track by $index\" data-ng-src=\"{{::getTrustedMediaUrl(track.url)}}\" kind=\"{{::track.kind}}\" label=\"{{::track.label}}\" srclang=\"{{::track.srclang}}\" \/>\r\n    <div>{{labels.get(\"html5VideoCompatibilityMessage\")}}<\/div>\r\n<\/video>\r\n
         */
        videoElement: string;

        /** The video-modal template.
         * @format html
         * @default <div class=\"{{ ::namespaceClass('__video-container') }}\">\r\n    <div class=\"{{ ::namespaceClass('__close-button') }}\" data-ng-click=\"closeModal()\"><\/div>\r\n    <div class=\"{{ ::namespaceClass('__video-wrapper') }}\" data-ng-style=\"settings.get('modalPlayback') && {'padding-bottom':settings.get('modalVideoAspectRatio') * 100 + '%'}\">\r\n        <div data-ec-video-compile-after data-mstar-include=\"templates.videoElement\"><\/div>\r\n    <\/div>\r\n<\/div>\r\n<div class=\"{{::namespaceClass('__mask')}}\" data-ng-click=\"closeModal()\"><\/div>\r\n
         */
        videoModal: string;
    }
}
