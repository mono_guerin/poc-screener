define([], function() {
    require('es5-shim');
    require('jquery');
    require('asterix-core');
    require('markets-components-core');
    require('../build/markets-components-chartdatamanager.js');
});
