define(['morningstar'], function (morningstar) {
	'use strict';
	var marketscore = 'markets-components-core';
	var QSAPI = morningstar.components[marketscore];
	require('./js/sdk_ms_qs_chartdatamanager.js')(window, QSAPI);	
});