(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("morningstar"));
	else if(typeof define === 'function' && define.amd)
		define("markets-components-chartdatamanager", ["morningstar"], factory);
	else if(typeof exports === 'object')
		exports["markets-components-chartdatamanager"] = factory(require("morningstar"));
	else
		root["markets-components-chartdatamanager"] = factory(root["morningstar"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_1__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(1)], __WEBPACK_AMD_DEFINE_RESULT__ = function (morningstar) {
		'use strict';
		var marketscore = 'markets-components-core';
		var QSAPI = morningstar.components[marketscore];
		__webpack_require__(2)(window, QSAPI);	
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ }),
/* 1 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_1__;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*! v2.9.0 */ // Create a UMD mode
	(function(global, factory) {
		var __isAMD = ("function" === 'function' && __webpack_require__(3)),
			__isNode = (typeof module === "object" && typeof module.exports === 'object'),
			__isWeb = !__isNode;
	
		if (__isNode) {
	
			// For CommonJS and CommonJS-like environments
			// var QSAPI = require('QSAPI')(window);
			module.exports = factory;
	
		} else if (__isAMD) {
	
			// Register as a named AMD module.
			!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
		}
	
	
	})(typeof window !== 'undefined' ? window : this, function(window, noGlobal) {
		var strundefined = typeof undefined;
		var QSAPI = noGlobal;
	(function(QSAPI) {
	    'use strict';
	    var $ = QSAPI.$;
	    var Util = QSAPI.Util;
	    var QS_DateFmt = QSAPI.QS_DateFmt;
	    var QS_NumFmt = QSAPI.QS_NumFmt;
	    var QS_Style = {
	        redFontColor: '#ff0000',
	        redFontBG: '#FFC8C8',
	        greenFontColor: '#008000', //
	        greenFontBG: '#CDFFD7',
	        normalFontBG: '#ccc',
	        layouthelperBG: "#fff",
	        redFlashFontColor: '#ff0000',
	        greenFlashFontColor: '#008000',
	        normalFlashFontColor: '#121212',
	        chart: {
	            tickerColors: ["#293f6f", "#9b0033", "#e96610", "#f1ad02", "#e1b2c1", "#006065", "#809aba", "#9faa00", //#728fb2
	                "#e2e5b2", "#728fb2", "#fbe6b3", "#f2a370", "#b2cfd0", "#bec5d4", "#c5cc66", "#f8d1b7", "#c36685", "#c9cbcb"
	            ],
	            fillColors: { pChart: "#c9d2dd", vChart: "#2f4880", sChart: "#d9dfeb", minichartPchart: "#004DBC" }
	        },
	        flot: { grid: { color: "#545454", tickColor: "#dddddd" }, alpha: 0.7 }
	    };
	    Util.QS_Style = QS_Style;
	    Util.textKey = $.browser.msie ? "innerText" : "textContent";
	    Util.formatDoubledata = function(value, type, decimalpos, needformat, needunit, seperate, unmark, stayzs, isDefaultActualDecimal) {
	            var depos = "",
	                returnvalue = "";
	            if ($.trim(value).length == 0) return returnvalue;
	            if (typeof decimalpos == 'undefined') {
	                switch (type) {
	                    case "20": //forex  5
	                        depos = 5;
	                        break;
	                    case "1": //stock/fund
	                        depos = 2;
	                        break;
	                    case "10": //ETF
	                        depos = 2;
	                        break;
	                    default:
	                        depos = 2;
	                }
	            } else {
	                depos = decimalpos;
	            }
	            // if value smaller 0.0001, show the actual result
	            if (Math.abs(value) * 1000 < 1 && value != 0 && isDefaultActualDecimal) {
	                depos = QS_NumFmt.DefaultActualDecimal(value, decimalpos);
	            }
	            if (needformat && typeof QS_NumFmt != "undefined" && typeof QS_NumFmt.Fmt2 == 'function') {
	                if (unmark) {
	                    returnvalue = QS_NumFmt.FmtUnMark(value, needunit || false, depos, needformat || false, stayzs || false); //n, isC, d,isFormat
	                } else {
	                    returnvalue = QS_NumFmt.Fmt2(value, needunit || false, seperate || "", depos, false); //n, isC, sep, d,isFormat
	                }
	            } else {
	                returnvalue = parseFloat(value).toFixed(depos);
	            }
	            return returnvalue;
	        }
	        /*  Some functions provided to format the date or numbers*/
	    var d = new Date();
	    QSAPI.startTick = new Date(d.getFullYear(), d.getMonth(), d.getDate()).getTime();
	
	    Util.startTick = QSAPI.startTick;
	
	    Util.formatDate = function(date, format) {
	        if (arguments.length < 2 && !date.getTime) {
	            format = date;
	            date = new Date();
	        }
	        typeof format != 'string' && (format = 'YYYY-MM-DD');
	        var week = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	        return format.replace(/YYYY|YY|MM|DD|hh|mm|ss|WW|ww/g, function(a) {
	            switch (a) {
	                case "YYYY":
	                    return date.getFullYear();
	                case "YY":
	                    return (date.getFullYear() + "").slice(2);
	                case "MM":
	                    return date.getMonth() + 1;
	                case "DD":
	                    return date.getDate();
	                case "hh":
	                    return date.getHours();
	                case "mm":
	                    return date.getMinutes();
	                case "ss":
	                    return date.getSeconds();
	                case "WW":
	                    return week[date.getDay()];
	                case "ww":
	                    return week[date.getDay()].slice(0, 3);
	            }
	        });
	    };
	    Util.flashWithDirectional = function(el, v, c, isUp) { //v:current data, c:old data
	        var QS_Style = Util.QS_Style;
	        if (!v || !c) {
	            el.style.cssText = "background-color:" + QS_Style.normalFontBG + ";color:" + (isUp ? QS_Style.greenFlashFontColor : QS_Style.redFlashFontColor);
	        } else {
	            el.style.cssText = "background-color:" + ((v > c) ? QS_Style.greenFontBG : QS_Style.redFontBG) + ";color:" + (isUp ? QS_Style.greenFlashFontColor : QS_Style.redFlashFontColor);
	        }
	        setTimeout(function() {
	            el.style.cssText = "background-color:;color:" + (isUp ? QS_Style.greenFontColor : QS_Style.redFontColor);
	        }, 300);
	    };
	    Util.flash = function(el, v, c) { //v:current data, c:old data
	        var QS_Style = Util.QS_Style;
	        if (!v || !c) {
	            el.style.cssText = "background-color:" + QS_Style.normalFontBG + ";color:" + QS_Style.normalFlashFontColor;
	        } else {
	            el.style.cssText = "background-color:" + ((v > c) ? QS_Style.greenFontBG : QS_Style.redFontBG) + ";color:" + QS_Style.normalFlashFontColor;
	        }
	        setTimeout(function() {
	            el.style.cssText = "background-color:;color:;";
	        }, 300);
	    };
	    Util.isValid = function(ob, checkUndefined, checkNull, checkEmpty, checkZero) {
	        var o = void(0);
	        if (checkUndefined && ob == o) {
	            return false;
	        }
	        if (checkNull && ob == null) {
	            return false;
	        }
	        if (typeof ob == "string") {
	            if (checkEmpty && $.trim(ob) == '') {
	                return false;
	            }
	            if (checkZero && !isNaN(ob) && parseFloat(ob) == 0) {
	                return false;
	            }
	        }
	        return true;
	    };
	    Util.isFunction = function(f) {
	        return typeof f == 'function';
	    };
	    Util.getType = function(value) {
	        var oP = Object.prototype,
	            toString = oP.toString;
	        if (null === value) {
	            return 'null';
	        }
	        var type = typeof value;
	        if ('undefined' === type || 'string' === type) {
	            return type;
	        }
	        var typeString = toString.call(value);
	        switch (typeString) {
	            case '[object Array]':
	                return 'array';
	            case '[object Date]':
	                return 'date';
	            case '[object Boolean]':
	                return 'boolean';
	            case '[object Number]':
	                return 'number';
	            case '[object Function]':
	                return 'function';
	            case '[object RegExp]':
	                return 'regexp';
	            case '[object Object]':
	                if (undefined !== value.nodeType) {
	                    if (3 == value.nodeType) {
	                        return (/\S/).test(value.nodeValue) ? 'textnode' : 'whitespace';
	                    } else {
	                        return 'element';
	                    }
	                } else {
	                    return 'object';
	                }
	            default:
	                return 'unknow';
	        }
	    };
	    Util.Window = {
	        getHostAddress: function() {
	            return Util.formatString("{0}//{1}{2}", window.location.protocol, window.location.host, window.location.pathname);
	        },
	        getLocationArg: function() {
	            var urlCfg = {};
	            var __url = window.location.search;
	            if (__url != "" && __url.length > 0) {
	                __url = __url.substr(__url.indexOf("?") + 1);
	            }
	            var carr = __url.split("&");
	            for (var i = 0; i < carr.length; i++) {
	                var item = carr[i].split("=");
	                urlCfg[$.trim(item[0])] = $.trim(item[1]);
	            }
	            return urlCfg;
	        },
	        genURL: function(data) {
	            var sessionKey = QSAPI.getSessionId();
	            if (sessionKey) {
	                data["qs_wsid"] = sessionKey;
	            }
	            return Util.formatString("{0}?{1}", Util.Window.getHostAddress(), $.param(data));
	
	        },
	        genExternalURL: function(domain, data) {
	            return Util.formatString("{0}?{1}", domain, $.param(data));
	
	        },
	        openWindow: function(URL, title) {
	            //fix pdf can't be open
	            var win = window.open("", title, "toolbar=no,directories=no,status=no,scrollbars=yes,resizable=yes,menubar=no,width=1000,height=800");
	            win.location.href = URL;
	            win.focus();
	            return win;
	        }
	    };
	    Util.FDate = function(value, islong) { //format time to 05:30:34 AM,
	        var t = Util.startTick + parseInt(value) * 1000;
	        var dt = new Date(t);
	        return (dt == null) ? "--" : (islong ? QS_DateFmt.getLongTime(dt) : QS_DateFmt.getTimeString(dt));
	    };
	    Util.ConvertDateObjectToCurrentTZ = function(date, exchange, targetTimeZone) {
	        if (!this.isValid(date, true, true)) {
	            return date
	        } else {
	            var diff = Util.getDiffByTimezone(exchange, targetTimeZone);
	            return new Date(date.getTime() - diff * 60000);
	        }
	    };
	    Util.ConvertDateObjectToCurrentTimezone = function(date, originalTimeZone, targetTimeZone) {
	        if (!this.isValid(date, true, true)) {
	            return date;
	        } else {
	            var diff = QSAPI.DataManager.getTwoTimeZoneOffset(originalTimeZone, targetTimeZone);
	            return new Date(date.getTime() - diff * 60000);
	        }
	    };
	    Util.convertToDateObject = function(dateStr, time) { //yyyy-mm-dd, seconds
	        var dateA = dateStr.split("-");
	        d = new Date(dateA[0], parseFloat(dateA[1]) - 1, dateA[2]).getTime();
	        return new Date(d + parseInt(time) * 1000);
	    };
	    Util.convertToCurrentTimezone = function(component, gkey, targetTimezone, field, dateFormat, timeFormat, originalTimeZone) {
	        if (gkey) {
	            var exch = gkey.split(".")[0];
	            var date, datestr, timestr, marketData = QSAPI.Cache.getMarketDataByGkey(component, gkey);
	            if (field == "Time" || field == "Date") {
	                datestr = marketData["Date"];
	                timestr = marketData["Time"];
	            } else if (field == "TradeTime" || field == "TradeDate") {
	                datestr = marketData["TradeDate"];
	                timestr = marketData["TradeTime"];
	            } else if (field == "PrePostDate" || field == "PrePostTime") {
	                datestr = marketData["PrePostDate"];
	                timestr = marketData["PrePostTime"];
	            }
	            if (Util.isValid(datestr, true, true, true) && Util.isValid(timestr, true, true, true)) {
	                if (Util.isValid(targetTimezone, true, true, true)) {
	                    if (originalTimeZone) {
	                        date = Util.ConvertDateObjectToCurrentTimezone(Util.convertToDateObject(datestr, timestr), originalTimeZone, targetTimezone);
	                    } else {
	                        date = Util.ConvertDateObjectToCurrentTZ(Util.convertToDateObject(datestr, timestr), exch, targetTimezone);
	                    }
	                } else {
	                    date = Util.convertToDateObject(datestr, timestr);
	                }
	                if (!dateFormat) {
	                    dateFormat = "YYYY-MM-DD";
	                }
	                if (!timeFormat) {
	                    timeFormat = "hh:mm tt";
	                }
	                return [QS_DateFmt.formatDateString(date, dateFormat), QS_DateFmt.getTimeString(date, 0, timeFormat)];
	            }
	        }
	        return [undefined, undefined];
	    };
	
	    //common static
	    if (!Util.isValid(Util.GB_CURRENCY_DETAIL, true, true, true)) {
	        Util.GB_CURRENCY_DETAIL = {
	            USD: {
	                "shortName": "US",
	                "symbol": "$"
	            },
	            CAN: {
	                "shortName": "CA",
	                "symbol": "$"
	            },
	            CAD: {
	                "shortName": "CA",
	                "symbol": "$"
	            },
	            CNY: {
	                "shortName": "CN",
	                "symbol": "￥"
	            },
	            GBP: {
	                "shortName": "GB",
	                "symbol": "￡"
	            },
	            GBX: {
	                "shortName": "GBX",
	                "symbol": "p",
	                "position": "right"
	            },
	            EUR: {
	                "shortName": "EUR",
	                "symbol": "€",
	                "position": "right"
	            }
	        };
	    }
	    //sort fix chrome bugs. 
	    Util.sort = function(arr, fn) {
	        if (arr.map) {
	            var result, map = arr.map(function(i, j) { return { i: i, j: j }; });
	            return Array.prototype.sort.call(map, function(a, b) {
	                result = fn(a.i, b.i);
	                if (result === 0) {
	                    return a.j - b.j;
	                }
	                return result;
	            }).map(function(i) { return i.i; });
	        } else {
	            return Array.prototype.sort.call(arr, function(a, b) { return fn(a, b); });
	        }
	    };
	
	    Util.setStyle = function(cfg) {
	        $.extend(true, Util.QS_Style, cfg);
	    };
	    Util.mapDataId = function(dataId) {
	        var _convertCols = {
	            "st168": "High52W",
	            "st169": "Low52W",
	            "st109": "HighDate52W",
	            "st106": "LowDate52W"
	        };
	        return _convertCols[dataId] || dataId;
	    };
	    Util.filterInputValue = function(value) {
	        return value.replace(/<script>/ig, '').replace(/<\/script>/ig, '');
	    };
	    Util.uniqueArray = function(array) {
	        var res = [];
	        var json = {};
	        for (var i = 0, l = array.length, a; i < l; i++) {
	            a = array[i]
	            if (!json[a]) {
	                res.push(a);
	                json[a] = true;
	            }
	        }
	        return res;
	    };
	    Util.formatDateToString = function(str) {
	        if (!str) return '';
	        var t = [];
	        t.push(str.slice(0, 4)), t.push(str.slice(4, 6)), t.push(str.slice(6, 8));
	        return t.join("-");
	    };
	    //Compatible with version <2.1    
	    $.extend(QSAPI.Util, window.Util);
	    //window.Util = QSAPI.Util;
	
	    //var QS_Style=QSAPI.Util.QS_Style;
	    //window.GB_CURRENCY_DETAIL=QSAPI.Util.GB_CURRENCY_DETAIL;
	    //var startTick=QSAPI.Util.startTick;
	})(QSAPI);
	/**
	 * @author Jacye.Ouyang
	 */
	(function(QSAPI) {
	    'use strict';
	    var $ = QSAPI.$;
	    var Util = QSAPI.Util;
	    if (!QSAPI.Widget.Chart) {
	        QSAPI.Widget.Chart = {};
	    }
	    var QS_DateFmt = QSAPI.QS_DateFmt;
	    var QS_NumFmt = QSAPI.QS_NumFmt;
	    QSAPI.Widget.Chart.Util = {
	        formatDate: function(date) { //formate date to yyyy-m-d
	            if (date == null) {
	                return null;
	            }
	            return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
	        },
	        formatDateYMD: function(date, line) {
	            if (date == null) {
	                return null;
	            }
	            var l = line;
	            if (typeof(line) == "undefined") {
	                l = "-";
	            }
	            return date.getFullYear() + l + ((date.getMonth() + 1) <= 9 ? "0" : "") + (date.getMonth() + 1) + l + (date.getDate() <= 9 ? "0" : "") + date.getDate();
	        },
	        formatDateMDY: function(date, line) { //formate date to mm-dd-yyyy
	            if (date == null) {
	                return null;
	            }
	            var l = line;
	            if (typeof(line) == "undefined") {
	                l = "-";
	            }
	            return ((date.getMonth() + 1) <= 9 ? "0" : "") + (date.getMonth() + 1) + l + (date.getDate() <= 9 ? "0" : "") + date.getDate() + l + date.getFullYear();
	        },
	        formatDateDMY: function(date, line) { //formate date to dd-mm-yyyy
	            if (date == null) {
	                return null;
	            }
	            var l = line;
	            if (typeof(line) == "undefined") {
	                l = "-";
	            }
	            var d = (date.getDate() <= 9 ? "0" : "") + date.getDate();
	            var m = ((date.getMonth() + 1) <= 9 ? "0" : "") + (date.getMonth() + 1);
	            var y = date.getFullYear();
	            return [d, m, y].join(l);
	        },
	        formatDateToVBDate: function(date) { //format date to mm/dd/yyyy.
	            if (date == null) {
	                return null;
	            }
	            return (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
	        },
	        convertToDate: function(d) { // convert yy-mm-dd to date format.
	            if (d == null || d == "") {
	                return null;
	            }
	            var f = d.split('-');
	            if (f.length > 1) {
	                var f1 = f[0],
	                    f2 = parseInt(f[1], 10),
	                    f3 = parseInt(f[2], 10);
	                return new Date(f1, f2 - 1, f3);
	            } else {
	                var f1 = parseInt(d.substr(0, 4), 10),
	                    f2 = parseInt(d.substr(4, 2), 10),
	                    f3 = parseInt(d.substr(6, 2), 10);
	                return new Date(f1, f2 - 1, f3);
	            }
	
	        },
	        convertYMDToDate: function(d) { // convert yymmdd to date format.
	            if (d == null || d == "") {
	                return null;
	            }
	            var y = parseInt(d.substr(0, 4)),
	                m = parseInt(d.substr(4, 2)) - 1,
	                d = parseInt(d.substr(6, 2));
	            return new Date(y, m, d);
	        },
	        convertToVBDate: function(d) { // convert mm/dd/yyyy to date format.
	            if (d == null || d == "") {
	                return null;
	            }
	            var f = d.split('/');
	            var f1 = f[2],
	                f2 = parseInt(f[0], 10),
	                f3 = parseInt(f[1], 10);
	            return new Date(f1, f2 - 1, f3);
	        },
	        compareDate: function(from, to) { // comapair the date (mm/dd/yyyy) if equal, return true.
	            var f = from.split('/'),
	                t = to.split('/'),
	                f1 = f[2],
	                f2 = parseInt(f[0], 10),
	                f3 = parseInt(f[1], 10);
	            var t1 = t[2],
	                t2 = parseInt(t[0], 10),
	                t3 = parseInt(t[1], 10);
	            return f1 < t1 ? true : (f1 == t1 ? (f2 < t2 ? true : (f2 == t2 ? (f3 <= t3 ? true : false) : false)) : false);
	        },
	        compareDateByday: function(from, to) { // is the same day.
	            if (!from || !to) return false;
	            var s = new Date(from.getFullYear(), from.getMonth(), from.getDate());
	            var e = new Date(to.getFullYear(), to.getMonth(), to.getDate());
	            return e.getTime() == s.getTime();
	        },
	        isVbDate: function(str) { //check the date format mm-dd-yyyy or mm/dd/yyy or mm.dd.yyyy.
	            var reg = /^(\d{1,2})(-|\/)(\d{1,2})\2(\d{4})$/;
	            var result = str.match(reg);
	            if (result == null) return false;
	            var y, m, d;
	            y = result[4]; //year number
	            m = parseInt(result[1], 10); //month number
	            d = parseInt(result[3], 10); //day 
	            if ((m < 1) || (m > 12) || (d < 1) || (d > 31)) return false;
	            if (((m == 4) || (m == 6) || (m == 9) || (m == 11)) && (d > 30)) return false;
	            if ((y % 4) == 0) {
	                if ((m == 2) && (d > 29)) return false;
	            } else {
	                if ((m == 2) && (d > 28)) return false;
	            }
	            return true;
	        },
	        trimDate: function(date) {
	            return new Date(date.getFullYear(), date.getMonth(), date.getDate());
	        },
	        toDateIndex: function(dt) { // days from 1900-1-1; 
	            if (dt == null) {
	                return 0;
	            }
	            var st = new Date(1900, 0, 1).getTime();
	            var end = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate()).getTime();
	            return Math.round((end - st) / 86400000); //(24 * 60 * 60 * 1000)=86400000
	        },
	        toDateFromIndex: function(index) { // must use new Date(1900,0,1) Notice Zone difference.
	            var date = new Date(1900, 0, 1);
	            date.setDate(index + 1);
	            return date; //new Date(index * 86400000 + this.millisecond1900);//(24 * 60 * 60 * 1000)=86400000  -2209017600000 =
	        },
	        toDateFromIndexOrTick: function(v, type) {
	            if (type == 0) {
	                return new Date(v);
	            } else {
	                return this.toDateFromIndex(v);
	            }
	        },
	        formatPChartXLabel: function(tick, type, isIntradayTimSyle, offset) {
	            if (tick == null) return "";
	            var t = parseInt(tick, 10),
	                dt;
	            if (type == 1 || isIntradayTimSyle) { // one day chart
	                dt = new Date(t);
	                return dt == null ? "" : QS_DateFmt.getTimeString(dt, offset);
	            } else if (type == 2 || type == 0) {
	                dt = new Date(t);
	                return dt == null ? "" : this.formatDateToWeekMMDD(dt);
	            } else {
	                dt = this.toDateFromIndex(t);
	                return dt == null ? "" : QS_DateFmt.getDateString(dt);
	            }
	        },
	        formatDateToWeekMMDD: function(d) { // www mm dd.
	            return QS_DateFmt.getWeekDay(d) + " " + QS_DateFmt.getMonthDate(d);
	        },
	        formatSChartXLabel: function(tick, freq) {
	            if (tick == null) return "";
	            var t = parseInt(tick, 10);
	            var dt = this.toDateFromIndex(t);
	            if (freq == "minute") {
	                return dt == null ? "" : QS_DateFmt.Mo[dt.getMonth()];
	            } else if (freq == "d") {
	                return dt == null ? "" : dt.getFullYear(); //DTFormat.ToShortYearMonth(dt);
	            } else {
	                return dt == null ? "" : dt.getFullYear().toString().substr(2, 2);
	            }
	        },
	        formatLegendDate: function(tick, type, offset) { // type is frequency type.
	            if (tick == null) return "";
	            var t = parseInt(tick, 10),
	                dt;
	            if (type != "d" && type != "w" && type != "m") {
	                dt = new Date(t);
	                return (dt == null) ? "" : QS_DateFmt.getDatetimeString(dt, offset);
	            } else {
	                dt = this.toDateFromIndex(t);
	                return (dt == null) ? " " : QS_DateFmt.getDateString(dt);
	            }
	        },
	        formateNumber: function(v, precision) {
	            return v == null || isNaN(v) ? "--" : QS_NumFmt.Fmt2(v, true, "", precision);
	        },
	        formatMDYDate: function(dt) {
	            return (dt == null) ? " " : QS_DateFmt.getDateString(dt); //QS_DateFmt.Mo[dt.getMonth()]+" "+(dt.getDate()<10?"0"+dt.getDate():dt.getDate())+", "+dt.getFullYear();
	        },
	        toHourMinutes: function(minutes) {
	            if (isNaN(minutes)) {
	                return null;
	            }
	            return (this.prefixZero(Math.floor(minutes / 60)) + ":" + this.prefixZero(minutes % 60));
	        },
	        timeToMinutes: function(time) { //09:30 to 570
	            if (typeof time !== 'string' || time.indexOf(':') < -1) {
	                return null;
	            }
	            return parseInt(time.split(':')[0]) * 60 + parseInt(time.split(':')[1]);
	        },
	        formatDateAsInteger: function(date, time) {
	            var dateArr = [];
	            var timeArr = [];
	            var dateStr = '';
	            var timeStr = '';
	            if (!date && !time) {
	                return 0;
	            }
	            if (date) {
	                dateArr = date.split('-');
	                dateStr = dateArr[2] + dateArr[1] + dateArr[0];
	            }
	            if (time) {
	                timeArr = time.split(':');
	                timeStr = timeArr[0] + timeArr[1];
	            }
	            return parseInt(dateStr + timeStr, 10);
	        },
	        prefixZero: function(num) {
	            if (num < 10) {
	                return '0' + num;
	            }
	            return num;
	        }
	
	    };
	})(QSAPI);
	/**
	 * @author Yuntao Zhou
	 */
	/*dependency: jquery-1.3.2.min.js*/
	/*dependency: jquery.json.min.js*/
	/*dependency: indicatorCalculattion.js*/
	/*dependency:chartUtil.js*/
	(function (QSAPI) {
	    'use strict';
	    var $ = QSAPI.$;
	    var Util = QSAPI.Util;
	    if (!QSAPI.DataManager) {
	        QSAPI.DataManager = {};
	    }
	    if (!QSAPI.Widget) {
	        QSAPI.Widget = {};
	    }
	    if (!QSAPI.Widget.Chart) {
	        QSAPI.Widget.Chart = {};
	    }
	    QSAPI.Widget.Chart.DataPoints = {
	        PRICE: "LastPrice",
	        DIVIDEND: "DD001",
	        TSDIVIDEND: 'TSD',
	        SPLITFROM: "HSB04",
	        SPLITTO: "HSB05",
	        NAV: "HS538",
	        TSPRICE: "TSP",
	        TSRETURN: "TSC",
	        EARNINGS: "HS030",
	        PE: "HS0A2",
	        PS: "HS0A5",
	        PB: "HS0A0",
	        PC: "HS0A1",
	        FAIRVALUE: "HS987",
	        SINTEREST: "STA0C",
	        REPS: "ST263",
	        MRETURNINDEX: "HS793",
	        POSTTAX: "HS856",
	        DRIndex: "117",
	        TOTALRETURN: "FACT830",
	        MARKETVALUE: "FACT964",
	        DATAAPIV2_NAV: "FACT2685",
	        DATAAPIV2_PRICE: "FACT2260",
	        FUNDSIZE: "FACT853",
	        QUALITATIVEFAIRVALUE: "8499",
	        QUANTITATIVEFAIRVALUE: "29301",
	        DISCOUNTCUMFAIR: "35450",
	        DAILYCUMFAIRNAV: "28456",
	        CURRENCY: "3010",
	        PREMIUMDISCOUNT: "3242",
	        SEMIANNUALEPS: "TS247",
	        ANNUALEPS: "TS248",
	        AJUSTCLOSE: 'HS377',
	        SHAREHOLDERSNUMBER: '3250',
	
	        parse: function (key, value, type, growthType) {
	            var _dp;
	            switch (key) {
	                case 'p':
	                    if (/fact_nav/.test(value)) {
	                        _dp = this.DATAAPIV2_NAV;
	                    } else if (/fact_price/.test(value)) {
	                        _dp = this.DATAAPIV2_PRICE;
	                    } else if (value === "return_k_tsp" || value === "return_tsp") {
	                        _dp = this.TSPRICE;
	                    } else if (/^return_nav/.test(value)) {
	                        _dp = this.NAV;
	                    } else if (/^return/.test(value) && !type) {
	                        if (growthType === "FundCategory") {
	                            _dp = this.DRIndex;
	                        } else {
	                            _dp = this.MRETURNINDEX;
	                        }
	                    } else if (value == "nav") {
	                        _dp = this.NAV;
	                    } else if (value == "postTax") {
	                        _dp = this.POSTTAX;
	                    } else if (value == "total_return") {
	                        _dp = this.TOTALRETURN;
	                    } else if (value == "market_value") {
	                        _dp = this.MARKETVALUE;
	                    } else if (value == "p_nav") {
	                        _dp = this.NAV;
	                    } else if (value == "tsp") {
	                        _dp = this.TSPRICE;
	                    } else if (value == "tsc" || value == "k_tsc") {
	                        _dp = this.TSRETURN;
	                    } else {
	                        _dp = this.PRICE;
	                    }
	                    break;
	                case 'growthPrice':
	                    if (value === 'tsp') {
	                        _dp = this.TSPRICE;
	                    } else if (/\.*nav/.test(value)) {
	                        _dp = this.NAV;
	                    } else {
	                        _dp = this.PRICE;
	                    }
	                    break;
	
	                case 'v':
	                    _dp = this.PRICE;
	                    break;
	                case "ds":
	                    var dps = [];
	                    if (value["d"]) {
	                        dps.push(this.DIVIDEND);
	                    }
	                    if (value["s"]) {
	                        dps.push(this.SPLITFROM + ":" + this.SPLITTO);
	                    }
	                    if (value["tsd"]) {
	                        dps.push(this.TSDIVIDEND);
	                    }
	                    _dp = dps.join(",")
	                    break;
	                case 'ap':
	                    _dp = this.AJUSTCLOSE;
	                    break;
	                case 'e':
	                    if (value == "se") {
	                        _dp = this.SEMIANNUALEPS + "," + this.ANNUALEPS;
	                    } else {
	                        _dp = this.EARNINGS;
	                    }
	
	                    break;
	                case 'returnIndex':
	                    _dp = this.RETURNINDEX;
	                    break;
	                case 'indicators':
	                    _dp = "";
	                    break;
	                case 'PE':
	                    _dp = this.PE;
	                    break;
	                case 'PS':
	                    _dp = this.PS;
	                    break;
	                case 'PB':
	                    _dp = this.PB;
	                    break;
	                case 'PC':
	                    _dp = this.PC;
	                    break;
	                case 'FairValue':
	                    _dp = this.FAIRVALUE;
	                    break;
	                case 'REPS':
	                    _dp = this.REPS;
	                    break;
	                case 'SInterest':
	                    _dp = this.SINTEREST;
	                    break;
	                case 'MReturnIndex':
	                    _dp = this.MRETURNINDEX;
	                    break;
	                case 'QuantitativeFairValue':
	                    _dp = this.QUANTITATIVEFAIRVALUE;
	                    break;
	                case 'DiscountCumFair':
	                    _dp = this.DISCOUNTCUMFAIR;
	                    break;
	                case 'DailyCumFairNav':
	                    _dp = this.DAILYCUMFAIRNAV;
	                    break;
	                case 'PremiumDiscount':
	                    _dp = this.PREMIUMDISCOUNT;
	                    break;
	                case 'FundSize':
	                    _dp = this.FUNDSIZE;
	                    break;
	                case 'ShareholdersNumber':
	                    _dp = this.SHAREHOLDERSNUMBER;
	                    break;
	            }
	            return _dp;
	        },
	        needFilter: function (indicatorName) {
	            return $.inArray(indicatorName, ['VBP', 'PrevClose', 'RDividend', 'VAcc', 'OBV']) > -1;
	        },
	        isPRICE: function (dp) {
	            return dp == this.PRICE;
	        },
	        isAjustClosePrice: function (dp) {
	            return dp === this.AJUSTCLOSE;
	        },
	        isFundamental: function (dp) {
	            return /*dp == this.PE || dp == this.PS || dp == this.PB || dp == this.PC || dp == this.FAIRVALUE ||*/ dp == this.REPS || dp == this.SINTEREST; // || dp == this.MRETURNINDEX;
	        },
	        isDividend: function (dp) {
	            return dp == this.DIVIDEND;
	        },
	        isTSDividend: function (dp) {
	            return dp == this.TSDIVIDEND;
	        },
	        isTSReturn: function (dp) {
	            return dp == this.TSRETURN;
	        },
	        isTSPrice: function (dp) {
	            return dp == this.TSPRICE;
	        },
	        isNeedClearCache: function (dp) {
	            return dp == this.TSRETURN || dp == this.TSDIVIDEND;
	        },
	        isSplit: function (dp) {
	            return dp == this.SPLITFROM || dp == this.SPLITTO || dp == (this.SPLITFROM + ":" + this.SPLITTO);
	        },
	        isEarnings: function (dp) {
	            return (dp == this.EARNINGS || dp == this.SEMIANNUALEPS || dp == this.ANNUALEPS || dp == (this.SEMIANNUALEPS + "," + this.ANNUALEPS));
	        },
	        isAnnualEPS: function (dp) {
	            return dp == this.ANNUALEPS;
	        },
	        isSemiAnnualEPS: function (dp) {
	            return dp == this.SEMIANNUALEPS;
	        },
	        isMReturnIndex: function (dp) {
	            return dp == this.MRETURNINDEX;
	        },
	        //Need delete after we fix the issue: mutilple version in a some application
	        isITFund: function (dp) {
	            return dp === this.TSPRICE || dp === this.ITPERFORMANCE || dp === this.TSRETURN || dp === this.TSDIVIDEND;
	        },
	        isDataAPIV1DataPoint: function (dp) {
	            return dp === this.DRIndex || dp === this.QUANTITATIVEFAIRVALUE || dp === this.DAILYCUMFAIRNAV || dp === this.DISCOUNTCUMFAIR;
	        },
	        isDataAPIV2DataPoint: function (dp) {
	            return dp === this.TOTALRETURN || dp === this.MARKETVALUE || dp === this.DATAAPIV2_NAV || dp === this.DATAAPIV2_PRICE || dp === this.FUNDSIZE;
	        },
	        isRDBFundamentalDataPoint: function (dp) {
	            return dp === this.PREMIUMDISCOUNT || dp === this.SHAREHOLDERSNUMBER;
	        },
	        needCurrency: function (dp) {
	            var dataPoints = QSAPI.Widget.Chart.DataPoints;
	            var datapointArr = [dataPoints.DISCOUNTCUMFAIR, dataPoints.DAILYCUMFAIRNAV, dataPoints.QUANTITATIVEFAIRVALUE];
	            if ($.inArray(dp, datapointArr) > -1) {
	                return true;
	            } else {
	                return false;
	            }
	        }
	    };
	    QSAPI.Widget.Chart.DataManager = function () {
	        this.dataUrls = {
	            chart: "ra/uniqueChartData",
	            chartExtend: "ra/{0}ChartData",
	            chartExport: "ra/export",
	            chartDrawings: "drawmanager.jsp",
	            getFODTimeseries: function () {
	                return QSAPI.urlFODTimeseries();
	            },
	            IWTTimeseries: function () {
	                return QSAPI.urlIWTTimeSeriesPath();
	            }
	        };
	        this.needUpdateIntradyData = true;
	        this.maxCacheSize = 30;
	        this.crossDomain = true;
	        this.dataDomain = QSAPI.getQSPath();
	        this.subscribeID = "QSAPI.Widget.Chart.DataManager";
	        this._timeout = 50000;
	        this._cache = {};
	        this._registedCharts = {};
	        this._cacheSize = 0;
	        this.CHARTDATATYPE = {
	            TS_WITH_PID_FIXED_FREQUENCY: -2,
	            TS_OHLCV_PID: 1,
	            TS_WITH_PID: 2,
	            FUNDAMENTAL_DIRECT: 3,
	            FUNDAMENTAL_EQUATY_DATA: 4,
	            REALTIME_INTRADAY_LAST: 5,
	            REALTIME_INTRADAY_RANGE: 6,
	            REALTIME_HISTORICAL: 7,
	            IT_HISTORICAL_FUND: 1610,
	            IT_HISTORICAL_STOCK: 1611,
	            IT_HISTORICAL_Cumulative: 162,
	            IT_HISTORICAL_DIVIDEND: 163,
	            DATAAPI_SID: 17,
	            DATAAPIV2_PID: 18,
	            FUNDAMENTAL_RDB: 19
	        };
	        this.STATUS = {
	            START: 0,
	            PENDING: 1
	        };
	        this._cacheRequest = {};
	        //this._XIsubUSExchange = [9,11,16,17,22,25,29,30,33,41,56];  // indexes exchange need get intraday chart from sub exchange.
	        this._currentDate = new Date();
	        this._dates = { //default configuration for start date of each frequency 
	            1: new Date(this._currentDate.getFullYear(), this._currentDate.getMonth(), this._currentDate.getDate() - 6), // start date for cached data.
	            5: new Date(this._currentDate.getFullYear(), this._currentDate.getMonth() - 1, this._currentDate.getDate() - 2), // 5 minutes
	            10: new Date(this._currentDate.getFullYear(), this._currentDate.getMonth() - 3, this._currentDate.getDate() - 10), // 10 minutes
	            15: new Date(this._currentDate.getFullYear(), this._currentDate.getMonth() - 3, this._currentDate.getDate() - 10), // 15 minutes
	            d: new Date(this._currentDate.getFullYear() - 10, 0, 1),
	            w: new Date(this._currentDate.getFullYear() - 20, 0, 1),
	            m: new Date(1900, 0, 1),
	            lastTradeDate: new Date()
	        };
	
	        this.updateChartInterval = "minute";
	        this.saveDrawings = false; // if need to store the data into database
	        this.MINUTEMSECONDS = 1000 * 60;
	        this._volumeDividend = 1000000;
	        this._subscribeTickers = {};
	    };
	
	    QSAPI.Widget.Chart.DataManager.prototype = {
	        /*start define getter and setter method*/
	        autoRefresh: function (value) {
	            if (!arguments.length) {
	                return this.needUpdateIntradyData;
	            }
	            this.needUpdateIntradyData = value;
	        },
	        updateInterval: function (value) {
	            if (!arguments.length) {
	                return this.updateChartInterval;
	            }
	            this.updateChartInterval = value;
	        },
	        saveDrawingsStatus: function (value) {
	            if (!arguments.length) {
	                return this.saveDrawings;
	            }
	            this.saveDrawings = (value === true ? true : false);
	        },
	        dataDomainValue: function (value) {
	            if (!arguments.length) {
	                return this.dataDomain;
	            }
	            this.dataDomain = value;
	        },
	        volumeDividend: function (value) {
	            if (!arguments.length) {
	                return this._volumeDividend;
	            }
	            this._volumeDividend = value;
	        },
	        /*end define getter and setter method*/
	
	        isContains: function (tickerObj) {
	            return !!this._getTickerCache(tickerObj);
	        },
	        isIWTChartDataType: function (cdt) {
	            cdt = Number(cdt);
	            return cdt === this.CHARTDATATYPE.IT_HISTORICAL_FUND || cdt === this.CHARTDATATYPE.IT_HISTORICAL_STOCK || cdt === this.CHARTDATATYPE.IT_HISTORICAL_Cumulative || cdt === this.CHARTDATATYPE.IT_HISTORICAL_DIVIDEND;
	        },
	        registerSymbol: function (chartObj, tickers) {
	            var forceDelay = QSAPI.Util.getValidForceDelay(chartObj.forceDelay);
	            var self = this;
	            var tickerObjects = this._toArray(tickers);
	            //var _subscribeTickerList = [];
	            for (var i = 0, tickerObj, l = tickerObjects.length; i < l; i++) {
	                tickerObj = tickerObjects[i];
	                if (!this._validateTickerObject(tickerObj)) {
	                    continue;
	                }
	                var key = this._getCacheKey(tickerObj);
	
	                if (!this._cache[key]) {
	                    this._cache[key] = new QSAPI.Widget.Chart.TickerCache(key);
	                    this._cache[key].tickerObject = tickerObj;
	                    this._cache[key].baseCurrency = tickerObj && tickerObj.currency;
	                    this._cache[key].volumeDividend(this._volumeDividend);
	                    this._registedCharts[key] = {
	                        charts: [chartObj],
	                        subscribable: this._getRTDTicker(tickerObj) && this._isNeedSubscribe(tickerObj, chartObj)
	                    };
	                    this._cacheSize++;
	                    /*if(this._getRTDTicker(tickerObj)&&this._isNeedSubscribe(tickerObj,chartObj)){
	                        _subscribeTickerList.push(tickerObj);
	                    }*/
	                    if (typeof chartObj._isPriceEditable == "function" && chartObj._isPriceEditable()) { //get edited price/volume data from DB
	                        this.getUpdatedPricePoints(tickerObj);
	                    }
	                } else {
	                    this._registedCharts[key] = this._registedCharts[key] || {};
	                    this._registedCharts[key]['charts'] = this._registedCharts[key]['charts'] || [];
	                    var charts = this._registedCharts[key]['charts'];
	                    var flag = false;
	                    for (var c = 0, cl = charts.length; c < cl; c++) {
	                        if (charts[c].id == chartObj.id) { //means has been registered.
	                            flag = true;
	                            break;
	                        }
	                    }
	                    if (!flag) {
	                        this._registedCharts[key]['charts'].push(chartObj);
	                    }
	                }
	                var forceDelays = this._cache[key].forceDelay();
	                if (this._getRTDTicker(tickerObj) && this._isNeedSubscribe(tickerObj, chartObj) && !forceDelays[forceDelay]) {
	                    this._cache[key].forceDelay(forceDelay);
	                    //_subscribeTickerList.push(tickerObj);
	                }
	            }
	            /*if (_subscribeTickerList.length > 0 && this.needUpdateIntradyData) {
	                var taskId = this._getTaskId(forceDelay);
	                QSAPI.Task.register({
	                    taskId: taskId,
	                    period: 24 * 60 * this.MINUTEMSECONDS,
	                    forceDelay: forceDelay,
	                    batchQuerySize:this.maxCacheSize,
	                    query:function(tickers, _taskId){
	                        self._query(tickers, _taskId);
	                    }
	                });
	                QSAPI.Task.setParams(taskId, this._getChannels(_subscribeTickerList), this);
	            }*/
	        },
	        unregisterSymbol: function (chartObj, tickers) {
	            var forceDelay = QSAPI.Util.getValidForceDelay(chartObj.forceDelay);
	            //var _unsubscribeTickerList=[];
	            var tickerObjects = this._toArray(tickers);
	            for (var i = 0, tickerObj, l = tickerObjects.length; i < l; i++) {
	                tickerObj = tickerObjects[i];
	                if (!this._validateTickerObject(tickerObj)) {
	                    continue;
	                }
	                var key = this._getCacheKey(tickerObj);
	                if (!this._cache[key]) {
	                    continue;
	                }
	                var forceDelays = this._cache[key].forceDelay();
	                if (this._getRTDTicker(tickerObj) && this._isNeedSubscribe(tickerObj, chartObj) && forceDelays[forceDelay]) {
	                    this._cache[key].forceDelay(forceDelay, false);
	                    //_unsubscribeTickerList.push(tickerObj);
	                }
	                var charts = (this._registedCharts[key] || {})['charts'] || [];
	                if (charts.length > 0) {
	                    for (var j = 0; j < charts.length; j++) {
	                        if (charts[j].id == chartObj.id) {
	                            QSAPI.Task.unregister(this._getTaskId(chartObj.id));
	                            charts.splice(j, 1);
	                        }
	                    }
	                    if (charts.length == 0 || this._cacheSize > this.maxCacheSize) {
	                        delete this._cache[key];
	                        delete this._registedCharts[key];
	                        this._cacheSize--;
	                    }
	                }
	            }
	            /*if (_unsubscribeTickerList.length > 0 && this.needUpdateIntradyData) {
	                console.log(_unsubscribeTickerList);
	                QSAPI.Task.removeParams(this._getTaskId(chartObj.id), this._getChannels(_unsubscribeTickerList), this);
	            }*/
	        },
	        /**
	         *infoList:{symbol:"OP00002RH",tickerObject:tickerObject,fields:["PE","PS","PB","PC","FairValue"]}
	         *freq:'d','w','m'
	         *startIndex:41910
	         *endIndex:41985,
	         *successCallback:function(result){}
	         */
	        getFundamentalData: function (infoList, freq, startIndex, endIndex, successCallback) {
	            var fields = infoList.fields || [];
	            if (freq != "d" && freq != "w" && freq != "m") { // if minutely data .
	                if (typeof (successCallback) == "function") { // excute success callback.
	                    var rd = {};
	                    for (var i = 0, l = fields.length; i < l; i++) {
	                        rd[fields[i]] = {
	                            data: [],
	                            spos: null
	                        };
	                    }
	                    successCallback(rd);
	                }
	            } else {
	                var datapoints = {};
	                for (var i = 0, l = fields.length; i < l; i++) {
	                    if (fields[i] == "MReturnIndex") {
	                        datapoints[fields[i]] = "return";
	                    } else {
	                        datapoints[fields[i]] = "fundamental";
	                    }
	                }
	                this.getHistoricalData([{
	                    symbol: infoList.symbol,
	                    tickerObject: infoList.tickerObject,
	                    datapoints: datapoints
	                }], freq, startIndex, endIndex, function (result) {
	                    if (typeof (successCallback) == "function") {
	                        successCallback(result[0])
	                    }
	                })
	            }
	        },
	        /**
	         *infoList:[{symbol:"OP00002RH",tickerObject:tickerObject,days:5,type:"price",datapoints:{p:"",v:"volume"},indicators:{SMA:[],EMA:[]}]
	         *freq:'d','w','m'
	         *startIndex:41910
	         *endIndex:41985,
	         *successCallback:function(result){}
	         */
	        _needGetExtraData: function (infoList) {
	            for (var i = 0; i < infoList.length; i++) {
	                var indicators = infoList[i].datapoints.indicators;
	                if (indicators) {
	                    for (var ind = 0; ind < indicators.length; ind++) {
	                        if (!QSAPI.Widget.Chart.DataPoints.needFilter(indicators[ind].name)) {
	                            return true;
	                        }
	                    }
	                }
	            }
	            return false;
	        },
	        _getExtraDataForIndicators: function (freq) {
	            var days = 0;
	            switch (freq) {
	                case 'd':
	                    days = 100;
	                    break;
	                case 'w':
	                    days = 365;
	                    break;
	                case 'm':
	                    days = 365 * 2;
	                    break;
	            }
	            return days;
	        },
	        getHistoricalData: function (infoList, freq, startIndex, endIndex, successCallback, keepOriginIndex, alignMainTicker) {
	            keepOriginIndex = keepOriginIndex === true ? true : false;
	            var self = this;
	            var rtdTickers = [];
	            var _util = QSAPI.Widget.Chart.Util;
	            var sIndex = keepOriginIndex ? startIndex : _util.toDateIndex(this._dates[freq]);
	            var eIndex = keepOriginIndex ? endIndex : _util.toDateIndex(this._dates["lastTradeDate"]);
	            if (keepOriginIndex && this._needGetExtraData(infoList)) {
	                sIndex = startIndex - this._getExtraDataForIndicators(freq);
	                if (sIndex < 0) {
	                    sIndex = 0;
	                }
	            }
	            var rInfoListMap = this._checkDataReady(infoList, freq, sIndex, eIndex);
	            if (!$.isEmptyObject(rInfoListMap)) {
	                var _queryList = [];
	                for (var key in rInfoListMap) {
	                    var type = key.split('_')[0];
	                    _queryList.push(this._queryRemoteData(type, rInfoListMap[key], freq, sIndex, eIndex));
	                }
	                $.when.apply($, _queryList).done(function () {
	                    for (var i = 0, l = arguments.length; i < l; i++) {
	                        self._cacheData(arguments[i] || {}, sIndex, eIndex);
	                    }
	                    self._execCallback(successCallback, self._formatEODDataOut(infoList, freq, startIndex, endIndex, alignMainTicker));
	                });
	                /*QSAPI.Pormise.all(_queryList).then(function(resultList){
	                    for(var i=0,l=resultList.length;i<l;i++){
	                        self._cacheData(resultList[i]|| { }, keepOriginIndex?startIndex:sIndex, keepOriginIndex?endIndex:eIndex);
	                    }
	                    self._execCallback(successCallback,self._formatEODDataOut(infoList,freq,startIndex,endIndex));
	                });*/
	            } else {
	                self._execCallback(successCallback, this._formatEODDataOut(infoList, freq, startIndex, endIndex, alignMainTicker));
	            }
	        },
	        /**
	         *infoList:[{symbol:"OP00002RH",tickerObject:tickerObject,days:5,type:"price",datapoints:{p:true,v:true},indicators:{SMA:[],EMA:[]}]
	         *freq:5,10,15,30,60
	         *startIndex:41980
	         *endIndex:41985,
	         *successCallback:function(result){}
	         *showPre:true/false
	         */
	        getIntradayData: function (infoList, freq, startIndex, endIndex, successCallback, showPre, id, forceDelay) { //type:return or price
	            forceDelay = QSAPI.Util.getValidForceDelay(forceDelay);
	            var self = this;
	            var rtdTickers = [];
	            var rInfoListMap = this._checkDataReady(infoList, freq, startIndex, endIndex, forceDelay, id, showPre);
	            if (!$.isEmptyObject(rInfoListMap)) {
	                var _queryList = [];
	                for (var key in rInfoListMap) {
	                    var type = key.split('_')[0];
	                    _queryList.push(this._queryRemoteData(type, rInfoListMap[key], freq, startIndex, endIndex, showPre, forceDelay))
	                }
	                $.when.apply($, _queryList).done(function () {
	                    for (var i = 0, l = arguments.length; i < l; i++) {
	                        self._cacheData(arguments[i], startIndex, endIndex);
	                    }
	                    self._execCallback(successCallback, self._formatIntradayDataOut(infoList, freq, startIndex, endIndex, showPre, forceDelay));
	                });
	                /*QSAPI.Pormise.all(_queryList).then(function(resultList){
	                    for(var i=0,l=resultList.length;i<l;i++){
	                        self._cacheData(resultList[i],startIndex,endIndex);
	                    }
	                    self._execCallback(successCallback, self._formatIntradayDataOut(infoList, freq, startIndex, endIndex, showPre, forceDelay));
	                });*/
	            } else {
	                this._execCallback(successCallback, this._formatIntradayDataOut(infoList, freq, startIndex, endIndex, showPre, forceDelay));
	            }
	            if (this._isAutoRefresh(id)) {
	                var params = this._getActiveTickers(id);
	                var taskId = this._getTaskId(id);
	                var taskCfg = QSAPI.Task.getCfg(taskId) || {};
	                var period = freq * this.MINUTEMSECONDS / 2;
	                if (taskCfg.period > 0) {
	                    if (taskCfg.period != period || taskCfg.showPre != showPre) {
	                        QSAPI.Task.setCfg(taskId, {
	                            params: params,
	                            period: period,
	                            showPre: showPre
	                        });
	                    } else {
	                        QSAPI.Task.setParams(taskId, params, this);
	                    }
	                } else {
	                    QSAPI.Task.register({
	                        taskId: taskId,
	                        period: period,
	                        params: params,
	                        showPre: showPre,
	                        forceDelay: forceDelay,
	                        batchQuerySize: this.maxCacheSize,
	                        query: function (tickers, _taskId) {
	                            self._query(tickers, _taskId);
	                        }
	                    });
	                }
	            }
	        },
	        _isAutoRefresh: function (id) {
	            var autoRefresh;
	            var charts;
	            for (var ticker in this._registedCharts) {
	                charts = this._registedCharts[ticker].charts;
	                for (var i = 0, l = charts.length; i < l; i++) {
	                    if (charts[i].id === id) {
	                        autoRefresh = charts[i].autoRefresh === false ? false : true;
	                        break;
	                    }
	                }
	                if (typeof autoRefresh !== 'undefined') {
	                    break;
	                }
	            }
	            return this.needUpdateIntradyData && autoRefresh;
	        },
	        _duplicateRemoval: function (str, special) {
	            var _str = typeof str == "string" ? str : str.join(",");
	            var _array = _str.split(",");
	            var _ret = [];
	            var _flagMap = {};
	            for (var i = 0, l = _array.length; i < l; i++) {
	                if (!_array[i] || special == _array[i]) continue;
	                if (!_flagMap[_array[i]]) {
	                    _flagMap[_array[i]] = true;
	                    _ret.push(_array[i]);
	                }
	            }
	            return _ret;
	        },
	        _getFODParameter: function (data) {
	            var para = {
	                type: 'bar',
	                JSONShort: true
	            };
	            var dateUtil = QSAPI.Widget.Chart.Util;
	            para.instrument = data.tickers;
	            if (data.days) {
	                para.tradingdays = data.days;
	                para.OpenClose = true;
	            }
	            if (data.sd) {
	                if (data.sd.indexOf('-') > 0) {
	                    para.sdate = data.sd;
	                } else {
	                    var sdate = dateUtil.formatDateDMY(dateUtil.convertYMDToDate(data.sd));
	                    para.sdate = sdate;
	                }
	                if (!data.ed && !data.st) {
	                    para.ed = 'now';
	                }
	            }
	            if (data.ed) {
	                if (data.ed.indexOf('-') > 0) {
	                    para.edate = data.ed;
	                } else {
	                    var edate = dateUtil.formatDateDMY(dateUtil.convertYMDToDate(data.ed));
	                    para.edate = edate;
	                }
	            }
	            if (data.st) {
	                para.stime = data.st;
	                if (!data.et) {
	                    para.etime = 'now';
	                }
	            }
	            if (!isNaN(data.f)) {
	                para.interval = data.f;
	            } else {
	                switch (data.f) {
	                    case 'd':
	                        para.type = 'dailybar';
	                        break;
	                    case 'w':
	                        para.type = 'weeklybar';
	                        break;
	                    case 'm':
	                        para.type = 'monthlybar';
	                        break;
	                }
	            }
	            return para;
	        },
	        _getIWTTSRequestInfo: function (chartDataType, infoList, freq, startIndex, endIndex) {
	            var startDate, endDate, priceType, tickers = [],
	                solutionKey,
	                fields = [],
	                _util = QSAPI.Widget.Chart.Util,
	                dataPoints = QSAPI.Widget.Chart.DataPoints,
	                api = {},
	                apiSetting;
	            api[dataPoints.TSRETURN] = 'timeseries_cumulativereturn';
	            api[dataPoints.TSPRICE] = 'timeseries_price';
	            api[dataPoints.TSDIVIDEND] = 'timeseries_dividend';
	
	            for (var i = 0, info, tickerObj, l = infoList.length; i < l; i++) {
	                info = infoList[i];
	                tickerObj = info.tickerObject;
	                solutionKey = tickerObj.solutionKey || info.solutionKey;
	                tickers.push(tickerObj.securityToken + '|' + tickerObj.idType);
	                if (startIndex >= 0 && endIndex >= 0) {
	                    startDate = _util.toDateFromIndex(startIndex);
	                    endDate = _util.toDateFromIndex(endIndex)
	                } else {
	                    startDate = this._dates[freq];
	                    endDate = this._dates["lastTradeDate"];
	                }
	                fields.push(info.convertDataPoints.join(","));
	                if (!apiSetting) {
	                    apiSetting = info.apiSetting;
	                }
	            }
	
	            var field, ids = [],
	                idTypes = [];
	            if (fields.length > 0) {
	                fields = this._duplicateRemoval(fields.join(","));
	            }
	            if (tickers.length > 0) {
	                tickers = this._duplicateRemoval(tickers.join(","));
	                for (var i = 0; i < tickers.length; i++) {
	                    ids.push(tickers[i].split('|')[0]);
	                    idTypes.push(tickers[i].split('|')[1] || 'MSID');
	                }
	                /**
	                 * Need uncomment when IWT support multi tickers.
	                  var sigleType = true;
	                for (var i = 1; i < idTypes.length; i++) {
	                    if (idTypes[i - 1] !== idTypes[i]) {
	                        sigleType = false;
	                        break;
	                    }
	                }
	                if (sigleType) {
	                    idTypes = idTypes.slice(0, 1);
	                }*/
	            }
	            switch (freq) {
	                case 'm':
	                    freq = 'monthly';
	                    break;
	                case 'w':
	                    freq = 'weekly';
	                    break;
	                case 'd':
	                    freq = 'daily';
	                    break;
	            }
	            field = fields[0];
	            var request = {};
	            request.API = api[field];
	            request.interfaceUrl = this.dataUrls.IWTTimeseries() + request.API + '/' + solutionKey;
	            request.para = {
	                startDate: _util.formatDateYMD(startDate),
	                endDate: _util.formatDateYMD(endDate),
	                currencyId: this._getCountryCode(tickerObj, this.CHARTDATATYPE.IT_HISTORICAL_STOCK),
	                frequency: freq,
	                outputType: "json",
	                id: ids.join('|'),
	                idType: idTypes.join('|')
	            }
	            if (apiSetting && apiSetting[request.API]) {
	                for (var key in apiSetting[request.API]) {
	                    request.para[key] = apiSetting[request.API][key];
	                }
	            }
	            if (Number(chartDataType) === this.CHARTDATATYPE.IT_HISTORICAL_FUND) {
	                request.para.priceType = 'NAV';
	                request.para.fallback = 'no';
	            }
	            return request;
	        },
	        _requestIWTTimeseries: function (interfaceUrl, para, requestInfo) {
	            var $deferred = $.Deferred();
	            var self = this;
	            this._requestData(interfaceUrl, para, function (response) {
	                $deferred.resolve(self._transformIWTtoChartData(response, requestInfo));
	            });
	            return $deferred;
	        },
	        _queryRemoteData: function (chartDataType, infoList, freq, startIndex, endIndex, showPreAfter, forceDelay) {
	            var deferred = $.Deferred();
	            var para = this._getRequestPara(chartDataType, infoList, freq, startIndex, endIndex, showPreAfter, forceDelay);
	            var requestInfo = {
	                frequency: para.f
	            }
	            var interfaceUrl = this.dataDomain + this.dataUrls.chart;
	            var isFODRequest = ($.inArray(Number(chartDataType), [this.CHARTDATATYPE.REALTIME_INTRADAY_LAST, this.CHARTDATATYPE.REALTIME_INTRADAY_RANGE, this.CHARTDATATYPE.REALTIME_HISTORICAL]) > -1) && QSAPI.isFod();
	            var isIWTTimeseries = this.isIWTChartDataType(Number(chartDataType));
	            if (isFODRequest) {
	                interfaceUrl = this.dataUrls.getFODTimeseries();
	                para = this._getFODParameter(para);
	                requestInfo.tradingDays = para.tradingdays;
	                if (para.tradingdays > 0) {
	                    para.tradingdays += 4; //Get more data in case of holidays
	                }
	                requestInfo.startDate = para.sdate;
	            }
	            if (isIWTTimeseries) {
	                // It's a temporary fix since IWT timeseries doesn't support mutiple tickers. 
	                var request = this._getIWTTSRequestInfo(chartDataType, infoList, freq, startIndex, endIndex);
	                interfaceUrl = request.interfaceUrl;
	                para = request.para;
	                var ids = para.id.split('|');
	                var idTypes = para.idType.split('|');
	                var queryList = [];
	                for (var i = 0; i < ids.length; i++) {
	                    var tempPara = $.extend(true, {}, para);
	                    tempPara.id = ids[i];
	                    tempPara.idType = idTypes[i];
	                    queryList.push(this._requestIWTTimeseries(interfaceUrl, tempPara, requestInfo));
	                }
	                var response = null;
	                $.when.apply($, queryList).done(function () {
	                    for (var j = 0, l = arguments.length; j < l; j++) {
	                        var result = arguments[j] || {};
	                        if (!response || !response.Data) {
	                            response = result;
	                        } else if (result.Data) {
	                            response.Data = response.Data.concat(result.Data);
	                        }
	                    }
	                    deferred.resolve(response);
	                });
	            } else {
	                var self = this;
	                this._requestData(interfaceUrl, para, function (response) {
	                    var res = $.extend(true, {}, response);
	                    if (isFODRequest) {
	                        res = QSAPI.fod.transformFodToChartData(res, requestInfo);
	                    }
	                    deferred.resolve(res);
	                }, function () {
	                    deferred.resolve([]);
	                });
	            }
	            return deferred;
	        },
	        _getRequestPara: function (chartDataType, infoList, freq, startIndex, endIndex, showPreAfter, forceDelay) {
	            var tickers = [];
	            var days = -1;
	            var para = {
	                f: freq
	            };
	            var fields = [];
	            var _unqinue = {};
	            for (var i = 0, ticker, tickerObj, chcheKey, info, l = infoList.length; i < l; i++) {
	                info = infoList[i];
	                tickerObj = info.tickerObject;
	                if (days < 0) {
	                    days = info.days;
	                }
	                if (chartDataType == this.CHARTDATATYPE.REALTIME_INTRADAY_LAST ||
	                    chartDataType == this.CHARTDATATYPE.REALTIME_INTRADAY_RANGE ||
	                    chartDataType == this.CHARTDATATYPE.REALTIME_HISTORICAL) {
	                    ticker = this._getRTDTicker(tickerObj);
	                    if (chartDataType == this.CHARTDATATYPE.REALTIME_INTRADAY_LAST ||
	                        chartDataType == this.CHARTDATATYPE.REALTIME_INTRADAY_RANGE) {
	                        para["preAfter"] = showPreAfter === false ? false : true;
	                        if (para["preAfter"]) {
	                            para["listingMarket"] = tickerObj.listMarket;
	                        }
	                        forceDelay && (para["forceDelay"] = 1);
	                    }
	                } else {
	                    if (chartDataType == this.CHARTDATATYPE.DATAAPIV2_PID) {
	                        ticker = tickerObj.performanceId;
	                    } else {
	                        ticker = this._getSecId(tickerObj);
	                    }
	
	                    if ((chartDataType == this.CHARTDATATYPE.TS_WITH_PID_FIXED_FREQUENCY ||
	                            chartDataType == this.CHARTDATATYPE.TS_WITH_PID ||
	                            chartDataType == this.CHARTDATATYPE.FUNDAMENTAL_DIRECT ||
	                            chartDataType == this.CHARTDATATYPE.FUNDAMENTAL_EQUATY_DATA ||
	                            chartDataType == this.CHARTDATATYPE.DATAAPI_SID ||
	                            chartDataType == this.CHARTDATATYPE.DATAAPIV2_PID ||
	                            chartDataType == this.CHARTDATATYPE.FUNDAMENTAL_RDB) &&
	                        info.convertDataPoints.length > 0) {
	                        for (var d = 0; d < info.convertDataPoints.length; d++) {
	                            if (QSAPI.Widget.Chart.DataPoints.needCurrency(info.convertDataPoints[d])) {
	                                info.convertDataPoints.push(QSAPI.Widget.Chart.DataPoints.CURRENCY);
	                                break;
	                            }
	                        }
	                        fields.push(info.convertDataPoints.join(","));
	                        if (chartDataType == this.CHARTDATATYPE.TS_WITH_PID_FIXED_FREQUENCY) { // for dividend, earnings and split, no need pass frequency and filter weekend and holiday data
	                            para["f"] = "d";
	                            para["rw"] = false;
	                            para["rh"] = false;
	                        }
	                    }
	                    para["country"] = this._getCountryCode(tickerObj, chartDataType);
	                }
	                if (!_unqinue[ticker]) {
	                    _unqinue[ticker] = true;
	                    tickers.push(ticker);
	                }
	            }
	            if (fields.length > 0) {
	                var fieldsStr = this._duplicateRemoval(fields.join(","), QSAPI.Widget.Chart.DataPoints.PRICE).join(",");
	                para["fields"] = fieldsStr.replace(/:/g, ",");
	            }
	
	            if (this._isMutualFund(tickerObj) && !isNaN(freq)) {
	                para.f = 'd';
	            }
	            var _util = QSAPI.Widget.Chart.Util;
	            if (days > 0) {
	                if (this._isMutualFund(tickerObj) && !isNaN(freq)) {
	                    para["ed"] = _util.formatDateYMD(this._dates["lastTradeDate"], "");
	                    para["sd"] = _util.formatDateYMD(this._dates["10"], "");
	                } else {
	                    para["days"] = days;
	                }
	            } else {
	                if (startIndex >= 0 && endIndex >= 0) {
	                    para["sd"] = _util.formatDateYMD(_util.toDateFromIndex(startIndex), "");
	                    para["ed"] = _util.formatDateYMD(_util.toDateFromIndex(endIndex), "");
	                } else {
	                    para["sd"] = _util.formatDateYMD(this._dates[freq], "");
	                    para["ed"] = _util.formatDateYMD(this._dates["lastTradeDate"], "");
	                }
	            }
	            para["tickers"] = tickers.join(",");
	            para["cdt"] = Math.abs(chartDataType);
	
	            return para;
	        },
	        _formatIntradayDataOut: function (infoList, frequency, sIndex, eIndex, showPre, forceDelay) {
	            var tickerObj, tickerCache, rData = [],
	                days = -1;
	            for (var i = 0, l = infoList.length; i < l; i++) {
	                var freq = frequency;
	                if (days < 0) {
	                    days = infoList[i].days;
	                }
	                tickerObj = infoList[i].tickerObject;
	                rData[i] = {
	                    symbol: infoList[i].symbol,
	                    tickerObject: tickerObj,
	                    fre: freq
	                };
	                tickerCache = this._getTickerCache(tickerObj);
	                if (tickerCache) {
	                    for (var o in infoList[i].datapoints) {
	                        if (!infoList[i].datapoints[o]) continue;
	                        var dp = {
	                            origin: infoList[i].datapoints[o],
	                            real: QSAPI.Widget.Chart.DataPoints.parse(o, infoList[i].datapoints[o], infoList[i].type),
	                            growthBaseValue: infoList[i].growthBaseValue
	                        };
	                        if (o == "p") {
	                            rData[i].price = tickerCache.getIntradayData(dp, freq, sIndex, eIndex, days, showPre, forceDelay, this._isMutualFund(tickerObj));
	                            if (rData[i].price != null) {
	                                rData[i].price.lastTradeDate = tickerCache.lastTradeDate(freq, forceDelay); // pass the last trade date to QS chart.
	                            }
	                            rData[i].currencyField = this._getCurrencyField(tickerObj, this._getChartDataType(tickerObj, dp.real, freq));
	                        } else if (o == "v") {
	                            rData[i].volume = tickerCache.getIntradayData(dp, freq, sIndex, eIndex, days, showPre, forceDelay, this._isMutualFund(tickerObj));
	                        } else if (o == "indicators") {
	                            var inds = infoList[i].datapoints[o] || [];
	                            rData[i].indicators = {};
	                            var iname;
	                            for (var j = 0, jl = inds.length; j < jl; j++) {
	                                iname = inds[j].name;
	                                if (iname == "EMA" || iname == "SMA") {
	                                    if (!rData[i].indicators[iname]) {
	                                        rData[i].indicators[iname] = {};
	                                    }
	                                    rData[i].indicators[iname][inds[j].p[0]] = tickerCache.getIndicatorData(rData[i], inds[j], freq, sIndex, eIndex, days, showPre, forceDelay);
	                                } else {
	                                    rData[i].indicators[iname] = tickerCache.getIndicatorData(rData[i], inds[j], freq, sIndex, eIndex, days, showPre, forceDelay);
	                                    if (iname == 'DYield' || iname == 'RDividend') {
	                                        rData[i].indicators[iname]['DailyDataPoint'] = true;
	                                    }
	                                }
	                            }
	                        } else { // not for intradays, fundamentals
	                            var otherPoints = infoList[i].datapoints[o] || [];
	                            rData[i][o] = {};
	                            for (var j = 0, jl = otherPoints.length; j < jl; j++) {
	                                rData[i][o]['DailyDataPoint'] = true;
	                                rData[i][o]['data'] = [];
	                            }
	                        }
	                    }
	                }
	            }
	            return rData;
	        },
	        _transformIWTtoChartData: function (data, requestInfo) {
	            var res = {
	                Status: {
	                    ErrorCode: "0",
	                    ErrorMsg: "Succeeded",
	                    SubErrorCode: null
	                },
	                Data: null
	            };
	            data = data.TimeSeries || data || {};
	            data.Security = data.Security || [];
	            var _util = QSAPI.Widget.Chart.Util;
	            for (var i = 0; i < data.Security.length; i++) {
	                var securityData = data.Security[i];
	                if (!res.Data) {
	                    res.Data = [];
	                }
	                var timeseriesData = {};
	                timeseriesData.Ticker = securityData.Id;
	                timeseriesData.Frequency = requestInfo.frequency;
	                timeseriesData.hasPreAfter = false;
	                timeseriesData.cdt = 16;
	                timeseriesData.DailyData = {};
	                var dataPoint = "";
	                if (securityData.HistoryDetail) {
	                    dataPoint = "TSP";
	                } else if (securityData.CumulativeReturnSeries) {
	                    dataPoint = "TSC";
	                    securityData = securityData.CumulativeReturnSeries[0];
	                } else if (securityData.DividendSeries) {
	                    dataPoint = "TSD";
	                    securityData = securityData.DividendSeries[0];
	                }
	                securityData = securityData && securityData.HistoryDetail || [];
	                timeseriesData.DailyData[dataPoint] = [];
	                for (var j = 0; j < securityData.length; j++) {
	                    var tsData = securityData[j];
	                    timeseriesData.DailyData[dataPoint].push({
	                        Index: _util.toDateIndex(_util.convertToDate(tsData.EndDate)),
	                        Date: _util.formatDateYMD(_util.convertToDate(tsData.EndDate), ''),
	                        OriginalDate: _util.formatDateYMD(_util.convertToDate(tsData.OriginalDate || ""), ''),
	                        Last: (function (Value) {
	                            if ($.isArray(Value)) {
	                                Value = Value[0].value;
	                            }
	                            return Value;
	                        })(tsData.Value),
	                        FillFlag: 0
	                    });
	                }
	                res.Data.push(timeseriesData);
	            }
	            return res;
	        },
	        _formatEODDataOut: function (infoList, freq, sIndex, eIndex, alignMainTicker) {
	            var tickerObj, tickerCache, rData = [],
	                startIndex = sIndex,
	                endIndex = eIndex;
	            alignMainTicker = alignMainTicker === false ? false : true;
	            for (var i = 0, l = infoList.length; i < l; i++) {
	                tickerObj = infoList[i].tickerObject;
	                if (!tickerObj) {
	                    continue;
	                }
	                rData[i] = {
	                    symbol: infoList[i].symbol,
	                    tickerObject: tickerObj,
	                    fre: freq
	                };
	                tickerCache = this._getTickerCache(tickerObj);
	                var isFund = this._isMutualFund(tickerObj);
	                if (tickerCache) {
	                    var DataPoints = QSAPI.Widget.Chart.DataPoints;
	                    for (var o in infoList[i].datapoints) {
	                        if (!infoList[i].datapoints[o]) continue;
	                        var dp = {
	                            origin: infoList[i].datapoints[o],
	                            real: DataPoints.parse(o, infoList[i].datapoints[o], isFund ? "" : infoList[i].type, tickerObj.getField("benchmarkType") || infoList[i].chartType),
	                            growthBaseValue: infoList[i].growthBaseValue
	                        };
	                        if (o == "p") {
	                            rData[i].price = tickerCache.getEODData(dp, freq, startIndex, endIndex);
	                            if (alignMainTicker && i == 0) {
	                                if (rData[i].price.data != null && rData[i].price.data.length > 0) {
	                                    startIndex = rData[i].price.data[0][rData[i].price.data[0].length - 1];
	                                    endIndex = rData[i].price.data[rData[i].price.data.length - 1][rData[i].price.data[rData[i].price.data.length - 1].length - 1];
	                                }
	                            }
	                            rData[i].currencyField = this._getCurrencyField(tickerObj, this._getChartDataType(tickerObj, dp.real, freq));
	                        } else if (o == "growthPrice") {
	                            var _freq = freq;
	                            if (DataPoints.isTSPrice(dp.real) && freq !== 'm') {
	                                _freq = 'd';
	                            }
	                            rData[i].growthPrice = tickerCache.getEODData(dp, _freq, startIndex, endIndex, rData[i]);
	                        } else if (o == "v") {
	                            rData[i].volume = tickerCache.getEODData(dp, freq, startIndex, endIndex);
	                        } else if (o == "e") { // earnings data
	                            if (dp.origin == "se") {
	                                rData[i].earningsData = tickerCache.getSemiAnnualEPS(dp, "d", startIndex, endIndex);
	                            } else {
	                                rData[i].earningsData = tickerCache.getEODData(dp, "d", startIndex, endIndex);
	                            }
	                        } else if (o == "ds") { // dividend and split
	                            var dps = [],
	                                dividendFreq = freq;
	                            if (dp.origin["d"]) {
	                                dps.push(DataPoints.DIVIDEND);
	                                dividendFreq = "d";
	                            }
	                            if (dp.origin["tsd"]) dps.push(DataPoints.TSDIVIDEND);
	                            if (dp.origin["s"]) {
	                                dps.push(DataPoints.SPLITFROM + ":" + DataPoints.SPLITTO);
	                                dividendFreq = "d";
	                            }
	                            rData[i].dividendAndSplit = tickerCache.getDividendAndSplitData(dps, dividendFreq, startIndex, endIndex);
	                        } else if (o === "ap") {
	                            rData[i].tsCache = tickerCache.getEODData(dp, freq, startIndex, endIndex);
	                        } else if (o == "indicators") { // indicator data.
	                            var inds = infoList[i].datapoints[o] || [];
	                            rData[i].indicators = {};
	                            var iname;
	                            for (var j = 0, jl = inds.length; j < jl; j++) {
	                                iname = inds[j].name;
	                                if (iname == "EMA" || iname == "SMA") {
	                                    if (!rData[i].indicators[iname]) {
	                                        rData[i].indicators[iname] = {};
	                                    }
	                                    rData[i].indicators[iname][inds[j].p[0]] = tickerCache.getIndicatorData(rData[i], inds[j], freq, startIndex, endIndex);
	                                } else {
	                                    rData[i].indicators[iname] = tickerCache.getIndicatorData(rData[i], inds[j], freq, startIndex, endIndex);
	                                }
	                            }
	                        } else { //if(DataPoints.isFundamental(dp.real) || DataPoints.isMReturnIndex(dp.real)){
	                            rData[i][o] = tickerCache.getEODData(dp, freq, startIndex, endIndex);
	                        }
	                    }
	                }
	            }
	            return rData;
	        },
	        /*need implement*/
	        exportDataToFile: function (fileType, showPreAfter, tickerObjects, frequency, days, st, ed, headers, priceDataPoint, dataPoints) {
	            //only support to export main ticker
	            if (tickerObjects.length > 0) {
	                dataPoints = dataPoints || [];
	                var _util = QSAPI.Widget.Chart.Util;
	                var tickerObj = tickerObjects[0];
	                var DataPoints = QSAPI.Widget.Chart.DataPoints;
	                var convertDataPoint = priceDataPoint ? DataPoints.parse("p", priceDataPoint) : DataPoints.PRICE;
	                var chartDataType = this._getChartDataType(tickerObj, convertDataPoint, frequency, days);
	                var convertDataPoints = [convertDataPoint];
	                $.each(dataPoints, function (i, d) {
	                    convertDataPoint = DataPoints.parse(d.key, d.value);
	                    if (this._getChartDataType(tickerObj, convertDataPoint, frequency, days) === chartDataType) {
	                        convertDataPoints.push(convertDataPoint);
	                    }
	                }.bind(this));
	                var infoList = [{
	                        tickerObject: tickerObj,
	                        days: days,
	                        convertDataPoints: convertDataPoints
	                    }],
	                    startIndex = _util.toDateIndex(st),
	                    endIndex = _util.toDateIndex(ed);
	                var para = this._getRequestPara(chartDataType, infoList, frequency, startIndex, endIndex, showPreAfter);
	                var clientTicker = this._getTicker(tickerObj);
	                para.url = this.dataDomain + this.dataUrls.chartExport;
	                para.headers = headers;
	                para.fileName = clientTicker;
	                para.sheetName = clientTicker;
	                para.ept = fileType;
	                QSAPI.Util.exportFile(para);
	            }
	        },
	        exportDataToExcel: function (showPreAfter, tickerObjects, frequency, days, st, ed, headers, priceDataPoint, dataPoints) {
	            this.exportDataToFile('EXCEL', showPreAfter, tickerObjects, frequency, days, st, ed, headers, priceDataPoint, dataPoints);
	        },
	        getChartNoteData: function (startDate, endDate, tickerObj, callback) {
	
	        },
	        getDrawings: function (tickerObj, dataPoints, callback) {
	            var self = this;
	            var tickerCache = this._getTickerCache(tickerObj);
	            if (!tickerCache) {
	                callback();
	            } else if (tickerCache.checkDrawingsReady()) {
	                callback(self._getDrawings(tickerCache, dataPoints));
	            } else {
	                var pid = this._getSecId(tickerObj);
	                if (this.saveDrawings) { //from database
	                    self._requestData(this.dataDomain + this.dataUrls.chartDrawings, {
	                        action: "loaddraw",
	                        pid: pid
	                    }, function (response) {
	                        var _pid = response.secId;
	                        if (pid == _pid) {
	                            tickerCache.drawings(self._handleDrawingsResponse(response.list));
	                        }
	                        callback(self._getDrawings(tickerCache, dataPoints));
	                    }, function () {
	                        callback();
	                    }, "cb");
	                } else { //from cookie
	                    var cookieDrawings = $.evalJSON(this._getCookie("QS_Chart_Drawing") || "{}") || {};
	                    tickerCache.drawings(cookieDrawings[pid] || {});
	                    callback(self._getDrawings(tickerCache, dataPoints));
	                }
	            }
	        },
	        removeDrawing: function (tickerObj, type, point, callback) {
	            callback = this._genCallback(callback);
	            var self = this;
	            var tickerCache = this._getTickerCache(tickerObj);
	            var removed = tickerCache.removeDrawing(type, point);
	            if (removed) {
	                if (this.saveDrawings) { // remove the datapoint from DB.
	                    var data = {
	                        action: "removedraw",
	                        drawid: point.id
	                    }
	                    this._requestData(this.dataDomain + this.dataUrls.chartDrawings, data, function (rdata) {
	                        callback(rdata);
	                    }, function () {
	                        callback(false);
	                    }, "cb");
	                } else {
	                    var drawings = $.evalJSON(this._getCookie("QS_Chart_Drawing") || "{}") || {};
	                    var newDrawings = {};
	                    var secId = this._getSecId(tickerObj);
	                    newDrawings[secId] = tickerCache.drawings();
	                    delete drawings[secId];
	                    $.extend(true, drawings, newDrawings);
	                    this._setCookie("QS_Chart_Drawing", $.toJSON(drawings), null, "/", null, false);
	                }
	            }
	        },
	        updateDrawing: function (tickerObj, type, point, callback) {
	            callback = this._genCallback(callback);
	            var self = this;
	            if (type != "levelline" && (!point || !point.oldStart || !point.oldEnd)) return;
	            this._handleSavedDrawPoint(point);
	            var tickerCache = this._getTickerCache(tickerObj);
	            var updated = tickerCache.updateDrawing(type, point);
	            if (updated) {
	                var secId = this._getSecId(tickerObj);
	                if (this.saveDrawings) {
	                    var infoObj = {
	                        drawType: type,
	                        start: point.start,
	                        end: point.end
	                    };
	                    var data = {
	                        action: "updatedraw",
	                        pid: secId,
	                        drawid: point.id,
	                        saveinfo: $.toJSON(infoObj)
	                    };
	                    this._requestData(this.dataDomain + this.dataUrls.chartDrawings, data, function (rdata) {
	                        if (rdata.errorCode == 0) {
	                            callback(true);
	                        } else {
	                            callback(false);
	                        }
	                    }, function () {
	                        callback(false);
	                    }, "cb")
	                } else {
	                    var drawings = $.evalJSON(this._getCookie("QS_Chart_Drawing") || "{}") || {};
	                    var newDrawings = {};
	                    newDrawings[secId] = tickerCache.drawings();
	                    delete drawings[secId];
	                    $.extend(true, drawings, newDrawings);
	                    this._setCookie("QS_Chart_Drawing", $.toJSON(this._getSavedDrawing(drawings)), null, "/", null, false);
	                }
	            }
	        },
	        saveLastDrawing: function (tickerObj, type, point, callback) {
	            callback = this._genCallback(callback);
	            var self = this;
	            var tickerCache = this._getTickerCache(tickerObj);
	            this._handleSavedDrawPoint(point);
	            point.drawType = type;
	            tickerCache.saveDrawing(point);
	            var secId = this._getSecId(tickerObj);
	            if (this.saveDrawings) {
	                delete point.oldStart;
	                delete point.oldEnd;
	                var info = $.toJSON(this._getSavedDrawing(point, true));
	                var data = {
	                    action: "insertdraw",
	                    pid: secId,
	                    saveinfo: info
	                };
	                this._requestData(this.dataDomain + this.dataUrls.chartDrawings, data, function (rdata) {
	                    if (rdata.secId == secId) {
	                        var returnValue = {};
	                        if (rdata.list) {
	                            for (var i = 0, l = rdata.list.length; i < l; i++) {
	                                returnValue[rdata.list[i].eventId] = $.evalJSON(rdata.list[i].eventInfo);
	                            }
	                        }
	                        callback(true, returnValue);
	                    }
	                }, function () {
	                    callback(false);
	                }, "cb")
	            } else {
	                var drawings = $.evalJSON(this._getCookie("QS_Chart_Drawing") || "{}") || {};
	                var newDrawings = {};
	                newDrawings[secId] = tickerCache.drawings();
	                delete drawings[secId];
	                $.extend(true, drawings, newDrawings);
	                this._setCookie("QS_Chart_Drawing", $.toJSON(this._getSavedDrawing(drawings)), null, "/", null, false);
	            }
	        },
	        getUpdatedPricePoints: function (tickerObj, callback) {
	            callback = this._genCallback(callback);
	            var action = "get";
	            var service = Util.formatString(this.dataUrls.chartExtend, action);
	            var self = this;
	            this._requestData(this.dataDomain + service, {
	                ticker: this._getRequestKey(tickerObj)
	            }, function (rdata) {
	                if (rdata.status && rdata.status.errorCode == 0) {
	                    self._saveExtendData(rdata);
	                    callback({
	                        errormsg: ""
	                    });
	                } else {
	                    callback({
	                        errormsg: rdata.status.errorMsg
	                    });
	                }
	            }, function (error) {
	                callback({
	                    errormsg: error
	                });
	            });
	        },
	        removeUpdatedPricePoint: function (tickerObj, data, callback) {
	            callback = this._genCallback(callback);
	            var self = this;
	            var action = "delete";
	            var service = Util.formatString(this.dataUrls.chartExtend, action);
	
	            this._requestData(this.dataDomain + service, {
	                id: data.id
	            }, function (rdata) {
	                if (rdata.status && rdata.status.errorCode == 0) {
	                    var tickerCache = self._getTickerCache(tickerObj);
	                    if (tickerCache) {
	                        tickerCache.removeExtendChartData(data);
	                    }
	                    self._saveExtendData(rdata);
	                    callback({
	                        errormsg: ""
	                    });
	                } else {
	                    callback({
	                        errormsg: rdata.status.errorMsg
	                    });
	                }
	            }, function (error) {
	
	            });
	        },
	        /**data={date:yyyy-mm-dd,freq:"1",o:12.5,c:12.7,h:12.8,l:12.55,v:20000,time:550}**/
	        saveUpdatedPricePoint: function (tickerObj, data, callback) {
	            callback = this._genCallback(callback);
	            var action = "add",
	                self = this,
	                paras = {
	                    ticker: this._getRequestKey(tickerObj),
	                    freq: data["freq"],
	                    date: data["date"],
	                    time: data["time"],
	                    newValue: $.toJSON({
	                        o: data["o"],
	                        c: data["c"],
	                        h: data["h"],
	                        l: data["l"],
	                        v: data["v"]
	                    })
	                };
	            if (data["id"] > 0) {
	                action = "update";
	                paras.id = data["id"];
	            }
	            var service = Util.formatString(this.dataUrls.chartExtend, action);
	            var tickerCache = this._getTickerCache(tickerObj);
	            this._requestData(this.dataDomain + service, paras, function (rdata) {
	                if (rdata.status && rdata.status.errorCode == 0) {
	                    self._saveExtendData(rdata);
	                    callback({
	                        errormsg: ""
	                    });
	                } else {
	                    callback({
	                        errormsg: rdata.status.errorMsg
	                    });
	                }
	            }, function (error) {
	                callback({
	                    errormsg: error
	                });
	            })
	        },
	        getDefaultStartDates: function () {
	            return this._dates;
	        },
	        getIPODateIndex: function (tickerObj, callback) { //get the IPO date, don't implement it.
	            if (typeof (callback) == "function") {
	                var tickerCache = this._getTickerCache(tickerObj);
	                callback(tickerCache.getIPODateIndex());
	            }
	        },
	        _getTicker: function (tickerObj) {
	            return tickerObj.clientTicker || tickerObj.ticker || tickerObj.tenforeTicker;
	        },
	        _getTickerCache: function (tickerObj) {
	            var key = this._getCacheKey(tickerObj);
	            return this._cache[key];
	        },
	        _getCacheKey: function (ticker) {
	            var cacheKey = "";
	            if (typeof ticker === "string") {
	                for (var key in this._cache) {
	                    if (key.indexOf(ticker) > -1) {
	                        cacheKey = key;
	                        break;
	                    }
	                }
	            } else {
	                cacheKey = (ticker.secId || '') + ',' + (ticker.performanceId || '') + "," + (this._getRTDTicker(ticker) || '');
	            }
	            return cacheKey;
	        },
	        _getRequestKey: function (tickerObj) {
	            return this._isMutualFund(tickerObj) || this._isCustomBenchmark(tickerObj) || this._isPortfolio(tickerObj) ? this._getSecId(tickerObj) : this._getRTDTicker(tickerObj);
	        },
	        _getSecId: function (tickerObj) {
	            return tickerObj.secId || tickerObj.performanceId;
	        },
	        _getRTDTicker: function (tickerObj) {
	            var exch = tickerObj.tenforeCode;
	            var type = tickerObj.type;
	            var ticker = tickerObj.tenforeTicker;
	            if (exch && type && ticker) {
	                if (type === 1 && (exch === 511 || exch === 125)) {
	                    exch = 22;
	                } else {
	                    exch = QSAPI.Util.generateGKeyCode(tickerObj);
	                }
	                return [exch, type, ticker].join(".");
	            }
	            return undefined;
	        },
	        _getCountryCode: function (tickerObj, chartDataType) {
	            var country;
	            if (this.isIWTChartDataType(chartDataType)) {
	                country = tickerObj["customCurrency"] || tickerObj["currency"];
	                var tickerCache = this._getTickerCache(tickerObj);
	                tickerCache.baseCurrency = country;
	            } else {
	                country = tickerObj["country"];
	            }
	            return country;
	        },
	        _getCurrencyCode: function (tickerObj, chartDataType) {
	            return tickerObj[this._getCurrencyField(tickerObj, chartDataType)];
	        },
	        _getCurrencyField: function (tickerObj, chartDataType) {
	            if (this.isIWTChartDataType(chartDataType) && tickerObj.customCurrency) {
	                return 'customCurrency';
	            } else {
	                return 'currency';
	            }
	        },
	        _isMutualFund: function (tickerObj) {
	            var type = tickerObj.type;
	            var mType = tickerObj.mType;
	            var isFundType = false;
	            if (tickerObj.tsfund && tickerObj.tenforeCode && tickerObj.tenforeCode.indexOf('9999') > -1 || (tickerObj.tsfund && !tickerObj.tenforeCode)) {
	                isFundType = true;
	            }
	            return isFundType || mType === "FO" || (type == 8 && (!mType || mType == "SA")) || tickerObj.isCustomFund;
	        },
	        _isCustomBenchmark: function (tickerObj) {
	            return tickerObj.getField("benchmarkType") && tickerObj.getField("benchmarkType").length > 0;
	        },
	        _isPortfolio: function (tickerObj) {
	            return tickerObj.mType === 'PO';
	        },
	        _genCallback: function (callback) {
	            return typeof (callback) == "function" ? callback : function () {};
	        },
	        _execCallback: function (callback, data) {
	            if (typeof (callback) == "function") { // excute success callback.
	                callback(data);
	            }
	        },
	        _combineURL: function (url, data) {
	            var _url = url;
	            data = data || {};
	            var _keys = [];
	            for (var key in data) {
	                _keys.push(key);
	            }
	            _keys.sort();
	            if (_url.indexOf("?") < 0) {
	                _url += "?";
	            }
	            for (var i = 0, l = _keys.length; i < l; i++) {
	                {
	                    if (_keys[i] !== 'OpenClose' && _keys[i] !== 'JSONShort') {
	                        _url += _keys[i] + "=" + data[_keys[i]] + "&";
	                    }
	                }
	            }
	            if ($.inArray('OpenClose', _keys) > -1) {
	                _url += 'OpenClose' + '&';
	            }
	            if ($.inArray('JSONShort', _keys) > -1) {
	                _url += 'JSONShort';
	            }
	            _url = _url.replace(/&$/ig, "");
	            return _url;
	        },
	        _clearCacheRequest: function (key) {
	            this._cacheRequest[key] = null;
	            delete this._cacheRequest[key];
	        },
	        _requestData: function (url, data, successcallback, failcallback, jsoncallback) { // ajax request common method
	            var self = this;
	            var _key = this._combineURL(url, data);
	            (function (_key, url, data, successcallback, failcallback, jsoncallback) {
	                self._cacheRequest[_key] = self._cacheRequest[_key] || {
	                    status: self.STATUS.START,
	                    success: [successcallback],
	                    fail: [failcallback]
	                };
	                if (self._cacheRequest[_key].status == self.STATUS.PENDING) {
	                    self._cacheRequest[_key].success.push(successcallback);
	                    self._cacheRequest[_key].fail.push(failcallback);
	                } else {
	                    self._cacheRequest[_key].status = self.STATUS.PENDING;
	                    var ajaxoption = {
	                        url: _key,
	                        type: "get",
	                        third: /.+api\/rest\.svc.+/.test(_key),
	                        // data: data,
	                        dataType: 'json',
	                        runComponentAuthCheckFirst: true,
	                        success: function (jsondata, code) {
	                            var successcallbacks = [];
	                            if (self._cacheRequest && self._cacheRequest[_key] && self._cacheRequest[_key].success) {
	                                successcallbacks = self._cacheRequest[_key].success;
	                            }
	                            while (successcallbacks.length > 0) {
	                                var successcallback = successcallbacks.splice(0, 1)[0];
	                                if (typeof (successcallback) == "function") {
	                                    successcallback(jsondata);
	                                }
	                            }
	                            self._clearCacheRequest(_key);
	                        },
	                        error: function (jX, code) {
	                            var failcallbacks = self._cacheRequest[_key].fail;
	                            while (failcallbacks.length > 0) {
	                                var failcallback = failcallbacks.splice(0, 1)[0];
	                                if (typeof (failcallback) == "function") {
	                                    failcallback(code);
	                                }
	                            }
	                            self._clearCacheRequest(_key);
	                        }
	                    }
	                    if (self.timeout) {
	                        ajaxoption.timeout = self.timeout;
	                    }
	                    // if (self.crossDomain) {
	                    //     ajaxoption.jsonp = jsoncallback || "jsoncallback";
	                    //     ajaxoption.dataType = "jsonp";
	                    // }
	                    return QSAPI.Util.ajax(ajaxoption);
	                }
	            })(_key, url, data, successcallback, failcallback, jsoncallback);
	        },
	
	        _checkDataReady: function (infoList, freq, startIndex, endIndex, forceDelay, id, showPreAfter) {
	            var datapoints, tickerObj, key, tickerCache;
	            var remoteInfoListMap = {},
	                validDataPoints = {};
	            var days = -1;
	            var tempInfo;
	            var originalFreq = freq;
	            for (var i = 0, l = infoList.length; i < l; i++) {
	                var tempInfo = $.extend(true, {}, infoList[i]);
	                tempInfo.convertDataPoints = [];
	                tickerObj = tempInfo.tickerObject;
	                if (!tickerObj) {
	                    continue;
	                }
	                datapoints = tempInfo.datapoints;
	                if (days < 0 && infoList[i].days > 0) {
	                    days = tempInfo.days;
	                }
	                key = this._getCacheKey(tickerObj);
	                tickerCache = this._cache[key];
	                if (!tickerCache) {
	                    continue;
	                }
	                var isFund = this._isMutualFund(tickerObj);
	                if (isFund && !isNaN(originalFreq)) {
	                    freq = 'd';
	                    tempInfo.freq = 'd';
	                } else {
	                    freq = originalFreq;
	                    tempInfo.freq = originalFreq;
	                }
	                for (var dpkey in tempInfo.datapoints) {
	                    var parseDataPoint = QSAPI.Widget.Chart.DataPoints.parse(dpkey, tempInfo.datapoints[dpkey], isFund ? "" : tempInfo.type, tickerObj.getField("benchmarkType") || infoList[i].chartType);
	                    if (parseDataPoint) {
	                        tempInfo.convertDataPoints.push(parseDataPoint);
	                    }
	                }
	                tempInfo.convertDataPoints = this._duplicateRemoval(tempInfo.convertDataPoints);
	                for (var j = 0; j < tempInfo.convertDataPoints.length; j++) {
	                    var dataPoint = tempInfo.convertDataPoints[j];
	                    var _ready = showPreAfter ? false : tickerCache.checkDataReady(freq, dataPoint, startIndex, endIndex, tempInfo.days, forceDelay);
	                    if (_ready && !this._isAutoRefresh(id) && !isNaN(freq) || QSAPI.Widget.Chart.DataPoints.isNeedClearCache(dataPoint)) {
	                        _ready = false;
	                    }
	                    if (!_ready) {
	                        var _chartDataType = this._getChartDataType(tickerObj, dataPoint, freq, days);
	                        var country = this._getCountryCode(tickerObj, _chartDataType);
	                        var key = _chartDataType + '_' + country;
	                        if (showPreAfter) {
	                            key += '_' + tickerObj.listMarket;
	                        }
	                        if (!remoteInfoListMap[key]) {
	                            remoteInfoListMap[key] = [];
	                        }
	                        if (!validDataPoints[key]) {
	                            validDataPoints[key] = [];
	                        }
	
	                        validDataPoints[key].push(dataPoint);
	                        remoteInfoListMap[key].push(tempInfo);
	                    } else { //remove datapoint from request 
	                        tempInfo.convertDataPoints.splice(j, 1);
	                        j--;
	                    }
	                }
	            }
	            var ret = {};
	            for (var cdt in remoteInfoListMap) {
	                var infoList = remoteInfoListMap[cdt];
	                for (var k = 0, kl = infoList.length, info; k < kl; k++) {
	                    info = $.extend(true, {}, infoList[k]);
	                    info.convertDataPoints = this._duplicateRemoval(validDataPoints[cdt]);
	                    if (!ret[cdt]) {
	                        ret[cdt] = [];
	                    }
	                    ret[cdt].push(info);
	                }
	            }
	            return ret;
	        },
	        _getChartDataType: function (tickerObj, dataPoint, freq, days) {
	            if (!isNaN(freq)) {
	                if (days > 0) {
	                    return this.CHARTDATATYPE.REALTIME_INTRADAY_LAST;
	                } else {
	                    return this.CHARTDATATYPE.REALTIME_INTRADAY_RANGE;
	                }
	            } else {
	                var DataPoints = QSAPI.Widget.Chart.DataPoints;
	                var _requestKey = this._getRequestKey(tickerObj);
	                var isRTDTicker = this._isRTDTicker(_requestKey);
	                if (DataPoints.isPRICE(dataPoint)) {
	                    if (isRTDTicker) {
	                        return this.CHARTDATATYPE.REALTIME_HISTORICAL;
	                    } else {
	                        return this.CHARTDATATYPE.TS_OHLCV_PID;
	                    }
	                } else if (DataPoints.isAjustClosePrice(dataPoint)) {
	                    return this.CHARTDATATYPE.TS_WITH_PID;
	                } else if (DataPoints.isFundamental(dataPoint)) {
	                    return this.CHARTDATATYPE.FUNDAMENTAL_DIRECT;
	                } else if (DataPoints.isDividend(dataPoint) || DataPoints.isSplit(dataPoint) || DataPoints.isEarnings(dataPoint)) {
	                    return this.CHARTDATATYPE.TS_WITH_PID_FIXED_FREQUENCY;
	                } else if (dataPoint === DataPoints.TSPRICE) {
	                    //IT_HISTORICAL_FUND means need to add priceType=NAV&fallback=no, only the custom fund like ETF need add it.
	                    return tickerObj.isCustomFund ? this.CHARTDATATYPE.IT_HISTORICAL_FUND : this.CHARTDATATYPE.IT_HISTORICAL_STOCK;
	                } else if (dataPoint === DataPoints.TSRETURN) {
	                    return this.CHARTDATATYPE.IT_HISTORICAL_Cumulative;
	                } else if (dataPoint === DataPoints.TSDIVIDEND) {
	                    return this.CHARTDATATYPE.IT_HISTORICAL_DIVIDEND;
	                } else if (DataPoints.isDataAPIV1DataPoint(dataPoint)) {
	                    return this.CHARTDATATYPE.DATAAPI_SID;
	                } else if (DataPoints.isDataAPIV2DataPoint(dataPoint)) {
	                    return this.CHARTDATATYPE.DATAAPIV2_PID;
	                } else if (DataPoints.isRDBFundamentalDataPoint(dataPoint)) {
	                    return this.CHARTDATATYPE.FUNDAMENTAL_RDB;
	                } else {
	                    return this.CHARTDATATYPE.TS_WITH_PID;
	                }
	            }
	        },
	        _isRTDTicker: function (ticker) {
	            return /^\d+\.\d+\.\S+$/.test(ticker);
	        },
	        _toArray: function (obj) {
	            if (!obj) return [];
	            if (Object.prototype.toString.call(obj) === '[object Array]') {
	                return obj;
	            } else {
	                return [obj];
	            }
	        },
	        _isNeedSubscribe: function (tickerObj, chartObj) {
	            return !((chartObj.isFundCategory && chartObj.isFundCategory(tickerObj)) || (chartObj.isRelativeRiskBenchmark && chartObj.isRelativeRiskBenchmark(tickerObj)));
	        },
	        _validateTickerObject: function (tickerObj) {
	            if (!tickerObj || !tickerObj.valid) return false;
	            return true;
	        },
	        _cacheData: function (result, startIndex, endIndex, append) {
	            if (result.Status && result.Status.ErrorCode == 0) {
	                var attachment = result.Attachment || {};
	                var data = result.Data || [];
	                var ticker;
	                var cacheKeyArr;
	                for (var i = 0, l = data.length; i < l; i++) {
	                    ticker = data[i].Ticker;
	                    cacheKeyArr = [];
	                    //cache data to all relative cachekey
	                    for (var key in this._cache) {
	                        if ($.inArray(ticker, key.split(',')) > -1) {
	                            cacheKeyArr.push(key);
	                        }
	                    }
	                    for (var c = 0; c < cacheKeyArr.length; c++) {
	                        var cacheKey = cacheKeyArr[c];
	                        this._cache[cacheKey].setChartData(data[i], startIndex, endIndex, append, attachment.forceDelay == 1 ? true : false);
	                    }
	                }
	            }
	        },
	        _handleFrequency: function (chartDataType, frequency) {
	            if (chartDataType == this.CHARTDATATYPE.TS_WITH_PID) {
	                return "d";
	            }
	            return frequency;
	        },
	        _saveExtendData: function (result) {
	            var data = result.data || {};
	            var symbolData = data.symbol || {};
	            for (var symbol in symbolData) {
	                if (!this._cache[symbol]) {
	                    this._cache[symbol] = new QSAPI.Widget.Chart.TickerCache(symbol);
	                    this._cache[symbol].volumeDividend(this._volumeDividend);
	                }
	                this._cache[symbol].setExtendChartData(symbolData[symbol]);
	            }
	        },
	        _handleDrawingsResponse: function (list) {
	            list = list || [];
	            var drawings = {};
	            for (var i = 0, l = list.length; i < l; i++) {
	                var dp = $.evalJSON(list[i].eventInfo);
	                if (!drawings[dp.drawType]) {
	                    drawings[dp.drawType] = [];
	                }
	                dp.id = list[i].eventId;
	                drawings[dp.drawType].push(dp);
	            }
	            return drawings;
	        },
	        _getDrawings: function (tickerCache, dataPoints) {
	            var drawings = tickerCache.drawings();
	            var ret = {};
	            for (var o in drawings) {
	                if (typeof o != "string") continue;
	                var drawing = drawings[o];
	                if (drawing.length > 0) {
	                    ret[o] = [];
	                    for (var i = 0, l = drawing.length; i < l; i++) {
	                        if (o == "levelline") { //return the all levelline to chart.
	                            ret[o].push($.extend(true, {}, drawing[i]));
	                        } else {
	                            var point = $.extend(true, {}, drawing[i]);
	                            var findStart = false;
	                            var startKey = "start";
	                            var endKey = "end";
	                            if (point.start.dIdx > point.end.dIdx) {
	                                startKey = "end";
	                                endKey = "start";
	                            }
	                            for (var j = 0; j < dataPoints.length; j++) {
	                                if (dataPoints[j][dataPoints[j].length - 1] == point[startKey].dIdx) {
	                                    point[startKey].x = dataPoints[j][0];
	                                    findStart = true;
	                                } else if (dataPoints[j][dataPoints[j].length - 1] == point[endKey].dIdx && findStart) {
	                                    point[endKey].x = dataPoints[j][0];
	                                    ret[o].push(point);
	                                    break;
	                                }
	                            }
	                        }
	                    }
	                }
	            }
	            return ret;
	        },
	        _getCookie: function (c_name) {
	            var i, x, y, ARRcookies = document.cookie.split(";"),
	                value = null;
	            for (i = 0; i < ARRcookies.length; i++) {
	                x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
	                y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
	                x = x.replace(/^\s+|\s+$/g, "");
	                if (x == c_name) {
	                    value = unescape(y);
	                }
	            }
	            return value;
	        },
	        _setCookie: function (name, value, expires, path, domain, secure) {
	            var today = new Date();
	            today.setTime(today.getTime());
	            if (expires) {
	                expires = expires * 1000 * 60 * 60 * 24;
	            }
	            var expires_date = new Date(today.getTime() + (expires));
	            document.cookie = name + "=" + escape(value) +
	                ((expires) ? ";expires=" + expires_date.toGMTString() : "") + //expires.toGMTString()
	                ((path) ? ";path=" + path : "") +
	                ((domain) ? ";domain=" + domain : "") +
	                ((secure) ? ";secure" : "");
	        },
	        _handleSavedDrawPoint: function (point) {
	            if (point) { //make sure below attributes will be stored.
	                var validKeys = {
	                    "start": true,
	                    "end": true,
	                    "oldStart": true,
	                    "oldEnd": true,
	                    "drawType": true
	                };
	                for (var key in point) {
	                    if (!validKeys[key]) {
	                        point[key] = null;
	                        delete point[key];
	                    }
	                }
	            }
	        },
	        _getSavedDrawing: function (drawings, insert) {
	            var drawing = {},
	                tempDraw = {}.type;
	            if (insert) {
	                for (var o in drawings) {
	                    if (o != "opDom") {
	                        var type = QSAPI.Util.getType(drawings[o]);
	                        if (type == "object") {
	                            drawing[o] = $.extend(true, {}, drawings[o]);
	                        } else if (type == "array") {
	                            drawing[o] = drawings[o].slice();
	                        } else {
	                            drawing[o] = drawings[o]
	                        }
	                    }
	                }
	            } else {
	                for (var symbol in drawings) {
	                    drawing[symbol] = {};
	                    for (var draw in drawings[symbol]) {
	                        drawing[symbol][draw] = [];
	                        for (var i = 0, l = drawings[symbol][draw].length; i < l; i++) {
	                            tempDraw = {};
	                            for (var o in drawings[symbol][draw][i]) {
	                                if (o != "opDom") {
	                                    var type = QSAPI.Util.getType(drawings[o]);
	                                    if (type == "object") {
	                                        tempDraw[o] = $.extend(true, {}, drawings[symbol][draw][i][o]);
	                                    } else if (type == "array") {
	                                        tempDraw[o] = drawings[symbol][draw][i][o].slice();
	                                    } else {
	                                        tempDraw[o] = drawings[symbol][draw][i][o];
	                                    }
	                                }
	                            }
	                            drawing[symbol][draw].push(tempDraw);
	                        }
	                    }
	                }
	            }
	            return drawing;
	        },
	        _extractRealtimeTicker: function (tickers) {
	            var ret = [];
	            for (var i = 0, l = tickers.length, ticker; i < l; i++) {
	                ticker = tickers[i].split(',');
	                for (var j = 0, jl = ticker.length, t; j < jl; j++) {
	                    t = ticker[j];
	                    if (/^\d+\.\d+\..+$/.test(t)) {
	                        ret.push(t);
	                        break;
	                    }
	                }
	            }
	            return ret;
	        },
	        _findChartObjs: function (ticker) {
	            var charts = [];
	            for (var key in this._registedCharts) {
	                if (key.indexOf(ticker) > -1) {
	                    charts = charts.concat(this._registedCharts[key]['charts'] || []);
	                }
	            }
	            return charts;
	        },
	        _query: function (tickers, taskId) {
	            var self = this;
	            var taskCfg = QSAPI.Task.getCfg(taskId);
	            var showPre = taskCfg.showPre;
	            var freq = 2 * taskCfg["period"] / this.MINUTEMSECONDS;
	            var forceDelay = taskCfg['forceDelay'];
	            var dateRange = this._getDateRange(freq, tickers, forceDelay, showPre);
	            for (var date in dateRange) {
	                var tickers = this._extractRealtimeTicker(dateRange[date].tickers);
	                var time = dateRange[date].time;
	                var para = {
	                    tickers: tickers.join(","),
	                    f: freq,
	                    cdt: this.CHARTDATATYPE.REALTIME_INTRADAY_RANGE,
	                    sd: dateRange[date].date,
	                    //ed:dateRnage["date"],
	                    st: this._convertTime(time)
	                };
	                if (showPre) {
	                    para.preAfter = true;
	                    para.listingMarket = dateRange[date].listingMarket;
	                }
	                var isFODRequest = QSAPI.isFod();
	                var interfaceUrl = this.dataDomain + this.dataUrls.chart;
	                if (isFODRequest) {
	                    para = this._getFODParameter(para);
	                    interfaceUrl = this.dataUrls.getFODTimeseries();
	                }
	                var requestInfo = {
	                    frequency: freq
	                };
	                (function (para, forceDelay) {
	                    forceDelay && (para['forceDelay'] = 1);
	                    self._requestData(interfaceUrl, para, function (result) {
	                        if (isFODRequest && QSAPI.fod.transformFodToChartData) {
	                            result = QSAPI.fod.transformFodToChartData(result, requestInfo);
	                        }
	                        var _tickers = self._updateTickers(result);
	                        if (_tickers.length > 0) {
	                            //cahce the update data
	                            self._cacheData(result, -1, -1, true);
	                            //refresh the chart
	                            for (var i = 0, l = _tickers.length, ticker; i < l; i++) {
	                                ticker = _tickers[i];
	                                var chartObjs = self._findChartObjs(ticker);
	                                for (var j = 0, jl = chartObjs.length, chartObj, fd; j < jl; j++) {
	                                    chartObj = chartObjs[j] || {};
	                                    if (chartObj.autoRefresh === false) continue;
	                                    fd = QSAPI.Util.getValidForceDelay(chartObj.forceDelay);
	                                    if (typeof (chartObj.refreshChart) == "function" && fd === forceDelay) {
	                                        chartObj.refreshChart();
	                                    }
	                                }
	                            }
	                        }
	                    });
	                })(para, forceDelay);
	            }
	        },
	        _updateTickers: function (result) {
	            var tickers = [];
	            if (result.Status && result.Status.ErrorCode == 0 && result.Data && result.Data.length > 0) {
	                for (var i = 0, l = result.Data.length, d; i < l; i++) {
	                    d = result.Data[i];
	                    if (d.DailyData && d.DailyData.LastPrice.length > 0 && (d.DailyData.LastPrice[0].SubDataPoints || []).length > 0) {
	                        tickers.push(d.Ticker);
	                    }
	                }
	            }
	            return tickers;
	        },
	        _convertTime: function (time) {
	            var hour = parseInt(time / 60, 10);
	            var minute = time % 60;
	            return (hour < 10 ? ("0" + hour) : hour) + ":" + (minute < 10 ? ("0" + minute) : minute) + ":" + "00";
	        },
	        _getDateRange: function (freq, tickers, forceDelay, showPre) {
	            var date = 0;
	            var tickerDateRange = {};
	            for (var i = 0, l = tickers.length, tickerCache; i < l; i++) {
	                tickerCache = this._cache[tickers[i]];
	                if (tickerCache) {
	                    var dateTime = tickerCache.getLastMinutelyDateTime(freq, forceDelay);
	                    var tickerObj = tickerCache.tickerObject;
	                    if (!dateTime) continue;
	                    date = dateTime["date"];
	                    if (showPre) {
	                        date += ('_' + tickerObj.listMarket);
	                    }
	                    if (!tickerDateRange[date]) {
	                        tickerDateRange[date] = {};
	                    }
	                    tickerDateRange[date].date = dateTime["date"];
	                    tickerDateRange[date].listingMarket = tickerObj.listMarket;
	                    if (tickerDateRange[date].time >= 0) {
	                        tickerDateRange[date].time = Math.min(tickerDateRange[date].time, dateTime["time"] + freq);
	                    } else {
	                        tickerDateRange[date].time = dateTime["time"] + freq;
	                    }
	                    tickerDateRange[date].tickers = tickerDateRange[date].tickers || [];
	                    tickerDateRange[date].tickers.push(tickers[i]);
	                }
	            }
	            return tickerDateRange;
	        },
	        _getChannels: function (tickerObjects) {
	            var channels = [];
	            for (var i = 0, l = tickerObjects.length, rtdTicker; i < l; i++) {
	                if (this._isMutualFund(tickerObjects[i])) continue;
	                rtdTicker = this._getRTDTicker(tickerObjects[i]);
	                if (rtdTicker) {
	                    channels.push(rtdTicker);
	                }
	            }
	            return channels;
	        },
	        _getTaskId: function (id) {
	            return this.subscribeID + '_' + id;
	        },
	        _getActiveTickers: function (id) {
	            var tickers = [];
	            for (var ticker in this._registedCharts) {
	                if (this._registedCharts[ticker]['subscribable']) {
	                    var charts = this._registedCharts[ticker]['charts'];
	                    for (var i = 0, l = charts.length, _id; i < l; i++) {
	                        _id = charts[i].id;
	                        if (_id == id) {
	                            tickers.push(ticker);
	                        }
	                    }
	                }
	            }
	            return tickers;
	        }
	    };
	
	    QSAPI.Widget.Chart.TickerCache = function (ticker) {
	        this.ticker = ticker; //could be 126.1.IBM, performance id or any unique symbol
	        this.timeDiff = 0;
	        this.openTime = 570;
	        this.closeTime = 960;
	        this.preOpenTime = 570;
	        this.afteCloseTime = 960;
	        this.hasPreAfter = false;
	        this.freqDataMap = {};
	        this.freqExtendDataMap = {};
	        this.freqTemporaryDataMap = {};
	        this._drawings = {};
	        this._volumeDividend = 1000000;
	        this.tickerObject = null;
	        this._forceDelay = {};
	        this.baseCurrency = null;
	    };
	
	    QSAPI.Widget.Chart.TickerCache.prototype = {
	        forceDelay: function (forceDelay, value) {
	            if (!arguments.length) {
	                return this._forceDelay;
	            }
	            if (value === false) {
	                this._forceDelay[forceDelay] = null;
	                delete this._forceDelay[forceDelay];
	            }
	            this._forceDelay[forceDelay] = true;
	        },
	        volumeDividend: function (value) {
	            if (!arguments.length) {
	                return this._volumeDividend;
	            }
	            this._volumeDividend = value;
	        },
	        drawings: function (value) {
	            if (!arguments.length) {
	                return this._drawings;
	            }
	            this._drawings = value;
	        },
	        saveDrawing: function (drawing) {
	            var type = drawing.drawType;
	            if (type) {
	                if (!this._drawings[type]) {
	                    this._drawings[type] = [];
	                }
	                this._drawings[type].push(drawing);
	            }
	        },
	        lastTradeDate: function (freq, forceDelay) {
	            if (this.freqDataMap[freq]) {
	                forceDelay = QSAPI.Util.getValidForceDelay(forceDelay);
	                var data = this.freqDataMap[freq][QSAPI.Widget.Chart.DataPoints.PRICE];
	                if (data) {
	                    var ts = data[forceDelay];
	                    var lastPoint = ts.lastPoint();
	                    if (lastPoint) {
	                        return QSAPI.Widget.Chart.Util.toDateFromIndex(lastPoint["Index"]);
	                    }
	                }
	            }
	            return new Date();
	        },
	        getLastMinutelyDateTime: function (freq, forceDelay) {
	            if (this.freqDataMap[freq]) {
	                forceDelay = QSAPI.Util.getValidForceDelay(forceDelay);
	                var data = this.freqDataMap[freq][QSAPI.Widget.Chart.DataPoints.PRICE];
	                var ts = data[forceDelay];
	                var lastPoint = ts.lastPoint();
	                if (lastPoint) {
	                    var lastSubPoint = ts.lastSubPoint(lastPoint);
	                    if (lastSubPoint) {
	                        var ret = {};
	                        ret["date"] = lastSubPoint["OriginalDate"] || parseInt(lastPoint["Date"], 10);
	                        var timeArray = lastSubPoint["OriginalTime"].split(":");
	                        ret["time"] = parseInt(timeArray[0], 10) * 60 + parseInt(timeArray[1], 10);
	                        return ret;
	                    }
	                }
	            }
	            return null;
	        },
	        getIPODateIndex: function () {
	            return null;
	        },
	        updateDrawing: function (type, drawing) {
	            var updated = false;
	            if (this._drawings[type]) {
	                for (var i = 0; i < this._drawings[type].length; i++) {
	                    var d = this._drawings[type][i];
	                    if (type == "levelline") { // update the levelline.
	                        if (d.start && drawing.oldStart && Math.abs(d.start.y - drawing.oldStart.y) < 0.0001) { // when the differ<0.0001 we regard the two digtils is equal.
	                            this._drawings[type][i].start = $.extend(true, {}, drawing.start);
	                            updated = true;
	                            break;
	                        }
	                    } else if (d.start.dIdx == drawing.oldStart.dIdx && d.end.dIdx == drawing.oldEnd.dIdx && Math.abs(d.start.y - drawing.oldStart.y) < 0.0001 && Math.abs(d.end.y - drawing.oldEnd.y) < 0.0001) {
	                        this._drawings[type][i].start = $.extend(true, {}, drawing.start);
	                        this._drawings[type][i].end = $.extend(true, {}, drawing.end);
	                        updated = true;
	                        break;
	                    }
	                }
	            }
	            return updated;
	        },
	        removeDrawing: function (type, drawing) {
	            var removed = false;
	            if (this._drawings[type]) {
	                for (var i = 0; i < this._drawings[type].length; i++) {
	                    var d = this._drawings[type][i];
	                    if (type == "levelline") {
	                        if (d.start.y == drawing.start.y) {
	                            this._drawings[type].splice(i, 1);
	                            removed = true;
	                            break;
	                        }
	                    } else if (d.start.dIdx == drawing.start.dIdx && d.end.dIdx == drawing.end.dIdx && d.start.y == drawing.start.y && d.end.y == drawing.end.y) {
	                        this._drawings[type].splice(i, 1);
	                        removed = true;
	                        break;
	                    }
	                }
	            }
	            return removed;
	        },
	        setExtendChartData: function (data) {
	            data = data || [];
	            var d, freq, date, time, newValue;
	            for (var i = 0, l = data.length; i < l; i++) {
	                d = data[i];
	                freq = d.freq;
	                date = d.date;
	                time = d.time;
	                newValue = $.evalJSON(d.newValue);
	                newValue.id = d.id;
	                if (!this.freqExtendDataMap[freq]) {
	                    this.freqExtendDataMap[freq] = {};
	                }
	                if (!this.freqExtendDataMap[freq][date]) {
	                    this.freqExtendDataMap[freq][date] = {};
	                }
	                this.freqExtendDataMap[freq][date][time] = newValue;
	            }
	        },
	        removeExtendChartData: function (data) {
	            var freq = data.freq;
	            var date = data.date;
	            var time = data.time;
	            if (this.freqExtendDataMap[freq] && this.freqExtendDataMap[freq][date] && this.freqExtendDataMap[freq][date][time]) {
	                this.freqExtendDataMap[freq][date][time] = null;
	                delete this.freqExtendDataMap[freq][date][time];
	                if ($.isEmptyObject(this.freqExtendDataMap[freq][date])) {
	                    this.freqExtendDataMap[freq][date] = null;
	                    delete this.freqExtendDataMap[freq][date];
	                }
	                if ($.isEmptyObject(this.freqExtendDataMap[freq])) {
	                    this.freqExtendDataMap[freq] = null;
	                    delete this.freqExtendDataMap[freq];
	                }
	                if ($.isEmptyObject(this.freqExtendDataMap)) {
	                    this.freqExtendDataMap = null;
	                    delete this.freqExtendDataMap;
	                }
	            }
	        },
	        setChartData: function (data, startIndex, endIndex, append, forceDelay) {
	            forceDelay = QSAPI.Util.getValidForceDelay(forceDelay);
	            this.timeDiff = data.TimeDiff !== undefined && data.TimeDiff !== "" ? parseInt(data.TimeDiff, 10) : this.timeDiff;
	            this.openTime = data.OpenMinutes > -1 ? data.OpenMinutes : this.openTime;
	            this.closeTime = data.CloseMinutes > -1 ? data.CloseMinutes : this.closeTime;
	            this.preOpenTime = data.PreMinutes > -1 ? data.PreMinutes : this.preOpenTime;
	            this.afteCloseTime = data.AfterMinutes > -1 ? data.AfterMinutes : this.afteCloseTime;
	            this.hasPreAfter = data.HasPreafter || this.hasPreAfter;
	
	            var frequency = data.Frequency;
	            if (typeof frequency != "undefined") {
	                if (!this.freqDataMap[frequency]) {
	                    this.freqDataMap[frequency] = {};
	                }
	                var dailyData = data.DailyData || {};
	                var DataPoints = QSAPI.Widget.Chart.DataPoints;
	                var splits = {};
	                for (var dp in dailyData) {
	                    var cacheData = dailyData[dp];
	                    $.each(cacheData, function (i, d) {
	                        d.DataType = data.DataType;
	                        var hasSubDataPoints = d.SubDataPoints && d.SubDataPoints.length;
	                        if (hasSubDataPoints) {
	                            var hasValidSubPoints = false;
	                            for (var i = 0; i < d.SubDataPoints.length; i++) {
	                                if (d.SubDataPoints[i].FillFlag === 0) {
	                                    hasValidSubPoints = true;
	                                    break;
	                                }
	                            }
	                            if (!hasValidSubPoints) {
	                                d.SubDataPoints = [];
	                            }
	                        }
	                    });
	                    if (DataPoints.isSplit(dp)) {
	                        splits[dp] = dailyData[dp];
	                    } else {
	                        if (!this.freqDataMap[frequency][dp]) {
	                            this.freqDataMap[frequency][dp] = {};
	                        }
	                        if (!this.freqDataMap[frequency][dp][forceDelay]) {
	                            this.freqDataMap[frequency][dp][forceDelay] = new QSAPI.Widget.Chart.Tiemseries();
	                        }
	                        var clearCache = false;
	                        if (DataPoints.isNeedClearCache(dp)) {
	                            clearCache = true;
	                        }
	                        this.freqDataMap[frequency][dp][forceDelay][append ? "append" : "set"](cacheData, startIndex, endIndex, clearCache);
	                    }
	                }
	
	                //combine the split from and split to as one data point.
	                var splitTo = splits[DataPoints.SPLITTO],
	                    splitFrom = splits[DataPoints.SPLITFROM];
	                if (splitTo && splitFrom) {
	                    var sdp = DataPoints.SPLITFROM + ":" + DataPoints.SPLITTO;
	                    var sdailyData = {};
	                    sdailyData[sdp] = [];
	                    var _map = {};
	                    for (var i = 0, l = splitFrom.length; i < l; i++) {
	                        _map[splitFrom[i].Index] = splitFrom[i];
	                    }
	
	                    for (var i = 0, l = splitTo.length, sto; i < l; i++) {
	                        sto = splitTo[i];
	                        var d = {
	                            Index: sto.Index,
	                            Date: sto.Date
	                        };
	                        d[DataPoints.SPLITFROM] = _map[sto.Index].Last;
	                        d[DataPoints.SPLITTO] = sto.Last;
	                        sdailyData[sdp].push(d);
	                    }
	                    if (!this.freqDataMap[frequency][sdp]) {
	                        this.freqDataMap[frequency][sdp] = {};
	                    }
	                    if (!this.freqDataMap[frequency][sdp][forceDelay]) {
	                        this.freqDataMap[frequency][sdp][forceDelay] = new QSAPI.Widget.Chart.Tiemseries();
	                    }
	                    this.freqDataMap[frequency][sdp][forceDelay][append ? "append" : "set"](sdailyData[sdp], startIndex, endIndex);
	                }
	            }
	        },
	        checkDrawingsReady: function () {
	            return false;
	        },
	        checkDataReady: function (frequency, dataPoint, startIndex, endIndex, days, forceDelay) {
	            forceDelay = QSAPI.Util.getValidForceDelay(forceDelay);
	            var DataPoints = QSAPI.Widget.Chart.DataPoints;
	            var rFreq = frequency;
	            if (DataPoints.isDividend(dataPoint) || DataPoints.isSplit(dataPoint) || DataPoints.isEarnings(dataPoint)) {
	                rFreq = "d";
	            }
	            if (!this.freqDataMap[rFreq] || !this.freqDataMap[rFreq][dataPoint] || !this.freqDataMap[rFreq][dataPoint][forceDelay]) return false;
	            return this.freqDataMap[rFreq][dataPoint][forceDelay].isReady(startIndex, endIndex, days);
	        },
	        getIndicatorData: function (rdata, indicator, freq, sIndex, eIndex, days, showPre, forceDelay) {
	            rdata = rdata || {};
	            var DataPoints = QSAPI.Widget.Chart.DataPoints;
	            var IndicatorCalculation = QSAPI.Widget.Chart.IndicatorCalculation;
	            var chartDataManager = QSAPI.Widget.Chart.DataManager;
	            var indicatorName = indicator.name;
	            var pType = indicator.pType;
	            var isFilled = true;
	            var price = rdata.price;
	            var volume = rdata.volume;
	            var dividendAndSplit = rdata.dividendAndSplit;
	            var isfund;
	            if (rdata.tickerObject) {
	                var mType = rdata.tickerObject.mType;
	                isfund = rdata.tickerObject.type === 8 && (!mType || mType == "FO" || mType == "SA");
	            }
	            var chartType = isfund ? "" : "stockorforex";
	            var dp = {
	                real: DataPoints.parse("p", pType, chartType),
	                origin: pType,
	                growthBaseValue: indicator.growthBaseValue
	            };
	            var volumeDp = $.extend(true, {}, dp, {
	                origin: 'volume'
	            });
	            var needFilter = DataPoints.needFilter(indicatorName);
	            if (!needFilter && isNaN(freq)) {
	                var spos = this.getEODData(dp, freq, sIndex, eIndex).spos;
	                price = this.getEODData(dp, freq, 0, Infinity, spos); // need a larger range to calcaute historical indicators. The range should be more specific and need to be improved.
	                volume = this.getEODData(volumeDp, freq, 0, Infinity, spos);
	            }
	            if (!price) {
	                if (!isNaN(freq)) {
	                    price = this.getIntradayData(dp, freq, sIndex, eIndex, days, showPre, forceDelay);
	                    isFilled = false;
	                } else {
	                    price = this.getEODData(dp, freq, needFilter ? sIndex : 0, needFilter ? eIndex : Infinity);
	                }
	            }
	            if (!volume && this._isRelatedVolumeIndicator(indicatorName)) {
	                if (!isNaN(freq)) {
	                    volume = this.getIntradayData(volumeDp, freq, sIndex, eIndex, days, showPre, forceDelay);
	                    isFilled = false;
	                } else {
	                    volume = this.getEODData(volumeDp, freq, needFilter ? sIndex : 0, needFilter ? eIndex : Infinity);
	                }
	            }
	            if (!dividendAndSplit && this._isRelatedDividendIndicator(indicatorName)) {
	                dividendAndSplit = this.getDividendAndSplitData([DataPoints.DIVIDEND], "d", sIndex, eIndex);
	            }
	
	            price = price || {};
	            volume = volume || {};
	            dividendAndSplit = dividendAndSplit || [];
	            var pData = price.data || [];
	            var vData = volume.data || [];
	            if (this._isRelatedVolumeIndicator(indicatorName) && vData.length < pData.length) {
	                pData = pData.slice(pData.length - voLen.length);
	            }
	            if (indicatorName == "VBP") {
	                return {
	                    data: IndicatorCalculation.calculateVolByPrice(pData, vData, freq, 1),
	                    spos: null,
	                    freq: freq
	                };
	            } else if (indicatorName == "PrevClose") {
	                return {
	                    data: this._getPreviousCloseData(pData, freq, sIndex, eIndex, days, showPre, forceDelay),
	                    spos: null,
	                    freq: freq
	                }
	            } else if (indicatorName == "RDividend") {
	                return {
	                    data: this._getRollDividendData(pData, dividendAndSplit, freq, sIndex, eIndex),
	                    spos: null,
	                    freq: freq
	                }
	            } else {
	                var spos = price.spos;
	                var startIndex = sIndex;
	                var endIndex = eIndex;
	                if (!isNaN(freq) && pData.length > 0) {
	                    startIndex = pData[0][pData[0].length - 1];
	                    endIndex = pData[pData.length - 1][pData[pData.length - 1].length - 1];
	                }
	                var indData = this._calculateIndicator(indicatorName, pData, vData, dividendAndSplit, freq, indicator["p"]);
	                var ret = [];
	                for (var i = 0, l = indData.length; i < l; i++) {
	                    ret.push(IndicatorCalculation.filtrateCalculatedData(startIndex, endIndex, isFilled, indData[i], freq, pType, spos));
	                }
	                if (ret.length > 1) {
	                    return ret;
	                } else if (ret.length == 1) {
	                    return ret[0];
	                } else {
	                    return {
	                        data: [],
	                        spos: null,
	                        freq: freq
	                    };
	                }
	            }
	        },
	        getDividendAndSplitData: function (dataPoints, freq, sIndex, eIndex) {
	            var forceDelay = false;
	            var ret = [];
	            var DataPoints = QSAPI.Widget.Chart.DataPoints;
	            for (var i = 0, l = dataPoints.length; i < l; i++) {
	                var dp = dataPoints[i];
	                var ts = this.freqDataMap[freq] && this.freqDataMap[freq][dp] && this.freqDataMap[freq][dp][forceDelay] ? this.freqDataMap[freq][dp][forceDelay] : null;
	                if (ts) {
	                    var type = (DataPoints.isDividend(dp) || DataPoints.isTSDividend(dp)) ? 1 : 2;
	                    var data = this._truncate(ts.list(), sIndex, eIndex);
	                    var d, md;
	                    for (var j = 0, jl = data.length; j < jl; j++) {
	                        d = data[j];
	                        md = {
	                            DateIndex: d.Index,
	                            Desc: type == 1 ? d.Last : (parseInt(d[DataPoints.SPLITFROM], 10) + ":" + parseInt(d[DataPoints.SPLITTO], 10)),
	                            Type: type
	                        };
	                        ret.push(md);
	                    }
	                }
	            }
	            if (l > 1) {
	                ret.sort(function (a, b) {
	                    if (a.DateIndex < b.DateIndex) {
	                        return -1;
	                    } else if (a.DateIndex == b.DateIndex) {
	                        return 0;
	                    } else {
	                        return 1;
	                    }
	                });
	            }
	            return ret;
	        },
	        getSemiAnnualEPS: function (dp, freq, sIndex, eIndex) {
	            var forceDelay = false;
	            var ret = [];
	            var DataPoints = QSAPI.Widget.Chart.DataPoints;
	            var data = [],
	                sdata = [],
	                spos = null;
	            var exts = this._getExtendData(freq);
	            var dataPoints = dp.real && dp.real.split(',');
	            for (var i = 0; i < dataPoints.length; i++) {
	                var dp = dataPoints[i];
	                var ts = dp && this.freqDataMap[freq] && this.freqDataMap[freq][dp] && this.freqDataMap[freq][dp][forceDelay] ? this.freqDataMap[freq][dp][forceDelay] : null;
	                if (ts) {
	                    var dataList = this._truncate(ts.list(), sIndex, eIndex);
	                    for (var j = 0; j < dataList.length; j++) {
	                        if (DataPoints.isAnnualEPS(dp)) {
	                            dataList[j].frequency = "annual";
	                            if (j >= 1) {
	                                dataList[j].upDown = Number(dataList[j].Last) >= Number(dataList[j - 1].Last) ? 1 : 0;
	                            } else {
	                                dataList[j].upDown = 1;
	                            }
	                        } else if (DataPoints.isSemiAnnualEPS(dp)) {
	                            dataList[j].frequency = "semi-annual";
	                            if (j >= 2) {
	                                dataList[j].upDown = Number(dataList[j].Last) >= Number(dataList[j - 2].Last) ? 1 : 0; // 1: up, 0:down
	                            } else {
	                                dataList[j].upDown = 1;
	                            }
	                        }
	                    }
	                    data = data.concat(dataList);
	                }
	            }
	            data.sort(function (a, b) {
	                return a.Index - b.Index;
	            });
	
	            for (var i = 1; i < data.length; i++) {
	                if (data[i].Index === data[i - 1].Index) {
	                    if (data[i].frequency === 'annual') {
	                        data.splice(i, 1);
	                    } else {
	                        data.splice(i - 1, 1);
	                    }
	                }
	            }
	            for (var i = 0, index = 0, l = data.length; i < l; i++) {
	                var d = data[i];
	                var md = [];
	                var date = QSAPI.Widget.Chart.Util.convertToDate(d.Date);
	                if (spos == null) {
	                    spos = d.PreviousClosePrice;
	                }
	                var exMDataMap = exts[QSAPI.Widget.Chart.Util.formatDateYMD(date)] || {};
	                var exMData = exMDataMap[0] || {};
	                spos = this._calculateStartPositionPrice(spos, d, exMData);
	                md.push(index);
	                md.push(d.Last);
	                md.push(d.upDown);
	                md.push(d.frequency);
	                md.push(d.Index); //date index
	                sdata.push(md);
	                index++;
	            }
	            return {
	                data: sdata,
	                spos: spos
	            };
	        },
	        getEODData: function (dataPoints, freq, sIndex, eIndex, spos) {
	            var validEposList = [];
	            var sdata = [],
	                originalPriceData = [],
	                epos = null;
	            var exts = this._getExtendData(freq);
	            var dp = dataPoints.real;
	            var type = dataPoints.origin;
	            var forceDelay = false;
	            var dataCurrency;
	            var ts = dp && this.freqDataMap[freq] && this.freqDataMap[freq][dp] && this.freqDataMap[freq][dp][forceDelay] ? this.freqDataMap[freq][dp][forceDelay] : null;
	            var chartDataPoints = QSAPI.Widget.Chart.DataPoints;
	            var lastTradeTime;
	            if (chartDataPoints.needCurrency(dp)) {
	                dataCurrency = this.freqDataMap[freq] && this.freqDataMap[freq][chartDataPoints.CURRENCY] && this.freqDataMap[freq][chartDataPoints.CURRENCY][forceDelay] && this.freqDataMap[freq][chartDataPoints.CURRENCY][forceDelay]["_dataList"][0] && this.freqDataMap[freq][chartDataPoints.CURRENCY][forceDelay]["_dataList"] && this.freqDataMap[freq][chartDataPoints.CURRENCY][forceDelay]["_dataList"][0]["Last"];
	            }
	            if (ts) {
	                var data = this._truncate(ts.list(), sIndex, eIndex);
	                if (data.length > 0) {
	                    lastTradeTime = QSAPI.Widget.Chart.Util.toDateFromIndex(data[data.length - 1].Index);
	                }
	                var lastClosePrice = null;
	                for (var i = 0, index = 0, l = data.length; i < l; i++) {
	                    var d = data[i];
	                    var md = [];
	                    var date = QSAPI.Widget.Chart.Util.convertToDate(d.Date);
	                    if (spos == null) {
	                        spos = d.PreviousClosePrice;
	                    }
	                    if (lastClosePrice == null) {
	                        lastClosePrice = d.PreviousClosePrice;
	                    }
	                    var exMDataMap = exts[QSAPI.Widget.Chart.Util.formatDateYMD(date)] || {};
	                    var exMData = exMDataMap[0] || {};
	                    var upFlag = Number(d.Last) > Number(lastClosePrice) ? 1 : 0;
	                    lastClosePrice = d.Last;
	                    spos = this._calculateStartPositionPrice(spos, d, exMData);
	                    epos = this._getPositionPrice(d, exMData);
	                    if (epos) {
	                        validEposList.push(epos);
	                    }
	                    if (type == "price" || /^return/.test(type) || type == "p_nav" || type == "p_price" || type == "tsc" || type == "k_tsc" || type == 'tsp' || /fact_price/.test(type)) {
	                        md.push(index);
	                        this._fillCHLO(md, type, spos, d, exMData, dataPoints.growthBaseValue);
	                        md.push(upFlag); //up/down flag
	                        md.push(exMData.id > 0 ? exMData.id : -1); //if this point has been updated by use. set the update Id
	                        md.push(d.Index); //date index
	                    } else if (type == "volume") {
	                        md.push(index);
	                        md.push((exMData.v || d.Volume) / this._volumeDividend);
	                        md.push(upFlag); //up/down flag
	                        md.push(d.Index); //date index
	                    } else if (type == "e") { //earnings
	                        md.push(index);
	                        md.push(d.Last);
	                        if (i > 3) {
	                            md.push(Number(d.Last) >= Number(data[i - 4].Last) ? 1 : 0); // 1: up, 0:down
	                        } else {
	                            md.push(1);
	                        }
	                        md.push("quarterly");
	                        md.push(d.Index); //date index  
	                    } else { //fundamental or nav
	                        var dataValue = (dataCurrency && dataCurrency == "GBP" && this.baseCurrency == "GBX") ? 100 * parseFloat(d.Last, 10) : parseFloat(d.Last, 10); //need backend to handle to float
	                        md.push(index);
	                        md.push(dataValue);
	                        md.push(d.Index); //date index
	                    }
	                    sdata.push(md);
	                    originalPriceData.push({
	                        dateIndex: d.Index,
	                        value: d.Last
	                    });
	                    index++;
	                }
	            }
	            if (validEposList.length > 0) {
	                epos = validEposList[validEposList.length - 1];
	            } else {
	                epos = null;
	            }
	            return {
	                data: sdata,
	                originalPrice: originalPriceData,
	                spos: spos,
	                freq: freq,
	                epos: epos,
	                lastTradeTime: lastTradeTime
	            };
	        },
	        getIntradayData: function (dataPoints, freq, sIndex, eIndex, days, showPre, forceDelay, isFundTicker) {
	            var validEposList = [];
	            var sdata = [],
	                originalPriceData = [],
	                spos = null,
	                epos = null;
	            var cachedFreq = isFundTicker ? 'd' : freq;
	            var exts = this._getExtendData(cachedFreq);
	            var dp = dataPoints.real;
	            var type = dataPoints.origin;
	            var DataPoint = QSAPI.Widget.Chart.DataPoints;
	            var lastTradeTime;
	            if (dp != DataPoint.POSTTAX) { //intraday don't support postTax
	                var ts = dp && this.freqDataMap[cachedFreq] && this.freqDataMap[cachedFreq][dp] && this.freqDataMap[cachedFreq][dp][forceDelay] ? this.freqDataMap[cachedFreq][dp][forceDelay] : null;
	                if (ts) {
	                    var data = this._truncate(ts.list(), sIndex, eIndex, days, isFundTicker);
	                    if (isFundTicker) {
	                        for (var i = data.length - 1; i >= 1; i--) {
	                            data[i].PreviousClosePrice = data[i - 1].Last;
	                        }
	                        data.shift();
	                    }
	                    for (var i = 0, index = 0, l = data.length; i < l; i++) {
	                        var d = data[i];
	                        if (!d.SubDataPoints || d.SubDataPoints.length == 0) {
	                            if (isNaN(cachedFreq)) {
	                                d.SubDataPoints = [];
	                                for (var time = this.openTime; time < this.closeTime; time++) {
	                                    d.SubDataPoints.push({
	                                        Date: null,
	                                        FillFlag: 0,
	                                        High: null,
	                                        Index: 0,
	                                        Last: d.Last,
	                                        LastMarket: null,
	                                        Low: null,
	                                        Open: null,
	                                        OriginalDate: d.OriginalDate,
	                                        OriginalDateTime: d.OriginalDateTime,
	                                        OriginalTime: d.OriginalTime,
	                                        PreviousClosePrice: null,
	                                        SubDataPoints: null,
	                                        Time: time,
	                                        Volume: null
	                                    });
	                                }
	                            } else {
	                                continue;
	                            }
	                        };
	                        var date = QSAPI.Widget.Chart.Util.convertToDate(d.Date);
	                        var tick = date.getTime();
	                        var lastSubDataPoint = d.SubDataPoints[d.SubDataPoints.length - 1];
	                        lastTradeTime = tick + lastSubDataPoint.Time * 60000;
	                        var mDataList = this._truncatePreAfter(d.SubDataPoints, freq, showPre);
	
	                        var lastClosePrice = null;
	                        if (spos == null && type !== "return_k") {
	                            spos = d.PreviousClosePrice;
	                        }
	                        if (lastClosePrice == null) {
	                            lastClosePrice = d.PreviousClosePrice;
	                        }
	                        var exMDataMap = exts[QSAPI.Widget.Chart.Util.formatDateYMD(date)] || {};
	                        for (var j = 0, jl = mDataList.length; j < jl; j++) {
	                            var md = [];
	                            var mData = mDataList[j];
	                            var exMData = exMDataMap[mData.Time] || {};
	                            spos = this._calculateStartPositionPrice(spos, mData, exMData);
	                            epos = this._getPositionPrice(mData, exMData);
	                            if (epos) {
	                                validEposList.push(epos);
	                            }
	                            var upFlag = mData.Last > lastClosePrice ? 1 : 0;
	                            lastClosePrice = mData.Last;
	                            if (type == "volume") {
	                                md.push(index); //index 
	                                md.push((exMData.v || mData.Volume) / this._volumeDividend);
	                                md.push(upFlag); //up/down flag
	                                md.push(tick + mData.Time * 60000); //time
	                                if (mData.Time == 1440) { // for futures, we need reduce the tick of end point.
	                                    md[md.length - 1] = md[md.length - 1] - 6000;
	                                }
	                            } else {
	                                md.push(index); //index
	                                this._fillCHLO(md, type, spos, mData, exMData, dataPoints.growthBaseValue); //CHLO
	                                md.push(upFlag); //up/down flag
	                                md.push(mData.FillFlag); // fill flag
	                                md.push(exMData.id > 0 ? exMData.id : -1); //if this point has been updated by use. set the update Id
	                                md.push(tick + mData.Time * 60000); //time
	                                if (mData.Time == 1440) { // for futures, we need reduce the tick of end point.
	                                    md[md.length - 1] = md[md.length - 1] - 6000;
	                                }
	                            }
	                            sdata.push(md);
	                            originalPriceData.push({
	                                dateIndex: tick + mData.Time * 60000,
	                                value: mData.Last
	                            });
	                            index++;
	                        }
	                    }
	                }
	            }
	            if (validEposList.length > 0) {
	                epos = validEposList[validEposList.length - 1];
	            } else {
	                epos = null;
	            }
	            return {
	                data: sdata,
	                originalPrice: originalPriceData,
	                spos: spos,
	                days: days,
	                freq: freq,
	                epos: epos,
	                lastTradeTime: lastTradeTime
	            };
	        },
	        _getPreviousCloseData: function (pDataList, freq, sIndex, eIndex, days, forceDelay) {
	            forceDelay = QSAPI.Util.getValidForceDelay(forceDelay);
	            var ret = [];
	            if (pDataList.length > 0) { //only intraday could have previous close indicator
	                var dp = QSAPI.Widget.Chart.DataPoints.PRICE;
	                var ts = this.freqDataMap[freq] && this.freqDataMap[freq][dp] && this.freqDataMap[freq][dp][forceDelay] ? this.freqDataMap[freq][dp][forceDelay] : null;
	                if (ts) {
	                    var data = this._truncate(ts.list(), sIndex, eIndex, days);
	                    if (data.length > 0) {
	                        var previousClosePrice = data[0].PreviousClosePrice;
	                        if (previousClosePrice > 0) {
	                            for (var i = 0, l = pDataList.length; i < l; i++) {
	                                ret.push([pDataList[i][0], previousClosePrice, pDataList[i][l - 1]]);
	                            }
	                        }
	                    }
	                }
	            }
	            return ret;
	        },
	        _getRollDividendData: function (pData, dData, freq, sIndex, eIndex) {
	            var ret = [];
	            if (pData.length > 0 && dData.length > 0) {
	                var k = 0;
	                var temp = parseFloat(dData[0].Desc, 10);
	                for (var i = 0, l = pData.length, idx, pIndex, cIndex, flag, dIndex, dp; i < l; i++) {
	                    flag = false;
	                    dp = [];
	                    idx = i > 0 ? i - 1 : 0;
	                    pIndex = pData[idx][pData[idx].length - 1];
	                    cIndex = pData[i][pData[i].length - 1];
	                    for (var j = k, jl = dData.length; j < jl; j++) {
	                        dIndex = dData[j].DateIndex;
	                        if (dIndex <= cIndex && dIndex > pIndex) {
	                            dp.push(pData[i][0]);
	                            temp = parseFloat(dData[j].Desc, 10);
	                            dp.push(temp);
	                            k = j;
	                            flag = true;
	                            break;
	                        }
	                    }
	                    if (flag) { //find the dividend data.   
	                        dp.push(cIndex);
	                    } else {
	                        dp.push(pData[i][0])
	                        dp.push(temp);
	                        dp.push(cIndex);
	                    }
	                    ret.push(dp);
	                }
	            }
	            return ret;
	        },
	        _calculateIndicator: function (indicatorName, pData, vData, dData, ty, arg) {
	            var indData;
	            var techIndicatorCal = QSAPI.Widget.Chart.IndicatorCalculation;
	            switch (indicatorName) {
	                case 'SMA':
	                    {
	                        indData = [techIndicatorCal.calculateSma(pData, ty, arg[0])];
	                    }
	                    break;
	                case 'EMA':
	                    {
	                        indData = [techIndicatorCal.calculateEma(pData, ty, arg[0])];
	                    }
	                    break;
	                case 'BBands':
	                    {
	                        indData = techIndicatorCal.calculateBBands(pData, ty, arg[0], arg[1]);
	                    }
	                    break;
	                case 'MACD':
	                    {
	                        indData = techIndicatorCal.calculateMACD(pData, ty, arg[0], arg[1], arg[2]);
	                    }
	                    break;
	                case 'RSI':
	                    {
	                        indData = techIndicatorCal.calculateRSI(pData, ty, arg[0]);
	                    }
	                    break;
	                case 'ROC':
	                    {
	                        indData = [techIndicatorCal.calculateROC(pData, ty, arg[0])];
	                    }
	                    break;
	                case 'SStochastic':
	                    {
	                        indData = techIndicatorCal.calculateSlowStochastic(pData, ty, arg[0], arg[1]);
	                    }
	                    break;
	                case 'FStochastic':
	                    {
	                        indData = techIndicatorCal.calculateFastStochastic(pData, ty, arg[0], arg[1]);
	                    }
	                    break;
	                case 'PSAR':
	                    {
	                        indData = [techIndicatorCal.calculatePSAR(pData, ty, arg[0], arg[1])];
	                    }
	                    break;
	                case 'WillR':
	                    {
	                        indData = [techIndicatorCal.calculateWillR(pData, ty, arg[0])];
	                    }
	                    break;
	                case 'SMAV':
	                    {
	                        indData = [techIndicatorCal.calculateSma(vData, ty, arg[0])];
	                    }
	                    break;
	                case 'DMI':
	                    {
	                        indData = techIndicatorCal.calculateDmi(pData, ty, arg[0]);
	                    }
	                    break;
	                case 'PChannel':
	                    {
	                        indData = techIndicatorCal.calculatePriceChannel(pData, ty, arg[0]);
	                    }
	                    break;
	                case 'MAE':
	                    {
	                        indData = techIndicatorCal.calculateMAEnvelope(pData, ty, arg[0], arg[1]);
	                    }
	                    break;
	                case 'Momentum':
	                    {
	                        indData = [techIndicatorCal.calculateMomentum(pData, ty, arg[0])];
	                    }
	                    break;
	                case 'ULT':
	                    {
	                        indData = [techIndicatorCal.calculateUlt(pData, ty, arg[0], arg[1])];
	                    }
	                    break;
	                case 'MFI':
	                    {
	                        indData = [techIndicatorCal.calculateMFI(pData, vData, ty, arg[0])];
	                    }
	                    break;
	                case 'VAcc':
	                    {
	                        indData = [techIndicatorCal.calculateVolumeAcc(pData, vData, ty, arg[0])];
	                    }
	                    break;
	                case 'UDRatio':
	                    {
	                        indData = [techIndicatorCal.calculateUpDownRatio(pData, vData, ty, arg[0], arg[1])];
	                    }
	                    break;
	                case 'OBV':
	                    {
	                        indData = [techIndicatorCal.calculateObv(pData, vData, ty, arg[0])];
	                    }
	                    break;
	                case 'VPlus':
	                    {
	                        indData = [techIndicatorCal.calculateVolPlus(pData, vData, ty, arg[0])];
	                    }
	                    break;
	                case 'VBP':
	                    {
	                        indData = [techIndicatorCal.calculateVolByPrice(pData, vData, ty, arg[0])];
	                    }
	                    break;
	                case 'DYield':
	                    {
	                        indData = [techIndicatorCal.calculateDividendYield(pData, dData, ty)];
	                    }
	                    break;
	                case 'FVolatility':
	                    {
	                        indData = [techIndicatorCal.calculateFVolatility(pData, ty)];
	                    }
	                    break;
	                case 'WMA':
	                    {
	                        indData = [techIndicatorCal.calculateWMA(pData, ty, arg[0])];
	                    }
	                    break;
	                case 'AccDis':
	                    {
	                        indData = [techIndicatorCal.calculateAccDis(pData, vData, ty)];
	                    }
	                    break;
	                case 'Mass':
	                    {
	                        indData = [techIndicatorCal.calculateMass(pData, ty)];
	                    }
	                    break;
	                case 'Volatility':
	                    {
	                        indData = [techIndicatorCal.calculateVolatility(pData, ty, arg[0], arg[1])];
	                    }
	                    break;
	                case 'ForceIndex':
	                    {
	                        indData = [techIndicatorCal.calculateForceIndex(pData, vData, ty, arg[0])];
	                    }
	                    break;
	                case 'ATR':
	                    {
	                        indData = [techIndicatorCal.calculateAtr(pData, ty)];
	                    }
	                    break;
	                case 'Keltner':
	                    {
	                        indData = techIndicatorCal.calculateKeltner(pData, ty, arg[0], arg[1], arg[2]);
	                    }
	                    break;
	                default:
	                    {}
	                    break;
	            }
	            return indData;
	        },
	        _isRelatedVolumeIndicator: function (indicatorName) {
	            var _flag = false;
	            switch (indicatorName) {
	                case "SMAV":
	                case "UDRatio":
	                case "OBV":
	                case "MFI":
	                case "VAcc":
	                case "VPlus":
	                case "VBP":
	                case "AccDis":
	                case "ForceIndex":
	                    _flag = true;
	                    break;
	            }
	            return _flag;
	        },
	        _isRelatedDividendIndicator: function (indicatorName) {
	            var _flag = false;
	            switch (indicatorName) {
	                case "RDividend":
	                case "DYield":
	                    _flag = true;
	                    break;
	            }
	            return _flag;
	        },
	        _findPreviousIndex: function (data, sIndex) {
	            if (isNaN(sIndex) || sIndex <= 0) {
	                return 0;
	            }
	            sIndex -= 1;
	            for (var l = data.length, i = l - 1; i > 0; i--) {
	                if (data[i].Index <= sIndex) {
	                    return data[i].Index;
	                }
	            }
	            return sIndex;
	        },
	        _truncate: function (data, sIndex, eIndex, days, isFundTicker) {
	            if (!data) return [];
	            if (days > 0) {
	                if (isFundTicker) {
	                    days += 1;
	                }
	                var range = (data.length >= days) ? (data.length - days) : 0;
	                return data.slice(range);
	            } else {
	                var retData = [];
	                if (isFundTicker) {
	                    sIndex = this._findPreviousIndex(data, sIndex);
	                }
	                for (var i = 0, l = data.length; i < l; i++) {
	                    if (data[i].Index >= sIndex && data[i].Index <= eIndex) {
	                        retData.push(data[i]);
	                    }
	                }
	                return retData;
	            }
	        },
	        _truncatePreAfter: function (data, freq, showPre) {
	            if (!data) return [];
	            var retData = [];
	            var sTime = showPre && this.hasPreAfter ? this.preOpenTime : this.openTime;
	            var eTime = showPre && this.hasPreAfter ? this.afteCloseTime : this.closeTime;
	
	            //convert to map
	            var _map = {};
	            var _sTime = -1,
	                _eTime = -1;
	            for (var i = 0, l = data.length, d; i < l; i++) {
	                d = data[i];
	                if (!_map[d.Time]) {
	                    _map[d.Time] = d;
	                }
	                if (_sTime == -1 && d.FillFlag == 0) {
	                    _sTime = d.Time;
	                }
	            }
	
	            for (var j = l - 1; j >= 0; j--) {
	                if (data[j].FillFlag == 0) {
	                    _eTime = data[j].Time;
	                    break;
	                }
	            }
	
	            if (_sTime === -1 && _eTime === -1) {
	                return retData;
	            }
	
	            var flag = freq != 60 || sTime % 60 == 0;
	            var startIndex = flag ? sTime : 60 * Math.ceil(sTime / 60);
	            for (var i = startIndex, dp; i <= eTime; i += freq) {
	                if (i < _sTime || i > _eTime) {
	                    retData.push(this._emptyDataPoint(i));
	                } else {
	                    dp = _map[i];
	                    if (dp && dp.FillFlag == 0) {
	                        retData.push(dp);
	                    } else {
	                        retData.push(this._emptyDataPoint(i));
	                    }
	                }
	            }
	
	
	            var extendRetData = [];
	            if (!flag) {
	                var offset = 60 * Math.ceil(sTime / 60) - sTime;
	                var interval;
	                if (offset % 30 == 0) {
	                    interval = 30;
	                } else if (offset % 15 == 0) {
	                    interval = 15;
	                } else if (offset % 5 == 0) {
	                    interval = 5;
	                } else {
	                    interval = 1;
	                }
	                for (var i = sTime, dp; i <= eTime; i += interval) {
	                    if (i % 60 == 0) continue;
	                    extendRetData.unshift(this._emptyDataPoint(i))
	                }
	                retData = retData.concat(extendRetData);
	                retData.sort(function (a, b) {
	                    if (a.Time > b.Time) {
	                        return 1;
	                    } else if (a.Time < b.Time) {
	                        return -1;
	                    } else {
	                        return 0;
	                    }
	                });
	            }
	
	            return retData;
	        },
	        _emptyDataPoint: function (time) {
	            return {
	                Time: time,
	                FillFlag: 1,
	                Open: NaN,
	                High: NaN,
	                Low: NaN,
	                Last: NaN,
	                Volume: NaN
	            };
	        },
	        _calculateStartPositionPrice: function (spos, data, exdata) {
	            if (typeof spos == "undefined" || spos == null || isNaN(spos)) {
	                return this._getPositionPrice(data, exdata);
	            }
	            return spos;
	        },
	        _getPositionPrice: function (data, exdata) {
	            if ($.isNumeric(exdata.c) || $.isNumeric(data.Last)) {
	                return $.isNumeric(exdata.c) ? exdata.c : data.Last;
	            }
	            return undefined;
	        },
	        _fillCHLO: function (md, type, spos, data, exdata, growthBaseValue) {
	            if (!growthBaseValue) {
	                growthBaseValue = 10000;
	            }
	            var ty = type.split("_");
	            var c = parseFloat(exdata.c || data.Last, 10),
	                h = parseFloat(exdata.h || data.High, 10),
	                l = parseFloat(exdata.l || data.Low, 10),
	                o = parseFloat(exdata.o || data.Open, 10);
	            if (spos > 0 && ty[0] == "return" || type.indexOf('tsc') > -1) {
	                var isTenK = ty.length > 1 && ($.inArray("k", ty) > -1);
	                var pre = isTenK ? growthBaseValue : 100;
	                var ad = isTenK ? growthBaseValue : 0;
	                spos = parseFloat(spos, 10);
	                if (type.indexOf('tsc') > -1) {
	                    spos += 100;
	                    c = ad + pre * ((c + 100) / spos - 1);
	                    h = ad + pre * ((h + 100) / spos - 1);
	                    l = ad + pre * ((l + 100) / spos - 1);
	                    o = ad + pre * ((o + 100) / spos - 1);
	                } else {
	                    c = ad + pre * (c - spos) / spos;
	                    h = ad + pre * (h - spos) / spos;
	                    l = ad + pre * (l - spos) / spos;
	                    o = ad + pre * (o - spos) / spos;
	                }
	            }
	            md.push(c, h, l, o);
	        },
	        _getExtendData: function (freq) {
	            if (this.freqExtendDataMap && freq && this.freqExtendDataMap[freq]) {
	                return this.freqExtendDataMap[freq];
	            }
	            return {};
	        }
	    };
	
	    QSAPI.Widget.Chart.Tiemseries = function () {
	        this._dataList = [];
	        this._ready = {};
	    };
	
	    QSAPI.Widget.Chart.Tiemseries.prototype = {
	        set: function (dataList, startIndex, endIndex, clearCache) {
	            var data;
	            if (clearCache) {
	                this._dataList = dataList;
	            } else {
	                this.append(dataList)
	            }
	            this._sort();
	            if (startIndex > 0 && this._dataList.length > 0) {
	                var rStartIndex = this._dataList[0].Index;
	                var start = Math.min(rStartIndex, startIndex);
	                var end = Math.max(rStartIndex, startIndex);
	                for (var i = start; i < end; i++) {
	                    if (this._ready[i]) continue;
	                    this._ready[i] = false;
	                }
	            }
	            if (endIndex > 0 && this._dataList.length > 0) {
	                var rEndIndex = this._dataList[this._dataList.length - 1].Index;
	                var start = Math.min(rEndIndex, endIndex);
	                var end = Math.max(rEndIndex, endIndex);
	                for (var i = end; i > start; i--) {
	                    if (this._ready[i]) continue;
	                    this._ready[i] = false;
	                }
	            }
	        },
	        append: function (dataList) {
	            var lastPoint = this.lastPoint();
	            for (var i = 0, l = dataList.length, data; i < l; i++) {
	                data = dataList[i];
	                if (!this._ready[data.Index]) {
	                    this._dataList.push(data);
	                    this._ready[data.Index] = true;
	                } else if (lastPoint && lastPoint.Index === data.Index) {
	                    var lastSubPoint = this.lastSubPoint(lastPoint);
	                    var firstSubPoint = this.firstSubPoint(lastPoint);
	                    var preDataPoints = [];
	                    if (!lastSubPoint) {
	                        lastSubPoint = {};
	                        lastPoint.SubDataPoints = [];
	                    }
	                    var subs = data.SubDataPoints || [];
	                    for (var j = 0, jl = subs.length, sub; j < jl; j++) {
	                        sub = subs[j];
	                        if (sub.Time > lastSubPoint.Time) {
	                            lastPoint.SubDataPoints.push(sub);
	                        } else if (sub.Time < firstSubPoint.Time) {
	                            preDataPoints.push(sub);
	                        }
	                    }
	                    lastPoint.SubDataPoints = preDataPoints.concat(lastPoint.SubDataPoints);
	                } else if (data.SubDataPoints && data.SubDataPoints.length) {
	                    for (var j = 0, len = this._dataList.length; j < len; j++) {
	                        if (this._dataList[j].Index === data.Index) {
	                            this._dataList[j] = data;
	                        }
	                    }
	                }
	            }
	        },
	        list: function () {
	            return this._dataList;
	        },
	        length: function () {
	            //return this._dataList.length;
	            var len = 0;
	            for (var i in this._ready) {
	                this._ready[i] && len++;
	            }
	            return len;
	        },
	        isReady: function (startIndex, endIndex, days) {
	            if (this.list().length <= 0) return false;
	            if (days > 0) {
	                return days <= this.length();
	            } else {
	                var dateIndexRange = this._getDateIndexRange();
	                var sIndex = dateIndexRange[0];
	                var eIndex = dateIndexRange[dateIndexRange.length - 1];
	                var minIndex = Math.min(sIndex, eIndex);
	                var maxIndex = Math.max(sIndex, eIndex);
	                return startIndex >= minIndex && endIndex <= maxIndex;
	            }
	        },
	        lastPoint: function () {
	            //this._sort();
	            var len = this._dataList.length;
	            if (len > 0) {
	                return this._dataList[len - 1];
	            }
	            return null;
	        },
	        lastSubPoint: function (lastPoint) {
	            var lastPoint = lastPoint || this.lastPoint();
	            if (lastPoint) {
	                var subs = lastPoint.SubDataPoints || [];
	                var len = subs.length;
	                if (len > 0) {
	                    return subs[len - 1];
	                }
	            }
	            return null;
	        },
	        firstSubPoint: function (lastPoint) {
	            var lastPoint = lastPoint || this.lastPoint();
	            if (lastPoint) {
	                var subs = lastPoint.SubDataPoints || [];
	                var len = subs.length;
	                if (len > 0) {
	                    return subs[0];
	                }
	            }
	            return null;
	        },
	        _getDateIndexRange: function () {
	            var dateIndexRange = [];
	            for (var i in this._ready) {
	                dateIndexRange.push(parseInt(i, 10));
	            }
	            return dateIndexRange;
	        },
	        _sort: function () {
	            this._dataList.sort(function (a, b) {
	                if (a.Index < b.Index) {
	                    return -1;
	                } else if (a.Index == b.Index) {
	                    return 0;
	                } else {
	                    return 1;
	                }
	            });
	        }
	    };
	    if (!QSAPI.DataManager.Chart) {
	        QSAPI.DataManager.Chart = new QSAPI.Widget.Chart.DataManager();
	    }
	})(QSAPI);
	/**
	* @author Jacye.Ouyang
	*/
	(function(QSAPI){
	    'use strict';
	    var $ = QSAPI.$;
	    var Util = QSAPI.Util;  
	    if(!QSAPI.Widget.Chart){
	        QSAPI.Widget.Chart = {};
	    }
	    QSAPI.Widget.Chart.IndicatorCalculation = {
	        filtrateCalculatedData: function(stIndex,endIndex,isFilled,aData,ty,type,startpos) {
	            if (aData == null || aData.length<1) {
	                return {data: [],spos: null};
	            }
	            var data  = [];
	            var lgg = aData[0].length, sposIndex = lgg>3?2:1;
	            var st = aData[0][aData[0].length-1], end = aData[aData.length-1][aData[aData.length-1].length-1];
	            var interVal = 1;
	            if (ty == "i"){
	                interVal = 600000;
	            }else if (ty == "d") { 
	                interVal = 1;
	            } else if (ty == "w") {
	                interVal = 7;
	            } else if (ty == "m") {
	                interVal = 30;
	            }
	            var inx = 0,spos = (typeof(startpos)=="undefined"?null:startpos);
	            if (isFilled && stIndex < st) {   
	                for (var i = stIndex; i < st; i += interVal) {
	                     var dp = [inx,NaN,i];
	                     data.push(dp);
	                     inx++;
	                }     
	            }
	            for (var j = 0; j < aData.length; j++) {
	                if (aData[j][aData[j].length-1] < stIndex || aData[j][aData[j].length-1] > endIndex) {
	                    continue;
	                }
	                if(type=="return"&&spos == null && !isNaN(aData[j][sposIndex])){
	                    spos = aData[j][sposIndex];
	                }
	                var dpp = [];
	                dpp.push(inx);
	                dpp.push(aData[j][1]);         
	                for (var h=2;h<aData[j].length;h++) {
	                    dpp.push(aData[j][h]);
	                }
	                data.push(dpp);
	                inx++;
	            }
	            /*if (isFilled && endIndex > end) {   // not fill the end.
	                for (var g = end; g<endIndex; g+=interVal) {
	                     var d = [inx,NaN,g+interVal];
	                     data.push(d);
	                     inx++;
	                }     
	            }*/
	            return {data: data,spos: spos};
	        },
	        EMA: function(aData,n) {
	            if(typeof(aData)=="undefined"||aData.length===0||isNaN(n)){
	                return [];
	            }
	            var aCache = [];
	            var lg = aData.length, k  = 2/(n+1), iPreEma= 0, iSmv = 0,m=0;
	            var needCut = true, st = 0;
	            for (var i = 0; i < lg; i++) {
	                if (needCut && (aData[i] == null || isNaN(aData[i]))) {
	                    aCache.push(NaN);
	                    st = i + 1;
	                    continue;
	                }
	                needCut = false;
	                if ((i - st) < n - 1) {
	                    aCache.push(NaN);
	                    if (aData[i] != null && !isNaN(aData[i])) {
	                        iSmv += aData[i], m++;
	                    }
	                    continue;
	                } else if ((i - st) == n - 1) {
	                    if (aData[i] != null && !isNaN(aData[i])) {
	                        iSmv += aData[i], m++;
	                    }
	                    aCache.push((iSmv == 0 ? NaN : (iSmv / m)));
	                    if (!isNaN(aCache[i])) {
	                        iPreEma = aCache[i];
	                    }
	                } else {
	                    var iCurPrice = (aData[i] == null ? NaN : aData[i]);
	                    var iEma = iPreEma + (iCurPrice - iPreEma) * k;
	                    aCache.push(iEma);
	                    if (!isNaN(aCache[i])) {
	                        iPreEma = iEma;
	                    }
	                }
	            }
	            return aCache;
	        },
	        calculateSma:function(aData,ty,n) {
	            if (n == null || n< 0 || aData == null || aData.length < n) {
	                return [];
	            }
	            var aCache = [];
	            var lg = aData.length, iSum = 0, m=0;
	            for (var i = 0; i<lg; i++) {
	                if (aData[i] != null && !isNaN(aData[i][1])) {
	                   iSum += aData[i][1];
	                   m++;
	                }
	                if (i < n-1) {
	                    aCache.push([i,NaN,aData[i][aData[i].length-1]]);
	                } else { 
	                    var smv = isNaN(aData[i][1])||m<=0?NaN:(iSum/m); 
	                    aCache.push([i,smv,aData[i][aData[i].length-1]]);
	                    if (aData[i-n+1] != null && !isNaN(aData[i-n+1][1])) {
	                        iSum-=aData[i-n+1][1];
	                        m--;
	                    }
	                }           
	            }
	            return aCache;
	        },
	        
	        calculateEma: function(aData,ty,n) {
	            if (n == null || n< 0 || aData == null || aData.length < n) {
	               return [];
	            }
	            var aCache = [];
	            var lg = aData.length, k  = 2/(n+1), iEma= NaN, iSmv = 0,m=0,iPreEma=NaN;
	            var st = 0;
	            for(var j=0;j<lg;j++){  // remove the NaN data points.
	                if(!isNaN(aData[j][1])){
	                    break;
	                }
	                st++;
	            }
	            for (var i=0;i<lg;i++) {
	                if ((i-st) <= n-1) { 
	                    if (aData[i] != null && !isNaN(aData[i][1])) {
	                        iSmv+=aData[i][1], m++;
	                    }
	                    if((i-st)==n-1){ // first point
	                        iPreEma = iEma = iSmv/m;
	                    }
	                } else {
	                    if(isNaN(aData[i][1])){  // fill data point.
	                        iEma = NaN;//aCache[i-1][1];
	                    }else{
	                        iEma = iPreEma + (aData[i][1]- iPreEma)*k;
	                        iPreEma = iEma;  // save the available pre-EMA value.
	                    }
	                }
	                aCache.push([i,iEma,aData[i][aData[i].length-1]]);
	            }
	            return aCache;
	        },
	        
	        calculateBBands:function(aData,ty,n,dev) { 
	            if (n< 0 || aData == null || aData.length < n) {
	               return [[],[]];
	            } 
	            var aCache = [];
	            var lg = aData.length; aCache[0] = [], aCache[1] = [];
	            var iSum = 0, m = 0;
	            for (var i = 0; i < lg; i++) { 
	                if (i < n-1) {
	                    aCache[0].push([i,NaN,NaN,aData[i][aData[i].length-1]]);
	                    aCache[1].push([i,NaN,NaN,aData[i][aData[i].length-1]]);
	                    if (aData[i] != null && !isNaN(aData[i][1])) {
	                        iSum += aData[i][1],
	                        m++;
	                    }
	                    continue;
	                } else { 
	                    if (aData[i] != null && !isNaN(aData[i][1])) {
	                        iSum+= aData[i][1];
	                        m++;
	                    } 
	                    var iAvg = m>0?(iSum/m):NaN; var iStdDev = 0,iStdDevSum = 0,  iNum =0;
	                    for (var j = i; j > (i-n); j--) {
	                        if (aData[j] != null && !isNaN(aData[j][1])) {
	                            iNum++;
	                            iStdDevSum += (Math.pow((aData[j][1]-iAvg),2));
	                        }
	                    }
	                    if(iNum > 1) iNum = iNum -1; // 
	                    iStdDev = Math.sqrt(iStdDevSum/iNum);
	                    var ibBandUpper = iAvg + dev*iStdDev;
	                    var ibBandLower = iAvg - dev*iStdDev;
	                    if(isNaN(aData[i][1])){
	                        aCache[0].push([i,NaN,NaN,aData[i][aData[i].length-1]]);
	                        aCache[1].push([i,NaN,NaN,aData[i][aData[i].length-1]]);
	                    }else{
	                        aCache[0].push([i,ibBandUpper,aData[i][1],aData[i][aData[i].length-1]]);
	                        aCache[1].push([i,ibBandLower,aData[i][1],aData[i][aData[i].length-1]]);
	                    }
	                    
	                    if (aData[i-n+1] != null && !isNaN(aData[i-n+1][1])) {
	                       iSum -= aData[i-n+1][1] ;
	                       m--;
	                    }
	                }                
	            }
	            return aCache;
	        },
	        
	        calculateMACD:function(aData,ty,slow,fast,signal) {
	            if (fast < 0 || aData == null || aData.length < slow) {
	                return [[],[],[]];
	            }
	            var aCache = [[],[],[]];
	            var slowEma = this.calculateEma(aData,ty,slow);
	            var fastEma = this.calculateEma(aData,ty,fast);
	            var lg = aData.length;
	            var iMacd, divergence;
	            for (var i = 0; i < lg; i++) {
	                iMacd = fastEma[i][1] - slowEma[i][1];
	                aCache[0][i] = [i,iMacd,aData[i][aData[i].length-1]];
	            }
	            var sigualEma = this.calculateEma(aCache[0],ty,signal);
	            for (var j = 0;j<lg;j++) { 
	                divergence = aCache[0][j][1] - sigualEma[j][1];  
	                aCache[2][j] = [j,divergence,aData[j][aData[j].length-1]];
	            }
	            aCache[1] = sigualEma;
	            return aCache;
	        },
	        
	        calculateRSI : function (aData,ty,n){// default n = 14
	            if (n< 0 || aData == null || aData.length < n) {
	               return [[],[],[]];
	            }
	            var aCache = [], lg = aData.length;
	            var iGain = 0, iLoss = 0,m = 0, preAvgGain = NaN, preAvgLoss = NaN,dx=0,iAvgGain,iAvgLoss,iRsi = NaN;
	            for (var i = 0; i < lg; i++) {
	               if (i <= n) { 
	                    if (i>0) {
	                        dx = isNaN(aData[i-1][1])||isNaN(aData[i][1])?0:(aData[i][1] - aData[i-1][1]); 
	                        if (dx > 0) { 
	                            iGain+=dx;
	                        } else {  // convert to postive 
	                            iLoss-=dx;
	                        }
	                    }
	                    if(i==n){ // the first point
	                        preAvgGain = iAvgGain = iGain/n;
	                        preAvgLoss = iAvgLoss = iLoss/n;
	                        iRsi = iAvgLoss==0?100:(100-(100/(1+iAvgGain/iAvgLoss)));
	                    }
	                    aCache[i] = [i,iRsi,aData[i][aData[i].length-1]];
	                } else {
	                    if(isNaN(aData[i][1])){
	                        aCache[i] = [i,NaN,aData[i][aData[i].length-1]];
	                        continue;
	                    }
	                    dx = isNaN(aData[i-1][1])||isNaN(aData[i][1])?0:(aData[i][1] - aData[i-1][1]); 
	                    var dxLoss = dx<0 ?-dx:0, dxGain = dx>0?dx:0; 
	                    iAvgGain = (preAvgGain*(n-1)+ dxGain)/n; //EMA(n)
	                    iAvgLoss = (preAvgLoss*(n-1)+ dxLoss)/n; //EMA(n)
	                    iRsi = iAvgLoss==0?100:(100-(100/(1+iAvgGain/iAvgLoss)));
	                    aCache[i] = [i,iRsi,aData[i][aData[i].length-1]];
	                    preAvgGain = iAvgGain; 
	                    preAvgLoss = iAvgLoss;
	                }
	            }
	            var overlines = this.calculateOverLines(aCache,30,70);
	            var t=[];
	            t[0]=aCache,t[1]=overlines[0],t[2]=overlines[1];
	            return t;
	            
	        },
	        
	        _calculateStochastic: function(aData,ty,k) { // k=14. only calculate K%
	            if (k < 0 || aData == null || aData.length < k) { 
	                return [[]];
	            }
	            var aCache = []; 
	            var lg = aData.length;
	            var iK;
	            for (var i = 0; i< lg; i++) {
	                if (i < (k - 1)) {
	                    aCache[i] = [i,NaN,aData[i][aData[i].length-1]];
	                } else { 
	                    var iMax = 0; 
	                    var iMin = 100000000;
	                    for (var j = i; j > i-k; j--) {
	                        if (aData[j][2] > iMax) {
	                            iMax = aData[j][2];
	                        }
	                        if (aData[j][3] < iMin) {
	                            iMin = aData[j][3];
	                        }
	                    }
	                    iK = (iMax<=iMin?NaN:100*(aData[i][1] - iMin)/(iMax-iMin));
	                    aCache[i] = [i,iK,aData[i][aData[i].length-1]];
	                }
	            }
	            return aCache;
	        },
	        calculateOverLines:function(data,l,h){  // 20 and 80
	            var d= [[],[]]
	            for(var i=0; i<data.length;i++){
	                d[0][i] = [data[i][0],l,data[i][2]];
	                d[1][i] = [data[i][0],h,data[i][2]];
	            }
	            return d;
	        },
	        calculateSlowStochastic:function(aData,ty,k,d) { //K% = D% of Fast. D% = SMA(3) of K%
	            var so = [];
	            var fastD = this._calculateStochastic(aData,ty,k);
	            so[0] = this.calculateSma(fastD,ty,d);
	            so[1] = this.calculateSma(so[0],ty,d);
	            var over = this.calculateOverLines(so[0],20,80);
	            so[2] = over[0];
	            so[3] = over[1];
	            return so;
	        },
	        
	        calculateFastStochastic:function(aData,ty,k,d) {  // K% and D% = SMA(3) of K%
	            var so = [];
	            so[0] = this._calculateStochastic(aData,ty,k);
	            so[1] = this.calculateSma(so[0],ty,d);
	            var over = this.calculateOverLines(so[0],20,80);
	            so[2] = over[0];
	            so[3] = over[1];
	            return so;
	        },
	        
	        calculateROC:function (aData,ty,n) {
	            if (n< 0 || aData == null || aData.length < n) {
	                return [];
	            }
	            var aCache = [];
	            var lg = aData.length;
	            var preClose,roc;
	            for (var i = 0;i < lg; i++) { 
	                if (i < n) {
	                    aCache.push([i,NaN,aData[i][aData[i].length-1]]);
	                } else {
	                    preClose = (aData[i-n] == null?NaN:aData[i-n][1]); 
	                    roc = NaN;
	                    if (!isNaN(preClose) && preClose !== 0) {
	                        roc = 100*(aData[i][1] - preClose)/preClose;
	                    }
	                    aCache[i] = [i,roc,aData[i][aData[i].length-1]];
	                }
	            }
	            return aCache;
	        },
	        
	        calculateWillR:function(aData,ty,n) { 
	            if (n < 0 || aData == null || aData.length < n) {
	                return [];
	            }
	            var aCache = []; 
	            var lg = aData.length;
	            var iHigh = 0, iLow = 1000000,iR;
	            for (var i = 0; i < lg; i++) { 
	                if (i < n) {
	                    if (aData[i] != null) {
	                        if (!isNaN(aData[i][2]) && aData[i][2] > iHigh) { 
	                            iHigh = aData[i][2];
	                        }
	                        if (!isNaN(aData[i][3]) && aData[i][3] < iLow) {
	                            iLow = aData[i][3];
	                        }
	                        if (i == n-1) { 
	                            iR = 100*(aData[i][1]-iHigh)/(iHigh-iLow); 
	                            aCache[i] = [i,iR,aData[i][aData[i].length-1]]; 
	                        } else {
	                            aCache[i] = [i,NaN,aData[i][aData[i].length-1]];
	                        }
	                    }
	                } else {
	                    iHigh = 0, iLow = 1000000;
	                    for (var j = i; j >i - n; j--) {
	                        if (!isNaN(aData[j][2]) && aData[j][2] > iHigh) {
	                            iHigh = aData[j][2];
	                        }
	                        if (!isNaN(aData[j][3]) && aData[j][3] < iLow) {
	                            iLow = aData[j][3];
	                        }
	                    }
	                    iR = 100*(aData[i][1]-iHigh)/(iHigh-iLow);
	                    aCache[i] = [i,iR,aData[i][aData[i].length-1]];
	                }         
	            }
	            return aCache;
	        },
	        calculatePSAR:function(aData,ty,a,maxA) {
	            if (aData == null || aData.length < 2) {
	                return;
	            }
	            var aCache = [];
	            var lg = aData.length;
	            var iTrend, iEp, iA = a, iSAR, iMax = 0, iMin = 1000000; 
	            var preTrend = [];
	            aCache[0] = [0,NaN,NaN,aData[0][aData[0].length-1]];var flag = false;
	            for (var i = 2; i <= lg; i++) { 
	                if (i >= 2&&!flag) {
	                    if(isNaN(aData[i-1][2])||isNaN(aData[i-1][3])||isNaN(aData[i-1][1])){
	                        aCache[i-1] = [i-1,NaN,aData[i-1][1],aData[i-1][aData[i-1].length-1]]; 
	                        continue;
	                    }
	                    flag = true;
	                    var mm = aData[i-2][2]>aData[i-1][2]?aData[i-2][2]:aData[i-1][2]; 
	                    var nn = aData[i-2][3]<aData[i-1][3]?aData[i-2][3]:aData[i-1][3];
	                    if (aData[i][1] >= aData[i-1][1]) {     // up trend
	                        iTrend = 0; iSAR  = nn; 
	                        iEp = mm; aCache[i-1] = [i-1,iSAR,aData[i-1][1],aData[i][aData[i].length-1]]; 
	                        preTrend[0] = preTrend[1] = 0;
	                    } else {                                       // down trend.
	                        iTrend  = 1; iSAR =mm; 
	                        iEp = nn; aCache[i-1] = [i-1,iSAR,aData[i-1][1],aData[i-1][aData[i-1].length]];
	                        preTrend[0] = preTrend[1] = 0;
	                    } 
	                }
	                var iCurrentSAR = (isNaN(iSAR)||isNaN(iEp))?NaN:(iSAR + iA*(iEp-iSAR)); 
	                if (i == lg) {
	                    aCache[i-1] = [i-1,iCurrentSAR,aData[1-1][1],aData[i-1][aData[i-1].length-1]];
	                    if(isNaN(aData[i-1][1])){
	                        aCache[i-1] = [i,NaN,NaN,aData[i-1][aData[i-1].length-1]];
	                    }
	                    break;
	                }
	                if (isNaN(aData[i][1]) || isNaN(aData[i][2]) || isNaN(aData[i][3])) {
	                    aCache[i-1] = [i,NaN,NaN,aData[i][aData[i].length-1]];
	                    continue;
	                }
	                if (preTrend[0] == preTrend[1]) {
	                    var min,max
	                    if (preTrend[1] == 0) {                                                                  //up trend
	                        if (i>1 && !isNaN(aData[i-1][3]) && !isNaN(aData[i-2][3])) {         
	                            min = (aData[i-1][3]<aData[i-2][3]?aData[i-1][3]:aData[i-2][3]); 
	                            if (iCurrentSAR > min) { 
	                                iCurrentSAR = min;
	                            }
	                        }
	                    } else {                                                                                      //down trend
	                        if (i>1&& !isNaN(aData[i-1][2]) && !isNaN(aData[i-2][2])) {
	                            max = (aData[i-1][2]>aData[i-2][2]?aData[i-1][2]:aData[i-2][2]);
	                            if (iCurrentSAR < max) {
	                                iCurrentSAR = max;
	                            }
	                        }
	                    }             
	                } else {
	                    iCurrentSAR = iEp;
	                }
	                aCache[i-1] = [i-1,iCurrentSAR,aData[i-1][1],aData[i-1][aData[i-1].length-1]];
	                if (preTrend[1] == 1) {
	                    if(aData[i][3] < iEp) { 
	                        iEp = aData[i][3]; 
	                        iA += a;
	                        if (iA > maxA) iA = maxA;
	                    }
	                } else {  
	                    if (aData[i][2] > iEp) {
	                        iEp = aData[i][2];
	                        iA += a;
	                        if (iA > maxA) iA = maxA;
	                    }
	                }                  
	                if ((preTrend[1]==0 && iCurrentSAR > aData[i][3]) || (preTrend[1] == 1 && iCurrentSAR < aData[i][2])) {
	                    iA = a;
	                    if (iTrend == 0) { 
	                        iTrend = 1;
	                    } else {
	                        iTrend  = 0;
	                    }
	                } 
	                iSAR = iCurrentSAR;
	                preTrend [0] = preTrend[1];
	                preTrend [1] = iTrend;
	            }
	            return aCache;
	        },
	        /*
	        calculatePSAR:function(aData,ty,a,maxA) {
	            if (aData == null || aData.length < 2) {
	                return;
	            }
	            var aCache = [];
	            var lg = aData.length,st=0;
	            var iTrend=[], iEp=[], iA = iUpA = iDownA = a, iSAR, iMax = 0, iMin = 1000000,preUpSAR=NaN,preDownSAR=NaN, firstTrend=NaN,fSAR=NaN; 
	            var preTrend = NaN;
	            aCache[0] = [0,NaN,aData[0][aData[0].length-1]];
	            var maxP = 0, minP = 1000000000;
	            for (var i = 0; i <lg; i++) { 
	                if(isNaN(aData[i][2])||isNaN(aData[i][3])||isNaN(aData[i][1])){  // 
	                    aCache[i] = [i,NaN,aData[i][aData[i].length-1]];
	                    if(i>0) iTrend[i]=iTrend[i-1];
	                    else iTrend[i] = NaN;
	                    continue;
	                }
	                if(st<1){// the first available point.
	                    aCache[i] = [i, NaN, aData[i][aData[i].length - 1]];
	                    if (i > 0) iTrend[i] = iTrend[i - 1];
	                    else iTrend[i] = NaN;
	                    st++;
	                    continue;
	                }
	                st++;
	                if(isNaN(iTrend[i-1])){ // first trend
	                    var temp = aData[i][1]>=aData[i-1][1]?1:0;
	                    if(isNaN(firstTrend)){
	                        firstTrend = temp;
	                        aCache[i] = [i,NaN,aData[i][aData[i].length-1]];
	                        if(firstTrend==1){
	                            fSAR = isNaN(fSAR)?100000000:fSAR;
	                            fSAR = Math.min(fSAR,aData[i][3]);
	                        }else{
	                            fSAR = isNaN(fSAR)?0:fSAR;
	                            fSAR = Math.max(fSAR,aData[i][2]);
	                        }
	                        iTrend[i]=NaN;
	                        continue;
	                    }else{
	                        //if(firstTrend!=temp){// fisrt revise
	                            if(firstTrend==1){
	                                fSAR = Math.min(fSAR,aData[i][3]);
	                                iEp[i] = Math.max(fSAR,aData[i][2]);
	                            }else{
	                                fSAR = Math.max(fSAR,aData[i][2]);
	                                iEp[i] = Math.min(fSAR,aData[i][3]);
	                            }
	                            iSAR = fSAR;
	                            aCache[i] = [i,fSAR,aData[i][aData[i].length-1]];
	                            iTrend[i-1] = iTrend[i] = temp;
	                            //console.log("First point|"+i+"|"+fSAR+"|"+temp);
	                            continue;
	                       // }
	                    } 
	                    
	                }else{
	                    //check for reverse
	                    if(iTrend[i-1]==1&&aCache[i-1][1]>aData[i][3]){
	                        iTrend[i] = 0;
	                        iA= a;
	                        iEp[i] = aData[i][3];
	                        iSAR=iEp[i-1];
	                        aCache[i] = [i,iSAR,aData[i][aData[i].length-1]];
	                        //if(i>lg-50) console.log("reverse:"+i+"|"+iSAR.toFixed(2)+"|"+iTrend[i]+"|"+iEp[i]+"|"+iA.toFixed(2)+"|"+aData[i][2]+"|"+aData[i][3]+"|"+aCache[i-1][1]);
	                        continue;
	                    }
	                    if(iTrend[i-1]==0&&aCache[i-1][1]<aData[i][2]){
	                        iTrend[i] = 1;
	                        iA= a;
	                        iSAR=iEp[i-1];
	                        iEp[i] = aData[i][2];
	                        aCache[i] = [i,iSAR,aData[i][aData[i].length-1]];
	                        //if(i>lg-50) console.log("reverse:"+i+"|"+iSAR.toFixed(2)+"|"+iTrend[i]+"|"+iEp[i]+"|"+iA.toFixed(2)+"|"+aData[i][2]+"|"+aData[i][3]+"|"+aCache[i-1][1]);
	                        continue;
	                    }
	                    var prevSAR =0,prevIep = 0;
	                    for(var j=i-1;j>=0;j--){
	                        if(!isNaN(aCache[j][1])){
	                            prevSAR = aCache[j][1];
	                            break;
	                        }   
	                    }
	                    for(var j=i-1;j>=0;j--){
	                        if(!isNaN(iEp[j])&&typeof(iEp[j])!="undefined"){
	                            prevIep = iEp[j];
	                            break;
	                        }
	                    }
	                    iSAR = prevSAR + iA * (prevIep - prevSAR);
	                    if(iTrend[i-1]==1){// up (preTrend==1)
	                        iEp[i] = Math.max(aData[i][2],iEp[i-1]);
	                        if(iEp[i]>iEp[i-1]){
	                            iA+=a;
	                            if(iA>maxA) iA = maxA;
	                        }
	                        if(iSAR>aData[i-1][3])iSAR = aData[i-1][3];
	                        if(iSAR>aData[i-2][3])iSAR = aData[i-2][3];
	                        if(iSAR>aData[i][3]){//reverse
	                            iTrend[i] = 0;
	                            iA= a;
	                            iEp[i] = aData[i][3];
	                            iSAR=iEp[i-1];
	                            aCache[i] = [i,iSAR,aData[i][aData[i].length-1]];
	                            //if(i>lg-50) console.log("reverse:"+i+"|"+iSAR.toFixed(2)+"|"+iTrend[i]+"|"+iEp[i]+"|"+iA.toFixed(2)+"|"+aData[i][2]+"|"+aData[i][3]+"|"+aCache[i-1][1]);
	                            continue;
	                        }
	                        iTrend[i] = aData[i][3]>iSAR?1:0;
	                        
	                    }else{//down
	                        iEp[i] = Math.min(aData[i][3],iEp[i-1]);
	                        if(iEp[i]<iEp[i-1]){
	                            iA+=a;
	                            if(iA>maxA) iA = maxA;
	                        }
	                        if(iSAR<aData[i-1][2])iSAR = aData[i-1][2];
	                        if(iSAR<aData[i-2][2])iSAR = aData[i-2][2];
	                        if(iSAR<aData[i][2]){ // reverse
	                            iTrend[i] = 1;
	                            iA= a;
	                            iSAR=iEp[i-1];
	                            iEp[i] = aData[i][2];
	                            aCache[i] = [i,iSAR,aData[i][aData[i].length-1]];
	                            //if(i>lg-50) console.log("reverse:"+i+"|"+iSAR.toFixed(2)+"|"+iTrend[i]+"|"+iEp[i]+"|"+iA.toFixed(2)+"|"+aData[i][2]+"|"+aData[i][3]+"|"+aCache[i-1][1]);
	                            continue;
	                        }
	                        iTrend[i] = aData[i][2]<iSAR?0:1;
	                    }
	                    aCache[i] = [i,iSAR,aData[i][aData[i].length-1]];
	                    //if(i>lg-50)console.log(iSAR.toFixed(2)+"|"+iTrend[i]+"|"+iEp[i]+"|"+iA.toFixed(2)+"|"+aData[i][2]+"|"+aData[i][3]);   
	                }
	            }
	            return aCache;
	        },*/
	        calculateDmi: function (aData, ty, n) {
	            if (aData == null || !aData.length) {
	                return [[], [], []];
	            }
	
	            var dm1 = 0,
	              dm2 = 0,
	              p = n || 14,
	              t1 = 0,
	              t2 = 0,
	              tr = 0,
	              s1 = 0,
	              s2 = 0,
	              s3 = 0,
	              e1 = [],
	              e2 = [],
	              e3 = [],
	              kp = 0,
	              dx = [],
	              dxNum = 0,
	              dxSum = 0;
	
	            var a1 = [], a2 = [], a3 = [];
	            a1[0] = a2[0] = a3[0] = [0, NaN, aData[0][aData[0].length - 1]];
	            for (var i = 1; i < aData.length; i++) {
	                a1[i] = [i, NaN, aData[i][aData[i].length - 1]];
	                a2[i] = [i, NaN, aData[i][aData[i].length - 1]];
	                a3[i] = [i, NaN, aData[i][aData[i].length - 1]];
	                t1 = aData[i][2] - aData[i - 1][2]; // the current high minus the prior high
	                t2 = aData[i - 1][3] - aData[i][3]; // the prior low minus the current low
	                if (t1 > t2) { // up movement +DM
	                    if (t1 > 0)
	                        dm1 = t1;
	                    else
	                        dm1 = 0;
	                    dm2 = 0
	                } else if (t1 < t2) {// down movement.
	                    if (t2 > 0)
	                        dm2 = t2;
	                    else
	                        dm2 = 0;
	                    dm1 = 0;
	                } else { // t1==t2
	                    dm1 = dm2 = 0;
	                }
	                tr = Math.max(Math.abs(aData[i][2] - aData[i][3]), Math.max(Math.abs(aData[i][2] - aData[i - 1][1]), Math.abs(aData[i][3] - aData[i - 1][1])));
	                if (isNaN(dm1)) {
	                    dm1 = 0;
	                }
	                if (isNaN(dm2)) {
	                    dm2 = 0;
	                }
	                if (isNaN(tr)) {
	                    tr = 0;
	                }
	
	                if (i < p) { //1-14 sum
	                    s1 += dm1;
	                    s2 += dm2;
	                    s3 += tr;
	                }
	                if (i == p) {
	                    e1[i] = s1 + dm1;//s1 / p;  //+DM14
	                    e2[i] = s2 + dm2;//s2 / p;   //-DM14
	                    e3[i] = s3 + tr;//s3 / p;   //TR14
	                    a1[i][1] = 100 * e1[i] / e3[i]; //+DI14
	                    a2[i][1] = 100 * e2[i] / e3[i]; //-DI14
	                    dx[i] = 100 * Math.abs(a1[i][1] - a2[i][1]) / (a1[i][1] + a2[i][1]); //DX
	                }
	                //kp = aData[i][1]*k;
	                if (i > p) {
	                    e1[i] = e1[i - 1] - e1[i - 1] / p + dm1;//dm1 * k + (1 - k) * e1[i - 1];
	                    e2[i] = e2[i - 1] - e2[i - 1] / p + dm2;//dm2 * k + (1 - k) * e2[i - 1];
	                    e3[i] = e3[i - 1] - e3[i - 1] / p + tr;//tr * k + (1 - k) * e3[i - 1];
	                    a1[i][1] = 100 * (e1[i] / e3[i]);
	                    a2[i][1] = 100 * (e2[i] / e3[i]);
	                    dx[i] = 100 * Math.abs(a1[i][1] - a2[i][1]) / (a1[i][1] + a2[i][1]); //DX
	
	                }
	                if (i >= p && i <= 2 * (p - 1)) {  // start from 25
	                    if (!isNaN(dx[i])) {
	                        dxSum += dx[i];
	                        dxNum++;
	                    }
	                }
	                if (i == 2 * p - 1) {
	                    if (!isNaN(dx[i])) {
	                        dxSum += dx[i];
	                        dxNum++;
	                    }
	                    a3[i][1] = dxSum / dxNum; //ADX
	                }
	                if (i > 2 * p - 1 && !isNaN(dx[i])) {
	                    a3[i][1] = (dx[i] + (p - 1) * (isNaN(a3[i - 1][1]) ? 0 : a3[i - 1][1])) / p;
	                }
	                if (isNaN(aData[i][1])) {
	                    a1[i][1] = NaN;
	                    a2[i][1] = NaN;
	                    a3[i][1] = NaN;
	                }
	            }
	            return [a1, a2, a3];
	        },
	        /*
	            Data point needed: high, low
	            Parameter range: 2<n<250
	            Default parameter: 2
	            Get the maximum value within the scope of 20.
	            Get the minimum value within the scope of 20.
	            Then draw the two lines by maximum,minimum
	            Hard
	            Overlay
	         */
	        calculatePriceChannel:function(aData,ty,n) {
	            //if(ty=="i")return[[],[]];
	            if (isNaN(n)||n<2||n>250||aData==null||aData.length<n) {
	                return [[],[]];
	            }
	            var arr1=[],arr2=[],max=aData[0][2],min=aData[0][3],imax=0, imin=0;
	            arr1.push([0,NaN,NaN,aData[0][aData[0].length - 1]]);
	            arr2.push([0,NaN,NaN,aData[0][aData[0].length - 1]]);
	            for(var i = 1;i<aData.length;i++){
	                if(max<=aData[i][2]){
	                    max = aData[i][2];
	                    imax  =i;
	                }
	                if(min>=aData[i][3]){
	                    min = aData[i][3];
	                    imin = i;
	                }
	                if((i-n)==imax){
	                    max =0;
	                    var begin = imax +1,end = imax +n+1;
	                    for(var k = begin;k<end;k++){
	                        if(max <=aData[k][2]){
	                            max = aData[k][2];
	                            imax  =k;
	                        }
	                    }
	                }
	                if((i-n)==imin){
	                    var begin = imin +2,end = imin +n+1;
	                    min = aData[begin-1][3],
	                    imin = begin-1;
	                    for(var k = begin;k<end;k++){
	                        if(min>=aData[k][3]){
	                            min = aData[k][3];
	                            imin = k;
	                        }
	                    }
	                }
	                if(i>n-2){
	                    arr1.push([i,max,aData[i][1],aData[i][aData[i].length - 1]]);
	                    arr2.push([i,min,aData[i][1],aData[i][aData[i].length - 1]]);
	                }else{
	                    arr1.push([i,NaN,NaN,aData[i][aData[i].length - 1]]);
	                    arr2.push([i,NaN,NaN,aData[i][aData[i].length - 1]]);
	                }
	                if(isNaN(aData[i][1])&&typeof(arr1[i])!="undefined"&&typeof(arr2[i])!="undefined"){ //intraday
	                    arr1[i]=[i,NaN,NaN,aData[i][aData[i].length - 1]];
	                    arr2[i]=[i,NaN,NaN,aData[i][aData[i].length - 1]];
	                }
	            }
	            return [arr1,arr2];
	        },
	        /*
	            Data point needed: MA
	            Parameter range: 0<MA<250, 1%<n<10%
	            Default parameter: MA=9, n=2%
	            moving average, then get the data
	            the two line are around the middle line
	            Easy
	            Overlay
	         */
	        calculateMAEnvelope:function(aData,ty,n,per) {
	            if (isNaN(n)||isNaN(per)||n<0||n>250||aData==null||aData.length<n) {
	                return [[],[]];
	            }
	            var arr1=[],arr2=[],vol=[],sum=0,m=0,d=0,d1=0,d2=0,ad1=null,ad2=null;
	            for(var i = 0;i<aData.length;i++){
	                ad1 = aData[i][1];
	                ad2 = aData[i][1];
	                if(!isNaN(aData[i][1])){
	                    sum +=aData[i][1];
	                    m++;
	                }
	                if(i<n){
	                    if(i==(n-1)){
	                        vol =[i,sum/n,aData[i][aData[i].length - 1]];
	                        d = m>0?sum/m:NaN;
	                        d1 =d*(100+per)/100;
	                        d2 =d*(100-per)/100;
	                    }else{
	                        vol =[i,NaN,aData[i][aData[i].length - 1]];
	                        d1=d2=NaN;
	                    }
	                }else{
	                    if(!isNaN(aData[i-n][1])){
	                        sum -=aData[i-n][1];
	                        m--;
	                    }
	                    d = m>0?sum/m:NaN;
	                    d1 =d*(100+per)/100;
	                    d2 =d*(100-per)/100;
	                }
	                if(isNaN(d1)){
	                    ad1=NaN;
	                }
	                if(isNaN(d2)){
	                    ad2=NaN;
	                }
	                if(ad1=="NaN"){
	                    ad1=NaN;
	                }
	                if(ad2=="NaN"){
	                    ad2=NaN;
	                }
	                //if(!isNaN(aData[i][1])){//intraday
	                    arr1.push([i,d1,ad1,aData[i][aData[i].length - 1]]);
	                    arr2.push([i,d2,ad2,aData[i][aData[i].length - 1]]);
	                //}else{
	                    //arr1.push([i,NaN,NaN,aData[i][aData[i].length - 1]]);
	                    //arr2.push([i,NaN,NaN,aData[i][aData[i].length - 1]]);
	                //}
	            }
	            return [arr1,arr2];
	        },
	        /*
	            Data point needed: price
	            Parameter range: 0<n<200
	            Default parameter: 2
	            MA
	            Easy
	            New
	         */
	        calculateMomentum:function(aData,ty,n) {
	            if (isNaN(n)||n<1||n>199 || aData == null || aData.length < n) {
	                return [];
	            }
	            var arr=[];
	            for(var i = 0;i<aData.length;i++){
	                arr.push((i<n?[i,NaN,aData[i][aData[i].length - 1]]:[i,(aData[i][1]-aData[i-n][1]),aData[i][aData[i].length - 1]]));
	            }
	            return arr;
	        }, 
	        /*
	            Data point needed: high, low, close
	            Parameter range: no parameter
	            bp=close-min(low,prev close);
	            tr = max(high,prevclose)-min(low,prevclose);
	            avg7=(bp1+...+bp7)/(tr1+....+tr7);
	            ult = 100*(4*avg7+2*avg14+avg28)/(4+2+1);
	            Intermediate
	            New
	         */
	        calculateUlt:function(aData,ty,n) {
	            if (aData == null) {
	                return [];
	            }
	            var arr=[],bp=[],tr=[],d=0,ult=[],k=0,bpSum=0,trSum=0,temp=0,result=0,tbp=0,ttr=0;
	            for(var i = 0;i<aData.length;i++){
	                d = aData[i];
	                if(i==0){
	                    bp[i]=d[1]-d[3];
	                    tr[i]=d[2]-d[3];
	                }else{
	                    tbp = d[1]-Math.min(d[3],aData[i-1][1]);
	                    ttr =Math.max(d[2],aData[i-1][1])-Math.min(d[3],aData[i-1][1]);
	                    if(isNaN(tbp)){
	                        bp[i] =bp[i-1];
	                    }else{
	                        bp[i]=tbp;
	                    }
	                    if(isNaN(ttr)){
	                        tr[i] =tr[i-1];
	                    }else{
	                        tr[i]=ttr;
	                    }
	                }
	                if(isNaN(bp[i])){
	                    bp[i]=0;
	                }
	                if(isNaN(tr[i])){
	                    tr[i]=0
	                }
	                if(i<28){
	                    bpSum +=bp[i];
	                    trSum +=tr[i];
	                    if(i==(k+6)){
	                        ult[k]=[];
	                        ult[k][0]=[bpSum,trSum];
	                    }
	                    if(i==(k+13)){
	                        ult[k][1]=[bpSum,trSum];
	                    }
	                    if(i==(k+27)){
	                        ult[k][2]=[bpSum,trSum];
	                    }
	                }
	                if(i>=27){
	                    if(k>0){
	                        temp = ult[k-1];
	                        ult[k]=[];
	                        ult[k][0]=[temp[0][0]-bp[i-28]+bp[i-21],temp[0][1]-tr[i-28]+tr[i-21]];
	                        ult[k][1]=[temp[1][0]-bp[i-28]+bp[i-14],temp[1][1]-tr[i-28]+tr[i-14]];  
	                        ult[k][2]=[temp[2][0]-bp[i-28]+bp[i],temp[2][1]-tr[i-28]+tr[i]];
	                    }
	                    result = 100*(4*ult[k][0][0]/ult[k][0][1]+2*ult[k][1][0]/ult[k][1][1]+ult[k][2][0]/ult[k][2][1])/7;
	                    arr.push([i,result,aData[i][aData[i].length - 1]]);
	                    k++;
	                }else{
	                    arr.push([i,NaN,aData[i][aData[i].length - 1]]);
	                }
	                if(isNaN(aData[i][1])&&typeof(arr[i][1])!="undefined"){ //intraday
	                    arr[i][1]=NaN;
	                }
	            }
	            return arr;
	        },
	        /*
	            Data point needed: close, high, low, volume
	            Parameter range: 0<m<100
	            Default parameter: m=14
	            tp = (high+low+close)/3;
	            mf = tp*vol;
	            mr = positive fm/negative mf;
	            mfi = 100-(100/(1+mr));
	            Intermediate
	            New
	         */
	        calculateMFI:function(aData,volData,ty,n){
	            if(!volData) return [];
	            if(isNaN(n)||n<1||n>99){return [];}
	            var len = aData.length;
	            var typicalp=[],pn=[],pmf=0,nmf=0,arr=[],temp;
	           
	            for(var i = 0;i<len;i++){
	                if(!volData[i]){
	                    continue;
	                }
	                if(isNaN(volData[i][1])){
	                    volData[i][1] =0;
	                }
	                temp =[i,NaN,aData[i][aData[i].length - 1]];
	                if (aData[i][1] == 0) { //no close price
	                    pn[i] = 0;
	                } else {
	                    typicalp[i] = (aData[i][1] + aData[i][2] + aData[i][3]) / 3;
	                    if (i > 0) {
	                        if(isNaN(typicalp[i])){
	                            pn[i - 1] = 0;
	                        }else{
	                            pn[i - 1] = typicalp[i] > typicalp[i - 1] ? 1 : (typicalp[i] == typicalp[i - 1] ? 0 : -1);
	                        }
	                        if (pn[i - 1] > 0) {
	                            pmf += typicalp[i] * volData[i][1];
	                        } else if (pn[i - 1] < 0) {
	                            nmf += typicalp[i] * volData[i][1];
	                        }
	                        if (i >= n) {
	                            if (i>n) {
	                                if (pn[i - n - 1] > 0) {
	                                    pmf -= typicalp[i - n] * volData[i - n][1]
	                                } else if (pn[i - n - 1] < 0) {
	                                    nmf -= typicalp[i - n] * volData[i - n][1];
	                                }
	                            }
	                            if (!isNaN(aData[i][1])) { //intraday
	                                temp[1] = 100 * pmf / (pmf + nmf);
	                            }
	                        }
	                    }
	                }
	                arr.push(temp);
	            }
	            return arr;
	        },
	        /*
	            Data point needed: volume, close price, high, low
	            Parameter range:  no parameter, daily
	            Default parameter: no parameter (however, we can draw MA OBV line, MA=30)
	            Volume x [Close - (High + Low)/2]
	            Easy
	            New
	         */
	        calculateVolumeAcc:function(aData,volData,ty,n){
	             // ty>10 volume data ty=1 historical daily data; ty=2 historical weekily data; ty=3 historical monthly data; ty=0 intraydayData.
	            var len = aData.length;
	            var vacc=[],arr=[];
	            if(!volData) return arr;
	            for(var i = 0;i<len;i++){
	                if(isNaN(volData[i][1])){
	                    volData[i][1] =0;
	                }
	                if (aData[i][1] == undefined || aData[i][1] == null || isNaN(aData[i][1]) || aData[i][1] == 0) { //no close price
	                    vacc[i] = NaN;
	                } else {
	                    vacc[i] = volData[i][1] * (aData[i][1] - (aData[i][2] + aData[i][3]) / 2);
	                }
	                arr.push([i,vacc[i],aData[i][aData[i].length-1]]);          
	            }
	            return arr;
	        },  
	        /*
	            Data point needed: Volume, Close, Yesterday close
	            Parameter range: no parameter, daily
	            if(cl>cl1) + volume
	                 ==    0
	                 <     -
	            obv = obvPre + ?;
	            Easy
	            New
	         */
	        calculateObv:function(aData,volData,ty,n){
	            var len = aData.length;
	            var obv=[],arr=[];
	            for(var i = 0;i<len;i++){
	                if(isNaN(volData[i][1])){
	                    volData[i][1] =0;
	                }
	                if(i==0){
	                    obv[i]=volData[0][1];
	                }else{
	                    if (aData[i][1] == undefined || aData[i][1] == null || isNaN(aData[i][1]) || aData[i][1] == 0) { //no close price
	                        obv[i] = NaN;
	                    }else if(i>0&&isNaN(aData[i-1][1])){ //jacky 2010.3.19 
	                        obv[i] = volData[i][1];
	                    } else{
	                        if(aData[i][1]>aData[i-1][1]){
	                            obv[i] = obv[i-1]+volData[i][1];
	                        }else if(aData[i][1]==aData[i-1][1]){
	                            obv[i] =obv[i-1];
	                        }else if(aData[i][1]<aData[i-1][1]){
	                            obv[i] =obv[i-1]-volData[i][1];
	                        }
	                    }
	                }
	                arr.push([i,obv[i],aData[i][aData[i].length-1]]);           
	            }
	            return arr;
	        },
	        /*
	            Methodology: Volume Ratio = Up Volume / Down Volume
	            UPVOL = SUM(VOL, m)  for up bars (CL > CL1)
	            DOWNVOL = SUM(VOL, m)  for down bars (CL < CL1)
	            UDVR = MA(UPVOL / DOWNVOL, n, maType)
	            m = Number of Periods
	            n = Smoothing Period
	            Referral Link:
	            1.http://www.linnsoft.com/tour/techind/udvr.htm
	            Data point needed: volume, close price, yesterday price
	            Parameter range: 0<m<100, 0<n<100
	            Default parameter: m=7, n=5
	            Hard
	            New
	         */
	        calculateUpDownRatio:function(aData,volData,ty,m,n){
	            if(!aData||aData.length<1||isNaN(n)||n<1||n>99||isNaN(m)||m<1||m>99){
	                return [];
	            }
	            var len = aData.length,
	                    datalen = len,
	                    valLen = volData.length;
				if (valLen < len && valLen > 0) {
					aData = aData.slice(len - valLen);
					len = valLen;
				}
	            var arr=[],close=[],sup=0, sdown=0, ud=[];
	            for (var i = 1, j = 0; i < len; i++, j++) {               
					if(!volData[i]){
						continue;
					}
					if(isNaN(volData[i][1])){
	                    volData[i][1] =0;
	                }
	                if (aData[i][1] == undefined || aData[i][1] == null || isNaN(aData[i][1]) || aData[i][1] == 0) {
	                    close[i - 1] = 0;
	                } else if (aData[i][1] > aData[i - 1][1]) {
	                    close[i - 1] = 1;
	                } else if (aData[i][1] < aData[i - 1][1]) {
	                    close[i - 1] = -1;
	                }else if (aData[i][1] == aData[i - 1][1]) {
	                    close[i - 1] = 2;
	                }else {
	                    close[i - 1] = 0;
	                }
	                if (close[j] == 1) {
	                    sup += volData[j + 1][1];
	                } else if (close[j] == -1) {
	                    sdown += volData[j + 1][1];
	                }else if (close[j] == 2) {
	                    sdown += volData[j + 1][1]/2;
	                    sup += volData[j + 1][1]/2;
	                }
	                if (j < m - 1 && valLen >= datalen) {
	                    ud[j] = -1;
	                    continue;
	                }
	                if (j >= m) {
	                    if (close[j - m] == 1) {
	                        sup -= volData[j - m + 1][1];
	                    } else if (close[j - m] == -1) {
	                        sdown -= volData[j - m + 1][1];
	                    }else if (close[j - m] == 2) {
	                        sdown -= volData[j - m + 1][1]/2;
	                        sup -= volData[j - m + 1][1]/2;
	                    }
	                }
	                ud[j] = [sup, sdown];
	            }
	            var mnlen = m+n,sum = 0,harr=[];
	            arr.push([0,NaN,aData[0][aData[0].length-1]]);
	            for(var i = 0;i<ud.length;i++){
	                if((ud[i]==-1) && valLen >= datalen){
	                    arr.push([i+1,NaN,aData[i][aData[i].length-1]]);
	                    continue;
	                }
	                if(ud[i][1]<0.00000001){
	                    harr[i]=0;
	                }else{
	                    harr[i]=ud[i][0]/ud[i][1];
	                }
	                sum +=harr[i];
	                if(i<mnlen-2){
	                    arr.push([i+1,NaN,aData[i][aData[i].length-1]]);
	                }else{
	                    if(i>mnlen-2){
	                        sum -=harr[i-n];
	                    }
	                    if(isNaN(aData[i][1])){ //intraday
	                        arr.push([i+1,NaN,aData[i+1][aData[i+1].length-1]]);
	                    }else{
	                        arr.push([i+1,sum/5,aData[i+1][aData[i+1].length-1]]);
	                    }
	                }
	            }
	            return arr;
	        },
	        /*
	            Data point needed: close price, volume
	            Parameter range: daily, Volume MA 2<n<200
	            Default parameter: 50
	            Easy
	         */
	        calculateVolPlus:function(aData,volData,ty,n) {
	            if (isNaN(n)||volData==null||volData.length<n) {
	                return [];
	            }
	            var arr=[],vol=[],sum=0;
	            for(var i = 0;i<volData.length;i++){
	                if(isNaN(volData[i][1])){
	                    volData[i][1] =0;
	                }
	                sum +=volData[i][1];
	                if(i<n){
	                    if(i==(n-1)){
	                        vol =[i,sum/n,aData[i][aData[i].length - 1]];
	                    }else{
	                        vol =[i,NaN,aData[i][aData[i].length - 1]];
	                    }
	                }else{
	                    sum -=volData[i-n][1];
	                    vol =[i,sum/n,aData[i][aData[i].length - 1]];
	                }
	                arr.push(vol);
	            }
	            return arr;
	        },
	        /*
	            Data point needed: Volume, accumulate volume, price
	            Parameter range: no parameter
	            This is a horizontal histogram that overlays a price chart. The histogram bars stretch from left
	              to right starting at the left side of the chart. The length of each bar is determined by the 
	              cumulative total of all volume bars for the periods during which the closing price fell within 
	              the vertical range of the histogram bar.
	            Hard
	            Overlay
	         */
	        calculateVolByPrice:function(aData,volData,ty,n){ //aData: price list. volData: volume data. n=12;
	            if (aData==null||volData==null) {
	                return [];
	            }
	            var len = aData.length,vacc=[],arr=[],min = Number.MAX_VALUE, max=Number.MIN_VALUE ;
	            for(var i = 0;i<len ;i++){
	                if(!isNaN(aData[i][1])){
	                    max = Math.max(max,aData[i][1]);
	                    min = Math.min(min,aData[i][1]);
	                }
	                            
	            }
	            var avg = (max-min)/12;
	            for(var i = 0;i<12;i++){
	                if(i===0){
	                    arr.push([min,(min+avg),0,0,0])
	                }else{
	                    arr.push([arr[i-1][1],(arr[i-1][1]+avg),0,0,0])
	                }
	            }
	            var d=0,v=0;
	            for(var i = 0;i<aData.length;i++){
	                if(isNaN(volData[i][1])){
	                    volData[i][1] =0;
	                }
	                d = aData[i][1];
	                v = volData[i][1];
	                if(d<=arr[0][1]){
	                    arr[0][2] +=volData[i][1]; //total volume. 3: down volume, 4: up volume
	                    (aData[i].length>4&&aData[i][5]==0)?(arr[0][4]+=v):(arr[0][3]+=v);
	                }else if(d<=arr[1][1]){
	                    arr[1][2] +=v;
	                    (aData[i].length>4&&aData[i][5]==0)?(arr[1][4]+=v):(arr[1][3]+=v);
	                }else if(d<=arr[2][1]){
	                    arr[2][2] +=v;
	                    (aData[i].length>4&&aData[i][5]==0)?(arr[2][4]+=v):(arr[2][3]+=v);
	                }else if(d<=arr[3][1]){
	                    arr[3][2] +=v;
	                    (aData[i].length>4&&aData[i][5]==0)?(arr[3][4]+=v):(arr[3][3]+=v);
	                }else if(d<=arr[4][1]){
	                    arr[4][2] +=v;
	                    (aData[i].length>4&&aData[i][5]==0)?(arr[4][4]+=v):(arr[4][3]+=v);
	                }else if(d<=arr[5][1]){
	                    arr[5][2] +=v;
	                    (aData[i].length>4&&aData[i][5]==0)?(arr[5][4]+=v):(arr[5][3]+=v);
	                }else if(d<=arr[6][1]){
	                    arr[6][2] +=v;
	                    (aData[i].length>4&&aData[i][5]==0)?(arr[6][4]+=v):(arr[6][3]+=v);
	                }else if(d<=arr[7][1]){
	                    arr[7][2] +=v;
	                    (aData[i].length>4&&aData[i][5]==0)?(arr[7][4]+=v):(arr[7][3]+=v);
	                }else if(d<=arr[8][1]){
	                    arr[8][2] +=v;
	                    (aData[i].length>4&&aData[i][5]==0)?(arr[8][4]+=v):(arr[8][3]+=v);
	                }else if(d<=arr[9][1]){
	                    arr[9][2] +=v;
	                    (aData[i].length>4&&aData[i][5]==0)?(arr[9][4]+=v):(arr[9][3]+=v);
	                }else if(d<=arr[10][1]){
	                    arr[10][2] +=v;
	                    (aData[i].length>4&&aData[i][5]==0)?(arr[10][4]+=v):(arr[10][3]+=v);
	                }else if(d<=arr[11][1]){
	                    arr[11][2] +=v;
	                    (aData[i].length>4&&aData[i][5]==0)?(arr[11][4]+=v):(arr[11][3]+=v);
	                }
	            }
	            var large = arr[0][2];
	            for(var i = 1;i<12;i++){
	                large = Math.max(large,arr[i][2])
	            }
	            var v = [arr,[min,max],large];
	            //console.log(v);
	            return v;
	        },  
	        calculateDividendYield: function (aData, dData, ty) {
	            var dlen = dData.length, alen = aData.length, arr = [], map = {}, mapNum = {}, tempYear = 0, dt;
	            if (dlen == 0 || alen == 0) {
	                return [];
	            }
	            for(var i = 0;i<dlen;i++){
	                if(dData[i].Type != 1) continue;
	                dt = QSAPI.Widget.Chart.Util.toDateFromIndex(dData[i].DateIndex);
	                tempYear = dt.getFullYear();
	                map[tempYear+""] = parseFloat(dData[i].Desc);
	                if (!mapNum[tempYear + ""]) {
	                    mapNum[tempYear + ""] = 1;
	                }else {
	                    mapNum[tempYear + ""]++;
	                }
	            }
	            var year =0,val, isIntraday = !isNaN(ty);
	            if(isIntraday){    // intraday 
	                val = 100*map[tempYear+""]*mapNum[tempYear+""];
	            }
	            for(var i = 0;i<alen;i++){
	                var tarr =[i,NaN,aData[i][aData[i].length - 1]];
	                if(!isNaN(aData[i][1])){
	                    if(!isIntraday){
	                        year = QSAPI.Widget.Chart.Util.toDateFromIndex(aData[i][aData[i].length-1]).getFullYear();
	                        val = 100*map[year+""]*mapNum[year+""]; 
	                    }   
	                    tarr[1] = val/aData[i][1];
	                }
	                arr.push(tarr);
	            }
	            return arr;
	        },
	        calculateFVolatility:function(aData,ty){
	            if(!aData || !aData.length)return [];
	            var tr,low,high,sum = 0, fv = [], k = 2/(21+1);
	            for(var i = 0;i < aData.length;i++){
	                fv[i]=[aData[i][0],NaN,aData[i][aData[i].length-1]];
	                if(aData[i].length<4||isNaN(aData[i][1])||isNaN(aData[i][2])||isNaN(aData[i][3]))continue;
	                low = aData[i][3], high = aData[i][2];  
	                if (i > 0) {
	                    for (var j = i - 1; j >= 1; j--) {
	                        if (!isNaN(aData[j][1])) {
	                            break;
	                        }
	                    }
	                    if (aData[j][1] < aData[i][3]) {
	                        low = aData[j][1];
	                    }
	                    if (aData[j][1] > aData[i][2]) {
	                        high = aData[j][1];
	                    }
	                }
	                tr = (high-low)/low;
	                if(i<21){
	                    sum+=tr;
	                }else if (i==21){
	                    fv[i][1]=sum/21;
	                } else {
	                    fv[i][1] = k*fv[j][1]+(1-k)*tr;
	                }
	            }
	            return fv;
	        },
	        calculateVolatility:function(aData,ty,p,pa){
	            if (!aData || aData.length===0) 
	                return [];
	            var arr = [];
	            var arrHL = [],temp=0;
	            for (var i = 0; i < aData.length; i++) {
	                arrHL.push(aData[i][2] - aData[i][3]);
	            }
	            var arrEma1 = this.EMA(arrHL, p);
	            for(var i = 0;i<aData.length;i++){
	                if(i<(p+pa-1)){
	                    arr.push([i, NaN, aData[i][aData[i].length - 1]]); 
	                }else{
	                    temp = (arrEma1[i]/arrEma1[i-pa]-1)*100;
	                    arr.push([i, temp, aData[i][aData[i].length - 1]]);
	                }
	            }
	            return arr;
	        },
	        calculateMass:function(aData,ty){
	            if (!aData || !aData.length) 
	                return [];
	            var arr = [], previous = NaN, temp = 0;
	            var arrHL = [];
	            for (var i = 0; i < aData.length; i++) {
	                arrHL.push(aData[i][2] - aData[i][3]);
	            }
	            var arrEma1 = this.EMA(arrHL, 9);
	            var arrEma2 = this.EMA(arrEma1, 9);
	            var sum = 0,arrCompare=[];
	            for (var i = 0; i < arrEma2.length; i++) {
	                if (i < 16) {
	                    arr.push([i, NaN, aData[i][aData[i].length - 1]]);
	                    continue;
	                }
	                arrCompare[i]=arrEma1[i]/arrEma2[i];
	                if (!isNaN(arrCompare[i])) {
	                    sum += arrCompare[i];
	                }
	                if (i < 40) {
	                    arr.push([i, NaN, aData[i][aData[i].length - 1]]);
	                    continue;
	                }
	                if (i > 40) {
	                    if (!isNaN(arrCompare[i - 25])) {
	                        sum -= arrCompare[i - 25];
	                    }
	                }
	                arr.push([i, sum, aData[i][aData[i].length - 1]]);
	            }
	            return arr;
	        },
	        calculateAccDis:function(aData,volData,ty){
	            if(!aData || !aData.length||volData.lenght==0)return [];
	            var arr =[],previous = NaN,temp=0;
	            for(var i = 0;i<aData.length;i++){
	                temp = (2*aData[i][1]-aData[i][2]-aData[i][3])*volData[i][1]/(aData[i][2]-aData[i][3])+(isNaN(previous)?0:previous);
	                arr[i]=[i,temp,aData[i][aData[i].length - 1]];
	                if(!isNaN(temp)){
	                    previous = temp;
	                }
	            }
	            return arr;
	        },
	        calculateWMA:function(aData,ty,p){
	            if(!aData || !aData.length)return [];
	            if(typeof(p)=="undefined"||p<1){
	                p=15;
	            }
	            var denominator  = p*(p+1)/2,arr=[],tsum =0,index=0,sum;
	            for(var i = 0;i<aData.length;i++){
	                tsum =0;
	                if(i<p-1||isNaN(aData[i][1])){
	                    arr[i]=[i,NaN,aData[i][aData[i].length - 1]];
	                }else {
	                    sum  = denominator;
	                    index = 1;
	                    for (var j = i - p + 1; j <= i; j++,index++) {
	                        if(isNaN(aData[j][1])){ // filter the NaN data point.
	                            sum -= index;
	                        }else{
	                            tsum += aData[j][1]* index;
	                        }   
	                    }
	                    arr[i] = [i, tsum / sum, aData[i][aData[i].length - 1]];
	                }
	            }
	            return arr;
	        },
	        calculateKeltner:function(aData,ty,e,t,a){
	            if(!aData || !aData.length)return [[],[],[]];
	            var arr=[],atr=[],ema=[],close=[],aa=[],bb=[],cc=[];
	            for(var i =0;i<aData.length;i++){
	                close.push(aData[i][1]);
	            }
	            atr = this.calculateAtr(aData,0,a);
	            ema = this.EMA(close,e);
	            for(var i =0;i<aData.length;i++){
	                if(isNaN(ema[i])||isNaN(atr[i][1])){
	                    aa.push([i, NaN,NaN, aData[i][aData[i].length - 1]]);
	                    bb.push([i, NaN,NaN, aData[i][aData[i].length - 1]]);
	                    cc.push([i, NaN,NaN, aData[i][aData[i].length - 1]]);
	                }else{
	                    aa.push([i, ema[i]+2*atr[i][1],aData[i][1], aData[i][aData[i].length - 1]]);
	                    bb.push([i, ema[i],aData[i][1], aData[i][aData[i].length - 1]]);
	                    cc.push([i, ema[i]-2*atr[i][1],aData[i][1], aData[i][aData[i].length - 1]]);
	                }
	            }
	            return [aa,bb,cc];
	        },
	        calculateAtr:function(aData,ty,p){
	            if(!aData || !aData.length)return [];
	                if (typeof (p) == "undefined") {
	                    p=14;
	                }
	                var arr = [],j=0,sum=0,hl=0,a=0,b=0,c=0,pre=0;
	                for(var i = 0;i<aData.length;i++){
	                    
	                    a = aData[i][2]-aData[i][3];
	                    if(i>0){
	                        b = Math.abs(aData[i][2]-aData[i-1][1]);
	                        c = Math.abs(aData[i][3]-aData[i-1][1]);
	                    }
	                    if(i<p-1){
	                        if(i==0&&!isNaN(a)){
	                            j++;
	                            sum +=a;
	                        }else if(!isNaN(a)&&!isNaN(b)&&!isNaN(c)){
	                            j++;
	                            sum +=Math.max(Math.max(a,b),c);
	                        }
	                        arr.push([i, NaN, aData[i][aData[i].length - 1]]);
	                    }else if(i==p-1){
	                        if(!isNaN(a)&&!isNaN(b)&&!isNaN(c)){
	                            j++;
	                            sum +=Math.max(Math.max(a,b),c);
	                        }
	                        pre = sum/j;
	                        arr.push([i, pre, aData[i][aData[i].length - 1]]);
	                    }else{
	                        if (!isNaN(a) && !isNaN(b) && !isNaN(c)) {
	                            if (!isNaN(pre)) {
	                                pre = (pre * (p - 1) + Math.max(Math.max(a, b), c)) / p;
	                                arr.push([i, pre, aData[i][aData[i].length - 1]]);
	                            } else {
	                                j++;
	                                sum += Math.max(Math.max(a, b), c);
	                                pre = sum / j;
	                                arr.push([i, NaN, aData[i][aData[i].length - 1]]);
	                            }
	                            arr.push([i, pre, aData[i][aData[i].length - 1]]);
	                        } else {
	                            arr.push([i, NaN, aData[i][aData[i].length - 1]]);
	                        }
	                    }
	                }
	                return arr;
	        },
	        calculateForceIndex:function(aData,volData,ty,p){
	            if(!aData || !aData.length||volData.lenght==0)return [];
	            var arr=[],ema=[];
	            //arr.push([0, NaN, aData[0][aData[0].length - 1]]); 
	            
				var voLen = volData.length;
				var dalen = aData.length;
				if (voLen < dalen) {
					aData = aData.slice(dalen - voLen);
				}
				
	            for(var i = 0;i<aData.length;i++){
	                if(isNaN(aData[i][1])||isNaN(volData[i][1])||i==0){
	                    ema.push(NaN); 
	                }else{
	                    ema.push((aData[i][1]-aData[i-1][1])*volData[i][1]); 
	                }
	            }
	            ema = this.EMA(ema,p);
	            for(var i =0;i<ema.length;i++){
	                arr.push([i, ema[i], aData[i][aData[i].length - 1]]); 
	            }
	            return arr;
	        }
	    };
	})(QSAPI);
	
	(function(QSAPI) {
	    var $ = QSAPI.$;
	    var Util = QSAPI.Util;
	    var dateUtil = QSAPI.Widget.Chart.Util;
	    var FIELDS = {
	        "D17": "Open",
	        "D18": "High",
	        "D19": "Low",
	        "D2": "Last",
	        "D16": "Volume",
	        "D953": "OriginalDate",
	        "D952": "OriginalTime"
	    };
	    if (!QSAPI.Widget) {
	        QSAPI.Widget = {};
	    }
	    if (!QSAPI.Widget.Chart) {
	        QSAPI.Widget.Chart = {};
	    }
	
	    function convertData(data) {
	        var convertData = {};
	        for (var field in data) {
	            if (!data[field]) {
	                continue;
	            }
	            var outKey = FIELDS[field];
	            if (outKey) {
	                $.each(outKey.split(','), function(i, k) {
	                    convertData[$.trim(k)] = data[field];
	                });
	            } else {
	                convertData[field] = data[field];
	            }
	        }
	        return convertData;
	    }
	
	    function getDayPrice(dayPriceArray, date) {
	        var ret;
	        for (var i = 0, l = dayPriceArray.length, dayPrice; i < l; i++) {
	            dayPrice = dayPriceArray[i];
	            if (dayPrice.OriginalDate === date) {
	                ret = {
	                    Last: dayPrice.Close,
	                    Open: dayPrice.Open,
	                    OriginalDate: dayPrice.OriginalDate,
	                    PreviousClosePrice: i > 0 ? dayPriceArray[i - 1].Close : null
	                };
	                break;
	            }
	        }
	        if (!ret) {
	            ret = {};
	            logger.warn('morningstar.marketdata.timeseries.fod', 'Can not get dayprice with date', {
	                dayPrice: dayPrice
	            });
	        }
	        return ret;
	    }
	
	    function convertTimezone(ticker, rdate, rtime) {
	        var gmtTimeDiff = Util.getGMTOffsetMinutes(ticker);
	        var convertTime = rtime,
	            convertDate = rdate;
	        if (gmtTimeDiff) {
	            var date = Util.formatRTDateTimeAsDate(rdate, rtime);
	            if (date) {
	                if (rtime) {
	                    date = new Date(date.getTime() + gmtTimeDiff * 60000);
	                }
	                convertDate = dateUtil.formatDateYMD(date, '');
	                convertTime = Util.formatDateString(date, 'time');
	            }
	        }
	        return {
	            date: convertDate,
	            time: convertTime
	        };
	    }
	
	    function parseHistoricalData(results, params) {
	        var items = [];
	        $.each(results || [], function(i, r) {
	            r.data = r.data || [];
	            var ticker = [r.exchangeid, r.type, r.symbol].join('.');
	            var openCloseTime = Util.getOpenClose(ticker);
	            $.each(openCloseTime, function(key, val) {
	                openCloseTime[key] = dateUtil.toHourMinutes(val);
	            });
	            r.data.sort(function(a, b) {
	                if (Util.formatRTDateTimeAsDate(a['D953']) > Util.formatRTDateTimeAsDate(b['D953'])) {
	                    return 1;
	                } else {
	                    return -1;
	                }
	            });
	            $.each(r.data, function(n, d) {
	                r.data[n] = convertData(d);
	                var tz = convertTimezone(ticker, d['D953'], d['D952']);
	                r.data[n]['Time'] = tz.time;
	                r.data[n]['Date'] = tz.date;
	                r.data[n]['Index'] = dateUtil.toDateIndex(dateUtil.convertYMDToDate(tz.date));
	                r.data[n]['FillFlag'] = 0;
	            });
	            items.push({
	                Ticker: ticker,
	                Frequency: params.frequency,
	                OpenTime: openCloseTime.open,
	                CloseTime: openCloseTime.close,
	                CloseMinutes: dateUtil.timeToMinutes(openCloseTime.close),
	                OpenMinutes: dateUtil.timeToMinutes(openCloseTime.open),
	                PreOpenTime: openCloseTime.preOpen,
	                PostCloseTime: openCloseTime.postClose,
	                DailyData: {
	                    LastPrice: r.data
	                }
	            });
	        });
	        return {
	            Data: items,
	            Status: {
	                ErrorCode: '0',
	                ErrorMsg: "Succeeded"
	            }
	        };
	    }
	
	    function parseIntradayData(results, params) {
	        var items = [];
	        $.each(results || [], function(i, r) {
	            r.data = r.data || [];
	            var dailyData = [];
	            var ticker = [r.exchangeid, r.type, r.symbol].join('.');
	            var openCloseTime = Util.getOpenClose(ticker);
	            $.each(openCloseTime, function(key, val) {
	                openCloseTime[key] = dateUtil.toHourMinutes(val);
	            });
	            r.data.sort(function(a, b) {
	                if (a.DayPrice) {
	                    a = {
	                        "D953": a.DayPrice[0].D953,
	                        "D952": '00:00'
	                    };
	                }
	                if (b.DayPrice) {
	                    b = {
	                        "D953": b.DayPrice[0].D953,
	                        "D952": '00:00'
	                    };
	                }
	                if (dateUtil.formatDateAsInteger(a['D953'], a['D952']) > dateUtil.formatDateAsInteger(b['D953'], b['D952'])) {
	                    return 1;
	                } else {
	                    return -1;
	                }
	            });
	            var dayPriceArray = [],
	                dayPriceIdxMapping = {},
	                currDate, closeTime, openTime, dayData;
	            $.each(r.data, function(n, d) {
	                if (d.DayPrice) {
	                    var dayPrice = new Object();
	                    dayPrice = convertData(d.DayPrice[0]);
	                    dayPrice.Date = convertTimezone(ticker, d.DayPrice[0]['D953']).date;
	                    if (dayPriceIdxMapping[dayPrice.OriginalDate] >= 0) {
	                        dayPriceArray[dayPriceIdxMapping[dayPrice.OriginalDate]] = dayPrice;
	                    } else {
	                        dayPriceArray.push(dayPrice);
	                        dayPriceIdxMapping[dayPrice.OriginalDate] = dayPriceArray.length > 0 ? dayPriceArray.length - 1 : 0;
	                    }
	                } else {
	                    if (!currDate || currDate != d['D953']) {
	                        currDate = d['D953'];
	                        closeTime = Util.formatRTDateTimeAsDate(currDate, openCloseTime.close);
	                        openTime = Util.formatRTDateTimeAsDate(currDate, openCloseTime.open);
	                        dayData = new Object();
	                        dayData.SubDataPoints = [];
	                        dayData.rtDate = d['D953'];
	                        dayData.Date = convertTimezone(ticker, d['D953']).date;
	                        dayData.Index = dateUtil.toDateIndex(dateUtil.convertYMDToDate(dayData.Date));
	                        dayData.FillFlag = 0;
	                        if (typeof dayPriceIdxMapping[dayData.rtDate] === 'undefined') {
	                            dayPriceArray.push({
	                                OriginalDate: dayData.rtDate
	                            });
	                            dayPriceIdxMapping[dayData.rtDate] = dayPriceArray.length > 0 ? dayPriceArray.length - 1 : 0;
	                        }
	                    }
	                    var dp = convertData(d);
	                    var tz = convertTimezone(ticker, d['D953'], d['D952']);
	                    dp['Time'] = dateUtil.timeToMinutes(tz.time);
	                    dp['Date'] = tz.date;
	                    dp['FillFlag'] = 0;
	                    var currTime = Util.formatRTDateTimeAsDate(currDate, tz.time);
	                    if (currTime <= closeTime && currTime >= openTime) {
	                        dayData.SubDataPoints.push(dp);
	                    }
	                    if (!r.data[n + 1] || currDate != r.data[n + 1]['D953']) {
	                        dailyData.push(dayData);
	                    }
	                }
	            });
	            var truncateDailyData = [];
	            for (var i = 0, dayPrice; i < dailyData.length; i++) {
	                dayPrice = getDayPrice(dayPriceArray, dailyData[i].rtDate);
	                dailyData[i].Last = dayPrice.Last;
	                dailyData[i].Open = dayPrice.Open;
	                dailyData[i].PreviousClosePrice = dayPrice.PreviousClosePrice;
	                dailyData[i].OriginalDate = dayPrice.OriginalDate;
	                if (params.tradingDays) {
	                    if (i >= (dailyData.length - params.tradingDays)) {
	                        truncateDailyData.push(dailyData[i]);
	                    }
	                } else if (params.startDate) {
	                    if (Util.formatRTDateTimeAsDate(dailyData[i].rtDate) >= Util.formatRTDateTimeAsDate(params.startDate)) {
	                        truncateDailyData.push(dailyData[i]);
	                    }
	                } else {
	                    truncateDailyData.push(dailyData[i]);
	                }
	                delete dailyData[i].rtDate;
	            }
	
	            items.push({
	                Ticker: ticker,
	                Frequency: params.frequency,
	                OpenTime: openCloseTime.open,
	                CloseTime: openCloseTime.close,
	                CloseMinutes: dateUtil.timeToMinutes(openCloseTime.close),
	                OpenMinutes: dateUtil.timeToMinutes(openCloseTime.open),
	                PreOpenTime: openCloseTime.preOpen,
	                PostCloseTime: openCloseTime.postClose,
	                DailyData: {
	                    LastPrice: truncateDailyData
	                }
	            });
	        });
	        return {
	            Data: items,
	            Status: {
	                ErrorCode: '0',
	                ErrorMsg: "Succeeded"
	            }
	        };
	    }
	
	    function parse(result, params) {
	        result.ts = result.ts || {};
	        var data;
	        if (isNaN(params.frequency)) {
	            data = parseHistoricalData(result.ts.results, params);
	        } else {
	            data = parseIntradayData(result.ts.results, params);
	        }
	        return data;
	    }
	
	    if (!QSAPI.fod) {
	        QSAPI.fod = {};
	    }
	
	    QSAPI.fod.transformFodToChartData = parse;
	
	})(QSAPI);
	// Expose QSAPI in window
	if ( typeof noGlobal === strundefined ) {
	    console.info('QSAPI is  window');
	    window.QSAPI = QSAPI;
	}
	
		return QSAPI;
	});

/***/ }),
/* 3 */
/***/ (function(module, exports) {

	/* WEBPACK VAR INJECTION */(function(__webpack_amd_options__) {module.exports = __webpack_amd_options__;
	
	/* WEBPACK VAR INJECTION */}.call(exports, {}))

/***/ })
/******/ ])
});
;
//# sourceMappingURL=markets-components-chartdatamanager.js.map