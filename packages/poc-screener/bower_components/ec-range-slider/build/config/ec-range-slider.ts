// startref
/// <reference path='../../.references.ts'/>
// endref
module EcRangeSlider {
    export enum _Type { ecRangeSlider }

    /**
     * @title RangeSlider
     */
    export interface EcRangeSlider {
        /** Component type.
         * @default ecRangeSlider
         * @options.hidden true
         * @type string
         * @readonly true
         */
        type: _Type;

        settings: Settings;

        labels: Labels;
    }
}
