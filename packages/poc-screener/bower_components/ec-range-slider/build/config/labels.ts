// startref
/// <reference path='../../.references.ts'/>
// endref
module EcRangeSlider {
    export interface Labels {
        max: AsterixCore.Label;
        min: AsterixCore.Label;
    }
}