// startref
/// <reference path='../../.references.ts'/>
// endref
module EcRangeSlider {
    export interface Settings extends AsterixCore.ComponentSettings {
        _templates: Templates;
        _parameters: {
            animate: any;
            behaviour: any;
            callback: any;
            connect: any;
            disabled: any;
            end: any;
            hideMinMaxLabels: any;
            hideSlider: any;
            link: any;
            margin: any;
            pips: any;
            range: any;
            showInputs: any;
            showMinMax: any;
            snap: any;
            start: any;
            step: any;
            tooltips: boolean;
        };

        /**
         * @default true
         */
        animate: boolean;
        /** Apply number format like integrer, decimal */
        applyNumberFormat: any;
        /**
         * @default none
         */
        behaviour: string;
        /**
         * @default slide
         */
        callback: string;
        /**
         * @default false
         */
        connect: any;
        /**
         * @default false
         */
        disabled?: boolean;

        /**
         * @default 10
         */
        end: number;
        /**
         * @default false
         */
        hideMinMaxLabels: boolean;

        /**
         * @default false
         */
        hideSlider: boolean;

        /**
         * @default {}
         */
        link: any;
        /**
         * @default false
         */
        margin: string;
        /**
         * @default ec-range-slider
         */
        namespaceClass: string;
        /**
         * @default {}
         */
        pips: any;
        /**
         * @default false
         */
        range: any;
        /**
         * @default false
         */
        showInputs: boolean;
        /**
         * @default true
         */
        showMinMax: boolean;
        /**
         * @default false
         */
        snap: boolean;
        /**
         * @default 1
         */
        start: number;
        /**
         * @default 1
         */
        step: number;
        /**
         * @default false
         */
        tooltips: boolean;
    }

    export interface Templates {
        /** The main template.
         * @format html
         * @default <div>\r\n\r\n    <div class=\"{{namespaceClass('__container')}}\">\r\n\r\n        <!-- Single input -->\r\n        <div data-ng-show=\"!twoHandles && parameters.showInputs\" class=\"{{namespaceClass('__inputs')}} {{namespaceClass('__inputs--single')}}\">\r\n            <span>\r\n                <input type=\"number\" data-ng-model=\"ngModel\" data-ng-disabled=\"parameters.disabled\" class=\"{{namespaceClass('__input')}} {{namespaceClass('__input--from')}}\" \/>\r\n            <\/span>\r\n        <\/div>\r\n\r\n        <!-- Double inputs -->\r\n        <span data-ng-show=\"twoHandles && parameters.showInputs\" class=\"{{namespaceClass('__inputs')}} {{namespaceClass('__inputs--min')}}\">\r\n            <input type=\"number\" data-ng-model=\"ngFrom\" data-ng-disabled=\"parameters.disabled\" class=\"ec-form__input ec-form__input--number {{namespaceClass('__input')}} {{namespaceClass('__input--from')}}\" \/>\r\n        <\/span>\r\n\r\n        <div data-ng-class=\"{ '{{namespaceClass('__slider--single')}}': !parameters.showInputs }\" class=\"{{namespaceClass('__slider')}}\">\r\n            <div data-ng-disabled=\"parameters.disabled\" data-ng-hide=\"parameters.hideSlider\" class=\"{{namespaceClass('__placeholder')}}\"><\/div>\r\n        <\/div>\r\n\r\n        <span data-ng-show=\"twoHandles && parameters.showInputs\" class=\"{{namespaceClass('__inputs')}} {{namespaceClass('__inputs--max')}}\">\r\n            <input type=\"number\" data-ng-model=\"ngTo\" data-ng-disabled=\"parameters.disabled\" class=\"ec-form__input ec-form__input--number {{namespaceClass('__input')}} {{namespaceClass('__input--to')}}\" \/>\r\n        <\/span>\r\n\r\n    <\/div>\r\n\r\n    <div class=\"{{namespaceClass('__range')}}\" data-ng-if=\"parameters.showMinMax\">\r\n        <span class=\"{{namespaceClass('__range-value')}} {{namespaceClass('__range-value--min')}}\"><span data-ng-hide=\"parameters.hideMinMaxLabels\">{{labels.get('min')}} <\/span>{{parameters.start}}<\/span>\r\n        <span class=\"{{namespaceClass('__range-value')}} {{namespaceClass('__range-value--max')}}\"><span data-ng-hide=\"parameters.hideMinMaxLabels\">{{labels.get('max')}} <\/span>{{parameters.end}}<\/span>\r\n    <\/div>\r\n<\/div>\r\n
         */
        main: string;
    }
}
