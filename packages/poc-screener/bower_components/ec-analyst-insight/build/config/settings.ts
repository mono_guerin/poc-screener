// startref
/// <reference path='../../.references.ts'/>
// endref
module EcAnalystInsight {
    export interface Settings extends AsterixNg.ComponentSettings {
        _templates: Templates;
        _parameters: any;

        _model: AsterixCore.DataSourceSettings;

        /** namespaceClass
         * @format string
         * @default ec-analyst-insight
         */
        namespaceClass: string;
    }

    export interface Templates {
        /** The main template.
         * @format html
         * @default <ec-section title=\"{{labels.get('insightTitle')}}\">\r\n    <ec-container class=\"{{namespaceClass('experts-view')}}\" mstar-component-id=\"containerAnalystInsight\">\r\n        <ec-section section-id=\"expertsInfo\" class=\"{{namespaceClass('__experts-details')}}\">\r\n            <figure class=\"{{namespaceClass('__figure-image')}}\">\r\n                <img class=\"{{namespaceClass('__experts-details--image')}}\" data-ng-src=\"{{model.expertsInfo.dpUrl}}\" alt=\"{{labels.get('imageThumbnail')}}\"\/>\r\n                <figcaption class=\"{{namespaceClass('__experts-details--info')}}\">\r\n                    <div class=\"{{namespaceClass('__experts-details--info-name')}}\">{{model.expertsInfo.name}}<\/div>\r\n                    <div class=\"{{namespaceClass('__experts-details--info-role')}}\">{{model.expertsInfo.role}}<\/div>\r\n                <\/figcaption>\r\n            <\/figure>\r\n        <\/ec-section>\r\n        <ec-section class=\"{{namespaceClass('__experts-review')}}\" section-id=\"expertsView\">\r\n            <div class=\"{{namespaceClass('__experts-review--content')}}\" data-ng-bind-html=\"model.expertsInfo.commentaryTrustedHTML\"><\/div>\r\n        <\/ec-section>\r\n        <ec-section class=\"{{namespaceClass('__experts-video')}}\" section-id=\"expertsVideo\">\r\n            <ec-video data-inline-template=\"\" mstar-component-id=\"expertsVideo\">\r\n                <figure class=\"{{namespaceClass('__figure-video')}}\">\r\n                    <img data-ng-if=\"model.expertsInfo.videoThumbUrl && model.expertsInfo.videoUrl\" class=\"ec-video-modal-preview-thumb\" alt=\"{{labels.get('videoThumbnail')}}\" data-ng-src=\"{{model.expertsInfo.videoThumbUrl}}\" title=\"{{labels.get('videoThumbnailTitle')}}\">\r\n                    <figcaption data-ng-if=\"labels.contains('figCaptionVideo')\" class=\"{{namespaceClass('__figcaption-video')}}\">{{::labels.get(\"figCaptionVideo\")}}<\/figcaption>\r\n                <\/figure>\r\n            <\/ec-video>\r\n        <\/ec-section>\r\n    <\/ec-container>\r\n<\/ec-section>\r\n\r\n
         */
        main: string;
    }
}
