// startref
/// <reference path='../../.references.ts'/>
// endref
module EcAnalystInsight {
    export interface Labels {
        imageThumbnail: AsterixCore.Label;
        insightTitle: AsterixCore.Label;
        videoThumbnail: AsterixCore.Label;
    }
}
