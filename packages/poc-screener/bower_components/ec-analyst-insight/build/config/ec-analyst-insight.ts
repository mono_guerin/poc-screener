// startref
/// <reference path='../../.references.ts'/>
// endref
module EcAnalystInsight {
    export enum _Type { ecAnalystInsight }

    /**
     * @title AnalystInsight
     */
    export interface EcAnalystInsight {
        /** Component type.
         * @default ecAnalystInsight
         * @options.hidden true
         * @type string
         * @readonly true
         */
        type: _Type;

        settings: Settings;

        labels: Labels;

        components: {
            containerAnalystInsight: EcContainer.EcContainer;
            expertsVideo: EcVideo.EcVideo;
        }
    }
}
