(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("morningstar"));
	else if(typeof define === 'function' && define.amd)
		define("markets-components-core", ["morningstar"], factory);
	else if(typeof exports === 'object')
		exports["markets-components-core"] = factory(require("morningstar"));
	else
		root["markets-components-core"] = factory(root["morningstar"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_1__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(1), __webpack_require__(2), __webpack_require__(3)], __WEBPACK_AMD_DEFINE_RESULT__ = function(morningstar, breakpoint, util) {
	    'use strict';
	    var marketscore = util.getComponentName('core');
	    morningstar.asterix.register(marketscore);
	
	    var QSAPI = morningstar.components[marketscore];
	    __webpack_require__(4)(window, QSAPI);
	
	    var $ = QSAPI.$;
	    $.extend(QSAPI.Util, util);
		QSAPI.getBreakpointConfig = breakpoint.getBreakpointConfig;
	    QSAPI.components = __webpack_require__(6);
	    return QSAPI;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ }),
/* 1 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_1__;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	    var breakpointConfig = {
	        "1": {
	            "client": "morn",
	            "breakPoint": [600, 800, 1000]
	        },
	        "2": {
	            "client": "quest",
	            "breakPoint": [600, 800, 1000]
	        },
	        "3": {
	            "client": "msn",
	            "breakPoint": [600, 800, 1000]
	        },
	        "4": {
	            "client": "theme4",
	            "breakPoint": [380, 600, 800, 1000]
	        },
	        "5": {
	            "client": "fide",
	            "breakPoint": [600, 800, 1000]
	        },
	        "6": {
	            "client": "pats",
	            "breakPoint": [600, 800, 1000]
	        }
	    };
	    return {
	        getBreakpointConfig: function(theme){
	            return breakpointConfig[theme];
	        }
	    }
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(1)], __WEBPACK_AMD_DEFINE_RESULT__ = function(morningstar) {
	    var rnotwhite = (/\S+/g);
	    var quikrComponentPrefix = 'markets-components-';
	
	    function standardizeLangSymbol(lang) {
	        lang = lang.replace(/(\w{2})[-_](\w{2})/, function(lang, $1, $2) {
	            return $1.toLowerCase() + '-' + $2.toUpperCase();
	        });
	        lang = lang.replace('UK', 'GB');
	        lang = lang.replace('jp', 'ja');
	        return lang;
	    }
	
	    function isWithMUIPrefix(object) {
	        if (object.isWithMUIPrefix) {
	            object.isWithMUIPrefix = 'mui-';
	        } else {
	            object.isWithMUIPrefix = '';
	        }
	        if (object.addMuiClassInTopElement) {
	            object.addMuiClassInTopElement = 'mui';
	        } else {
	            object.addMuiClassInTopElement = '';
	        }
	        return object;
	    }
	
	    function isClassNameWithMuiPrefix(isWithMUIPrefix, className) {
	        if (!isWithMUIPrefix) {
	            return className;
	        }
	        var classes = [];
	        var returnClassName = '';
	        if (typeof className === 'string' && className) {
	            classes = className.match(rnotwhite) || [];
	        }
	        if (classes.length > 0) {
	            for (var i = 0; i < classes.length; i++) {
	                returnClassName += "mui-" + classes[i] + " ";
	            };
	        }
	        return returnClassName;
	    }
	
	    function deferredExec(deferred, fun) {
	        switch (deferred.state()) {
	            case 'pending':
	                deferred.done(fun);
	                break;
	            case 'resolved':
	                fun();
	                break;
	            case 'rejected':
	                break;
	        }
	    }
	
	    function getComponentName(name) {
	        return quikrComponentPrefix + name;
	    }
	
	    var currencyMap = {
	        "USD,CAN,CAD,ARS,AUD,BSD,BBD,BMD,BND,KYD,CLP,COP,XCD,SVC,FJD,GYD,LRD,MXN,NAD,NZD,SGD,SBD,SOS,SRD,TVD": {
	            "unicode": "\u0024",
	            "symbol": "$"
	        },
	        "GBP,EGP,FKP,GIP,GGP,IMP,JEP,LBP,SHP,SYP": {
	            "unicode": "\u00a3",
	            "symbol": "£"
	        },
	        "GBX,BWP": {
	            "unicode": "\u0050",
	            "symbol": "P",
	            "orient": "right"
	        },
	        "EUR": {
	            "unicode": "\u20ac",
	            "symbol": "€",
	            "orient": "right"
	        },
	        "ANG,AWG": {
	            "unicode": "\u0192",
	            "symbol": "ƒ"
	        },
	        "CNY,JPY": {
	            "unicode": "\u00a5",
	            "symbol": "¥",
	        },
	        "GHC": {
	            "unicode": "\u00a2",
	            "symbol": "¢"
	        },
	        "ALL": {
	            "unicode": "\u004c\u0065\u006b",
	            "symbol": "Lek"
	        },
	        "AFN": {
	            "unicode": "\u060b",
	            "symbol": "؋"
	        },
	        "AZN": {
	            "unicode": "\u043c\u0430\u043d",
	            "symbol": "ман"
	        },
	        "BYR": {
	            "unicode": "\u0070\u002e",
	            "symbol": "p."
	        },
	        "BZD": {
	            "unicode": "\u0042\u005a\u0024",
	            "symbol": "BZ$"
	        },
	        "BOB": {
	            "unicode": "\u0024\u0062",
	            "symbol": "$b"
	        },
	        "BAM": {
	            "unicode": "\u004b\u004d",
	            "symbol": "KM"
	        },
	        "BGN,KZT,KGS,UZS": {
	            "unicode": "\u043b\u0432",
	            "symbol": "лв"
	        },
	        "BRL": {
	            "unicode": "\u0052\u0024",
	            "symbol": "R$"
	        },
	        "KHR": {
	            "unicode": "\u17db",
	            "symbol": "៛"
	        },
	        "CRC": {
	            "unicode": "\u20a1",
	            "symbol": "₡"
	        },
	        "HRK": {
	            "unicode": "\u006b\u006e",
	            "symbol": "kn"
	        },
	        "CUP": {
	            "unicode": "\u20b1",
	            "symbol": "₱"
	        },
	        "CZK": {
	            "unicode": "\u004b\u010d",
	            "symbol": "Kč"
	        },
	        "DKK,EEK,ISK,NOK,SEK": {
	            "unicode": "\u006b\u0072",
	            "symbol": "kr"
	        },
	        "DOP": {
	            "unicode": "\u0052\u0044\u0024",
	            "symbol": "RD$"
	        },
	        "GTQ": {
	            "unicode": "\u0051",
	            "symbol": "Q"
	        },
	        "HNL": {
	            "unicode": "\u004c",
	            "symbol": "L"
	        },
	        "HKD": {
	            "unicode": "\u0048\u004b\u0024",
	            "symbol": "HK$"
	        },
	        "HUF": {
	            "unicode": "\u0046\u0074",
	            "symbol": "Ft"
	        },
	        "MUR,NPR,PKR,SCR,LKR": {
	            "unicode": "\u20a8",
	            "symbol": "₨"
	        },
	        "INR ": {
	            "unicode": "\u20b9",
	            "symbol": "₹"
	        },
	        "IDR": {
	            "unicode": "\u0052\u0070",
	            "symbol": "Rp"
	        },
	        "OMR,QAR,SAR,YER": {
	            "unicode": "\ufdfc",
	            "symbol": "﷼"
	        },
	        "ILS": {
	            "unicode": "\u20aa",
	            "symbol": "₪"
	        },
	        "JMD": {
	            "unicode": "\u004a\u0024",
	            "symbol": "J$"
	        },
	        "KPW,KRW,KPW,KRW": {
	            "unicode": "\u20a9",
	            "symbol": "₩"
	        },
	        "LAK": {
	            "unicode": "\u20ad",
	            "symbol": "₭"
	        },
	        "Ls": {
	            "unicode": "\u004c\u0073",
	            "symbol": "Ls"
	        },
	        "CHF": {
	            "unicode": "\u0043\u0048\u0046",
	            "symbol": "CHF"
	        },
	        "LTL": {
	            "unicode": "\u004c\u0074",
	            "symbol": "Lt"
	        },
	        "MKD": {
	            "unicode": "\u0434\u0435\43d",
	            "symbol": "ден"
	        },
	        "MYR": {
	            "unicode": "\u0052\u004d",
	            "symbol": "RM"
	        },
	        "MNT": {
	            "unicode": "\u20ae",
	            "symbol": "₮"
	        },
	        "NIO": {
	            "unicode": "\u0043\u0024",
	            "symbol": "C$"
	        },
	        "NGN": {
	            "unicode": "\u20a6",
	            "symbol": "₦"
	        },
	        "PAB": {
	            "unicode": "\u0042\u002f\u002e",
	            "symbol": "B/."
	        },
	        "PYG": {
	            "unicode": "\u0047\u0073",
	            "symbol": "Gs"
	        },
	        "PEN": {
	            "unicode": "\u0053\u002f\u002e",
	            "symbol": "S/."
	        },
	        "PHP": {
	            "unicode": "\u20b1",
	            "symbol": "₱"
	        },
	        "PLN": {
	            "unicode": "\u007a\u0142",
	            "symbol": "zł"
	        },
	        "RON": {
	            "unicode": "\u006c\u0065\u0069",
	            "symbol": "lei"
	        },
	        "RUB": {
	            "unicode": "\u0400\u0443\u0431",
	            "symbol": "руб"
	        },
	        "RSD": {
	            "unicode": "\u0420\u0421\u0414",
	            "symbol": "РСД"
	        },
	        "ZAR": {
	            "unicode": "\u0052",
	            "symbol": "R"
	        },
	        "TWD": {
	            "unicode": "\u004e\u0054\u0024",
	            "symbol": "NT$"
	        },
	        "THB": {
	            "unicode": "\u0e3f",
	            "symbol": "฿"
	        },
	        "TTD": {
	            "unicode": "\u0054\u0054\u0024",
	            "symbol": "TT$"
	        },
	        "TRY": {
	            "unicode": "\u20ba",
	            "symbol": "₺"
	        },
	        "TRL": {
	            "unicode": "\u20a4",
	            "symbol": "₤"
	        },
	        "UAH": {
	            "unicode": "\u20b4",
	            "symbol": "₴"
	        },
	        "UYU": {
	            "unicode": "\u0024\u0055",
	            "symbol": "$U"
	        },
	        "VEF": {
	            "unicode": "\u0042\u0073",
	            "symbol": "Bs"
	        },
	        "VND": {
	            "unicode": "\u20ab",
	            "symbol": "₫"
	        },
	        "ZWD": {
	            "unicode": "\u005a\u0024",
	            "symbol": "Z$"
	        },
	        "KWD": {
	            "unicode": "\u062f\u002e\u0643",
	            "symbol": "د.ك"
	        },
	        "AED": {
	            "unicode": "\u062f\u002e\u0625",
	            "symbol": "د.إ"
	        }
	    };
	    var getCurrency = function(currency, key) {
	        var currencyObject;
	        for (var curr in currencyMap) {
	            var currArray = curr.split(',');
	            if (currArray.indexOf(currency) > -1) {
	                currencyObject = currencyMap[curr];
	                break;
	            }
	        }
	        if (key && currencyObject) {
	            return currencyObject[key];
	        } else {
	            return currencyObject;
	        }
	    };
	
	    // clone a object
	    // include Date, Array, object
	    var clone = function(obj) {
	        // Handle the 3 simple types, and null or undefined
	        if (null == obj || "object" != typeof obj) return obj;
	
	        // Handle Date
	        if (obj instanceof Date) {
	            var copy = new Date();
	            copy.setTime(obj.getTime());
	            return copy;
	        }
	
	        // Handle Array
	        if (obj instanceof Array) {
	            var copy = [];
	            for (var i = 0, len = obj.length; i < len; ++i) {
	                copy[i] = clone(obj[i]);
	            }
	            return copy;
	        }
	
	        // Handle Object
	        if (obj instanceof Object) {
	            var copy = {};
	            for (var attr in obj) {
	                if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
	            }
	            return copy;
	        }
	    };
	
	    return {
	        standardizeLangSymbol: standardizeLangSymbol,
	        isWithMUIPrefix: isWithMUIPrefix,
	        isClassNameWithMuiPrefix: isClassNameWithMuiPrefix,
	        deferredExec: deferredExec,
	        getComponentName: getComponentName,
	        log: morningstar.asterix.util.log,
	        getCurrency: getCurrency,
	        clone: clone
	    };
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*! v2.9.0 */ // Create a UMD mode
	(function(global, factory) {
		var __isAMD = ("function" === 'function' && __webpack_require__(5)),
			__isNode = (typeof module === "object" && typeof module.exports === 'object'),
			__isWeb = !__isNode;
	
		if (__isNode) {
	
			// For CommonJS and CommonJS-like environments
			// var QSAPI = require('QSAPI')(window);
			module.exports = factory;
	
		} else if (__isAMD) {
	
			// Register as a named AMD module.
			!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
		}
	
	
	})(typeof window !== 'undefined' ? window : this, function(window, noGlobal) {
		var strundefined = typeof undefined;
		var QSAPI = noGlobal;
	/*! jQuery Migrate v1.2.1 | (c) 2005, 2013 jQuery Foundation, Inc. and other contributors | jquery.org/license */
	jQuery.migrateMute===void 0&&(jQuery.migrateMute=!0),function(e,t,n){function r(n){var r=t.console;i[n]||(i[n]=!0,e.migrateWarnings.push(n),r&&r.warn&&!e.migrateMute&&(r.warn("JQMIGRATE: "+n),e.migrateTrace&&r.trace&&r.trace()))}function a(t,a,i,o){if(Object.defineProperty)try{return Object.defineProperty(t,a,{configurable:!0,enumerable:!0,get:function(){return r(o),i},set:function(e){r(o),i=e}}),n}catch(s){}e._definePropertyBroken=!0,t[a]=i}var i={};e.migrateWarnings=[],!e.migrateMute&&t.console&&t.console.log&&t.console.log("JQMIGRATE: Logging is active"),e.migrateTrace===n&&(e.migrateTrace=!0),e.migrateReset=function(){i={},e.migrateWarnings.length=0},"BackCompat"===document.compatMode&&r("jQuery is not compatible with Quirks Mode");var o=e("<input/>",{size:1}).attr("size")&&e.attrFn,s=e.attr,u=e.attrHooks.value&&e.attrHooks.value.get||function(){return null},c=e.attrHooks.value&&e.attrHooks.value.set||function(){return n},l=/^(?:input|button)$/i,d=/^[238]$/,p=/^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,f=/^(?:checked|selected)$/i;a(e,"attrFn",o||{},"jQuery.attrFn is deprecated"),e.attr=function(t,a,i,u){var c=a.toLowerCase(),g=t&&t.nodeType;return u&&(4>s.length&&r("jQuery.fn.attr( props, pass ) is deprecated"),t&&!d.test(g)&&(o?a in o:e.isFunction(e.fn[a])))?e(t)[a](i):("type"===a&&i!==n&&l.test(t.nodeName)&&t.parentNode&&r("Can't change the 'type' of an input or button in IE 6/7/8"),!e.attrHooks[c]&&p.test(c)&&(e.attrHooks[c]={get:function(t,r){var a,i=e.prop(t,r);return i===!0||"boolean"!=typeof i&&(a=t.getAttributeNode(r))&&a.nodeValue!==!1?r.toLowerCase():n},set:function(t,n,r){var a;return n===!1?e.removeAttr(t,r):(a=e.propFix[r]||r,a in t&&(t[a]=!0),t.setAttribute(r,r.toLowerCase())),r}},f.test(c)&&r("jQuery.fn.attr('"+c+"') may use property instead of attribute")),s.call(e,t,a,i))},e.attrHooks.value={get:function(e,t){var n=(e.nodeName||"").toLowerCase();return"button"===n?u.apply(this,arguments):("input"!==n&&"option"!==n&&r("jQuery.fn.attr('value') no longer gets properties"),t in e?e.value:null)},set:function(e,t){var a=(e.nodeName||"").toLowerCase();return"button"===a?c.apply(this,arguments):("input"!==a&&"option"!==a&&r("jQuery.fn.attr('value', val) no longer sets properties"),e.value=t,n)}};var g,h,v=e.fn.init,m=e.parseJSON,y=/^([^<]*)(<[\w\W]+>)([^>]*)$/;e.fn.init=function(t,n,a){var i;return t&&"string"==typeof t&&!e.isPlainObject(n)&&(i=y.exec(e.trim(t)))&&i[0]&&("<"!==t.charAt(0)&&r("$(html) HTML strings must start with '<' character"),i[3]&&r("$(html) HTML text after last tag is ignored"),"#"===i[0].charAt(0)&&(r("HTML string cannot start with a '#' character"),e.error("JQMIGRATE: Invalid selector string (XSS)")),n&&n.context&&(n=n.context),e.parseHTML)?v.call(this,e.parseHTML(i[2],n,!0),n,a):v.apply(this,arguments)},e.fn.init.prototype=e.fn,e.parseJSON=function(e){return e||null===e?m.apply(this,arguments):(r("jQuery.parseJSON requires a valid JSON string"),null)},e.uaMatch=function(e){e=e.toLowerCase();var t=/(chrome)[ \/]([\w.]+)/.exec(e)||/(webkit)[ \/]([\w.]+)/.exec(e)||/(opera)(?:.*version|)[ \/]([\w.]+)/.exec(e)||/(msie) ([\w.]+)/.exec(e)||0>e.indexOf("compatible")&&/(mozilla)(?:.*? rv:([\w.]+)|)/.exec(e)||[];return{browser:t[1]||"",version:t[2]||"0"}},e.browser||(g=e.uaMatch(navigator.userAgent),h={},g.browser&&(h[g.browser]=!0,h.version=g.version),h.chrome?h.webkit=!0:h.webkit&&(h.safari=!0),e.browser=h),a(e,"browser",e.browser,"jQuery.browser is deprecated"),e.sub=function(){function t(e,n){return new t.fn.init(e,n)}e.extend(!0,t,this),t.superclass=this,t.fn=t.prototype=this(),t.fn.constructor=t,t.sub=this.sub,t.fn.init=function(r,a){return a&&a instanceof e&&!(a instanceof t)&&(a=t(a)),e.fn.init.call(this,r,a,n)},t.fn.init.prototype=t.fn;var n=t(document);return r("jQuery.sub() is deprecated"),t},e.ajaxSetup({converters:{"text json":e.parseJSON}});var b=e.fn.data;e.fn.data=function(t){var a,i,o=this[0];return!o||"events"!==t||1!==arguments.length||(a=e.data(o,t),i=e._data(o,t),a!==n&&a!==i||i===n)?b.apply(this,arguments):(r("Use of jQuery.fn.data('events') is deprecated"),i)};var j=/\/(java|ecma)script/i,w=e.fn.andSelf||e.fn.addBack;e.fn.andSelf=function(){return r("jQuery.fn.andSelf() replaced by jQuery.fn.addBack()"),w.apply(this,arguments)},e.clean||(e.clean=function(t,a,i,o){a=a||document,a=!a.nodeType&&a[0]||a,a=a.ownerDocument||a,r("jQuery.clean() is deprecated");var s,u,c,l,d=[];if(e.merge(d,e.buildFragment(t,a).childNodes),i)for(c=function(e){return!e.type||j.test(e.type)?o?o.push(e.parentNode?e.parentNode.removeChild(e):e):i.appendChild(e):n},s=0;null!=(u=d[s]);s++)e.nodeName(u,"script")&&c(u)||(i.appendChild(u),u.getElementsByTagName!==n&&(l=e.grep(e.merge([],u.getElementsByTagName("script")),c),d.splice.apply(d,[s+1,0].concat(l)),s+=l.length));return d});var Q=e.event.add,x=e.event.remove,k=e.event.trigger,N=e.fn.toggle,T=e.fn.live,M=e.fn.die,S="ajaxStart|ajaxStop|ajaxSend|ajaxComplete|ajaxError|ajaxSuccess",C=RegExp("\\b(?:"+S+")\\b"),H=/(?:^|\s)hover(\.\S+|)\b/,A=function(t){return"string"!=typeof t||e.event.special.hover?t:(H.test(t)&&r("'hover' pseudo-event is deprecated, use 'mouseenter mouseleave'"),t&&t.replace(H,"mouseenter$1 mouseleave$1"))};e.event.props&&"attrChange"!==e.event.props[0]&&e.event.props.unshift("attrChange","attrName","relatedNode","srcElement"),e.event.dispatch&&a(e.event,"handle",e.event.dispatch,"jQuery.event.handle is undocumented and deprecated"),e.event.add=function(e,t,n,a,i){e!==document&&C.test(t)&&r("AJAX events should be attached to document: "+t),Q.call(this,e,A(t||""),n,a,i)},e.event.remove=function(e,t,n,r,a){x.call(this,e,A(t)||"",n,r,a)},e.fn.error=function(){var e=Array.prototype.slice.call(arguments,0);return r("jQuery.fn.error() is deprecated"),e.splice(0,0,"error"),arguments.length?this.bind.apply(this,e):(this.triggerHandler.apply(this,e),this)},e.fn.toggle=function(t,n){if(!e.isFunction(t)||!e.isFunction(n))return N.apply(this,arguments);r("jQuery.fn.toggle(handler, handler...) is deprecated");var a=arguments,i=t.guid||e.guid++,o=0,s=function(n){var r=(e._data(this,"lastToggle"+t.guid)||0)%o;return e._data(this,"lastToggle"+t.guid,r+1),n.preventDefault(),a[r].apply(this,arguments)||!1};for(s.guid=i;a.length>o;)a[o++].guid=i;return this.click(s)},e.fn.live=function(t,n,a){return r("jQuery.fn.live() is deprecated"),T?T.apply(this,arguments):(e(this.context).on(t,this.selector,n,a),this)},e.fn.die=function(t,n){return r("jQuery.fn.die() is deprecated"),M?M.apply(this,arguments):(e(this.context).off(t,this.selector||"**",n),this)},e.event.trigger=function(e,t,n,a){return n||C.test(e)||r("Global events are undocumented and deprecated"),k.call(this,e,t,n||document,a)},e.each(S.split("|"),function(t,n){e.event.special[n]={setup:function(){var t=this;return t!==document&&(e.event.add(document,n+"."+e.guid,function(){e.event.trigger(n,null,t,!0)}),e._data(this,n,e.guid++)),!1},teardown:function(){return this!==document&&e.event.remove(document,n+"."+e._data(this,n)),!1}}})}(jQuery,window);
	
	(function($){$.toJSON=function(o)
	{if(typeof(JSON)=='object'&&JSON.stringify)
	return JSON.stringify(o);var type=typeof(o);if(o===null)
	return"null";if(type=="undefined")
	return undefined;if(type=="number"||type=="boolean")
	return o+"";if(type=="string")
	return $.quoteString(o);if(type=='object')
	{if(typeof o.toJSON=="function")
	return $.toJSON(o.toJSON());if(o.constructor===Date)
	{var month=o.getUTCMonth()+1;if(month<10)month='0'+month;var day=o.getUTCDate();if(day<10)day='0'+day;var year=o.getUTCFullYear();var hours=o.getUTCHours();if(hours<10)hours='0'+hours;var minutes=o.getUTCMinutes();if(minutes<10)minutes='0'+minutes;var seconds=o.getUTCSeconds();if(seconds<10)seconds='0'+seconds;var milli=o.getUTCMilliseconds();if(milli<100)milli='0'+milli;if(milli<10)milli='0'+milli;return'"'+year+'-'+month+'-'+day+'T'+
	hours+':'+minutes+':'+seconds+'.'+milli+'Z"';}
	if(o.constructor===Array)
	{var ret=[];for(var i=0;i<o.length;i++)
	ret.push($.toJSON(o[i])||"null");return"["+ret.join(",")+"]";}
	var pairs=[];for(var k in o){var name;var type=typeof k;if(type=="number")
	name='"'+k+'"';else if(type=="string")
	name=$.quoteString(k);else
	continue;if(typeof o[k]=="function")
	continue;var val=$.toJSON(o[k]);pairs.push(name+":"+val);}
	return"{"+pairs.join(", ")+"}";}};$.evalJSON=function(src)
	{if(typeof(JSON)=='object'&&JSON.parse)
	return JSON.parse(src);return eval("("+src+")");};$.secureEvalJSON=function(src)
	{if(typeof(JSON)=='object'&&JSON.parse)
	return JSON.parse(src);var filtered=src;filtered=filtered.replace(/\\["\\\/bfnrtu]/g,'@');filtered=filtered.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,']');filtered=filtered.replace(/(?:^|:|,)(?:\s*\[)+/g,'');if(/^[\],:{}\s]*$/.test(filtered))
	return eval("("+src+")");else
	throw new SyntaxError("Error parsing JSON, source is not valid.");};$.quoteString=function(string)
	{if(string.match(_escapeable))
	{return'"'+string.replace(_escapeable,function(a)
	{var c=_meta[a];if(typeof c==='string')return c;c=a.charCodeAt();return'\\u00'+Math.floor(c/16).toString(16)+(c%16).toString(16);})+'"';}
	return'"'+string+'"';};var _escapeable=/["\\\x00-\x1f\x7f-\x9f]/g;var _meta={'\b':'\\b','\t':'\\t','\n':'\\n','\f':'\\f','\r':'\\r','"':'\\"','\\':'\\\\'};})((typeof QSAPI != 'undefined' && typeof QSAPI.$ != 'undefined'?QSAPI.$:jQuery));
	/* Copyright (c) 2011 Brandon Aaron (http://brandonaaron.net)
	 * Licensed under the MIT License (LICENSE.txt).
	 *
	 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
	 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
	 * Thanks to: Seamus Leahy for adding deltaX and deltaY
	 *
	 * Version: 3.0.6
	 * 
	 * Requires: 1.2.2+
	 */
	(function(d){var b=["DOMMouseScroll","mousewheel"];if(d.event.fixHooks){for(var a=b.length;a;){d.event.fixHooks[b[--a]]=d.event.mouseHooks}}d.event.special.mousewheel={setup:function(){if(this.addEventListener){for(var e=b.length;e;){this.addEventListener(b[--e],c,false)}}else{this.onmousewheel=c}},teardown:function(){if(this.removeEventListener){for(var e=b.length;e;){this.removeEventListener(b[--e],c,false)}}else{this.onmousewheel=null}}};d.fn.extend({mousewheel:function(e){return e?this.bind("mousewheel",e):this.trigger("mousewheel")},unmousewheel:function(e){return this.unbind("mousewheel",e)}});function c(i){var g=i||window.event,k=[].slice.call(arguments,1),f=0,e=true,j=0,h=0;i=d.event.fix(g);i.type="mousewheel";if(g.wheelDelta){f=g.wheelDelta/120}if(g.detail){f=-g.detail/3}h=f;if(g.axis!==undefined&&g.axis===g.HORIZONTAL_AXIS){h=0;j=-1*f}if(g.wheelDeltaY!==undefined){h=g.wheelDeltaY/120}if(g.wheelDeltaX!==undefined){j=-1*g.wheelDeltaX/120}k.unshift(i,f,j,h);return(d.event.dispatch||d.event.handle).apply(this,k)}})((typeof QSAPI != 'undefined' && typeof QSAPI.$ != 'undefined'?QSAPI.$:jQuery));
	var MigratoryDataClient={};
	MigratoryDataClient.a=function(){return navigator.userAgent&&navigator.userAgent.indexOf("ANTGalio")!==-1?"Opera":navigator.userAgent&&navigator.userAgent.indexOf("Chrome")!==-1&&navigator.userAgent.indexOf("WebKit")!==-1?"WebKit Chrome":navigator.userAgent&&navigator.userAgent.indexOf("Android")!==-1?"WebKit Android":navigator.userAgent&&navigator.userAgent.indexOf("iPhone")!==-1?"WebKit iPhone":navigator.userAgent&&navigator.userAgent.indexOf("WebKit")!==-1?"WebKit":navigator.userAgent&&navigator.userAgent.indexOf("MSIE")!==
	-1?"IE":navigator.userAgent&&navigator.userAgent.indexOf("Gecko")!==-1?"Gecko":navigator.userAgent&&navigator.userAgent.indexOf("Opera Mobi")!==-1?"Opera Mobile":navigator.userAgent&&navigator.userAgent.indexOf("Opera Mini")!==-1?"unknown":window.opera?"Opera":"unknown"};MigratoryDataClient.b=function(a){if(!document.body)throw"Error: The document doesn't have a body!";var b=true;if(this.c==="unknown"){b=false;if(a)if(this.d===null)throw"Error: Browser not supported!";else this.d(this.NOTIFY_UNSUPPORTED_BROWSER)}return b};
	MigratoryDataClient.e=function(){if(document.readyState==="complete")this.f();else if(document.addEventListener){document.addEventListener("DOMContentLoaded",this.f,false);window.addEventListener("load",this.f,false)}else if(document.attachEvent){document.attachEvent("onreadystatechange",this.f);window.attachEvent("onload",this.f);var a=false;try{a=window.frameElement==null}catch(b){}document.documentElement.doScroll&&a&&this.g()}};
	MigratoryDataClient.g=function(){if(!MigratoryDataClient.h){try{document.documentElement.doScroll("left")}catch(a){setTimeout(MigratoryDataClient.g,1);return}MigratoryDataClient.f()}};MigratoryDataClient.i=function(a){this.h?a():this.j.push(a)};MigratoryDataClient.f=function(){if(!MigratoryDataClient.h)if(document.body){MigratoryDataClient.h=true;for(var a=0;a<MigratoryDataClient.j.length;a++)MigratoryDataClient.j[a]();MigratoryDataClient.j=null}else setTimeout(MigratoryDataClient.f,13)};
	MigratoryDataClient.k=function(a){var b=window.location.protocol,c=window.location.host,d=window.location.port;if(c.indexOf("localhost")===0)return{l:"localhost:80",m:a+"/"};if(d.length>0&&c.lastIndexOf(":")!==-1)c=c.substring(0,c.lastIndexOf(":"));var e=a.indexOf("//");if(e===-1)return null;var f=a.substring(0,e);if(b!==f)return null;a=a.substring(f.length+2);e=a.indexOf("/");if(e!==-1)a=a.substring(0,e);e=a.lastIndexOf(":");b="";if(e!==-1){b=a.substring(e+1);a=a.substring(0,e)}if(navigator.userAgent&&
	navigator.userAgent.indexOf("ANTGalio")!==-1){e=80;if(f==="https:")e=443;if(b!==""&&b!==e&&d!==e)if(b!==d)return null}else if(b!==d)return null;if(a.length<4)return null;var g=-1;e=a.length-1;for(var i=c.length-1;e>=0&&i>=0;e--,i--)if(a.charAt(e)!==c.charAt(i)){g=e;break}d="";if(g===-1)if(e===-1&&i===-1){e=a.indexOf(".");d=a.substring(e+1)}else if(e===-1)if(c.charAt(i-1)===".")d=a;else{e=a.indexOf(".");if(e===-1)return null;d=a.substring(e+1)}else{if(i===-1)if(a.charAt(e-1)===".")d=c;else{e=c.indexOf(".");
	if(e===-1)return null;d=c.substring(e+1)}}else{e=a.indexOf(".",g+1);if(e===-1)return null;d=a.substring(e+1)}if(d.length<4||d.indexOf(".")===-1)return null;d+=b.length>0?":"+b:"";a=f+"//"+a+(b.length>0?":"+b:"")+"/";if(this.n>=2){c=d.split(".");if(c.length>=this.n)d=c.slice(-1*this.n).join(".")}return{l:d,m:a}};MigratoryDataClient.o=function(a){return this.k(a)===null};
	MigratoryDataClient.p=function(a,b){if(a.name)a=a.name;else{var c=a.toString();a=c.substring(c.indexOf("function")+8,c.indexOf("("));a=a.replace(/^\s+|\s+$/g,"");if(a.length===0)a="anonymous";if(a==="anonymous"&&typeof b==="object")for(var d=0;d<b.length;d++)for(var e in b[d])if(b[d].hasOwnProperty(e)&&typeof b[d][e]==="function")if(b[d][e].toString()===c)return e}return a};
	MigratoryDataClient.q=function(a){if(a===null||a===undefined||typeof a==="number"||typeof a==="boolean")return a;else if(typeof a==="string")return'"'+a+'"';else if(typeof a==="function")return this.p(a,this)+"()";else if(a instanceof Array)return this.r(a);var b="{",c=0;for(var d in a){if(c>0)b+=", ";if(a.hasOwnProperty(d))b+=d+":"+this.q(a[d]);c++}b+="}";return b};
	MigratoryDataClient.s=function(){return"xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g,function(a){var b=Math.random()*16|0;return(a=="x"?b:b&3|8).toString(16)})};
	MigratoryDataClient.t=function(a,b){var c=MigratoryDataClient.u;if(a>0){if(a<=this.v)c=a*MigratoryDataClient.w-Math.floor(Math.random()*MigratoryDataClient.w);else if(MigratoryDataClient.x===MigratoryDataClient.TRUNCATED_EXPONENTIAL_BACKOFF){a=a-this.v;c=Math.min(MigratoryDataClient.y*Math.pow(2,a)-Math.floor(Math.random()*MigratoryDataClient.y*a),MigratoryDataClient.z)}else c=MigratoryDataClient.y;if(b!==undefined&&c<MigratoryDataClient.u)c=MigratoryDataClient.u}return c};
	MigratoryDataClient.a0=function(a,b){for(var c=0;c<a.length;c++)if(a[c]===b)return c;return-1};MigratoryDataClient.a1=function(a){for(var b=[],c=0;c<a.length;c++)b.push(a[c]);return b};MigratoryDataClient.a2=function(a,b){for(var c=[],d=0;d<a.length;d++)this.a0(b,a[d])===-1&&c.push(a[d]);return c};MigratoryDataClient.a3=function(a,b){for(var c=[],d=0;d<a.length;d++)this.a0(b,a[d])!==-1&&c.push(a[d]);return c};
	MigratoryDataClient.a4=function(a,b){for(var c=this.a1(a),d=0;d<b.length;d++)this.a0(a,b[d])===-1&&c.push(b[d]);return c};
	MigratoryDataClient.a5=function(a,b,c,d,e){var f=this.p(arguments.callee.caller,this);if(typeof a!=="object"||typeof a.length!=="number")throw"Error: "+f+". The argument should be a list!";if(c!==null&&a.length<c)throw"Error: "+f+". The list argument should have at minimum "+c+" elements!";if(b!==null)for(var g=0;g<a.length;g++)if(typeof a[g]!==b)throw"Error: "+f+". The list argument should contain only '"+b+"' elements, the "+g+"-th element is not of type '"+b+"'!";if(typeof d==="object"&&typeof d.test===
	"function")for(g=0;g<a.length;g++)if(!d.test(a[g]))throw"Error: "+f+". "+e+". The "+g+"-th element is the cause of the error!";};MigratoryDataClient.r=function(a){for(var b="[",c=0;c<a.length;c++){if(c>0)b+=", ";b+=this.q(a[c])}b+="]";return b};
	MigratoryDataClient.a6=function(a,b){if(a===this.a7.a8)this.a9(b);else if(a===this.a7.aa)this.ab(b);else if(a===this.a7.ac)this.ae(b);else if(this.af!==null)if(this.af.ag===this.ah&&a===this.a7.ah)this.ai(b);else if(this.af.ag===this.aj&&a===this.a7.aj)this.ak(b);else this.af.ag===this.al&&a===this.a7.al&&this.ae(b)};
	MigratoryDataClient.ai=function(a){var b=a[this.a7.am];if(b!==undefined)this.an(this.ao,this.ap.aq[b]);else{if(this.ar!==this.as){b=a[this.a7.at];if(b===undefined){MigratoryDataClient.an(this.ao,"server subscribe response is missing the session id");return}this.au=b;b=a[this.a7.av];if(b!==undefined&&b!==null)if(b==1)MigratoryDataClient.aw=true;b=a[this.a7.ax];if(b!==undefined&&b!==null)MigratoryDataClient.ay=b*1E3*1.4;this.az=a[this.a7.b0];a=this.ar;this.ar=this.as;this.b2=this.b1=0;this.b3();if(a!==
	this.b4){this.b5.b6[this.b7]!==undefined&&this.b5.b6[this.b7].b8++;if(a===null||this.b9)this.ba({type:this.NOTIFY_SERVER_UP,info:""})}else this.b5.b6[this.b7]!==undefined&&this.b5.b6[this.b7].bb++;this.b9=false}this.bc()}};MigratoryDataClient.ak=function(a){a=a[this.a7.am];a!==undefined?this.an(this.ao,this.ap.aq[a]):this.bc()};
	MigratoryDataClient.a9=function(a){for(var b=[],c=0,d=0;d<a.length;d++){var e=a[d],f=this.a7.bd,g=this.a7.be,i=this.a7.bf,k=this.a7.bg,l=this.a7.bh,j=this.a7.bi;if(e[f]===undefined||e[g]===undefined)return;var m=false,h=false;if(e[l]!==undefined)if(e[l]==this.bj)m=true;else if(e[l]==this.bk)h=true;l=[];i=e[i];k=e[k];if(i!==undefined&&k!=undefined)if(i instanceof Array)for(var n=0;n<i.length;n++)l[n]={name:i[n],value:k[n]};else l[0]={name:i,value:k};f={subject:e[f],content:e[g],fields:l,replyToSubject:e[j],
	isSnapshot:m,isRecovery:h,seq:0,epoch:0};g=this.a7.bl;if(e[g]!==undefined){g=((new Date).getTime()&16777215)-e[g];if(g>-14400000)f.latency=g}if(MigratoryDataClient.aw==true&&this.bm[f.subject]===undefined){g=parseInt(e[this.a7.bn]);e=parseInt(e[this.a7.bo]);f.seq=e;f.epoch=g;j=this.bp[f.subject];if(j===undefined){this.bp[f.subject]={seqid:7E4,seq:0,recovery:false};j=this.bp[f.subject]}else j.seq++;if(j.seqid!==g){j.seqid=g;j.seq=e;j.recovery=false}else if(j.seq!==e)if(j.recovery==false){j.seq--;if(e<=
	j.seq)continue;MigratoryDataClient.bq();return}else{j.recovery=false;if(e>j.seq){g={type:this.NOTIFY_DATA_RESYNC,info:f.subject};this.ba(g)}else continue;j.seq=e}else if(j.recovery==true){j.recovery=false;g={type:this.NOTIFY_DATA_SYNC,info:f.subject};this.ba(g)}}b[c]=f;c++}if(c>0){this.b5.b6[this.b7]!==undefined&&this.b5.b6[this.b7].br++;this.ba(b)}};MigratoryDataClient.an=function(a,b){a=a+", "+b;b=this.b5.b6[this.b7].aq;if(b[a]===undefined)b[a]=1;else b[a]++;this.b2++;this.bq()};
	MigratoryDataClient.ab=function(a){var b=this.a7.bs,c=this.a7.bd;if(!(a[b]===undefined||a[c]===undefined)){var d=true,e=this.NOTIFY_SUBSCRIBE_DENY;switch(a[b]){case "a":e=this.NOTIFY_SUBSCRIBE_ALLOW;d=false;break;case "d":b=a[this.a7.am];if(b!==undefined){if(b==MigratoryDataClient.bt)e=this.NOTIFY_SUBSCRIBE_TIMEOUT}else e=this.NOTIFY_SUBSCRIBE_DENY;break}if(d===true){this.bu=this.a2(this.bu,[a[c]]);delete this.bm[a[c]];delete this.bp[a[c]]}this.ba({type:e,info:a[c]})}};
	MigratoryDataClient.ba=function(a){this.bv.push(a);setTimeout(function(){var b=MigratoryDataClient.bv.shift();if(b&&b instanceof Array)MigratoryDataClient.bw.call(window,b);else MigratoryDataClient.d!==null&&MigratoryDataClient.d.call(window,b)},0)};MigratoryDataClient.ae=function(a){this.ad(a);this.bc()};MigratoryDataClient.ad=function(a){var b=a[this.a7.bx];a=a[this.a7.bs];b!==undefined&&b!==null&&this.ba({type:MigratoryDataClient.by(a),info:b})};
	MigratoryDataClient.by=function(a){if(a===undefined||a===null)return MigratoryDataClient.NOTIFY_PUBLISH_FAILED;switch(a){case "OK":return MigratoryDataClient.NOTIFY_PUBLISH_OK;case "DENY":return MigratoryDataClient.NOTIFY_PUBLISH_DENIED;default:return MigratoryDataClient.NOTIFY_PUBLISH_FAILED}};
	MigratoryDataClient.bz=function(){this.b5.c0=(new Date).getTime();this.b5.b6[this.b7]!==undefined&&this.b5.b6[this.b7].c1++;this.c2!==null&&this.b3();this.af!==null&&this.af.ag===this.c3&&this.bc();var a=(new Date).getTime();if(a-this.c4>=this.c5){this.c4=a;a={};a.ag=this.c3;this.c6(a)}};MigratoryDataClient.b3=function(){this.c2!==null&&clearTimeout(this.c2);if(this.ay>0)this.c2=setTimeout(function(){MigratoryDataClient.b2++;MigratoryDataClient.bq()},this.ay)};
	MigratoryDataClient.bq=function(){this.b5.b6[this.b7]!==undefined&&this.b5.b6[this.b7].c7++;if((new Date).getTime()-this.c8<18E4)this.c9++;else{this.c9=0;this.c8=(new Date).getTime()}if(this.c9>=MigratoryDataClient.ca){var a={type:this.NOTIFY_RECONNECT_RATE_EXCEEDED,info:""};MigratoryDataClient.d!==null&&MigratoryDataClient.d.call(window,a);a=MigratoryDataClient.getSubjects();for(var b=[],c=0;c<MigratoryDataClient.cb.length;c++){var d=MigratoryDataClient.cb[c].cc+" "+MigratoryDataClient.cb[c].m;b[c]=
	d}MigratoryDataClient.disconnect();MigratoryDataClient.setServers(b);MigratoryDataClient.subscribe(a)}else{if(this.cd!==null){clearTimeout(this.cd);this.cd=null}this.ce();this.cf();this.cg();if(this.ar!==null)this.ar=this.ch;this.ci.push(this.cb[this.b7]);this.az=this.au=this.b7=null;this.b1++;if(this.c2!==null){clearTimeout(this.c2);this.c2=null}if(!this.b9&&(this.b1===this.cj||this.b1===this.cb.length)){this.b9=true;a={type:this.NOTIFY_SERVER_DOWN,info:""};MigratoryDataClient.d!==null&&MigratoryDataClient.d.call(window,
	a)}a=false;if(MigratoryDataClient.aw==true){a=true;for(d in this.bp){b=this.bp[d];if(b.seqid!=7E4)if(b.recovery==true)a=false;else b.recovery=true}}if(MigratoryDataClient.ck)if(this.cb.length>0){d={};d.ag=this.cl;this.c6(d)}MigratoryDataClient.cm=false;if(this.bu.length>0){d={};d.ag=this.ah;if(a==true)d.cn=true;d.bu=this.bu;this.c6(d)}}};
	MigratoryDataClient.co=function(){this.cp();var a=this.cb[this.b7].m;if(MigratoryDataClient.ck){encoding=this.cq;transport=this.cr}if("/"!==a.substring(a.length-1,a.length))a+="/";var b=MigratoryDataClient.t(this.b2,this.cl);this.cs=setTimeout(function(){MigratoryDataClient.cs=null;MigratoryDataClient.an(MigratoryDataClient.ct,MigratoryDataClient.af.ag)},b);transport.call(this,a,this.b7,null)};
	MigratoryDataClient.cu=function(){this.cp();var a=false,b=this.cb[this.b7].m,c=this.o(b),d=null;if(!MigratoryDataClient.cm&&MigratoryDataClient.cv!=="")d=MigratoryDataClient.cv;if(!MigratoryDataClient.ck){if(!this.cw||!c)b=this.cx(b);if(!c&&!this.cy(b,this.b7))return}this.bu=this.a4(this.bu,this.af.bu);var e=c&&!this.cz?this.d0:null,f=null,g=null,i=this.d1,k=this.a7,l=null,j=null;if(MigratoryDataClient.ck){i=this.d2;e=this.cq}else if(this.c==="IE"&&c&&MigratoryDataClient.d3){i=this.d4;k=this.d5;e=
	this.d6}if(this.ar!==this.as){if(!MigratoryDataClient.ck)if(this.c==="IE"&&!c)if(this.d7){e=this.d8;i=this.d9}else{e=this.da;f="MigratoryDataClient0.db";g=this.dc;i=this.dd;k=this.d5}else if(this.c==="IE"&&MigratoryDataClient.d3&&this.d7){a=true;e=this.d8;i=this.de;k=this.d5}else{i=this.df;if(this.c==="WebKit Android")i=this.dg;e=c&&!this.cz?this.d0:i;i=this.dh}l=navigator.userAgent;j=this.di}var m=null;if(this.af.dj!==undefined&&this.af.dj!==null)m=this.af.dj;c="";for(var h=0;h<this.af.bu.length;h++){if(h>
	0)g=f=null;if(!MigratoryDataClient.cm)MigratoryDataClient.cm=true;c+=this.dk(this.af.bu[h],e,f,g,this.au,k,this.af.cn,d,a,l,j,m);j=l=null}if("/"!==b.substring(b.length-1,b.length))b+="/";a=MigratoryDataClient.u;if(MigratoryDataClient.ck===false)a=MigratoryDataClient.t(this.b2,this.ah);this.cs=setTimeout(function(){MigratoryDataClient.cs=null;MigratoryDataClient.an(MigratoryDataClient.ct,MigratoryDataClient.af.ag)},a);i.call(this,b,this.b7,c)};
	MigratoryDataClient.dl=function(){this.cp();var a=this.cb[this.b7].m,b=this.o(a);if(!MigratoryDataClient.ck){if(!this.cw||!b)a=this.cx(a);if(!b&&!this.cy(a,this.b7))return}this.bu=this.a2(this.bu,this.af.bu);if(this.ar!==this.as){this.bc();for(b=0;b<this.af.bu.length;b++)delete this.bm[this.af.bu[b]]}else{var c=b&&!this.cz?this.d0:null,d=this.d1,e=this.a7;if(MigratoryDataClient.ck){c=this.cq;d=this.d2}else if(this.c==="IE"&&b&&MigratoryDataClient.d3&&this.d7){d=this.d4;e=this.d5;c=this.d6;if("/"!==
	a.substring(a.length-1,a.length))a+="/"}var f="";for(b=0;b<this.af.bu.length;b++){f+=this.dm(this.af.bu[b],c,this.au,e);delete this.bm[this.af.bu[b]]}this.cs=setTimeout(function(){MigratoryDataClient.cs=null;MigratoryDataClient.an(MigratoryDataClient.ct,MigratoryDataClient.af.ag)},this.u);d.call(this,a,this.b7,f)}};
	MigratoryDataClient.dn=function(){this.cp();var a=this.cb[this.b7].m,b=this.o(a);if(!MigratoryDataClient.ck){if(!this.cw||!b)a=this.cx(a);if(!b&&!this.cy(a,this.b7))return}if(this.ar!==this.as)this.bc();else{var c=b&&!this.cz?this.d0:null,d=this.d1,e=this.a7,f=this.az;if(MigratoryDataClient.ck){c=this.cq;d=this.d2;f=null}else if(this.c==="IE"&&b&&MigratoryDataClient.d3&&this.d7){d=this.d4;e=this.d5;c=this.d6;if("/"!==a.substring(a.length-1,a.length))a+="/"}b=MigratoryDataClient.dp(this.au,e,c,f);
	this.cs=setTimeout(function(){MigratoryDataClient.cs=null;MigratoryDataClient.an(MigratoryDataClient.ct,MigratoryDataClient.af.ag)},this.u);d.call(this,a,this.b7,b)}};
	MigratoryDataClient.dq=function(){this.cp();var a=this.cb[this.b7].m,b=this.o(a);if(!MigratoryDataClient.ck){if(!this.cw||!b)a=this.cx(a);if(!b&&!this.cy(a,this.b7))return}var c=b&&!this.cz?this.d0:null,d=this.d1,e=this.a7;if(this.ar!==this.as){a=this.af.dr.closure;a!==undefined&&a!==null&&this.ba({type:MigratoryDataClient.NOTIFY_PUBLISH_FAILED,info:a});this.bc()}else{var f=this.cv,g=this.az;if(MigratoryDataClient.ck){d=this.d2;c=this.cq;g=f=null}else if(this.c==="IE"&&b&&MigratoryDataClient.d3){d=
	this.d4;e=this.d5;c=this.d6}if(this.ar===this.as){b=this.ds(this.af.dr,c,this.au,e,f,g);if("/"!==a.substring(a.length-1,a.length))a+="/";this.cs=setTimeout(function(){MigratoryDataClient.cs=null;MigratoryDataClient.an(MigratoryDataClient.ct,MigratoryDataClient.af.ag)},this.u);d.call(this,a,this.b7,b)}}};MigratoryDataClient.bc=function(a){if(this.dt.length!==0)if(a!==undefined&&a!==null){if(this.dt[0].ag===a){this.dt.shift();this.cf();this.du(false)}}else{this.dt.shift();this.cf();this.du(false)}};
	MigratoryDataClient.c6=function(a){this.dt.push(a);this.dv(false)};MigratoryDataClient.du=function(a){this.dt.length!==0&&setTimeout(function(){MigratoryDataClient.dv(a)},0)};MigratoryDataClient.dv=function(a){if(this.dw)if(this.b(true))if(!(!a&&(this.af!==null||this.dt.length===0))){this.af=this.dt[0];switch(this.af.ag){case this.ah:this.cu();break;case this.aj:this.dl();break;case this.c3:this.dn();break;case this.cl:this.co();break;case this.al:this.dq();break}}};
	MigratoryDataClient.dx=function(){this.dw=true;this.du(false)};
	MigratoryDataClient.ce=function(){for(;this.dt.length>0;){var a=this.dt[0];switch(a.ag){case this.ah:var b=a.dy;a=this.a2(a.bu,this.bu);if(a.length>0){this.bu=this.a4(this.bu,a);for(var c=0;c<a.length;c++){this.bp[a[c]]={seqid:7E4,seq:0,recovery:false};if(b!==undefined&&b!==null&&b>=100){b=Math.floor(b/100)*100;this.bm[a[c]]=b}}}break;case this.aj:a=this.a3(a.bu,this.bu);if(a.length>0){this.bu=this.a2(this.bu,a);for(c=0;c<a.length;c++){delete this.bm[a[c]];delete this.bp[a[c]]}}break;case this.al:if(a.dr.closure!==
	undefined&&a.dr.closure!==null){b={type:this.NOTIFY_PUBLISH_FAILED,info:a.dr.closure};MigratoryDataClient.d!==null&&MigratoryDataClient.d.call(window,b)}break}this.dt.shift()}};MigratoryDataClient.cf=function(){this.af=null;if(this.cs!==null){clearTimeout(this.cs);this.cs=null}if(this.dz!==null&&this.dz.readyState&&this.dz.readyState!==4){if(typeof XMLHttpRequest!=="undefined")this.dz.aborted=true;this.dz.abort()}this.dz!==null&&delete this.dz;this.dz=null};
	MigratoryDataClient.cg=function(){if(this.e0!==null){clearTimeout(this.e0);this.e0=null}if(this.e1!==null)if(this.e1.e2!=="HTML5")if(this.e1.e2==="XDR_HTML5"){var a=document.getElementById("MigratoryDataClient1");a!==null&&a.contentWindow.postMessage("disconnect","*")}else if(this.e1.getElementById){a=this.e1.getElementById("e3");if(a!==null){a.src="";this.e1.body.removeChild(a);delete a;a=null}delete this.e1;this.e1=null;CollectGarbage()}else{if(MigratoryDataClient.ck){this.e1.onmessage=function(){};
	this.e1.onopen=function(){};this.e1.onclose=function(){};this.e1.close()}else{if(this.e1.e4!==undefined){clearTimeout(this.e1.e4);this.e1.e4=undefined}this.e1.readyState&&this.e1.readyState!==4&&this.e1.abort();this.e1.e2==="XDR_STREAM"&&this.e1.abort()}delete this.e1;this.e1=null}};
	MigratoryDataClient.e5=function(){this.cf();this.cg();this.b7=null;this.cb=[];this.b5.b6=[];this.ar=null;this.bu=[];this.bm={};this.az=this.au=null;this.ci=[];this.c9=this.b1=0;this.c8=(new Date).getTime();this.b2=0;this.e6=this.b9=false;this.dt=[];this.bm={};this.bp={};this.aw=this.cm=false;if(this.c2!==null){clearTimeout(this.c2);this.c2=null}};
	MigratoryDataClient.e7=function(){if(this.o(this.cb[this.b7].m)&&!this.cz){if(this.e0!==null){clearTimeout(this.e0);this.e0=null}if(this.e1!==null){this.e1.responseText="";this.e1.e8=0}}else{this.ar=this.b4;this.cg();this.az=this.au=null;MigratoryDataClient.cm=false;if(this.bu.length>0){var a={};a.ag=this.ah;if(MigratoryDataClient.aw==true)a.cn=true;a.bu=this.bu;this.c6(a)}}};MigratoryDataClient.cp=function(){if(this.b7===null)this.b7=this.e9()};
	MigratoryDataClient.e9=function(){var a=this.a2(this.cb,this.ci);if(a.length===0){this.ci=[];a=this.cb}if(a.length===0)throw"Error: e9() No available servers!";for(var b=0,c=0;c<a.length;c++)b+=a[c].cc;var d=-1;if(b===0)d=Math.floor(a.length*Math.random());else{var e=Math.floor(b*Math.random());for(c=b=0;c<a.length;c++){b+=a[c].cc;if(b>e){d=c;break}}}return this.a0(this.cb,a[d])};
	MigratoryDataClient.cx=function(a){var b=this.k(a);if(b===null||this.dc!==null&&b.l!==this.dc)throw"Error: Invalid common parent domain of the servers! Cause server is '"+a+"'.";if(this.dc===null){this.dc=b.l;if(b.l.indexOf(":")===-1)document.domain=b.l}return b.m};MigratoryDataClient.cy=function(a,b){var c="MigratoryDataClient2"+b;if(window.frames[c]===undefined||window.frames[c].ea===undefined){this.eb(a,b);return false}return true};
	MigratoryDataClient.eb=function(a,b){b="MigratoryDataClient2"+b;var c=document.getElementById(b);if(!c){c=document.createElement("iframe");c.name=b;c.id=b;c.style.display="none";document.body.appendChild(c)}b=MigratoryDataClient.t(this.b2,this.ah);this.cs=setTimeout(function(){MigratoryDataClient.cs=null;c.src="";c.parentNode.removeChild(c);MigratoryDataClient.an(MigratoryDataClient.ct,"iframe")},b);if("/"!==a.substring(a.length-1,a.length))a+="/";c.src=a+"_"+this.ec(this.dc,"ea","MigratoryDataClient.ed")};
	MigratoryDataClient.ed=function(){clearTimeout(this.cs);this.cs=null;this.du(true)};MigratoryDataClient.ee=function(a){return this.cz?new XMLHttpRequest:this.ef(a)};
	MigratoryDataClient.ef=function(a){if(a){if(this.eg)this.eg.responseText="";else this.eg={open:function(b,c){b=MigratoryDataClient.eh(c);MigratoryDataClient.ei.connect("0",b.host,b.port,"MigratoryDataClient.ej")},setRequestHeader:function(){},send:function(b){MigratoryDataClient.ei.write("0","POST / HTTP/1.1\r\nContent-Length: "+b.length+"\r\n\r\n"+b)},readyState:4,status:200,responseText:"",abort:function(){this.responseText="";MigratoryDataClient.ei.close("0")}};return this.eg}if(this.ek)this.ek.responseText=
	"";else this.ek={open:function(b,c){b=MigratoryDataClient.eh(c);MigratoryDataClient.ei.connect("1",b.host,80,"MigratoryDataClient.el")},setRequestHeader:function(){},send:function(b){MigratoryDataClient.ei.write("1","POST / HTTP/1.1\r\nContent-Length: "+b.length+"\r\n\r\n"+b)},readyState:4,status:200,responseText:"",abort:function(){this.responseText="";MigratoryDataClient.ei.close("1")}};return this.ek};
	MigratoryDataClient.eh=function(a){var b,c;b=a.indexOf("https://")==0?"https://":"http://";a=a.substring(b.length);var d=a.indexOf("/");if(d!=-1){c=a.substring(0,d);a.substring(d)}else c=a;d=c.lastIndexOf(":");if(d!=-1){a=c.substring(d+1);c=c.substring(0,d)}else a=b=="https://"?"443":"80";return{host:c,port:a}};MigratoryDataClient.el=function(a){if(this.dz){this.dz.responseText+=a;this.em()}};MigratoryDataClient.ej=function(a){if(this.e1){this.e1.responseText+=a;this.en()}};
	MigratoryDataClient.eo=function(){this.b(false);var a=document.createElement("div");document.body.appendChild(a);var b=document.createElement("div");b.id="MigratoryDataClient3";a.appendChild(b);setTimeout(function(){var c="flash-transport.swf";if(typeof MigratoryDataClientFlashTransport==="string")c=MigratoryDataClientFlashTransport;swfobject.embedSWF(c,"MigratoryDataClient3","0","0","9",false,{readyCallback:"MigratoryDataClient.ep"},{allowFullScreen:false,allowScriptAccess:"always"},{id:"MigratoryDataClient4",
	name:"MigratoryDataClient4"})},0)};MigratoryDataClient.ep=function(){MigratoryDataClient.ei=document.getElementById("MigratoryDataClient4");if(!MigratoryDataClient.ei)throw"Error: Could not get the reference of the flash-transport.swf!";MigratoryDataClient.dx()};
	MigratoryDataClient.d4=function(a,b,c){b=document.getElementById("MigratoryDataClient5");if(b===null){b=document.createElement("iframe");b.id="MigratoryDataClient5";b.style.display="none";document.body.appendChild(b)}var d=(new Date).getTime();b.src=a+"_"+c+d};
	MigratoryDataClient.dd=function(a,b,c){this.e1=new ActiveXObject("htmlfile");this.e1.open();MigratoryDataClient.dc.indexOf(":")===-1?this.e1.write("<html><head><script>document.domain='"+MigratoryDataClient.dc+"';<\/script></head><body></body></html>"):this.e1.write("<html><head></head><body></body></html>");this.e1.close();this.e1.parentWindow.MigratoryDataClient0=this;b=this.e1.createElement("iframe");b.id="e3";this.e1.body.appendChild(b);this.e1.eq=0;b.src=a+"_"+c};
	MigratoryDataClient.db=function(a){MigratoryDataClient.er(a);MigratoryDataClient.e1.eq+=a.length;MigratoryDataClient.e1.eq>=MigratoryDataClient.es&&MigratoryDataClient.af===null&&MigratoryDataClient.ar!==MigratoryDataClient.b4&&MigratoryDataClient.e7()};
	MigratoryDataClient.de=function(a,b,c){this.e1={};this.e1.e2="XDR_HTML5";MigratoryDataClient.eq=0;b=document.getElementById("MigratoryDataClient1");if(b===null){b=document.createElement("iframe");b.id="MigratoryDataClient1";b.style.display="none";document.body.appendChild(b)}var d=(new Date).getTime();b.src=a+"_"+c+d};
	MigratoryDataClient.et=function(a){if(a.data.indexOf(MigratoryDataClient.a7.eu)!==-1){MigratoryDataClient.er(a.data);MigratoryDataClient.eq+=a.data.length;MigratoryDataClient.eq>=MigratoryDataClient.es&&MigratoryDataClient.af===null&&MigratoryDataClient.ar!==MigratoryDataClient.b4&&MigratoryDataClient.e7()}};
	MigratoryDataClient.d9=function(a,b,c){window.frames["MigratoryDataClient2"+b].ea("window.parent.MigratoryDataClient.e1 = new XDomainRequest();");this.e1.e8=0;this.e1.e2="XDR_STREAM";this.e1.onload=function(){MigratoryDataClient.ev()};this.e1.onprogress=function(){MigratoryDataClient.ev()};this.e1.onerror=function(){};this.e1.ontimeout=function(){};try{this.e1.open("POST",a);this.e1.send(c)}catch(d){}};
	MigratoryDataClient.ev=function(){var a=this.e1;if(a.responseText){for(var b=a.responseText.substring(a.e8),c=b.indexOf(this.a7.ew);c!==-1;){b=b.substring(0,c);this.er(b);a.e8+=c+1;b=a.responseText.substring(a.e8);c=b.indexOf(this.a7.ew)}a.e8>=this.es&&this.af===null&&this.ar!==this.b4&&this.e7()}};
	MigratoryDataClient.dh=function(a,b,c){var d=this.o(a);if(d)this.e1=this.ee(true);else window.frames["MigratoryDataClient2"+b].ea("window.parent.MigratoryDataClient.e1 = new XMLHttpRequest();");this.e1.e8=0;this.e1.onreadystatechange=function(){MigratoryDataClient.en()};this.e1.open("POST",a,true);d&&this.c.indexOf("WebKit")===0&&this.e1.setRequestHeader("Content-Type","text/plain");this.e1.send(c)};
	MigratoryDataClient.en=function(){var a=this.e1;if(!(a===null||a.readyState!==3&&a.readyState!==4||a.status!==200)){if(this.c.indexOf("Opera")!==-1){this.e1.e4!==undefined&&clearTimeout(this.e1.e4);this.e1.e4=setTimeout(function(){MigratoryDataClient.e1.e4=undefined;MigratoryDataClient.en()},this.ex)}if(a.responseText){for(var b=a.responseText.substring(a.e8),c=b.indexOf(this.a7.ew);c!==-1;){b=b.substring(0,c);this.er(b);a.e8+=c+1;b=a.responseText.substring(a.e8);c=b.indexOf(this.a7.ew)}a.e8>=this.es&&
	this.af===null&&this.ar!==this.b4&&this.e7()}}};MigratoryDataClient.ey=function(){if(typeof XMLHttpRequest==="undefined"){try{return new ActiveXObject("Msxml2.XMLHTTP.6.0")}catch(a){}try{return new ActiveXObject("Msxml2.XMLHTTP.3.0")}catch(b){}try{return new ActiveXObject("Msxml2.XMLHTTP")}catch(c){}throw"Error: The browser does not support XMLHttpRequest!";}else return new XMLHttpRequest};
	MigratoryDataClient.d1=function(a,b,c){var d=this.o(a);if(d)this.dz=this.ee(false);else window.frames["MigratoryDataClient2"+b].ea("window.parent.MigratoryDataClient.dz = ("+this.ey.toString()+")();");this.dz.onreadystatechange=function(){MigratoryDataClient.em()};this.dz.open("POST",a,true);d&&this.c.indexOf("WebKit")===0&&this.dz.setRequestHeader("Content-Type","text/plain");if(this.c.indexOf("IE")===0){this.dz.setRequestHeader("Content-Type","text/plain");this.dz.setRequestHeader("Connection",
	"close")}this.dz.send(c)};MigratoryDataClient.em=function(){var a=this.dz;a===null||typeof XMLHttpRequest!=="undefined"&&typeof a.aborted!=="undefined"&&a.aborted!==null&&a.aborted==true||a===null||a.readyState!==4||a.status!==200||a.responseText&&this.er(a.responseText)};
	MigratoryDataClient.cr=function(a){a=a.substring(0,a.indexOf("://"))==="http"?"ws://"+a.substring(a.indexOf("://")+3)+"WebSocketConnection":"wss://"+a.substring(a.indexOf("://")+3)+"WebSocketConnection-Secure";this.e1=MigratoryDataClient.ez(a);this.f0=MigratoryDataClient.s();this.e1.f1=this.f0;this.e1.onmessage=function(b){MigratoryDataClient.f2(b.data)};this.e1.onopen=function(){MigratoryDataClient.bc()};this.e1.onclose=function(){if(MigratoryDataClient.f0===this.f1){MigratoryDataClient.cf();MigratoryDataClient.b2++;
	MigratoryDataClient.bc(MigratoryDataClient.cl);var b=MigratoryDataClient.t(MigratoryDataClient.b2);MigratoryDataClient.cd=setTimeout(function(){MigratoryDataClient.bq()},b)}}};MigratoryDataClient.ez=function(a){if(window.WebSocket)return new WebSocket(a);else if(window.MozWebSocket)return new MozWebSocket(a);return null};MigratoryDataClient.d2=function(a,b,c){this.e1!=null&&this.e1.readyState===1&&this.e1.send(c)};
	MigratoryDataClient.f2=function(a){var b=MigratoryDataClient.e1;if(!(b===null||b.readyState!==1))if(a){b=a;for(var c=b.indexOf(MigratoryDataClient.a7.ew);c!==-1;){b=b.substring(0,c);MigratoryDataClient.er(b);b=a.substring(c+1);c=b.indexOf(MigratoryDataClient.a7.ew)}}};
	MigratoryDataClient.er=function(a){for(var b=0,c=[],d="\u0000",e={},f=0;;){var g=a.indexOf(this.a7.eu,b);if(g===-1)break;if(g-b>0){b=a.substring(b,g);d=b.charAt(0);e=this.f3(b);if(d===this.a7.a8){c[f]=e;f++}else this.a6(d,e)}b=g+1}f>0&&d!=="\u0000"&&this.a6(d,c);this.bz()};
	MigratoryDataClient.f3=function(a){if(this.a7.f4[a.charAt(0)]!==undefined){for(var b=1,c={};;){if(b>=a.length)break;var d=a.charAt(b),e=a.indexOf(this.a7.f5,b+1);if(e===-1)return c;if(this.a7.f6[d]!==undefined){b++;var f="";switch(this.a7.f7[d]){case this.ap.f8:f=this.f9(this.a7,a.substring(b,e));break;case this.ap.fa:f=MigratoryDataClient.fb(this.a7,a.substring(b,e));break}b=c[d];if(b===undefined)c[d]=f;else if(c[d]instanceof Array)c[d][c[d].length]=f;else{c[d]=[];c[d][0]=b;c[d][1]=f}}b=e+1}return c}};
	MigratoryDataClient.ec=function(a,b,c){var d="";d+=this.d5.fc;d+=this.d5.fd;d+=this.fe(this.d5,a);d+=this.d5.f5;d+=this.d5.ff;d+=this.fe(this.d5,b);d+=this.d5.f5;d+=this.d5.fg;d+=this.fe(this.d5,c);d+=this.d5.f5;d+=this.d5.eu;return d};
	MigratoryDataClient.dk=function(a,b,c,d,e,f,g,i,k,l,j,m){var h="";h+=k?f.fh:f.ah;h+=f.bd;h+=this.fe(f,a);h+=f.f5;if(b!==null){h+=f.fi;h+=this.fj(f,b);h+=f.f5;if(this.c.indexOf("Opera")!==-1){h+=f.fk;h+=this.fj(f,1);h+=f.f5}}if(this.bm[a]!==undefined){h+=f.fl;h+=this.fj(f,this.bm[a]);h+=f.f5}if(c!==null){h+=f.fg;h+=this.fe(f,c);h+=f.f5}if(i!==null){h+=f.fm;h+=this.fe(f,i);h+=f.f5}if(d!==null){h+=f.fd;h+=this.fe(f,d);h+=f.f5}if(e!==null){h+=f.at;h+=this.fj(f,e);h+=f.f5}if(g!==undefined)if(this.bp[a]!==
	undefined&&this.bp[a].seqid!==7E4){h+=f.bn;h+=this.fj(f,this.bp[a].seqid);h+=f.f5;h+=f.bo;h+=this.fj(f,this.bp[a].seq+1);h+=f.f5}if(l!==null){h+=f.fn;h+=this.fe(f,l);h+=f.f5}if(j!==null){h+=f.fo;h+=this.fj(f,j);h+=f.f5}if(m!==null){h+=f.fp;h+=this.fj(f,m);h+=f.f5}h+=f.eu;return h};
	MigratoryDataClient.dm=function(a,b,c,d){var e="";e+=d.aj;e+=d.bd;e+=this.fe(d,a);e+=d.f5;if(this.bm[a]!==undefined){e+=d.fl;e+=this.fj(d,this.bm[a]);e+=d.f5}if(c!==null){e+=d.at;e+=this.fj(d,c);e+=d.f5}if(b!==null){e+=d.fi;e+=this.fj(d,b);e+=d.f5}e+=d.eu;return e};MigratoryDataClient.dp=function(a,b,c,d){var e="";e+=b.c3;e+=b.at;e+=this.fj(b,a);e+=b.f5;if(c!==null){e+=b.fi;e+=this.fj(b,c);e+=b.f5}if(d!==null){e+=b.b0;e+=this.fj(b,d);e+=b.f5}e+=b.eu;return e};
	MigratoryDataClient.ds=function(a,b,c,d,e,f){var g="";g+=d.al;g+=d.bd;g+=this.fe(d,a.subject);g+=d.f5;g+=d.be;g+=this.fe(d,a.content);g+=d.f5;if(a.closure!==undefined&&a.closure!==null){g+=d.bx;g+=this.fe(d,a.closure);g+=d.f5}if(a.fields!==undefined&&a.fields!==null)for(var i=0;i<a.fields.length;i++){var k=a.fields[i].name,l=a.fields[i].value;g+=d.bf;g+=this.fe(d,k);g+=d.f5;g+=d.bg;g+=this.fe(d,l);g+=d.f5}if(a.replyToSubject!==undefined&&a.replyToSubject!==null){g+=d.bi;g+=this.fe(d,a.replyToSubject);
	g+=d.f5}if(b!==null){g+=d.fi;g+=this.fj(d,b);g+=d.f5;if(this.c.indexOf("Opera")!==-1){g+=d.fk;g+=this.fj(d,1);g+=d.f5}}if(c!==null){g+=d.at;g+=this.fj(d,c);g+=d.f5}if(f!==null){g+=d.b0;g+=this.fj(d,f);g+=d.f5}if(e!==null){g+=d.fm;g+=this.fe(d,e);g+=d.f5}g+=d.eu;return g};MigratoryDataClient.d5={};MigratoryDataClient.d5.eu="!";MigratoryDataClient.d5.ew="z";MigratoryDataClient.d5.f5="$";MigratoryDataClient.d5.fq="~";MigratoryDataClient.d5.fr=" ";MigratoryDataClient.d5.fs='"';
	MigratoryDataClient.d5.ft="#";MigratoryDataClient.d5.fu="%";MigratoryDataClient.d5.fv="'";MigratoryDataClient.d5.fw="/";MigratoryDataClient.d5.fx="<";MigratoryDataClient.d5.fy=">";MigratoryDataClient.d5.fz="[";MigratoryDataClient.d5.g0="\\";MigratoryDataClient.d5.g1="]";MigratoryDataClient.d5.g2="^";MigratoryDataClient.d5.g3="`";MigratoryDataClient.d5.g4="{";MigratoryDataClient.d5.g5="|";MigratoryDataClient.d5.g6="}";MigratoryDataClient.d5.g7="";MigratoryDataClient.d5.ah="&";
	MigratoryDataClient.d5.aj="(";MigratoryDataClient.d5.a8=")";MigratoryDataClient.d5.c3="*";MigratoryDataClient.d5.fc="+";MigratoryDataClient.d5.g8=",";MigratoryDataClient.d5.aa="0";MigratoryDataClient.d5.fh="2";MigratoryDataClient.d5.al="5";MigratoryDataClient.d5.ac="8";MigratoryDataClient.d5.bd="&";MigratoryDataClient.d5.be="(";MigratoryDataClient.d5.bo=")";MigratoryDataClient.d5.bn="*";MigratoryDataClient.d5.fi="+";MigratoryDataClient.d5.at=",";MigratoryDataClient.d5.fd="-";
	MigratoryDataClient.d5.fg=".";MigratoryDataClient.d5.ff="?";MigratoryDataClient.d5.am="0";MigratoryDataClient.d5.g9="1";MigratoryDataClient.d5.bl="2";MigratoryDataClient.d5.fk="3";MigratoryDataClient.d5.bx="4";MigratoryDataClient.d5.av="5";MigratoryDataClient.d5.fm="7";MigratoryDataClient.d5.bs="8";MigratoryDataClient.d5.b0="9";MigratoryDataClient.d5.bf="D";MigratoryDataClient.d5.bg="E";MigratoryDataClient.d5.fl="G";MigratoryDataClient.d5.bh="J";MigratoryDataClient.d5.fn="K";
	MigratoryDataClient.d5.fo="L";MigratoryDataClient.d5.ax="M";MigratoryDataClient.d5.fp="N";MigratoryDataClient.d5.bi="R";MigratoryDataClient.d5.ga={};
	(function(a){var b=a.ga;b["\u0000"]="y";for(var c=1;c<8;c++)b[String.fromCharCode(c)]=String.fromCharCode(c+39);b["\u0008"]="x";for(c=9;c<21;c++)b[String.fromCharCode(c)]=String.fromCharCode(c+39);b["\u0015"]="=";for(c=22;c<32;c++)b[String.fromCharCode(c)]=String.fromCharCode(c+41);b[a.eu]="I";b[a.ew]="w";b[a.f5]="J";b[a.fq]="K";b[a.fr]="L";b[a.fs]="M";b[a.ft]="N";b[a.fu]="O";b[a.fv]="P";b[a.fw]="_";b[a.fx]="Q";b[a.fy]="R";b[a.fz]="S";b[a.g0]="T";b[a.g1]="U";b[a.g2]="V";b[a.g3]="W";b[a.g4]="X";b[a.g5]=
	"Y";b[a.g6]="Z";b[a.g7]="v"})(MigratoryDataClient.d5);MigratoryDataClient.d5.gb={};(function(a){for(var b in a.ga)if(a.ga.hasOwnProperty(b))a.gb[a.ga[b]]=b})(MigratoryDataClient.d5);MigratoryDataClient.a7={};MigratoryDataClient.a7.eu="";MigratoryDataClient.a7.ew="\u0019";MigratoryDataClient.a7.f5="\u001e";MigratoryDataClient.a7.fq="\u001f";MigratoryDataClient.a7.gc="\u0000";MigratoryDataClient.a7.gd="\n";MigratoryDataClient.a7.ge="\r";MigratoryDataClient.a7.fs='"';MigratoryDataClient.a7.g0="\\";
	MigratoryDataClient.a7.ah="\u0001";MigratoryDataClient.a7.aj="\u0002";MigratoryDataClient.a7.a8="\u0003";MigratoryDataClient.a7.c3="\u0004";MigratoryDataClient.a7.fc="\u0005";MigratoryDataClient.a7.g8="\u0006";MigratoryDataClient.a7.aa="\t";MigratoryDataClient.a7.fh="\u000c";MigratoryDataClient.a7.al="\u0010";MigratoryDataClient.a7.ac="\u0013";MigratoryDataClient.a7.bd="\u0001";MigratoryDataClient.a7.be="\u0002";MigratoryDataClient.a7.bo="\u0003";MigratoryDataClient.a7.bn="\u0004";
	MigratoryDataClient.a7.fi="\u0005";MigratoryDataClient.a7.at="\u0006";MigratoryDataClient.a7.fd="\u0007";MigratoryDataClient.a7.fg="\u0008";MigratoryDataClient.a7.ff="\t";MigratoryDataClient.a7.am="\u000b";MigratoryDataClient.a7.g9="\u000c";MigratoryDataClient.a7.bl="\u000e";MigratoryDataClient.a7.fk="\u000f";MigratoryDataClient.a7.bx="\u0010";MigratoryDataClient.a7.av="\u0011";MigratoryDataClient.a7.fm="\u0013";MigratoryDataClient.a7.bs="\u0014";MigratoryDataClient.a7.b0="\u0015";
	MigratoryDataClient.a7.bf="\u001b";MigratoryDataClient.a7.bg="\u001c";MigratoryDataClient.a7.fl="\u001d";MigratoryDataClient.a7.bh="'";MigratoryDataClient.a7.fn="#";MigratoryDataClient.a7.fo="$";MigratoryDataClient.a7.ax="%";MigratoryDataClient.a7.fp="(";MigratoryDataClient.a7.bi=",";MigratoryDataClient.a7.ga={};
	(function(a){var b=a.ga;b[a.eu]="\u0001";b[a.f5]="\u0002";b[a.fq]="\u0003";b[a.gc]="\u0004";b[a.gd]="\u0005";b[a.ge]="\u0006";b[a.fs]="\u0007";b[a.g0]="\u0008";b[MigratoryDataClient.d5.eu]="\t";b[a.ew]="\u000b"})(MigratoryDataClient.a7);MigratoryDataClient.a7.gb={};(function(a){for(var b in a.ga)if(a.ga.hasOwnProperty(b))a.gb[a.ga[b]]=b})(MigratoryDataClient.a7);MigratoryDataClient.a7.f4={};
	(function(a){a.f4[a.ah]=true;a.f4[a.aj]=true;a.f4[a.a8]=true;a.f4[a.c3]=true;a.f4[a.fc]=true;a.f4[a.g8]=true;a.f4[a.aa]=true;a.f4[a.fh]=true;a.f4[a.al]=true;a.f4[a.ac]=true})(MigratoryDataClient.a7);MigratoryDataClient.a7.f6={};
	(function(a){a.f6[a.bd]=true;a.f6[a.be]=true;a.f6[a.bo]=true;a.f6[a.bn]=true;a.f6[a.fi]=true;a.f6[a.at]=true;a.f6[a.fd]=true;a.f6[a.fg]=true;a.f6[a.ff]=true;a.f6[a.am]=true;a.f6[a.g9]=true;a.f6[a.bl]=true;a.f6[a.fk]=true;a.f6[a.av]=true;a.f6[a.fm]=true;a.f6[a.bs]=true;a.f6[a.b0]=true;a.f6[a.bf]=true;a.f6[a.bg]=true;a.f6[a.fl]=true;a.f6[a.bh]=true;a.f6[a.fn]=true;a.f6[a.fo]=true;a.f6[a.bx]=true;a.f6[a.fp]=true;a.f6[a.ax]=true;a.f6[a.bi]=true})(MigratoryDataClient.a7);MigratoryDataClient.ap={};
	MigratoryDataClient.ap.gf=1;MigratoryDataClient.ap.gg=2;MigratoryDataClient.ap.fa=3;MigratoryDataClient.ap.f8=4;MigratoryDataClient.ap.gh={};(function(a){var b=a.gh;b.bd=a.fa;b.be=a.fa;b.gi=a.fa;b.bo=a.f8;b.bn=a.f8;b.fi=a.f8;b.at=a.f8;b.fd=a.fa;b.fg=a.fa;b.ff=a.fa;b.am=a.f8;b.g9=a.fa;b.bl=a.f8;b.fk=a.f8;b.fm=a.fa;b.bs=a.fa;b.b0=a.f8;b.bf=a.fa;b.bg=a.fa;b.bh=a.fa;b.fn=a.fa;b.fo=a.f8;b.bx=a.fa;b.av=a.f8;b.ax=a.f8;b.bi=a.fa})(MigratoryDataClient.ap);MigratoryDataClient.a7.f7={};
	MigratoryDataClient.d5.f7={};(function(){for(var a in MigratoryDataClient.ap.gh)if(MigratoryDataClient.ap.gh.hasOwnProperty(a)){MigratoryDataClient.a7.f7[MigratoryDataClient.a7[a]]=MigratoryDataClient.ap.gh[a];MigratoryDataClient.d5.f7[MigratoryDataClient.d5[a]]=MigratoryDataClient.ap.gh[a]}})();MigratoryDataClient.ap.aq={};MigratoryDataClient.ap.aq[0]="UNKNOWN_SESSION_ID";
	MigratoryDataClient.fe=function(a,b){for(var c="",d=0;d<b.length;d++){var e=a.ga[b.charAt(d)];if(e!==undefined){c+=a.fq;c+=e}else c+=b.charAt(d)}return c};MigratoryDataClient.fb=function(a,b){for(var c="",d=0;d<b.length;d++){var e=b.charAt(d);if(e===a.fq){if(d+1>=b.length||a.gb[b.charAt(d+1)]===undefined)throw"Error: fb() Illegal argument '"+b+"'!";e=a.gb[b.charAt(d+1)];d++}c+=e}return c};
	MigratoryDataClient.fj=function(a,b){if((b&4294967168)===0){var c=String.fromCharCode(b),d=a.ga[c];return d===undefined?c:a.fq+d}var e;e=(b&4278190080)!==0?24:(b&16711680)!==0?16:8;c=[];for(d=0;d<10;d++)c.push(0);for(var f=0,g=0;e>=0;){var i=b>>e&255;g++;c[f]|=i>>g;d=a.ga[String.fromCharCode(c[f])];if(d!==undefined){c[f]=a.fq.charCodeAt(0);c[f+1]=d.charCodeAt(0);f++}f++;c[f]|=i<<7-g&127;e-=8}d=a.ga[String.fromCharCode(c[f])];if(d!==undefined){c[f]=a.fq.charCodeAt(0);c[f+1]=d.charCodeAt(0);f++}f++;
	a="";for(d=0;d<f;d++)a+=String.fromCharCode(c[d]);return a};
	MigratoryDataClient.f9=function(a,b){var c="Error: f9() Illegal argument '"+b+"'!",d=0,e=-1,f=0,g,i=b.length,k=0;if(i===1)return b.charCodeAt(0);else if(i===2&&b.charAt(0)===a.fq){g=a.gb[b.charAt(1)];if(g!==undefined)return g.charCodeAt(0);else throw c;}for(;i>0;i--){g=b.charAt(k);k++;if(g===a.fq){if(i-1<0)throw c;i--;g=b.charAt(k);k++;g=a.gb[g];if(g===undefined)throw c;}else g=g;if(e>0){f|=g.charCodeAt(0)>>e;d=d<<8|f;f=g.charCodeAt(0)<<8-e}else f=g.charCodeAt(0)<<-e;e=(e+7)%8}return d};
	MigratoryDataClient.NOTIFY_UNSUPPORTED_BROWSER="NOTIFY_UNSUPPORTED_BROWSER";MigratoryDataClient.NOTIFY_SERVER_DOWN="NOTIFY_SERVER_DOWN";MigratoryDataClient.NOTIFY_SERVER_UP="NOTIFY_SERVER_UP";MigratoryDataClient.NOTIFY_DATA_RESYNC="NOTIFY_DATA_RESYNC";MigratoryDataClient.NOTIFY_DATA_SYNC="NOTIFY_DATA_SYNC";MigratoryDataClient.NOTIFY_PUBLISH_OK="NOTIFY_PUBLISH_OK";MigratoryDataClient.NOTIFY_PUBLISH_FAILED="NOTIFY_PUBLISH_FAILED";MigratoryDataClient.NOTIFY_PUBLISH_DENIED="NOTIFY_PUBLISH_DENIED";
	MigratoryDataClient.NOTIFY_PUBLISH_NO_SUBSCRIBER="NOTIFY_PUBLISH_NO_SUBSCRIBER";MigratoryDataClient.NOTIFY_SUBSCRIBE_ALLOW="NOTIFY_SUBSCRIBE_ALLOW";MigratoryDataClient.NOTIFY_SUBSCRIBE_DENY="NOTIFY_SUBSCRIBE_DENY";MigratoryDataClient.NOTIFY_SUBSCRIBE_TIMEOUT="NOTIFY_SUBSCRIBE_TIMEOUT";MigratoryDataClient.NOTIFY_RECONNECT_RATE_EXCEEDED="NOTIFY_RECONNECT_RATE_EXCEEDED";MigratoryDataClient.CONSTANT_WINDOW_BACKOFF="CONSTANT_WINDOW_BACKOFF";MigratoryDataClient.TRUNCATED_EXPONENTIAL_BACKOFF="TRUNCATED_EXPONENTIAL_BACKOFF";
	MigratoryDataClient.notifyAfterReconnectRetries=function(a){if(typeof a!=="number"||a<1)throw"Error: notifyAfterReconnectRetries() should have a positive number as an argument!";this.cj=a};
	MigratoryDataClient.setServers=function(a,b){b=typeof b!=="undefined"?b:false;this.a5(a,"string",1,/^(\d+)?\s*https?:\/\/(\w|-)+(\.(\w|-)+)*(:\d+)?$/i,"Error: setServers() - the argument should be a list of URLs, and each URL could be optionally preceded by a weight");for(var c=[],d=0;d<a.length;d++){var e=/https?:\/\/(\w|-)+(\.(\w|-)+)*(:\d+)?$/i.exec(a[d])[0],f=/^\d+/.exec(a[d]);if(f===null)f=100;else{f=parseInt(f[0]);if(f>100)throw"Error: setServers() - the weight needs to be an integer between 0 and 100!";
	}this.cw||this.cx(e);c.push({m:e,cc:f})}this.cb=c;this.b5.b6=[];for(d=0;d<a.length;d++){this.b5.b6[d]={};this.b5.b6[d].b8=0;this.b5.b6[d].c7=0;this.b5.b6[d].bb=0;this.b5.b6[d].c1=0;this.b5.b6[d].br=0;this.b5.b6[d].aq={}}if(MigratoryDataClient.ck){a={};a.ag=this.cl;this.c6(a)}if(b===true){b=MigratoryDataClient.getSubjects();if(b.length>0)MigratoryDataClient.subscribe(b);else{a={};a.ag=this.ah;a.dy=0;a.bu=[""];this.c6(a)}}};MigratoryDataClient.getSubjects=function(){return this.a1(this.bu)};
	MigratoryDataClient.setMessageHandler=function(a){if(typeof a!=="function")throw"Error: setMessageHandler() should have a function as an argument!";this.bw=a};MigratoryDataClient.setStatusHandler=function(a){if(typeof a!=="function")throw"Error: setStatusHandler() should have a function as an argument!";this.d=a};MigratoryDataClient.subscribe=function(a){MigratoryDataClient.subscribeWithHistory(a,0)};
	MigratoryDataClient.subscribeWithHistory=function(a,b){this.a5(a,"string",1,/^\/[^\/*]+\/([^\/*]+\/)*([^\/*]+|\*)$/,"Error: subscribe() - a subject is invalid");if(this.cb.length===0)throw"Error: subscribe() - the servers are not configured!";if(this.bw===null)throw"Error: subscribe() - the message handler is not configured!";if(typeof b!=="number"||b<0)throw"Error: subscribeWithHistory() - the second argument should be a positive number or zero!";a=this.a2(a,this.bu);if(a.length!==0){if(this.e6===
	false)this.e6=true;var c={};c.ag=this.ah;c.dy=0;if(b!=0)c.dj=b;c.bu=a;this.c6(c)}};
	MigratoryDataClient.setNumberOfSubdomainLevels=function(a){if(typeof a!=="number"||a<2)throw"Error: setNumberOfSubdomainLevels() should have a positive number larger or equal to 2 as an argument!";if(this.cb.length>0)throw"Error: Error: setNumberOfSubdomainLevels() Unable to set the number of subdomain levels when servers are already configured - use the api call setNumberOfSubdomainLevels() before the api call setServers()!";this.n=a};
	MigratoryDataClient.subscribeWithConflation=function(a,b){this.a5(a,"string",1,/^\/[^\/*]+\/([^\/*]+\/)*([^\/*]+|\*)$/,"Error: subscribe() - a subject is invalid");if(this.cb.length===0)throw"Error: subscribeWithConflation() - the servers are not configured!";if(this.bw===null)throw"Error: subscribeWithConflation() - the message handler is not configured!";a=this.a2(a,this.bu);if(a.length!==0){if(b>=100){b=Math.floor(b/100)*100;for(var c=0;c<a.length;c++)this.bm[a[c]]=b}else b=0;if(this.e6===false)this.e6=
	true;c={};c.ag=this.ah;c.dy=b;c.bu=a;this.c6(c)}};MigratoryDataClient.unsubscribe=function(a){this.a5(a,"string",1,/^\/[^\/*]+\/([^\/*]+\/)*([^\/*]+|\*)$/,"Error: unsubscribe() - a subject is invalid");a=this.a3(a,this.bu);if(a.length!==0){var b={};b.ag=this.aj;b.bu=a;this.c6(b)}};MigratoryDataClient.disconnect=function(){this.b(false)&&this.e5()};
	MigratoryDataClient.setEntitlementToken=function(a){if(this.bu.length===0){this.cv=a;this.cm=false}else throw"Error: setEntitlementToken() - unable to set the entitlement token when there are running subject subscriptions!";};
	MigratoryDataClient.getInfo=function(){s="Date: "+(new Date).toString()+"\n";s+="Uptime: "+((new Date).getTime()-this.b5.gk)+" ms\n";s+="window.location: "+window.location+"\n";s+="document.domain: "+document.domain+"\n";s+="User-agent: "+navigator.userAgent+"\n";s+="Detected browser: "+this.c+"\n";s+="Servers: ";for(var a=0;a<this.cb.length;a++){if(a>0)s+=", ";s+=this.cb[a].cc+" "+this.cb[a].m}s+="\nSubjects: "+this.bu.toString()+"\n";s+="Connection status ["+(this.b7===null?null:this.cb[this.b7].m)+
	"]: "+this.ar+"\n";s+="Time from last server activity: "+(this.b5.c0!==null?(new Date).getTime()-this.b5.c0:null)+" ms\n";s+="Servers down before notify: "+this.cj+"\n";s+="Consecutive server down count: "+this.b1+" times\n";for(a=0;a<this.b5.b6.length;a++){s+="\nServer up ["+this.cb[a].m+"]: "+this.b5.b6[a].b8+" times\n";s+="Server down ["+this.cb[a].m+"]: "+this.b5.b6[a].c7+" times\n";s+="Server connection recycled ["+this.cb[a].m+"]: "+this.b5.b6[a].bb+" times\n";s+="Received server events ["+
	this.cb[a].m+"]: "+this.b5.b6[a].c1+"\n";s+="Received messages ["+this.cb[a].m+"]: "+this.b5.b6[a].br+"\n";for(var b in this.b5.b6[a].aq)if(this.b5.b6[a].aq.hasOwnProperty(b))s+="Error ["+this.cb[a].m+"] x"+this.b5.b6[a].aq[b]+" times : "+b+"\n"}return s};
	MigratoryDataClient.publish=function(a){if(this.cb.length===0)throw"Error: publish() - the servers are not configured, use setServers() first!";if(a===undefined||a===null)throw"Error: publish() - the message argument is undefined or null!";if(a.subject===undefined||a.subject===null)throw"Error: publish() - the subject of the message is undefined or null!";if(!/^\/[^\/*]+\/([^\/*]+\/)*([^\/*]+|\*)$/.test(a.subject))throw"Error: publish() - the subject of te message is invalid,  "+a.subject+" is the cause of the error!";
	if(a.content===undefined||a.content===null)throw"Error: publish() - the content of the message is undefined or null!";if(a.fields!==undefined&&a.fields!==null){if(typeof a.fields!=="object"||typeof a.fields.length!=="number")throw"Error: publish() - the message.fields element should be a list!";for(var b=0;b<a.fields.length;b++){if(!("name"in a.fields[b]))throw"Error: publish() - the "+b+"-th element from fields list doesn't have the name key!";if(!("value"in a.fields[b]))throw"Error: publish() - the "+
	b+"-th element from fields list doesn't have the value key!";}}if(this.e6===false){this.e6=true;this.bp[""]={seqid:7E4,seq:0,recovery:false};b={};b.ag=this.ah;b.bu=[""];this.c6(b)}a.replyToSubject!==undefined&&a.replyToSubject!==null&&MigratoryDataClient.subscribe([a.replyToSubject]);b={};b.ag=this.al;b.dr=a;this.c6(b)};MigratoryDataClient.setQuickReconnectMaxRetries=function(a){if(typeof a!=="number"||a<2)throw"Error: setQuickReconnectMaxRetries() - the argument must be higher than 2!";this.v=a};
	MigratoryDataClient.setQuickReconnectInitialDelay=function(a){if(typeof a!=="number"||a<1)throw"Error: setQuickReconnectInitialDelay() - the argument must be higher than 1!";this.w=a*1E3};
	MigratoryDataClient.setReconnectPolicy=function(a){if(a===undefined||a!==MigratoryDataClient.CONSTANT_WINDOW_BACKOFF&&a!==MigratoryDataClient.TRUNCATED_EXPONENTIAL_BACKOFF)throw"Error: setReconnectPolicy() - the argument must be either MigratoryDataClient.CONSTANT_WINDOW_BACKOFF or MigratoryDataClient.TRUNCATED_EXPONENTIAL_BACKOFF!";this.x=a};
	MigratoryDataClient.setReconnectTimeInterval=function(a){if(typeof a!=="number"||a<1)throw"Error: setReconnectTimeInterval() - the argument must be higher than 1!";this.y=a*1E3};MigratoryDataClient.setReconnectMaxDelay=function(a){if(typeof a!=="number"||a<1)throw"Error: setReconnectMaxDelay() - the argument must be higher than 1!";this.z=a*1E3};
	MigratoryDataClient.notifyWhenReconnectRateExceedsThreshold=function(a){if(typeof a!=="number"||a<2)throw"Error: notifyWhenReconnectRateExceedsThreshold() - the argument must be higher than 2!";this.ca=a};MigratoryDataClient.cw=false;MigratoryDataClient.cz=false;MigratoryDataClient.d3=false;MigratoryDataClient.c5=9E5;MigratoryDataClient.gl=3E4+Math.floor(Math.random()*1E4);MigratoryDataClient.ay=MigratoryDataClient.gl;MigratoryDataClient.u=1E4;MigratoryDataClient.ex=100;MigratoryDataClient.es=524288;
	MigratoryDataClient.cj=1;MigratoryDataClient.ca=10;MigratoryDataClient.n=0;MigratoryDataClient.aw=false;MigratoryDataClient.v=3;MigratoryDataClient.w=5E3;MigratoryDataClient.x=MigratoryDataClient.TRUNCATED_EXPONENTIAL_BACKOFF;MigratoryDataClient.y=2E4;MigratoryDataClient.z=36E4;MigratoryDataClient.cm=false;MigratoryDataClient.cv="";MigratoryDataClient.ah="SUBSCRIBE";MigratoryDataClient.aj="UNSUBSCRIBE";MigratoryDataClient.c3="PING";MigratoryDataClient.cl="CONNECT";MigratoryDataClient.al="PUBLISH";
	MigratoryDataClient.gm=0;MigratoryDataClient.da=1;MigratoryDataClient.df=2;MigratoryDataClient.d0=3;MigratoryDataClient.gn=4;MigratoryDataClient.dg=5;MigratoryDataClient.go=6;MigratoryDataClient.gp=7;MigratoryDataClient.cq=8;MigratoryDataClient.d8=9;MigratoryDataClient.d6=10;MigratoryDataClient.as="SERVER_UP";MigratoryDataClient.ch="SERVER_DOWN";MigratoryDataClient.b4="SERVER_RECYCLE";MigratoryDataClient.bj="1";MigratoryDataClient.gq="2";MigratoryDataClient.bk="3";MigratoryDataClient.di=1;
	MigratoryDataClient.ct="ERROR_TIMEOUT";MigratoryDataClient.gr="ERROR_HTTP";MigratoryDataClient.ao="ERROR_SERVER";MigratoryDataClient.gs=0;MigratoryDataClient.gt=1;MigratoryDataClient.bt=2;MigratoryDataClient.c=MigratoryDataClient.a();MigratoryDataClient.dc=null;MigratoryDataClient.cb=[];MigratoryDataClient.bw=null;MigratoryDataClient.d=null;MigratoryDataClient.bv=[];MigratoryDataClient.c9=0;MigratoryDataClient.c8=(new Date).getTime();MigratoryDataClient.dw=false;MigratoryDataClient.j=[];
	MigratoryDataClient.h=false;MigratoryDataClient.e6=false;MigratoryDataClient.b7=null;MigratoryDataClient.ar=null;MigratoryDataClient.bu=[];MigratoryDataClient.bm={};MigratoryDataClient.bp={};MigratoryDataClient.au=null;MigratoryDataClient.az=null;MigratoryDataClient.e1=null;MigratoryDataClient.c4=(new Date).getTime();MigratoryDataClient.c2=null;MigratoryDataClient.ci=[];MigratoryDataClient.b1=0;MigratoryDataClient.b2=0;MigratoryDataClient.b9=false;MigratoryDataClient.f0=null;
	MigratoryDataClient.dt=[];MigratoryDataClient.af=null;MigratoryDataClient.dz=null;MigratoryDataClient.cs=null;MigratoryDataClient.cd=null;MigratoryDataClient.b5={};MigratoryDataClient.b5.gk=(new Date).getTime();MigratoryDataClient.b5.c0=null;MigratoryDataClient.b5.b6=[];MigratoryDataClient.ck=false;MigratoryDataClient.d7=false;if(window.WebSocket)MigratoryDataClient.ck=true;else if(window.MozWebSocket)MigratoryDataClient.ck=true;
	if(typeof MigratoryDataClient_Disable_WebSocket_Transport!=="undefined"&&MigratoryDataClient_Disable_WebSocket_Transport==true)MigratoryDataClient.ck=false;if(MigratoryDataClient.ck==false)if(window.XDomainRequest){var xdrTest=new XDomainRequest;try{xdrTest.open("GET",window.location.protocol+"//127.0.0.1");xdrTest.send();MigratoryDataClient.d7=true;xdrTest.abort()}catch(e$$7){xdrTest.abort();MigratoryDataClient.d7=false}}else MigratoryDataClient.d7=false;
	if(MigratoryDataClient.c==="IE")try{window.attachEvent("onunload",function(){MigratoryDataClient.e5()})}catch(e$$8){window.addEventListener("unload",function(){MigratoryDataClient.e5()},false)}else window.addEventListener("unload",function(){MigratoryDataClient.e5()},false);MigratoryDataClient.e();if(MigratoryDataClient.c==="WebKit iPhone")MigratoryDataClient.es=65536;else if(MigratoryDataClient.c==="Opera Mobile"){MigratoryDataClient.es=32768;MigratoryDataClient.ex=500}
	if(MigratoryDataClient.ck==true){MigratoryDataClient.cw=true;MigratoryDataClient.cz=true;MigratoryDataClient.d3=true}else if(MigratoryDataClient.d7&&window.postMessage){MigratoryDataClient.cw=true;MigratoryDataClient.cz=true;MigratoryDataClient.d3=true;try{window.attachEvent("onmessage",MigratoryDataClient.et)}catch(e$$9){window.addEventListener("onmessage",MigratoryDataClient.et,false)}}else if(this.XMLHttpRequest&&(new XMLHttpRequest).withCredentials!==undefined){MigratoryDataClient.cw=true;MigratoryDataClient.cz=
	true}else if(this.swfobject&&swfobject.hasFlashPlayerVersion("9")&&(typeof MigratoryDataClient_Allow_Flash_Transport==="undefined"||MigratoryDataClient_Allow_Flash_Transport==true)){MigratoryDataClient.cw=true;MigratoryDataClient.i(function(){MigratoryDataClient.eo()})}if(!MigratoryDataClient.cw||MigratoryDataClient.cz)MigratoryDataClient.i(function(){MigratoryDataClient.dx()});
	
	(function (window, QSAPI) {
		'use strict';
	    if( typeof QSAPI === typeof undefined){
	        window.QSAPI = QSAPI = {};
	    }
	    if (!QSAPI.Util) {
	        QSAPI.Util = {};
	    }
	    if (!QSAPI.Widget) {
	        QSAPI.Widget = {};
	    }
	    if (!QSAPI.Data) {
	        QSAPI.Data = {};
	    }
	    if (!QSAPI.Lang) {
	        QSAPI.Lang = {};
	    }
	    if (!QSAPI.QS_Lang) {
	        QSAPI.QS_Lang = {};
	    }
	    if (!QSAPI.$ && (window.$ || window.jQuery)) {
	        QSAPI.$ = window.$ || window.jQuery;
	    }
	})(window, typeof QSAPI !== 'undefined' ? QSAPI : undefined);
	
	(function(QSAPI) {
	    'use strict';
	    var $ = QSAPI.$;
	    var config = {
	        showDelayAsBackup: true
	    };
	    var base = {
	        set: function(cache, sourceType, ticker, data) { //create/update cache
	            var _cache = this._getCache(cache, sourceType);
	            if (!_cache[ticker]) {
	                _cache[ticker] = data;
	            } else {
	                $.extend(_cache[ticker], data);
	            }
	        },
	        remove: function(cache, sourceType, ticker) { //remove cache
	            var _cache = this._getCache(cache, sourceType);
	            delete _cache[ticker];
	        },
	        get: function(cache, sourceType, ticker) { //get cache           
	            var _cache = this._getCache(cache, sourceType);
	            if (!_cache[ticker] || $.isEmptyObject(_cache[ticker])) {
	                return {};
	            }
	            return $.extend({}, _cache[ticker]);
	        },
	        _getCache: function(cache, sourceType) {
	            if (!sourceType) {
	                return cache;
	            } else {
	                return cache[sourceType];
	            }
	        },
	        _getLatestData: function(data1, data2) { //Compare market data
	            data1 = data1 || {};
	            data2 = data2 || {};
	            var data2Ahead = true;
	            if (data1["TradeDate"] && data2["TradeDate"]) {
	                var date1 = parseInt(data1["TradeDate"].replace(/\-/g, ""), 10),
	                    date2 = parseInt(data2["TradeDate"].replace(/\-/g, ""), 10);
	                if (date1 > date2) {
	                    data2Ahead = false;
	                } else if (date1 < date2) {
	                    data2Ahead = true;
	                } else {
	                    if (data1["TradeTime"] && data2["TradeTime"]) {
	                        var time1 = parseInt(data1["TradeTime"], 10),
	                            time2 = parseInt(data2["TradeTime"], 10);
	                        if (time1 > time2) {
	                            data2Ahead = false;
	                        } else if (time1 < time2) {
	                            data2Ahead = true;
	                        }
	                    }
	                }
	            }
	            if (data2Ahead) {
	                return $.extend({}, data1, data2);
	            } else {
	                return $.extend({}, data2, data1);
	            }
	        },
	        _getDataField: function(data, field) {
	            if (!field) {
	                return data;
	            } else {
	                return data[field];
	            }
	        }
	    };
	    var core = {
	        T: {
	            cache: {},
	            gkeyListTickerMap: {}, //{"1.1.IBM"/*query ticker*/:"14.1.IBM"/*list ticker*/}
	            set: function(listTicker, data) {
	                base.set(this.cache, false, listTicker, data);
	                if (data["gkey"]) {
	                    this.gkeyListTickerMap[data["gkey"]] = listTicker;
	                }
	            },
	            get: function(gkey) {
	                var listTicker = this.gkeyListTickerMap[gkey];
	                var tickerObj = base.get(this.cache, false, listTicker);
	                tickerObj = $.extend({}, tickerObj, { gkey: gkey });
	                return tickerObj;
	            }
	        },
	        S: {
	            cache: {},
	            set: function(subTicker, data) {
	                base.set(this.cache, false, subTicker, data);
	            },
	            remove: function(subTicker) {
	                base.remove(this.cache, false, subTicker);
	            },
	            get: function(subTicker) {
	                return base.get(this.cache, false, subTicker);
	            }
	        },
	        Raw: {
	            cache: {},
	            subTickerMap: {},
	            set: function(subTicker, data) {
	                base.set(this.cache, false, subTicker, data);
	            },
	            remove: function(subTicker) {
	                base.remove(this.cache, false, subTicker);
	            },
	            get: function(subTicker) {
	                var tickers = this.getTickerMap(subTicker),
	                    data = {};
	                for (var ticker in tickers) {
	                    $.extend(data, base.get(this.cache, false, ticker));
	                }
	                return data;
	            },
	            setTickerMap: function(subTicker, extendTicker) {
	                this.subTickerMap[subTicker] = this.subTickerMap[subTicker] || {};
	                this.subTickerMap[subTicker][extendTicker] = true;
	            },
	            getTickerMap: function(subTicker) {
	                var defMap = {};
	                defMap[subTicker] = true;
	                return this.subTickerMap[subTicker] || defMap;
	            }
	        },
	        R: {
	            cache: { pull: {}, push: {} },
	            set: function(sourceType, subTicker, data) {
	                base.set(this.cache, sourceType, subTicker, data);
	            },
	            remove: function(sourceType, subTicker) {
	                base.remove(this.cache, sourceType, subTicker);
	            },
	            get: function(sourceType, subTicker) {
	                return base.get(this.cache, sourceType, subTicker);
	            }
	        },
	        D: {
	            cache: { pull: {}, push: {} },
	            set: function(sourceType, subTicker, data) {
	                base.set(this.cache, sourceType, subTicker, data);
	            },
	            remove: function(sourceType, subTicker) {
	                base.remove(this.cache, sourceType, subTicker);
	            },
	            get: function(sourceType, subTicker) {
	                return base.get(this.cache, sourceType, subTicker);
	            }
	        }
	    };
	    QSAPI.Cache = {
	        getTickerData: function(gkey, field) { // ticker data
	            var data = core.T.get(gkey);
	            return base._getDataField(data, field);
	        },
	        setTickerData: function(listTicker, data) {
	            core.T.set(listTicker, data);
	        },
	        getStaticData: function(subTicker, field) { // static data
	            var data = core.S.get(subTicker);
	            return base._getDataField(data, field);
	        },
	        setStaticData: function(subTicker, data) {
	            core.S.set(subTicker, data);
	        },
	        getRawData: function(subTicker, field) { // raw data
	            var data = core.Raw.get(subTicker);
	            return base._getDataField(data, field);
	        },
	        setRawData: function(subTicker, data) {
	            core.Raw.set(subTicker, data);
	        },
	        setRawTickerMap: function(subTicker, data) {
	            core.Raw.setTickerMap.apply(core.Raw, arguments);
	        },
	        getMarketData: function(sourceType, RDType, ticker) { //market data
	            RDType = (RDType || '').toUpperCase();
	            var _cache = {};
	            if (ticker) {
	                var isMixRD = false,
	                    isMixPullPush = false;
	                if (RDType === 'R' || RDType === 'D') {
	                    _cache = core[RDType].cache;
	                } else {
	                    isMixRD = true;
	                }
	                if (sourceType === 'pull' || sourceType === 'push') { // || sourceType == "push"
	                    if (!isMixRD) {
	                        _cache = _cache[sourceType][ticker];
	                    } else {
	                        _cache = base._getLatestData(core["D"].cache[sourceType][ticker], core["R"].cache[sourceType][ticker]);
	                    }
	                } else {
	                    if (isMixRD) {
	                        var _D = base._getLatestData(core["D"].cache["push"][ticker], core["D"].cache["pull"][ticker]);
	                        var _R = base._getLatestData(core["R"].cache["push"][ticker], core["R"].cache["pull"][ticker]);
	                        _cache = base._getLatestData(_D, _R);
	                    } else {
	                        _cache = base._getLatestData(_cache["push"][ticker], _cache["pull"][ticker]);
	                    }
	                }
	            }
	            return _cache || {};
	        },
	        setMarketData: function(sourceType, RDType, ticker, data) {
	            core[RDType].cache[sourceType][ticker] = core[RDType].cache[sourceType][ticker] || {};
	            $.extend(core[RDType].cache[sourceType][ticker], data);
	        },
	        getData: function(component, gkey, field, includeStatic) { // market data mix with ticker data, raw data
	            if (!gkey) return {};
	            var subTicker = QSAPI.DataManager.getSubscribeTicker(gkey, component),
	                RDType = QSAPI.DataManager.getRDType(this.getTickerData(gkey)),
	                data = this.getDataBySubTicker(component, subTicker, RDType, field, includeStatic);
	            if (!field) {
	                var tickerData = this.getTickerData(gkey);
	                data = $.extend(tickerData, data);
	            }
	            return data;
	        },
	        getDataBySubTicker: function(component, subTicker, RDType, field, includeStatic) { // market data mix with ticker data, raw data
	            if (!subTicker) return {};
	            var marketData = this.getMarketData(component.dataType, QSAPI.Util.getValidRDType(component.forceDelay, RDType), subTicker),
	                rawData = this.getRawData(subTicker),
	                data = {
	                    'R/D': RDType
	                };
	            if (includeStatic) {
	                var staticData = this.getStaticData(subTicker);
	                $.extend(data, staticData, marketData, rawData);
	            } else {
	                $.extend(data, marketData, rawData);
	            }
	            return base._getDataField(data, field);
	        },
	        getMarketDataByGkey: function(component, gkey) { // market data
	            if (!gkey) return {};
	            var subTicker = QSAPI.DataManager.getSubscribeTicker(gkey, component),
	                RDType = QSAPI.DataManager.getRDType(this.getTickerData(gkey)),
	                marketData = this.getMarketData(component.dataType, QSAPI.Util.getValidRDType(component.forceDelay, RDType), subTicker);
	            return marketData;
	        },
	        remove: function(subTicker) {
	            core.S.remove(subTicker);
	            core.Raw.remove(subTicker);
	            this.removeMarketData(subTicker);
	        },
	        removeMarketData: function(ticker) {
	            core.R.remove("pull", ticker);
	            core.R.remove("push", ticker);
	            core.D.remove("pull", ticker);
	            core.D.remove("push", ticker);
	        },
	        S: core.S.cache,
	        T: core.T.cache,
	        Raw: core.Raw.cache,
	        R: core.R.cache,
	        D: core.D.cache
	    }
	})(QSAPI);
	(function (QSAPI) {
	    'use strict';
	    var $ = QSAPI.$;
	    var Util = QSAPI.Util;
	
	    if (!QSAPI.QS_TimezoneMap) { // defined the minutes offset to UTC for some timezones.
	        QSAPI.QS_TimezoneMap = {
	            "ADT": -180,
	            "AKDT": -480,
	            "AKST": -540,
	            "AST": -240,
	            "CDT": -300,
	            "CST": -360,
	            "EDT": -240,
	            "EST": -300,
	            "EGT": -60,
	            "EGST": 0,
	            "GMT": 0,
	            "HADT": -540,
	            "HAST": -600,
	            "MDT": -360,
	            "MST": -420,
	            "NDT": -150,
	            "NST": -210,
	            "PDT": -420,
	            "PST": -480,
	            "WGT": -180,
	            "WGST": -120,
	            "CET": 60,
	            "BST": 60,
	            "CEST": 120,
	            "SAST": 120
	        };
	    }
	
	    if (!QSAPI.QS_TimezoneForRegionMap) { // defined the minutes offset to UTC for some timezones.
	        QSAPI.QS_TimezoneForRegionMap = {
	            "ADT": "Atlantic/Bermuda",
	            "AKDT": "US/Alaska", //not exist in new timezoneMap yet
	            "AKST": "US/Alaska", //not exist in new timezoneMap yet
	            "AST": "Asia/Bahrain",
	            "CDT": "America/Mexico_City",
	            "CST": "US/Central",
	            "EDT": "America/New_York",
	            "EST": "America/New_York",
	            "EGT": "America/Scoresbysund", //not exist in new timezoneMap yet
	            "EGST": "America/Scoresbysund", //not exist in new timezoneMap yet
	            "GMT": "GMT",
	            "HADT": "US/Hawaii", //not exist in new timezoneMap yet
	            "HAST": "US/Hawaii", //not exist in new timezoneMap yet
	            "MDT": "America/Mazatlan",
	            "MST": "US/Mountain", //mock
	            "NDT": "Canada/Newfoundland", //not exist in new timezoneMap yet
	            "NST": "Canada/Newfoundland", //not exist in new timezoneMap yet
	            "PDT": "US/Pacific", //not exist in new timezoneMap yet
	            "PST": "US/Pacific", //not exist in new timezoneMap yet
	            "WGT": "America/Godthab", //not exist in new timezoneMap yet
	            "WGST": "America/Godthab", //not exist in new timezoneMap yet
	            "CET": "CET",
	            "BST": "Europe/London",
	            "CEST": "Europe/Paris",
	            "SAST": "Africa/Johannesburg"
	        };
	    }
	
	    var QS_TimezoneMap = QSAPI.QS_TimezoneMap;
	    var QS_TimezoneForRegionMap = QSAPI.QS_TimezoneForRegionMap;
	
	    /**
	     * Adapt old timezone way, like input 'cet','GMT','SAST'
	     * First match old mapping, and match new mapping is exist.
	     */
	    Util.getTimezoneDiffAdaptOldMapping = function (timezone) {
	        var oldTimezoneMap = QS_TimezoneMap;
	        var newTimezoneMap = QS_TimezoneForRegionMap;
	        var timeDiff = 0;
	
	        if (oldTimezoneMap[timezone]) {
	            timeDiff = oldTimezoneMap[timezone];
	        }
	        var newTimezoneName = newTimezoneMap[timezone];
	        if (newTimezoneName) {
	            timeDiff = QSAPI.DataManager.getTimezoneTimediff(newTimezoneName) || timeDiff;
	        }
	        return timeDiff;
	    };
	
	    Util.formatString = function (str) {
	        if (typeof (str) != "string") return "";
	        if (arguments.length <= 1)
	            return str;
	        var lastIndex = arguments.length - 2;
	        for (var i = 0; i <= lastIndex; i++)
	            str = str.replace(new RegExp("\\{" + i + "\\}", "gi"), arguments[i + 1]);
	        return str;
	    };
	    Util.checkAuth = function (callbacks, result, textStatus, runComponentAuthCheckFirst, ajaxConfig) {
	        var o = typeof result == 'string' ? $.evalJSON(result) : result;
	        var status = o && o.status ? o.status : {};
	        if (status.errorCode == '-1' && status.subErrorCode == '10000') {
	            if (runComponentAuthCheckFirst) {
	                callbacks.onSuccess(result, textStatus);
	            } else if (typeof callbacks.onFailure == 'function') {
	                callbacks.onFailure(GLOBALSTATE.AUTHEXPIRED);
	            }
	            Util.handleApiTokenExpiredCallback(ajaxConfig);
	        } else {
	            if (typeof callbacks.onSuccess == 'function') {
	                callbacks.onSuccess(result, textStatus);
	            }
	        }
	    };
	
	    Util._isGETJSON = function (config) {
	        return config.type.toLowerCase() == 'get' && (config.dataType == 'json' || config.dataType == 'jsonp');
	    };
	
	    Util._isSameDomain = function (config) {
	        if (config.url) {
	            var rprotocol = /^\/\//;
	            var rurl = /^([\w.+-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/;
	            var ajaxLocation;
	            try {
	                ajaxLocation = location.href;
	            } catch (e) {
	                // Use the href attribute of an A element
	                // since IE will modify it given document.location
	                ajaxLocation = document.createElement("a");
	                ajaxLocation.href = "";
	                ajaxLocation = ajaxLocation.href;
	            }
	
	            // Segment location into parts
	            var ajaxLocParts = rurl.exec(ajaxLocation.toLowerCase()) || [];
	            var url = config.url.replace(rprotocol, ajaxLocParts[1] + "//");
	
	            var parts = rurl.exec(url.toLowerCase());
	            if (parts == null) return true;
	            return parts[2] === ajaxLocParts[2];
	        }
	        return false;
	    };
	
	    Util.ajax = function (url, config) {
	        $.support.cors = true;
	        if (!url.third) { //access 3th service
	            if (typeof url === "object") {
	                config = url;
	                url = undefined;
	            }
	            var sessionId, session_sessionId, CSRFToken;
	            if (typeof QSAPI != 'undefined') {
	                if (typeof QSAPI.getSessionId == 'function') {
	                    sessionId = QSAPI.getSessionId();
	                    session_sessionId = QSAPI.getSessionId(true);
	                }
	                if (typeof QSAPI.getCSRFToken == 'function') {
	                    CSRFToken = QSAPI.getCSRFToken();
	                }
	                if (typeof QSAPI.getInstID == 'function') {
	                    config.data = config.data || {};
	                    config.data.instid = QSAPI.getInstID();
	                }
	                if (typeof QSAPI.getVersion == 'function') {
	                    config.data = config.data || {};
	                    config.data.sdkver = QSAPI.getVersion();
	                }
	                if ($.isFunction(QSAPI.getAccessToken)) {
	                    config.data = config.data || {};
	                    config.data.accessToken = QSAPI.getAccessToken();
	                }
	            }
	            if (typeof CSRFToken != 'undefined' && CSRFToken != null && CSRFToken.length > 0) {
	                config.data = config.data || {};
	                config.data["CToken"] = CSRFToken;
	            }
	            if (typeof sessionId != 'undefined' && sessionId != null && sessionId.length > 0) {
	                config.data = config.data || {};
	                config.data['qs_wsid'] = sessionId;
	                config.data.accessToken = null;
	                delete config.data.accessToken;
	            } else {
	                if (typeof session_sessionId != 'undefined' && session_sessionId != null && session_sessionId.length > 0 && !Util._isSameDomain(config)) {
	                    config.data = config.data || {};
	                    config.data['qs_wsid'] = session_sessionId;
	                    config.data.accessToken = null;
	                    delete config.data.accessToken;
	                } else if (config.data.accessToken) {
	                    var headers = {
	                        "authorization": "Bearer " + config.data.accessToken.replace(/^Bearer\s/, ''),
	                        "cache-control": "no-cache"
	                    };
	                    if (/.+QS-markets.*/.test(config.url) || /.+QS-pullqs.*/.test(config.url)) {
	                        headers.accept = 'text/plain';
	                    }
	                    config.headers = headers;
	                    config.data.accessToken = null;
	                    config.data.sessionId = null;
	                    delete config.data.accessToken;
	                    delete config.data.sessionId;
	                    delete config.data.instid;
	
	                    if (/.+QS-fod.*/.test(config.url)) {
	                        config.data.dataFrequency = null;
	                        config.data.device = null;
	                        config.data.componentName = null;
	                        config.data.sdkver = null;
	                        config.data.CToken = null;
	                        config.data._ = null;
	                        delete config.data.dataFrequency;
	                        delete config.data.device;
	                        delete config.data.componentName;
	                        delete config.data.sdkver;
	                        delete config.data.CToken;
	                        delete config.data._;
	                    }
	                }
	            }
	
	            var _oldSuccess = config.success;
	            config.data.productType = QSAPI.getProductType(config.data.productType);
	            var ajaxSuccessProcess = function (o, textStatus, ajaxConfig) {
	                Util.checkAuth({
	                    onSuccess: _oldSuccess,
	                    onFailure: QSAPI.getInitCallback()
	                }, o, textStatus, config.runComponentAuthCheckFirst, ajaxConfig)
	            }
	
	            if (Util._isGETJSON(config) && Util._isSupportCORS()) {
	                var _d = Util._parse(config.url, config.data);
	                config.url = _d.url;
	                config.data = _d.data;
	                config.type = 'get';
	                if (!/.+QS-fod.*/.test(config.url)) {
	                    config.dataType = null;
	                    delete config.dataType;
	                }
	                config.success = function (o, textStatus) {
	                    o = o || '';
	                    if (o != '' && typeof o !== 'object') {
	                        o = o.replace(/\r+\n+/g, '');
	                    } else if (typeof o === 'object') {
	                        o = $.toJSON(o);
	                    }
	                    try {
	                        o = o == '' ? {} : $.evalJSON(o);
	                    } catch (error) {
	                        try {
	                            o = o == '' ? {} : eval("(" + o + ")");
	                        } catch (error) {
	                            o = '{}';
	                        }
	                    }
	                    ajaxSuccessProcess(o, textStatus, config);
	                };
	                config.error = function (xhr, textStatus) {
	                    if (xhr.status === 401 || xhr.status === 403) {
	                        // normal error handle
	                        var callback = QSAPI.getInitCallback();
	                        var returnObject = QSAPI.Util.errorHandler(xhr, textStatus);
	                        if (typeof callback == 'function') {
	                            callback(returnObject);
	                        }
	
	                        Util.handleApiTokenExpiredCallback(config);
	                    }
	                };
	            } else {
	                config.success = ajaxSuccessProcess;
	            }
	
	            return $.ajax(config);
	        } else {
	            return $.ajax(url);
	        }
	    };
	
	    Util.handleApiTokenExpiredCallback = function (ajaxConfig) {
	        var apiTokenExpiredCallback = QSAPI.getApiTokenExpiredCallback();
	
	        if (typeof apiTokenExpiredCallback == 'function') {
	            var isApiGateway = QSAPI.isAPIGateway();
	            var env = QSAPI.getQSVersion();
	
	            /*  return options parameter when using sdk
	                this object will cover when using ec-loader
	            options: {
	                token: {
	                    url: 'http://markets-uat.morningstar.com/getids.jsp?instid=QSDEMO&sdkver=2.4.28...',
	                    env: 'stage',
	                    name: 'marketsApiToken'
	                }
	            }
	            */
	            var options = {};
	            options['token'] = {};
	            options.url = ajaxConfig.url; // just adapt ec-loader
	            options['token'].url = ajaxConfig.url;
	            options['token'].environment = env;
	            if (isApiGateway) {
	                options['token'].name = 'accessToken';
	            } else {
	                options['token'].name = 'sessionKey';
	            }
	
	            /**
	             * Get a new token from apiTokenExpiredCallback outside, and send the fail request again.
	             * retryCount avoid send fail request infinite, just one time.
	             * 
	             * Now support sessionKey/accessToken
	             */
	            var _handleReceiveToken = function (tokenName, tokenValue) {
	
	                if (tokenName === 'accessToken') {
	                    QSAPI.setAccessToken(tokenValue);
	                    ajaxConfig.headers['authorization'] = "Bearer " + tokenValue.replace(/^Bearer\s/, '');
	                } else if (tokenName === 'sessionKey') {
	                    QSAPI.setSessionId(tokenValue);
	                    ajaxConfig.data['qs_wsid'] = tokenValue;
	                }
	
	                if (!ajaxConfig.retryCount) {
	                    ajaxConfig.retryCount = 1;
	                    return $.ajax(ajaxConfig);
	                }
	            }
	
	            apiTokenExpiredCallback(options, _handleReceiveToken);
	        }
	    }
	
	    $.each(["get", "post"], function (i, method) {
	        Util[method] = function (url, data, callback, type) {
	            if (typeof data === 'function') {
	                type = type || callback;
	                callback = data;
	                data = undefined;
	            }
	            return Util.ajax({
	                type: method,
	                url: url,
	                data: data,
	                success: callback,
	                dataType: type
	            });
	        };
	    });
	
	    Util.getJSON = function (url, data, callback) {
	        return Util.get(url, data, callback, 'json');
	    };
	
	    Util.getScript = function (url, callback) {
	        return Util.get(url, undefined, callback, "script");
	    };
	
	    Util._parse = function (url, data) {
	        url = url.replace(/=\?(&|$)/g, "=jsonpcallbcak$1");
	        var arr = url.split("?");
	        url = arr[0];
	        if (arr.length > 1) {
	            if (typeof data == 'undefined') {
	                data = {};
	            }
	            var paras = arr[1].split("&"),
	                _temp;
	            for (var i = 0, l = paras.length; i < l; i++) {
	                _temp = paras[i].split("=");
	                if (_temp.length >= 1 && _temp[1] != "jsonpcallbcak" && _temp[1] != "" && typeof data[_temp[0]] == 'undefined') {
	                    data[_temp[0]] = _temp[1] || "_empty_";
	                }
	            }
	        }
	        if (data['sessionId'] && data['qs_wsid']) {
	            data['qs_wsid'] = null;
	            delete data['qs_wsid'];
	        }
	        var _paras = [];
	        if (typeof data == 'object' && $.isEmptyObject(data) == false) {
	            for (var k in data) {
	                if (k != 'qs_wsid' && k != 'sessionId' && k != 'tmpid') {
	                    if (data[k] === '_empty_') {
	                        _paras.push(k);
	                    } else {
	                        _paras.push(k + "=" + encodeURIComponent(data[k]));
	                    }
	                    data[k] = null;
	                    delete data[k];
	                }
	            }
	        }
	        if (_paras.length > 0) {
	            url += "?" + _paras.join("&");
	        }
	        return {
	            url: url,
	            data: data
	        };
	    };
	
	    //Detect browser support for CORS
	    Util._isSupportCORS = function () {
	        if ('withCredentials' in new XMLHttpRequest()) { /* supports cross-domain requests */
	            return true;
	        } else {
	            return false;
	        }
	    };
	
	    Util.IdProducer = {
	        val: 0,
	        getId: function () {
	            this.val++;
	            return this.val;
	        }
	    };
	
	    Util.StringBuffer = function (str) {
	        var arr = [];
	        var s = str || "";
	        arr.push(s);
	        this.append = function (str1) {
	            arr.push(str1);
	            return this;
	        };
	        this.toString = function () {
	            return arr.join("");
	        };
	        this.appendFormat = function (str1) {
	            var r = /{\d+}/;
	            var args = [].slice.call(arguments);
	            args.shift();
	            var m;
	            while (m = str1.match(r)) {
	                str1 = str1.replace(m[0], args[0]);
	                args.shift();
	            }
	            arr.push(str1);
	        };
	    };
	    Util.exportFile = function (data) {
	        if (Util.isIOS()) {
	            Util.openWindow(data);
	        } else {
	            Util.openWindowWithPost(data);
	        }
	    };
	    Util.openWindow = function (data) {
	        var sessionId = QSAPI.getSessionId();
	        if (typeof sessionId != 'undefined' && sessionId != null && sessionId.length > 0) {
	            data['qs_wsid'] = sessionId;
	        }
	        var params = [];
	        for (var item in data) {
	            if (item == "url") continue;
	            params.push(item + '=' + (data[item] + '').replace(/\'/g, "&apos;"));
	        }
	        window.open(data.url + '?' + params.join('&'), 'exportWindow');
	    };
	
	    Util.openWindowWithPost = function (data) {
	        var form = null;
	        var sessionId = QSAPI.getSessionId();
	        if (typeof sessionId != 'undefined' && sessionId != null && sessionId.length > 0) {
	            data['qs_wsid'] = sessionId;
	        }
	        if (typeof (window.frames["export"]) == 'undefined') {
	            var html = "<div class='export' style='display:none;'>";
	            html += "<form id='exportform' name='exportform' method='post' action='" + data.url + "' target='export'>";
	            html += "</form>";
	            html += "<iframe id='export' name='export' style='display:none;'></iframe>";
	            html += "</div>";
	            var el = $(html).appendTo($('body'));
	            form = el.find("form#exportform");
	        } else {
	            form = $('body').find("form#exportform").attr("action", data.url);
	        }
	        form.empty();
	        var element = "";
	        var value = "";
	        for (var item in data) {
	            if (item == "url") continue;
	            value = (data[item] + '').replace(/\'/g, "&apos;");
	            element += "<input type='hidden' name='" + item + "' value='" + value + "'/>";
	        }
	        form.append(element);
	        form.submit();
	    };
	    Util.getSecID = function (tickerObject) {
	        var secId = null,
	            id = tickerObject.getField('secId') || tickerObject.getField('performanceId') || '';
	        if (id != '') {
	            secId = id;
	        }
	        return secId;
	    };
	    Util.getTenforeID = function (tickerObject) {
	        return Util.generateGKey(tickerObject);
	    };
	
	    Util.getSecurityInfo = function (tickerObject) {
	        return {
	            secId: tickerObject.getField('secId') || '',
	            performanceId: tickerObject.getField('performanceId') || '',
	            gkey: tickerObject.getField('gkey') || '',
	            ISIN: tickerObject.getField('ISIN'),
	            CUSIP: tickerObject.getField('CUSIP'),
	            Name: tickerObject.getField('Name')
	        }
	    };
	
	    Util.openWindowDirect = function (data) {
	        var form = null;
	        var sessionId = QSAPI.getSessionId();
	        if (typeof sessionId != 'undefined' && sessionId != null && sessionId.length > 0) {
	            data['qs_wsid'] = sessionId;
	        }
	        if (typeof (window.frames["export"]) == 'undefined') {
	            var html = "<div class='export' style='display:none;'>";
	            html += "<form id='openwindow' name='openwindow' method='post' action='" + data.url + "' target='_blank'>";
	            html += "</form>";
	            html += "</div>";
	            var el = $(html).appendTo($('body'));
	            form = el.find("form#openwindow");
	        } else {
	            form = $('body').find("form#openwindow");
	        }
	        form.empty();
	        var element = "";
	        for (var item in data) {
	            if (item == "url") continue;
	            element += "<input type='hidden' name='" + item + "' value='" + data[item] + "'/>";
	        }
	        form.append(element);
	        form.submit();
	    };
	    Util.getData = function (config, callbacks) {
	        $.support.cors = true;
	        var defaultConfig = {
	            type: 'GET',
	            cache: false,
	            timeout: 30000,
	            dataType: 'json',
	            url: null,
	            data: {},
	            success: function () {},
	            error: function () {}
	        };
	        $.extend(true, defaultConfig, config);
	        defaultConfig.success = function (data, textStatus) {
	            var data = data || {};
	            if (!data.status || data.status.errorCode == '0') { //new strcture
	                if (typeof callbacks.onSuccess == 'function') {
	                    callbacks.onSuccess(data);
	                }
	            } else {
	                if (data.data) {
	                    if (typeof callbacks.onSuccess == 'function') {
	                        callbacks.onSuccess(data);
	                    }
	                } else if (typeof callbacks.onFailure == 'function') { //-1 stand for account error
	                    callbacks.onFailure(data);
	                }
	            }
	        };
	        defaultConfig.error = function (xhr, textStatus, errorThrown) {
	            var data = errorHandler(xhr, textStatus);
	            if (typeof callbacks.onFailure == 'function') {
	                callbacks.onFailure(data);
	            }
	        };
	        Util.ajax(defaultConfig);
	    };
	    var errorHandler = function (xhr, textStatus) {
	        var data;
	        if (xhr.status == "401") {
	            data = GLOBALSTATE.AUTHERR;
	        } else if (xhr.status == "404") {
	            data = GLOBALSTATE.NOFIND;
	        } else if (xhr.status == "503") {
	            data = GLOBALSTATE.SERUNAVAIL;
	        } else if (textStatus == 'timeout') {
	            data = GLOBALSTATE.TIMEOUT;
	        } else {
	            data = GLOBALSTATE.SERERR;
	        }
	        return data;
	    }
	    Util.errorHandler = function (xhr, textStatus) {
	        return errorHandler(xhr, textStatus);
	    };
	    Util.getDiffByTimezone = function (exch, objTs) { // get the diff of timezone between the timezone of exchange and object timezone.
	        if (typeof (QSAPI.DataManager) == "undefined") return 0;
	        if (typeof (QS_TimezoneMap) != "object" || typeof QS_TimezoneMap[objTs] == "undefined" || typeof (QSAPI.DataManager.cfg.exchangeTimezoneMap) != "object" || typeof QSAPI.DataManager.cfg.exchangeTimezoneMap[exch] == "undefined") return 0;
	        return QSAPI.DataManager.cfg.exchangeTimezoneMap[exch] - QS_TimezoneMap[objTs];
	    };
	    Util.getTimeZoneOffset = function (objTs) {
	        if (typeof objTs == 'undefined' || objTs.length == 0 || typeof (QS_TimezoneMap) != "object" || typeof QS_TimezoneMap[objTs] == "undefined") {
	            return -99999;
	        } else {
	            return QS_TimezoneMap[objTs];
	        }
	    };
	    Util.isCanadaExch = function (exch) {
	        if (exch == 113 || exch == 127 || exch == 128) {
	            return true;
	        }
	        return false;
	    };
	
	    /**
	     * use backend componsiteExchangeID to confirm market
	     */
	    Util.isCompositiveExch = function (tickerObject) {
	        if (!tickerObject['CompositeExchangeID']) {
	            return false;
	        }
	        return !(tickerObject['CompositeExchangeID'] === tickerObject['listMarket']);
	    };
	    /**
	     * is us compositive exch 
	     */
	    Util.isUSCompositiveExch = function (tickerObject) {
	        return Util.isCompositiveExch(tickerObject) && tickerObject['listMarket'] < 126;
	    };
	    Util.generateGKey = function (tenforeCode, tenforeType, tenforeTicker, mType) {
	        if (typeof (tenforeCode.getField) == "function") {
	            var tickerObj = tenforeCode;
	            tenforeCode = tickerObj.getField("tenforeCode");
	            tenforeType = tickerObj.getField("type");
	            tenforeTicker = tickerObj.getField("tenforeTicker");
	        }
	        //tenforeCode = Util.generateGKeyCode.apply(this,arguments);
	        return [tenforeCode, tenforeType, tenforeTicker].join(".");
	    };
	    Util.generateGKeyCode = function (tickerObject) {
	        /*if(tenforeCode==22||tenforeCode==10||tenforeCode==511||tenforeCode==25){
	            tenforeCode=125;
	        }
	        else */ //Do not need to convert OOTC to 125, because the inst maybe not have 125 permission
	        var tenforeCode = tickerObject.tenforeCode;
	        if (Util.isCompositiveExch(tickerObject)) {
	            tenforeCode = tickerObject['CompositeExchangeID'];
	        }
	        return tenforeCode;
	    };
	    var exchTimezoneMap = {
	        134: "CST",
	        136: "CST",
	        132: "JST",
	        139: "IST",
	        146: "EDT",
	        151: "GMT",
	        211: "CET",
	        213: "CET",
	        181: "CET",
	        245: "GMT",
	        193: "SAST",
	        156: "BST"
	
	    };
	    Util.getTimezoneByExch = function (exch) {
	        return exchTimezoneMap[exch] || "EST";
	    };
	    var GLOBALSTATE = {
	        AUTHOK: {
	            errorCode: '0',
	            errorMsg: 'Authentication is successful.'
	        },
	        INITERR: {
	            errorCode: '-1',
	            errorMsg: 'Not initialize QSAPI.'
	        },
	        AUTHERR: {
	            errorCode: '-2',
	            errorMsg: 'Account is incorrect.'
	        },
	        //Above is for method QSAPI.init()
	        SERUNAVAIL: {
	            errorCode: '-3',
	            errorMsg: 'Service temporarily unavailable.'
	        },
	        TIMEOUT: {
	            errorCode: '-4',
	            errorMsg: 'Request timeout.'
	        },
	        NOFIND: {
	            errorCode: '-5',
	            errorMsg: 'Service not found.'
	        },
	        SERERR: {
	            errorCode: '-6',
	            errorMsg: 'Service response error.'
	        },
	        AUTHEXPIRED: {
	            errorCode: '-7',
	            errorMsg: 'Authentication is expired.'
	        },
	        RETURNERR: {
	            errorCode: '-8',
	            errorMsg: 'Response is invalid.'
	        },
	        //Above is for all ajax request
	        CONNOKPULL: {
	            errorCode: '0',
	            errorMsg: 'Connect pull server successfully.'
	        },
	        CONNERRPULL: {
	            errorCode: '-2',
	            errorMsg: 'Connect pull server unsuccessfully.'
	        },
	        //Above is for method QSAPI.DataManager.connectToPull()
	        CONNOKPUSH: {
	            errorCode: '0',
	            errorMsg: 'Connect push server successfully.'
	        },
	        CONNERRPUSH: {
	            errorCode: '-3',
	            errorMsg: 'Connect push server unsuccessfully.'
	        },
	        DUPLICATELOGINERR: {
	            errorCode: '-9',
	            errorMsg: 'Duplicated login.'
	        },
	        //Above is for method QSAPI.DataManager.connectToPush()
	        NOAUTH: {
	            errorCode: '-1',
	            errorMsg: 'No authentication.'
	        },
	        //Above is for method QSAPI.DataManager.connectToPush()/QSAPI.DataManager.connectToPull()
	        SUBOK: {
	            errorCode: '0',
	            errorMsg: 'Subscribe successfully.'
	        },
	        SUBFAIL: {
	            errorCode: '-4',
	            errorMsg: 'Some tickers objects can not be subscribed.'
	        },
	        //Above is for method QSAPI.DataManager.subscribePush()/QSAPI.DataManager.subscribePull()
	        UNSUBOK: {
	            errorCode: '0',
	            errorMsg: 'Unsubscribe successfully.'
	        },
	        UNSUBFAIL: {
	            errorCode: '-4',
	            errorMsg: 'Some tickers objects can not be unsubscribed.'
	        },
	        //Above is for method QSAPI.DataManager.unSubscribePull()/QSAPI.DataManager.unSubscribePush()
	        CONNERR: {
	            errorCode: '-5',
	            errorMsg: 'Not Connect.'
	        },
	        NOSUBID: {
	            errorCode: '-6',
	            errorMsg: 'Empty subscribeID of this component.'
	        }
	        //Above is for method QSAPI.DataManager.subscribePush()/QSAPI.DataManager.subscribePull()/QSAPI.DataManager.unSubscribePull()/QSAPI.DataManager.unSubscribePush()
	    };
	    Util.getGlobalStatus = function () {
	        return GLOBALSTATE;
	    };
	    Util.checkFund = function (tenforeCode, tenforeType, mType) {
	        if (tenforeType == 8) {
	            if (mType && mType != "FO") {
	                return false;
	            } else {
	                return true;
	            }
	        }
	        return false;
	    };
	    Util.formatNameString = function (str) {
	        if (str == '' || str == null) {
	            return str;
	        }
	        var t = str.split(" ");
	        var t1 = [];
	        for (var i = 0, l = t.length; i < l; i++) {
	            var firstChar = t[i].substring(0, 1);
	            var otherChars = t[i].substring(1);
	            t1.push(firstChar.toUpperCase() + otherChars.toLowerCase());
	        }
	        return t1.join(" ");
	    };
	    Util.getExchByGkey = function (gkey) {
	        return gkey.split(".")[0];
	    };
	    Util.getValidForceDelay = function (forceDelay) {
	        if (typeof forceDelay === 'boolean') {
	            return forceDelay;
	        } else {
	            return false;
	        }
	    };
	    Util.getValidRDType = function (forceDelay, RDType) {
	        var validForceDelay = Util.getValidForceDelay(forceDelay);
	        if (validForceDelay) {
	            return 'D';
	        }
	        return RDType;
	    };
	    Util.copySubscribeOptions = function (target, source) {
	        target["forceDelay"] = source["forceDelay"];
	        target["dataType"] = source["dataType"];
	        target["showSubMarket"] = source["showSubMarket"];
	        target["needSubscribeLevelII"] = source["needSubscribeLevelII"];
	        target["autoRefresh"] = source["autoRefresh"];
	        target["onlypush"] = source["onlypush"];
	    };
	    Util.isAutoRefresh = function (autoRefresh) {
	        if (autoRefresh === true || autoRefresh === false) { //if specified in widget, use this
	            return autoRefresh;
	        } else {
	            return QSAPI.isAutoRefresh();
	        }
	    };
	    Util.isMergePrePost = function (mergePrePost) {
	        if (mergePrePost === true || mergePrePost === false) { //if specified in widget, use this
	            return mergePrePost;
	        } else {
	            return QSAPI.isMergePrePost();
	        }
	    };
	    Util.DataList = function () {
	        this.dataSource = [];
	        this.containter = "";
	        this.template = "";
	        this.reTmp = /{\$(\S+?)}/gm;
	    };
	    Util.DataList.prototype.bind = function () {
	        var html = this.parseData();
	        this.container.append(html);
	    };
	    Util.DataList.prototype.parseData = function () {
	        var _t = this,
	            _d = _t.dataSource,
	            _list = [],
	            _item;
	        for (var i = 0, n = _d.length; i < n; i++) {
	            _item = _d[i];
	            _list.push(_t.parseRowData(_item));
	        }
	        return _list.join("");
	    };
	    Util.DataList.prototype.parseRowData = function (item) {
	        var _t = $.trim(this.template),
	            match, result = _t,
	            s;
	        while (match = this.reTmp.exec(_t)) {
	            s = RegExp.$1;
	            result = result.replace(match[0], item[s] ? item[s] : " ");
	        }
	        return result;
	    };
	    Util.removeDuplication = function (arr, difType) {
	        var obj = {},
	            key;
	        arr = arr || [];
	        for (var i = 0, n = arr.length; i < n; i++) {
	            key = difType ? (arr[i] + "_" + typeof (arr[i])) : arr[i];
	            if (!obj[key]) {
	                obj[key] = true;
	            } else {
	                arr.splice(i, 1);
	                i--;
	                n--;
	            }
	        }
	        return arr;
	    };
	    Util.buildTickerObject = function (symbol, data) {
	        var ticker = QSAPI.TickersCache.get(symbol);
	        if (!ticker) {
	            ticker = new QSAPI.Ticker(symbol);
	        }
	        if (!data.listTicker && data.gkey) {
	            data.listTicker = data.gkey;
	        }
	        for (var field in data) {
	            ticker.setField(field, data[field]);
	        }
	        ticker.setField("valid", true);
	        ticker.setField("isReady", true);
	        QSAPI.TickersCache.add(ticker);
	        QSAPI.TickersCache.save(ticker);
	        return ticker;
	    };
	    Util.replaceWithInStrict = function (factory, paraObj) {
	        (function (factory, paraObj) {
	            if (typeof factory !== 'function' || typeof paraObj !== 'object') {
	                return;
	            }
	            factory(paraObj);
	        })(factory, paraObj);
	    };
	    Util.formatVolume = function ($el, originalValue, decimal, isDefaultActualDecimal) {
	        var $cloneEl = $el.clone().css('visibility', 'hidden').appendTo($el.parent());
	        var $textEl = $('<span>' + originalValue + '</span>').appendTo($cloneEl);
	        var needUnit = false;
	        if ($textEl.width() >= $el.width()) {
	            needUnit = true;
	        }
	        $cloneEl.remove();
	        var formatValue;
	        if (needUnit) {
	            var decimal = (originalValue >= 1000000) ? (decimal || 2) : 0;
	            formatValue = QSAPI.QS_NumFmt.Fmt2(originalValue, true, " ", decimal, true, undefined, isDefaultActualDecimal);
	        } else {
	            formatValue = QSAPI.QS_NumFmt.FmtUnMark(originalValue, true, " ", 0, true);
	        }
	        return formatValue;
	    };
	    Util.containsStaticToken = function (dps) {
	        var staticTokens = QSAPI.DataManager.dataHandler['core'] && QSAPI.DataManager.dataHandler['core'].staticTokens;
	        dps = dps || [];
	        for (var i = 0; i < dps.length; i++) {
	            if ($.inArray(dps[i], staticTokens) > -1) {
	                return true;
	            }
	        }
	        return false;
	    };
	    Util.containsDynamicTokens = function (dps) {
	        var dynamicTokens = QSAPI.DataManager.dataHandler['core'] && QSAPI.DataManager.dataHandler['core'].dynamicTokens;
	        dps = dps || [];
	        for (var i = 0; i < dps.length; i++) {
	            if (dynamicTokens[dps[i]]) {
	                return true;
	            }
	        }
	        return false;
	    };
	    Util.keySize = function (obj) {
	        var size = 0;
	        for (var key in obj) {
	            size++;
	        }
	        return size;
	    };
	    Util.isIOS = function () {
	        return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
	    };
	    Util.getDeviceType = function () {
	        if (navigator.userAgent.match(/Android/i) ||
	            navigator.userAgent.match(/webOS/i) ||
	            navigator.userAgent.match(/iPhone/i) ||
	            navigator.userAgent.match(/iPad/i) ||
	            navigator.userAgent.match(/iPod/i) ||
	            navigator.userAgent.match(/Windows Phone/i) ||
	            navigator.userAgent.match(/windows ce/i) ||
	            navigator.userAgent.match(/symbian/i) ||
	            navigator.userAgent.match(/BlackBerry/i)
	        ) {
	            return 'M';
	        } else {
	            return 'D';
	        }
	    };
	
	    /**
	     * import those function to support fod.
	     */
	    // get those from string.js in marketdata/ 
	    Util.prefixZero = function (num) {
	        if (num < 10) {
	            return '0' + num;
	        }
	        return num;
	    };
	
	    // get those from date.js in marketdata/ 
	    Util.formatRTDateTimeAsDate = function (rtDate, rtTime) {
	        if (typeof (rtDate) === 'string') {
	            var date = rtDate.split('-'),
	                dateObj = new Date();
	            dateObj.setFullYear(date[2]);
	            dateObj.setMonth(parseInt(date[1], 10) - 1);
	            dateObj.setDate(parseInt(date[0], 10));
	            if (typeof (rtTime) === 'string') {
	                var time = rtTime.replace(/(.+)\.\d{3}/, '$1').split(':'),
	                    milliseconds = rtTime.match(/\.\d{3}/) && rtTime.match(/\.\d{3}/)[0].replace('.', '');
	                dateObj.setHours(parseInt(time[0], 10));
	                dateObj.setMinutes(parseInt(time[1] || 0, 10));
	                dateObj.setSeconds(parseInt(time[2] || 0, 10));
	                dateObj.setMilliseconds(parseInt(milliseconds || 0, 10));
	            } else {
	                dateObj.setHours(0);
	                dateObj.setMinutes(0);
	                dateObj.setSeconds(0);
	                dateObj.setMilliseconds(0);
	            }
	            return dateObj;
	        }
	        return null;
	    }
	
	    Util.formatDateString = function (date, type, needSec, split) {
	        if (date && date.getDate) {
	            var dateStr = "";
	            split = split || ((type === 'date') ? "-" : ":");
	            switch (type) {
	                case 'date':
	                    dateStr = [Util.prefixZero(date.getDate()), Util.prefixZero(date.getMonth() + 1), date.getFullYear()].join(split);
	                    break;
	                case 'time':
	                    var dateArr = [Util.prefixZero(date.getHours()), Util.prefixZero(date.getMinutes())]
	                    if (needSec) {
	                        dateArr.push(Util.prefixZero(date.getSeconds()));
	                    }
	                    dateStr = dateArr.join(split);
	                    break;
	            }
	            return dateStr;
	        }
	        return null;
	    }
	
	    Util.formatDateAsInteger = function (date, time) {
	        var date = date.split('-'),
	            time = time.split(':');
	        return parseInt(date[2] + date[1] + date[0] + time[0] + time[1], 10);
	    }
	
	    Util.toHourMinutes = function (minutes) {
	        if (isNaN(minutes)) {
	            return null;
	        }
	        return (Util.prefixZero(Math.floor(minutes / 60)) + ":" + Util.prefixZero(minutes % 60));
	    }
	
	    Util.formatRTDateTimeAsString = function (date, time) {
	        var ret = {};
	        if (date) {
	            date = date.split('-');
	            ret.date = [date[2], date[1], date[0]].join('');
	        }
	        if (time) {
	            if (isNaN(time) && time.indexOf(':') > -1) {
	                time = time.replace(/(.+)\.\d{3}/, '$1').split(':');
	                ret.time = parseInt(time[0], 10) * 3600 + parseInt(time[1], 10) * 60 + parseInt(time[2], 10);
	            } else if (!isNaN(time)) {
	                ret.time = time;
	            }
	        }
	        return ret;
	    }
	
	
	    /**
	     * exchangeInfoLookup functions from fod
	     */
	    Util.findExchange = function (instrument) {
	        /*
	        {
	            delayMinute: 15
	            estOffsetByMinutes: 0
	            gmtOffsetByMinutes: -240
	            micCode: "XNAS"
	            name: "NASDAQ"
	            shortName: "NASDAQ"
	            timezone: "America/New_York"
	        }
	        */
	        var exchangeInfo = QSAPI.DataManager.cfg.exchangeInfo || {};
	        if (exchangeInfo[instrument]) {
	            return exchangeInfo[instrument];
	        }
	        // instrument = instrument.replace(/^(\d+?\.\d+?\.)\S+?$/, '$1*');
	
	        // only a get the exchange market code
	        instrument = instrument.split('.')[0];
	        if (exchangeInfo[instrument]) {
	            return exchangeInfo[instrument];
	        }
	        return undefined;
	    }
	
	    Util.getESTOffsetByMinutes = function (instrument) {
	        var exch = Util.findExchange(instrument);
	        if (exch) {
	            // return exch.estOffset;
	            return exch.estOffsetByMinutes;
	        } else {
	            console.warn('morningstar.marketdata.exchangeInfoLookup', 'Unable to get time offset with EST', {
	                instrument: instrument
	            });
	            return undefined;
	        }
	    }
	
	    Util.getGMTOffsetMinutes = function (instrument) {
	        var exch = Util.findExchange(instrument);
	        if (exch) {
	            // return exch.gmtOffset;
	            return exch.gmtOffsetByMinutes;
	        } else {
	            console.warn('morningstar.marketdata.exchangeInfoLookup', 'Unable to get time offset with GMT', {
	                instrument: instrument
	            });
	            return undefined;
	        }
	    }
	    Util.getESTOffsetMinutes = function (instrument) {
	        var exch = Util.findExchange(instrument);
	        if (exch) {
	            // return exch.gmtOffset;
	            return exch.estOffsetByMinutes;
	        } else {
	            console.warn('morningstar.marketdata.exchangeInfoLookup', 'Unable to get time offset with EST', {
	                instrument: instrument
	            });
	            return undefined;
	        }
	    }
	
	    Util.getDelayMinutes = function (instrument) {
	        var delayMinutes;
	        var exch = Util.findExchange(instrument);
	        if (exch) {
	            // delayMinutes = exch.delay;
	            delayMinutes = exch.delayMinute;
	        }
	        if (typeof delayMinutes === 'undefined') {
	            console.warn('morningstar.marketdata.exchangeInfoLookup', 'Unable to get delay minutes', {
	                instrument: instrument
	            });
	            delayMinutes = 15;
	        } else if (delayMinutes < 0) {
	            console.warn('morningstar.marketdata.exchangeInfoLookup', 'Unable to get valid delay minutes', {
	                instrument: instrument,
	                delayMinutes: delayMinutes
	            });
	            delayMinutes = 15;
	        }
	        return delayMinutes;
	    }
	
	    Util.getOpenClose = function (instrument) {
	        var marketTimeMap = QSAPI.DataManager.cfg.exchangeMarketTimeMap || {};
	        var exch = Util.findExchange(instrument);
	        var instrumentMarketCode = instrument.split('.')[0];
	        var marketTimeArr = marketTimeMap[instrumentMarketCode];
	        if (exch) {
	            return {
	                open: marketTimeArr[0],
	                close: marketTimeArr[1],
	                // preOpen: exch.preOpen,
	                // postClose: exch.postClose
	            };
	        } else {
	            console.warn('morningstar.marketdata.exchangeInfoLookup', 'Unable to get open and close', {
	                instrument: instrument
	            });
	            return {};
	        }
	    }
	
	    Util.convertTimezoneInfo = function (timezoneInfo) {
	        if (!$.isArray(timezoneInfo)) {
	            return;
	        }
	        var timezoneInfoObj = {};
	
	        $.each(timezoneInfo, function (index, item) {
	            var _timezone = item['timezone'] || '';
	            timezoneInfoObj[_timezone] = item;
	        });
	
	        return timezoneInfoObj;
	    }
	    Util.assignCurrency = function (tickerObject, configObj) {
	        var currency = configObj.currencyType === 'tradeCurrency' ? (tickerObject.tradedCurrency || tickerObject.currency) : (tickerObject.currency || tickerObject.tradedCurrency);
	        if (configObj.tradeCurrency && configObj.tradeCurrency.length > 0 && $.inArray(tickerObject.listMarket, configObj.tradeCurrency) > -1) {
	            currency = tickerObject.tradedCurrency || tickerObject.currency;
	        } else if (configObj.listCurrency && configObj.listCurrency.length > 0 && $.inArray(tickerObject.listMarket, configObj.listCurrency) > -1) {
	            currency = tickerObject.currency || tickerObject.tradedCurrency;
	        }
	        return currency;
	    };
	})(QSAPI);
	//for backwards compatible
	String.format = function () {
	    return QSAPI.Util.formatString.apply(this, arguments);
	};
	/**
	 @author Jacye.Ouyang
	 */
	/*Dependence: essential:GOLBAL_DATA; optional: gExchangeMap,gMICExangeMap */
	/* control permission of accessing data from push server the exchanges */
	(function(QSAPI) {
	    'use strict';
	    var $ = QSAPI.$;
	    QSAPI.PermissionChecker = function(permissionData, gExchMap, gMICMap) { // the permission data is from GOLBAL_DATA
	        this.dataPermission = (function() {
	            // 1 Stocks, (some legacy Warrants)
	            // 2 Stock & Index Options
	            // 3 Futures
	            // 4 Future Options
	            // 5 Spots
	            // 6
	            // 7 Corporate Bonds
	            // 8 Mutual Funds
	            // 9 Government Bonds
	            // 10 Indices
	            // 11 Municipal Bonds
	            // 12 
	            // 13 Strategies & Spreads
	            // 14 Statistic Symbols
	            // 15 Monetary Funds
	            // 16 Unspecified Bonds
	            // 17 Unspecified Funds & Certificates
	            // 18 Warrants
	            // 19 Money Market Symbols
	            // 20 Forex Symbols
	            var ret = {};
	            for (var t = 1; t <= 20; t++) {
	                ret[t] = {};
	            }
	            return ret;
	        })();
	        this.modulePermission = {};
	        this.exchangePermission = {};
	        //this.securityTypes = [1, 10, 3, 20, 2, 8, 7, 9, 11, 16, 13, 3, 18]; // 1:ST,ETF and CE; 10:index;
	        //this.subUSExchange = [41,11,9,30,29,33,22,25,56,10];
	        this.MICToExchangeID = {};
	        this._init(permissionData, gExchMap, gMICMap);
	    };
	    QSAPI.PermissionChecker.prototype = {
	        _init: function(permissionData, gExchMap, gMICMap) {
	            var tExchMap = this._initDataPermission(permissionData.DataPermission);
	            this.exchangePermission = tExchMap;
	            this._initACBExchMap(gExchMap, gMICMap);
	        },
	        _initDataPermission: function(data) {
	            var tExchMap = {};
	            if (data && data.length) {
	                var idxs, secList = [],
	                    exch, type, code, idx, ckidx;
	                for (var i = 0; i < data.length; i++) {
	                    secList = data[i]["sec"].replace(/,$/, "").split(",");
	                    exch = data[i]["exch"];
	                    type = data[i]["type"];
	                    code = data[i]["code"].replace(/,$/, "").split(",");
	                    ckidx = data[i]["ckIdx"];
	                    tExchMap[exch] = true;
	                    if (data[i]["sec"] == "-1") { // index, stock,ETF and CE                // not index
	                        for (var t in this.dataPermission) {
	                            if (!this.dataPermission[t][exch]) {
	                                this.dataPermission[t][exch] = {};
	                            }
	                            this.dataPermission[t][exch][type] = {
	                                code: code
	                            };
	                        }
	                    } else { // other security type.
	                        for (var j = 0; j < secList.length; j++) {
	                            if (typeof(secList[j]) == "undefined" || secList[j] == "") continue;
	                            if (!this.dataPermission[secList[j]]) this.dataPermission[secList[j]] = {};
	                            if (!this.dataPermission[secList[j]][exch]) this.dataPermission[secList[j]][exch] = {};
	                            this.dataPermission[secList[j]][exch][type] = { code: code };
	                            if (ckidx == 1) { // check the ticker in one exch.
	                                this.dataPermission[secList[j]][exch][type] = { code: code, idx: data[i]["idx"].replace(/,$/, "").split(",") };
	                            }
	                        }
	                    }
	                }
	            }
	            return tExchMap;
	        },
	        getDataSource: function(exch, sectype, subcode) {
	            var sourcelist = (this.dataPermission[sectype] || {})[exch] || {};
	            if (typeof subcode != "undefined") {
	                for (var sourcetype in sourcelist) {
	                    if ($.inArray(subcode, sourcelist[sourcetype]) != -1) {
	                        return sourcetype;
	                    }
	                }
	            } else {
	                return "R" in sourcelist ? "R" : "D";
	            }
	        },
	        _initACBExchMap: function(gExchMap, gMICMap) {
	            if (typeof(gExchMap) == "undefined" || typeof(gMICMap) == "undefined") return;
	            //gExchMap  tenforExchangeID -> shortName
	            //gMICMap   MIC -> shortName
	            //tExchMap  tenforExchangeID power
	            var shortNameToMIC = {},
	                shortNameToExch = {};
	            for (var ii in gMICMap) { //
	                if (gMICMap[ii] != "") {
	                    shortNameToMIC[gMICMap[ii]] = ii;
	                }
	            }
	            for (var ex in gExchMap) { //
	                if (gExchMap[ex] != "") {
	                    shortNameToExch[gExchMap[ex]] = ex;
	                }
	            }
	            for (var shorN in shortNameToMIC) {
	                if (shortNameToExch[shorN]) {
	                    this.MICToExchangeID[shortNameToMIC[shorN]] = shortNameToExch[shorN];
	                }
	            }
	        },
	        /* provide interface to get the subcodes to subscribe data from Push or Pull server*/
	        /* sType: tenfore security type. tExchange: tenfore exchange code, tTicker: tenfore Ticker,dataType:"D" or "R".*/
	        getSubcodes: function(sType, tExchange, tTicker, dataType) {
	            var subcode = [];
	            if (this.dataPermission[sType] && this.dataPermission[sType][tExchange] && this.dataPermission[sType][tExchange][dataType]) {
	                if (sType == 10 && this.dataPermission[sType][tExchange][dataType]["idx"]) { // index need check the ticker is in the idx list
	                    if ($.inArray(tTicker, this.dataPermission[sType][tExchange][dataType]["idx"]) > -1) {
	                        subcode = this.dataPermission[sType][tExchange][dataType]["code"];
	                    }
	                } else {
	                    subcode = this.dataPermission[sType][tExchange][dataType]["code"];
	                }
	            }
	            return subcode;
	        },
	        /* provide interface for auto-complete component to check permission */
	        /*check the permission using MIC Code such as: XTSE,XNYS,XNAS. For USA security, you can use country id.*/
	        checkACBPermission: function(micCode, exchId, securityType) {
	            var exchEntitlement = false;
	            if (typeof(exchId) != "undefined" && exchId != "") {
	                exchEntitlement = this.exchangePermission[exchId] || false;
	            } else {
	                if (this.MICToExchangeID[micCode]) {
	                    exchEntitlement = this.exchangePermission[this.MICToExchangeID[micCode]] || false;
	                }
	            }
	            if (exchEntitlement && typeof(exchId) != "undefined" && exchId != "" && typeof(securityType) != "undefined" && securityType != "") {
	                return this.dataPermission[securityType][exchId] || false;
	            }
	            return exchEntitlement;
	        },
	        /** provide interface for ticker object check permission **/
	        /** tickerObject is a instance of QSAPI.Ticker**/
	        checkTickerObjectPremission: function(tickerObject) {
	            var eCode = tickerObject.getField("tenforeCode"),
	                type = parseInt(tickerObject.getField("type"), 10),
	                tTicker = tickerObject.getField("tenforeTicker"),
	                exchCode = "";
	            /*check if the ticker is R permission using the real sub exchange*/
	            var subcodes = this.getSubcodes(type, eCode, tTicker, "R");
	            if (subcodes.length > 0) { // Clients will subscribe real-time if they have real-time data permission.
	                if (QSAPI.Util.isCompositiveExch(tickerObject)) { // USA stock,MF,Options and index(use 126 as exchange to subscribe level I data.)
	                    exchCode = tickerObject['CompositeExchangeID'];
	                    if (type != 10 || type != 2) {
	                        subcodes = this.getSubcodes(type, exchCode, tTicker, "R", false); // get USCOMP exchange
	                    }
	                } else {
	                    exchCode = eCode;
	                }
	            } else {
	                if (QSAPI.Util.isCompositiveExch(tickerObject)) { // USA stock,MF, Options and index(use 126 as exchange to subscribe level I data.)
	                    exchCode = tickerObject['CompositeExchangeID'];
	                } else {
	                    exchCode = eCode;
	                }
	                if (type == 2) {
	                    subcodes = this.getSubcodes(type, eCode, tTicker, "D", false); // get individual exchange subcodes when it is option
	                } else {
	                    subcodes = this.getSubcodes(type, exchCode, tTicker, "D", false); // get USCOMP exchange
	                }
	            }
	            if (subcodes.length > 0) {
	                return true;
	            } else {
	                return false;
	            }
	        },
	        getExchIdWithMicCode: function(micCode) {
	            return this.MICToExchangeID[micCode];
	        }
	    };
	})(QSAPI);
	/**
		require lib/jquery.js
		require common.js
		require permissionChecker.js
		require SDKDataManager.js
	*/
	(function(QSAPI) {
	    'use strict';
	    var $ = QSAPI.$;
	
	    QSAPI.TickersCache = {
	        tickers: {}, //mapping of queryKey-> new QSAPI.Ticker
	        add: function(ticker) {
	            this.tickers[ticker['symbol']] = ticker;
	        },
	        get: function(symbol) {
	            var ticker = this.tickers[symbol];
	            return !ticker ? null : ticker;
	        },
	        set: function(symbol, data) {
	            $.extend(this.tickers[symbol], data);
	        },
	        exist: function(symbol) {
	            return !!this.tickers[symbol];
	        },
	        save: function(ticker) {
	            var key = ticker['listTicker'];
	            if (!key) {
	                return;
	            }
	            var _ticker = $.extend({}, ticker);
	            delete _ticker.index;
	            delete _ticker.isReady;
	            delete _ticker.clear;
	            delete _ticker.setData;
	            //delete ticker.hasNews;
	            //delete ticker.clone;
	            //delete ticker.getField;
	            //delete ticker.setField;
	            QSAPI.Cache.setTickerData(key, _ticker);
	        }
	    };
	
	    /**
	     * @param symbolArray
	     *            ["IBM","MSFT"]
	     */
	    QSAPI.TickerFactory = {
	        create: function(symbolArray, callback) {
	            return new QSAPI.Tickers(symbolArray, callback);
	        }
	    };
	
	    QSAPI.Tickers = function(symbolArray, callback) {
	        this.id = QSAPI.Util.IdProducer.getId();
	        this.callbackPool = [];
	        callback = callback || {};
	        if (!_checkParameter(symbolArray)) {
	            _throwSymbolArrayError();
	        }
	
	        this.config = {
	            groupSymbolQuantity: 30,
	            idService: QSAPI.getQSPath() + 'getids.jsp?cb=?&symbol=',
	            extraIdService: QSAPI.getQSPath() + 'ra/retrieveExtraId?jsoncallback=?&tickers='
	        };
	
	        var config = this.config;
	        var tickerList = _createTickers(symbolArray);
	        var needResponseTimes = 0;
	        var responsedTimes = 0;
	        var cdm = QSAPI.DataManager;
	        var timer = null;
	        var uniqueSymbol = _getUniqueAndNoCacheSymbol();
	        var uniqueSymbolTickerList = _createTickers(uniqueSymbol);
	        this.tickerList = tickerList;
	        _putTickersToCache(uniqueSymbolTickerList);
	        _loadData(uniqueSymbol);
	        timer = setInterval(function() {
	            if (needResponseTimes > 0 && needResponseTimes == responsedTimes) {
	                clearInterval(timer);
	                _updateToReady(uniqueSymbol);
	            } else if (needResponseTimes == 0) {
	                clearInterval(timer);
	            }
	        }, 100);
	        if (callback.onSuccess && typeof(callback.onSuccess) == 'function') {
	            this.ready(callback.onSuccess);
	        }
	        /*************************************************************** */
	        /* QSAPI.Tickers private method */
	        /*************************************************************** */
	
	        function _putTickersToCache(uniqueSymbolTickerList) {
	            for (var i = 0, len = uniqueSymbolTickerList.length; i < len; i++) {
	                if (!QSAPI.TickersCache.exist(uniqueSymbolTickerList[i]['symbol'])) {
	                    QSAPI.TickersCache.add(uniqueSymbolTickerList[i]);
	                }
	            }
	        }
	
	        function _getUniqueAndNoCacheSymbol() {
	            var map = {};
	            var uniqueSymbol = [];
	            var symbol = null;
	            for (var i = 0, len = tickerList.length; i < len; i++) {
	                symbol = tickerList[i]['symbol'];
	                if (!QSAPI.TickersCache.exist(symbol) && !map[symbol]) {
	                    map[symbol] = true;
	                    uniqueSymbol.push(symbol);
	                }
	            }
	            return uniqueSymbol;
	        }
	
	        function _loadData(uniqueSymbol) {
	            var symbol, ITFundSymbol = [],
	                pfsymbols = [],
	                tfsymbols = [],
	                symbols = [],
	                csymbols = [],
	                posymbol = [],
	                benchmarksymbols = [],
	                length = uniqueSymbol.length;
	            for (var i = 0; i < length; i++) {
	                symbol = uniqueSymbol[i];
	                if (symbol.indexOf("]") >= 0) {
	                    ITFundSymbol.push(symbol);
	                    continue;
	                }
	                var preSy = uniqueSymbol[i].substring(0, 5);
	                if ((preSy == "VAUSA" || preSy == "SPUSA") && symbol.length >= 10) { // mutual fund for Princal.
	                    pfsymbols.push(symbol);
	                    continue;
	                }
	                if (/^\d+.\d+.MSTATS_\S+$/.test(symbol)) { // only support 126:1:IBM.
	                    tfsymbols.push(symbol);
	                    continue;
	                }
	                if (symbol.split("|").length == 4) {
	                    csymbols.push(symbol);
	                    continue;
	                }
	                if (/.+;PO/.test(symbol)) {
	                    posymbol.push(symbol);
	                    continue;
	                }
	                if (/.+;benchmark.+;/.test(symbol)) {
	                    benchmarksymbols.push(symbol);
	                    continue;
	                }
	                symbols.push(symbol);
	            }
	            _loadBenchmarksData(benchmarksymbols);
	            _loadTSFundData(ITFundSymbol);
	            _loadPFundData(pfsymbols);
	            _loadCategoryData(csymbols);
	            _loadPortfolioData(posymbol);
	            _loadNormalData(symbols);
	        }
	
	        function _loadPortfolioData(symbols) {
	            if (typeof(QSAPI.mockExchangeCode) == "undefined") {
	                QSAPI.mockExchangeCode = "9999";
	            }
	            for (var i = 0, l = symbols.length; i < l; i++) {
	                var symbolArray = symbols[i].split("|");
	                var symbol = symbolArray[0].split(";");
	                var portfolioId = symbol[0];
	                var name;
	                if (symbolArray.length > 1) {
	                    name = symbolArray[1];
	                }
	                //xxxxxxx;PO|Name
	                var ticker = QSAPI.TickersCache.get(symbols[i]);
	                if (ticker) {
	                    ticker["valid"] = true;
	                    //ticker["isReady"] = true;
	                    ticker["ticker"] = portfolioId;
	                    ticker["clientTicker"] = portfolioId;
	                    ticker["Name"] = name || portfolioId;
	                    ticker["performanceId"] = portfolioId;
	                    ticker["secId"] = portfolioId;
	                    ticker["noPID"] = false;
	                    ticker["gkey"] = portfolioId;
	                    ticker["mType"] = symbol[1];
	                    ticker["gkey"] = (QSAPI.mockExchangeCode + "_PO") + ":" + portfolioId;
	                    ticker["listTicker"] = ticker["gkey"];
	                }
	            }
	        }
	
	        function _loadCategoryData(uniqueSymbol) {
	            needResponseTimes++;
	            _requestData(config.extraIdService, uniqueSymbol, function(data) {
	                data = data || [];
	                if (typeof(QSAPI.mockExchangeCode) == "undefined") {
	                    QSAPI.mockExchangeCode = "9999";
	                }
	                for (var i = 0, l = data.length; i < l; i++) {
	                    var symbol = data[i]["ORIGIN"];
	                    var ticker = QSAPI.TickersCache.get(symbol);
	                    if (ticker) {
	                        ticker["country"] = data[i]["XI018"] || data[i]["OS01V"] || "",
	                            ticker["valid"] = data[i]["AggID"] || data[i]["OS00I"] ? true : false;
	                        //ticker["isReady"] = true;
	                        ticker["categoryId"] = data[i]["ORIGIN"].split("|")[1];
	                        ticker["Name"] = data[i]["OS01W"] || data[i]["FCName"];
	                        ticker["ticker"] = data[i]["Ticker"] || ticker["categoryId"] || ticker["Name"];
	                        ticker["clientTicker"] = data[i]["ClientTicker"] || data[i]["OS01W"] || data[i]["FCName"];
	                        ticker["performanceId"] = data[i]["OS06Y"] || data[i]["AggID"] || "";
	                        ticker["secId"] = data[i]["OS00I"] || data[i]["AggID"] || "";
	                        ticker["noPID"] = ticker["performanceId"].length > 0 || ticker["secId"].length > 0 ? true : false;
	                        ticker["gkey"] = (QSAPI.mockExchangeCode + (ticker["categoryId"] ? "_CA" : "")) + ":" + (ticker["categoryId"] || ticker["secId"]);
	                        ticker["listTicker"] = ticker["gkey"];
	                    }
	                }
	                responsedTimes++;
	            });
	        }
	
	        function _loadNormalData(uniqueSymbol) {
	            needResponseTimes++;
	            _requestData(config.idService, uniqueSymbol, function(result) {
	                var data = _fillWithInValidTickers((result || []), uniqueSymbol);
	                _putDataToCacheTickers(data);
	                responsedTimes++;
	            });
	        }
	
	        function _requestData(url, uniqueSymbol, callback) {
	            var symbols = [];
	            for (var i = 0; i < uniqueSymbol.length; i++) {
	                if (/^\d+:\d+:\S+$/.test(uniqueSymbol[i])) { // only support 126:1:IBM.
	                    var temp = uniqueSymbol[i].split(/:/);
	                    if (temp.length >= 3) {
	                        symbols.push(temp[0] + ":" + temp[2]);
	                    }
	                } else {
	                    symbols.push(uniqueSymbol[i]);
	                }
	            }
	            var len = symbols.length;
	            if (len == 0) {
	                callback([]);
	            } else {
	                var maxCount = config.groupSymbolQuantity;
	                if (len > maxCount) {
	                    var subSymbolStr = [];
	                    var c = Math.ceil(len / maxCount);
	                    var cbNum = 0;
	                    var returnArray = [];
	                    for (var i = 0; i < c; i++) {
	                        subSymbolStr.push(symbols.slice(maxCount * i, maxCount * (i + 1)).join(','));
	                        QSAPI.Util.ajax({
	                            url: url + subSymbolStr[i],
	                            type: "get",
	                            data: null,
	                            dataType: "json",
	                            success: function(data) {
	                                cbNum++;
	                                returnArray = returnArray.concat(data.Records || data.data || []); //merge the return
	                                if (cbNum >= c) {
	                                    callback(returnArray);
	                                }
	                            },
	                            error: function(data) {
	                                callback([]);
	                            }
	                        });
	                    }
	                } else {
	                    QSAPI.Util.ajax({
	                        url: url + symbols.join(','),
	                        type: "get",
	                        data: null,
	                        dataType: "json",
	                        success: function(data) {
	                            callback(data.Records || data.data || []);
	                        },
	                        error: function(data) {
	                            callback([]);
	                        }
	                    });
	                }
	            }
	        }
	
	        function _fillWithInValidTickers(data, symbols) {
	            var passTickers = [],
	                tickers = [],
	                queryKey, re = /^(\d+?)\.(\d+?)\.(\S+?)$/;
	            for (var i = 0, len = data.length; i < len; i++) {
	                queryKey = data[i]['QueryKey'];
	                passTickers.push(queryKey);
	            }
	            for (var i = 0, n = symbols.length; i < n; i++) {
	                if ($.inArray(symbols[i], passTickers) == -1) {
	                    if (!re.test(symbols[i])) {
	                        continue;
	                    }
	                    var arr = symbols[i].split(".");
	                    tickers.push({
	                        QueryKey: symbols[i],
	                        TenforeInfo: [arr[2], arr[0], arr[1], arr[0]],
	                        Ticker: arr[2],
	                        PID: "",
	                        MockTicker: true
	                    });
	                }
	            }
	            return data.concat(tickers);
	        }
	
	        function _loadBenchmarksData(symbols) {
	            for (var i = 0, l = symbols.length; i < l; i++) {
	                var symbol, name;
	                var symbolInfo = symbols[i].split(/;benchmark.+;/);
	                if (symbolInfo.length > 1) {
	                    symbol = symbolInfo[0];
	                    name = symbolInfo[1];
	                    var ticker = QSAPI.TickersCache.get(symbols[i]);
	                    if (ticker) {
	                        ticker["valid"] = true;
	                        ticker["symbol"] = symbols[i];
	                        ticker["ticker"] = symbol;
	                        ticker["clientTicker"] = name;
	                        ticker["performanceId"] = symbol;
	                        ticker["benchmarkType"] = symbols[i].match(/;benchmark_(.+);/)[1];
	                        ticker["secId"] = symbol;
	                        ticker["type"] = 8;
	                        ticker["mType"] = 'FO'; // in this case, it will get 
	                        ticker["noPID"] = false;
	                        ticker["gkey"] = symbol;
	                        ticker["Name"] = name;
	                        ticker["tenforeTicker"] = (QSAPI.mockExchangeCode + "_benchmark") + ":" + symbol;
	                    }
	                }
	            }
	        }
	
	        function _loadTSFundData(symbols) {
	            var idMap = {},
	                ids = [];
	            for (var i = 0, l = symbols.length; i < l; i++) {
	                var symbol = symbols[i],
	                    rtsymbol;
	                if (symbol.indexOf("@_") > -1) {
	                    symbol = symbols[i].split("@_")[0];
	                    rtsymbol = symbols[i].split("@_")[1];
	                }
	                var uniqueSymbol = symbol,
	                    name,
	                    clientTicker = symbol;
	                //FOUSA06CGQ]7]0]IXALL$$ALL|MSID|FundName
	                //FOUSA06CGQ]7]0]IXALL$$ALL|MSID|FundName@_Ticker
	                var infoArr, idType, securityToken;
	                if (symbol.indexOf("|") > -1) {
	                    infoArr = symbol.split('|');
	                    if (infoArr.length >= 3) {
	                        name = infoArr.pop();
	                    }
	                    idType = infoArr.length > 1 ? infoArr[1] : "Morningstar";
	                    securityToken = infoArr[0];
	                    if (!name || rtsymbol) {
	                        var id = rtsymbol || symbol.split(']')[0];
	                        ids.push(id);
	                        idMap[id] = {
	                            symbol: symbols[i],
	                            name: name,
	                            securityToken: infoArr[0],
	                            idType: idType,
	                        };
	                    }
	                }
	
	                if (symbol.indexOf("]") > -1) {
	                    var ls = symbol.split(']');
	                    uniqueSymbol = ls[0];
	                    clientTicker = ls[0].length > 10 ? ls[0].substring(0, 10) : ls[0];
	                }
	
	                var ticker = QSAPI.TickersCache.get(symbols[i]);
	                if (ticker) {
	                    ticker["valid"] = true;
	                    //ticker["isReady"] = true;
	                    ticker["ticker"] = uniqueSymbol;
	                    ticker["clientTicker"] = clientTicker;
	                    ticker["Name"] = name;
	                    ticker["performanceId"] = uniqueSymbol;
	                    ticker["secId"] = uniqueSymbol;
	                    ticker["type"] = 8;
	                    ticker["exch"] = "MSIT";
	                    //ticker["tenforeCode"] = "21";
	                    //ticker["tenforeTicker"] = uniqueSymbol;
	                    ticker["noPID"] = false;
	                    ticker["gkey"] = uniqueSymbol;
	                    ticker["securityToken"] = securityToken || symbol;
	                    ticker["listTicker"] = ticker["gkey"];
	                    ticker["tsfund"] = true;
	                    ticker["idType"] = idType || "Morningstar";
	                }
	            }
	            if (ids.length > 0) {
	                needResponseTimes++;
	                _requestData(config.idService, ids, function(result) {
	                    var data = _fillWithInValidTickers((result || []), ids);
	                    for (var i = 0, l = data.length; i < l; i++) {
	                        var symbolInfo = idMap[ids[i]];
	                        data[i].QueryKey = symbolInfo.symbol;
	                        _putDataToCacheTickers([data[i]]);
	                        var ticker = QSAPI.TickersCache.get(symbolInfo.symbol);
	                        ticker.Name = symbolInfo.name || ticker.Name || ids[i];
	                        ticker.securityToken = symbolInfo.securityToken;
	                        ticker.idType = symbolInfo.idType;
	                    }
	                    responsedTimes++;
	                });
	            }
	
	        }
	
	        function _loadPFundData(symbols) { // Princal Mutual Fund.
	            for (var i = 0, l = symbols.length; i < l; i++) {
	                var symbol = symbols[i],
	                    uniqueSymbol = symbol,
	                    name = symbol;
	                if (symbol.indexOf("_") > -1) {
	                    var ls = symbol.split('_');
	                    uniqueSymbol = ls[0];
	                    name = ls[1].length > 10 ? ls[1].substring(0, 10) : ls[1];
	                }
	                var ticker = QSAPI.TickersCache.get(symbol);
	                if (ticker) {
	                    ticker["valid"] = true;
	                    //ticker["isReady"] = true;
	                    ticker["ticker"] = uniqueSymbol;
	                    ticker["clientTicker"] = name;
	                    ticker["performanceId"] = uniqueSymbol;
	                    ticker["secId"] = uniqueSymbol;
	                    ticker["type"] = 8;
	                    ticker["exch"] = "XNAS";
	                    ticker["tenforeCode"] = "21";
	                    ticker["tenforeTicker"] = uniqueSymbol;
	                    ticker["noPID"] = false;
	                    ticker["gkey"] = uniqueSymbol;
	                    ticker["listTicker"] = ticker["gkey"];
	                }
	            }
	        }
	
	        function _updateToReady(symbolArray) {
	            for (var i = 0, len = symbolArray.length; i < len; i++) {
	                QSAPI.TickersCache.get(symbolArray[i])["isReady"] = true;
	            }
	        }
	
	        function _clearTenforeInfo(d) {
	            var tenforInfo = d.TenforeInfo || [];
	            var isEmpty = true;
	            for (var i = 0, l = tenforInfo.length; i < l; i++) {
	                if (tenforInfo[i] != "") {
	                    isEmpty = false;
	                    break;
	                }
	            }
	            if (isEmpty) {
	                d.TenforeInfo = null;
	                delete d.TenforeInfo;
	            }
	        }
	
	        function _putDataToCacheTickers(data) {
	            var d, tInfo, tempSymbol;
	            for (var i = 0, len = data.length; i < len; i++) {
	                d = data[i];
	                _clearTenforeInfo(d);
	                // QS-5145 hard code for foreax
	                if (d.TenforeInfo && d.TenforeInfo.length > 3 && d.TenforeInfo[1] != d.TenforeInfo[3] && d.TenforeInfo[1] == "240") {
	                    d.TenforeInfo[1] = "245";
	                }
	                // QS-5309 one of TenforeInfo[3]/TenforeInfo[1] is empty, they will populate each other
	                if (d.TenforeInfo && d.TenforeInfo.length > 3 && d.TenforeInfo[1] != d.TenforeInfo[3] && !d.TenforeInfo[3]) {
	                    d.TenforeInfo[3] = d.TenforeInfo[1];
	                }
	                if (d.TenforeInfo && d.TenforeInfo.length > 3 && d.TenforeInfo[1] != d.TenforeInfo[3] && !d.TenforeInfo[1]) {
	                    d.TenforeInfo[1] = d.TenforeInfo[3];
	                }
	                var queryKey = d['QueryKey'];
	                var ticker = QSAPI.TickersCache.get(queryKey);
	                if (!ticker && d['Exch']) {
	                    ticker = QSAPI.TickersCache.get(d['Exch'] + ":" + queryKey);
	                }
	                tInfo = d.TenforeInfo || [];
	                if (!ticker && tInfo.length >= 3) {
	                    tempSymbol = tInfo[1] + "." + tInfo[2] + "." + tInfo[0];
	                    for (var j = 0, m = uniqueSymbol.length; j < m; j++) {
	                        if (uniqueSymbol[j] == tempSymbol) {
	                            ticker = QSAPI.TickersCache.get(tempSymbol);
	                            break
	                        }
	                    }
	                }
	                if (!ticker && tInfo.length > 3) {
	                    var exch = queryKey.split(":");
	                    if (!isNaN(exch[0])) {
	                        var exchs = [tInfo[1]].concat(tInfo[1].split(" "));
	                        if ($.inArray(exch[0], exchs) > -1) {
	                            ticker = QSAPI.TickersCache.get(exch[0] + ":" + tInfo[2] + ":" + tInfo[0]);
	                        }
	                    }
	                }
	                if (ticker) {
	                    ticker.setData(d);
	                }
	            }
	        }
	
	        /**
	         * @param symbolArray
	         *            return boolean
	         */
	        function _checkParameter(symbolArray) {
	            if ($.isArray(symbolArray)) {
	                for (var i = 0; i < symbolArray.length; i++) {
	                    if (typeof(symbolArray[i]) != "string") {
	                        return false;
	                    } else {
	                        symbolArray[i] = symbolArray[i];
	                    }
	                }
	            } else {
	                return false;
	            }
	            return true;
	        }
	
	        function _createTickers(symbolArray) {
	            var tList = [],
	                ticker = null;
	            for (var i = 0, len = symbolArray.length; i < len; i++) {
	                //symbolArray[i]=symbolArray[i].replace(/\./g,":");
	                ticker = new QSAPI.Ticker(symbolArray[i]);
	                tList.push(ticker);
	            }
	            return tList;
	        }
	
	        function _throwSymbolArrayError() {
	            throw new Error(
	                "Error from executing QSAPI.Tickers(symbolArray).Parameter is wrong, example: new QSAPI.Tickers(['IBM','MSFT'])");
	        }
	    };
	
	    QSAPI.Tickers.prototype = {
	        ready: function(callback) {
	            var tickerList = this.tickerList;
	            var timer = null;
	            var callbackPool = this.callbackPool;
	            if (typeof(callback) == 'function') {
	                callbackPool.push(callback);
	            }
	            var allReady = _isReady();
	            if (!allReady) {
	                timer = setInterval(function() {
	                    allReady = _isReady();
	                    if (allReady) {
	                        clearInterval(timer);
	                        _executeCallback();
	                    }
	                }, 100);
	            } else {
	                _executeCallback();
	            }
	
	            function _cloneDataFromCache() {
	                for (var i = 0; i < tickerList.length; i++) {
	                    var t = tickerList[i];
	                    var srcTicker = QSAPI.TickersCache.tickers[tickerList[i].symbol];
	                    $.extend(t, srcTicker, {
	                        'index': t.index,
	                        'symbol': t.symbol
	                    });
	                }
	            }
	
	            function _isReady() {
	                for (var i = 0; i < tickerList.length; i++) {
	                    if (!QSAPI.TickersCache.get(tickerList[i]['symbol'])['isReady']) {
	                        return false;
	                    }
	                }
	                _cloneDataFromCache();
	                return true;
	            }
	
	            function _executeCallback() {
	                while (callbackPool.length > 0) {
	                    callbackPool[0](tickerList);
	                    callbackPool.splice(0, 1);
	                }
	            }
	        },
	        getTickerList: function() {
	            return this.tickerList;
	        }
	    };
	    /**
	     * 
	     * @param gKey
	     *            Symbol/PerformaceId
	     * @returns {QSAPI.ticker}
	     */
	    QSAPI.Ticker = function(symbol) {
	        var _getCTZDate = function(day) {
	            var date = new Date();
	            return new Date(date.getTime() + (date.getTimezoneOffset()) * 60 * 1000 + day * 1000 * 60 * 60 * 24);
	        };
	        var _getCTZDateRange = function() {
	            //var sDate = _getCTZDate(0);
	            var eDate = _getCTZDate(0);
	            var years = eDate.getFullYear(),
	                months = (eDate.getMonth() + 1) < 10 ? "0" + (eDate.getMonth() + 1) : (eDate.getMonth() + 1),
	                days = eDate.getDate() < 10 ? "0" + eDate.getDate() : eDate.getDate();
	            var yeare = eDate.getFullYear(),
	                monthe = (eDate.getMonth() + 1) < 10 ? "0" + (eDate.getMonth() + 1) : (eDate.getMonth() + 1),
	                daye = eDate.getDate() < 10 ? "0" + eDate.getDate() : eDate.getDate();
	            return [
	                QSAPI.Util.formatString("{0}-{1}-{2}T00:00:00", years, months, days),
	                QSAPI.Util.formatString("{0}-{1}-{2}T{3}:{4}:00", yeare, monthe, daye, eDate.getHours(), eDate.getMinutes())
	            ];
	        };
	        return {
	            'index': QSAPI.Util.IdProducer.getId(),
	            'symbol': symbol,
	            'isReady': false,
	            'valid': false,
	            clone: function() {
	                return $.extend({}, this, { 'index': QSAPI.Util.IdProducer.getId() });
	            },
	            hasNews: function(symbols) {
	                var range = _getCTZDateRange();
	                var symbol = this.symbol;
	                var idType = '';
	                if (this.performanceId && this.performanceId.length > 0) {
	                    symbol = this.performanceId, idType = "SC";
	                } else if (/^\d+\:\S+/.test(symbol)) {
	                    idType = "ES";
	                } else if (/^[A-Za-z]+\:\S+/.test(symbol)) {
	                    idType = "ES";
	                } else {
	                    idType = "SC"
	                }
	                var returnData = false;
	                QSAPI.Util.getData({
	                    url: QSAPI.getQSPath() + "getNewsResultCount.jsp",
	                    async: false,
	                    data: {
	                        productCode: "QS",
	                        //fromDate:range[0],
	                        //toDate:range[1],
	                        currentDay: "1",
	                        symbols: symbols || symbol,
	                        idType: idType,
	                        ntSourceCode: "DJNMNDJEN,DJNMNDJBN,DJNMNBW,DJNMNPR,DJNMNCW,DJNMNCNCN,DJNMNPMZN,DJNMNMKTW,MRKTWIREUSPR____"
	                    }
	                }, {
	                    onSuccess: function(data) {
	                        if (data && data.Count) {
	                            returnData = true;
	                        } else {
	                            returnData = false;
	                        }
	                    },
	                    onFailure: function() {
	                        returnData = false;
	                    }
	                });
	                return returnData;
	            },
	            setData: function(data) {
	                var rs = data;
	                _fillSType(rs);
	                _fillSecId(rs);
	                var tenforInfo = rs.TenforeInfo || [];
	                var ternforeCode = rs['TenforeInfo'] && rs['TenforeInfo'].length ? rs['TenforeInfo'][rs['TenforeInfo'].length > 3 ? 3 : 1] : null;
	                var listMarket = rs['TenforeInfo'] ? rs['TenforeInfo'][1] : null;
	                var tenforeTicker = tenforInfo.length > 0 ? tenforInfo[0] : (rs.PID || rs.SID);
	                var gkey, listTicker;
	                var secType = _toStockTypeCode(rs.Type, tenforInfo.length > 2 ? tenforInfo[2] : null);
	                if (tenforInfo && tenforInfo.length > 0) {
	                    if (tenforInfo.length > 2 && tenforInfo[2] == "20" && rs.TenforeSubMarkets) { //forex
	                        ternforeCode = rs.TenforeSubMarkets;
	                    }
	                    if (!listMarket) {
	                        listMarket = ternforeCode;
	                    }
	                    gkey = QSAPI.Util.generateGKey(ternforeCode, secType, tenforeTicker, rs.Type);
	                    listTicker = QSAPI.Util.generateGKey(listMarket, secType, tenforeTicker, rs.Type);
	                } else {
	                    if (typeof(QSAPI.mockExchangeCode) == "undefined") {
	                        QSAPI.mockExchangeCode = "9999";
	                    }
	                    var ticker = $.trim(rs.Ticker);
	                    tenforInfo = [(ticker.length > 0 ? ticker : (rs.PID || rs.SID)), QSAPI.mockExchangeCode + (typeof(rs.Type) != "undefined" ? "_" + rs.Type : "")];
	                    ternforeCode = tenforInfo[1];
	                    if (!listMarket) {
	                        listMarket = ternforeCode;
	                    }
	                    tenforeTicker = tenforInfo[0];
	                    gkey = QSAPI.Util.generateGKey(listMarket, secType, tenforeTicker);
	                    listTicker = QSAPI.Util.generateGKey(listMarket, secType, tenforeTicker);
	                }
	                var mType = _convertMTypeFromTenforeType(secType, rs.Type, ternforeCode);
	                var performanceId = rs.PID == "" ? ((secType == 2) ? tenforInfo[0] : (ternforeCode + "." + secType + "." + tenforInfo[0])) : rs.PID;
	                var sId = rs.SID == "" ? ((secType == 2) ? tenforInfo[0] : (ternforeCode + "." + secType + "." + tenforInfo[0])) : rs.SID;
	                var matchers = performanceId.match(/^\d+[.:]20[.:](\S+)$/);
	                if (matchers && matchers.length > 1) { //fix forex PID issue
	                    rs.PID = rs.SID = sId = performanceId = "CU$$" + matchers[1].replace("FLIT", "");
	                }
	                var exchangeMap = QSAPI.DataManager._getExchangeMap() || {};
	                $.extend(this, {
	                    'mType': mType,
	                    'type': secType,
	                    'gkey': gkey,
	                    'MSTicker': gkey,
	                    'listTicker': listTicker, //ticker with list market
	                    'tenforeCode': ternforeCode,
	                    'Exchange': exchangeMap[listMarket] || listMarket,
	                    'listMarket': listMarket,
	                    'S214': listMarket,
	                    'tenforeTicker': rs['TenforeInfo'] ? rs['TenforeInfo'][0] : null,
	                    'companyId': _getCompanyId(mType, rs),
	                    'secId': sId,
	                    'performanceId': performanceId,
	                    'msexch': _getMSExch(rs['Exch']),
	                    'exch': rs['Exch'],
	                    'MIC-CODE': rs['Exch'],
	                    'country': rs['Region'],
	                    'ISIN': rs.ISIN,
	                    'clientTicker': rs.ClientTicker || rs.Ticker || tenforInfo[0],
	                    'ticker': rs.Ticker || tenforInfo[0],
	                    'Name': rs.OS01W || rs.Name,
	                    'tradedCurrency': rs['TransactionMoney'] || '',
	                    'currency': rs['Currency'] || '',
	                    'InitialDate': rs.InitialDate ? rs.InitialDate : "",
	                    'CUSIP': rs.CUSIP ? rs.CUSIP : "",
	                    //'isReady': true,
	                    'valid': true,
	                    'Indicator': 'Regular',
	                    'categoryId': rs.OF012,
	                    "relativeRiskBenchmarkId": rs.OS384,
	                    "mstarIndexName": rs.MstarIndex,
	                    "mstarIndexId": rs.MstarIndexId,
	                    "prospectusBenchmarkId": rs.PrimaryProspectusBenchmarkId,
	                    "prospectusBenchmarkName": rs.PrimaryProspectusBenchmark,
	                    "clientSupport": rs.Ext2 == "yes",
	                    'noPID': (!performanceId || performanceId == '') && (!sId || sId == '') ? true : false,
	                    'Status': rs.Status,
	                    'CompositeExchangeID': rs.CompositeExchangeID,
	                    'sectorCode': rs['IT152'] ? rs['IT152'] : null
	                });
	                if (sId || performanceId) {
	                    this['securityToken'] = (sId || performanceId) + ']';
	                    this['idType'] = 'Morningstar';
	                }
	                if (rs.MockTicker) {
	                    this["mockTicker"] = true;
	                }
	                if (this.type == 3) this.noPID = false; // commodity future's PID is tenfore information.
	                if (rs.FundData) {
	                    $.extend(this, {
	                        'IMA_RDate': rs.FundData.IMA_RDate ? rs.FundData.IMA_RDate : "",
	                        'MStar_RDate': rs.FundData.MStar_RDate ? rs.FundData.MStar_RDate : ""
	                    });
	                }
	                if (secType == 2) { // options
	                    $.extend(this, {
	                        optiontype: rs.OptionType,
	                        rootsymbol: rs.RootSymbol,
	                        expirydate: _formatDateString(rs.ExpiredDate),
	                        strikeprice: rs.StrikePrice
	                    });
	                } else if (secType == 7) { //Debenture
	                    this.rootsymbol = rs.RootSymbol;
	                }
	                if (rs.Type == "FE" && rs.EtfData != null && rs.EtfData.IIV &&
	                    rs.EtfData.IIV.TenforeInfo != null) { // IIV for ETF ticker.
	                    var etf = rs.EtfData.IIV;
	                    var eGkey = QSAPI.Util.generateGKey(listMarket, secType, etf.TenforeInfo[0], rs.Type);
	                    var etfData = {
	                        'checked': true,
	                        'tenforeCode': ternforeCode,
	                        'tenforeTicker': etf.TenforeInfo[0],
	                        'mType': 'FE',
	                        'type': _toStockTypeCode(etf.Type, etf.TenforeInfo[2]),
	                        'performanceId': performanceId + "IIV",
	                        'secId': sId,
	                        'MIC-CODE': etf.Exch,
	                        'exch': etf.Exch,
	                        'ticker': etf.Ticker,
	                        'country': etf.Region,
	                        'Name': etf.Name,
	                        'eGkey': eGkey,
	                        'gkey': eGkey,
	                        'ready': true,
	                        'valid': true
	                    };
	                    var ticker = new QSAPI.Ticker(etf.Ticker);
	                    $.extend(ticker, etfData);
	                }
	
	                function _getCompanyId(mType, rs) {
	                    if (mType == "FE" || mType == "FC" || mType == "FO") {
	                        return rs["DAS02"];
	                    }
	                    return rs["CFID"];
	                }
	
	                function _getMSExch(exch) {
	                    if (typeof exch == "undefined" || exch == "") {
	                        return "";
	                    }
	                    var mic = QSAPI.DataManager._getMICExangeMap();
	                    if (mic == null) return "";
	                    return mic[exch] || "";
	                }
	
	                function _formatDateString(str) {
	                    if (str && str.length >= 8) {
	                        var t = [];
	                        t.push(str.slice(0, 4)), t.push(str.slice(4, 6)), t.push(str.slice(6, 8));
	                        return t.join("-");
	                    }
	                    return "";
	                }
	
	                function _fillSType(rs) {
	                    var tInfo = rs.TenforeInfo || [];
	                    if (!rs.Type && tInfo.length >= 3) {
	                        if (tInfo[2] == '1') {
	                            rs.Type = 'ST';
	                        } else if (tInfo[2] == '10') {
	                            rs.Type = 'XI';
	                        }
	                    }
	                }
	
	                function _fillSecId(rs) { //hardcode to fill the secid with tenfore ticker for 28.10.xxxx
	                    var tInfo = rs.TenforeInfo || [];
	                    if (!rs.SID && tInfo.length >= 3) {
	                        if ((tInfo[1] == "28" || tInfo[1] == "27") && tInfo[2] == "10") {
	                            rs.SID = tInfo[0];
	                        }
	                    }
	                }
	
	                function _toStockTypeCode(ty, tenforeType) {
	                    if (typeof(tenforeType) != "undefined" && tenforeType != null && tenforeType != "" && !isNaN(tenforeType)) {
	                        return parseInt(tenforeType, 10);
	                    }
	                    if (ty == null)
	                        return 1;
	                    var t = ty.toUpperCase();
	                    if (t == "ST") { // stock
	                        return 1;
	                    } else if (t == "XI") { // Index
	                        return 10;
	                    } else if (t == "FE" || t == "FC") { // ETF
	                        return 1;
	                    } else if (t == "FO" || ty == "SA") { // FC: close fund, FO: open fund.
	                        return 8;
	                    } else {
	                        return 1;
	                    }
	                }
	
	                // according the tenfore sec type to generate mstar sectype
	                function _convertMTypeFromTenforeType(secType, mType, tenforeExch) {
	                    var result = null;
	                    if (typeof(mType) != 'undefined' && mType != null && mType != '' && mType != '--') {
	                        result = mType;
	                    } else if (QSAPI.Util.isCanadaExch(tenforeExch) && secType == 8) { // hard code 
	                        result = "ST";
	                    } else {
	                        var mType = "ST";
	                        switch (secType) {
	                            case 10:
	                                mType = "XI";
	                                break;
	                            case 8:
	                                mType = 'FC';
	                                break;
	                            case 7:
	                            case 11:
	                            case 16:
	                                mType = "Bond";
	                                break;
	                            case 13:
	                                mType = "Spread";
	                                break;
	                            case 2:
	                                mType = "Options";
	                                break;
	                            case 3:
	                                mType = "FU";
	                                break;
	                            case 20:
	                                mType = "FX";
	                                break;
	                        }
	                        result = mType;
	                    }
	                    return result;
	                }
	                QSAPI.TickersCache.save(this);
	            },
	            getField: function(field) {
	                return !field ? this : this[field];
	            },
	            setField: function(field, value) {
	                if (field) {
	                    this[field] = typeof(value) == "undefined" ? null : value;
	                }
	            }
	        };
	    };
	})(QSAPI);
	(function(QSAPI) {
	    'use strict';
	    var $ = QSAPI.$;
	    QSAPI.Task = (function() {
	        var taskTimer = {};
	        var taskObj = {};
	        var defTaskConfig = {
	            batchQuerySize: 30,
	            period: 30000
	        };
	        /* task object include:
	         * taskId
	         * period --optional
	         * params  --optional
	         * batchQuerySize --optional
	         * query()
	         */
	        var _register = function(task) {
	            var taskId = task.taskId;
	            QSAPI.TaskAttachment.set(taskId, task.params); //remove duplicate 
	            if (!taskTimer[taskId]) {
	                taskTimer[taskId] = setInterval(function() {
	                    _query(task);
	                }, task.period);
	                taskObj[taskId] = task;
	            } else {
	                _query(task);
	            }
	        };
	        var _query = function(task) {
	            var taskId = task.taskId;
	            var params = QSAPI.TaskAttachment.get(taskId);
	            if (!params.length) {
	                return;
	            }
	            if (params.length > task.batchQuerySize) {
	                var batch = Math.ceil(params.length / task.batchQuerySize);
	                var sIdx, eIdx, subParams;
	                for (var i = 0; i < batch; i++) {
	                    sIdx = i * task.batchQuerySize;
	                    eIdx = Math.min(sIdx + task.batchQuerySize, params.length);
	                    subParams = params.slice(sIdx, eIdx);
	                    task.query(subParams, taskId);
	                }
	            } else {
	                task.query(params, taskId);
	            }
	        };
	
	        return {
	            register: function(task) {
	                task = $.extend({}, defTaskConfig, task);
	                _register(task);
	            },
	            unregister: function(taskId) {
	                if (taskTimer[taskId]) {
	                    clearInterval(taskTimer[taskId]);
	                    taskTimer[taskId] = null;
	                    delete taskTimer[taskId];
	                    taskObj[taskId] = null;
	                    delete taskObj[taskId];
	                    QSAPI.TaskAttachment.clear(taskId);
	                }
	            },
	            setParams: function(taskId, params, subscription) {
	                QSAPI.TaskAttachment.set(taskId, params, subscription);
	            },
	            removeParams: function(taskId, params, subscription) {
	                QSAPI.TaskAttachment.remove(taskId, params, subscription);
	            },
	            subscribe: function(taskId, params, subscription) {
	                var _params = QSAPI.TaskAttachment.set(taskId, params, subscription);
	                var task = taskObj[taskId];
	                if (task) {
	                    _query(task);
	                }
	
	            },
	            unsubscribe: function(taskId, params, subscription) {
	                QSAPI.TaskAttachment.remove(taskId, params, subscription);
	            },
	            setCfg: function(taskId, taskCfg) {
	                if (taskTimer[taskId]) {
	                    clearInterval(taskTimer[taskId]);
	                    taskTimer[taskId] = null;
	                }
	                if (taskObj[taskId]) {
	                    var task = $.extend(taskObj[taskId], taskCfg);
	                    this.register(task);
	                }
	            },
	            getCfg: function(taskId) {
	                return taskObj[taskId];
	            },
	            getSubscriptionsByTicker: function() {
	                return QSAPI.TaskAttachment.getSubscriptionsByTicker.apply(QSAPI.TaskAttachment, arguments);
	            },
	            getTickersBySubscrId: function() {
	                return QSAPI.TaskAttachment.getTickersBySubscrId.apply(QSAPI.TaskAttachment, arguments);
	            }
	        };
	    })();
	    QSAPI.TaskAttachment = (function() {
	        var params = {},
	            subscription = {},
	            paramSubscrMap = {};
	        var checkExist = function(id, list) {
	            return $.inArray(id, list) != -1;
	        }
	        var removeDuplication = function(symbols) {
	            var obj = {};
	            symbols = symbols || [];
	            for (var i = 0, n = symbols.length; i < n; i++) {
	                if (!obj[symbols[i]]) {
	                    obj[symbols[i]] = true;
	                } else {
	                    symbols.splice(i, 1);
	                    i--;
	                    n--;
	                }
	            }
	            return symbols;
	        };
	        return {
	            get: function(taskId) {
	                return params[taskId] || [];
	            },
	            set: function(taskId, query, subscr) {
	                params[taskId] = params[taskId] || [];
	                query = removeDuplication(query);
	                var validQuery = [];
	                for (var i = 0, n = query.length; i < n; i++) {
	                    if (!checkExist(query[i], params[taskId])) {
	                        validQuery.push(query[i]);
	                    }
	                    this.setSubscription(taskId, query[i], subscr);
	                }
	                params[taskId] = params[taskId].concat(validQuery);
	                return validQuery;
	            },
	            remove: function(taskId, query, subscr) {
	                params[taskId] = params[taskId] || [];
	                var idx;
	                for (var i = 0, n = query.length; i < n; i++) {
	                    idx = $.inArray(query[i], params[taskId]);
	                    if (idx != -1) {
	                        if (subscr) {
	                            this.removeSubscription(taskId, query[i], subscr);
	                            var subscrs = this.getSubscriptionsByTicker(taskId, query[i]);
	                            if (subscrs && subscrs.length < 1) {
	                                params[taskId].splice(idx, 1);
	                            }
	                        } else {
	                            params[taskId].splice(idx, 1);
	                        }
	                    }
	                }
	            },
	            clear: function(taskId) {
	                delete params[taskId];
	                this.clearSubscription(taskId);
	            },
	            getAllSubscriptions: function(taskId) {
	                return subscription[taskId];
	            },
	            getSubscription: function(taskId, subscrId) {
	                return subscription[taskId][subscrId];
	            },
	            getSubscriptionsByTicker: function(taskId, ticker) {
	                if (!paramSubscrMap[taskId]) {
	                    return;
	                }
	                var subscrIdMap = paramSubscrMap[taskId][ticker],
	                    subscrList = [];
	                for (var i in subscrIdMap) {
	                    subscrList.push(this.getSubscription(taskId, i));
	                }
	                return subscrList;
	            },
	            getTickersBySubscrId: function(taskId, subscrId) {
	                var subscr = this.getSubscription(taskId, subscrId);
	                return subscr.tickers;
	            },
	            setSubscription: function(taskId, ticker, subscr) {
	                if (subscr) {
	                    if (typeof(subscription[taskId]) == "undefined") {
	                        subscription[taskId] = {};
	                        paramSubscrMap[taskId] = {};
	                    }
	                    var subscrID = subscr.subscribeID;
	                    if (typeof(subscription[taskId][subscrID]) == "undefined") {
	                        subscription[taskId][subscrID] = subscription[taskId][subscrID] || {};
	                        subscription[taskId][subscrID]["widget"] = subscr;
	                        subscription[taskId][subscrID]["tickers"] = [];
	                    }
	                    paramSubscrMap[taskId][ticker] = paramSubscrMap[taskId][ticker] || {};
	                    paramSubscrMap[taskId][ticker][subscrID] = true;
	                    subscription[taskId][subscrID]["tickers"].push(ticker);
	                }
	            },
	            removeSubscription: function(taskId, ticker, subscr) {
	                if (typeof(subscr) != "undefined" && typeof(subscription[taskId]) != "undefined" && typeof(subscription[taskId][subscr.subscribeID]) != "undefined") {
	                    var subscrId = subscr.subscribeID;
	                    var tickers = subscription[taskId][subscrId]["tickers"];
	                    for (var i = 0, n = tickers.length; i < n; i++) {
	                        if (tickers[i] == ticker) {
	                            subscription[taskId][subscrId]["tickers"].splice(i, 1);
	                            delete paramSubscrMap[taskId][ticker][subscrId];
	                            return false;
	                        }
	                    }
	                }
	            },
	            clearSubscription: function(taskId) {
	                delete subscription[taskId];
	                delete paramSubscrMap[taskId];
	            }
	        };
	    })();
	})(QSAPI);
	(function(QSAPI) {
	    'use strict';
	    var $ = QSAPI.$;
	    var Util = QSAPI.Util;
	    QSAPI.Subscriber = function(permissionChecker, cfg) {
	        this.cfg = {};
	        if (cfg) {
	            $.extend(this.cfg, cfg);
	        }
	        this.subSeparator = ".";
	        this.permissionChecker = permissionChecker;
	        this.mfSecIdGkeyMapping = {};
	        this.globalStatus = QSAPI.Util.getGlobalStatus();
	    };
	
	    QSAPI.Subscriber.prototype = {
	        setPermissionChecker: function(permissionChecker) {
	            this.permissionChecker = permissionChecker;
	        },
	        subscribe: function(component, tickerObjects, callbacks, type) {
	            var self = this;
	            tickerObjects = tickerObjects || [];
	            var gkey = null,
	                tickerObject = null,
	                subscribePara = null,
	                noPermissionTickerObjects = [],
	                noTenforeInfoTickerObjects = [],
	                successTickerObjects = [],
	                channels = [],
	                mfSecIds = [];
	            for (var i = 0, l = tickerObjects.length; i < l; i++) {
	                tickerObject = tickerObjects[i];
	                gkey = tickerObject.getField('gkey');
	                if (tickerObject.getField('tenforeCode') == null || tickerObject.getField('tenforeTicker') == null) {
	                    noTenforeInfoTickerObjects.push(tickerObject);
	                } else {
	                    subscribePara = self._getSubscribePara(tickerObject, component);
	                    if (subscribePara.length > 0) {
	                        var subTicker = subscribePara[0].itemName;
	                        if (Util.checkFund(tickerObject)) { //for MF tickers, we need to get NAV from another data source
	                            var secId = tickerObject.getField('secId') || tickerObject.getField('performanceId');
	                            if (secId && secId != '') {
	                                mfSecIds.push(secId);
	                                if (!this.mfSecIdGkeyMapping[secId]) {
	                                    this.mfSecIdGkeyMapping[secId] = subTicker;
	                                }
	                            }
	                        }
	                        channels = channels.concat(subscribePara);
	                        if (tickerObject.getField('mType') == 'FE') { // get IIV subscribe parameters.
	                            var subscribeParaIIV = self._getIIVSubscribeParas(tickerObject, component);
	                            if (subscribeParaIIV.length > 0) {
	                                channels = channels.concat(subscribeParaIIV);
	                            }
	                        }
	                        self._cacheSubscription(subscribePara, tickerObject, component);
	                        successTickerObjects.push(tickerObject);
	                    } else {
	                        noPermissionTickerObjects.push(tickerObject);
	                    }
	                }
	            }
	            this._handleDataHandles(type, channels, component, true, successTickerObjects, mfSecIds);
	            if (typeof callbacks.onSuccess == 'function' && successTickerObjects.length > 0) {
	                var result = this.globalStatus.SUBOK;
	                result["tickerObjects"] = successTickerObjects;
	                callbacks.onSuccess(result);
	            }
	            if (typeof callbacks.onFailure == 'function' && (noTenforeInfoTickerObjects.length > 0 || noPermissionTickerObjects.length > 0)) {
	                var result = this.globalStatus.SUBFAIL;
	                $.extend(result, {
	                    InvalidTickerObjects: noTenforeInfoTickerObjects,
	                    noPermissionTickerObjects: noPermissionTickerObjects
	                });
	                callbacks.onFailure(result);
	            }
	
	        },
	        _handleDataHandles: function(type, channels, component, subscribe, successTickerObjects, mfSecIds) {
	            if (channels && channels.length) {
	                if (type === 'push' && QSAPI.isFod()) {
	                    this._handleDataObjs('push', channels, component, subscribe, successTickerObjects, mfSecIds)
	                } else if (type === 'push' && this.cfg.streamingDataType.indexOf('D') === -1) {
	                    var pullChannels = [],
	                        pushChannels = [];
	                    for (var i = 0, l = channels.length, channel; i < l; i++) {
	                        channel = channels[i];
	                        if (channel.sourceType === 'DO' || channel.sourceType === 'DS') {
	                            pullChannels.push(channel);
	                        } else {
	                            pushChannels.push(channel);
	                        }
	                    }
	                    this._handleDataObjs('pull', pullChannels, component, subscribe, successTickerObjects, mfSecIds);
	                    this._handleDataObjs('push', pushChannels, component, subscribe, successTickerObjects, mfSecIds)
	                } else {
	                    this._handleDataObjs(type, channels, component, subscribe, successTickerObjects, mfSecIds)
	                }
	            }
	        },
	        _handleDataObjs: function(type, channels, component, subscribe, successTickerObjects, mfSecIds) {
	            if (this.dataObjs && this.dataObjs.length && channels && channels.length) {
	                var dataObj, dataHandler, subType = {};
	                if (type == "pull" || type == "mix") {
	                    subType["pull"] = true;
	                }
	                if (type == "push" || type == "mix") {
	                    subType["push"] = true;
	                }
	                for (var i = 0, n = this.dataObjs.length; i < n; i++) {
	                    dataObj = this.dataObjs[i];
	                    if (dataObj) {
	                        if ((dataObj.getType() == "pull" && subType["pull"]) || (dataObj.getType() == "push" && subType["push"])) {
	                            if (subscribe) {
	                                dataObj.batchSubscribe(channels, component);
	                                dataHandler = dataObj.getDataHandler();
	                                if (dataHandler && typeof dataHandler.forceUpdateQuoteData == 'function') {
	                                    dataHandler.forceUpdateQuoteData(component, successTickerObjects);
	                                }
	                                if (mfSecIds.length > 0) {
	                                    dataHandler.getFundDataFromXOI(mfSecIds, this.mfSecIdGkeyMapping, function(o) {
	                                        dataHandler.updateMarketPrice(o, "xoi");
	                                    });
	                                }
	                            } else {
	                                dataObj.batchUnsubscribe(channels, component);
	                            }
	                        }
	                    }
	                }
	            }
	        },
	        unSubscribe: function(component, tickerObjects, callbacks, type) {
	            this._unSubscribe(component, tickerObjects, callbacks, type);
	        },
	        removeCache: function(component) {
	            QSAPI.SubscriberManager.removeComponent(component.subscribeID);
	            var tickerObjects = QSAPI.SubscriberManager.getTickerObjectsBySubID(component.subscribeID);
	            var subType = QSAPI.DataManager.getSubscribeType(component);
	            for (var type in subType) {
	                if (subType[type]) {
	                    this._unSubscribe(component, tickerObjects, null, type);
	                }
	            }
	        },
	        _unSubscribe: function(component, tickerObjects, callbacks, type) {
	            callbacks = callbacks || {};
	            var self = this;
	            tickerObjects = tickerObjects || [];
	            var gkey = null,
	                tickerObject = null,
	                subscribePara = null,
	                channels = [],
	                successTickerObjects = [],
	                noSubscribedTickerObjects = [];
	            for (var i = 0, l = tickerObjects.length; i < l; i++) {
	                tickerObject = tickerObjects[i];
	                gkey = tickerObject.getField('gkey');
	                if (gkey == null) {
	                    noSubscribedTickerObjects.push(tickerObject);
	                    continue;
	                } else {
	                    // var isValid = this._checkSubscribed(component, tickerObject);
	                    // if (isValid) {                  
	                    var subTickers = QSAPI.SubscriberManager.getSubscribeTickers(gkey, component.subscribeID);
	                    self._removeSubscription(tickerObject, component);
	                    subscribePara = self._getSubscribePara(tickerObject, component);
	                    if (subscribePara.length > 0) {
	                        var _subTicker;
	                        for (var j = 0, jl = subscribePara.length; j < jl; j++) {
	                            _subTicker = subscribePara[j].itemName;
	                            if (!QSAPI.SubscriberManager.existSubscription(_subTicker, component.forceDelay)) {
	                                channels.push(subscribePara[j]);
	                            }
	                        }
	                        if (tickerObject.getField('mType') == 'FE') { // get IIV subscribe parameters.
	                            var subscribeParaIIV = self._getIIVSubscribeParas(tickerObject, component);
	                            if (subscribeParaIIV.length > 0) {
	                                channels = channels.concat(subscribeParaIIV);
	                            }
	                        }
	                    }
	                    successTickerObjects.push(tickerObject);
	                    //} else {
	                    //    noSubscribedTickerObjects.push(tickerObject);
	                    //}
	                }
	            }
	            this._handleDataHandles(type, channels, component, false);
	            if (typeof callbacks.onSuccess == 'function' && successTickerObjects.length > 0) {
	                var result = this.globalStatus.UNSUBOK;
	                $.extend(result, {
	                    tickerObjects: successTickerObjects
	                });
	                callbacks.onSuccess(result);
	            }
	            if (typeof callbacks.onFailure == 'function' && (noSubscribedTickerObjects.length > 0)) {
	                var result = this.globalStatus.UNSUBFAIL;
	                $.extend(result, {
	                    InvalidTickerObjects: noSubscribedTickerObjects
	                });
	                callbacks.onFailure(result);
	            }
	        },
	        _cacheSubscription: function(subPara, tickerObject, component) {
	            var subscribeID = component.subscribeID,
	                gkey = tickerObject.getField("gkey"),
	                _tickers = [];
	            for (var i = 0, n = subPara.length; i < n; i++) {
	                _tickers.push(subPara[i].itemName.replace(/\|L2$/, ""));
	            }
	            QSAPI.SubscriberManager.setSubscribeTickers(gkey, subscribeID, QSAPI.Util.removeDuplication(_tickers));
	            QSAPI.SubscriberManager.setComponent(component);
	        },
	        _removeSubscription: function(tickerObject, component) {
	            var gkey = tickerObject.getField('gkey'),
	                subscribeID = component.subscribeID;
	            QSAPI.SubscriberManager.removeSubscribeTickers(gkey, subscribeID);
	        },
	        _getSubscribePara: function(tickerObject, component) {
	            var eCode = tickerObject['tenforeCode'], //getTenforeCode(),
	                type = parseInt(tickerObject['type'], 10),
	                tTicker = tickerObject['tenforeTicker'],
	                subTenforeCode = tickerObject['subTenforeCode'],
	                listMarket = tickerObject['listMarket'] || eCode,
	                isUSComposeExch = QSAPI.Util.isCompositiveExch(tickerObject),
	                subComposite = isUSComposeExch && !component.showSubMarket; //US stock use composite default
	            var exchCode, aParams = [];
	            if (subTenforeCode > 0) {
	                exchCode = subTenforeCode;
	            } else if (subComposite) {
	                exchCode = tickerObject['CompositeExchangeID'];
	            } else {
	                exchCode = eCode;
	            }
	            var subcodes = [];
	            if (component.forceDelay) {
	                subcodes = this.permissionChecker.getSubcodes(type, exchCode, tTicker, "D");
	                if (subcodes.length) {
	                    this._getSubcribeParas(aParams, type, "D", exchCode, subcodes, tTicker); // get levelI subscribe parameters.
	                }
	            } else {
	                //check list market R permission firstly
	                subcodes = this.permissionChecker.getSubcodes(type, listMarket, tTicker, "R");
	                if (subcodes.length) {
	                    this._getSubcribeParas(aParams, type, "R", exchCode, subcodes, tTicker); // get levelI subscribe parameters.
	                } else {
	                    subcodes = this.permissionChecker.getSubcodes(type, exchCode, tTicker, "D");
	                    this._getSubcribeParas(aParams, type, "D", exchCode, subcodes, tTicker); // get levelI subscribe parameters.
	                }
	            }
	            // Clients will subscribe real-time if they have real-time data permission.               
	            if (component.needSubscribeLevelII && (type == 1 || type == 2)) { // level II for Stock,etf,Open-end Fund and options
	                if (subComposite) { // US exchange.
	                    //subscribe 1~26 and 34 exchange for levelII, maybe a accurately list is better if we can get from realtime
	                    for (var i = 1; i <= 26; i++) {
	                        this._getSubcribeParas(aParams, type, "R", i, this.permissionChecker.getSubcodes(type, i, tTicker, "R"), tTicker, true);
	                    }
	                    // this._getSubcribeParas(aParams, type, "R", 14, this.permissionChecker.getSubcodes(type, 14, tTicker, "R"), tTicker, true);
	                    // this._getSubcribeParas(aParams, type, "R", 19, this.permissionChecker.getSubcodes(type, 19, tTicker, "R"), tTicker, true);
	                    // this._getSubcribeParas(aParams, type, "R", 1, this.permissionChecker.getSubcodes(type, 1, tTicker, "R"), tTicker, true);
	                    this._getSubcribeParas(aParams, type, "R", 34, this.permissionChecker.getSubcodes(type, 34, tTicker, "R"), tTicker, true);
	                } else { // other exchange. get levelI and levelII data.
	                    this._getSubcribeParas(aParams, type, "R", eCode, this.permissionChecker.getSubcodes(type, eCode, tTicker, "R"), tTicker, true);
	                }
	            }
	            return aParams;
	        },
	        _getIIVSubscribeParas: function(tickerObject, component) {
	            var aParams = [],
	                gkey = tickerObject.getField('eGkey'),
	                etf = QSAPI.Cache.getStaticData(gkey);
	            if (etf) {
	                var dy = "D",
	                    etfSubcodes = [];
	                if (component.forceDelay) {
	                    etfSubcodes = this.permissionChecker.getSubcodes(10, etf["tenforeCode"], etf["tenforeTicker"], "D", false);
	                    if (etfSubcodes.length) {
	                        this._getSubcribeParas(aParams, 10, "D", etf["tenforeCode"], etfSubcodes, etf["tenforeTicker"]);
	                    }
	                } else {
	                    etfSubcodes = this.permissionChecker.getSubcodes(10, etf["tenforeCode"], etf["tenforeTicker"], "R", false);
	                    if (etfSubcodes.length) {
	                        this._getSubcribeParas(aParams, 10, "R", etf["tenforeCode"], etfSubcodes, etf["tenforeTicker"]);
	                    } else {
	                        etfSubcodes = this.permissionChecker.getSubcodes(10, etf["tenforeCode"], etf["tenforeTicker"], "D", false);
	                        if (etfSubcodes.length) {
	                            this._getSubcribeParas(aParams, 10, "D", etf["tenforeCode"], etfSubcodes, etf["tenforeTicker"]);
	                        }
	                    }
	                }
	            }
	            return aParams;
	        },
	        _getSourceType: function(dataType, sty) {
	            var ty;
	            if (typeof(QSAPI.DataManager) != "undefined" && QSAPI.DataManager.isDemoCache) { // demo cached mode.
	                if (sty == 2) {
	                    ty = "DEMOP";
	                } else {
	                    ty = "DEM";
	                }
	            } else {
	                //nnpush only support the realtime data
	                if (dataType == 'R') {
	                    if (sty == 2) {
	                        ty = 'RO';
	                    } else {
	                        ty = 'RS';
	                    }
	                } else {
	                    if (sty == 2) {
	                        ty = 'DO';
	                    } else {
	                        ty = 'DS';
	                    }
	                }
	            }
	            return typeof ty != 'undefined' ? ty : dataType;
	        },
	        _getSubcribeParas: function(aParams, sType, dataType, exchCode, subcodes, tTicker, isLevelII) {
	            var item = exchCode + this.subSeparator + sType + this.subSeparator + tTicker;
	            if (isLevelII) item = item + "|L2"; // for level II data.
	            aParams.push({
	                serviceName: exchCode,
	                sourceType: this._getSourceType(dataType, sType),
	                itemName: item
	            });
	        },
	        _buildSubscribeParas: function(item, dataType) {
	            var arr = item.split(".");
	            return {
	                serviceName: arr[0],
	                sourceType: this._getSourceType(dataType, arr[1]),
	                itemName: item
	            };
	        },
	        _checkSubscribed: function(component, tickerObject) {
	            var gkey = tickerObject.getField('gkey'),
	                subscribeID = component.subscribeID;
	            var subTickers = QSAPI.SubscriberManager.getSubscribeTickers(gkey, subscribeID);
	            return subTickers && subTickers.length > 0;
	        },
	        setDataObjs: function(dataObjs) {
	            this.dataObjs = dataObjs;
	        }
	    };
	    QSAPI.SubscriberManager = {
	        gkeySubTickerMap: {}, //{gkey:{subscribeID:[subTicker1,subTicker2]}}
	        subTickerGkeyMap: {}, //{subTicker:{subscribeID:{gkey1:true,gkey2:true}}}
	        subIDComponentMap: {}, //{subID:component}
	        setSubscribeTickers: function(gkey, subscribeID, subTickers) {
	            this.gkeySubTickerMap[gkey] = this.gkeySubTickerMap[gkey] || {};
	            this.gkeySubTickerMap[gkey][subscribeID] = subTickers;
	            var _ticker;
	            for (var i = 0, n = subTickers.length; i < n; i++) {
	                _ticker = subTickers[i];
	                this.subTickerGkeyMap[_ticker] = this.subTickerGkeyMap[_ticker] || {};
	                this.subTickerGkeyMap[_ticker][subscribeID] = this.subTickerGkeyMap[_ticker][subscribeID] || {};
	                this.subTickerGkeyMap[_ticker][subscribeID][gkey] = true;
	            }
	        },
	        getSubscribeTickers: function(gkey, subscribeID) {
	            var subTickers = this.gkeySubTickerMap[gkey] ? this.gkeySubTickerMap[gkey][subscribeID] : [];
	            return subTickers;
	        },
	        removeSubscribeTickers: function(gkey, subscribeID) {
	            if (this.gkeySubTickerMap[gkey]) {
	                var tickers = this.gkeySubTickerMap[gkey][subscribeID],
	                    subTicker;
	                for (var i = 0, n = tickers.length; i < n; i++) {
	                    subTicker = tickers[i];
	                    if (this.subTickerGkeyMap[subTicker] && this.subTickerGkeyMap[subTicker][subscribeID][gkey]) {
	                        delete this.subTickerGkeyMap[subTicker][subscribeID][gkey];
	                        if ($.isEmptyObject(this.subTickerGkeyMap[subTicker][subscribeID])) {
	                            delete this.subTickerGkeyMap[subTicker][subscribeID];
	                            if ($.isEmptyObject(this.subTickerGkeyMap[subTicker])) {
	                                delete this.subTickerGkeyMap[subTicker];
	                                QSAPI.Cache.remove(subTicker); //remove cache data                            
	                            }
	                        }
	                    }
	                }
	                this.gkeySubTickerMap[gkey][subscribeID] = [];
	            }
	        },
	        existSubscription: function(subTicker, forceDelay) {
	            var exist = this.subTickerGkeyMap[subTicker] && !$.isEmptyObject(this.subTickerGkeyMap[subTicker]);
	            if (exist) { //check whether the ticker are subscribed in same interval as target ticker
	                var otherInterval = true;
	                for (var subID in this.subTickerGkeyMap[subTicker]) {
	                    var comp = this.getComponent(subID);
	                    if (Util.getValidForceDelay(comp.forceDelay) === Util.getValidForceDelay(forceDelay)) {
	                        otherInterval = false;
	                    }
	                }
	                exist = !otherInterval;
	            }
	            return exist;
	        },
	        getSubscription: function(subTicker) {
	            return this.subTickerGkeyMap[subTicker];
	        },
	        getGkeys: function(subTicker, subID) {
	            return (this.subTickerGkeyMap[subTicker] ? this.subTickerGkeyMap[subTicker][subID] : {}) || {};
	        },
	        getTickerObjectsBySubID: function(subID) {
	            var tickerObjects = [],
	                tickerObj;
	            for (var gkey in this.gkeySubTickerMap) {
	                if (this.gkeySubTickerMap[gkey][subID] && this.gkeySubTickerMap[gkey][subID].length) {
	                    tickerObj = QSAPI.Cache.getTickerData(gkey);
	                    tickerObjects.push(tickerObj);
	                }
	            }
	            return tickerObjects;
	        },
	        setComponent: function(component) {
	            this.subIDComponentMap[component.subscribeID] = component;
	        },
	        removeComponent: function(subscribeID) { //destroy the mapping when clear component
	            delete this.subIDComponentMap[subscribeID];
	        },
	        getComponent: function(subscribeID) {
	            return this.subIDComponentMap[subscribeID];
	        }
	    };
	})(QSAPI);
	(function(QSAPI) {
	    'use strict';
	    var $ = QSAPI.$;
	    var Util = QSAPI.Util;
	    QSAPI.Monitor = function(options) {
	        var self = this;
	        var oneRequestItemsNumber = 0;
	        var oneResponseItemsNumber = 0;
	        this.options = {
	            tryCount: 5,
	            host: QSAPI.getQSPath() + "ra/addStreamingData",
	            deviceType: Util.getDeviceType()
	        };
	        /* 
	        {   
	            '126.1': true,
	            '127.1': false,
	            '22.1': true 
	        }
	        */
	        this.recordObject = {};
	        this.timer = null;
	        this.delayTime = 2000;
	        $.extend(this.options, options);
	
	        var generateQueryString = function(recordObject) {
	            oneRequestItemsNumber = 0;
	            var array = [];
	            for (var key in recordObject) {
	                if (recordObject[key] === false) {
	                    array.push(key);
	                    oneRequestItemsNumber++;
	                }
	            }
	            return array.join(',');
	        }
	
	        var markRecordFromResult = function(data) {
	            oneResponseItemsNumber = 0;
	            var recordItem = self.recordObject;
	            $.each(data, function(index, field) {
	                if (field["ExchangeId"] && field["SecurityType"]) {
	                    recordItem[field["ExchangeId"] + '.' + field["SecurityType"]] = true;
	                    oneResponseItemsNumber++;
	                }
	            });
	        };
	
	        var sendRecordToRemote = function(recordObject, times) {
	            clearTimeout(self.timer);
	            times = times || 0;
	            var queryString = generateQueryString(recordObject);
	            self.timer = setTimeout(function() {
	                QSAPI.Util.ajax({
	                    type: 'GET',
	                    url: self.options.host,
	                    dataType: 'JSON',
	                    data: {
	                        'tickers': queryString,
	                        device: self.options.deviceType
	                    },
	                    success: function(result) {
	                        if (result.Status.ErrorCode === '0') {
	                            var Data = result.Data || [];
	                            markRecordFromResult(Data);
	                            if (oneResponseItemsNumber !== oneRequestItemsNumber) {
	                                handleUnrecordRecord(times);
	                            }
	                        }
	                    },
	                    error: function() {
	                        handleUnrecordRecord(times);
	                    }
	                });
	            }, self.delayTime);
	        };
	
	        // check if has send record fail data, the key is still false
	        // has max try count
	        var handleUnrecordRecord = function(times) {
	            var recordItem = self.recordObject;
	            var unRecordObject = {};
	            for (var key in recordItem) {
	                if (recordItem[key] === false) {
	                    unRecordObject = unRecordObject || {};
	                    unRecordObject[key] = false;
	                }
	            }
	            if (!$.isEmptyObject(unRecordObject) && (times < self.options.tryCount)) {
	                times++;
	                sendRecordToRemote(unRecordObject, times);
	            } else {
	                clearTimeout(self.timer);
	            }
	        }
	
	        var transformTicker = function(ticker, level2Type) {
	            var transformedStr,
	                tickerArray = ticker.split('-'),
	                tradeTicker = (tickerArray[0] || '').split('\.'),
	                listTicker = (tickerArray[1] || '').split('\.');
	
	            if (tradeTicker.length >= 2) {
	                transformedStr = tradeTicker[0] + '.' + tradeTicker[1];
	                if (listTicker.length > 0) {
	                    transformedStr = transformedStr + '.' + listTicker[0];
	                }
	            }
	            if (level2Type) {
	                transformedStr = transformedStr + '|' + level2Type;
	            }
	            return transformedStr;
	        }
	
	        this.record = function(gKeyList, level2TypeList) {
	            var isNeedAddNewItem = false;
	            $.each(gKeyList, function(index, gKey) {
	                if (gKey) {
	                    var transformString = '';
	                    // level 2 ticker, transform to 126.1|BMO / 126.1|BMP
	                    if (level2TypeList && level2TypeList[index]) {
	                        transformString = transformTicker(gKey, level2TypeList[index]);
	                    } else {
	                        transformString = transformTicker(gKey);
	                    }
	
	                    if (typeof self.recordObject === 'undefined') {
	                        self.recordObject = {};
	                    }
	                    if (typeof self.recordObject[transformString] === 'undefined') {
	                        self.recordObject[transformString] = false;
	                        isNeedAddNewItem = true;
	                    }
	                }
	            });
	            if (isNeedAddNewItem) {
	                sendRecordToRemote(self.recordObject, 0);
	            }
	        };
	
	        this.destory = function() {
	            clearTimeout(self.timer);
	            self.recordObject = {};
	        }
	    };
	})(QSAPI);
	(function (QSAPI) {
	    'use strict';
	    var $ = QSAPI.$;
	    var Util = QSAPI.Util;
	
	    var generateQueryString = function (array) {
	        return array.join(',');
	    }
	
	    QSAPI.TimezoneManager = function (options) {
	        var self = this;
	        this.data = {};
	        this.options = {
	            URL: QSAPI.getQSPath() + "ra/getTimezone"
	        };
	        $.extend(this.options, options);
	    };
	
	    QSAPI.TimezoneManager.prototype = {
	        /**
	         * @tickers Required. need tenforce ticker. Like 126.1.IBM,33.10.MSGBGBPP
	         * @callback Required. calback, return data.
	         */
	        getTickersTimezone: function (tickers, callback) {
	            var self = this;
	            if (!$.isArray(tickers)) {
	                tickers = tickers.split(',');
	            } else {
	                tickers = this._genQueryTickers(tickers);
	            }
	            var needGetDataTickers = this._filterNeedGetDataTicker(tickers);
	
	            if (needGetDataTickers.length !== 0) {
	                this._getTimezoneFromRemote(needGetDataTickers, function () {
	                    var returnData = self._getReturnData(tickers);
	                    callback(returnData);
	                });
	            } else {
	                var returnData = self._getReturnData(tickers);
	                callback(returnData);
	            }
	        },
	        /**
	         * @tickers Required. need tenforce ticker. Like 126.1.IBM,33.10.MSGBGBPP
	         * @return already saved timezones
	         */
	        getTimezoneByTicker: function (ticker) {
	            var self = this;
	            if (!ticker) {
	                return '';
	            }
	            ticker = typeof ticker === 'string' ? ticker : ticker.listTicker;
	            return self.data[ticker] || '';;
	        },
	        /**
	         * @param fullName, timezone full name, like America/New_York
	         * @return time diff compare to GMT/UTC
	         */
	        getTimezoneAbbr: function (fullName) {
	            if (!fullName) {
	                return '';
	            }
	            if (!QSAPI.DataManager.cfg.timezoneMap) {
	                throw new Error('timezone map is empty');
	            }
	            var timezoneMap = QSAPI.DataManager.cfg.timezoneMap || {};
	            var _abbreviation = timezoneMap && timezoneMap[fullName] && timezoneMap[fullName]["abbreviation"] || '';
	            return _abbreviation;
	        },
	        /**
	         * @param fullName, timezone full name, like America/New_York
	         * @return time diff compare to GMT/UTC
	         */
	        getTimezoneTimediff: function (fullName) {
	            if (!fullName) {
	                return 0;
	            }
	            if (!QSAPI.DataManager.cfg.timezoneMap) {
	                throw new Error('timezone map is empty');
	            }
	            var timezoneMap = QSAPI.DataManager.cfg.timezoneMap || {};
	            var _timeDiff = timezoneMap && timezoneMap[fullName] && timezoneMap[fullName]["gmtOffsetByMinutes"] || 0;
	            return _timeDiff;
	        },
	        _genQueryTickers: function (tickers) {
	            var queryTickers = [];
	            $.each(tickers, function (index, item) {
	                if (typeof item === 'string') {
	                    queryTickers.push(item);
	                } else {
	                    queryTickers.push(item.listTicker);
	                }
	            });
	            return queryTickers;
	        },
	        _getReturnData: function (tickers) {
	            var self = this;
	            var data = self.data;
	            var returnData = [];
	            $.each(tickers, function (index, ticker) {
	                returnData.push(data[ticker]);
	            });
	            return returnData;
	        },
	        _getTimezoneFromRemote: function (tickers, callback) {
	            var self = this;
	            var queryString = generateQueryString(tickers);
	            QSAPI.Util.ajax({
	                type: 'GET',
	                url: self.options.URL,
	                dataType: 'JSON',
	                data: {
	                    'tickers': queryString
	                },
	                success: function (result) {
	                    if (result.Status.ErrorCode === '0') {
	                        var remoteData = result.Data || [];
	                        self._saveData(remoteData);
	                        if (typeof callback == 'function') {
	                            callback();
	                        }
	                    }
	                },
	                error: function () {
	                    if (typeof callbacks == 'function') {
	                        callback();
	                    }
	                }
	            });
	        },
	        _filterNeedGetDataTicker: function (tickers) {
	            var self = this;
	            var needGetDataTicker = [];
	            $.each(tickers, function (index, ticker) {
	                if (!self.data[ticker]) {
	                    needGetDataTicker.push(ticker);
	                }
	            });
	            return needGetDataTicker;
	        },
	        _saveData: function (data) {
	            var self = this;
	            $.each(data, function (index, item) {
	                self.data[item.ticker] = item.timezone;
	            });
	        }
	    }
	})(QSAPI);
	(function(QSAPI) {
	    'use strict';
	    var $ = QSAPI.$;
	    var Util = QSAPI.Util;
	    QSAPI.DataHandler = function(cfg, callbacks) {
	        this.cfg = {
	            gExchangeMarketTimeMap: {},
	            pullSubSeparator: '_',
	            pushSubSeparator: '|'
	        };
	        if (cfg) {
	            $.extend(true, this.cfg, cfg);
	        }
	        this.callbacks = callbacks || {};
	        this.defaultModuleType = 'default';
	        //this.type=type;
	        this._id = -1;
	        this.widgets = {}; // 0:whatchlist 1:chart 2:news 3:quicktake 4:alert 5:tickertape 6:l2MarketDepth 7:time&sales 8:options 10:quotes
	        this.staticTokens = ['hs05a', 'tenforeCode', 'tenforeTicker', 'type', 'qv001', 'qv002', 'qv003',
	            'qv004', 'qv005', 'qv006', 'qv007', 'qv008', 'qv009', 'qv010', 'of028', 'os03s', 'os463',
	            'of009', 'sta0f', 'hs001', 'hs00l', 'sta4n', 'sta4j', 'sta4l', 'st159', 'os378', 'os388',
	            'os00m', 'st153', 'la03z', 'os060', 'os089', 'aa0a5', 'st168', 'st169', 'st109', 'st106',
	            'st200', 'st202', 'st201', 'st181', 'st206', 'st198', 'st263', 'st408', 'st415', 'sta44',
	            'pm032', 'sta65', 'pd003', 'pd005', 'pd007', 'pd009', 'pm493', 'pd00d', 'pd014', 'pd00f',
	            'st292', 'st299', 'os09r', 'st185', 'sta0c', 'ar111', 'ac057', 'hs377', 'os088', 'ar112',
	            'os02w', 'sta4h', 'pd00h', 'pm494', 'pm495', 'pd00b', 'st816', 'optiontype',
	            'rootsymbol', 'expirydate', 'strikeprice', 'S1361', 'os013', 'st412', 'st110', 'pd001', 'hs03w', 'hs05x', 'hs05v', 'st160', 'st410', 'os01x','st82s','StarRating','StarRatingDatetime','DividendFrequency'
	        ];
	        this.tenforeStaticToken = 'S721,S695,S696,S697,S698,S529,S1361,S214,S527,S528,D1968,S650,S651,S652,S653,S654,S547,S1527,S1012,S3377,S12,S676,S1776,S3080,S1315,S1077,S1314,S31';
	        this.dynamicTokens = {
	            "PreDate": "s",
	            "PreTime": "i",
	            "PrePrice": "f",
	            "PreChg": "f",
	            "PreChg%": "f",
	            "PostDate": "s",
	            "PostTime": "i",
	            "PostPrice": "f",
	            "PostChg": "f",
	            "PostChg%": "f",
	            "PrePostDate": "s",
	            "PrePostime": "i",
	            "PrePostPrice": "f",
	            "PrePostChg": "f",
	            "PrePostChg%": "f",
	            "PrePostClosePrice": "f",
	            "TradeDate": "s",
	            "TradeTime": "i",
	            "LastPrice": "f",
	            "Chg": "f",
	            "Chg%": "f",
	            "LastMarket": "s",
	            "OpenPrice": "f",
	            "HighPrice": "f",
	            "LowPrice": "f",
	            "ClosePrice": "f",
	            "MiddlePrice": "f",
	            "AskMarket": "s",
	            "AskPrice": "f",
	            "AskSize": "i",
	            "BidMarket": "s",
	            "BidPrice": "f",
	            "BidSize": "i",
	            "R/D": "s",
	            "Date": "s",
	            "Time": "i",
	            "LastVolume": "i",
	            "Volume": "i",
	            "VWAP": "f",
	            "MarketCentre": "s",
	            "OpenInt": "i",
	            "Indicator": "s",
	            "401": "i",
	            "402": "i",
	            "404": "f",
	            "405": "i",
	            "407": "s",
	            "409": "i",
	            "-102": "i",
	            "-501": "i",
	            "BidTime": "i",
	            "AskTime": "i",
	            "D2156": "s",
	            "D2157": "s",
	            "D2158": "s",
	            "D2159": "s",
	            "D2160": "s",
	            "ContractName": "s",
	            "ExpirationDate": "s",
	            "NetChange": "f",
	            "High52W": "s",
	            "HighDate52W": "s",
	            "Low52W": "s",
	            "LowDate52W": "s",
	            "Name": "s",
	            "OpenClose": "s",
	            "Transactions": "s",
	            "MSTicker": "s",
	            "TickerStatus": "s",
	            "SessionStatus": "s",
	            "DisnatVolume": "i",
	            "262": "f",
	            "263": "f",
	            "268": "f",
	            "269": "f",
	            "272": "f",
	            "273": "f",
	            "278": "f",
	            "279": "f",
	            "282": "f",
	            "283": "f"
	        };
	        this.l2Tokens = {
	            "401": "i",
	            "402": "i",
	            "404": "f",
	            "405": "i",
	            "407": "s",
	            "409": "i",
	            "-102": "i"
	        };
	        //extend the level2 fields
	        for (var i = 0; i < 20; i++) {
	            this.dynamicTokens['AskPrice_' + i] = 'f';
	            this.dynamicTokens['AskOrder_' + i] = 'i';
	            this.dynamicTokens['AskSize_' + i] = 'i';
	            this.dynamicTokens['BidPrice_' + i] = 'f';
	            this.dynamicTokens['BidOrder_' + i] = 'i';
	            this.dynamicTokens['BidSize_' + i] = 'i';
	        }
	
	        this.LA03Z = {
	            "$VAL_UMKNOWN$": "Not Classified",
	            "101": "Basic Materials",
	            "308": "Communication Services",
	            "102": "Consumer Cyclical",
	            "205": "Consumer Defensive",
	            "309": "Energy",
	            "103": "Financial Services",
	            "206": "Healthcare",
	            "310": "Industrials",
	            "104": "Real Estate",
	            "311": "Technology",
	            "207": "Utilities"
	        };
	        this.Mo = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', '']
	
	        this.prePostTokens = {
	            'Chg': 'PrePostChg',
	            'Chg%': 'PrePostChg%',
	            'TradeTime': 'PrePostTime',
	            'TradeDate': 'PrePostDate',
	            'Time': 'PrePostTime',
	            'Date': 'PrePostDate',
	            'LastPrice': 'PrePostPrice',
	            'ClosePrice': 'PrePostClosePrice'
	        };
	
	        var MARKETSTATE = {
	            "OPEN": "Open",
	            "CLOSED": "Closed",
	            "PRE": "Pre-Open",
	            "POST": "Post-Trading"
	        };
	
	        var _sf = this;
	        var _getTenforeStaticData = function(secIDS, callbacks) {
	            Util.getData({
	                url: _sf.cfg.staticDataService,
	                type: "get",
	                dataType: "jsonp",
	                data: {
	                    'secId': secIDS,
	                    'tfTokens': _sf.tenforeStaticToken
	                }
	            }, {
	                onSuccess: function(o) {
	                    if (callbacks.onSuccess) {
	                        callbacks.onSuccess(o);
	                    }
	                },
	                onFailure: function(o) {
	                    if (callbacks.onFailure) {
	                        callbacks.onFailure(o);
	                    }
	                }
	            });
	        };
	        this.getSubSeparator = function() {
	            return {
	                pull: this.cfg.pullSubSeparator,
	                push: this.cfg.pushSubSeparator
	            }
	        };
	
	        var getMarketState = function(data) {
	            return data.SessionStatus;
	        };
	
	        var formatDateString = function(str) {
	            if (!str) return '';
	            var t = [];
	            t.push(str.slice(0, 4)), t.push(str.slice(4, 6)), t.push(str.slice(6, 8));
	            return t.join("-");
	        };
	
	        // transform dd-mm-yyyy to mm-dd-yyyy
	        var formatRTDateString = function(str) {
	            if (!str) return '';
	            return str.replace(/(\w+)-(\w+)-(\w+)/, "$2-$1-$3");
	        };
	
	        var setChanges = function(cachedData, marketPriceMap, prefix) {
	            prefix = prefix || 'Last';
	            var __state = getMarketState(marketPriceMap) || getMarketState(cachedData),
	                __lastPrice = marketPriceMap[prefix + 'Price'],
	                __olastPrice = cachedData[prefix + 'Price'],
	                __isPost = prefix === 'Post' && __state === MARKETSTATE.POST,
	                __closePrice = __isPost ? marketPriceMap.LastPrice : marketPriceMap.ClosePrice,
	                __oclosePrice = __isPost ? cachedData.LastPrice : cachedData.ClosePrice;
	            if (typeof __lastPrice !== 'undefined' || typeof __closePrice !== 'undefined') {
	                __lastPrice = typeof __lastPrice !== 'undefined' ? parseFloat(__lastPrice, 10) : __olastPrice;
	                __closePrice = typeof __closePrice !== 'undefined' ? parseFloat(__closePrice, 10) : __oclosePrice;
	                if (__lastPrice >= 0 && __closePrice > 0) {
	                    var key = prefix === 'Last' ? 'Chg' : prefix + 'Chg';
	                    var change = __lastPrice - __closePrice;
	                    marketPriceMap[key] = change;
	                    marketPriceMap[key + '%'] = change / __closePrice * 100;
	                }
	            }
	        };
	
	        var setPrePostExtendFileds = function(marketPriceMap) {
	            var state = getMarketState(marketPriceMap),
	                fields = ['Price', 'Chg', 'Chg%', 'Date', 'Time'],
	                prefix;
	            if (state === MARKETSTATE.OPEN || state === MARKETSTATE.CLOSED) {
	                if ((marketPriceMap.PostDate && marketPriceMap.PostTime) || (marketPriceMap.PreDate && marketPriceMap.PreTime)) {
	                    prefix = parseInt(marketPriceMap.PostDate + marketPriceMap.PostTime, 10) > parseInt(marketPriceMap.PreDate + marketPriceMap.PreTime, 10) ? 'Post' : 'Pre';
	                }
	            } else if (state === MARKETSTATE.POST) {
	                prefix = 'Post';
	            } else if (state === MARKETSTATE.PRE) {
	                prefix = 'Pre';
	            }
	            if (prefix) {
	                for (var i = 0, l = fields.length, filed, value; i < l; i++) {
	                    filed = fields[i];
	                    value = marketPriceMap[prefix + filed];
	                    if (typeof value !== 'undefined') {
	                        marketPriceMap['PrePost' + filed] = value;
	                    }
	                }
	                value = marketPriceMap[prefix === 'Post' ? 'LastPrice' : 'ClosePrice'];
	                if (value) {
	                    marketPriceMap['PrePostClosePrice'] = value;
	                }
	                marketPriceMap['PrePostTxt'] = prefix === 'Post' ? 'After Hours' : 'Pre-Market';
	            }
	        };
	
	        var setAskBidMarket = function(marketPriceMap) {
	            var ap = marketPriceMap["AskPrice"],
	                as = marketPriceMap["AskSize"],
	                bp = marketPriceMap["BidPrice"],
	                bs = marketPriceMap["BidSize"];
	            if (typeof ap != 'undefined' || typeof as != 'undefined') {
	                if (!marketPriceMap["AskMarket"]) {
	                    marketPriceMap["AskMarket"] = marketPriceMap["LastMarket"];
	                }
	            }
	            if (typeof bs != 'undefined' || typeof bp != 'undefined') {
	                if (!marketPriceMap["BidMarket"]) {
	                    marketPriceMap["BidMarket"] = marketPriceMap["LastMarket"];
	                }
	            }
	        };
	
	        var returnExistFields = function(fields, marketPriceMap, subCacheData) {
	            var field0 = fields[0],
	                field1 = fields[1],
	                returnFields = {};
	            if (typeof marketPriceMap[field0] != 'undefined' && typeof marketPriceMap[field1] != 'undefined') {
	                //format the price or size to float or int
	                marketPriceMap[field0] = parseFloat(marketPriceMap[field0], 10);
	                marketPriceMap[field1] = parseInt(marketPriceMap[field1], 10);
	
	                returnFields = {
	                    updateFields: {},
	                    storeFields: fields
	                }
	
	                //build the needUpdateFields
	                if (marketPriceMap[field0] != subCacheData[field0]) {
	                    returnFields['updateFields'][field0] = {
	                        value: marketPriceMap[field0],
	                        oValue: subCacheData[field0],
	                        dataType: 'f'
	                    }
	                }
	                if (marketPriceMap[field1] != subCacheData[field1]) {
	                    returnFields['updateFields'][field1] = {
	                        value: marketPriceMap[field1],
	                        oValue: subCacheData[field1],
	                        dataType: 'i'
	                    }
	                }
	
	            } else if (typeof marketPriceMap[field0] != 'undefined') {
	                marketPriceMap[field0] = parseFloat(marketPriceMap[field0], 10);
	                returnFields = {
	                    storeFields: [field0]
	                }
	                if (typeof subCacheData[field1] != 'undefined') {
	                    returnFields.updateFields = {};
	                    if (marketPriceMap[field0] != subCacheData[field0]) {
	                        returnFields['updateFields'][field0] = {
	                            value: marketPriceMap[field0],
	                            oValue: subCacheData[field0],
	                            dataType: 'f'
	                        }
	                    }
	                    if (typeof subCacheData[field0] == 'undefined') {
	                        returnFields['updateFields'][field1] = {
	                            value: subCacheData[field1],
	                            oValue: marketPriceMap[field1],
	                            dataType: 'i'
	                        }
	                    }
	                }
	            } else if (typeof marketPriceMap[field1] != 'undefined') {
	                marketPriceMap[field1] = parseFloat(marketPriceMap[field1], 10);
	                returnFields = {
	                    storeFields: [field1]
	                }
	                if (typeof subCacheData[field0] != 'undefined') {
	                    returnFields.updateFields = {};
	                    if (marketPriceMap[field1] != subCacheData[field1]) {
	                        returnFields['updateFields'][field1] = {
	                            value: marketPriceMap[field1],
	                            oValue: subCacheData[field1],
	                            dataType: 'f'
	                        }
	                    }
	                    if (typeof subCacheData[field0] == 'undefined') {
	                        returnFields['updateFields'][field0] = {
	                            value: subCacheData[field0],
	                            oValue: marketPriceMap[field0],
	                            dataType: 'i'
	                        }
	                    }
	                }
	            }
	            return returnFields;
	        };
	        var handleAskBidData = function(cachedData, marketPriceMap, needUpdateField) {
	            if (!cachedData["subExchangeAskBid"]) {
	                cachedData["subExchangeAskBid"] = {};
	            }
	            var am = marketPriceMap["AskMarket"],
	                bm = marketPriceMap["BidMarket"];
	
	            if (typeof am != 'undefined') {
	                if (!cachedData["subExchangeAskBid"][am]) {
	                    cachedData["subExchangeAskBid"][am] = {};
	                }
	                var subAskGData = cachedData["subExchangeAskBid"][am];
	                //check askprice/asksize paris of marketPriceMap
	                var askFields = returnExistFields(['AskPrice', 'AskSize'], marketPriceMap, subAskGData);
	                var updatedAskFields = askFields['updateFields'] || [],
	                    storeAskFields = askFields['storeFields'] || [];
	                for (var fieldName in updatedAskFields) {
	                    needUpdateField[fieldName] = updatedAskFields[fieldName];
	                    cachedData[fieldName] = updatedAskFields[fieldName]['value'];
	                    if (am != cachedData['AskMarket']) {
	                        needUpdateField['AskMarket'] = { value: am, oValue: cachedData['AskMarket'], dataType: 's' };
	                        cachedData['AskMarket'] = am;
	                    }
	                }
	                for (var i = 0, l = storeAskFields.length; i < l; i++) {
	                    var fieldName = storeAskFields[i];
	                    cachedData["subExchangeAskBid"][am][fieldName] = marketPriceMap[fieldName];
	                }
	            }
	            if (typeof bm != 'undefined') {
	                if (!cachedData["subExchangeAskBid"][bm]) {
	                    cachedData["subExchangeAskBid"][bm] = {};
	                }
	                var subBidGData = cachedData["subExchangeAskBid"][bm];
	
	                var bidFields = returnExistFields(['BidPrice', 'BidSize'], marketPriceMap, subBidGData);
	
	                var updatedBidFields = bidFields['updateFields'] || [],
	                    storeBidFields = bidFields['storeFields'] || [];
	                for (var fieldName in updatedBidFields) {
	                    needUpdateField[fieldName] = updatedBidFields[fieldName];
	                    cachedData[fieldName] = updatedBidFields[fieldName]['value'];
	                    if (bm != cachedData['BidMarket']) {
	                        needUpdateField['BidMarket'] = { value: bm, oValue: cachedData['BidMarket'], dataType: 's' };
	                        cachedData['BidMarket'] = bm;
	                    }
	                }
	                for (var i = 0, l = storeBidFields.length; i < l; i++) {
	                    var fieldName = storeBidFields[i];
	                    cachedData["subExchangeAskBid"][bm][fieldName] = marketPriceMap[fieldName];
	                }
	
	            }
	
	        };
	
	        var getSourceTypeCode = function(sourceType) {
	            if (sourceType.indexOf('DEM') == 0) {
	                return 'E';
	            } else if (sourceType.indexOf('R') == 0) {
	                return 'R';
	            } else {
	                return 'D';
	            }
	        };
	        var handleForBond = function(cachedData, marketPriceMap, needUpdateField) {
	            if (marketPriceMap["D942"]) {
	                var time = marketPriceMap["D942"],
	                    formatTime;
	                if (typeof(time) != "undefined") {
	                    time = time.substr(0, 8).split(":");
	                    formatTime = parseInt(time[0], 10) * 60 * 60 + parseInt(time[1], 10) * 60 + parseInt(time[2], 10);
	                    if (cachedData["TradeTime"] != formatTime) {
	                        cachedData["TradeTime"] = formatTime;
	                        needUpdateField["TradeTime"] = needUpdateField["TradeTime"] || {};
	                        needUpdateField["TradeTime"].value = formatTime;
	                    }
	                }
	            }
	            if (!marketPriceMap["currency"]) {
	                if (cachedData["currency"] != "USD") {
	                    cachedData["currency"] = "USD";
	                    needUpdateField["currency"] = needUpdateField["currency"] || {};
	                    needUpdateField["currency"].value = "USD";
	                }
	            }
	        };
	
	        this.getMarketQuoteData = function(component, gkey, field) { // get last price,chg%,chg,time and date from cache. 
	            var mergePrePost = Util.isMergePrePost(component.mergePrePost),
	                oData = QSAPI.Cache.getData(component, gkey, false, true),
	                data = {};
	            if (typeof field != 'undefined' &&
	                !(field == "LastPrice" || field == "Chg" || field == "ClosePrice" || field == "Chg%" || field == "Time" || field == "Date" || field == "R/D")) {
	                return { value: oData[field] };
	            }
	            var keyMaps = {
	                "LastPrice": !mergePrePost ? 'LastPrice' : 'PrePostPrice',
	                "Chg": !mergePrePost ? 'Chg' : 'PrePostChg',
	                "Chg%": !mergePrePost ? 'Chg%' : 'PrePostChg%',
	                "Time": !mergePrePost ? 'Time' : 'PrePostTime',
	                "Date": !mergePrePost ? 'Date' : 'PrePostDate',
	                "ClosePrice": !mergePrePost ? 'ClosePrice' : 'PrePostClosePrice'
	            };
	            for (var key in keyMaps) {
	                data[key] = { dataType: "f", value: oData[keyMaps[key]] };
	            }
	            data["PrePostTxt"] = { dataType: "s", value: oData["PrePostTxt"] };
	            data["R/D"] = { dataType: "s", value: oData["R/D"] };
	            return typeof field == 'undefined' ? data : data[field];
	        };
	
	        this._splitItemName = function(itemName) {
	            var aL = itemName.split(this.cfg.pushSubSeparator),
	                tempAL;
	            if (aL.length != 3) { //the result is different between push and pull.
	                tempAL = itemName.split(this.cfg.pullSubSeparator);
	                if (tempAL.length == 3) {
	                    aL = tempAL;
	                } else if (tempAL.length > 3) { // handle pull return 
	                    aL = [];
	                    aL[0] = tempAL[0];
	                    var temp = [],
	                        tempALLength = tempAL.length;
	                    for (var ti = 1; ti < tempALLength - 1; ti++) {
	                        temp.push(tempAL[ti]);
	                    }
	                    aL[1] = temp.join(this.cfg.pullSubSeparator);
	                    aL[2] = tempAL[tempALLength - 1];
	                }
	            }
	            return aL;
	        };
	
	        this.updateMarketPrice = function(body, sourceType) {
	            body = body || {};
	            var itemsReceived = [],
	                isPush4 = false;
	            if (body.items) {
	                itemsReceived = body.items || [];
	            } else {
	                isPush4 = true;
	                itemsReceived = body || [];
	            }
	            var result = {},
	                aL, gKey, marketPriceMap, fieldsForItem, subjectForItem, oExch, cachedData;
	            for (var i = 0, itemLh = itemsReceived.length; i < itemLh; i++) {
	                fieldsForItem = itemsReceived[i].fields;
	                if (!fieldsForItem || fieldsForItem.length == 0) {
	                    continue;
	                }
	                if (isPush4) {
	                    subjectForItem = itemsReceived[i].subject; // /DEM/SC/126.1.IBM, /R/SC/127.1.RCI/A;
	                    subjectForItem = subjectForItem.split("|")[0]; // remove the "|L2" if there is.
	                    var _subjects = subjectForItem.split("/");
	                    if (_subjects.length > 4) { // Ticker include "/" 127.1.RCI/A or 126.1.C/WS/A
	                        for (var i = 4; i < _subjects.length; i++) {
	                            _subjects[3] += '/' + _subjects[i]
	                        }
	                    }
	                    var _itemNames = _subjects[3].split(".");
	                    result.serviceName = _itemNames[0];
	                    result.itemName = _itemNames[2];
	                    result.symbolType = _itemNames[1];
	                    result.dataType = getSourceTypeCode(_subjects[1]);
	                } else {
	                    result.serviceName = itemsReceived[i].service;
	                    result.itemName = itemsReceived[i].symbol;
	                    result.dataType = (itemsReceived[i].source || '').toUpperCase(); //result.sourceType; // D or R.
	                    aL = this._splitItemName(result.itemName);
	                    result.symbolType = aL[0];
	                    result.itemName = aL[1];
	                }
	                oExch = result.serviceName;
	                gKey = Util.generateGKey(result.serviceName, result.symbolType, result.itemName);
	                cachedData = QSAPI.Cache.getMarketData(sourceType, result.dataType, gKey) || {};
	                marketPriceMap = {
	                    "marketPriceData": true,
	                    "R/D": result.dataType,
	                    "MSTicker": gKey,
	                    "LastMarket": oExch //set LastMarket to sub-exchanges, beacause there is no LastMarket at sub-exchanges -- David 20120831 for Disnat
	                };
	
	                for (var j = 0, len = fieldsForItem.length; j < len; j++) {
	                    if (fieldsForItem[j].name == 'ClosePrice' && fieldsForItem[j].value == 0) { // if closeprice is 0, we will ignore it.
	                        continue;
	                    }
	                    marketPriceMap[fieldsForItem[j].name] = fieldsForItem[j].value || '';
	                }
	
	                //step 1:check date time paris
	                //remove Date/Time checking for SDK --- if ((typeof marketPriceMap['Date'] != 'undefined' ^ typeof marketPriceMap['Time'] != 'undefined')||
	                //DateTimeChecked is used for MF tickers, because there is only date about the MF ticker
	                if (!marketPriceMap['DateTimeChecked']) {
	                    if ((typeof marketPriceMap['TradeDate'] != 'undefined' ^ typeof marketPriceMap['TradeTime'] != 'undefined') ||
	                        (typeof marketPriceMap['PostDate'] != 'undefined' ^ typeof marketPriceMap['PostTime'] != 'undefined') ||
	                        (typeof marketPriceMap['PreDate'] != 'undefined' ^ typeof marketPriceMap['PreTime'] != 'undefined')) {
	                        continue;
	                    }
	                    marketPriceMap['DateTimeChecked'] = null;
	                    delete marketPriceMap['DateTimeChecked'];
	                }
	
	                //step: reset name with S3377
	                if (marketPriceMap["S3377"]) {
	                    marketPriceMap["Name"] = marketPriceMap["S3377"];
	                    delete marketPriceMap["S3377"];
	                }
	
	                //step: reset ask/bid market by using lastmarket if they are not exist.
	                setAskBidMarket(marketPriceMap);
	
	                //step: reset close price for special exchange
	                if (parseFloat(marketPriceMap.ClosePrice, 10) <= 0 && (result.serviceName == "77" || result.serviceName == "75") && result.symbolType == "3") { //77exchange and futrue
	                    marketPriceMap.ClosePrice = marketPriceMap.LastPrice;
	                }
	
	                //step: reset trade price with Bid for Forex(secType: 20)
	                if (result.symbolType == '20') {
	                    if (typeof marketPriceMap.LastPrice === 'undefined') {
	                        marketPriceMap.LastPrice = marketPriceMap.BidPrice;
	                    }
	                    if (typeof marketPriceMap.Spread === 'undefined') {
	                        marketPriceMap.Spread = parseFloat(marketPriceMap.BidPrice, 10) - parseFloat(marketPriceMap.AskPrice, 10)
	                    }
	                    if (!marketPriceMap.TradeDate) {
	                        marketPriceMap.TradeDate = marketPriceMap.BidDate;
	                    }
	                    if (!marketPriceMap.TradeTime) {
	                        marketPriceMap.TradeTime = marketPriceMap.BidTime;
	                    }
	
	                }
	
	                //step: reset the open/closed/halted status
	                if (marketPriceMap.TickerStatus) {
	                    marketPriceMap.Indicator = 'Halted';
	                } else if (marketPriceMap.SessionStatus) {
	                    marketPriceMap.PrePostTxt = getMarketState(marketPriceMap) === MARKETSTATE.PRE ? 'Pre-Market' : (getMarketState(marketPriceMap) === MARKETSTATE.POST ? 'After Hours' : '');
	                    marketPriceMap.OpenClose = 'Closed';
	                    if (getMarketState(marketPriceMap) === MARKETSTATE.CLOSED || getMarketState(marketPriceMap) === MARKETSTATE.POST) {
	                        marketPriceMap.Indicator = 'Closed';
	                    } else if (getMarketState(marketPriceMap) === MARKETSTATE.PRE || getMarketState(marketPriceMap) === MARKETSTATE.OPEN) {
	                        marketPriceMap.Indicator = 'Regular';
	                        if (getMarketState(marketPriceMap) === MARKETSTATE.OPEN) {
	                            marketPriceMap.OpenClose = 'Open';
	                        }
	                    }
	                    if (getMarketState(marketPriceMap) === MARKETSTATE.OPEN) {
	                        marketPriceMap.OpenClose = 'Open';
	                    } else {
	                        marketPriceMap.OpenClose = 'Closed';
	                    }
	                }
	
	                //step: calculate to reset the trade change(%), post change(%) and pre change(%)
	                setChanges(cachedData, marketPriceMap, 'Pre');
	                setChanges(cachedData, marketPriceMap);
	                setChanges(cachedData, marketPriceMap, 'Post');
	
	                //step: merge pre and post as a new point
	                setPrePostExtendFileds(marketPriceMap);
	
	                var needUpdateField = {},
	                    noneedfillgdata;
	                for (var fieldName in marketPriceMap) {
	                    var ty = this.dynamicTokens[fieldName] || 's';
	                    noneedfillgdata = false;
	                    switch (fieldName) {
	                        case 'TradeDate':
	                        case 'Date':
	                        case 'PrePostDate':
	                        case 'PostDate':
	                        case 'PreDate':
	                            ty = 's';
	                            marketPriceMap[fieldName] = formatDateString(marketPriceMap[fieldName]);
	                            break;
	                        case 'ClosePriceDate':
	                            ty = 's';
	                            marketPriceMap[fieldName] = marketPriceMap[fieldName].replace(/(\d{4})(\d{2})(\d{2})/, "$2-$3-$1");
	                            break;
	                        case 'ExpirationDate':
	                            marketPriceMap[fieldName] = marketPriceMap[fieldName].split("-").reverse().join("-");
	                            break;
	                        case 'TradeTime':
	                        case 'LastVolume':
	                        case 'Time':
	                        case 'PrePostTime':
	                        case 'PostTime':
	                        case 'PreTime':
	                            ty = 'i';
	                            marketPriceMap[fieldName] = parseInt(marketPriceMap[fieldName], 10);
	                            break;
	                        case 'LastPrice':
	                        case 'ClosePrice':
	                        case 'PrePostClosePrice':
	                        case 'PrePostPrice':
	                        case 'PostPrice':
	                        case 'PrePrice':
	                            ty = 'f';
	                            marketPriceMap[fieldName] = parseFloat(marketPriceMap[fieldName], 10);
	                            break;
	                        case 'Volume':
	                        case 'DisnatVolume':
	                            ty = 'i';
	                            marketPriceMap[fieldName] = parseFloat(marketPriceMap[fieldName], 10);
	                            break;
	                        case 'AskMarket':
	                        case 'BidMarket':
	                        case 'AskSize':
	                        case 'AskPrice':
	                        case 'BidSize':
	                        case 'BidPrice':
	                            noneedfillgdata = result.symbolType != "20"; //fix forex calculate chg bugs
	                            break;
	                        case 'Name':
	                            if (result.symbolType == 3) {
	                                marketPriceMap[fieldName] = _formatContactName(marketPriceMap[fieldName]);
	                            }
	                            break;
	                        case 'HighDate52W':
	                        case 'LowDate52W':
	                            marketPriceMap[fieldName] = marketPriceMap[fieldName].replace(/(\d{2})\.(\d{2})\.(\d{4})/, "$2-$1-$3");
	                            break;
	                        case 'High52W':
	                        case 'Low52W':
	                            marketPriceMap[fieldName] = parseFloat(marketPriceMap[fieldName], 10);
	                            break;
	                        case 'MiddlePrice':
	                        case 'HighPrice':
	                        case 'LowPrice':
	                        case 'VWAP':
	                        case 'OpenPrice':
	                        case 'NetChange':
	                        case "NAV":
	                            ty = 'f';
	                            marketPriceMap[fieldName] = parseFloat(marketPriceMap[fieldName], 10);
	                            break;
	                        case '-501':
	                            marketPriceMap[fieldName] = (marketPriceMap[fieldName] || 0) % 1000; //ensure milliseconds exclude seconds 				
	                            break;
	                        case 'D30':
	                            marketPriceMap["Transactions"] = marketPriceMap[fieldName];
	                            fieldName = "Transactions";
	                            break;
	                    }
	                    if (!noneedfillgdata && marketPriceMap[fieldName] != cachedData[fieldName] || this.l2Tokens[fieldName]) { //l2 data will send in pair
	                        needUpdateField[fieldName] = { value: marketPriceMap[fieldName], oValue: cachedData[fieldName], dataType: ty };
	                        if (fieldName == "Name") {
	                            if (marketPriceMap[fieldName]) {
	                                if (!cachedData["Name"]) {
	                                    cachedData["Name"] = marketPriceMap[fieldName];
	                                } else {
	                                    delete needUpdateField[fieldName];
	                                }
	                            }
	                        } else {
	                            cachedData[fieldName] = marketPriceMap[fieldName];
	                        }
	                    }
	                }
	
	                //we need handle ask/bid as the same market
	                handleAskBidData(cachedData, marketPriceMap, needUpdateField);
	
	                if (result.symbolType == 7 && oExch == 71) { //For bond, use D942 as trade time
	                    handleForBond(cachedData, marketPriceMap, needUpdateField);
	                }
	
	                //step: update UI
	                if (!$.isEmptyObject(needUpdateField)) {
	                    QSAPI.Cache.setMarketData(sourceType, marketPriceMap["R/D"], gKey, cachedData);
	                    if (typeof this.callbacks.onDataUpdate == 'function') {
	                        needUpdateField["dataExch"] = { value: oExch, dataType: "i" }; // the exchange data from.
	                        needUpdateField["sourceType"] = sourceType;
	                        needUpdateField["RDType"] = marketPriceMap["R/D"];
	                        this.callbacks.onDataUpdate(gKey, needUpdateField);
	                    }
	                }
	            }
	        };
	        this.getWidget = function(type, id) {
	            if (this.widgets[type] && this.widgets[type][id.toString()]) {
	                return this.widgets[type][id.toString()];
	            } else {
	                return {};
	            }
	        };
	        this.getAllRegistedWidgets = function(type) {
	            return this.widgets[type];
	        };
	        this.register = function(obj) {
	            if (typeof obj.type == 'undefined' || obj.type == null) {
	                obj.type = this.defaultModuleType;
	            }
	            if (typeof obj.id == 'undefined' || obj.id == null) {
	                obj.id = this._idServer(obj.type.toString());
	            }
	            if (!this.widgets[obj.type.toString()]) {
	                this.widgets[obj.type.toString()] = {};
	            }
	            this.widgets[obj.type.toString()][obj.id] = obj;
	        };
	
	        this.unregister = function(obj) {
	            delete this.widgets[obj.type.toString()][obj.id];
	        };
	
	        this._idServer = function(type) {
	            var id = this._id--;
	            return type + "_" + id;
	        };
	
	        var handlePrePostTokensAsUpdateField = function(dynamicTokens, prePostTokens, d, isMergePrePost) {
	            var needUpdateField = {};
	            if (!d) return;
	            if (isMergePrePost) {
	                for (var k in prePostTokens) {
	                    var nk = prePostTokens[k];
	                    if (typeof d[nk] == 'undefined') continue;
	                    needUpdateField[k] = {
	                        value: d[nk],
	                        oValue: null,
	                        dataType: dynamicTokens[k]
	                    }
	                }
	
	            }
	            return needUpdateField;
	        };
	
	        this.forceUpdateQuoteData = function(component, tickerObjects) {
	            if (typeof component.updateData != 'function') {
	                return;
	            }
	            var isMergePrePost = Util.isMergePrePost(component.mergePrePost),
	                tickerObject,
	                subscribeTicker,
	                RDType;
	            for (var s = 0; s < tickerObjects.length; s++) {
	                tickerObject = tickerObjects[s];
	                subscribeTicker = QSAPI.DataManager.getSubscribeTicker(tickerObject, component);
	                RDType = QSAPI.DataManager.getRDType(tickerObject);
	                var gd = QSAPI.Cache.getMarketData(component.dataType, Util.getValidRDType(component.forceDelay, RDType), subscribeTicker);
	                if (typeof gd == 'undefined') continue;
	                var needUpdateField = handlePrePostTokensAsUpdateField(this.dynamicTokens, this.prePostTokens, gd, isMergePrePost);
	                for (var k in this.dynamicTokens) {
	                    if (needUpdateField[k]) continue;
	                    if (typeof gd[k] != 'undefined') {
	                        needUpdateField[k] = {
	                            value: gd[k],
	                            oValue: null,
	                            dataType: this.dynamicTokens[k]
	                        }
	                    }
	                }
	                if ($.isEmptyObject(needUpdateField)) {
	                    continue;
	                }
	                component.updateData(tickerObject, needUpdateField);
	            }
	        };
	
	        /**Start to get fund data from XOI**/
	        this.mfFieldsMapping = {
	            "nav": ["LastPrice"],
	            "chg": ["Chg"],
	            "%chg": ["Chg%"],
	            "preNav": ["ClosePrice"],
	            "navDate": ["TradeDate", "Date"],
	            "preNavDate": ["ClosePriceDate"]
	        };
	
	        this._formatFundDataFromXOI = function(o, mfSecIdGkeyMapping) {
	            var items = [],
	                data = o.data;
	            if (data && data.length > 0) {
	                var d, secId, gkey, values, fields;
	                for (var i = 0, l = data.length; i < l; i++) {
	                    fields = [];
	                    d = data[i];
	                    secId = d['secId'];
	                    if (!secId || secId == '') continue;
	                    gkey = mfSecIdGkeyMapping[secId];
	                    if (!gkey) continue;
	                    gkey = gkey.split(".");
	                    values = d.values || {};
	                    for (var field in values) {
	                        if (values[field] == '') continue;
	                        var mapKeys = this.mfFieldsMapping[field] || [];
	                        for (var j = 0, jl = mapKeys.length; j < jl; j++) {
	                            fields.push({
	                                "name": mapKeys[j],
	                                "value": values[field]
	                            })
	                        }
	                    }
	                    if (fields.length > 0) {
	                        fields.push({
	                            "name": "DateTimeChecked",
	                            "value": true
	                        })
	                        items.push({
	                            source: 'D',
	                            service: gkey[0],
	                            symbol: '8_' + gkey[2] + '_-1',
	                            fields: fields
	                        })
	                    }
	                }
	            }
	            return { items: items };
	        };
	        this._queryFundData = function(secIds, callback) {
	            Util.getJSON(QSAPI.getQSPath() + "getFundNavData.jsp?cb=?", { secIds: secIds }, callback);
	        };
	        this.getFundDataFromXOI = function(secIds, mfSecIdGkeyMapping, callback) {
	            var self = this,
	                batchQuerySize = 80;
	            if (secIds.length > batchQuerySize) {
	                var subSecIdsStr = [];
	                var c = Math.ceil(secIds.length / batchQuerySize);
	                var cbNum = 0;
	                var returnData = [];
	                for (var i = 0; i < c; i++) {
	                    subSecIdsStr.push(secIds.slice(batchQuerySize * i, batchQuerySize * (i + 1)).join(','));
	                    this._queryFundData(subSecIdsStr[i], function(result) {
	                        cbNum++;
	                        returnData = returnData.concat(result.data || []);
	                        if (cbNum >= c) {
	                            callback(self._formatFundDataFromXOI({ data: returnData }, mfSecIdGkeyMapping));
	                        }
	                    })
	                }
	            } else {
	                this._queryFundData(secIds.join(','), function(result) {
	                    callback(self._formatFundDataFromXOI(result, mfSecIdGkeyMapping));
	                });
	            }
	        };
	        /**End to get fund data from XOI**/
	
	        this.getSNAPQuoteData = function(component, tickerObjects, pullObj, callback) {
	            var pullKeys = [],
	                mfSecIds = [],
	                pullKey, secId, gkey, tickerObject, snapQuoteData = [],
	                indexMap = {},
	                mfSecIdGkeyMapping = {},
	                gkeySubTickerMap = {},
	                forceDelay = Util.getValidForceDelay(component && component.subscribeID ? component.forceDelay : false),
	                mergePrePost = Util.isMergePrePost(component ? component.mergePrePost : false);
	            try {
	                for (var i = 0, l = tickerObjects.length; i < l; i++) {
	                    snapQuoteData[i] = {};
	                    tickerObject = tickerObjects[i];
	                    gkey = tickerObject.getField('gkey');
	                    if (gkey) {
	                        if (!indexMap[gkey]) {
	                            indexMap[gkey] = [];
	                        }
	                        indexMap[gkey].push(i);
	                    }
	                    //for mf ticker, we need get the quote data from XOI
	                    if (Util.checkFund(tickerObject)) {
	                        secId = tickerObject.getField('secId') || tickerObject.getField('performanceId');
	                        if (secId && secId != '') {
	                            mfSecIds.push(secId);
	                            if (!mfSecIdGkeyMapping[secId]) {
	                                mfSecIdGkeyMapping[secId] = gkey;
	                            }
	                        }
	                    }
	                    if (tickerObject.getField('tenforeCode') != null && tickerObject.getField('tenforeCode') != "" && tickerObject.getField('type') != null && tickerObject.getField('type') != "" && tickerObject.getField('tenforeTicker') != null && tickerObject.getField('tenforeTicker') != "") {
	                        if (component && component.subscribeID) {
	                            pullKey = QSAPI.DataManager.getSubscribeTicker(tickerObject, component);
	                        } else {
	                            pullKey = Util.generateGKey(tickerObject);
	                        }
	                        gkeySubTickerMap[gkey] = pullKey;
	                        pullKeys.push(pullKey);
	                    }
	
	                }
	            } catch (e) {
	
	            }
	            var self = this,
	                cbNum = 0,
	                readyStatus = {};
	
	            if (pullKeys.length == 0 && mfSecIds.length == 0) {
	                callback(snapQuoteData);
	            } else if (pullKeys.length == 0) {
	                readyStatus['PULL'] = true;
	                this.getFundDataFromXOI(mfSecIds, mfSecIdGkeyMapping, function(o) {
	                    self.updateMarketPrice(o, "pull");
	                    readyStatus['XOI'] = true;
	                    self._onSnapQuoteDataArrived(callback, readyStatus, snapQuoteData, indexMap, forceDelay, gkeySubTickerMap, mergePrePost);
	                });
	            } else if (mfSecIds.length == 0) {
	                readyStatus['XOI'] = true;
	                pullObj.query(pullKeys, forceDelay, function(o) {
	                    self.updateMarketPrice(o, "pull");
	                    readyStatus['PULL'] = true;
	                    self._onSnapQuoteDataArrived(callback, readyStatus, snapQuoteData, indexMap, forceDelay, gkeySubTickerMap, mergePrePost);
	                }, component);
	            } else {
	                this.getFundDataFromXOI(mfSecIds, mfSecIdGkeyMapping, function(o) {
	                    self.updateMarketPrice(o, "pull");
	                    readyStatus['XOI'] = true;
	                    self._onSnapQuoteDataArrived(callback, readyStatus, snapQuoteData, indexMap, forceDelay, gkeySubTickerMap, mergePrePost);
	                });
	                pullObj.query(pullKeys, forceDelay, function(o) {
	                    self.updateMarketPrice(o, "pull");
	                    readyStatus['PULL'] = true;
	                    self._onSnapQuoteDataArrived(callback, readyStatus, snapQuoteData, indexMap, forceDelay, gkeySubTickerMap, mergePrePost);
	                }, component);
	            }
	        };
	
	        this._onSnapQuoteDataArrived = function(callback, readyStatus, snapQuoteData, indexMap, forceDelay, gkeySubTickerMap, mergePrePost) {
	            if (!readyStatus['XOI'] || !readyStatus['PULL']) return;
	            for (var gkey in indexMap) {
	                var RDType = QSAPI.DataManager.getRDType(QSAPI.Cache.getTickerData(gkey));
	                var gd = QSAPI.Cache.getMarketData("pull", Util.getValidRDType(forceDelay, RDType), gkeySubTickerMap[gkey]);
	                if (!gd) continue;
	                var qd = {},
	                    needUpdateFields = handlePrePostTokensAsUpdateField(this.dynamicTokens, this.prePostTokens, gd, mergePrePost);
	                for (var k in this.dynamicTokens) {
	                    if (needUpdateFields[k]) {
	                        qd[k] = needUpdateFields[k].value;
	                    } else if (typeof gd[k] != 'undefined') {
	                        qd[k] = gd[k];
	                    }
	                }
	                for (var i = 0, l = indexMap[gkey].length; i < l; i++) {
	                    snapQuoteData[indexMap[gkey][i]] = qd;
	                }
	            }
	            if (typeof callback == 'function') {
	                callback(snapQuoteData);
	            }
	        };
	
	        var _getCacheKey = function(secid, tenforeinfo) {
	            return Util.formatString("{0}|{1}", secid || tenforeinfo || "", tenforeinfo || "");
	        };
	        var getStaticDataFromCache = function(staticData, mapIndex, mapSIdGkey, tokens) {
	            var d = {},
	                idx, gkey;
	            for (var sid in mapSIdGkey) {
	                gkey = mapSIdGkey[sid]; //sid maybe is secid or tenfore
	                idx = mapIndex[sid];
	                d[idx[0]] = {};
	                if (gkey && QSAPI.Cache.getStaticData(gkey)) {
	                    for (var j = 0, jl = tokens.length; j < jl; j++) {
	                        var token = tokens[j];
	                        var value = QSAPI.Cache.getStaticData(gkey, token);
	                        if (typeof value == 'undefined') continue;
	                        d[idx[0]][token] = value;
	                    }
	                }
	                if (idx.length > 0) {
	                    for (var i = 0, l = idx.length; i < l; i++) {
	                        staticData[idx[i]] = d[idx[i]];
	                    }
	                }
	            }
	        };
	
	        var _formatContactName = function(name) {
	            return $.trim(name.replace(/(.+)\s*CONT\s*\(.+\)*/, "$1"));
	        };
	
	        this._cacheStaticData = function(mapSIdGkey, d) {
	            var mkey, gkey, dr, ecode;
	            for (var i = 0; i < d.length; i++) { // store static data to cache.
	                mkey = _getCacheKey(d[i].secId, d[i].tfTicker);
	                gkey = mapSIdGkey[mkey];
	                if (!gkey)
	                    continue;
	                dr = {};
	                d[i]["staticData"] = true;
	                ecode = Util.getExchByGkey(gkey);
	                for (var j in d[i]) {
	                    if (j == "pd003" || j == "pd005" || j == "pd007" || j == "pd009" || j == "pd00b" || j == "pd00d" || j == "pd00f" ||
	                        j == "pd00h" || j == "pd014" || j == "pm493" || j == "pm494" || j == "pm495") { // convert to two decimal for return value.
	                        dr[j] = isNaN(d[i][j]) ? "" : parseFloat(d[i][j]).toFixed(2);
	                    } else {
	                        dr[j] = d[i][j];
	                    }
	                }
	                if (dr['rr01y']) { //merge fund's Rating to ST's st200
	                    dr['st200'] = dr['rr01y'];
	                }
	
	                //start to merge 52 wk data points and use the RTD data points preferentially.
	                var isFund = dr["mType"] != "FE" && dr["mType"] != "FC";
	                //52 wk high price
	                if (typeof dr['S695'] != 'undefined') {
	                    dr['High52W'] = dr['S695'];
	                } else if (isFund && typeof dr['os70c'] != 'undefined') {
	                    dr['High52W'] = dr['os70c'];
	                }
	                //52 wk high date
	                if (typeof dr['S696'] != 'undefined') {
	                    dr['HighDate52W'] = dr['S696'].substring(4, 6) + "-" + dr['S696'].substring(6, 8) + "-" + dr['S696'].substring(0, 4);
	                } else if (isFund && typeof dr['os70d'] != 'undefined') {
	                    dr['HighDate52W'] = dr['os70d'];
	                }
	                //52 wk low
	                if (typeof dr['S697'] != 'undefined') {
	                    dr['Low52W'] = dr['S697'];
	                } else if (isFund && typeof dr['os70e'] != 'undefined') {
	                    dr['Low52W'] = dr['os70e'];
	                }
	                //52 wk low date
	                if (typeof dr['S698'] != 'undefined') {
	                    dr['LowDate52W'] = dr['S698'].substring(4, 6) + "-" + dr['S698'].substring(6, 8) + "-" + dr['S698'].substring(0, 4);
	                } else if (isFund && typeof dr['os70f'] != 'undefined') {
	                    dr['LowDate52W'] = dr['os70f'];
	                }
	                //console.log(dr['S3080'] + " " + dr['st476']);
	                if (typeof dr['S3080'] != 'undefined' && typeof dr['st476'] == 'undefined') {
	                    dr['st476'] = formatRTDateString(dr['S3080']);
	                }
	                if (typeof dr['S1315'] != 'undefined' && typeof dr['st159'] == 'undefined') {
	                    dr['st159'] = dr['S1315'];
	                }
	                // if (typeof dr['S1077'] != 'undefined' && typeof dr['st412'] == 'undefined') {
	                //     dr['st412'] = dr['S1077'];
	                // }
	                if (typeof dr['S1314'] != 'undefined' && typeof dr['st160'] == 'undefined') {
	                    dr['st160'] = dr['S1314'] / 1000000;
	                }
	                if (typeof dr['S529'] != 'undefined' && typeof dr['st263'] == 'undefined') {
	                    dr['st263'] = dr['S529'];
	                }
	                if (typeof dr['D1968'] != 'undefined' && typeof dr['st159'] == 'undefined') {
	                    dr['st159'] = dr['D1968'] / 1000000;
	                }
	
	                if (dr['la03z']) { //map to key
	                    dr['la03z'] = this.LA03Z[dr['la03z']] || '';
	                }
	                if (dr['os378']) { //map to string - david
	                    dr['os378Ori'] = dr['os378'];
	                    dr['os378'] = this.Mo[dr['os378'] - 1] || '';
	                }
	                if (ecode == "27") { // MSCI indexs
	                    dr["LastPrice"] = dr["os065"] == "" ? "" : parseFloat(dr["os065"]); // daily price to last price.
	                    dr["Chg%"] = dr["pd003"] == "" ? "" : parseFloat(dr["pd003"]);
	                    dr["Chg"] = (dr["LastPrice"] != "" && dr["Chg%"] != "") ? dr["LastPrice"] * dr["Chg%"] / 100 : "";
	                }
	                if (dr["mType"] == "ST" && dr["currency"] == "GBP") { // change the currency from GBP to GBX for stock in Lodon exchanges.
	                    dr["currency"] = "GBX";
	                    dr["High52W"] = dr["st168"] * 100; // 52 w high
	                    dr["Low52W"] = dr["st169"] * 100; // 52 w low;
	                }
	                if (dr['S547']) {
	                    dr['CouponRate'] = dr["S547"];
	                    delete dr["S547"];
	                }
	                if (dr['S1527']) {
	                    dr['IssueType'] = dr["S1527"];
	                    delete dr["S1527"];
	                }
	                if (dr['S1012']) {
	                    dr['CUSIP'] = dr["S1012"];
	                    delete dr["S1012"];
	                }
	                
	                if(dr['S1776']){
	                    dr['S676'] = dr['S1776'];
	                }
	
	                if (!dr["Name"] && (dr["S3377"] || dr["S12"] || dr["S31"])) {
	                    dr["Name"] = dr["S3377"] || Util.formatNameString(dr["S12"]) || _formatContactName(dr["S31"]);
	                }
	                QSAPI.Cache.setStaticData(gkey, dr);
	            }
	        }
	
	        this._handlerStaticData = function(staticData, mapIndex, mapSIdGkey, remoteData, callback) {
	            this._cacheStaticData(mapSIdGkey, remoteData);
	            if (typeof callback == 'function') {
	                getStaticDataFromCache(staticData, mapIndex, mapSIdGkey, [].concat(this.staticTokens, this.tenforeStaticToken.split(",")));
	                callback(staticData);
	            }
	        };
	        /**
	         **@para [QSAPI.Ticker,QSAPI.Ticker]
	         */
	
	        this._getStaticData = function(secIds, maxCount, callback) {
	            if (!secIds || secIds.length == 0) {
	                callback([]);
	            } else {
	                if (secIds.length > maxCount) {
	                    var subSecIdsStr = [];
	                    var c = Math.ceil(secIds.length / maxCount);
	                    var cbNum = 0;
	                    var returnArray = [];
	                    for (var i = 0; i < c; i++) {
	                        subSecIdsStr.push(secIds.slice(maxCount * i, maxCount * (i + 1)).join(','));
	                        _getTenforeStaticData(subSecIdsStr[i], {
	                            onSuccess: function(o) {
	                                cbNum++;
	                                returnArray = returnArray.concat(o.data || []); //merge the return
	                                if (cbNum >= c) {
	                                    callback(returnArray);
	                                }
	                            },
	                            onFailure: function(o) {
	                                callback([]);
	                            }
	                        });
	                    }
	                } else {
	                    _getTenforeStaticData(secIds.join(','), {
	                        onSuccess: function(o) {
	                            callback(o.data || []);
	                        },
	                        onFailure: function(o) {
	                            callback([]);
	                        }
	                    });
	                }
	            }
	        };
	        this.getStaticData = function(component, tickerObjects, callback) {
	            var self = this;
	            if (tickerObjects.length == 0) {
	                callback([]);
	            } else {
	                var t, tenfore, key = "",
	                    secIds = [],
	                    secId,
	                    uniqueSIds = {},
	                    staticData = [],
	                    mapIndex = {},
	                    mapSIdGkey = {};
	                for (var i = 0, l = tickerObjects.length; i < l; i++) {
	                    t = tickerObjects[i];
	                    secId = Util.getSecID(t);
	                    tenfore = component ? QSAPI.DataManager.getSubscribeTicker(t, component) : Util.generateGKey(t);
	                    if (secId || tenfore) { //secId or tenfore
	                        key = _getCacheKey(secId, tenfore);
	                        if (!mapIndex[key]) {
	                            mapIndex[key] = [];
	                        }
	                        mapIndex[key].push(i);
	                        mapSIdGkey[key] = tenfore;
	                        if (!uniqueSIds[secId]) {
	                            uniqueSIds[secId] = true;
	                            if (!secId) {
	                                secIds.push(Util.formatString("|{0}", tenfore));
	                            } else if (!tenfore) {
	                                secIds.push(secId);
	                            } else {
	                                secIds.push(Util.formatString("{0}|{1}", secId, tenfore));
	                            }
	                        }
	                    }
	                    staticData[i] = {}; //for keep the length and order
	                }
	
	                if (secIds.length == 0) {
	                    self._handlerStaticData(staticData, mapIndex, mapSIdGkey, [], callback);
	                    return;
	                }
	
	                var maxCount = 50;
	                this._getStaticData(secIds, maxCount, function(returnArray) {
	                    self._handlerStaticData(staticData, mapIndex, mapSIdGkey, returnArray, callback);
	                });
	            }
	        };
	
	        this.destory = function() {
	
	        };
	        this.getNeedUpdateFields = function(component, gkey, marketData) {
	            var cacheData = QSAPI.Cache.getMarketDataByGkey(component, gkey),
	                needUpdateFields = {};
	            for (var field in marketData) {
	                // if (marketData[field] != cacheData[field]) {
	                needUpdateFields[field] = { value: marketData[field], oValue: cacheData[field], dataType: this.dynamicTokens[field] || "s" };
	                // }
	            }
	            return needUpdateFields;
	        }
	    };
	})(QSAPI);
	(function(QSAPI) {
	    'use strict';
	    var $ = QSAPI.$;
	    var Util = QSAPI.Util;
	
	    var FIELDS = {
	            "D2": "LastPrice",
	            "D782": "Date",
	            "D943": "Time",
	            "D784": "TradeDate",
	            "D770": "TradeTime",
	            "D3": "LastVolume",
	            "D4": "BidPrice",
	            "D5": "BidSize",
	            "D6": "AskPrice",
	            "D7": "AskSize",
	            "D8": "BidMarket",
	            "D12": "AskMarket",
	            "D16": "Volume",
	            "D33": "Volume",
	            "D17": "OpenPrice",
	            "D18": "HighPrice",
	            "D19": "LowPrice",
	            "D1978": "ClosePrice",
	            "D799": "ClosePriceDate",
	            "D954": "D954,SessionStatus",
	            "D774": "PrePrice",
	            "D934": "PreDate",
	            "D775": "PreTime",
	            "D776": "PostPrice",
	            "D935": "PostDate",
	            "D777": "PostTime",
	            "D791": "BidDate",
	            "D792": "AskDate",
	            "D940": "BidTime",
	            "D941": "AskTime",
	            "D21": "OpenInt",
	            "D57": "MiddlePrice",
	            "D98": "NAV",
	            "D99": "OfferPrice",
	            "D103": "NAVChange",
	            "D214": "LastMarket",
	            "D243": "VWAP",
	            "D2124": "Gid",
	            "S214": "S214",
	            "S676": "MIC-CODE",
	            "S1776": "MIC-CODE",
	            "S9": "Currency",
	            "S13": "ACNI",
	            "S1683": "MSSymbol",
	            "D138": "D138",
	            "D422": "D422",
	            "D506": "D506",
	            "D30": "D30",
	            "S31": "S31,ContractName",
	            "H10": "ExpirationDate",
	            "H14": "NetChange",
	            "D2374": "High52W",
	            "D2382": "HighDate52W",
	            "D2375": "Low52W",
	            "D2383": "LowDate52W",
	            "S1734": "S1734",
	            "S3377": "S3377",
	            "D909": "D909",
	            "D910": "D910",
	            "D911": "D911",
	            "D912": "D912",
	            "D913": "D913",
	            "D914": "D914",
	            'D2367': "D2367",
	            "H2": "H2",
	            "D2008": "D2008"
	        },
	        DATE_TIME_FIELDS = {
	            "D784_D770": "R", //Date and time of Last Trade, EST
	            "D782_D943": "RD", //Date and time of Last Update of any Real-time Field, EST
	            "D792_D941": "RD", //Date and time of Ask, EST
	            "D791_D940": "RD", //Date and time of Bid, EST
	            "D934_D775": "R", //Date and time PreTrade, EST
	            "D935_D777": "R" //Date and time Post Trade, EST
	        },
	        OVERRIDE_FIELDS = {
	            "Name": "S3377,S31,S12"
	        };
	
	    function getDateTimeFields(field) {
	        for (var key in DATE_TIME_FIELDS) {
	            if ($.inArray(field, key.split('_')) > -1) {
	                return key;
	            }
	        }
	        return '';
	    }
	
	    function fillDateTimeFields(ret, dateTime, fieldArray) {
	        if (dateTime.date) {
	            ret.push({
	                "name": FIELDS[fieldArray[0]],
	                "value": dateTime.date
	            });
	        }
	        if (dateTime.time) {
	            ret.push({
	                "name": FIELDS[fieldArray[1]],
	                "value": dateTime.time
	            });
	        }
	    }
	
	
	    function handleDateTimeFields(result, fields) {
	        var ret = [];
	        var fieldArray = fields.split('_');
	        if (result.D2367 !== 'EST') {
	            convertTimezone(ret, result, fieldArray);
	        } else {
	            convertRealtimeToDelay(ret, result, fieldArray);
	        }
	        return ret;
	    }
	
	    function convertTimezone(ret, result, fieldArray) {
	        //for those have D2367 is not EST
	        var estTimeDiff = Util.getESTOffsetByMinutes([result.H2, result.H3, result.H1].join('.'));
	        if (typeof estTimeDiff === 'undefined') {
	            convertRealtimeToDelay(ret, result, fieldArray);
	        } else {
	            if (result[fieldArray[0]]) {
	                var date = Util.formatRTDateTimeAsDate(result[fieldArray[0]], result[fieldArray[1]]);
	                if (date) {
	                    date = new Date(date.getTime() + estTimeDiff * 60000);
	                    result[fieldArray[0]] = Util.formatDateString(date, 'date');
	                    result[fieldArray[1]] = Util.formatDateString(date, 'time', true);
	                    convertRealtimeToDelay(ret, result, fieldArray);
	                }
	            } else if (result[fieldArray[1]]) {
	                convertRealtimeToDelay(ret, result, fieldArray);
	            }
	        }
	    }
	
	    function convertRealtimeToDelay(ret, result, fieldArray) {
	        //for R date time fields
	        var dateTime = Util.formatRTDateTimeAsString(result[fieldArray[0]], result[fieldArray[1]]);
	        if (result.D2008 !== 'R' && DATE_TIME_FIELDS[fieldArray.join('_')] === 'R') { //for non-real time permission, need make Realtime Datetime to delayed
	            var delayMinutes = Util.getDelayMinutes([result.H2, result.H3, result.H1].join('.'));
	            dateTime.time -= delayMinutes * 60;
	        }
	        fillDateTimeFields(ret, dateTime, fieldArray);
	    }
	
	    function convertFields(result) {
	        var fields = [];
	        var dateTimeFields = {};
	        for (var field in result) {
	            if (!result[field]) {
	                continue;
	            }
	            var outKey = FIELDS[field];
	            if (outKey) {
	                $.each(outKey.split(','), function(i, k) {
	                    var dtFields = getDateTimeFields(field);
	                    if (dtFields.length === 0) {
	                        fields.push({
	                            "name": $.trim(k),
	                            "value": result[field]
	                        });
	                    } else {
	                        dateTimeFields[dtFields] = true;
	                    }
	                });
	            } else {
	                fields.push({
	                    "name": field,
	                    "value": result[field]
	                });
	            }
	        }
	        for (var dtKey in dateTimeFields) {
	            if (!dateTimeFields[dtKey]) {
	                continue;
	            }
	            fields = fields.concat(handleDateTimeFields(result, dtKey));
	        }
	        for (var key in OVERRIDE_FIELDS) {
	            var overFields = OVERRIDE_FIELDS[key].split(',');
	            for (var i = 0, l = overFields.length; i < l; i++) {
	                if (result[overFields[i]]) {
	                    fields.push({
	                        "name": key,
	                        "value": result[overFields[i]]
	                    });
	                    break;
	                }
	            }
	        }
	        return fields;
	    }
	
	
	    function fodQuote(result) {
	        var items = [];
	        result.quotes = result.quotes || {};
	        if (result.quotes.error) {
	            console.warn('morningstar.marketdata.quote.rtfodparser', 'RT data has error', {
	                error: result.quotes.error
	            });
	        }
	        $.each(result.quotes.results || [], function(i, r) {
	            items.push({
	                ticker: [r.H2, r.H3, r.H1].join('.'),
	                symbol: [r.H3, r.H1, -1].join('_'),
	                source: r.D2008 === 'R' ? 'R' : 'D', //D2008 will have R/D/E(EOD)
	                service: r.H2,
	                fields: convertFields(r)
	            });
	        });
	        return items;
	    }
	
	    if (!QSAPI.fod) {
	        QSAPI.fod = {};
	    }
	    QSAPI.fod.transformFodToQuoteData = fodQuote;
	})(QSAPI);
	(function(QSAPI) {
	    'use strict';
	    'use strict';
	    var $ = QSAPI.$;
	    var Util = QSAPI.Util,
	        readyDeferred = $.Deferred(),
	        socket = null,
	        readyDeferredError = {},
	        subscribeList = [],
	        globalStatus = Util.getGlobalStatus(),
	        callbacks = {},
	        callbackQuene = [],
	        dataHandler = null,
	        configs = {};
	
	    function formatMessage(message) {
	        var instrument = message[0];
	        if (/.+\.HEARTBEAT$/.test(instrument)) {
	            return null;
	        }
	        var instrumentArray = instrument.split('.'),
	            result = {
	                'H1': instrumentArray[2],
	                'H2': instrumentArray[0],
	                'H3': instrumentArray[1]
	            };
	        for (var i = 3, l = message.length, m; i < l; i++) {
	            m = message[i].split('=');
	            result[m[0]] = m[1];
	        }
	        if (subscribeList[instrument]) {
	            result['D2008'] = subscribeList[instrument]['D2008'];
	        }
	        if (!result['D770']) { //it is a quote message, then add message time stamp return  
	            result['D943'] = Util.formatRTDateTimeAsString(null, message[2]).time;
	            if (result['D2008'] !== 'R') {
	                result['D943'] -= Util.getDelayMinutes(instrument) * 60;
	            }
	            var gmtdiff = Util.getGMTOffsetMinutes(instrument);
	            var estDiff = Util.getESTOffsetMinutes(instrument);
	            if (typeof gmtdiff !== 'undefined' && typeof estDiff !== 'undefined') {
	                result['D943'] += (gmtdiff - estDiff) * 60;
	            }
	        }
	        result['DateTimeChecked'] = true;
	        var quoteParse = QSAPI.fod.transformFodToQuoteData;
	        return quoteParse({
	            'quotes': {
	                'results': [result]
	            }
	        });
	    }
	    var getStreamServer = function() {
	        var url = QSAPI.urlFODPushPath();
	        return QSAPI.Util.ajax({
	            module: 'stream',
	            dataType: 'xml',
	            timeout: 2000,
	            type: 'get',
	            url: url
	        })
	    };
	
	    var handleCallback = function(cbType, r) {
	        if (typeof callbacks[cbType] == 'function') {
	            callbacks[cbType](r);
	        }
	        if (callbackQuene && callbackQuene.length) {
	            for (var i = 0, n = callbackQuene.length; i < n; i++) {
	                if (typeof callbackQuene[i][cbType] == 'function') {
	                    callbackQuene[i][cbType](r);
	                    delete callbackQuene[i][cbType];
	                }
	            }
	        }
	    };
	    var createWebSocket = function(server, token) {
	        if (!server || !token) {
	            return;
	        }
	        if (!socket || socket.readyState === 3) {
	            var protocol = window.location.protocol === 'https:' ? 'wss://' : 'ws://';
	            socket = new WebSocket(protocol + server);
	            socket.onopen = function(event) {
	                socket.send('&authenticateSession&token=' + token);
	            };
	            socket.onmessage = function(event) {
	                var reg = /<.+>(.*)<\/.+>/ig,
	                    message;
	                if (reg.test(event.data)) {
	                    message = event.data.replace(reg, '$1').split('|');
	                }
	                //if it is 126.1.IBM format
	                if (/^\d+\.\d+\.\S+$/.test(message[0]) && message.length >= 3) {
	                    var data = formatMessage(message);
	                    dataHandler.updateMarketPrice({ items: data }, "push");
	                } else {
	                    //Info or Error message format
	                    if (message[0] === 'Successfully Authenticated with Server') {
	                        readyDeferred.resolve(socket);
	                        //need to subscribe the subscribed list
	                        handleCallback("onSuccess", {
	                            errorCode: '0',
	                            errorMsg: 'Connect fod streaming server successfully'
	                        });
	
	                    } else if (message[0] === 'Failed to Authenticate with Server') {
	                        socket.close();
	                        handleCallback("onFailure", globalStatus.NOAUTH);
	                    } else {
	                        console.log("stream server error message: " + message);
	                    }
	                }
	            };
	            socket.onerror = function(event) {
	                handleCallback("onFailure", globalStatus.CONNERRPUSH);
	                console.log('Fod stream connection error.');
	            };
	            socket.onclose = function(event) {
	                console.log('Fod stream connection close.');
	            };
	        }
	    };
	    var _connect = function() {
	        var tryCount = 0;
	        var connetStreaming = function() {
	            getStreamServer().done(function(result) {
	                result = parse(result);
	                createWebSocket(result.server, result.token);
	            }).fail(function(xhr, textStatus) {
	                if (tryCount < configs.tryCount) {
	                    tryCount++;
	                    connetStreaming();
	                } else {
	                    handleCallback("onFailure", globalStatus.CONNERRPUSH);
	                    console.log('Fod connection fail.');
	                }
	            });
	        };
	        connetStreaming();
	    };
	
	    function parse(result) {
	        var $result = $(result),
	            error = $result.find('error').text();
	        if (error.toLowerCase() === 'ok') {
	            return {
	                server: $result.find('stream1').text(),
	                token: $result.find('token').text()
	            }
	        } else if (error.toLowerCase().indexOf('error') > -1) {
	            console.log('Fod streaming connect error ' + error);
	            if (error.toLowerCase().indexOf('user already has a streaming session') > -1) {
	                handleCallback("onFailure", globalStatus.DUPLICATELOGINERR);
	            }
	        }
	        return {
	            server: '',
	            token: ''
	        };
	    }
	
	    function _subscribe(instruments) {
	        if (!instruments) {
	            console.log('Fod stream subscribe instruments is empty');
	        }
	        var needAdd = [];
	        $.each(instruments, function(i, instrument) {
	            if (!subscribeList.hasOwnProperty(instrument)) {
	                subscribeList[instrument] = {
	                    subsribeCount: 0
	                }
	                needAdd.push(instrument);
	                subscribeList[instrument].subsribeCount++;
	            }
	        });
	        if (needAdd.length) {
	            readyDeferred.done(function(socket) {
	                QSAPI.Util.ajax({
	                    dataType: 'json',
	                    timeout: 2000,
	                    type: 'get',
	                    url: Util.formatString('{0}?fields=H1,H2,H3,D2,D2008&JSONShort&instrument={1}', QSAPI.urlFODQuote(), needAdd.join(','))
	                }).done(function(result) {
	                    result = QSAPI.fod.transformFodToQuoteData(result);
	                    if (result && result.length > 0) {
	                        $.each(result, function(i, quoteData) {
	                            if (subscribeList[quoteData.ticker]) {
	                                for (var f = 0; f < quoteData.fields.length; f++) {
	                                    subscribeList[quoteData.ticker]['D2008'] = quoteData.source;
	                                }
	                            }
	                        });
	                    }
	                    socket.send('&AddToWatchWithSnap&StreamType=A&Instrument=' + needAdd.join(','));
	                }).fail(function() {
	                    socket.send('&AddToWatchWithSnap&StreamType=A&Instrument=' + needAdd.join(','));
	                });
	            });
	        }
	    };
	
	    var _unSubscribe = function(instruments) {
	        if (!instruments) {
	            console.log('morningstar.marketdata.stream unsubscribe instruments is empty');
	        }
	        var newDelete = [];
	        $.each(instruments, function(i, instrument) {
	            if (subscribeList.hasOwnProperty(instrument)) {
	                subscribeList[instrument].subsribeCount--;
	                if (subscribeList[instrument].subsribeCount <= 0) {
	                    delete subscribeList[instrument];
	                    newDelete.push(instrument);
	                }
	            }
	        });
	        if (newDelete.length) {
	            readyDeferred.done(function(socket) {
	                socket.send('&DeleteFromWatch&StreamType=A&Instrument=' + newDelete.join(','));
	            });
	        }
	    }
	    var _setCallbacks = function(callback) {
	        callbackQuene.push(callback);
	    };
	    if (!QSAPI.fod) {
	        QSAPI.fod = {};
	    }
	    QSAPI.fod.stream = function(cfg, dh, cbs) {
	        this.callBacks = callbacks = $.extend(true, {}, cbs);
	        this.dataHandler = dataHandler = dh;
	        this.configs = configs = $.extend(true, {}, cfg);
	        return {
	            subscribe: _subscribe,
	            unSubscribe: _unSubscribe,
	            connect: _connect,
	            setCallback: _setCallbacks
	        }
	    };
	})(QSAPI);
	(function(QSAPI) {
	    'use strict';
	    var $ = QSAPI.$;
	    QSAPI.Pull = function(dataHandler, callbacks, options) {
	        this._cacheRequest = {};
	        this.STATUS = {
	            START: 0,
	            PENDING: 1
	        };
	        this.setCallback = function(callbacks) {
	            this.callbackQuene = this.callbackQuene || [];
	            this.callbackQuene.push(callbacks);
	        };
	        this.setCallback(callbacks);
	        this.options = {
	            tryCount: 1,
	            debugURL: "apis/sdkAuthDebug.jsp",
	            taskId: "pull",
	            qsPullPath: QSAPI.getQSPullPath(),
	            pullService: QSAPI.getQSPullPath() + "getPullQuote.jsp",
	            frequency: 30000,
	            sessionInvalidMaxCount: 10,
	            autoStopPull: false //Auto stop pull when session is invalid.
	        };
	        var globalStatus = QSAPI.Util.getGlobalStatus();
	        //debug: -1:connect unsuccessfully
	        //debug: 0: connect sucessfully
	        if (options) {
	            $.extend(this.options, options);
	        }
	        this.frequency = this.options.frequency;
	        this.dataHandler = dataHandler;
	        this.ready = {};
	        /*this.rate={
		        fast:5,
		        slow:10,
		        slower:15,
		        slowest:30
		    };*/
	        this.debugTryCount = 0;
	        this.batchQuerySize = 50;
	        this.tryCountFlag = 0;
	        if (this.options.autoStopPull) {
	            this._sessionInvalidTryCount = 0;
	            this._clearSessionInvalidTryCount = function() {
	                this._sessionInvalidTryCount = 0;
	            };
	            this._sessionInvalidProcess = function() {
	                var tryCount = ++this._sessionInvalidTryCount;
	                if (tryCount > this.options.sessionInvalidMaxCount) {
	                    this.disconnect();
	                }
	            };
	        } else {
	            this._clearSessionInvalidTryCount = this._sessionInvalidProcess = function() {};
	        }
	        var sf = this;
	        this.setPullService = function(pullService) {
	            this.options.pullService = pullService;
	        };
	        var sf = this;
	        var _connectionToPull = function() {
	            //try to get the pull data from pull server using the special ticker [126.1.IBM]
	            sf.tryCountFlag++;
	            var result = {
	                retryCount: sf.tryCountFlag
	            };
	            sf.handleCallback("onRetry", result);
	            QSAPI.Util.ajax({
	                type: 'GET',
	                url: sf.options.qsPullPath + "connect.jsp?cb=?",
	                cache: false,
	                timeout: 10000,
	                dataType: 'json',
	                success: function(result, textStatus) {
	                    if (result.status == 'ok') {
	                        sf.tryCountFlag = 0;
	                        sf.handleCallback("onSuccess", globalStatus.CONNOKPULL);
	                    } else {
	                        _connectFailure();
	                    }
	                },
	                error: function(XMLHttpRequest, textStatus, errorThrown) {
	                    _connectFailure();
	                }
	            });
	        };
	
	        var _connectFailure = function() {
	            if (sf.options.tryCount > 1 && sf.tryCountFlag < sf.options.tryCount) {
	                _connectionToPull();
	            } else {
	                sf.tryCountFlag = 0;
	                sf.handleCallback("onFailure", globalStatus.CONNERRPULL);
	            }
	        };
	        //for debug
	        var _genRetryCount = function() {
	            var ran = Math.random();
	            var totalTimes = Math.ceil(sf.options.tryCount * ran); //total connection count
	            return totalTimes > 0 ? totalTimes - 1 : 0; //connect debugURL count
	        };
	        var _debugConnection = function() {
	            if (typeof sf.options.debug != "undefined") { //need debug
	                if (sf.options.debug == "-1") { //unsucessfully
	                    _debugQuery(sf.options.tryCount, false);
	                } else { //sucessfully
	                    var times = _genRetryCount();
	                    if (times == 0) { //directly connect to pull url
	                        _connectionToPull();
	                    } else {
	                        _debugQuery(times, true);
	                    }
	                }
	            }
	        };
	        var _debugQuery = function(times, needNormalConnection) {
	            var data = {
	                cmd: "pull",
	                debug: sf.options.debug
	            }
	            var _query = function(data) {
	                sf.debugTryCount++;
	                if (typeof sf.callbacks.onRetry == 'function') {
	                    sf.callbacks.onRetry({
	                        retryCount: sf.debugTryCount
	                    });
	                }
	                QSAPI.Util.ajax({
	                    type: 'GET',
	                    url: String.format("{0}{1}?callback=?", sf.options.qsPath, sf.options.debugURL),
	                    cache: false,
	                    timeout: 10000,
	                    dataType: 'json',
	                    data: data,
	                    error: function(XMLHttpRequest, textStatus, errorThrown) {
	                        if (sf.debugTryCount >= times) {
	                            if (needNormalConnection) {
	                                sf.tryCountFlag = sf.debugTryCount;
	                                _connectionToPull();
	                            } else {
	                                if (typeof sf.callbacks.onFailure == 'function') {
	                                    sf.callbacks.onFailure(globalStatus.CONNERRPULL);
	                                }
	                            }
	                            sf.debugTryCount = 0;
	                        } else {
	                            _query(data);
	                        }
	                    }
	                });
	            }
	            if (sf.debugTryCount < times) {
	                _query(data);
	            }
	        };
	
	        var getAdaptiveChannel = function(channels) {
	            var list = [],
	                ticker;
	            for (var i = 0, n = channels.length; i < n; i++) {
	                ticker = channels[i].itemName;
	                if (!/\|L2$/.test(ticker)) {
	                    list.push(ticker);
	                }
	            }
	            return list;
	        };
	
	        var _generatePullRequestKey = function(tickers, forceDelay) {
	            var keys = [];
	            for (var i = 0, l = tickers.length; i < l; i++) {
	                keys.push(tickers[i] + "_" + forceDelay);
	            }
	            return keys.join(",");
	        };
	
	        /*this._checkCacheRequest = function (key) {
	            var _ret = {
	                'cache': [],
	                'new': []
	            };
	            var _cacheKeys = {};
	            for (var _key in this._cacheRequest) {
	                var _array = _key.split(",");
	                for (var i = 0, l = _array.length; i < l; i++) {
	                    _cacheKeys[_array[i]] = { status: this._cacheRequest[_key].status };
	                }
	            }
	            var _newKeys = key.split(",");
	            for (var j = 0, jl = _newKeys.length, _newKey; j < jl; j++) {
	                _newKey = _newKeys[j];
	                if (_cacheKeys[_newKey] && _cacheKeys[_newKey].status === this.STATUS.PENDING) {
	                    _ret['cache'].push(_newKey);
	                } else {
	                    _ret['new'].push(_newKey);
	                }
	            }
	            return _ret;
	        };*/
	
	        this._clearCacheRequest = function(key) {
	            this._cacheRequest[key] = { status: this.STATUS.START, callbacks: [] };
	            delete this._cacheRequest[key];
	        };
	
	        this.getTaskId = function(component) {
	            var taskId = "{0}_{1}";
	            return QSAPI.Util.formatString(taskId, this.options.taskId, QSAPI.Util.getValidForceDelay(component.forceDelay));
	        };
	        this.getDataHandler = function() {
	            return this.dataHandler;
	        };
	        this.initConnection = function(pullService, frequency) {
	            this.setPullService(pullService);
	            this.frequency = frequency;
	            if (typeof this.options.debug != "undefined") {
	                _debugConnection();
	            } else {
	                _connectionToPull();
	            }
	        };
	        this.initRegister = function(component) {
	            var taskId = this.getTaskId(component);
	            if (this.ready[taskId]) {
	                return;
	            }
	            var self = this;
	            QSAPI.Task.register({
	                taskId: taskId,
	                period: this.frequency,
	                batchQuerySize: this.batchQuerySize,
	                query: function(tickers, taksId) {
	                    var forceDelay = taksId.replace(new RegExp(self.options.taskId + '_'), '') === 'true' ? true : false;
	                    var components = [],
	                        unique = {};
	                    for (var i = 0, l = tickers.length, subscriptions; i < l; i++) {
	                        subscriptions = QSAPI.TaskAttachment.getSubscriptionsByTicker(taskId, tickers[i]);
	                        for (var j = 0, jl = subscriptions.length, subscr; j < jl; j++) {
	                            subscr = subscriptions[j].widget;
	                            if (QSAPI.Util.getValidForceDelay(subscr.forceDelay) === forceDelay && !unique[subscr.subscribeID]) {
	                                components.push(subscr);
	                                unique[subscr.subscribeID] = true;
	                            }
	                        }
	                    }
	                    self._query(tickers, forceDelay, undefined, components);
	                }
	            });
	            this.ready[taskId] = true;
	        };
	        this.disconnect = function() {
	            QSAPI.Task.unregister(this.getTaskId({ forceDelay: true }));
	            QSAPI.Task.unregister(this.getTaskId({ forceDelay: false }));
	        };
	
	        this.query = function(tickers, forceDelay, callback, component) {
	            if (!tickers || !tickers.length) {
	                return false;
	            }
	            if ($.isFunction(forceDelay)) {
	                callback = forceDelay;
	                forceDelay = false;
	            }
	            if (tickers.length > this.batchQuerySize) {
	                var subTickers = [];
	                var c = Math.ceil(tickers.length / this.batchQuerySize);
	                var cbNum = 0;
	                var returnItems = [];
	                for (var i = 0; i < c; i++) {
	                    subTickers.push(tickers.slice(this.batchQuerySize * i, this.batchQuerySize * (i + 1)));
	                    this._query(subTickers[i], forceDelay, function(result, textStatus) {
	                        cbNum++;
	                        returnItems = returnItems.concat(result.items || []);
	                        if (cbNum >= c && typeof callback == 'function') {
	                            callback({ items: returnItems });
	                        }
	                    }, [component]);
	                }
	            } else {
	                this._query(tickers, forceDelay, function(result, textStatus) {
	                    if (typeof callback == 'function') {
	                        callback(result);
	                    }
	                }, [component]);
	            }
	        };
	
	        this._query = function(tickers, forceDelay, callback, components) {
	            if (!tickers.length) {
	                return;
	            }
	            forceDelay = QSAPI.Util.getValidForceDelay(forceDelay);
	            var params = {
	                query: tickers.join(","),
	                dataFrequency: this.frequency,
	                device: QSAPI.Util.getDeviceType()
	            };
	            if (forceDelay) {
	                params["forceDelay"] = 1;
	            }
	            if (components.length === 1 && components[0].name) {
	                params["componentName"] = components[0].name;
	            } else {
	                var componentTickers = [];
	                for (var i = 0, l = components.length, component, componentName; i < l; i++) {
	                    component = components[i];
	                    componentName = component.name;
	                    if (!componentName) continue;
	                    if (!componentTickers[componentName]) {
	                        componentTickers[componentName] = [];
	                    }
	                    for (var j = 0, jl = tickers.length, subTicker, subGkeys; j < jl; j++) {
	                        subTicker = tickers[j];
	                        subGkeys = QSAPI.SubscriberManager.getGkeys(subTicker, component.subscribeID);
	                        if (!$.isEmptyObject(subGkeys)) {
	                            componentTickers[componentName].push(subTicker);
	                        }
	                    }
	                }
	                var componentNames = [];
	                if (QSAPI.Util.keySize(componentTickers) === 1) {
	                    for (var name in componentTickers) {
	                        componentNames.push(name);
	                    }
	                } else {
	                    for (var name in componentTickers) {
	                        componentNames.push(name + "|" + QSAPI.Util.uniqueArray(componentTickers[name]).join("-"));
	                    }
	                }
	                params["componentName"] = componentNames.join(",");
	            }
	            var _key = _generatePullRequestKey(tickers, forceDelay);
	            var self = this;
	            (function(_key, params, callback) {
	                self._cacheRequest[_key] = self._cacheRequest[_key] || { status: self.STATUS.START, callbacks: [callback] };
	                if (self._cacheRequest[_key].status === self.STATUS.PENDING) {
	                    self._cacheRequest[_key].callbacks.push(callback);
	                } else {
	                    self._cacheRequest[_key].status = self.STATUS.PENDING;
	                    var config = {
	                        type: 'GET',
	                        //jsonpCallback: "MStar_QSAPI_PullCallback",
	                        url: self.options.pullService + '?cb=?',
	                        cache: false,
	                        timeout: 10000,
	                        dataType: 'json',
	                        runComponentAuthCheckFirst: true,
	                        data: params,
	                        success: function(result, textStatus) {
	                            if (QSAPI.isFod() && QSAPI.fod.transformFodToQuoteData) {
	                                var body = {};
	                                body.items = [];
	                                result = QSAPI.fod.transformFodToQuoteData(result);
	                                if (result && result.length > 0) {
	                                    for (var i = 0; i < result.length; i++) {
	                                        body.items.push(result[i]);
	                                    }
	                                }
	                                result = body;
	                            }
	
	                            var _cacheCallbacks = [];
	                            if (self._cacheRequest && self._cacheRequest[_key] && self._cacheRequest[_key].callbacks) {
	                                _cacheCallbacks = self._cacheRequest[_key].callbacks;
	                            }
	                            self._checkAuthExpire(result, textStatus, _cacheCallbacks);
	                            self.dataHandler.updateMarketPrice(result, "pull");
	                            self._clearCacheRequest(_key);
	                        },
	                        error: function(xhr, textStatus) {
	                            var data = QSAPI.Util.errorHandler(xhr, textStatus);
	                            self.handleCallback("onFailure", data);
	                            self._clearCacheRequest(_key);
	                        }
	                    };
	
	                    if (QSAPI.isFod()) {
	                        var queryTickers = config.data.query;
	                        // remove needless parameters
	                        delete config.data.query;
	
	                        config.url = QSAPI.urlFODQuote();
	                        config.data.instrument = queryTickers;
	                        config.data.fields = 'H1,H2,H3,H10,H14,DA,S9,S12,S13,S31,S214,S676,S1776,S1000,S1683,S1734,S3377';
	                        config.data.JSONShort = true;
	                    }
	                    QSAPI.Util.ajax(config);
	                }
	            })(_key, params, callback);
	        };
	
	        this._checkAuthExpire = function(result, textStatus, callbacks) {
	            if (typeof(result) == "undefined" || result == null || result == "" || $.isEmptyObject(result)) {
	                this.handleCallback("onFailure", globalStatus.RETURNERR);
	            } else if (result.status && result.status.errorCode == '-1') {
	                if (result.status.subErrorCode == "10000") {
	                    this._sessionInvalidProcess();
	                    this.handleCallback("onFailure", globalStatus.AUTHEXPIRED);
	                }
	            } else {
	                this._clearSessionInvalidTryCount();
	                while (callbacks.length > 0) {
	                    var callback = callbacks.splice(0, 1)[0];
	                    if (typeof callback == 'function') {
	                        callback(result, textStatus);
	                    }
	                }
	
	            }
	        };
	
	        this.batchSubscribe = function(channels, component) { //['126.1.IBM','126.1.MSFT']
	            this.initRegister(component);
	            QSAPI.Task.subscribe(this.getTaskId(component), getAdaptiveChannel(channels), component);
	        };
	
	        this.batchUnsubscribe = function(channels, component) {
	            QSAPI.Task.unsubscribe(this.getTaskId(component), getAdaptiveChannel(channels), component);
	        };
	
	        this.handleCallback = function(cbType, r) {
	            if (this.callbackQuene && this.callbackQuene.length) {
	                for (var i = 0, n = this.callbackQuene.length; i < n; i++) {
	                    if (typeof this.callbackQuene[i][cbType] == 'function') {
	                        this.callbackQuene[i][cbType](r);
	                        delete this.callbackQuene[i][cbType];
	                    }
	                }
	            }
	        };
	        this.getType = function() {
	            return "pull";
	        };
	    };
	})(QSAPI);
	/* Push- Migaratory 4.0*/
	(function(QSAPI) {
	    'use strict';
	    var $ = QSAPI.$;
	    var Util = QSAPI.Util;
	    QSAPI.Push = function(dataHandler, callback, options) {
	        var self = this;
	        this.options = {
	            tryCount: 1,
	            debugURL: "apis/sdkAuthDebug.jsp",
	            host: window.location.protocol + "//nnpush.morningstar.com"
	        };
	        if (options) {
	            $.extend(this.options, options);
	        }
	        this.pullObj = null;
	        this.dataHandler = dataHandler;
	        this.debugTryCount = 0;
	        this.callback = callback || {};
	        this.tryCountFlag = 0;
	        var globalStatus = QSAPI.Util.getGlobalStatus();
	        this.getDataHandler = function() {
	            return this.dataHandler;
	        };
	        var stream = null;
	        if (QSAPI.isFod() && QSAPI.fod && QSAPI.fod.stream) {
	            stream = new QSAPI.fod.stream(options, dataHandler, callback);
	        }
	
	        var _connectionToPush = function() {
	            //set entitlementToken to push server | include since 5.0
	            MigratoryDataClient.setEntitlementToken(QSAPI.getSessionId() || QSAPI.getSessionId(true))
	                //Define the number of failed attempts to connect to one or more MigratoryData servers before triggering a status notification | include since 5.0
	            MigratoryDataClient.notifyAfterReconnectRetries(self.options.tryCount); // Define the number of failed attempts to connect 
	
	            MigratoryDataClient.setStatusHandler(function(status) { // 
	                if (status.type == MigratoryDataClient.NOTIFY_SERVER_DOWN) { // connect failed
	                    self.handleCallback("onFailure", globalStatus.CONNERRPUSH);
	                } else if (status.type == MigratoryDataClient.NOTIFY_SERVER_UP) { // connect successful 
	                    self.handleCallback("onSuccess", globalStatus.CONNOKPUSH);
	                }
	                if (typeof self.callback.onStateChanged == 'function') { // provide the infomation of status changed of push server
	                    var result = {
	                        errorCode: status.type,
	                        errorMsg: status.info
	                    };
	                    self.handleCallback("onStateChanged", result);
	                }
	            });
	            MigratoryDataClient.setMessageHandler(function(messages) { // set
	                //console.log(messages);
	                self.dataHandler.updateMarketPrice(messages, "push"); // message arrived.
	            });
	            MigratoryDataClient.setServers([self.options.host], true); // connect to push servers.
	        };
	
	        //for debug
	        var _genRetryCount = function() {
	            var ran = Math.random();
	            var totalTimes = Math.ceil(self.options.tryCount * ran); //total connection count
	            return totalTimes > 0 ? totalTimes - 1 : 0; //connect debugURL count
	        };
	        var _debugConnection = function() {
	            if (typeof self.options.debug != "undefined") { //need debug
	                if (self.options.debug == "-1") { //unsucessfully
	                    _debugQuery(self.options.tryCount, false);
	                } else { //sucessfully
	                    var times = _genRetryCount();
	                    if (times == 0) { //directly connect to pull url
	                        _connectionToPush();
	                    } else {
	                        _debugQuery(times, true);
	                    }
	                }
	            }
	        };
	        var _debugQuery = function(times, needNormalConnection) {
	            var data = {
	                cmd: "push",
	                debug: self.options.debug
	            }
	            var _query = function(data) {
	                self.debugTryCount++;
	                if (typeof self.callback.onRetry == 'function') {
	                    self.callback.onRetry({
	                        retryCount: self.debugTryCount
	                    });
	                }
	                $.ajax({
	                    type: 'GET',
	                    url: QSAPI.Util.formatString("{0}{1}?callback=?", self.options.qsPath, self.options.debugURL),
	                    cache: false,
	                    timeout: 10000,
	                    dataType: 'json',
	                    data: data,
	                    error: function(XMLHttpRequest, textStatus, errorThrown) {
	                        if (self.debugTryCount >= times) {
	                            if (needNormalConnection) {
	                                self.tryCountFlag = self.debugTryCount;
	                                _connectionToPush();
	                            } else {
	                                if (typeof self.callback.onFailure == 'function') {
	                                    self.callback.onFailure(globalStatus.CONNERRPUSH);
	                                }
	                            }
	                            self.debugTryCount = 0;
	                        } else {
	                            _query(data);
	                        }
	                    }
	                });
	            }
	            if (self.debugTryCount < times) {
	                _query(data);
	            }
	        };
	
	        var getRTTickers = function(channels) {
	            var tickers = [];
	            for (var i = 0; i < channels.length; i++) {
	                tickers.push(channels[i].itemName);
	            }
	            return tickers;
	        };
	
	        this.initConnection = function(host) {
	            this.options.host = host;
	            if (stream) {
	                stream.connect();
	            } else if (typeof this.options.debug != "undefined") {
	                _debugConnection();
	            } else {
	                _connectionToPush();
	            }
	        };
	
	        this.disconnect = function() {
	            if (stream) {
	
	            } else {
	                MigratoryDataClient.disconnect();
	            }
	        };
	
	        this._subscribeConsolidatedTickers = function(channels) {
	            //sent consolidated ticker list to DSS
	            var items = [],
	                count = 0,
	                chn;
	            for (var j = 0; j < channels.length; j++) {
	                chn = channels[j];
	                if (count > 80) { //only send the first 80 tickers
	                    break;
	                }
	                if (parseInt(chn.serviceName) >= 126) { //filter out the sub-exch
	                    items.push(this._getItem(chn));
	                    count++;
	                }
	            }
	            MigratoryDataClient.subscribe(items);
	        };
	
	        this._subscribeToPull = function(channels) {
	            this.pullObj.batchSubscribe(channels);
	        };
	
	        this._getItem = function(chn) {
	            return "/" + chn.sourceType + "/SC/" + chn.itemName;
	        };
	
	        this.batchSubscribe = function(channels) {
	            var items = [];
	            var count = 0,
	                chn;
	            if (stream) {
	                var tickers = getRTTickers(channels);
	                stream.subscribe(tickers);
	            } else {
	                for (var j = 0; j < channels.length; j++) {
	                    chn = channels[j];
	                    items.push(this._getItem(chn));
	                    if ((j + 1) % 50 == 0 || j == channels.length - 1) {
	                        count++;
	                        var paraObj = { count: count };
	                        var keepScopeFunction = function(obj) {
	                            setTimeout(function() {
	                                if (items.length) MigratoryDataClient.subscribe(items);
	                                items = [];
	                            }, 300 * obj.count);
	                        };
	                        Util.replaceWithInStrict(keepScopeFunction, paraObj);
	                    }
	                }
	            }
	        };
	
	        this.batchUnsubscribe = function(channels) {
	            var items = [],
	                chn;
	            if (stream) {
	                var tickers = getRTTickers(channels);
	                stream.unSubscribe(tickers);
	            } else {
	                for (var j = 0; j < channels.length; j++) {
	                    chn = channels[j];
	                    items.push(this._getItem(chn));
	                    if ((j + 1) % 50 == 0 || j == channels.length - 1) {
	                        if (items.length) MigratoryDataClient.unsubscribe(items);
	                        items = [];
	                    }
	                }
	            }
	        };
	        this.handleCallback = function(cbType, r) {
	            if (typeof this.callback[cbType] == 'function') {
	                this.callback[cbType](r);
	            }
	            if (this.callbackQuene && this.callbackQuene.length) {
	                for (var i = 0, n = this.callbackQuene.length; i < n; i++) {
	                    if (typeof this.callbackQuene[i][cbType] == 'function') {
	                        this.callbackQuene[i][cbType](r);
	                        delete this.callbackQuene[i][cbType];
	                    }
	                }
	            }
	        };
	        this.setCallback = function(callbacks) {
	            if (stream) {
	                stream.setCallback(callbacks);
	            } else {
	                this.callbackQuene = this.callbackQuene || [];
	                this.callbackQuene.push(callbacks);
	            }
	        };
	        this.getType = function() {
	            return "push";
	        };
	    };
	})(QSAPI);
	(function(QSAPI){
	    'use strict';
		var $ = QSAPI.$;
	
		QSAPI.Queue = function(_delay){
			var dalay = _delay || 100;
		    var maxLength = 500;
		    var queue = [];
		    var timer = null;
		    var firstRun = true;
		    var length = queue.length;
		    var runQueue = function(){
		        length = queue.length;
		        if (length >= maxLength) {
		            queue = queue.slice(length - maxLength, length);
		        }
		        if (length > 0) {
		            //clearTimeout(timer);
		            timer = window.setTimeout(function(){
		                queue[0]();
		                queue.splice(0, 1);
		                runQueue();
		                firstRun = false;
		            }, firstRun ? 2000 : dalay); //waiting for the page load in first run.
		        }
		        else {
		            timer = window.setTimeout(function(){
		                runQueue();
		            })
		        }
		    }
		    runQueue();
		    return {
		        add: function(fn){
		            queue.push(fn);
		        }
		    };
		};
	
	
		QSAPI.XDomain = {
			hasPostMessage:window["postMessage"],
			receiveMessage:function(callback,source_origin){
				if(this.hasPostMessage){
					var rm_callback;
					if(callback){
						// rm_callback && QSAPI.XDomain.receiveMessage();
						
						rm_callback = function(e){
							if(typeof source_origin==="string" && e.origin !==source_origin){
								return false;
							}
							callback(e);
						};
					}
					if ( window["addEventListener"] ) {
						window[ callback ? "addEventListener" : "removeEventListener" ]("message", rm_callback, false);
					}else{
						window[ callback ? "attachEvent" : "detachEvent" ]("onmessage", rm_callback);
					}
				}
			},
			postMessage: function (message, target_url, target) {
				if(this.hasPostMessage){
					target["postMessage"](message,target_url.replace( /([^:]+:\/\/[^\/]+).*/, '$1' ));
				}
			}
		};
	
		QSAPI.XWinProxy_parent = function(config){
			this.children = {};
			this.selfDomain = (window.location.protocol+"//"+window.location.host)||'';
			this.config={
				qsDomain:"quotespeed.morningstar.com"
			};
			this._extend(this.config,config);
			this.isXDOM = false;
			this._cid = 0;
			this._init();
		};
	
		QSAPI.XWinProxy_parent.prototype={
			_init:function(){
				var self=this;
				QSAPI.XDomain.receiveMessage(function(e){
					var vals = decodeURIComponent(e.data).split("|");
					if(vals.length>=2){
						self.callFunction(vals[0], vals[1]);
					}
				},this._getQSDomain());
			},
			_getQSDomain:function(){
				return window.location.protocol+"//"+this.config.qsDomain;
			},
			_extend:function(src,dest){
				if(!dest) return;
				for(var k in dest){
					if(dest[k]){
						src[k] = dest[k];
					}
				}
			},
			createChild:function(iframe,id){
				if(iframe){
					var child;
					this.isXDOM = iframe.src.indexOf(this.selfDomain)==0?false:true;
					if(!this.isXDOM){//is the same domain
						child = {
							ifm : iframe,
							queue : new QSAPI.Queue(100),
							callFunction : function(functionName,parameter){
								if(typeof this.ifm.contentWindow[functionName]==="function"){
									parameter = typeof parameter==="string"?parameter:$.toJSON(parameter);
									this.ifm.contentWindow[functionName](parameter);
								}
							}
						}
					}else{
						if(QSAPI.XDomain.hasPostMessage){//support postmessage
							child = {
								ifm : iframe,
								queue : new QSAPI.Queue(100),
								callFunction : function(functionName,parameter){
									var self = this;
									parameter = typeof parameter==="string"?parameter:$.toJSON(parameter);
									QSAPI.XDomain.postMessage(encodeURIComponent(functionName + '|' + parameter), self.ifm.src, self.ifm.contentWindow);
								}
							}
						}else{
							child = {
								ifm : iframe,
								queue : new QSAPI.Queue(100),
								callFunction : function(functionName,parameter){
									var self = this;
									this.queue.add(function(){
										parameter = typeof parameter==="string"?parameter:$.toJSON(parameter);
										self.ifm.contentWindow.location = self.ifm.src.replace(/#.*$/, '') + '#' + encodeURIComponent(functionName + '|' + parameter);
									});
								}
							}
						}
					}
					this.addChild(child,id);
				}
			},
			addChild: function(child, key){
		        this.children[key||this._cid++] = child;
		    },
			setQSDomain:function(qsDomain){
				if(typeof qsDomain!='undefined'&&qsDomain.length>0&&this.qsDomain!=qsDomain){
					this.qsDomain=qsDomain;
					this._init();
				}
			},
			callFunction: function(functionName, parameter){
		        if (window[functionName]) {
		            window[functionName](parameter);
		        }
		    },
			callChildFunction:function(functionName,parameter,id){
				if(typeof id != "undefined"){//call special one widget
					if(this.children[id]){
						this.children[id].callFunction(functionName,parameter);
					}
				}else{
					for(var c in this.children){
						try{
							this.children[c].callFunction(functionName,parameter);
						}catch(e){
							this.children[c]=null;
							delete this.children[c];
						}
					}
				}
			}
		};
	})(QSAPI);
	
	(function(QSAPI){
	    'use strict';
	    var $ = QSAPI.$;
		QSAPI.IframeHandler = function(config){
			this.config = config;
			this.regExp=/(.*?)(?=:\/\/)/i;
			this.protocol = null; 
			this.xwinProxy = new QSAPI.XWinProxy_parent({
				qsDomain:this._removeProtocol(this.config.qsDomain)
			});
		};
	
		QSAPI.IframeHandler.prototype={
			create:function(container, widgetURL, config, callbacks){
				config=config||{width:200,height:300};
				var self = this;
		        var frame = document.createElement('iframe');
		        var frameId = this._getIfameId(config.id);
		        frame.setAttribute("frameborder", "0", 0);
				if(config.scroll!==true){
					frame.setAttribute("scrolling", "no", 0);
				}
				frame.style.display = "block";
		        frame.id = frameId;
		        container.appendChild(frame);
				
				widgetURL=this._setParameter(widgetURL, 'instid', this.config.instid);
				
				for(var p in config){
					if (p == "id" || p == "proxyurl") {
						continue;
					}
	
					if (p == 'width' || p == 'height') {
						var v = parseFloat(config[p],10);
						if(!isNaN(v)){
							frame[p] = v;
						}
					}
					widgetURL = this._setParameter(widgetURL, p, config[p]);
				}
				
				//extend the callbacks to src
				widgetURL = this._setCallbacks(frameId,widgetURL,config,callbacks);
				
				if(this.config.sessionKey && this.config.sessionKey.length>0){
					widgetURL = this._setParameter(widgetURL, 'qs_wsid', this.config.sessionKey);
				}
				widgetURL = this._setParameter(widgetURL, 'wid', frameId);
				
				var proxyurl = this._getParameter('proxyurl',widgetURL);
	
				if(!proxyurl && this.config.proxyurl){
					widgetURL = this._setParameter(widgetURL, 'proxyurl', encodeURIComponent(this.config.proxyurl));
					proxyurl = this.config.proxyurl;
				}
	
				this.protocol = (proxyurl.match(this.regExp) || [""])[0];
	
				if(this.protocol.length>0){
					widgetURL = this._unifyProtocol(widgetURL,this.protocol);
				}
	
		        frame.src = widgetURL;
		        
		        this._addIframe(frameId);
	
		        return frame;
			},
			getXWinProxy:function(){
				return this.xwinProxy;
			},
			setTicker: function(ticker, id) {
				this.xwinProxy.callChildFunction("acceptTicker",ticker,id);
			},
			call:function(functionName,parameter){
				this.xwinProxy.callChildFunction(functionName,parameter);
			},
			setConfig:function(cfg){
				 $.extend(true, this.config, cfg);
			},
			_unifyProtocol:function(url,protocol){
				return url.replace(this.regExp,protocol);
			},
			_removeProtocol:function(url){
				var domain=url.match(/[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+\.?/)
				if(domain!=null){
					return domain[0];
				}else{
					return null;
				}
			},
			_getIfameId: function(id){
				return typeof id!="undefined"?id:"QSAPI_IFRAME_"+new Date().getTime();
		    },
			_addIframe: function(){
				for(var i=0,l=arguments.length;i<l;i++){
					this.xwinProxy.createChild(document.getElementById(arguments[i]),this._getIfameId(arguments[i]));
				}
			},
			_setParameter: function(source, name, value){
		        var pattern = name + '=([^&]*)';
		        var replaceText = name + '=' + value;
				var matches=source.match(pattern);
				if(matches){
					var tmp = matches[0];
					tmp = source.replace(tmp, replaceText);
					return tmp;
		        }else {
		            if (source.match('[\?]')) {
		                return source + '&' + replaceText;
		            }
		            else {
		                return source + '?' + replaceText;
		            }
		        }
		    },
			_getParameter: function(name, source){
		        return unescape((new RegExp(name + '=' + '(.+?)(&|$)').exec(source || location.search) || [, ''])[1]);
		    },
			_setCallbacks:function(frameId,widgetURL,config,callbacks){
				var self=this;
				callbacks = callbacks||{};
				if (config.fixHeight === false || config.fixHeight === "false") {
				    var onHeightChangeName = 'QSAPI_CB_onHeightChange_' + frameId;
				    var oldOnHeightChange = callbacks['onHeightChange'] || this._getParameter('onHeightChange', widgetURL);
				    widgetURL = this._setParameter(widgetURL, 'onHeightChange', onHeightChangeName);
				    window[onHeightChangeName] = function (para) {
				        self.xwinProxy.callFunction(oldOnHeightChange, para);
				        var newpara = para;
				        if (typeof (para) == "string") {
				            newpara = $.evalJSON(para);
				        }
				        var ifm = document.getElementById(frameId);
				        ifm.style.height = newpara.height + "px"
				    };
				    
				}
	
				var onLoadName = 'QSAPI_CB_onLoad_' + frameId;
				var oldOnload = callbacks['onLoad'] || this._getParameter('onLoad', widgetURL);
				widgetURL = this._setParameter(widgetURL, 'onLoad', onLoadName);
				window[onLoadName] = function (para) {
				    self.xwinProxy.callFunction(oldOnload, para);
				    var newpara = para;
				    if (typeof (para) == "string") {
				        newpara = $.evalJSON(para);
				    }
				    var ifm = document.getElementById(frameId);
				    ifm.style.height = newpara.height + "px"
				};
				
				if(config.fixWidth === false || config.fixWidth === "false"){
					var onWidthChangeName = 'QSAPI_CB_onWidthChange_' + frameId;
					var oldOnWidthChange = callbacks['onWidthChange']||this._getParameter('onWidthChange', widgetURL);
					widgetURL = this._setParameter(widgetURL, 'onWidthChange', onWidthChangeName);
					
					window[onWidthChangeName] = function(para){
						self.xwinProxy.callFunction(oldOnWidthChange, para);
						var newpara={};
						if (typeof (para) == "string") {
							newpara = $.evalJSON(para);
						}
						if(newpara.width>0){
							var ifm = document.getElementById(frameId);
							ifm.style.width = newpara.width + "px"
						}
					}
				}
				
				for (var c in callbacks) {
				    if( c == 'onHeightChange' && (config.fixHeight === false || config.fixHeight === "false")){
				        continue;
				    }else if(c == "onWidthChange" && (config.fixWidth === false || config.fixWidth === "false")){
				        continue;
				    }
				    widgetURL = this._setParameter(widgetURL, c, callbacks[c]);
				}
			
				return widgetURL;
			}
		};
	})(QSAPI);
	
	(function(QSAPI){
	    'use strict';
	    var $ = QSAPI.$;
	    QSAPI.IndicatorsHandler = function (cfg) {
	        this.bclfMap = {
	            '2': 'BC', '3': 'BC', '4': 'LF', '5': 'LF', '6': 'BC & LF', '7': 'BC & LF'
	        };
	        this.cfg = {
	            frequency: 30000,
	            batchQuerySize: 50,
	            tenforeTokens: 'D506,D214',
	            markets: '126,1,14,16',
	            taskId: "indicator"
	        };
	        if (cfg) {
	            $.extend(this.cfg, cfg);
	        }
	        this.cachedData = {};//{subscribeID:{subTicker:subTickerObjectOfWidget}}
	        this._register();
	    };
	    QSAPI.IndicatorsHandler.prototype = {
	        _register: function () {
	            var self = this;
	            QSAPI.Task.register({
	                taskId: this.cfg.taskId,
	                period: this.cfg.frequency,
	                batchQuerySize: this.cfg.batchQuerySize,
	                query: function () {
	                    self.query.apply(self, arguments);
	                }
	            });
	        },
	        query: function (tickers, component, callback) {
	            if ($.isFunction(component)) {
	                callback = component;
	                component = null;
	            }
	            if (tickers && tickers.length && $.isFunction(tickers[0].getField)) {
	                tickers = this._getChannels(tickers, component);
	            }
	            var self = this;
	            QSAPI.Util.ajax({
	                type: 'GET',
	                url: this.cfg.qsPullPath + "getXmlServiceRawData.jsp?cb=?",
	                cache: false,
	                timeout: 60000,
	                dataType: 'json',
	                data: {
	                    'tickers': tickers.join(','),
	                    'tokens': this.cfg.tenforeTokens
	                },
	                success: function (result, textStatus) {
	                    self._handleResponse(result, callback);
	                    self.forceUpdateFlag = false;//No need force update after handle once
	                }
	            });
	        },
	        _handleResponse: function (data, callback) {
	            if (data && data.items) {
	                data = data.items || [];
	                for (var i = 0, l = data.length; i < l; i++) {
	                    var d = data[i],
	                        iTicker = d.symbol,
	                        oExch = iTicker.split(".")[0],
	                        fields = d.fields || [],
	                        needUpdateField = {},
	                        marketPriceMap = {
	                            'LastMarket': oExch,
	                            'indicatorData':true
	                        },
	                        golbalCachedData = QSAPI.Cache.getRawData(iTicker);
	                    for (var j = 0, jl = fields.length; j < jl; j++) {
	                        marketPriceMap[this._getCacheKey(fields[j].name)] = this._getCacheValue(fields[j].name, fields[j].value) || '';
	                    }
	                    for (var field in marketPriceMap) {
	                        if (field == "LastMarket") continue;
	                        var cValue = marketPriceMap[field];
	                        var oValue = golbalCachedData[field];
	                        if (this.forceUpdateFlag || (!this.forceUpdateFlag && cValue != oValue)) {//force update component when subscribe method called
	                            needUpdateField[field] = { value: cValue, oValue: oValue, dataType: 's' };
	                        }
	                    }
	                    if (!$.isEmptyObject(needUpdateField)) {
	                        delete marketPriceMap["LastMarket"];
	                        QSAPI.Cache.setRawData(iTicker, marketPriceMap);
	                        var subWidgets = QSAPI.Task.getSubscriptionsByTicker(this.cfg.taskId, iTicker);
	                        for (var w in subWidgets) {
	                            var component = subWidgets[w].widget;
	                            if (component && typeof component.updateData == 'function') {
	                                if(this.cachedData[component.subscribeID]){
	                                    var tickerObj = this.cachedData[component.subscribeID][iTicker];                                    
	                                    if (tickerObj && tickerObj.getField) {
	                                        component.updateData(tickerObj, needUpdateField);
	                                    }
	                                }
	                            }
	                        }
	                    }
	                    if ($.isFunction(callback)) {
	                        callback(marketPriceMap);
	                    }
	                }
	            }
	        },
	        _getCacheKey: function (filed) {
	            switch (filed) {
	                case 'D506':
	                    filed = "Bclf";
	                    break;
	                case 'D214':
	                    filed = "LastMarket";
	                    break;
	            }
	            return filed;
	        },
	        _getCacheValue: function (filed, ovalue) {
	            var value = ovalue;
	            switch (filed) {
	                case 'D506':
	                    value = this.bclfMap[ovalue] || "";
	                    break;
	            }
	            return value;
	        },
	        _getChannels: function (tickerObjects, component) {
	            var tickerObject, iTicker, channels = [], tenforeCode, tenforeTicker, tenforeType, markets = this.cfg.markets.split(","),subscribeID=component.subscribeID;
	            for (var i = 0, l = tickerObjects.length; i < l; i++) {
	                tickerObject = tickerObjects[i];
	                tenforeCode = tickerObject.getField('tenforeCode');
	                tenforeType = tickerObject.getField('type');
	                tenforeTicker = tickerObject.getField('tenforeTicker');
	                iTicker = QSAPI.DataManager.getSubscribeTicker(tickerObject, component);
	                this.cachedData[subscribeID]=this.cachedData[subscribeID]||{};
	                if (QSAPI.Util.isCompositiveExch(tickerObject)) {
	                    for (var j = 0, n = markets.length; j < n; j++) {
	                        var _newTicker = markets[j] + "." + tenforeType + "." + tenforeTicker;                       
	                        this.cachedData[subscribeID][_newTicker] = tickerObject;
	                        channels.push(_newTicker);
	                        QSAPI.Cache.setRawTickerMap(iTicker,_newTicker);
	                    }
	                }
	                else {                    
	                    channels.push(iTicker);
	                    this.cachedData[subscribeID][iTicker] = tickerObject;
	                }
	            }
	            return channels;
	        },
	        subscribe: function (component, tickerObjects) {
	            var channels = this._getChannels(tickerObjects, component);
	            QSAPI.Task.subscribe(this.cfg.taskId, channels, component);
	        },
	        unSubscribe: function (component, tickerObjects) {
	            var channels = this._getChannels(tickerObjects, component);
	            QSAPI.Task.unsubscribe(this.cfg.taskId, channels, component);
	        }
	    };
	})(QSAPI);
	(function(QSAPI){
	    'use strict';
		var $ = QSAPI.$;
		QSAPI.HL52WHandler=function(cfg){
			this.cfg = {
				batchQuerySize:50,
				taskId:"HL52W"
			};
			this.tokens="D2374,D2382,D2375,D2383";
			this.tokensMap={
				"D2374":"High52W",
				"D2382":"HighDate52W",
				"D2375":"Low52W",
				"D2383":"LowDate52W"
			};
			if(cfg){
				$.extend(this.cfg,cfg);
			}
			this.cachedData = {};//{subscribeID:{subTicker:subTickerObjectOfWidget}}
			this._register();
		};
		QSAPI.HL52WHandler.prototype = {
			_register:function(){
				var self=this;
				QSAPI.Task.register({
					taskId:this.cfg.taskId,
					period:this.cfg.frequency,
					batchQuerySize:this.cfg.batchQuerySize,
					query:function(){
						self.query.apply(self,arguments);
					}
				});
			},
			query:function(tickers,callback){
				if(tickers&&tickers.length&&$.isFunction(tickers[0].getField)){
					tickers=this._getChannels(tickers);
				}
				var self=this;
				QSAPI.Util.ajax({
					type:'GET',
					url:this.cfg.qsPullPath + "getXmlServiceRawData.jsp?cb=?",
					cache:false,
					timeout:60000,
					dataType:'json',
					data:{
						'tickers':tickers.join(','),
						'tokens':this.tokens
					},
					success:function(result,textStatus){
						self._handleResponse(result,callback);
						self.forceUpdateFlag=false;//No need force update after handle once
					}
				});
			},
			_handleResponse:function(data,callback){
				var fieldKey;
				if(data&&data.items){
					data = data.items || [];
					for(var i=0,l=data.length;i<l;i++){
						var d = data[i],
							iTicker = d.symbol,
	                        golbalCachedData=QSAPI.Cache.getRawData(iTicker),
							needUpdateField={},
							fields = d.fields || [],
							marketPriceMap={};				
						for(var j=0,jl=fields.length;j<jl;j++){
							fieldKey=this.tokensMap[fields[j].name];
							if(!fieldKey){
								continue;
							}
							marketPriceMap[fieldKey] = this._format(fieldKey,fields[j].value);
						}							
						for(var field in marketPriceMap){
							var cValue = marketPriceMap[field];
							var oValue = golbalCachedData[field];	
							if(this.forceUpdateFlag||(!this.forceUpdateFlag&&cValue!= oValue)){
								needUpdateField[field] = {value: cValue,oValue:oValue,dataType:'s'};
							}
						}	
						if (!$.isEmptyObject(needUpdateField)) {
						    QSAPI.Cache.setRawData(iTicker, marketPriceMap);
							var subWidgets=QSAPI.Task.getSubscriptionsByTicker(this.cfg.taskId,iTicker);
							for(var w in subWidgets){
								var component=subWidgets[w].widget;	
								if(component && typeof component.updateData=='function'){
								    if (this.cachedData[component.subscribeID]) {
								        var tickerObj = this.cachedData[component.subscribeID][iTicker];
								        if (tickerObj && tickerObj.getField) {
								            component.updateData(tickerObj, needUpdateField);
								        }
								    }
								}
							}
						}
						if($.isFunction(callback)){					    
							callback(marketPriceMap);
						}
					}
				}
			},
			_format:function(field,value){
				if(field=="HighDate52W"||field=="LowDate52W"){
					value = value.replace(/(\d{2})\.(\d{2})\.(\d{4})/,"$2-$1-$3");
				}
				else if(field=="High52W"||field=="Low52W"){
					value = parseFloat(value,10);
				}
				return value;
			},
			_getChannels: function (tickerObjects, component) {
			    var tickerObject, iTicker, channels = [], subscribeID = component.subscribeID;
			    this.cachedData[subscribeID] = this.cachedData[subscribeID] || {};
				for(var i=0,l=tickerObjects.length;i<l;i++){
					tickerObject=tickerObjects[i];
					iTicker = QSAPI.DataManager.getSubscribeTicker(tickerObject, component);
					this.cachedData[subscribeID][iTicker] = tickerObject;
					channels.push(iTicker);
				}
				return channels;		
			},
			subscribe:function(component,tickerObjects){
			    var channels = this._getChannels(tickerObjects, component);
				this.forceUpdateFlag=true;//Need force update after newly subscribe
				QSAPI.Task.subscribe(this.cfg.taskId,channels,component);
			},
			unSubscribe:function(component,tickerObjects){
			    var channels = this._getChannels(tickerObjects, component);
				QSAPI.Task.unsubscribe(this.cfg.taskId,channels,component);
			}
		};
	})(QSAPI);
	
	/**
		require lib/jquery.js
		require permissionChecker.js
		require dataHandler.js
		require pull.js
		require push.js
		require monitor.js
		require timezone.js
	*/
	(function(QSAPI) {
	    'use strict';
	    var $ = QSAPI.$;
	    QSAPI.ClassAuthentication = function(instId, options, callback) {
	        this.instId = instId;
	        this.options = options || {};
	        this.authURL = {
	            debug: "apis/sdkAuthDebug.jsp",
	            live: "service/auth"
	        };
	        this.permissionQueryURL = "service/queryPermission";
	        this.tryCountFlag = 0;
	        this.callback = callback;
	        this.globalStatus = QSAPI.Util.getGlobalStatus();
	    };
	    QSAPI.ClassAuthentication.prototype = {
	        login: function(userEmail, password, callbacks, config) {
	            this._innerLogin({ userEmail: userEmail, password: password }, callbacks, config);
	        },
	        _innerLogin: function(paras, callbacks, config) {
	            callbacks = callbacks || {};
	            config = config || {};
	            if (typeof config.tryCount != 'undefined') {
	                config.tryCount = this.options.tryCount;
	            }
	            if (this.instId == null && !paras.accessToken) {
	                if (typeof callbacks.onFailure == 'function') {
	                    var result = this.globalStatus.INITERR;
	                    callbacks.onFailure(result);
	                    this.handleCallback(result);
	                }
	            } else {
	                var authURL = QSAPI.Util.formatString("{0}{1}?callback=?", this.options.qsPath, (typeof config.debug != 'undefined' ? this.authURL.debug : this.authURL.live));
	                var data;
	                if (paras.sessionId || paras.accessToken) {
	                    authURL = QSAPI.Util.formatString("{0}{1}?callback=?", this.options.qsPath, this.permissionQueryURL);
	                    data = {
	                        sessionId: paras.sessionId,
	                        accessToken: paras.accessToken
	                    };
	                } else if (paras.userProfile) {
	                    data = {
	                        cmd: 'login',
	                        userProfile: paras.userProfile,
	                        instid: this.instId
	                    };
	                } else if (paras.userEmail && paras.password) {
	                    data = {
	                        cmd: 'login',
	                        encode: true,
	                        instid: this.instId,
	                        email: paras.userEmail,
	                        pwd: paras.password
	                    };
	                }
	                data['fields'] = '1,2,4,16,32,64,1024,2048,4096';
	                if (typeof config.debug != 'undefined') { //test system -3/-4 errorCode
	                    data["debug"] = config.debug;
	                }
	                this._requestAuth(authURL, data, callbacks);
	            }
	        },
	        _requestAuth: function(authURL, data, callbacks) {
	            var self = this;
	            self.tryCountFlag++;
	            QSAPI.Util.ajax({
	                type: 'GET',
	                url: authURL,
	                cache: false,
	                timeout: 10000,
	                dataType: 'json',
	                data: data,
	                success: function(result, textStatus) {
	                    self.tryCountFlag = 0; //if successful, set tryCount is 0;
	                    if (result && result.status && result.status.errorCode == '0') {
	                        if (typeof self.callback.afterAuthenticate == 'function') {
	                            self.callback.afterAuthenticate(result.attachment);
	                        }
	                        if (typeof callbacks.onSuccess == 'function') {
	                            var result = self.globalStatus.AUTHOK;
	                            callbacks.onSuccess(result);
	                            self.handleCallback(result);
	                        }
	                    } else {
	                        if (typeof callbacks.onFailure == 'function') { //-1 stand for account error
	                            var result = self.globalStatus.AUTHERR;
	                            callbacks.onFailure(result);
	                            self.handleCallback(result);
	                        }
	                    }
	                },
	                error: function(xhr, textStatus, errorThrown) {
	                    if (self.options.tryCount > 1 && self.tryCountFlag < self.options.tryCount) {
	                        self._requestAuth(authURL, data, callbacks);
	                    } else {
	                        self.tryCountFlag = 0;
	                        var returnObject = QSAPI.Util.errorHandler(xhr, textStatus);
	                        if (typeof callbacks.onFailure == 'function') {
	                            callbacks.onFailure(returnObject);
	                            self.handleCallback(returnObject);
	                        }
	                    }
	                }
	            });
	        },
	        handleCallback: function(result) {
	            if (this.callbackQuene && this.callbackQuene.length) {
	                for (var i = 0, n = this.callbackQuene.length; i < n; i++) {
	                    if (typeof this.callbackQuene[i] == 'function') {
	                        this.callbackQuene[i](result);
	                    }
	                }
	            }
	        },
	        setCallback: function(callback) {
	                this.callbackQuene = this.callbackQuene || [];
	                this.callbackQuene.push(callback);
	            }
	            /*,
			logout:function(){
				var authURL=this.qsPath+"apis/sdkAuth.jsp?cb=?";
				$.getJSON(authURL,{cmd:'logout'},function(o){
				})
			}*/
	    };
	
	    QSAPI.ClassDataManager = function(cfg) {
	        this.globalStatus = QSAPI.Util.getGlobalStatus();
	        this.TYPE = {
	            PUSH: 'push',
	            PULL: 'pull',
	            MIX: "mix"
	        };
	        this.CMD = {
	            SUBSCRIBE: 'subscribe',
	            UNSUBSCRIBE: 'unSubscribe'
	        };
	        this.cfg = {
	            tryCount: 1,
	            dataPermission: null,
	            exchangeMap: null,
	            exchangeNameMap: null,
	            MICExangeMap: null,
	            exchangeMarketTimeMap: null,
	            pullSubSeparator: '_',
	            pushSubSeparator: '|',
	            qsPath: null,
	            qsPullPath: null,
	            pullService: "getPullQuote.jsp",
	            frequency: 30000,
	            autoStopPull: false,
	            l2Permission: null,
	            streamingDataType: 'R'
	        };
	        if (cfg) {
	            $.extend(true, this.cfg, cfg);
	        }
	        this.push = null;
	        this.pull = null;
	        this.monitor = null;
	        this.timezoneManager = null;
	        this.__SNAPPullObj = null;
	        this.permissionChecker = null;
	        this.dataHandler = {};
	        this.subscriber = null;
	        this.connStatus = {
	            success: 1,
	            fail: -1,
	            disconnect: 0
	        };
	        this.connResult = {
	            pull: 0,
	            push: 0
	        };
	    };
	
	    QSAPI.ClassDataManager.prototype = {
	        init: function() {
	            var self = this;
	            this.permissionChecker = new QSAPI.PermissionChecker({ DataPermission: this.cfg.dataPermission }, this.cfg.exchangeMap, this.cfg.MICExangeMap);
	            this.dataHandler["core"] = new QSAPI.DataHandler({
	                staticDataService: this.cfg.qsPath + 'service/staticData',
	                pullSubSeparator: this.cfg.pullSubSeparator,
	                pushSubSeparator: this.cfg.pushSubSeparator,
	                gExchangeMarketTimeMap: this.cfg.exchangeMarketTimeMap
	            }, {
	                onDataUpdate: function(subTicker, needUpdateField) {
	                    self.__updateData(subTicker, needUpdateField);
	                }
	            });
	            if (!this.subscriber) {
	                this.subscriber = new QSAPI.Subscriber(this.permissionChecker, { pushSubSeparator: this.cfg.pushSubSeparator, pullSubSeparator: this.cfg.pullSubSeparator, streamingDataType: this.cfg.streamingDataType });
	            }
	            this._initPullObj();
	            this.monitor = new QSAPI.Monitor();
	            this.timezoneManager = new QSAPI.TimezoneManager();
	        },
	        setCfg: function(cfg) {
	            if (cfg && cfg.timezoneMap) {
	                cfg.timezoneMap = QSAPI.Util.convertTimezoneInfo(cfg.timezoneMap);
	            }
	            if (cfg) {
	                $.extend(true, this.cfg, cfg);
	            }
	        },
	        setDataRecord: function(gKeyList, level2TypeList) {
	            this.monitor.record(gKeyList, level2TypeList);
	        },
	        getTickersTimezone: function(tickers, callback) {
	            this.timezoneManager.getTickersTimezone(tickers, callback);
	        },
	        getTimezoneByTicker: function(tickers) {
	            return this.timezoneManager.getTimezoneByTicker(tickers);
	        },
	        getTimezoneAbbr: function(fullName) {
	            return this.timezoneManager.getTimezoneAbbr(fullName);
	        },
	        getTimezoneTimediff: function(fullName) {
	            var timeDiff = this.timezoneManager.getTimezoneTimediff(fullName);
	            return timeDiff;
	        },
	        getTwoTimeZoneOffset: function(originalTimeZone, displayedTimeZone) {
	            if (!originalTimeZone || !displayedTimeZone) {
	                return 0;
	            } else {
	                var oTimezoneDiff = this.getTimezoneTimediff(originalTimeZone);
	                if (!oTimezoneDiff) {
	                    oTimezoneDiff = QSAPI.Util.getTimezoneDiffAdaptOldMapping(originalTimeZone);
	                }
	                // adapt the old timezone mapping modal
	                var dTimezoneDiff = this.getTimezoneTimediff(displayedTimeZone);
	                if (!dTimezoneDiff) {
	                    dTimezoneDiff = QSAPI.Util.getTimezoneDiffAdaptOldMapping(displayedTimeZone);
	                }
	
	                if (oTimezoneDiff !== '' && dTimezoneDiff !== '') {
	                    return oTimezoneDiff - dTimezoneDiff;
	                } else {
	                    return 0;
	                }
	            }
	        },
	        _initPullObj: function() {
	            var pullConfig = {
	                tryCount: this.cfg.tryCount,
	                qsPath: this.cfg.qsPath,
	                qsPullPath: this.cfg.qsPullPath,
	                autoStopPull: this.cfg.autoStopPull,
	                frequency: this.cfg.frequency
	            };
	            this.pull = new QSAPI.Pull(this.dataHandler["core"], {}, pullConfig); //init pull object for manual refresh
	        },
	        connectToPush: function() {
	            this.defaultSourceType = "push";
	            this._connectToPush.apply(this, arguments);
	        },
	        _connectToPush: function(pushPath, callbacks, config) { //connect to push server
	            var self = this;
	            callbacks = callbacks || {};
	            config = config || {};
	            if (QSAPI.checkLogin()) {
	                var pushConfig = {
	                    tryCount: this.cfg.tryCount,
	                    qsPath: this.cfg.qsPath
	                };
	                if (config.debug != null) {
	                    pushConfig.debug = config.debug;
	                }
	                if (this.push) {
	                    if (this.connResult.push != this.connStatus.disconnect) {
	                        var cb = callbacks[this.connResult.push == this.connStatus.success ? "onSuccess" : "onFailure"];
	                        if (typeof(cb) == "function") {
	                            cb(this.globalStatus[this.connResult.push == this.connStatus.success ? "CONNOKPUSH" : "CONNERRPUSH"]);
	                        }
	                    } else {
	                        this.push.setCallback(callbacks);
	                    }
	                    return;
	                }
	                callbacks = callbacks || {};
	                var originalSuccess = callbacks.onSuccess;
	                callbacks.onSuccess = function() {
	                    self.connResult.push = self.connStatus.success;
	                    if ($.isFunction(originalSuccess)) {
	                        originalSuccess.apply(this, arguments);
	                    }
	                };
	                var originalFailure = callbacks.onFailure;
	                callbacks.onFailure = function() {
	                    if (!QSAPI.isFod()) {
	                        self.connResult.push = self.connStatus.fail;
	                    }
	                    if ($.isFunction(originalFailure)) {
	                        originalFailure.apply(this, arguments);
	                    }
	                };
	                this.push = new QSAPI.Push(this.dataHandler["core"], callbacks, pushConfig);
	                this.push.initConnection(this._unifyProtocol(pushPath));
	            } else {
	                if (typeof callbacks.onFailure == 'function') {
	                    callbacks.onFailure(this.globalStatus.NOAUTH);
	                }
	            }
	        },
	        disconnectPush: function() {
	            if (this.push != null) {
	                this.push.disconnect();
	            }
	            this.push = null;
	            this.connResult.push = this.connStatus.disconnect;
	        },
	        connectToPull: function() {
	            this.defaultSourceType = "pull";
	            this._connectToPull.apply(this, arguments);
	        },
	        _connectToPull: function(pullService, frequency, callbacks, config) { //connect to pull server 
	            var self = this;
	            callbacks = callbacks || {};
	            config = config || {};
	            if (QSAPI.checkLogin()) {
	                if (this.connResult.pull) { //1: connect success -1: connect fail 0: disconnect
	                    var cb = callbacks[this.connResult.pull == this.connStatus.success ? "onSuccess" : "onFailure"];
	                    if (typeof(cb) == "function") {
	                        cb(this.globalStatus[this.connResult.pull == this.connStatus.success ? "CONNOKPULL" : "CONNERRPULL"]);
	                    }
	                    return;
	                }
	                callbacks = callbacks || {};
	                var originalSuccess = callbacks.onSuccess;
	                callbacks.onSuccess = function() {
	                    self.connResult.pull = self.connStatus.success;
	                    if ($.isFunction(originalSuccess)) {
	                        originalSuccess.apply(this, arguments);
	                    }
	                };
	                var originalFailure = callbacks.onFailure;
	                callbacks.onFailure = function() {
	                    self.connResult.pull = self.connStatus.fail;
	                    if ($.isFunction(originalFailure)) {
	                        originalFailure.apply(this, arguments);
	                    }
	                };
	                this.pull.setCallback(callbacks);
	                if (!this._isConnectingPull) {
	                    this.pull.initConnection(this._unifyProtocol(pullService), frequency);
	                    this._isConnectingPull = true;
	                }
	            } else {
	                if (typeof callbacks.onFailure == 'function') {
	                    callbacks.onFailure(this.globalStatus.NOAUTH);
	                }
	            }
	        },
	        disconnectPull: function() {
	            if (this.pull != null) {
	                this.pull.disconnect();
	            }
	            this.pull = null;
	            this.connResult.pull = this.connStatus.disconnect;
	        },
	        disconnect: function() {
	            this.disconnectPull();
	            this.disconnectPush();
	        },
	        connect: function(cfg, callbacks) {
	            var self = this;
	            this.setCfg(cfg || {});
	            this.checkConnect("push", function() {
	                self._forceConnectToPull(callbacks);
	            }, function(r) {
	                self._forceConnectToPull(callbacks);
	            });
	        },
	        _unifyProtocol: function(url) {
	            var reg = new RegExp(QSAPI.Util.formatString(this.cfg.baseAPIGateway, '.*', '.*'));
	            if (!reg.test(url)) {
	                return url.replace(/(.*?)(?=:\/\/)/i, window.location.protocol.split(":")[0]);
	            } else {
	                return url;
	            }
	        },
	        _forceConnectToPull: function(callbacks) {
	            this.checkConnect("pull", function(r) {
	                if ($.isFunction(callbacks.onSuccess)) {
	                    callbacks.onSuccess(r);
	                }
	            }, function(r) {
	                if ($.isFunction(callbacks.onFailure)) {
	                    callbacks.onFailure(r);
	                }
	            });
	        },
	        checkConnect: function(type, successCallback, failCallback) {
	            var self = this;
	            if (!this.connResult[type]) {
	                if (this.cfg.pullService.indexOf("/") == -1) {
	                    this.cfg.pullService = this.cfg.qsPullPath + this.cfg.pullService;
	                }
	                var path = type == "push" ? QSAPI.getQSPushPath() : this.cfg.pullService;
	                var args = [path];
	                if (type == "pull") {
	                    args.push(this.cfg.frequency);
	                }
	                args.push({
	                    onSuccess: function() {
	                        successCallback();
	                    },
	                    onFailure: function() {
	                        failCallback(self.globalStatus[type == "push" ? "CONNERRPUSH" : "CONNERRPULL"]);
	                    }
	                });
	                this[type == "push" ? "_connectToPush" : "_connectToPull"].apply(this, args);
	            } else {
	                successCallback();
	            }
	        },
	        getQuoteData: function(component, tickers, callback, fields) {
	            if (typeof component.subscribeID === 'undefined' && typeof component.forceDelay === 'undefined' && typeof component.name === 'undefined' && typeof component.mergePrePost === 'undefined') {
	                fields = callback;
	                callback = tickers;
	                tickers = component;
	                component = {};
	            }
	
	            if (typeof component.subscribeID === 'undefined') {
	                component.subscribeID = "MS_MKTS_SNAPQUOTE_" + QSAPI.Util.IdProducer.getId()
	            }
	
	            if (typeof component.name === 'undefined') {
	                component.name = 'datamanager';
	            }
	
	            var self = this;
	            var callbackInvoker;
	            if ($.isFunction(callback)) {
	                callbackInvoker = function(quoteData) {
	                    for (var i = 0, n = quoteData.length; i < n; i++) {
	                        quoteData[i] = self._filterFields(quoteData[i], fields);
	                    }
	                    callback(quoteData);
	                }
	            }
	            if (typeof tickers == "string") {
	                tickers = tickers.split(",");
	            }
	            if (tickers.length && typeof(tickers[0]) == "string") { //126.1.IBM				
	                QSAPI.TickerFactory.create(tickers, {
	                    onSuccess: function(tickerObjects) {
	                        self._getQuoteData(component, tickerObjects, callbackInvoker);
	                    }
	                });
	            } else {
	                this._getQuoteData(component, tickers, callbackInvoker);
	            }
	
	        },
	        getStaticData: function(component, tickers, callback) {
	            if (!(component && component.subscribeID)) {
	                callback = tickers;
	                tickers = component;
	                component = null;
	            }
	            this.dataHandler["core"].getStaticData(component, tickers, callback);
	        },
	        hasNews: function(tickers) {
	            var returnValue = false;
	            if (tickers && tickers.length > 0) {
	                var ticker = null,
	                    symbol = "",
	                    symbols = [];
	                for (var i = 0, l = tickers.length; i < l; i++) {
	                    ticker = tickers[i];
	                    symbol = ticker.getField("performanceId");
	                    if (symbol && symbol.length) {
	                        symbols.push(symbol);
	                    }
	                }
	                returnValue = tickers[0].hasNews(symbols.join(","));
	            }
	            return returnValue;
	        },
	        // subscribePush: function(component, tickers, callbacks) {
	        //     if (!component.onlypush) {
	        //         this.subscribeMix.apply(this, arguments);
	        //     } else {
	        //         this._subscribePush.apply(this, arguments);
	        //     }
	        // },
	        subscribePush: function(component, tickers, callbacks) {
	            var self = this;
	            this.checkConnect("push", function() {
	                self.__execCommand(self.CMD.SUBSCRIBE, self.TYPE.PUSH, component, tickers, callbacks);
	            }, function(r) {
	                if (callbacks && $.isFunction(callbacks.onFailure)) {
	                    callbacks.onFailure(r);
	                }
	            });
	        },
	        subscribePull: function(component, tickers, callbacks) {
	            var self = this;
	            this.checkConnect("pull", function() {
	                self.__execCommand(self.CMD.SUBSCRIBE, self.TYPE.PULL, component, tickers, callbacks);
	            }, function(r) {
	                if (callbacks && $.isFunction(callbacks.onFailure)) {
	                    callbacks.onFailure(r);
	                }
	            });
	        },
	        subscribeMix: function(component, tickers, callbacks) {
	            //Check both connected
	            var self = this;
	            this.connect({}, {
	                onSuccess: function() {
	                    self.__execCommand(self.CMD.SUBSCRIBE, self.TYPE.MIX, component, tickers, callbacks);
	                },
	                onFailure: function(r) {
	                    if (callbacks && $.isFunction(callbacks.onFailure)) {
	                        callbacks.onFailure(r);
	                    }
	                }
	            });
	        },
	        subscribe: function(component, tickers, callbacks) {
	            var subType = this.getSubscribeType(component);
	            if (subType["pull"] && subType["push"]) {
	                this.subscribeMix(component, tickers, callbacks);
	            } else if (subType["pull"]) {
	                this.subscribePull(component, tickers, callbacks);
	            } else if (subType["push"]) {
	                this.subscribePush(component, tickers, callbacks);
	            }
	        },
	        subscribeIndicatorData: function(component, tickers) {
	            this._initIndicatorHandler("indicators");
	            this.dataHandler["indicators"].subscribe(component, tickers);
	        },
	        unSubscribeIndicatorData: function(component, tickers) {
	            if (this.dataHandler["indicators"]) {
	                this.dataHandler["indicators"].unSubscribe(component, tickers);
	            }
	        },
	        subscribe52WData: function(component, tickers) {
	            this._init52WHLHandler("HL52W");
	            this.dataHandler["HL52W"].subscribe(component, tickers);
	        },
	        unSubscribe52WData: function(component, tickers) {
	            if (this.dataHandler["HL52W"]) {
	                this.dataHandler["HL52W"].unSubscribe(component, tickers);
	            }
	        },
	        getSubscribeTicker: function(tickerObject, component) {
	            if (!tickerObject) {
	                return "";
	            }
	            var isTickerObj = typeof(tickerObject.getField) == "function";
	            if (!isTickerObj) {
	                tickerObject = QSAPI.Cache.getTickerData(tickerObject);
	            }
	            var gkey = tickerObject.gkey,
	                subTickers = [];
	            component = component || {};
	            if (component && component.subscribeID) {
	                subTickers = QSAPI.SubscriberManager.getSubscribeTickers(gkey, component.subscribeID);
	            }
	            subTickers = subTickers || [];
	            if (!subTickers.length && this.subscriber) {
	                var subPara = this.subscriber._getSubscribePara(tickerObject, component);
	                for (var i = 0, n = subPara.length; i < n; i++) {
	                    subTickers.push(subPara[i].itemName);
	                }
	            }
	            return subTickers.join(",");
	        },
	        _initIndicatorHandler: function(handlerName) {
	            if (!this.dataHandler[handlerName]) {
	                this.dataHandler[handlerName] = new QSAPI.IndicatorsHandler({
	                    qsPullPath: QSAPI.getQSPullPath(),
	                    frequency: (this.pull ? this.pull.frequency : 60000) * 5
	                });
	            }
	        },
	        _init52WHLHandler: function(handlerName) {
	            if (!this.dataHandler[handlerName]) {
	                this.dataHandler[handlerName] = new QSAPI.HL52WHandler({
	                    qsPullPath: QSAPI.getQSPullPath(),
	                    frequency: this.pull ? this.pull.frequency : 60000
	                });
	            }
	        },
	        get52WHLData: function(tickerObjects, callback) {
	            this._init52WHLHandler("HL52W");
	            this.dataHandler["HL52W"].query(tickerObjects, callback);
	        },
	        getIndicatorData: function(component, tickerObjects, callback) {
	            this._initIndicatorHandler("indicators");
	            this.dataHandler["indicators"].query(tickerObjects, component, callback);
	        },
	        unSubscribe: function(component, tickers, callbacks) {
	            var subType = this.getSubscribeType(component);
	            if (subType["pull"] && subType["push"]) {
	                this.unSubscribeMix(component, tickers, callbacks);
	            } else if (subType["pull"]) {
	                this.unSubscribePull(component, tickers, callbacks);
	            } else if (subType["push"]) {
	                this.unSubscribePush(component, tickers, callbacks);
	            }
	        },
	        unSubscribePush: function(component, tickers, callbacks) {
	            this.__execCommand(this.CMD.UNSUBSCRIBE, this.TYPE.PUSH, component, tickers, callbacks);
	        },
	        // unSubscribePush: function(component, tickers, callbacks) {
	        //     this.unSubscribeMix.apply(this, arguments);
	        // },
	        unSubscribePull: function(component, tickers, callbacks) {
	            this.__execCommand(this.CMD.UNSUBSCRIBE, this.TYPE.PULL, component, tickers, callbacks);
	        },
	        unSubscribeMix: function(component, tickers, callbacks) {
	            this.__execCommand(this.CMD.UNSUBSCRIBE, this.TYPE.MIX, component, tickers, callbacks);
	        },
	        deregisterComponent: function(component) {
	            if (!this.__isValidComponent(component)) {
	                return;
	            }
	            if (this.subscriber) {
	                this.subscriber.removeCache(component);
	            }
	        },
	        destory: function() {
	            this.disconnect();
	            this.instId = null;
	        },
	
	        /*************************************/
	        /**private method that will not open**/
	
	        _getStateProfile: function(profileId, callback) {
	            var id, type;
	            if ($.isPlainObject(profileId)) {
	                id = profileId.id;
	                type = profileId.type;
	            } else {
	                id = profileId;
	            }
	            if (id === 'LATEST' || id === 'latest') {
	                var _self = this;
	                this._getStateProfileList(type, function(profileList) {
	                    if ($.isArray(profileList) && profileList.length > 0) {
	                        id = profileList[0].id;
	                    }
	                    _self._getStateProfileWithId(id, type, callback);
	                });
	            } else {
	                this._getStateProfileWithId(id, type, callback);
	            }
	
	        },
	        _getStateProfileList: function(type, callback) {
	            callback = this.__genCallback(callback);
	            var _self = this;
	            var paraObj = {
	                type: type
	            };
	            _self.__handleStateProfile({
	                type: paraObj.type,
	                action: "gettmplist"
	            }, function(result) {
	                callback(result && result.temp);
	            });
	        },
	        _getStateProfileWithId: function(profileId, profileType, callback) {
	            callback = this.__genCallback(callback);
	            if (profileId > 0) {
	                var _self = this;
	                var paraObj = {
	                    profileId: profileId,
	                    profileType: profileType
	                };
	                var keepScopeFunction = function(obj) {
	                    _self.__handleStateProfile({
	                        tplId: obj.profileId,
	                        type: obj.profileType,
	                        action: "gettmp"
	                    }, function(result) {
	                        if (!$.isEmptyObject(result)) {
	                            result.profileId = profileId;
	                        }
	                        callback(result);
	                    });
	                };
	                QSAPI.Util.replaceWithInStrict(keepScopeFunction, paraObj);
	            } else {
	                callback({});
	            }
	        },
	        _saveStateProfile: function(profile, callback) {
	            profile = profile || {};
	            callback = this.__genCallback(callback);
	            var profileId = profile.profileId;
	            var type = profile.profileType;
	            delete profile.profileId;
	            var _self = this;
	            var paraObj = { profileId: profileId, profile: profile, type: type };
	            var keepScopeFunction = function(obj) {
	                _self.__handleStateProfile({
	                    tplId: obj.profileId,
	                    action: obj.profileId > 0 ? "savetmp" : "addtmp",
	                    type: obj.type,
	                    profile: $.toJSON(obj.profile)
	                }, function(result) {
	                    obj.profile.profileId = obj.profileId > 0 ? obj.profileId : result;
	                    callback(obj.profile);
	                });
	            };
	            QSAPI.Util.replaceWithInStrict(keepScopeFunction, paraObj);
	        },
	        __genCallback: function(callback) {
	            return typeof callback === "function" ? callback : function() {};
	        },
	        __handleStateProfile: function(data, callback) {
	            data = data || {};
	            data["type"] = data["type"] || "scfg";
	            if (data["action"] == "addtmp") {
	                data["auto_random_title"] = "true";
	            }
	            QSAPI.Util.getJSON(QSAPI.getQSPath() + "template.jsp", data, callback);
	        },
	        _getStaticData: function(pids, maxcount, callback) {
	            var dataHandler = this.dataHandler["core"];
	            if (dataHandler != null) {
	                dataHandler._getStaticData(pids, maxcount || 100, callback);
	            }
	        },
	        _getQuoteData: function(component, tickerObjects, callback) {
	            this._getSnapQuoteData(component, tickerObjects, callback);
	        },
	        _getSnapQuoteData: function(component, tickerObjects, callback) {
	            this.dataHandler['core'].getSNAPQuoteData(component, tickerObjects, this.pull, callback);
	        },
	        _filterFields: function(quoteData, fields) {
	            var isFilter = false,
	                newData = {};
	            if (fields) {
	                var fieldsArr = fields.split(","),
	                    n = fieldsArr.length;
	                if (n) {
	                    isFilter = true;
	                    for (var i = 0; i < n; i++) {
	                        if (typeof(quoteData[fieldsArr[i]]) != "undefined") {
	                            newData[fieldsArr[i]] = quoteData[fieldsArr[i]];
	                        }
	                    }
	                }
	            }
	            if (isFilter) {
	                return newData;
	            } else {
	                return quoteData;
	            }
	        },
	        _getExchIdWithMicCode: function(micCode) {
	            var exchId = this.permissionChecker.getExchIdWithMicCode(micCode);
	            if (this.permissionChecker.checkACBPermission(micCode, exchId)) {
	                return exchId;
	            } else {
	                return null;
	            }
	        },
	        _checkPremission: function(tickerObject) {
	            return this.permissionChecker.checkTickerObjectPremission(tickerObject);
	        },
	        _checkAutoCompletePermission: function(micCode, exchId) {
	            return this.permissionChecker.checkACBPermission(micCode, exchId);
	        },
	        _getData: function(component, gkey, field) {
	            if (this.dataHandler["core"]) {
	                return this.dataHandler["core"].getMarketQuoteData(component, gkey, field);
	            } else {
	                return {};
	            }
	        },
	        _getExchangeMap: function() {
	            return this.cfg.exchangeMap;
	        },
	        _getExchangeNameMap: function() {
	            return this.cfg.exchangeNameMap;
	        },
	        _getL2Permission: function() {
	            return this.cfg.l2Permission;
	        },
	        _getMICExangeMap: function() {
	            return this.cfg.MICExangeMap;
	        },
	        __execCommand: function(cmd, type, component, tickers, callbacks) {
	            if (!QSAPI.Util.isAutoRefresh(component.autoRefresh)) {
	                return;
	            }
	            callbacks = callbacks || {};
	            var self = this;
	            var errorResult = null;
	            if (!this.__isConnect(type)) {
	                errorResult = this.globalStatus.CONNERR;
	            } else if (!this.__isValidComponent(component)) {
	                errorResult = this.globalStatus.NOSUBID;
	            }
	            if (errorResult != null) {
	                if (typeof callbacks.onFailure == 'function') {
	                    callbacks.onFailure(errorResult);
	                }
	            } else {
	                this.subscriber.setDataObjs([this.pull, this.push]);
	                if (cmd == this.CMD.SUBSCRIBE) {
	                    this.subscriber.subscribe(component, tickers, callbacks, type);
	                } else if (cmd == this.CMD.UNSUBSCRIBE) {
	                    this.subscriber.unSubscribe(component, tickers, callbacks, type);
	                }
	            }
	        },
	        __updateData: function(subTicker, needUpdateField) {
	            var subscriber = this.subscriber;
	            if (!subscriber) {
	                return;
	            }
	            var subMap = QSAPI.SubscriberManager.getSubscription(subTicker);
	            for (var subID in subMap) {
	                for (var gkey in subMap[subID]) {
	                    var tickerObj = QSAPI.Cache.getTickerData(gkey),
	                        component = QSAPI.SubscriberManager.getComponent(subID);
	                    $.extend(tickerObj, { MSTicker: subTicker });
	                    if (needUpdateField["sourceType"] === 'push' && !QSAPI.isFod()) {
	                        this.setDataRecord([subTicker + '-' + tickerObj.listTicker]);
	                    }
	                    if (component && typeof component.updateData == 'function') {
	                        if (!this.__checkValidData(component, needUpdateField, this.getRDType(tickerObj))) {
	                            continue;
	                        }
	                        if (!tickerObj || !tickerObj.getField) {
	                            continue;
	                        }
	                        var sourceType = (component.dataType || "").toLowerCase();
	                        if (sourceType === 'push' || sourceType === 'streaming') {
	                            needUpdateField.sourceType = 'push';
	                        }
	                        component.updateData(tickerObj, needUpdateField);
	                    }
	                }
	            }
	        },
	        __checkValidData: function(component, needUpdateField, RDType) {
	            var subType = this.getSubscribeType(component);
	            var sourceTypeMatch = subType[needUpdateField.sourceType] || (subType['push'] && needUpdateField.sourceType === 'pull');
	            if (sourceTypeMatch) {
	                var forceDelay = QSAPI.Util.getValidForceDelay(component.forceDelay);
	                if (forceDelay) {
	                    return needUpdateField["RDType"] === "D";
	                } else {
	                    return needUpdateField["RDType"] === RDType;
	                }
	            }
	            return sourceTypeMatch;
	        },
	        getRDType: function(tickerObj) {
	            tickerObj = tickerObj || {};
	            return this.permissionChecker.getDataSource(tickerObj.listMarket || tickerObj.tenforeCode, tickerObj.type);
	        },
	        getSubscribeType: function(component) {
	            var subType = {},
	                sourceType = (component.dataType || "").toLowerCase();
	            if (!sourceType) {
	                sourceType = this.defaultSourceType;
	            }
	            if (sourceType == "streaming" || sourceType == "push" || sourceType == "mix") {
	                subType["push"] = true;
	            }
	            if ((sourceType != "streaming" && sourceType != "push") || sourceType == "mix") {
	                subType["pull"] = true;
	            }
	            return subType;
	        },
	        __isValidComponent: function(component) {
	            if (!component || !component.subscribeID) {
	                return false;
	            }
	            return true;
	        },
	        __isConnect: function(type) {
	            if (type == this.TYPE.MIX) {
	                return this.connResult["pull"] == this.connStatus.success && this.connResult["push"] == this.connStatus.success;
	            } else {
	                return this.connResult[type] == this.connStatus.success;
	            }
	        },
	        refresh: function(component, tickers, callback) {
	            var callbackInvoker;
	            var self = this;
	            var tickerObjects = [];
	            var cacheTickers = [];
	            var cachedMarketData = [];
	            if ($.isFunction(callback)) {
	                callbackInvoker = function(data, isCacheTickers) {
	                    if (data && data.length) {
	                        var l = data.length,
	                            subTicker, tickerObj, subTickers;
	                        for (var i = 0; i < l; i++) {
	                            subTicker = data[i].MSTicker;
	                            if (subTicker) {
	                                var tickerList = isCacheTickers ? cacheTickers : tickerObjects;
	                                for (var j = 0, jl = tickerList.length; j < jl; j++) {
	                                    tickerObj = tickerList[i];
	                                    subTickers = self.getSubscribeTicker(tickerList[i], component).replace(/\|L2/g, '').split(",");
	                                    if ($.inArray(subTicker, subTickers) != -1) {
	                                        QSAPI.Cache.setTickerData(tickerObj.listTicker, { MSTicker: subTicker });
	                                        callback(tickerObj.gkey, self.dataHandler["core"].getNeedUpdateFields(component, tickerObj.gkey, data[i]));
	                                    }
	                                }
	                            }
	                        }
	                    }
	                };
	            }
	            if (component._initialization_) {
	                for (var i = 0, l = tickers.length, _ticker; i < l; i++) {
	                    _ticker = tickers[i];
	                    var marketdata = QSAPI.Cache.getMarketDataByGkey(component, _ticker.gkey);
	                    if ($.isEmptyObject(marketdata)) {
	                        tickerObjects.push(_ticker);
	                    } else {
	                        cacheTickers.push(_ticker);
	                        cachedMarketData.push(marketdata);
	                    }
	                }
	            } else {
	                tickerObjects = tickers;
	            }
	            if ($.isFunction(callbackInvoker) && cacheTickers.length > 0) {
	                callbackInvoker(cachedMarketData, true);
	            }
	            QSAPI.DataManager.getQuoteData(component, tickerObjects, callbackInvoker);
	        }
	    };
	    $.extend(QSAPI, {
	        cfg: {
	            env: 'stage',
	            tryCount: 1, //re-connection count
	            autoStopPull: true, //Auto stop pull when session is invalid.
	            autoRefresh: true, //pull/push data by default,
	            mergePrePost: false, //merge pre/post to last
	            streamingDataType: 'R',
	            baseAPIGateway: 'https://www.us{0}-api.morningstar.com/{1}',
	            fod: false,
	            needTrack: true
	        },
	        uiCfg: {},
	        version: '2.9.0',
	        instId: null,
	        DataManager: new QSAPI.ClassDataManager(), //prevent from mutil widget converage, like autocomplete will cover chart
	        Authentication: null,
	        iframeHandler: null,
	        loginStatus: false,
	        qsPath: null,
	        qsPullPath: null,
	        qsAlertPath: null,
	        initCallback: null,
	        apiTokenExpiredCallback: null,
	        CSRFToken: null,
	        init: function(instId, cfg, callback) {
	            if ($.isPlainObject(instId) && $.isFunction(cfg)) {
	                callback = cfg;
	                cfg = instId;
	                instId = cfg.instId;
	            }
	            var self = this;
	            this.instId = instId;
	            this.initCallback = callback;
	            this.apiTokenExpiredCallback = cfg.apiTokenExpiredCallback;
	            if (cfg) {
	                $.extend(true, this.cfg, cfg);
	            }
	            if (this.isAPIGateway()) {
	                this.cfg.env = this.getAPIGatewayEnvAlias(this.cfg.env);
	            } else {
	                this.cfg.env = this.getMarketsEnvAlias(this.cfg.env);
	            }
	            this.qsPath = this.urlQSPath(this.cfg.qsPath);
	            this.qsPullPath = this.urlQSPullPath(this.cfg.qsPullPath);
	            this.qsAlertPath = this.urlQSAlertPath(this.cfg.qsAlertPath);
	            this.qsPushPath = this.urlQSPushPath(this.cfg.qsPushPath);
	
	            if (this.Authentication) {
	                if (this.authCompleted) {
	                    callback(this.authResult);
	                } else {
	                    this.Authentication.setCallback(callback);
	                }
	                return;
	            }
	            this.Authentication = new QSAPI.ClassAuthentication(this.instId, {
	                qsPath: this.qsPath,
	                tryCount: this.cfg.tryCount
	            }, {
	                afterAuthenticate: function(o) {
	                    if (!self.isAPIGateway()) {
	                        self.cfg.sessionKey = o.SessionID;
	                    }
	                    self.loginStatus = true;
	                    self.CSRFToken = o.CToken;
	                    var exchangeInfo = o.ExchangeInfo;
	                    var timezoneInfoArr = o.Timezone;
	                    var externalToken = o.token;
	                    var shortName = {},
	                        timezone = {},
	                        exchangeTimezone = {},
	                        micCode = {},
	                        exchangeName = {},
	                        l2Permission = {};
	                    for (var mode in o.l2) {
	                        for (var i = 0, modeExch = o.l2[mode]; i < modeExch.length; i++) {
	                            var exch = modeExch[i];
	                            if (!l2Permission[exch]) {
	                                l2Permission[exch] = [];
	                            }
	                            if ($.inArray(mode, l2Permission[exch]) == -1) {
	                                l2Permission[exch].push(mode);
	                            }
	                        }
	                    }
	                    for (var exch in exchangeInfo) {
	                        var exchInfo = exchangeInfo[exch];
	                        shortName[exch] = exchInfo.shortName;
	                        exchangeTimezone[exch] = exchInfo.gmtOffsetByMinutes;
	                        if (exchInfo.micCode && exchInfo.micCode != '') {
	                            micCode[exchInfo.micCode] = exchInfo.shortName;
	                        }
	                        exchangeName[exch] = exchInfo.name;
	                    }
	                    self.externalToken = externalToken;
	                    self.DataManager.setCfg({
	                        tryCount: self.cfg.tryCount, //set tryCount
	                        qsPath: self.qsPath,
	                        qsPullPath: self.qsPullPath,
	                        dataPermission: o.enablement || [],
	                        exchangeInfo: exchangeInfo,
	                        exchangeMarketTimeMap: o.marketTime || {},
	                        exchangeMap: shortName,
	                        exchangeTimezoneMap: exchangeTimezone,
	                        timezoneMap: timezoneInfoArr,
	                        MICExangeMap: micCode,
	                        exchangeNameMap: exchangeName,
	                        pullSubSeparator: o.pullSeparator || '_',
	                        pushSubSeparator: o.pushSeparator || '|',
	                        frequency: self.cfg.pullFrequency,
	                        autoStopPull: self.cfg.autoStopPull,
	                        l2Permission: l2Permission,
	                        streamingDataType: self.cfg.streamingDataType,
	                        baseAPIGateway: self.cfg.baseAPIGateway
	                    });
	                    self.DataManager.init();
	                }
	            });
	            var originalCallback = callback;
	            var globalStatus = QSAPI.Util.getGlobalStatus();
	            callback = function(r) {
	                self.authCompleted = true;
	                if (r && r.errorCode == globalStatus.AUTHOK.errorCode) {
	                    self.authResult = globalStatus.AUTHOK;
	                } else {
	                    self.authResult = globalStatus.AUTHERR;
	                }
	                if ($.isFunction(originalCallback)) {
	                    originalCallback.apply(this, arguments);
	                }
	            };
	            if (this.cfg.sessionKey && this.cfg.sessionKey !== '') {
	                this.Authentication._innerLogin({ sessionId: this.cfg.sessionKey }, {
	                    onSuccess: callback,
	                    onFailure: callback
	                })
	            } else if (this.cfg.profileurl && this.cfg.profileurl !== '') {
	                this.Authentication._innerLogin({ userProfile: this.cfg.profileurl }, {
	                    onSuccess: callback,
	                    onFailure: callback
	                })
	            } else if (this.cfg.accessToken && this.cfg.accessToken !== '') {
	                this.Authentication._innerLogin({ accessToken: this.cfg.accessToken }, {
	                    onSuccess: callback,
	                    onFailure: callback
	                })
	            }
	        },
	        setCfg: function(config) { //for API usage
	            config = config || {};
	            if (!this.cfg.sessionKey) {
	                this.cfg.sessionKey = config.sessionKey;
	            }
	            if (!this.cfg.session_sessionKey) {
	                this.cfg.session_sessionKey = config.session_sessionKey;
	            }
	            if (!this.CSRFToken) {
	                this.CSRFToken = config.CToken;
	            }
	            this.loginStatus = true;
	            this.DataManager.setCfg($.extend(true, {
	                tryCount: this.cfg.tryCount, //set tryCount
	                qsPath: this.qsPath,
	                qsPullPath: this.qsPullPath
	            }, config));
	            this.DataManager.init();
	        },
	        trackComponentUsage: function(outerTrackInfo) {
	            var self = this;
	            // from each JSP, the track request will send by apimanager
	            if (!this.cfg.needTrack) {
	                return;
	            }
	            // as like autocomplete/minichart as a sub component, not need send this request.
	            if (typeof outerTrackInfo.needTrack !== 'undefined' && !outerTrackInfo.needTrack) {
	                return;
	            }
	
	            var trackInfo = $.extend({
	                sdkVersion: this.getVersion(),
	                productType: this.getProductType(),
	                componentType: outerTrackInfo.componentType,
	                clientDomain: window.location.hostname
	            }, this.cfg.trackInfo, outerTrackInfo);
	
	            // send request, ignore it success or not.
	            QSAPI.Util.ajax({
	                url: QSAPI.urlQSPath() + 'trackComponentUsage.jsp',
	                data: trackInfo,
	                type: "get",
	                dataType: "json"
	            });
	        },
	        setTrackActionValue: function(trackValue) {
	            this.cfg.needTrack = trackValue;
	        },
	        getTrackActionValue: function() {
	            return this.cfg.needTrack;
	        },
	        setUICfg: function(cfg) {
	            $.extend(true, this.uiCfg, cfg);
	        },
	        getAPIGatewayEnvAlias: function(env) {
	            if (typeof env !== 'string') {
	                return env;
	            }
	            env = env.toLowerCase();
	            // APIGateWay has stg, uat, prod three environments
	            // markets prestage / loader qa => stg
	            // markets stage / loader uat => uat
	            switch (env) {
	                case 'prestage':
	                case 'pre-stage':
	                case 'qa':
	                case 'dev':
	                    env = 'stg';
	                    break;
	                case 'stage':
	                case 'uat':
	                    env = 'uat';
	                    break;
	                case 'production':
	                case 'live':
	                case 'prod':
	                    env = 'prod';
	                    break;
	            }
	            return env;
	        },
	        getMarketsEnvAlias: function(env) {
	            if (typeof env !== 'string') {
	                return env;
	            }
	            env = env.toLowerCase();
	            switch (env) {
	                case 'qa':
	                case 'dev':
	                    env = 'prestage';
	                    break;
	                case 'uat':
	                    env = 'stage';
	                    break;
	                case 'prod':
	                    env = 'production';
	                    break;
	            }
	            return env;
	        },
	        createWidget: function(container, widgetURL, config, callbacks) {
	            if (this.checkLogin()) {
	                var cfg = {
	                    instid: this.instId,
	                    sessionKey: this.cfg.sessionKey,
	                    qsDomain: QSAPI.Util.formatString('//{0}.morningstar.com/', this.urlMarketsBase(this.cfg.env).web),
	                    proxyurl: this.cfg.proxyurl
	                };
	                if (this.iframeHandler) {
	                    this.iframeHandler.setConfig(cfg);
	                } else {
	                    this.iframeHandler = new QSAPI.IframeHandler(cfg);
	                }
	                this.xwinProxy = this.iframeHandler.getXWinProxy();
	                return this.iframeHandler.create(container, widgetURL, config, callbacks);
	            }
	        },
	        setTicker: function(ticker, id) {
	            if (this.iframeHandler) {
	                this.iframeHandler.setTicker(ticker, id);
	            }
	        },
	        call: function(functionName, parameter) {
	            if (this.iframeHandler) {
	                this.iframeHandler.call(functionName, parameter);
	            }
	        },
	        getUICfg: function(key) {
	            return this.uiCfg[key] || {};
	        },
	        getQSPath: function() {
	            return this.qsPath;
	        },
	        getQSPullPath: function() {
	            return this.qsPullPath;
	        },
	        getQSPushPath: function() {
	            return window.location.protocol + this.qsPushPath;
	        },
	        getQSAlertPath: function() {
	            return this.qsAlertPath;
	        },
	        getInstID: function() {
	            return this.instId;
	        },
	        setSessionId: function(session) {
	            if (!session) {
	                return;
	            }
	            if (this.cfg.sessionKey) {
	                this.cfg.sessionKey = session
	            }
	            if (this.cfg.session_sessionKey) {
	                this.cfg.session_sessionKey = session
	            }
	        },
	        getSessionId: function(session) {
	            session = session === true ? true : false;
	            return !session ? this.cfg.sessionKey : this.cfg.session_sessionKey;
	        },
	        getProductType: function(productType) {
	            if (productType) {
	                return productType;
	            }
	            return this.cfg.productType || 'sdk';
	        },
	        checkLogin: function() {
	            return this.loginStatus;
	        },
	        getQSVersion: function() {
	            return this.cfg.env;
	        },
	        setLoginStatus: function(loginStatus) {
	            this.loginStatus = loginStatus;
	        },
	        getPullFrquency: function() {
	            if (this.DataManager.pull != null) {
	                return this.DataManager.pull.frequency;
	            }
	            return null;
	        },
	        getInitCallback: function() {
	            return this.initCallback;
	        },
	        getApiTokenExpiredCallback: function() {
	            return this.apiTokenExpiredCallback;
	        },
	        getCSRFToken: function() {
	            return this.CSRFToken;
	        },
	        getExternalToken: function() {
	            return this.externalToken;
	        },
	        getVersion: function() {
	            return this.version;
	        },
	        isAutoRefresh: function() { //Default is true
	            return this.cfg.autoRefresh !== false;
	        },
	        isMergePrePost: function() {
	            return this.cfg.mergePrePost === true;
	        },
	        noConflict: function() {
	            var _QSAPI = {};
	
	            _QSAPI = window.QSAPI;
	
	            // delete the window.QSAPI
	            window.QSAPI = null;
	            delete window.QSAPI;
	
	            return _QSAPI;
	        },
	        setAccessToken: function(accessToken) {
	            this.cfg.accessToken = accessToken;
	        },
	        getAccessToken: function() {
	            return this.cfg.accessToken;
	        },
	        isAPIGateway: function() {
	            return this.cfg.accessToken && this.cfg.accessToken.length > 0;
	        },
	        isFod: function() {
	            return this.cfg.fod && this.isAPIGateway();
	        },
	        urlAPIGatewayBase: function(env, path) {
	            switch (env) {
	                case 'stg':
	                case 'uat':
	                    env = '-' + env;
	                    break;
	                default:
	                    env = '';
	                    break;
	            }
	            return QSAPI.Util.formatString(this.cfg.baseAPIGateway, env, path);
	        },
	        urlMarketsBase: function(env) {
	            var base = {
	                'web': 'quotespeed',
	                'pull': 'pullqs',
	                'alert': 'qsalert',
	                'push': 'nnpush',
	                'IWT': 'lt.morningstar.com/api/rest.svc/'
	            };
	            switch (env) {
	                case 'qa':
	                case 'dev':
	                case 'pre-stage':
	                case 'prestage':
	                    base.web = base.pull = base.alert = 'qswebbdevwb6001';
	                    base.push = 'devmsquoteeng82';
	                    base.IWT = 'euiwtqaint.morningstar.com/api/rest.svc/';
	                    break;
	                case 'uat':
	                case 'stage':
	                    base.web = base.pull = base.alert = 'markets-uat';
	                    base.push = 'qsfeed-stg';
	                    base.IWT = 'eultrc.morningstar.com/api/rest.svc/';
	                    break;
	                case 'pre-release':
	                case 'prerelease':
	                    base.web = base.pull = base.alert = 'qspre';
	                    break;
	            }
	            return base;
	        },
	        urlQSPath: function(url) {
	            if (url) {
	                return url;
	            } else {
	                return this.isAPIGateway() ? this.urlAPIGatewayBase(this.cfg.env, 'QS-markets/') : QSAPI.Util.formatString('//{0}.morningstar.com/', this.urlMarketsBase(this.cfg.env).web);
	            }
	        },
	        urlQSPullPath: function(url, context) {
	            if (url) {
	                return url;
	            } else {
	                context = context || 'pull';
	                return this.isAPIGateway() ? this.urlAPIGatewayBase(this.cfg.env, 'QS-pullqs/' + context + '/') : QSAPI.Util.formatString('//{0}.morningstar.com/pullservice/{1}/', this.urlMarketsBase(this.cfg.env).pull, context);
	            }
	        },
	        urlQSAlertPath: function(url) {
	            if (url) {
	                return url;
	            } else {
	                return QSAPI.Util.formatString('//{0}.morningstar.com/', this.urlMarketsBase(this.cfg.env).alert);
	            }
	        },
	        urlQSPushPath: function(url) {
	            if (url) {
	                return url;
	            } else {
	                return QSAPI.Util.formatString('//{0}.morningstar.com', this.urlMarketsBase(this.cfg.env).push);
	            }
	        },
	        urlFODQuote: function(url) {
	            if (url) {
	                return url;
	            } else {
	                return this.urlAPIGatewayBase(this.cfg.env, 'QS-fod/fodrouter/index.php');
	            }
	        },
	        urlFODTimeseries: function(url) {
	            if (url) {
	                return url;
	            } else {
	                return this.urlAPIGatewayBase(this.cfg.env, 'QS-fod/fodrouter/indexTS');
	            }
	        },
	        urlFODPushPath: function(url) {
	            if (url) {
	                return url;
	            } else {
	                return this.urlAPIGatewayBase(this.cfg.env, 'QS-fod/fodrouter/streaming?Startstream');
	            }
	        },
	        urlIWTTimeSeriesPath: function(url) {
	            if (url) {
	                return url;
	            } else {
	                return 'https://' + this.urlMarketsBase(this.cfg.env).IWT;
	            }
	        }
	    });
	    /*it is used to append chart to datamanegr even there are two core module.*/
	    if (!QSAPI.DataManager.Chart && QSAPI.Widget.Chart && QSAPI.Widget.Chart.DataManager) {
	        QSAPI.DataManager.Chart = new QSAPI.Widget.Chart.DataManager();
	    }
	})(QSAPI);
	// Expose QSAPI in window
	if ( typeof noGlobal === strundefined ) {
	    console.info('QSAPI is  window');
	    window.QSAPI = QSAPI;
	}
	
		return QSAPI;
	});

/***/ }),
/* 5 */
/***/ (function(module, exports) {

	/* WEBPACK VAR INJECTION */(function(__webpack_amd_options__) {module.exports = __webpack_amd_options__;
	
	/* WEBPACK VAR INJECTION */}.call(exports, {}))

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(1), __webpack_require__(3), __webpack_require__(7), __webpack_require__(8)], __WEBPACK_AMD_DEFINE_RESULT__ = function(morningstar, util, container, eventsFactory) {
	    'use strict';
	    var log = util.log;
	    var QSAPI = morningstar.components[util.getComponentName('core')];
	    var $ = QSAPI.$;
	    var QUIKR_VERSION = '2.29.0';
	
	    function _call(deferred, fun) {
	        switch (deferred.state()) {
	            case 'pending':
	                deferred.done(fun);
	                break;
	            case 'resolved':
	                fun();
	                break;
	            case 'rejected':
	                log.info("Authenciation is failed.");
	                break;
	        }
	    }
	    /**
	     * add for page-buidler
	     */
	    function migrateConfiguration(initialConfiguration) {
	        if (!$.isEmptyObject(initialConfiguration.component.configuration)) {
	            return initialConfiguration;
	        }
	        var _needMigrateConfiguration = {};
	        var noNeedMigratePropertyArray = ['instid', 'sessionKey', 'qsPath', 'qsPullPath', 'component'];
	        $.each(initialConfiguration, function(value) {
	            if ($.inArray(value, noNeedMigratePropertyArray) < 0) {
	                _needMigrateConfiguration[value] = initialConfiguration[value];
	            }
	        });
	        initialConfiguration.component.configuration = _needMigrateConfiguration;
	        return initialConfiguration;
	    }
	
	    function trackQuikrComponentUsage(needTrack, trackInfo) {
	        if (!QSAPI.getTrackActionValue()) {
	            return;
	        }
	        if (QSAPI.trackComponentUsage && needTrack) {
	            QSAPI.trackComponentUsage(trackInfo);
	        }
	    }
	
	    function init(componentType, component, element, initialConfiguration, actionHandler) {
	        initialConfiguration.component = initialConfiguration.component || {};
	        initialConfiguration.component.configuration = initialConfiguration.component.configuration || {};
	        initialConfiguration.component.configuration.quikrVersion = QUIKR_VERSION;
	        initialConfiguration = migrateConfiguration(initialConfiguration);
	
	        // needTrack mark if the independent qukir component or not.
	        // needTrackOnConfiguration mark sub component doesn't send track request.
	        var needTrackOnConfiguration = initialConfiguration.component.configuration.needTrack;
	        var needTrack = !!component.properties.needTrack;
	        if (typeof needTrackOnConfiguration !== 'undefined' && !needTrackOnConfiguration) {
	            needTrack = false;
	        }
	        var trackInfo = $.extend({
	            needTrack: needTrack,
	            componentType: componentType,
	            quikrVersion: QUIKR_VERSION
	        }, initialConfiguration.component.configuration.trackInfo);
	        trackQuikrComponentUsage(needTrack, trackInfo);
	
	        return component.init($(element), initialConfiguration.component || {}, actionHandler);
	    }
	
	    function extendInitConfig(initialConfiguration) {
	        initialConfiguration.component = initialConfiguration.component || {};
	        initialConfiguration.component.configuration = initialConfiguration.component.configuration || {};
	        $.extend(initialConfiguration, {
	            instid: morningstar.asterix.config.getOption('instid'),
	            env: morningstar.asterix.config.getOption('env'),
	            sessionKey: morningstar.asterix.context.getToken('marketsApiToken'),
	            accessToken: morningstar.asterix.context.getToken('apiGatewayToken')
	        });
	    }
	
	    /**
	     * @function apiTokenExpiredCallback from 
	     * 1.callbacks parameter(Quikir init way),
	     * 2.morningstar.asterix.context.getApiTokenExpiredCallback() (ec-loader)
	     */
	    function getApiTokenExpiredCallback(initConfig) {
	        if (initConfig.component.callbacks.apiTokenExpiredCallback &&
	            typeof initConfig.component.callbacks.apiTokenExpiredCallback === 'function') {
	
	            return initConfig.component.callbacks.apiTokenExpiredCallback;
	        } else if (morningstar.asterix.context.getApiTokenExpiredCallback &&
	            typeof morningstar.asterix.context.getApiTokenExpiredCallback() === 'function') {
	
	            return transformSDKToApiTokenFromLoader;
	        } else {
	            return null;
	        }
	    }
	
	    function transformSDKToApiTokenFromLoader(options, handleApiTokenExpiredCallback) {
	        var apiTokenExpiredCallbackFromLoader = morningstar.asterix.context.getApiTokenExpiredCallback();
	
	        // transform 
	        // accessToken => apiGatewayToken from sdk to ec-loader,
	        // sessionKey => marketsApiToken from sdk to ec-loader
	        if (options['token'].name === 'accessToken') {
	            options['token'].name = 'apiGatewayToken';
	        } else if (options['token'].name === 'sessionKey') {
	            options['token'].name = 'marketsApiToken';
	        }
	
	        apiTokenExpiredCallbackFromLoader(options, function(tokenName, tokenValue) {
	            // transform 
	            // apiGatewayToken => accessToken from ec-loader to sdk,
	            // marketsApiToken => sessionKey from ec-loader to sdk,
	            if (tokenName === 'apiGatewayToken') {
	                tokenName = 'accessToken';
	            } else if (tokenName === 'marketsApiToken') {
	                tokenName = 'sessionKey';
	            }
	            handleApiTokenExpiredCallback(tokenName, tokenValue);
	        });
	    }
	
	    function executeCallbacks(config, func, args) {
	        if (config.component && config.component.callbacks && $.isFunction(config.component.callbacks[func])) {
	            config.component.callbacks[func](args);
	        }
	    }
	
	    function registerCallbacks(component, callbacks, properties) {
	        var eventList = properties.events;
	        var cbs = $.extend(true, {}, callbacks);
	        if ($.isFunction(component.on)) {
	            for (var event in eventList) {
	                var callbackName = eventList[event];
	                if (cbs && cbs[callbackName]) {
	                    component.on(event, cbs[callbackName]);
	                }
	            }
	        }
	        replaceCallbacksToEvents(component, callbacks, properties);
	        return component;
	    }
	
	    function replaceCallbacksToEvents(component, callbacks, properties) {
	        var eventsList = properties.events;
	        var callbackName;
	        for (var event in eventsList) {
	            callbackName = eventsList[event];
	            callbacks[callbackName] = (function(event) {
	                return function(data) {
	                    component.trigger(event, data);
	                }
	            })(event);
	        }
	    }
	
	    function setCallbacks(callbacks, eventList, _toBeTrigerdEvents) {
	        for (var event in eventList) {
	            var callbackName = eventList[event];
	            if (!callbacks[callbackName]) {
	                callbacks[callbackName] = (function(event) {
	                    return function(data) {
	                        _toBeTrigerdEvents[event] = {
	                            eventName: event,
	                            args: data
	                        };
	                    }
	                })(event);
	            }
	        }
	    }
	
	    function _createComponent(componentType, containerInstance, params, component) {
	        var deferred = $.Deferred(),
	            _component,
	            _properties = $.extend({
	                setter: {},
	                getter: {},
	                events: {}
	            }, component.properties),
	            _toBeTrigerdEvents = {},
	            _initConfig = $.extend(true, {}, params.initialConfiguration),
	            sessionKey = '',
	            accessToken = '',
	            apiTokenExpiredCallback = null,
	            actionHandler = params.actionHandler;
	
	        extendInitConfig(_initConfig);
	        sessionKey = _initConfig.sessionKey;
	        accessToken = sessionKey ? '' : _initConfig.accessToken; // if has sessionKey, do not use accessToken
	        _initConfig.component.callbacks = _initConfig.component.callbacks || {};
	        setCallbacks(_initConfig.component.callbacks, _properties.events, _toBeTrigerdEvents);
	        apiTokenExpiredCallback = getApiTokenExpiredCallback(_initConfig);
	        _component = eventsFactory();
	        _component = registerCallbacks(_component, _initConfig.component.callbacks, _properties);
	        if (component.authorized || _initConfig.authorized) {
	            var inst = init(componentType, component, params.element, _initConfig);
	            _component = $.extend(true, inst, _component);
	            deferred.resolve();
	        } else {
	            if (sessionKey && sessionKey !== "" || accessToken) {
	                QSAPI.init(_initConfig.instid, {
	                    env: _initConfig.env,
	                    sessionKey: _initConfig.sessionKey,
	                    qsPath: _initConfig.qsPath,
	                    qsPullPath: _initConfig.qsPullPath,
	                    accessToken: accessToken,
	                    productType: _initConfig.component.configuration.productType || 'quikr',
	                    apiTokenExpiredCallback: apiTokenExpiredCallback,
	                    fod: _initConfig.fod
	                }, function(result) {
	                    if (result.errorCode == 0) {
	                        _initConfig.component.configuration = $.extend(true, _initConfig.component.configuration, QSAPI.getExternalToken());
	                        var inst = init(componentType, component, params.element, _initConfig, actionHandler);
	                        _component = $.extend(true, inst, _component);
	                        deferred.resolve();
	                    } else {
	                        executeCallbacks(_initConfig, "onError", result);
	                        deferred.reject();
	                        log.error("Session id is invalid!");
	
	                    }
	                });
	            } else {
	                var result = {
	                    errorCode: "-9",
	                    errorMsg: "Session id is invalid."
	                };
	                executeCallbacks(_initConfig, "onError", result);
	                deferred.reject();
	                log.error("Session id is invalid!");
	            }
	        }
	        return {
	            setProperty: function(name, value) {
	                value = util.clone(value);
	                _call(deferred, function() {
	                    if (_component.setProperty) {
	                        _component.setProperty(name, value);
	                    } else {
	                        var methodName = _properties.setter[name],
	                            method = methodName && _component[methodName] ? _component[methodName] : _component[name];
	                        if ($.isFunction(method)) {
	                            if (name === 'parentSize') {
	                                if (value.width > 0 || value.height > 0) {
	                                    if (_initConfig.component && _initConfig.component.callbacks && $.isFunction(_initConfig.component.callbacks.onResize)) {
	                                        _initConfig.component.callbacks.onResize(value.width, value.height);
	                                    }
	                                    method.apply(_component, [value.width, value.height]);
	                                } else {
	                                    log.debug('invalid width:' + value.width + ' and height ' + value.height + ' for component');
	                                }
	                            } else {
	                                method.apply(_component, [value]);
	                            }
	                        } else {
	                            log.debug('invalid property:' + name + ' for component');
	                        }
	                    }
	                });
	            },
	            getProperty: function(name) {
	                var ret = {};
	                if (_component) {
	                    if (_component.getProperty) {
	                        ret = _component.getProperty(name);
	                    } else {
	                        var methodName = _properties.getter[name];
	                        if (methodName && $.isFunction(_component[methodName])) {
	                            ret = _component[methodName]();
	                        } else if ($.isFunction(_component[name])) {
	                            ret = _component[name]();
	                        } else {
	                            log.debug('invalid property:' + name + ' for component');
	                        }
	                    }
	                } else {
	                    log.debug('Component is not initialized');
	                }
	                return ret;
	            },
	            setParameter: function(name, value) {
	                value = util.clone(value);
	                this.setProperty(name, value);
	            },
	            getParameter: function(name) {
	                this.getProperty(name);
	            },
	            on: function(eventName, callback) {
	                _component.toBeTrigerdEvents = _toBeTrigerdEvents;
	                _component.on(eventName, callback);
	            },
	            off: function(eventName, callback) {
	                _component.off(eventName, callback);
	                if (_toBeTrigerdEvents[eventName]) {
	                    delete _toBeTrigerdEvents[eventName];
	                }
	            },
	            trigger: function(eventName, args) {
	                _component.trigger(eventName, args);
	            },
	            destroy: function() {
	                containerInstance.off();
	                _call(deferred, function() {
	                    var method = _component.destroy || _component.clear;
	                    if ($.isFunction(method)) {
	                        method.apply(_component, []);
	                    } else {
	                        log.debug('missing destroy method for component');
	                    }
	                });
	            }
	        };
	    }
	
	    function register(name, component) {
	        var componentName = util.getComponentName(name);
	        morningstar.asterix.register(componentName);
	        var _component = morningstar.components[componentName];
	        _component.name = componentName;
	        _component.__version__ = QUIKR_VERSION;
	        _component.createComponent = function(params) {
	            var containerInstance = container($(params.element));
	            var instance = _createComponent(name, containerInstance, params, component);
	            containerInstance.on(containerInstance.events.resize, function(data) {
	                instance.setProperty('parentSize', {
	                    width: data.width,
	                    height: data.height
	                });
	            });
	            containerInstance.on(containerInstance.events.empty, function() {
	                instance.destroy();
	            });
	            return instance;
	        };
	        return _component;
	    }
	
	    return {
	        register: register
	    }
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(1), __webpack_require__(3)], __WEBPACK_AMD_DEFINE_RESULT__ = function(morningstar, util) {
	    'use strict';
	    var EVENT = 'morningstar-markets-container-event',
	        EVENT_DATA = EVENT + '-data',
	        EVENTS = {
	            "resize": EVENT + "-resize",
	            "empty": EVENT + "-empty"
	        },
	        $ = morningstar.components[util.getComponentName('core')].$,
	        timeout_id,
	        $elements = $([]);
	
	    $.event.special[EVENT] = {
	        setup: function() {
	            var $element = $(this);
	            $elements = $elements.add($element);
	            $element.data(EVENT_DATA, {
	                width: width($element),
	                height: height($element),
	                hasChild: false
	            });
	            if ($elements.length === 1) {
	                loopy();
	            }
	        },
	        teardown: function() {
	            var $element = $(this);
	            $elements = $elements.not($element);
	            $element.removeData(EVENT_DATA);
	            if (!$elements.length) {
	                clearTimeout(timeout_id);
	            }
	        }
	    };
	
	    function loopy() {
	        timeout_id = window.setTimeout(function() {
	            $elements.each(function() {
	                var $element = $(this),
	                    w = width($element),
	                    h = height($element),
	                    hasChild = $element.children().length > 0,
	                    data = $element.data(EVENT_DATA) || {};
	                if (data.width !== w || data.height !== h) {
	                    data.width = w;
	                    data.height = h;
	                    $element.data(EVENT_DATA, data);
	                    $element.trigger(EVENT, {
	                        event: EVENTS.resize,
	                        width: w,
	                        height: h
	                    });
	                }
	                if (data.hasChild !== hasChild) {
	                    if (data.hasChild === true && hasChild === false) {
	                        $element.trigger(EVENT, {
	                            event: EVENTS.empty
	                        });
	                    }
	                    data.hasChild = hasChild;
	                    $element.data(EVENT_DATA, data);
	                }
	            });
	            loopy();
	        }, 100);
	    }
	
	    function width($element) {
	        var isWindow = $.isWindow($element) || $element.is('body');
	        return isWindow ? document.documentElement.clientWidth : $element.width();
	    }
	
	    function height($element) {
	        var isWindow = $.isWindow($element) || $element.is('body');
	        return isWindow ? document.documentElement.clientHeight : $element.height();
	    }
	
	    function isValidEvent(event) {
	        var flag = false;
	        for (var key in EVENTS) {
	            if (event === EVENTS[key]) {
	                flag = true;
	                break;
	            }
	        }
	        return flag;
	    }
	
	    return function($element) {
	        var isWindow = $.isWindow($element) || $element.is('body');
	        var registeredEvents = {};
	        return {
	            events: EVENTS,
	            $element: $element,
	            on: function(event, fn) {
	                if (isValidEvent(event)) {
	                    registeredEvents[event] = true;
	                    $element.on(EVENT, function(e, data) {
	                        e.stopPropagation();
	                        if (data.event === event) {
	                            fn(data);
	                        }
	                    });
	                }
	            },
	            off: function(event) {
	                if (!event) {
	                    $element.off(EVENT);
	                } else if (registeredEvents[event]) {
	                    registeredEvents[event] = null;
	                    delete registeredEvents[event];
	                    if ($.isEmptyObject(registeredEvents)) {
	                        $element.off(EVENT);
	                    }
	                }
	            }
	        };
	    };
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(1), __webpack_require__(3)], __WEBPACK_AMD_DEFINE_RESULT__ = function(morningstar, util) {
	    'use strict';
	    var log = util.log;
	    var QSAPI = morningstar.components[util.getComponentName('core')];
	    var $ = QSAPI.$;
	
	    return function(instance) {
	        var instance = instance || {};
	        instance.callbackList = [];
	        instance.on = function(eventName, callback) {
	            instance.callbackList[eventName] = callback;
	            if (this.toBeTrigerdEvents.hasOwnProperty(eventName)) {
	                this.trigger(eventName, this.toBeTrigerdEvents[eventName].args);
	                delete this.toBeTrigerdEvents[eventName];
	            }
	        };
	        instance.off = function(eventName, callback) {
	            if (!instance.callbackList.hasOwnProperty(eventName)) {
	                return;
	            }
	            if (instance.callbackList.hasOwnProperty(eventName)) {
	                delete instance.callbackList[eventName];
	            }
	        };
	        instance.trigger = function(eventName, args) {
	            if (args === undefined) {
	                args = [];
	            }
	            if (instance.callbackList[eventName]) {
	                var callback = instance.callbackList[eventName];
	                callback.apply(this, [args]);
	            } else {
	                this.toBeTrigerdEvents[eventName] = {
	                    eventName: eventName,
	                    args: args
	                }
	            }
	        };
	        instance.toBeTrigerdEvents = {};
	        return instance;
	    };
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ })
/******/ ])
});
;
//# sourceMappingURL=markets-components-core.js.map