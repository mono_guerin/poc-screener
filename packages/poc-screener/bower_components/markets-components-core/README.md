# markets-components-core

## OverView
1. Morningstar Markets™ Authentication provides the interface of authentication.
2. Morningstar Markets™ Tickers provide a static method to generate tickers object.
3. Morningstar Markets™ DataManager offers clients capabilities to connect to various Morningstar data sources.

## Usage
The component require a authentication process as below.

1. Email to <a href="mailto:marketsdevelopment@morningstar.com">markets development team</a> to get instid and account. if you already have these, please skip this step.
2. Use the account to do authentication.

#### Authentication

You can invoke below web service provided by Markets team in your server side. And you could parse the response JSON to get the session ID. This id will be used when you initializing this component.

***Note: There is a IP white list with your servers. Please provide them to Marktes team before you invoke the web service.***

**Web service**

    Production URL: https://quotespeed.morningstar.com/service/auth
	Staging URL: https://markets-uat.morningstar.com/service/auth

**Request parameters**

<table cellpadding=0 cellspacing=0 border="1">
<thead>
<tr class="header">
<th>key</th>
<th>Required</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td>instid</td>
<td>Yes</td>
<td>An unique ID assigned to you by Morningstar markets.</td>
</tr>
<tr>
<td>instuid</td>
<td>Yes</td>
<td>Unique identifier for the user provided by client side. We suggest to set is as same as email.</td>
</tr>
<tr>
<td>email</td>
<td>Yes</td>
<td>Unique identifier for the user provided by client side.</td>
</tr>
<tr>
<td>group</td>
<td>Yes</td>
<td>User group is provided by Morningstar.</td>
</tr>
<tr>
<td>e</td>
<td>Yes</td>
<td>RTD credential, it will be used to pass the encrypted RTD account string. For how to get this string, please reference following links. <a gref="https://mswiki.morningstar.com/pages/viewpage.action?title=Entitlement+Integration&spaceKey=SWPAAS">https://mswiki.morningstar.com/pages/viewpage.action?title=Entitlement+Integration&spaceKey=SWPAAS</td>
</tr>
</tbody>
</table>

**Response JSON**


    {
	    "status": {
			"errorCode":"0",
			"subErrorCode":"",
			"errorMsg":"Sucessful"
		},
		"data":"95818CD5B0B2BC0E2F7B2577235F40E7",
		"attachment":{
			"SessionID":"95818CD5B0B2BC0E2F7B2577235F40E7",
			"sessionTimeout":1592
		}
    }


## Browser compatibility
Markets components are using browser native number formatting function (Intl) which is a ES6(ES2015) feature. It may not be supported in some lower browsers such as IE 10. If you want your product to compatible with these browsers, please add below polyfill to your page.

    <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Intl.~locale.en,Intl.~locale.fr"></script>

## Sample codes
       
    <script>
		var QSAPI = morningstar.components['markets-components-core'];
		QSAPI.init(INSTID,{
			env: 'stage',
			sessionKey: 'xxxxxxxxxxxxxxxxxxxxx'
		},function(result){
			if (result.errorCode == 0) {
	            //use the datamanager or components here
	        }else{
	            //reobtain the session id 
	        }
		});
	</script>    

## Methods

### init(instid, cfg, callback)
This method is used to do the authenciation with the options.

> instid: Required. It is provided by Morningstar markets team. eg.QSDEMO

> cfg: Required. it is JSON object as below table.
> <table cellpadding=0 cellspacing=0 border="1">
            <thead>
                <tr class="header">
                    <th align="left">JSON key</th>
                    <th align="left">Selectable value</th>
                    <th align="left">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr class="odd">
                    <td align="left">env</td>
                    <td align="left">"stage" and "production"</td>
                    <td align="left">This option is used in configuration the data environment, "stage" is a default setting.</td>
                </tr>
                <tr class="even">
                    <td align="left">tryCount</td>
                    <td align="left">Number</td>
                    <td align="left">This option is used in configuration the count of re-authenticate, the default setting is 1.</td>
                </tr>
                <tr class="even">
                    <td align="left">sessionKey</td>
                    <td align="left">String</td>
                    <td align="left">It is required when option profileurl is missing. You can get it through Morningstar service. About this, you can reference "Based on the session key".</td>
                </tr>
            </tbody>
        </table>

> callback: Required, It will be fired when authenciation is completed. Below table will show all data of the callback parameter. eg. functon(result){}
> <table cellpadding=0 cellspacing=0 border="1">
    <thead>
        <tr class="header">
            <th align="left">errorCode</th>
            <th align="left">errorMsg</th>
        </tr>
    </thead>
    <tbody>
        <tr class="odd">
            <td align="left">0</td>
            <td align="left">Authentication is successful.</td>
        </tr>
        <tr class="even">
            <td align="left">-2</td>
            <td align="left">Account is incorrect.</td>
        </tr>
        <tr class="odd">
            <td align="left">-3</td>
            <td align="left">Service temporarily unavailable.</td>
        </tr>
        <tr class="even">
            <td align="left">-4</td>
            <td align="left">Request timeout.</td>
        </tr>
        <tr class="odd">
            <td align="left">-5</td>
            <td align="left">Service not found.</td>
        </tr>
        <tr class="even">
            <td align="left">-6</td>
            <td align="left">Service response error.</td>
        </tr>
        <tr class="odd">
            <td align="left">-7</td>
            <td align="left">Authentication is expired.</td>
        </tr>
        <tr class="even">
            <td align="left">-8</td>
            <td align="left">Response is invalid.</td>
        </tr>
    </tbody>
</table>


### TickerFactory.create(tickers, callbacks)
This method will be used to create the ticker objects those will be required to use in datamanager.
> tickers: An array of symbols which could be symbol code, ISIN, CUSIP, performance Id, secId and realtime id.

> callbacks: A JSON object contains two key-function pairs.
>
    {
		onsuccess:function(tickerObjects){
		},
		onFailure:function(tickerObjects){
		}
	}
> Examples:

>
    QSAPI.TickerFactory.create(['0P0000B042', '0P0000ZTCC'], {
        onSuccess: function (tickerList) {
			for(var i=0;i<tickerList.length;i++){
                var ticker=tickerList[i];
                console.log(ticker.symbol);
            }
        }
    });


Each valid ticker object will contains  some fields as below table.
<table cellpadding=0 cellspacing=0 border="1">
    <thead>
        <tr class="header">
            <th align="left">Property</th>
            <th align="left">Description</th>
        </tr>
    </thead>
    <tbody>
        <tr class="odd">
            <td align="left">Name</td>
            <td align="left">The name of security.</td>
        </tr>
        <tr class="even">
            <td align="left">symbol</td>
            <td align="left">Unique of the ticker object, it can be ticker,perfomanceId and secId.</td>
        </tr>
        <tr class="odd">
            <td align="left">clientTicker</td>
            <td align="left">The ticker name from client.</td>
        </tr>
        <tr class="even">
            <td align="left">ticker</td>
            <td align="left">The standard ticker name of security.</td>
        </tr>
        <tr class="odd">
            <td align="left">tenforeTicker</td>
            <td align="left">The ticker name defined by Tenfore System of Morningstar.</td>
        </tr>
        <tr class="even">
            <td align="left">exch</td>
            <td align="left">The standard abbreviation of exchange</td>
        </tr>
        <tr class="odd">
            <td align="left">msexch</td>
            <td align="left">The exchage defined in MorningStar system.</td>
        </tr>
        <tr class="even">
            <td align="left">tenforeCode</td>
            <td align="left">The exchange code defined by Tenfore System of Morningstar. It is integer.</td>
        </tr>
        <tr class="odd">
            <td align="left">type</td>
            <td align="left">The security type defined by Tenfore System of Morningstar(1:Stock/ETF;2:Options;3:Futures;10:Indexes;8:Funds;20:Forex).</td>
        </tr>
        <tr class="even">
            <td align="left">mType</td>
            <td align="left">The security type defined by MorningStar Application System("ST":Stock;"FE":ETF;"Options":Options;"FO":Open-End Fund;"FC":Close-End Fund;"XI":Indexes;"FX":Forex).</td>
        </tr>
        <tr class="odd">
            <td align="left">performanceId</td>
            <td align="left">Unique identifier of the ticker in Morningstar system.</td>
        </tr>
        <tr class="even">
            <td align="left">secId</td>
            <td align="left">Unique identifier of the ticker in Morningstar system.It is equial to "performanceId" for Stock. It is used as an unique identifier of Fund.</td>
        </tr>
        <tr class="odd">
            <td align="left">country</td>
            <td align="left"></td>
        </tr>
        <tr class="even">
            <td align="left">currency</td>
            <td align="left">Currency of ticker.</td>
        </tr>
        <tr class="odd">
            <td align="left">CUSIP</td>
            <td align="left"></td>
        </tr>
        <tr class="even">
            <td align="left">ISIN</td>
            <td align="left"></td>
        </tr>
        <tr class="odd">
            <td align="left">gkey</td>
            <td align="left">It is from "tenforeCode" and "tenforeTicker" joinning with a colon,such as "126:IBM".</td>
        </tr>
        <tr class="even">
            <td align="left">index</td>
            <td align="left">The index of ticker in cache array</td>
        </tr>
        <tr class="odd">
            <td align="left">isReady</td>
            <td align="left">A boolean value that indicats whether the ticker object has been ready.</td>
        </tr>
        <tr class="even">
            <td align="left">valid</td>
            <td align="left">A booean value that indicats whether the ticker object is available.</td>
        </tr>
        <tr class="odd">
            <td align="left">optiontype</td>
            <td align="left">It is only for Options("C": Call; "P":Pull)</td>
        </tr>
        <tr class="even">
            <td align="left">expirydate</td>
            <td align="left">It is only for Options</td>
        </tr>
        <tr class="odd">
            <td align="left">strikeprice</td>
            <td align="left">It is only for Options</td>
        </tr>
    </tbody>
</table>
 
### DataManager.subscribePull(component, tickerObjects)
It is used to subcribe the tickers to data module to get the real time data points.

> component: An object. It must have a unique attribute "subscribeID" and an interface "updateData".

> E.g.
> 
  	var component = {
		subscribeID: 'xxxx_1',
		updateData: function(tickerObject, dataFields){
		}
	} 

> tickerObjects: An array of the ticker object that built with TickerFactory.create.


### DataManager.unSubscribePull(component, tickerObjects)
It is used to unsubcribe the tickers out of data manager.

> component: An object. It must have a unique attribute "subscribeID" and an interface "updateData".

> E.g.
> 
  	var component = {
		subscribeID: 'xxxx_1',
		updateData: function(tickerObject, dataFields){
		}
	} 

> tickerObjects: An array of the ticker object that built with TickerFactory.create.

### DataManager.deregisterComponent(component)
It is used to unsubscribe all component related ticker objects at one time.

> component: An object. It must have a unique attribute "subscribeID" and an interface "updateData".
> 
> E.g.
> 
  	var component = {
		subscribeID: 'xxxx_1',
		updateData: function(tickerObject, dataFields){
		}
	} 

### DataManager.getQuoteData(component, tickerObjects, callback, dataPoints)
It is used to get the realtime data points.

> component: An object. It must have a unique attribute "subscribeID" and an interface "updateData".
> 
> E.g.
> 
  	var component = {
		subscribeID: 'xxxx_1',
		updateData: function(tickerObject, dataFields){
		}
	} 

> tickerObjects: An array of the ticker object that built with TickerFactory.create.

> callback: A function with the data of the request tickers
> dataPoints: A specified points of quote data,split by comma,such as "LastPrice,TradeDate,...". Below table will show all valid data points.
<table cellpadding=0 cellspacing=0 border="1">
    <thead>
        <tr class="header">
            <th align="left">DataPointId</th>
            <th align="left">DataPointName</th>
        </tr>
    </thead>
    <tbody>
        <tr class="odd">
            <td align="left">TradeDate</td>
            <td align="left">Trade date</td>
        </tr>
        <tr class="even">
            <td align="left">TradeTime</td>
            <td align="left">Trade time</td>
        </tr>
        <tr class="odd">
            <td align="left">LastPrice</td>
            <td align="left">Last trade price</td>
        </tr>
        <tr class="even">
            <td align="left">Chg</td>
            <td align="left">Change of price</td>
        </tr>
        <tr class="odd">
            <td align="left">Chg%</td>
            <td align="left">Percentage change of price</td>
        </tr>
        <tr class="even">
            <td align="left">LastVolume</td>
            <td align="left">Last trade volume</td>
        </tr>
        <tr class="odd">
            <td align="left">LastMarket</td>
            <td align="left">Last trade market</td>
        </tr>
        <tr class="even">
            <td align="left">OpenPrice</td>
            <td align="left">Open price</td>
        </tr>
        <tr class="odd">
            <td align="left">HighPrice</td>
            <td align="left">High price</td>
        </tr>
        <tr class="even">
            <td align="left">LowPrice</td>
            <td align="left">Low price</td>
        </tr>
        <tr class="odd">
            <td align="left">ClosePrice</td>
            <td align="left">Close price</td>
        </tr>
        <tr class="even">
            <td align="left">MiddlePrice</td>
            <td align="left">Middle price</td>
        </tr>
        <tr class="odd">
            <td align="left">AskMarket</td>
            <td align="left">Ask market</td>
        </tr>
        <tr class="even">
            <td align="left">AskPrice</td>
            <td align="left">Ask price</td>
        </tr>
        <tr class="odd">
            <td align="left">AskSize</td>
            <td align="left">Ask size</td>
        </tr>
        <tr class="even">
            <td align="left">BidMarket</td>
            <td align="left">Bid Market</td>
        </tr>
        <tr class="odd">
            <td align="left">BidPrice</td>
            <td align="left">Bid price</td>
        </tr>
        <tr class="even">
            <td align="left">BidSize</td>
            <td align="left">Bid size</td>
        </tr>
        <tr class="odd">
            <td align="left">VWAP</td>
            <td align="left">Volume-weighted average price</td>
        </tr>
        <tr class="even">
            <td align="left">MarketCentre</td>
            <td align="left">Market Centre</td>
        </tr>
        <tr class="odd">
            <td align="left">OpenInt</td>
            <td align="left">Open Int.</td>
        </tr>
        <tr class="even">
            <td align="left">indicator</td>
            <td align="left">Trade indicator</td>
        </tr>
        <tr class="odd">
            <td align="left">Date</td>
            <td align="left">Date</td>
        </tr>
        <tr class="even">
            <td align="left">Time</td>
            <td align="left">Time</td>
        </tr>
        <tr class="odd">
            <td align="left">Volume</td>
            <td align="left">Volume</td>
        </tr>
    </tbody>
</table>


### DataManager.getStaticData(component, tickerObjects, callback)
It is used to get the realtime data points.

> component: An object. It must have a unique attribute "subscribeID" and an interface "updateData".
> 
> E.g.
> 
  	var component = {
		subscribeID: 'xxxx_1',
		updateData: function(tickerObject, dataFields){
		}
	} 

> tickerObjects: An array of the ticker object that built with TickerFactory.create.

> callback: A function with the static data of the request tickers. Below table will show all valid data points.
<table cellpadding=0 cellspacing=0 border="1">
<thead>
    <tr class="header">
        <th align="left">DataPointId</th>
        <th align="left">DataPointName</th>
    </tr>
</thead>
<tbody>
    <tr class="odd">
        <td align="left">la03z</td>
        <td align="left">Sector</td>
    </tr>
    <tr class="even">
        <td align="left">pd00b</td>
        <td align="left">Total Ret YTD (Daily)</td>
    </tr>
    <tr class="odd">
        <td align="left">pd00d</td>
        <td align="left">Total Ret 1 Yr (Daily)</td>
    </tr>
    <tr class="even">
        <td align="left">pd00f</td>
        <td align="left">Total Ret Annlzd 3 Yr (Daily)</td>
    </tr>
    <tr class="odd">
        <td align="left">pd00h</td>
        <td align="left">Total Ret Annlzd 5 Yr (Daily)</td>
    </tr>
    <tr class="even">
        <td align="left">pd003</td>
        <td align="left">Total Ret 1 Day (Daily)</td>
    </tr>
    <tr class="odd">
        <td align="left">pd005</td>
        <td align="left">Total Ret 1 Wk (Daily)</td>
    </tr>
    <tr class="even">
        <td align="left">pd007</td>
        <td align="left">Total Ret 1 Mo (Daily)</td>
    </tr>
    <tr class="odd">
        <td align="left">pd009</td>
        <td align="left">Total Ret 3 Mo (Daily)</td>
    </tr>
    <tr class="even">
        <td align="left">pd014</td>
        <td align="left">Total Ret Annlzd 2 Yr (Daily)</td>
    </tr>
    <tr class="odd">
        <td align="left">pm032</td>
        <td align="left">12 Mo Yield</td>
    </tr>
    <tr class="even">
        <td align="left">pm493</td>
        <td align="left">Total Ret 6 Mo (Daily)</td>
    </tr>
    <tr class="odd">
        <td align="left">pm494</td>
        <td align="left">Total Ret Annlzd 10 Yr (Daily)</td>
    </tr>
    <tr class="even">
        <td align="left">pm495</td>
        <td align="left">Total Ret Annlzd 15 Yr (Daily)</td>
    </tr>
    <tr class="odd">
        <td align="left">st106</td>
        <td align="left">Date: Price 52 Wk Low</td>
    </tr>
    <tr class="even">
        <td align="left">st109</td>
        <td align="left">Date: Price 52 Wk High</td>
    </tr>
    <tr class="odd">
        <td align="left">st153</td>
        <td align="left">Industry</td>
    </tr>
    <tr class="even">
        <td align="left">st159</td>
        <td align="left">Market Cap (mil) (Daily)</td>
    </tr>
    <tr class="odd">
        <td align="left">st168</td>
        <td align="left">Price 52 Wk High</td>
    </tr>
    <tr class="even">
        <td align="left">st169</td>
        <td align="left">Price 52 Wk Low</td>
    </tr>
    <tr class="odd">
        <td align="left">st181</td>
        <td align="left">Economic Moat</td>
    </tr>
    <tr class="even">
        <td align="left">st198</td>
        <td align="left">P/E Ratio Forward</td>
    </tr>
    <tr class="odd">
        <td align="left">st200</td>
        <td align="left">Morningstar Rating</td>
    </tr>
    <tr class="even">
        <td align="left">st201</td>
        <td align="left">Fair Value Uncertainty</td>
    </tr>
    <tr class="odd">
        <td align="left">st202</td>
        <td align="left">Morningstar Fair Value per Share</td>
    </tr>
    <tr class="even">
        <td align="left">st206</td>
        <td align="left">Stewardship Grade</td>
    </tr>
    <tr class="odd">
        <td align="left">st263</td>
        <td align="left">EPS TTM</td>
    </tr>
    <tr class="even">
        <td align="left">st408</td>
        <td align="left">P/B Ratio Current</td>
    </tr>
    <tr class="odd">
        <td align="left">st415</td>
        <td align="left">P/S Ratio Current</td>
    </tr>
    <tr class="even">
        <td align="left">st816</td>
        <td align="left">Security Type</td>
    </tr>
    <tr class="odd">
        <td align="left">sta0f</td>
        <td align="left">Avg Volume (in Mil) 2 Wk (Daily)</td>
    </tr>
    <tr class="even">
        <td align="left">sta4j</td>
        <td align="left">S&amp;P Industry</td>
    </tr>
    <tr class="odd">
        <td align="left">sta4l</td>
        <td align="left">S&amp;P Industry Group</td>
    </tr>
    <tr class="even">
        <td align="left">sta4n</td>
        <td align="left">S&amp;P Sector</td>
    </tr>
    <tr class="odd">
        <td align="left">sta65</td>
        <td align="left">Forward Dividend Yield %</td>
    </tr>
    <tr class="even">
        <td align="left">of009</td>
        <td align="left">Fund Size</td>
    </tr>
    <tr class="odd">
        <td align="left">of028</td>
        <td align="left">Morningstar Category</td>
    </tr>
    <tr class="even">
        <td align="left">os378</td>
        <td align="left">Fiscal Year-End Month</td>
    </tr>
    <tr class="odd">
        <td align="left">os388</td>
        <td align="left">Minimum Investment</td>
    </tr>
    <tr class="even">
        <td align="left">os00m</td>
        <td align="left">Annual Report Net Expense Ratio</td>
    </tr>
    <tr class="odd">
        <td align="left">os060</td>
        <td align="left">NAV (Daily)</td>
    </tr>
    <tr class="even">
        <td align="left">os03s</td>
        <td align="left">Turnover Ratio %</td>
    </tr>
    <tr class="odd">
        <td align="left">os089</td>
        <td align="left">Latest Dividend</td>
    </tr>
    <tr class="even">
        <td align="left">os463</td>
        <td align="left">Max Front Load</td>
    </tr>
    <tr class="odd">
        <td align="left">aa0a5</td>
        <td align="left">Company Name</td>
    </tr>
    <tr class="even">
        <td align="left">hs001</td>
        <td align="left">Growth Grade - Monthly</td>
    </tr>
    <tr class="odd">
        <td align="left">hs05a</td>
        <td align="left">Equity Style Box (Long)</td>
    </tr>
    <tr class="even">
        <td align="left">hs00l</td>
        <td align="left">Fixed Inc Style Box (Long)</td>
    </tr>
</tbody>
</table>

