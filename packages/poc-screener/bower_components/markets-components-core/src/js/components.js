define(['morningstar', './util', './container', './eventsFactory'], function(morningstar, util, container, eventsFactory) {
    'use strict';
    var log = util.log;
    var QSAPI = morningstar.components[util.getComponentName('core')];
    var $ = QSAPI.$;
    var QUIKR_VERSION = '@markets-components-quikr-version@';

    function _call(deferred, fun) {
        switch (deferred.state()) {
            case 'pending':
                deferred.done(fun);
                break;
            case 'resolved':
                fun();
                break;
            case 'rejected':
                log.info("Authenciation is failed.");
                break;
        }
    }
    /**
     * add for page-buidler
     */
    function migrateConfiguration(initialConfiguration) {
        if (!$.isEmptyObject(initialConfiguration.component.configuration)) {
            return initialConfiguration;
        }
        var _needMigrateConfiguration = {};
        var noNeedMigratePropertyArray = ['instid', 'sessionKey', 'qsPath', 'qsPullPath', 'component'];
        $.each(initialConfiguration, function(value) {
            if ($.inArray(value, noNeedMigratePropertyArray) < 0) {
                _needMigrateConfiguration[value] = initialConfiguration[value];
            }
        });
        initialConfiguration.component.configuration = _needMigrateConfiguration;
        return initialConfiguration;
    }

    function trackQuikrComponentUsage(needTrack, trackInfo) {
        if (!QSAPI.getTrackActionValue()) {
            return;
        }
        if (QSAPI.trackComponentUsage && needTrack) {
            QSAPI.trackComponentUsage(trackInfo);
        }
    }

    function init(componentType, component, element, initialConfiguration, actionHandler) {
        initialConfiguration.component = initialConfiguration.component || {};
        initialConfiguration.component.configuration = initialConfiguration.component.configuration || {};
        initialConfiguration.component.configuration.quikrVersion = QUIKR_VERSION;
        initialConfiguration = migrateConfiguration(initialConfiguration);

        // needTrack mark if the independent qukir component or not.
        // needTrackOnConfiguration mark sub component doesn't send track request.
        var needTrackOnConfiguration = initialConfiguration.component.configuration.needTrack;
        var needTrack = !!component.properties.needTrack;
        if (typeof needTrackOnConfiguration !== 'undefined' && !needTrackOnConfiguration) {
            needTrack = false;
        }
        var trackInfo = $.extend({
            needTrack: needTrack,
            componentType: componentType,
            quikrVersion: QUIKR_VERSION
        }, initialConfiguration.component.configuration.trackInfo);
        trackQuikrComponentUsage(needTrack, trackInfo);

        return component.init($(element), initialConfiguration.component || {}, actionHandler);
    }

    function extendInitConfig(initialConfiguration) {
        initialConfiguration.component = initialConfiguration.component || {};
        initialConfiguration.component.configuration = initialConfiguration.component.configuration || {};
        $.extend(initialConfiguration, {
            instid: morningstar.asterix.config.getOption('instid'),
            env: morningstar.asterix.config.getOption('env'),
            sessionKey: morningstar.asterix.context.getToken('marketsApiToken'),
            accessToken: morningstar.asterix.context.getToken('apiGatewayToken')
        });
    }

    /**
     * @function apiTokenExpiredCallback from 
     * 1.callbacks parameter(Quikir init way),
     * 2.morningstar.asterix.context.getApiTokenExpiredCallback() (ec-loader)
     */
    function getApiTokenExpiredCallback(initConfig) {
        if (initConfig.component.callbacks.apiTokenExpiredCallback &&
            typeof initConfig.component.callbacks.apiTokenExpiredCallback === 'function') {

            return initConfig.component.callbacks.apiTokenExpiredCallback;
        } else if (morningstar.asterix.context.getApiTokenExpiredCallback &&
            typeof morningstar.asterix.context.getApiTokenExpiredCallback() === 'function') {

            return transformSDKToApiTokenFromLoader;
        } else {
            return null;
        }
    }

    function transformSDKToApiTokenFromLoader(options, handleApiTokenExpiredCallback) {
        var apiTokenExpiredCallbackFromLoader = morningstar.asterix.context.getApiTokenExpiredCallback();

        // transform 
        // accessToken => apiGatewayToken from sdk to ec-loader,
        // sessionKey => marketsApiToken from sdk to ec-loader
        if (options['token'].name === 'accessToken') {
            options['token'].name = 'apiGatewayToken';
        } else if (options['token'].name === 'sessionKey') {
            options['token'].name = 'marketsApiToken';
        }

        apiTokenExpiredCallbackFromLoader(options, function(tokenName, tokenValue) {
            // transform 
            // apiGatewayToken => accessToken from ec-loader to sdk,
            // marketsApiToken => sessionKey from ec-loader to sdk,
            if (tokenName === 'apiGatewayToken') {
                tokenName = 'accessToken';
            } else if (tokenName === 'marketsApiToken') {
                tokenName = 'sessionKey';
            }
            handleApiTokenExpiredCallback(tokenName, tokenValue);
        });
    }

    function executeCallbacks(config, func, args) {
        if (config.component && config.component.callbacks && $.isFunction(config.component.callbacks[func])) {
            config.component.callbacks[func](args);
        }
    }

    function registerCallbacks(component, callbacks, properties) {
        var eventList = properties.events;
        var cbs = $.extend(true, {}, callbacks);
        if ($.isFunction(component.on)) {
            for (var event in eventList) {
                var callbackName = eventList[event];
                if (cbs && cbs[callbackName]) {
                    component.on(event, cbs[callbackName]);
                }
            }
        }
        replaceCallbacksToEvents(component, callbacks, properties);
        return component;
    }

    function replaceCallbacksToEvents(component, callbacks, properties) {
        var eventsList = properties.events;
        var callbackName;
        for (var event in eventsList) {
            callbackName = eventsList[event];
            callbacks[callbackName] = (function(event) {
                return function(data) {
                    component.trigger(event, data);
                }
            })(event);
        }
    }

    function setCallbacks(callbacks, eventList, _toBeTrigerdEvents) {
        for (var event in eventList) {
            var callbackName = eventList[event];
            if (!callbacks[callbackName]) {
                callbacks[callbackName] = (function(event) {
                    return function(data) {
                        _toBeTrigerdEvents[event] = {
                            eventName: event,
                            args: data
                        };
                    }
                })(event);
            }
        }
    }

    function _createComponent(componentType, containerInstance, params, component) {
        var deferred = $.Deferred(),
            _component,
            _properties = $.extend({
                setter: {},
                getter: {},
                events: {}
            }, component.properties),
            _toBeTrigerdEvents = {},
            _initConfig = $.extend(true, {}, params.initialConfiguration),
            sessionKey = '',
            accessToken = '',
            apiTokenExpiredCallback = null,
            actionHandler = params.actionHandler;

        extendInitConfig(_initConfig);
        sessionKey = _initConfig.sessionKey;
        accessToken = sessionKey ? '' : _initConfig.accessToken; // if has sessionKey, do not use accessToken
        _initConfig.component.callbacks = _initConfig.component.callbacks || {};
        setCallbacks(_initConfig.component.callbacks, _properties.events, _toBeTrigerdEvents);
        apiTokenExpiredCallback = getApiTokenExpiredCallback(_initConfig);
        _component = eventsFactory();
        _component = registerCallbacks(_component, _initConfig.component.callbacks, _properties);
        if (component.authorized || _initConfig.authorized) {
            var inst = init(componentType, component, params.element, _initConfig);
            _component = $.extend(true, inst, _component);
            deferred.resolve();
        } else {
            if (sessionKey && sessionKey !== "" || accessToken) {
                QSAPI.init(_initConfig.instid, {
                    env: _initConfig.env,
                    sessionKey: _initConfig.sessionKey,
                    qsPath: _initConfig.qsPath,
                    qsPullPath: _initConfig.qsPullPath,
                    accessToken: accessToken,
                    productType: _initConfig.component.configuration.productType || 'quikr',
                    apiTokenExpiredCallback: apiTokenExpiredCallback,
                    fod: _initConfig.fod
                }, function(result) {
                    if (result.errorCode == 0) {
                        _initConfig.component.configuration = $.extend(true, _initConfig.component.configuration, QSAPI.getExternalToken());
                        var inst = init(componentType, component, params.element, _initConfig, actionHandler);
                        _component = $.extend(true, inst, _component);
                        deferred.resolve();
                    } else {
                        executeCallbacks(_initConfig, "onError", result);
                        deferred.reject();
                        log.error("Session id is invalid!");

                    }
                });
            } else {
                var result = {
                    errorCode: "-9",
                    errorMsg: "Session id is invalid."
                };
                executeCallbacks(_initConfig, "onError", result);
                deferred.reject();
                log.error("Session id is invalid!");
            }
        }
        return {
            setProperty: function(name, value) {
                value = util.clone(value);
                _call(deferred, function() {
                    if (_component.setProperty) {
                        _component.setProperty(name, value);
                    } else {
                        var methodName = _properties.setter[name],
                            method = methodName && _component[methodName] ? _component[methodName] : _component[name];
                        if ($.isFunction(method)) {
                            if (name === 'parentSize') {
                                if (value.width > 0 || value.height > 0) {
                                    if (_initConfig.component && _initConfig.component.callbacks && $.isFunction(_initConfig.component.callbacks.onResize)) {
                                        _initConfig.component.callbacks.onResize(value.width, value.height);
                                    }
                                    method.apply(_component, [value.width, value.height]);
                                } else {
                                    log.debug('invalid width:' + value.width + ' and height ' + value.height + ' for component');
                                }
                            } else {
                                method.apply(_component, [value]);
                            }
                        } else {
                            log.debug('invalid property:' + name + ' for component');
                        }
                    }
                });
            },
            getProperty: function(name) {
                var ret = {};
                if (_component) {
                    if (_component.getProperty) {
                        ret = _component.getProperty(name);
                    } else {
                        var methodName = _properties.getter[name];
                        if (methodName && $.isFunction(_component[methodName])) {
                            ret = _component[methodName]();
                        } else if ($.isFunction(_component[name])) {
                            ret = _component[name]();
                        } else {
                            log.debug('invalid property:' + name + ' for component');
                        }
                    }
                } else {
                    log.debug('Component is not initialized');
                }
                return ret;
            },
            setParameter: function(name, value) {
                value = util.clone(value);
                this.setProperty(name, value);
            },
            getParameter: function(name) {
                this.getProperty(name);
            },
            on: function(eventName, callback) {
                _component.toBeTrigerdEvents = _toBeTrigerdEvents;
                _component.on(eventName, callback);
            },
            off: function(eventName, callback) {
                _component.off(eventName, callback);
                if (_toBeTrigerdEvents[eventName]) {
                    delete _toBeTrigerdEvents[eventName];
                }
            },
            trigger: function(eventName, args) {
                _component.trigger(eventName, args);
            },
            destroy: function() {
                containerInstance.off();
                _call(deferred, function() {
                    var method = _component.destroy || _component.clear;
                    if ($.isFunction(method)) {
                        method.apply(_component, []);
                    } else {
                        log.debug('missing destroy method for component');
                    }
                });
            }
        };
    }

    function register(name, component) {
        var componentName = util.getComponentName(name);
        morningstar.asterix.register(componentName);
        var _component = morningstar.components[componentName];
        _component.name = componentName;
        _component.__version__ = QUIKR_VERSION;
        _component.createComponent = function(params) {
            var containerInstance = container($(params.element));
            var instance = _createComponent(name, containerInstance, params, component);
            containerInstance.on(containerInstance.events.resize, function(data) {
                instance.setProperty('parentSize', {
                    width: data.width,
                    height: data.height
                });
            });
            containerInstance.on(containerInstance.events.empty, function() {
                instance.destroy();
            });
            return instance;
        };
        return _component;
    }

    return {
        register: register
    }
});