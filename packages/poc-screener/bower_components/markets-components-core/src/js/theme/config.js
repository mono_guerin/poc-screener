(function(root, factory){
    if(typeof exports === 'object' && typeof module === 'object'){
        module.exports = factory();
    } else if (typeof define === 'function' && define.amd){
        define("QSAPI", [], factory());
    } else {
        return factory();
    }
})(this, function(){

    var themeInfo = {
        "1": {
            "client": "morn",
            "name": "1",
            "webpackIndex": "./src/js/theme/theme1.js",
            "breakPoint": [600, 800, 1000]
        },
        "2": {
            "client": "quest",
            "name": "2",
            "webpackIndex": "./src/js/theme/theme2.js",
            "breakPoint": [600, 800, 1000]
        },
        "3": {
            "client": "msn",
            "name": "3",
            "webpackIndex": "./src/js/theme/theme3.js",
            "breakPoint": [600, 800, 1000]
        },
        "4": {
            "client": "theme4",
            "name": "4",
            "webpackIndex": "./src/js/theme/theme4.js",
            "breakPoint": [380, 600, 800, 1000]
        },
        "5": {
            "client": "fide",
            "name": "5",
            "webpackIndex": "./src/js/theme/theme5.js",
            "breakPoint": [600, 800, 1000]
        },
        "6": {
            "client": "pats",
            "name": "6",
            "webpackIndex": "./src/js/theme/theme6.js",
            "breakPoint": [600, 800, 1000]
        }
    };
    
    return themeInfo;
});


    

    