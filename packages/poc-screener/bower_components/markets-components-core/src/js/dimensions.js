define(['morningstar', './util'], function(morningstar, util) {
    'use strict';
    var NAMESPACE = 'morningstar-markets',
        RESIZE_EVENT = NAMESPACE + '-resize',
        RESIZE_EVENT_DATA = RESIZE_EVENT + '-data',
        $ = morningstar.components[util.getComponentName('core')].$,
        timeout_id,
        $elements = $([]);

    $.event.special[RESIZE_EVENT] = {
        setup: function() {
            var $element = $(this);
            $elements = $elements.add($element);
            $element.data(RESIZE_EVENT_DATA, {
                width: $element.width(),
                height: $element.height()
            });
            if ($elements.length === 1) {
                loopy();
            }
        },
        teardown: function() {
            var $element = $(this);
            $elements = $elements.not($element);
            $element.removeData(RESIZE_EVENT_DATA);
            if (!$elements.length) {
                clearTimeout(timeout_id);
            }
        }
    };

    function loopy() {
        timeout_id = window.setTimeout(function() {
            $elements.each(function() {
                var $element = $(this),
                    width = $element.width(),
                    height = $element.height(),
                    data = $element.data(RESIZE_EVENT_DATA) || {};
                if (data.width !== width || data.height !== height) {
                    $element.data(RESIZE_EVENT_DATA, {
                        width: width,
                        height: height
                    })
                    $element.trigger(RESIZE_EVENT, [width, height]);
                }
            });
            loopy();
        }, 100);
    }

    return {
        onResize: function($element, fn) {
            if ($.isWindow($element) || $element.is('body')) {
                $(window).on('resize.' + NAMESPACE, function(e) {
                    fn(document.documentElement.clientWidth, document.documentElement.clientHeight);
                });
            } else {
                $element.on(RESIZE_EVENT, function(e, width, height) {
                    e.stopPropagation();
                    fn(width, height);
                });
            }
        },
        offResize: function($element) {
            if ($.isWindow($element) || $element.is('body')) {
                $(window).off('.' + NAMESPACE);
            } else {
                $element.off(RESIZE_EVENT);
            }

        }
    };
});