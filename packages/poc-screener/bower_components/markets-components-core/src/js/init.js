define(['./core', './status', './ajax', './util', './data/datamanager'], function (QSAPI) {
	'use strict';
	var $ = QSAPI.$;
	var Util = QSAPI.Util;

	var _instid;
	var _CSRFToken;
	var _cfg = {
		env : 'production',
		autoStopPull : true,
		autoRefresh : true
	};
	var _uiCfg = {};
	var _globalStatus = QSAPI.Util.getGlobalStatus();
	var _initCallbackQueue = [];
	var _triggerInitCallbackQueue = [];
	var _triggerInitCallbacks = function (result) {
		var callback;
		while (_triggerInitCallbackQueue.length > 0) {
			callback = _triggerInitCallbackQueue.shift();
			if ($.isFunction(callback)) {
				callback(result);
			}
		}
	};

	var _auth = function (options) {
		var authURL = Util.formatString('{0}{1}?callback=?', options.qsPath, 'service/queryPermission');
		var data = {
			'instid' : options.instid,
			'fields' : '1,2,4,8,16,32,64,128,1024'
		};
		if (options.sessionKey) {
			data['sessionId'] = options.sessionKey;
		} else if (options.profileurl) {
			data['userProfile'] = options.profileurl;
		}
		return Util.ajax({
			'type' : 'GET',
			'url' : authURL,
			'cache' : false,
			'dataType' : 'json',
			'data' : data,
			'success' : options.onSuccess,
			'error' : options.onFailure
		});
	};

	var _getPathConfigByEnv = function (env) {
		var _qsPath,
		_qsPullPath,
		_qsAlertPath;
		if (env === 'dev') {
			_qsPath = _qsAlertPath = '//rtqdev.morningstar.com/opt/';
			_qsPullPath = '//rtqdev.morningstar.com/pullservice/pull/';
		} else if (env === 'pre-stage' || env === 'prestage') {
			_qsPath = _qsAlertPath = '//markets-uat.morningstar.com/prestage/';
			_qsPullPath = '//markets-uat.morningstar.com/prestgPullservice/pull/';
		} else if (env === 'stage') {
			_qsPath = _qsAlertPath = '//markets-uat.morningstar.com/';
			_qsPullPath = '//markets-uat.morningstar.com/pullservice/pull/';
		} else if (env === 'pre-release' || env === 'prerelease') {
			_qsPath = _qsAlertPath = '//qspre.morningstar.com/';
			_qsPullPath = '//qspre.morningstar.com/pullservice/pull/';
		} else {
			_qsPath = '//quotespeed.morningstar.com/';
			_qsPullPath = '//pullqs.morningstar.com/pullservice/pull/';
			_qsAlertPath = '//qsalert.morningstar.com/';
		}
		return {
			qsPath : _qsPath,
			qsPullPath : _qsPullPath,
			qsAlertPath : _qsAlertPath
		};
	};

	var _initDataManager = function (o) {
		QSAPI.DataManager.init({
			qsPath : _cfg.qsPath,
			qsPullPath : _cfg.qsPullPath,
			dataPermission : o.enablement || [],
			exchangeMap : o.shortName || {},
			timezoneMap : o.ExchangeTimezone || {},
			MICExangeMap : o.micCode || {},
			exchangeMarketTimeMap : o.marketTime || {},
			pullSubSeparator : o.pullSeparator || '_',
			pushSubSeparator : o.pushSeparator || '|',
			frequency : _cfg.pullFrequency,
			autoStopPull : _cfg.autoStopPull
		});
	};

	$.extend(QSAPI, {
		init : function (instid, cfg, callback) {
			_instid = instid;
			var pathCfg = _getPathConfigByEnv(cfg.env || _cfg.env);
			if (cfg) {
				$.extend(true, _cfg, pathCfg, cfg);
			}
			_initCallbackQueue.push(callback);
			_triggerInitCallbackQueue.push(callback);

			if (QSAPI.ready) {
				_triggerInitCallbacks(QSAPI.readyResponse);
			} else {
				if (QSAPI.authRequest) {
					return;
				}
				if (_instid) {
					if (!_cfg.sessionKey && !_cfg.profileurl) {
						return;
					}

					var authOptions = {
						instid : _instid,
						qsPath : _cfg.qsPath,
						sessionKey : _cfg.sessionKey,
						profileurl : _cfg.profileurl,
						onSuccess : function (result, textStatus) {
							QSAPI.ready = true;
							if (result && result.status && result.status.errorCode == '0') {
								var o = result.attachment || {};
								_cfg.sessionKey = o.SessionID;
								_CSRFToken = o.TempID;
								_initDataManager(o);
								QSAPI.readyResponse = _globalStatus.AUTHOK;
							} else {
								QSAPI.readyResponse = _globalStatus.AUTHERR;
							}
							_triggerInitCallbacks(QSAPI.readyResponse);
						},
						onFailure : function (XMLHttpRequest, textStatus, errorThrown) {
							QSAPI.readyResponse = Util.errorHandler(XMLHttpRequest, textStatus);
							_triggerInitCallbacks(QSAPI.readyResponse);
						}
					};
					QSAPI.authRequest = _auth(authOptions);

				} else {
					_triggerInitCallbacks(_globalStatus.INITERR);
				}
			}
		},
		setCfg : function (config) {
			config = config || {};
			if (!_cfg.sessionKey) {
				_cfg.sessionKey = config.sessionKey;
			}
			if (!_cfg.session_sessionKey) {
				_cfg.session_sessionKey = config.session_sessionKey;
			}
			QSAPI.ready = true;
			_initDataManager(config);
		},
		setUICfg : function (cfg) {
			$.extend(true, _uiCfg, cfg);
		},
		getUICfg : function (key) {
			return _uiCfg[key] || {};
		},
		getVersion : function () {
			return '@version';
		},
		getQSVersion : function () {
			return _cfg.env;
		},
		getQSPath : function () {
			return _cfg.qsPath;
		},
		getQSPullPath : function () {
			return _cfg.qsPullPath;
		},
		getQSAlertPath : function () {
			return _cfg.qsAlertPath;
		},
		getSessionId : function (session) {
			session = session === true ? true : false;
			return !session ? _cfg.sessionKey : _cfg.session_sessionKey;
		},
		getCSRFToken : function () {
			return _CSRFToken;
		},
		getInstID : function () {
			return _instid;
		},
		getInitCallback : function () {
			return function (result) {
				var callback;
				while (_initCallbackQueue.length > 0) {
					callback = _initCallbackQueue.shift();
					if ($.isFunction(callback)) {
						callback(result);
					}
				}
			};
		},
		isAutoRefresh : function () {
			return _cfg.autoRefresh !== false;
		}
	});

	return QSAPI;
});
