define(['../core', '../status', './push'], function (QSAPI) {
	'use strict';
	var $ = QSAPI.$;

	var _cfg = {};
	var _connectToPush = function (pushDomain, callbacks, config) {
		QSAPI.Push.connect();
	};

	QSAPI.DataManager = {
		init : function (cfg) {
			this.setCfg(cfg);
			//console.log(cfg);

		},
		setCfg : function (cfg) {
			if (cfg) {
				$.extend(true, _cfg, cfg);
			}
		},
		connectToPush : function () {},
		disconnectPush : function () {},
		connectToPull : function () {},
		disconnectPull : function () {},
		connect : function () {},
		disconnect : function () {},
		subscribePush : function () {},
		subscribePull : function () {},
		subscribe : function () {},
		unSubscribe : function () {},
		deregisterComponent : function () {},
		getQuoteData : function () {},
		getStaticData : function () {},
		hasNews : function () {},

		checkConnect : function () {},
		subscribeMix : function () {},
		subscribeIndicatorData : function () {},
		unSubscribeIndicatorData : function () {},
		subscribe52WData : function () {},
		unSubscribe52WData : function () {},
		getSubscribeTicker : function () {}
	};

	return QSAPI;
});
