define(['morningstar', './util'], function(morningstar, util) {
    'use strict';
    var EVENT = 'morningstar-markets-container-event',
        EVENT_DATA = EVENT + '-data',
        EVENTS = {
            "resize": EVENT + "-resize",
            "empty": EVENT + "-empty"
        },
        $ = morningstar.components[util.getComponentName('core')].$,
        timeout_id,
        $elements = $([]);

    $.event.special[EVENT] = {
        setup: function() {
            var $element = $(this);
            $elements = $elements.add($element);
            $element.data(EVENT_DATA, {
                width: width($element),
                height: height($element),
                hasChild: false
            });
            if ($elements.length === 1) {
                loopy();
            }
        },
        teardown: function() {
            var $element = $(this);
            $elements = $elements.not($element);
            $element.removeData(EVENT_DATA);
            if (!$elements.length) {
                clearTimeout(timeout_id);
            }
        }
    };

    function loopy() {
        timeout_id = window.setTimeout(function() {
            $elements.each(function() {
                var $element = $(this),
                    w = width($element),
                    h = height($element),
                    hasChild = $element.children().length > 0,
                    data = $element.data(EVENT_DATA) || {};
                if (data.width !== w || data.height !== h) {
                    data.width = w;
                    data.height = h;
                    $element.data(EVENT_DATA, data);
                    $element.trigger(EVENT, {
                        event: EVENTS.resize,
                        width: w,
                        height: h
                    });
                }
                if (data.hasChild !== hasChild) {
                    if (data.hasChild === true && hasChild === false) {
                        $element.trigger(EVENT, {
                            event: EVENTS.empty
                        });
                    }
                    data.hasChild = hasChild;
                    $element.data(EVENT_DATA, data);
                }
            });
            loopy();
        }, 100);
    }

    function width($element) {
        var isWindow = $.isWindow($element) || $element.is('body');
        return isWindow ? document.documentElement.clientWidth : $element.width();
    }

    function height($element) {
        var isWindow = $.isWindow($element) || $element.is('body');
        return isWindow ? document.documentElement.clientHeight : $element.height();
    }

    function isValidEvent(event) {
        var flag = false;
        for (var key in EVENTS) {
            if (event === EVENTS[key]) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    return function($element) {
        var isWindow = $.isWindow($element) || $element.is('body');
        var registeredEvents = {};
        return {
            events: EVENTS,
            $element: $element,
            on: function(event, fn) {
                if (isValidEvent(event)) {
                    registeredEvents[event] = true;
                    $element.on(EVENT, function(e, data) {
                        e.stopPropagation();
                        if (data.event === event) {
                            fn(data);
                        }
                    });
                }
            },
            off: function(event) {
                if (!event) {
                    $element.off(EVENT);
                } else if (registeredEvents[event]) {
                    registeredEvents[event] = null;
                    delete registeredEvents[event];
                    if ($.isEmptyObject(registeredEvents)) {
                        $element.off(EVENT);
                    }
                }
            }
        };
    };
});