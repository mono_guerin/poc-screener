define(['morningstar', './util'], function(morningstar, util) {
    'use strict';
    var log = util.log;
    var QSAPI = morningstar.components[util.getComponentName('core')];
    var $ = QSAPI.$;

    return function(instance) {
        var instance = instance || {};
        instance.callbackList = [];
        instance.on = function(eventName, callback) {
            instance.callbackList[eventName] = callback;
            if (this.toBeTrigerdEvents.hasOwnProperty(eventName)) {
                this.trigger(eventName, this.toBeTrigerdEvents[eventName].args);
                delete this.toBeTrigerdEvents[eventName];
            }
        };
        instance.off = function(eventName, callback) {
            if (!instance.callbackList.hasOwnProperty(eventName)) {
                return;
            }
            if (instance.callbackList.hasOwnProperty(eventName)) {
                delete instance.callbackList[eventName];
            }
        };
        instance.trigger = function(eventName, args) {
            if (args === undefined) {
                args = [];
            }
            if (instance.callbackList[eventName]) {
                var callback = instance.callbackList[eventName];
                callback.apply(this, [args]);
            } else {
                this.toBeTrigerdEvents[eventName] = {
                    eventName: eventName,
                    args: args
                }
            }
        };
        instance.toBeTrigerdEvents = {};
        return instance;
    };
});