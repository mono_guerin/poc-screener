define(['./core'], function (QSAPI) {
	'use strict';

	var GLOBALSTATE = {
		AUTHOK : {
			errorCode : '0',
			errorMsg : 'Authentication is successful.'
		},
		INITERR : {
			errorCode : '-1',
			errorMsg : 'Not initialize QSAPI.'
		},
		AUTHERR : {
			errorCode : '-2',
			errorMsg : 'Account is incorrect.'
		},
		//Above is for method QSAPI.init()
		SERUNAVAIL : {
			errorCode : '-3',
			errorMsg : 'Service temporarily unavailable.'
		},
		TIMEOUT : {
			errorCode : '-4',
			errorMsg : 'Request timeout.'
		},
		NOFIND : {
			errorCode : '-5',
			errorMsg : 'Service not found.'
		},
		SERERR : {
			errorCode : '-6',
			errorMsg : 'Service response error.'
		},
		AUTHEXPIRED : {
			errorCode : '-7',
			errorMsg : 'Authentication is expired.'
		},
		RETURNERR : {
			errorCode : '-8',
			errorMsg : 'Response is invalid.'
		},
		//Above is for all ajax request
		CONNOKPULL : {
			errorCode : '0',
			errorMsg : 'Connect pull server successfully.'
		},
		CONNERRPULL : {
			errorCode : '-2',
			errorMsg : 'Connect pull server unsuccessfully.'
		},
		//Above is for method QSAPI.DataManager.connectToPull()
		CONNOKPUSH : {
			errorCode : '0',
			errorMsg : 'Connect push server successfully.'
		},
		CONNERRPUSH : {
			errorCode : '-3',
			errorMsg : 'Connect push server unsuccessfully.'
		},
		//Above is for method QSAPI.DataManager.connectToPush()
		NOAUTH : {
			errorCode : '-1',
			errorMsg : 'No authentication.'
		},
		//Above is for method QSAPI.DataManager.connectToPush()/QSAPI.DataManager.connectToPull()
		SUBOK : {
			errorCode : '0',
			errorMsg : 'Subscribe successfully.'
		},
		SUBFAIL : {
			errorCode : '-4',
			errorMsg : 'Some tickers objects can not be subscribed.'
		},
		//Above is for method QSAPI.DataManager.subscribePush()/QSAPI.DataManager.subscribePull()
		UNSUBOK : {
			errorCode : '0',
			errorMsg : 'Unsubscribe successfully.'
		},
		UNSUBFAIL : {
			errorCode : '-4',
			errorMsg : 'Some tickers objects can not be unsubscribed.'
		},
		//Above is for method QSAPI.DataManager.unSubscribePull()/QSAPI.DataManager.unSubscribePush()
		CONNERR : {
			errorCode : '-5',
			errorMsg : 'Not Connect.'
		},
		NOSUBID : {
			errorCode : '-6',
			errorMsg : 'Empty subscribeID of this component.'
		}
	};

	QSAPI.Util.errorHandler = function (xhr, textStatus) {
		var data;
		if (xhr.status == "404") {
			data = GLOBALSTATE.NOFIND;
		} else if (xhr.status == "503") {
			data = GLOBALSTATE.SERUNAVAIL;
		} else if (textStatus == 'timeout') {
			data = GLOBALSTATE.TIMEOUT;
		} else {
			data = GLOBALSTATE.SERERR;
		}
		return data;
	};

	QSAPI.Util.getGlobalStatus = function () {
		return GLOBALSTATE;
	};

	return QSAPI;
});
