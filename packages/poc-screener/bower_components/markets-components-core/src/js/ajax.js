define(['./core', './status'], function (QSAPI) {
	'use strict';
	var $ = QSAPI.$;
	var _globalStatus = QSAPI.Util.getGlobalStatus();

	var _isGETJSON = function (config) {
		return config.type.toLowerCase() === 'get' && (config.dataType.toLowerCase() === 'json' || config.dataType.toLowerCase() === 'jsonp');
	};

	var _isSupportCORS = function () {
		if ('withCredentials' in new window.XMLHttpRequest()) {
			/* supports cross-domain requests */
			return true;
		} else {
			return false;
		}
	};

	var _isSameDomain = function (config) {
		if (config.url) {
			var rprotocol = /^\/\//;
			var rurl = /^([\w.+-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/;
			var ajaxLocation;
			try {
				ajaxLocation = location.href;
			} catch (e) {
				// Use the href attribute of an A element
				// since IE will modify it given document.location
				ajaxLocation = document.createElement("a");
				ajaxLocation.href = "";
				ajaxLocation = ajaxLocation.href;
			}

			// Segment location into parts
			var ajaxLocParts = rurl.exec(ajaxLocation.toLowerCase()) || [];
			var url = config.url.replace(rprotocol, ajaxLocParts[1] + "//");

			var parts = rurl.exec(url.toLowerCase());
			if (parts === null) {
				return true;
			} else {
				return parts[2] === ajaxLocParts[2];
			}
		}
		return false;
	};

	var _parse = function (url, data) {
		url = url.replace(/=\?(&|$)/g, "=jsonpcallbcak$1");
		var arr = url.split("?");
		url = arr[0];
		if (arr.length > 1) {
			if (typeof data == 'undefined') {
				data = {};
			}
			var paras = arr[1].split("&"),
			_temp;
			for (var i = 0, l = paras.length; i < l; i++) {
				_temp = paras[i].split("=");
				if (_temp.length === 2 && _temp[1] !== "jsonpcallbcak" && _temp[1] !== "" && typeof data[_temp[0]] === 'undefined') {
					data[_temp[0]] = _temp[1];
				}
			}
		}
		if (data.sessionId && data.qs_wsid) {
			data.qs_wsid = null;
			delete data.qs_wsid;
		}
		var _paras = [];
		if (typeof data === 'object' && $.isEmptyObject(data) === false) {
			for (var k in data) {
				if (k !== 'qs_wsid' && k !== 'sessionId' && k !== 'tmpid') {
					_paras.push(k + "=" + encodeURIComponent(data[k]));
					data[k] = null;
					delete data[k];
				}
			}
		}
		if (_paras.length > 0) {
			url += "?" + _paras.join("&");
		}
		return {
			url : url,
			data : data
		};
	};

	var _checkAuth = function (callbacks, result, textStatus, runComponentAuthCheckFirst) {
		var o = typeof result === 'string' ? eval("(" + result + ")") : result;
		var status = o && o.status ? o.status : {};
		if (status.errorCode == '-1' && status.subErrorCode == '10000') {
			if (runComponentAuthCheckFirst) {
				callbacks.onSuccess(result, textStatus);
			} else if ($.isFunction(callbacks.onFailure)) {
				callbacks.onFailure(_globalStatus.AUTHEXPIRED);
			}
		} else {
			if ($.isFunction(callbacks.onSuccess)) {
				callbacks.onSuccess(result, textStatus);
			}
		}
	};

	var Util = {};

	Util.ajax = function (url, config) {
		$.support.cors = true;
		if (!url.third) { //access 3th service
			if (typeof url === "object") {
				config = url;
				url = undefined;
			}
			var sessionId,
			session_sessionId,
			CSRFToken;
			if ($.isFunction(QSAPI.getSessionId)) {
				sessionId = QSAPI.getSessionId();
				session_sessionId = QSAPI.getSessionId(true);
			}
			if ($.isFunction(QSAPI.getCSRFToken)) {
				CSRFToken = QSAPI.getCSRFToken();
			}
			if ($.isFunction(QSAPI.getInstID == 'function')) {
				config.data = config.data || {};
				config.data.instid = QSAPI.getInstID();
			}
			if ($.isFunction(QSAPI.getVersion == 'function')) {
				config.data = config.data || {};
				config.data.sdkver = QSAPI.getVersion();
			}
			if (typeof sessionId !== 'undefined' && sessionId !== null && sessionId.length > 0) {
				config.data = config.data || {};
				config.data.qs_wsid = sessionId;
			} else {
				if (typeof session_sessionId !== 'undefined' &&
					session_sessionId !== null &&
					session_sessionId.length > 0 &&
					!_isSameDomain(config)) {
					config.data = config.data || {};
					config.data.qs_wsid = session_sessionId;
				}
			}
			if (typeof CSRFToken !== 'undefined' && CSRFToken !== null && CSRFToken.length > 0) {
				config.data = config.data || {};
				config.data.tmpid = CSRFToken;
			}
			var _oldSuccess = config.success;
			var ajaxSuccessProcess = function (o, textStatus) {
				_checkAuth({
					onSuccess : _oldSuccess,
					onFailure : QSAPI.getInitCallback()
				}, o, textStatus, config.runComponentAuthCheckFirst);
			};

			if (_isGETJSON(config) && _isSupportCORS()) {
				var _d = _parse(config.url, config.data);
				config.url = _d.url;
				config.data = _d.data;
				config.type = 'get';
				config.dataType = null;
				delete config.dataType;
				config.success = function (o, textStatus) {
					o = o || '';
					if (o !== '') {
						o = o.replace(/\r+\n+/g, '');
					}
					try {
						o = o === '' ? {} : eval("(" + o + ")");
					} catch (error) {
						o = '{}';
					}
					ajaxSuccessProcess(o, textStatus);
				};
			} else {
				config.success = ajaxSuccessProcess;
			}
			return $.ajax(config);
		} else {
			return $.ajax(url);
		}
	};

	$.each(['get', 'post'], function (i, method) {
		Util[method] = function (url, data, callback, type) {
			if (typeof data === 'function') {
				type = type || callback;
				callback = data;
				data = undefined;
			}
			return Util.ajax({
				type : method,
				url : url,
				data : data,
				success : callback,
				dataType : type
			});
		};
	});

	Util.getJSON = function (url, data, callback) {
		return Util.get(url, data, callback, 'json');
	};

	Util.getScript = function (url, callback) {
		return Util.get(url, undefined, callback, "script");
	};

	$.extend(QSAPI.Util, Util);

	return QSAPI;
});
