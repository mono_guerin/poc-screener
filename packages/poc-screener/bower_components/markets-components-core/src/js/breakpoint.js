define(function() {
    var breakpointConfig = {
        "1": {
            "client": "morn",
            "breakPoint": [600, 800, 1000]
        },
        "2": {
            "client": "quest",
            "breakPoint": [600, 800, 1000]
        },
        "3": {
            "client": "msn",
            "breakPoint": [600, 800, 1000]
        },
        "4": {
            "client": "theme4",
            "breakPoint": [380, 600, 800, 1000]
        },
        "5": {
            "client": "fide",
            "breakPoint": [600, 800, 1000]
        },
        "6": {
            "client": "pats",
            "breakPoint": [600, 800, 1000]
        }
    };
    return {
        getBreakpointConfig: function(theme){
            return breakpointConfig[theme];
        }
    }
});