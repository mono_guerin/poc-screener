define([], function() {
    require('es5-shim');
    require('jquery');
    require('asterix-core');
    require('../build/markets-components-core.js');
});
