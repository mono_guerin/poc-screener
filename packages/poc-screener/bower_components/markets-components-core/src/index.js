define(['morningstar', './js/breakpoint', './js/util'], function(morningstar, breakpoint, util) {
    'use strict';
    var marketscore = util.getComponentName('core');
    morningstar.asterix.register(marketscore);

    var QSAPI = morningstar.components[marketscore];
    require('./js/sdk_ms_qs_core.js')(window, QSAPI);

    var $ = QSAPI.$;
    $.extend(QSAPI.Util, util);
	QSAPI.getBreakpointConfig = breakpoint.getBreakpointConfig;
    QSAPI.components = require('./js/components.js');
    return QSAPI;
});