// startref
/// <reference path='../../.references.ts'/>
// endref
module EcButton {
  export enum _ButtonType { ecButton }

  /**
   * @title Button
   */
  export interface EcButton {
    /** Component type.
     * @default ecButton
     * @options.hidden true
     * @type string
     * @readonly true
     */

    type: _ButtonType;

    settings: Settings;

    // todo(slind): create label interface in ngcore/mstarcore
    labels: Labels;
  }
}
