// startref
/// <reference path='../../.references.ts'/>
// endref
module EcButton {

    export enum ButtonType {
        badge,
        button,
        link,
        reset,
        submit
    }


    export interface Settings extends AsterixCore.ComponentSettings {
        _templates: Templates;

        _model: {
            disabled: AsterixCore.DataSourceSettings;
        };

        _parameters: {
            badgeContent: AsterixNg.Parameter;
            buttonCSS: AsterixNg.Parameter;
            buttonType: AsterixNg.Parameter;
            captionLabelKey: AsterixNg.Parameter;
            disabled: AsterixNg.Parameter;
            iconCSS: AsterixNg.Parameter;
            iconPosition: AsterixNg.Parameter;
            href: AsterixNg.Parameter;
            hrefParams: AsterixNg.Parameter;
            onClick: AsterixNg.Parameter;
            visible: AsterixNg.Parameter;
        };

        /** The namespaceClass
         * @default ec-button
         * @type string
         */
        namespaceClass: string;

        /* This is used to set the content of badge in button.
         */
        badgeContent?: any

        /** This is used to set icons name available in svg sprite
         * @default person-padless
         */
        iconName?: string

        /** This is used to set the path icon of svg sprite
         * @default mds_icons
         */
        iconSprite?: string

        /**
         * @default ec-components-button
         */
        buttonCSS: string;

        /**
         * @type string
         * @default button
         */
        buttonType: ButtonType;

        /**
         * @default false
         */
        disabled: boolean;

        iconCSS: string;

        /**
         * @default left
         */
        iconPosition: string;

        /* Url for the link. Can be a templated url
        * @default "#"
        */
        href: string;

        /** show hide button component in different configuration.
         * @format boolean
         * @default true
         */
        visible: boolean

        /* target for the link. Possible values are "_blank", "_new"
        * @default "_blank"
        */
        target: string;

        /* Parameters for templated href url
        */
        hrefParams?: any;

        /* click event handler for this ec-button. Handler receives "event" object as an argument
        */
        onClick?: any;
    }

    export interface Templates {
        /** The badge button template.
         * @format html
         * @default <button title=\"{{ parameters.captionLabelKey ? labels.get(parameters.captionLabelKey) : buttonLabel }}\"\r\n    data-ng-if=\"parameters.visible\" data-ng-attr-type=\"{{parameters.buttonType}}\" data-ng-click=\"buttonClick($event)\"\r\n    class=\"{{ ::namespaceClass('__btn') + ' '+ namespaceClass('__badge-btn') }} {{parameters.buttonCSS}}\"\r\n    data-ng-disabled=\"parameters.disabled\">\r\n    <span data-ng-if=\"parameters.iconCSS\" class=\"{{ ::namespaceClass('__icon') }}\" data-ng-class=\"parameters.iconCSS\"><\/span>\r\n    <span class=\"{{ ::namespaceClass('__caption') }}\">{{ parameters.captionLabelKey ? labels.get(parameters.captionLabelKey) : buttonLabel }}<\/span>\r\n    <span class=\"{{ ::namespaceClass('__badge-content') }}\">{{parameters.badgeContent}}<\/span>\r\n<\/button>
         */
        badge: string;

         /** The icon only button template.
         * @format html
         * @default <button title=\"{{ parameters.captionLabelKey ? labels.get(parameters.captionLabelKey) : buttonLabel }}\"\r\n    data-ng-if=\"parameters.visible\" data-ng-attr-type=\"{{parameters.buttonType}}\" data-ng-click=\"buttonClick($event)\"\r\n    class=\"{{ ::namespaceClass('__btn') + ' ' + namespaceClass('--icon-only') }} {{parameters.buttonCSS}}\"\r\n    data-ng-disabled=\"parameters.disabled\" role=\"button\">\r\n    <svg class=\"mui-icon mui-button__icon\">\r\n        <use data-ng-attr-href=\"{{ ::settings.get('iconSprite') }}.svg#{{ ::settings.get('iconName') }}\"\/>\r\n    <\/svg>\r\n<\/button>
         */
        icon: string;

        /** The main template.
         * @format html
         * @default <button title=\"{{ parameters.captionLabelKey ? labels.get(parameters.captionLabelKey) : buttonLabel }}\"\r\n    data-ng-if=\"parameters.visible\" data-ng-attr-type=\"{{parameters.buttonType}}\"\r\n    data-ng-click=\"buttonClick($event)\" class=\"{{ ::namespaceClass('__btn') }} {{parameters.buttonCSS}}\" data-ng-disabled=\"parameters.disabled || model.disabled\">\r\n    <span data-ng-if=\"parameters.iconCSS\" class=\"{{namespaceClass('__icon')}}\" data-ng-class=\"parameters.iconCSS\"><\/span>\r\n    <span class=\"{{ ::namespaceClass('__caption') }}\">{{ parameters.captionLabelKey ? labels.get(parameters.captionLabelKey) : buttonLabel }}<\/span>\r\n<\/button>\r\n
         */
        main: string;

        /** The link button template.
         * @format html
         * @default <a title=\"{{ parameters.captionLabelKey ? labels.get(parameters.captionLabelKey) : buttonLabel }}\"\r\n    data-ng-if=\"parameters.visible\" href=\"{{href}}\" target=\"{{ ::settings.get('target') }}\"\r\n    data-ng-click=\"buttonClick($event)\" class=\"{{ ::namespaceClass('__link') }} {{parameters.buttonCSS}}\">\r\n    <span data-ng-if=\"parameters.iconCSS\" class=\"{{iconPositionClass}}\" data-ng-class=\"parameters.iconCSS\"><\/span>\r\n    {{ parameters.captionLabelKey ? labels.get(parameters.captionLabelKey) : buttonLabel }}\r\n<\/a>\r\n
         */
        link: string;
    }
}