# Button

## Introduction
ec-button is a component which renders simple button. 

### Usage

ecButton can be used as an Angular directive 

#### Example:
	<ec-button mstar-component-id="ecButtonInstance"></ec-button>

###Configurable Options

 1. caption (String)
 2. buttonType (String)
    - button
    - submit
    - reset
    - link
 3. disabled (Boolean)
 4. buttonCSS (String)
 5. iconCSS (String)
 6. iconPosition (String)
 7. onClick (Function)

###How to run
Assuming you have setup ec correctly. You can run the ecButton as follows:

	gulp previewComponent -c button

###How To configure
Add configuration of defined ecButtons in components section of js/_config.js. 

	morningstar.appConfig = morningstar.asterix.extend({
	    components: {
	      ecButtonInstance: {
	        type: "ecButton",
	        settings: {
	          buttonType: "submit",
	          disabled: false,
	          buttonCSS: "CSS Class Name",
	          iconCSS: "Icon CSS Class Name",
	          iconPosition: "left",
			  onClick: function(){}
	        },
	        labels: {
	          "caption": {
	            "en": "Submit"
	          }
	        }
	      })
