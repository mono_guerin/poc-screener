// startref
/// <reference path='../../.references.ts'/>
// endref
module AsterixCore {
    export interface ComponentSettings {
        /**
         * @default null
         * @enum ["areaChart", "assetAllocations", "charts", "fixedIncomeStyleBox", "lineChart", "pieChart", "stockStyleBox"]
         */
        colorPaletteName?: string;

        /**
         * @default null
         */
        colorPalettes?: any;

        /**
         * @default null
         */
        colors?: string[];

        /** The currency to use.
         * @title Currency
         * @type string
         * @default null
         * @enum ["","USD","EUR","GBP","INR","AUD","CAD","SGD","CHF","JPY","CNY","SEK"]
         */
        currencyId?: string;

        /**
         * @default null
         */
        fieldDefinitions?: FieldDefinitions;

        /**
         * @default null
         */
        format?: FormatSettings;

        /**
         * @default null
         */
        formatAliases?: FormatAliases;

        /**
         * @default null
         */
        freezeConfig: boolean;

        /** The language id to use.
         * @title Language Id
         * @default null
         * @enum ["","en","en-GB","sv-SE"]
         */
        languageId?: string;

        /**
        * @default null
        */
        useCalculatedWidth: boolean;

        /**
        * @default null
        */
        viewBreakpoints?: ViewBreakpoints;
    }

    export interface Label {
        'en': string;
        'en-GB': string;
        'sv-SE': string;
    }

    export interface FormatAliases {
        integer: NumberFormat;
        shortDecimal: NumberFormat;
        standardDecimal: NumberFormat;
        longDecimal: NumberFormat;
    }

    export interface FormatSettings {
        number: NumberFormat;
        date: DateFormat;

        /** The null string format.
         * @default –
         */
        nullString?: string;
    }

    export interface NumberFormat {
        /**
         * @default number
         */
        dataType: string;

        /**
         * @default decimal
         * @enum ["decimal","currency","percent"]
         */
        style: string;

        /** See http://www.currency-iso.org/dam/downloads/lists/list_one.xml for codes.
         * @default "GBP"
         */
        currency?: string;

        /**
         * @default symbol
         * @enum ["symbol","code","name"]
         */
        currencyDisplay?: string;

        /**
         * @default true
         */
        useGrouping?: boolean;

        /** Possible values are from 1 to 21
         * @default null
         */
        minimumIntegerDigits?: number;

        /** Possible values are from 0 to 20
         * @default null
         */
        minimumFractionDigits?: number;

        /** Possible values are from 0 to 20
         * @default null
         */
        maximumFractionDigits?: number;

        /** Possible values are from 1 to 21
         * @default null
         */
        minimumSignificantDigits?: number;

        /** Possible values are from 1 to 21
         * @default null
         */
        maximumSignificantDigits?: number;

        /**
         * @default -%v
         */
        negativeTemplate?: string;

        abbreviate?: {
            abbreviations: {
                /**
                 * @default k
                 */
                thousands: string;

                /**
                 * @default m
                 */
                millions: string;

                /**
                 * @default b
                 */
                billions: string;

                /**
                 * @default t
                 */
                trillions: string;
            };

            /**
             * @default ""
             * @enum ["", "auto", "thousands", "millions", "billions", "trillions"]
             */
            mode: string;
        }

        /** The null string format.
         * @default –
         */
        nullString?: string;
    }

    export interface DateFormat {
        /**
         * @default date
         */
        dataType: string;

        /** Will default based on locale.
         * @default null
         */
        timeZone?: string;

        /** Will default based on locale.
         * @default null
         */
        hour12?: boolean;

        /**
         * @enum ["numeric", "2-digit"]
         * @default numeric
         */
        year?: string;

        /**
         * @enum ["numeric", "2-digit", "narrow", "short", "long"]
         * @default numeric
         */
        month?: string;

        /**
         * @enum ["numeric", "2-digit"]
         * @default numeric
         */
        day?: string;

        /**
         * @enum ["numeric", "2-digit"]
         * @default null
         */
        hour?: string;

        /**
         * @enum ["numeric", "2-digit"]
         * @default null
         */
        minute?: string;

        /**
         * @enum ["numeric", "2-digit"]
         * @default null
         */
        second?: string;

        /**
         * @enum ["short", "long"]
         * @default null
         */
        timeZoneName?: string;

        /**
         * @enum ["narrow", "short", "long"]
         * @default null
         */
        weekday?: string;

        /**
         * @enum ["narrow", "short", "long"]
         * @default null
         */
        era?: string;

        /** The null string format.
         * @default –
         */
        nullString: string;
    }

    /**
     * Note: all of these need to be explicitly cleaned up in asterix-ng's ModelBinder.
     * Ideally ModelBinder would handle this smarter, e.g. if model property has property dataSourceType but it's undefined/null/empty, clear ALL object properties
     * (but this doesn't allow for the case when we hardcode data directly onto a model property that inherits this interface, so we would have to re-work those cases and start using defaults)
     */
    export interface DataSourceSettings {
        /** The type of data source.
         * @enum ["", "api", "selector"]
         * @default ""
         */
        dataSourceType: string;

        /** Whether or not to merge new values with the current data source value. Currently only applicable for dataSourceType "selector".
         * @default false
         */
        merge: boolean;

        /** The transform stack to apply to the result of the data source prior to binding it to the model, e.g. ["wrapObjectPath", "wrappingPropertyName"]
         * The first element in the array is referencing a specific transform type. That transform type (function) will be called with the data as the first argument and the rest of the array elements after.
         * @default []
         */
        transformPipeline: TransformNode[];

        /** The selector expression, used when the type is set to "selector", e.g. model(_).prop (parent model's "prop")
         */
        selector: string;

        /** The api settings, used when the type is set to "api".
         */
        api: ApiSettings;

        /** The default values for this data source. Use this to "hardcode" values rather than assigning them directly to the data source property in the configuration.
         * This can also be used to specify the expected format of a data source. To do this, extend the DataSourceSettings interface and override this property with an interface that
         * represents the desired structure.
         */
        defaults: any;
    }

    export interface TransformNode {
        /* Name of the transform to apply. The transform must be registered with morningstar.asterix.ng.transforms.register()
        */
        name: string;

        /* Options to provide to the transform. The transform will be able to access the options as "this.options"
        */
        options: any;
    }

    export interface ApiSettings {
        /** Method
         * @default get
         * @enum ["get", "post", "put", "delete", "jsonp"]
         */
        method: string;

        /** Url
         */
        url: string;

        /** Use previous data when api has error
         * @type boolean
         * @default true
         */
        usePreviousDataOnError: boolean;

        /** The validation expression, e.g. inputsForm.$valid
         * @type string
         * @default ""
         */
        validationExpression: string;

        /** The validation expressions (evaluated by ConfigEvaluator) that need to evaluate to true in order for the api call to be made.
         * @type string
         * @default []
         */
        validationExpressions: string[];

        parameters: {
            /** The expression(s) to watch (on parameters).
             * Can be an array of strings or a string. If given a single, empty string it will watch the whole parameters object.
             * @default null
             */
            watchedPaths: any;

            /** The transform stack to apply to the parameters prior to calling the api with them. Will be called with the parameters object as its first argument.
             * @default []
             */
            transformPipeline: TransformNode[];

            /** Specifies whether or not to perform a deep watch.
             * @default true
             */
            deep: boolean;

            /** The time (ms) to throttle parameter changes (to avoid spamming api calls).
             * @type integer
             * @default 200
             */
            throttleTime: number;

            /** if the toTransform is sync i.e. it doesn't return promise instead it returns the result itself, then set this property to true. This will reduce wait-time before api call
             * @type boolean
             * @default false
             */
            isTransformSync: boolean;
        }
    }

    export enum ValidationRuleType {
        alpha,
        alphaSpaces,
        alphaNumeric,
        alphaNumSpaces,
        alphaDash,
        alphaDashSpaces,

        integer,
        range,
        regex,
        required
    }

    export enum DataType {
        number,
        string,
        date
    }

    export interface Validation {
        /** Should this instance handle the display of its own (and its children's unhandled) validation messages?
         * @default self
         */
        validationMessageHandlerType: ValidationMessageHandlerType;

        /** When validation messages should be displayed (if at all handled by this instance, see above)
         * @default always
         */
        messageWhen: ValidationTriggerWhen;

        /** The data type. Only applicable for field-level validation.
         * @type string
         * @title Data Type
         */
        dataType: DataType;

        /** todo: use messageWhen instead
         * @default touched
         */
        tooltipWhen: ValidationTriggerWhen;

        rules: { [key:string]: ValidationRuleSettings; };
        modelRules: { [key:string]: ModelValidationRuleSettings; };
    }

    export enum ValidationMessageHandlerType {
        self, parent
    }

    export enum ValidationTriggerWhen {
        always,
        dirty,
        touched,
        manual, // "dirty state" will be set manually by the calling code
        never
    }

    export interface ModelValidationRuleSettings {
        /**
         * The error message label key.
         */
        labelKey: string;

        /**
         * The in-data for the transform pipeline.
         */
        data: string;

        /**
         * The transform pipeline.
         */
        transformPipeline: TransformNode[];

        /**
         * The end result of "transformPipeline" above should equal "ok" for this rule to be considered value.
         */
        ok: any;

        /**
         * Label placeholder data, can be used like {{_ self().placeholders[0] _}} in referenced labels.
         */
        placeholders: any[];
    }

    export interface ValidationRuleSettings {
        /**
         * @type string
         */
        type: ValidationRuleType;
        labelKey: string;
        min: any;
        max: any;
        pattern: string;
        placeholders: any[];
    }

    export interface ViewBreakpoints {
      /** max-width for small view
      */
      small: number;

      /** max-width for medium view
      */
      medium: number;

      /** max-width for large view
      */
      large: number;

      /** max-width for xlarge view
      */
      xlarge: number;
    }
}
