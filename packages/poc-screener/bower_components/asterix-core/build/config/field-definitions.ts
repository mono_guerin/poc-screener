// startref
/// <reference path='../../.references.ts'/>
// endref
module AsterixCore {

    export interface FieldDefinition {
        requiredFields?: string[];

        /**
         * object: NumberFormat |
         * object: DateFormat |
         * string: enum["standardDecimal","integer","shortDecimal","longDecimal"]
         */
        format?: any;
    }

    /* To be used inside Settings */
    export interface FieldDefinitions {
        categoryId?: FieldDefinition;
        categoryName?: FieldDefinition;
        closePrice?: FieldDefinition;
        debtEquityRatio?: FieldDefinition;
        dividendYield?: FieldDefinition;
        epsGrowth5Y?: FieldDefinition;
        expenseRatio?: FieldDefinition;
        industryName?: FieldDefinition;
        name?: FieldDefinition;
        marketCap?: FieldDefinition;
        priceBookRatio?: FieldDefinition;
        priceCurrency?: FieldDefinition;
        priceEarningsRatio?: FieldDefinition;
        priceSalesRatio?: FieldDefinition;
        rating?: FieldDefinition;
        returnD1?: FieldDefinition;
        returnW1?: FieldDefinition;
        returnM0?: FieldDefinition;
        returnM1?: FieldDefinition;
        returnM3?: FieldDefinition;
        returnM6?: FieldDefinition;
        returnM12?: FieldDefinition;
        returnM36?: FieldDefinition;
        returnM60?: FieldDefinition;
        returnM120?: FieldDefinition;
        revenueGrowth3Y?: FieldDefinition;
        roettm?: FieldDefinition;
        salesYield?: FieldDefinition;
        sectorName?: FieldDefinition;
    }
}
