**Please DO NOT contribute to this repo directly. All PR should go to the team project**

# **core**

# Introduction

# Configuration

## Types config
Types config are configurations that are applied to all instances of that type.

The instance that it will be applied to can be current or a sub-component of the current instance. The sub-component hierarchy is not limited i.e it can be child, grand-child, grand grand-child and so on.

Types config can be defined in either default config or instance config of a component.

> Example: Below default config of security-report component disables sorting for all instances of ecTable

> *components/security-report/src/config/ec-security-report.json*
> ```js
> {
>     "type": "ecSecurityReport",
>     "settings": {
>         //default settings go here
>     },
>     "types": {
>         "ecTable": {
>             "type": "ecTable",
>             "settings": {
>                 "sortingEnabled": false
>             }
>         }
>     }
> }
> ```

Whereas a client-config of client-x may want sorting to be enabled for tables in security-report
> Example: Below client-config for security-report component enables sorting for all instances of ecTable

> *components/security-report/src/config/ec-security-report.json*
> ```js
> {
>     "components": {
>         "securityReportInstance1": {
>             "type": "ecSecurityReport",
>             "settings": {
>                 //instance settings go here
>             },
>             "types": {
>                 "ecTable": {
>                     "type": "ecTable",
>                     "settings": {
>                         "sortingEnabled": true
>                     }
>                 }
>             }
>         }
>     }
> }
> ```

### Precedence order (left is less and right is more)
> Default config ==> **types-config in default-config** ==> **types-config in app-config** ==> blueprints config ==> instance config

## Blueprints

Blueprints are predefined configs for components that consumer components or code can apply. Components can define as many blueprints. Blueprints are built with component code.

### Defining and using blueprints
Blueprint definition is a json object and they are defined as part of component's default config i.e. *src/config/{component-name}.json*
> Example 1: blueprint for multiline ecInput

> defining: *components/input/src/config/ec-input.json*
>```js
> {
>     "type": "ecInput",
>     "blueprints": {
>         "multiline": {
>             "type": "ecInput",
>             "settings": {
>                 "inputType": "textarea",
>                 "rows": 10,
>                 "cols": 100
>             }
>         }
>     }    
> }
>```

> using: *components/filters/src/config/ec-filters.json*
> ```js
> {
>     "type": "ecFilters",
>     "components": {
>         "search": {
>             "type": "ecInput",
>             "blueprint": "multiline"
>         }
>     }
> }
> ```

To keep component's default json file maintainable, its strongly recommended to keep blueprints definitions separate. Per convention blueprint files should be defined in *src/config/blueprints* folder. Blueprints can also reference external html files. Html templates used in blueprints should be saved in *blueprints/html* folder

#### Example: Blueprint config in a separate file

> *components/input/src/config/blueprints/multiline-config.json*
> ```js
> {
>     "type": "ecInput",
>     "blueprints": {
>         "multiline": {
>             "type": "ecInput",
>             "settings": {
>                 "_templates": {
>                     "main": "{{#blueprints/html/multiline.html#}}",
>                 }
>                 "inputType": "textarea",
>                 "rows": 10,
>                 "cols": 100
>             }
>         }
>     }    
> }
> ```

*Note: Since, blueprint files get merged with default config, each blueprints config needs to be defined from the root*

#### Example 2: Blueprint for Performance table

> defining: *components/table/src/config/blueprints/performance-config.json*
>```json
> {
>     "type": "ecTable",
>     "blueprints": {
>         "performance": {
>             "type": "ecTable",
>             "settings": {
>                 "orientation": "horizontal",
>                 "sortingEnabled": false,
>                 "paginationMode": "none",
>                 "stickyLeadEnabled": true,
>                 "stickyLeadBreakPoints": [{
>                     "stickyColIndices": [0],
>                     "columnsPerPage": 1,
>                     "minWidth": 0,
>                     "maxWidth": 300
>                 }, {
>                     "stickyColIndices": [0],
>                     "columnsPerPage": 2,
>                     "minWidth": 301,
>                     "maxWidth": 460
>                 }, {
>                     "stickyColIndices": [0],
>                     "columnsPerPage": 5,
>                     "minWidth": 461,
>                     "maxWidth": 1024
>                 }]
>             }
>         }
>     }
> }
>```

> Usage: components/security-report/preview/{client}/config.js
> ```json
> {
>     "type": "ecTable",
>     "blueprint": "performance",
>     "settings": {
>         "caption": "debtProfileDataTable",
>         "fields": [
>             "reportDateYear",
>             "financialLeverage"
>         ]
>     }
> }
> ```

### Precedence order (left is less and right is more)
> Default config ==> types-config in default-config ==> types-config in app-config ==> **blueprints config** ==> instance config

### Guidelines for defining blueprints
- Try to keep blueprints as generic
- Before adding blueprints always first check with core team (SPOC is Tobias)
- Blueprints should be defined in *blueprints* folder
- Multiple blueprints cannot be applied on components hence blueprints should be independent

## Precedence
Below diagram illustrates precedence of config. Config on the left gets overridden with config on right, in case of conflict

![Precedence order for config](../../docs/config-precedence.png)

`Config` - the final config object for the component
* `Base config` - the base config shared by all components (from asterix)
* `Default config`
    - `Inheritable config from parent’s default config` - the settings declared in a component's default config that a child component can inherit, e.g. top-level settings in `ec-xray.json` that are inherited by the `asset allocation` component
    - `Default config for type` - the default config for the component type, e.g. the config declared for the `asset allocation` component in `ec-asset-allocation.json`
    - `Default instance config (from parent)` - the default config for this instance of the component, e.g. the config for `asset allocation` within the `x-ray` as declared in `ec-xray.json`
* `Instance config`
    - `Inheritable config from parent instance` - the settings declared in the app config that a child component can inherit, e.g. the top-level `x-ray` settings that are inherited by the `asset allocation` component
    - `Component’s instance config`
        - `Type override from default config` - config from a type override for a component type within a (top-level) component's default config
        - `Type override from instance config (from app config)`- config from a type override for a component type within a (top-level) component's instance config
        - `Config from blueprint` - config from a blueprint applied to the component. A blueprint can be applied either in the default config or the instance config for the component.
        - `Instance config (from app config)` - the instance config for the component declared in the app config


# Events
## Triggering custom events
All Ec components should trigger appropriate events for user interactions e.g. user input changed for chosen, model portfolio card selection changed etc. For raising custom events there are 2 steps as below

1. Registering custom event for component: in the _entrypoint.js, register meta property “allowedEvents” with the array of custom events that the component can trigger
```javascript
 morningstar.asterix.meta.register('ecTable', 'allowedEvents', ['ecTableShowMoreRows', 'ecTableShowLessRows']);
```
Precedence order (left is less and right is more)
> Default config ==> types-config in default ==> types-config in app ==> blueprints config ==> instance config

2. Trigger custom event from directive code
```javascript
scope.handleTableRowsToggle = function(event) {
    if (showExpandedRows) {
        scope.instance.trigger('ecTableShowMoreRows', [event]);
    } else {
        scope.instance.trigger('ecTableShowLessRows', [event]);
    }
};
```
Note: Custom event name should be prefixed with component-type (e.g. "ecTable") followed by event-topic (e.g. "ShowMoreRows")

# ComponentInstance
Represents an instance of a component. Contains the instance's associated settings, labels, model and provides an interface for listening to events of the instance.

## Validation State
Contains validation state information for a ComponentInstance. Is responsible for storing and aggregating validation states throughout the component instance hierarchy. Generally the state will be set automatically through bindings (see mstarModelValidation and mstarValidate in asterix-ng).

> Public interface
```js
ValidationState(instance) - Constructor. Takes a ComponentInstance as its only argument.
isValid() - Returns false if the merged state contains any invalid result
getNamespacedState() - Returns the own state for this instance with namespaced (based on module path) result keys
getMergedStateFiltered([predicate]) - Returns the merged state for this instance and all its descendants. Takes an optional predicate that filters the results prior to returning them.
getState() - Returns the own state for this instance.
setState(ownState) - Sets the own state and triggers re-calculation of merged states up the chain.
```

> Structure for own state (getState())
```js
{
    ruleName: {
        returnValue: true, // whether this rule passed or not
        rule: {
            labelKey: 'someLabelKey',
            // the rest of rule definition. will differ depending on whether it's a field-level validation rule or a model validation rule
        }
    },
    // ..other results
}
```
> Structure for merged state (getMergedStateFiltered())
```js
{
    'modulePath_ruleName': {
        returnValue: true, // whether this rule passed or not
        label: 'translated label', // will be set based on the rule's labelKey, translated by the instance that created the validation state
        ownInstanceId: 'instanceId', // the last segment of modulePath
        modulePath: 'full.instanceId',
        rule: {
            labelKey: 'someLabelKey',
            // the rest of rule definition. will differ depending on whether it's a field-level validation rule or a model validation rule
        }
    },
    // ..other results
}
```

# Instrumentation
Instrumentation in asterix supports logging and tracing. The objective of instrumentation is to help investigate issues during development and production.

For effective instrumentation, relevant information should be logged. Tracing helps in measuring performance of operation. Operation can be IO based async operation such as an "ajax call" or cpu-intensive operation such as "rebasing calculation". Logging and tracing is added in asterix core and ng. Effective logging and tracing should also be added in components

![Instrumentation design](../../docs/instrumentation-design.png)

Instrumentation has publisher/subscriber model where log-providers register for being notified for logs of different types (error, trace etc)

## Update identity
Clients can set identity info with instrumentation. Identity can be set with `morningstar.asterix.instrumentation.updateIdentity(identity)` method. When the identity is set asterix will log this as *info*. It is recommended **identity** to have below properties but not limited and client can add more info.

- `sessionId`: sessionId/userId of the end-user. This will be added to each log
- `configPath`: Config path used by client for loading the tool component
- `url`: Url of the page
- `Client IP`: IP address of the end-user
- `User agent`: User agent used by the end-user. Should also include version no
- `Platform`: Platform/OS used by the end-user with version no (if possible to retrieve with JavaScript)

## Log types & Log-Levels
### Log types
Logging can be done for different log-type. Below are the log-type supported by Logger

| Log type | Type code | Notes |
|---|---|---|
| FATAL | 1 | Application encountered fatal error and may not work |
| ERROR | 2 | Application encountered error. 1 or more functionalities may not work |
| WARN | 4 | Application noticed unexpected behaviour however functionality not affected |
| INFO | 8 | Application started/completed a major milestone when performing some operation |
| DEBUG | 16 | Application started/completed a minor milestone when performing some operation |
| TRACE | 32 | Application completed performance tracing of an operation |

Log types are defined in *morningstar.asterix.instrumentation.LOG_TYPE*
Example: `morningstar.asterix.instrumentation.LOG_TYPE.ERROR`

### Log levels
A log-level is en-ability of 0 or more log-types for log-providers. Below are log-types pre-defined in asterix core

| Log level | En ability |
| --- | --- |
| NONE | 0 |
| DEFAULT | FATAL, ERROR, WARN |
| ALL | FATAL, ERROR, WARN, INFO, DEBUG, TRACE |

Log levels are defined in *morningstar.asterix.instrumentation.LOG_LEVEL*
Example: `morningstar.asterix.instrumentation.LOG_LEVEL.DEFAULT`

Developers can define their own log-levels with log-types separated using bitwise OR (|) operator
Example:
```js
var myLogLevel = morningstar.asterix.instrumentation.LOG_TYPE.INFO | morningstar.asterix.instrumentation.LOG_TYPE.TRACE;
```
## Tags decorated logger
Tags decorated logger is a logger instance which accepts tags in ctor and creates a logger instance with tags. These tags are included in each of the log made using this logger.

The tags are passed as the last argument to the logger methods i.e. `error()`, `info()` etc

To get decorated logger instance you can invoke `morningstar.asterix.instrumentation.getTagsDecoratedLogger(tags)`

## Invoking logger methods

The default-logger instance is accessible as `morningstar.asterix.instrumentation.logger`. So, to do an info log, developers can invoke `morningstar.asterix.instrumentation.logger.info('some information')`

Invoking from inside component directives
> `morningstar.asterix.instrumentation.logger` is accessible in
> `scope.logger` inside component's directive. Also, accessible as `componentInstance.getLogger()`. It is highly recommended to
> use this when invoking logger methods related to component as it would tag the logs with component's module-path
>
> `componentInstance.getLogger().info('some information')`
> `scope.logger.info('some information')`

## Log providers
Log providers are objects that subscribe to instrumentation to get notified for logs so as to capture, report or print them at a target. For example ConsoleLogProvider will print all logs in console

Log providers should have a public property logLevel

### Built-in log providers
Asterix core provides some built-in log providers as below. Built-in log providers are accessible with `morningstar.asterix.providers`

- ConsoleLogProvider: Prints log information in console

 ```js
 function ConsoleLogProvider() {
 }
 ```

- FilterLogProvider: filters the notifications based on filter criteria
 ```js
 function FilterLogProvider(filters) {
 }
 ```

### Creating custom log providers
Developers can create custom log providers. The custom log provider should have method defined for each log-type that will be called by *instrumentation.logger* i.e. `fatal()`, `error()`, `warn()`, `debug()`, `info()` & `trace()`. Also, the log-provider may have property logLevel which the *instrumentation.logger* will check to determine whether log-provider is interested in a particular log-type. If logLevel is not present then *instrumentation.LOG_LEVEL.DEFAULT* is assumed

Example:

```js
var MyLogProvider = function() { 		
    this.logLevel =
morningstar.asterix.instrumentation.LOG_LEVEL.ALL;
    this.error = function() {};
    this.fatal = function() {};
    this.warn = function() {};
    this.info = function() {};
    this.debug = function() {};
    this.trace = function() {},;
};
```

### Registering log-provider with instrumentation

To register log-provider with instrumentation call `instrumentation.registerLogProvider(name, provider, filters)`

*filters* can have following properties
    - `logLevel`
    - `componentModulePath`
    - `operation`
    - `includeStack`
    - `filterFunc`
Example:

```js
morningstar.asterix.instrumentation.registerLogProvider('screenerDevLogProvider',
new morningstar.asterix.providers.ConsoleLogProvider(), {
    logLevel: morningstar.asterix.instrumentation.LOG_LEVEL.ALL,
    componentModulePath: 'standardTopNavFundsScreener',
    operation: new RegExp(/setModelProperty/i),
    includeStack: true,
    filterFunc: function() {
        return arguments[1] && arguments[1].indexOf('filterMinMax') > -1;
    }
});
```

To unregister a log-provider call `instrumentation.unregisterLogProvider(name)`

## Tracing
Tracing allows for measuring performance of an operation. The operation can be sync or async.

Tracing api in asterix is accessible with `morningstar.asterix.instrumentation.tracer` object

### API
- `tracer.startTrace(name, tags, loggerInstance)` : This method starts tracing of operation. It returns a `trace` object. `name` must be unique so as to easily identify in logs.
loggerInstance` is optional and if supplied will be used for logging a trace otherwise the default logger instance will be used

- `tracer.endTrace(trace, contextData)`: Ends tracing for `trace` object. `trace` is the object returned from `startTrace()`. `contextData` is can be an object with contextual information about the operation

- `trace.end(contextData)`: Alternative to `tracer.endTrace()`

	> Example - synchronous
	```js
	// Generate performance data as per the frequency tab selected
	function createFrequencyData(chartValue, performanceChartData, selectedFrequency) {
	    var trace = morningstar.asterix.instrumentation.tracer.startTrace('mstarRebase.createFrequencyData()');
	    CHART_BASE_VALUE = chartValue;
	    //code for rebasing
	    trace.end({
	        chartValue: chartValue,
	        performanceChartData: performanceChartData,
	        selectedFrequency: selectedFrequency,
	        series: series
	    });
	    return series;
	}
	```

	> Example - Asynchronous
	```js
	api: function(apiSettings, data, urlParameters) {
	    var trace = morningstar.asterix.instrumentation.tracer.startTrace('mstarAjax.api()');
	    //code to prepare ajax request
	    var promise = this.handleHttpPromise($http(apiSettings));
	    promise.then(function(response) {
	        trace.end({apiSettings: apiSettings, data: data, urlParameters: urlParameters, response: response, status: 'success'});
	    }, function(response) {
	        trace.end({apiSettings: apiSettings, data: data, urlParameters: urlParameters, response: response, status: 'error'});
	    });
	    return {
	        promise: promise,
	        cancel: function() {
	            trace.end({apiSettings: apiSettings, data: data, urlParameters: urlParameters, status: 'cancelled'});
	            canceller.resolve('Cancelled.');
	        }
	    };
	}
	```
# Usage tracking
Usage tracking api provides a way for components to publish usage-tracking events or hits. It follows the same publisher-subscriber model as in the instrumentation. Clients can register *Usage tracking provider* to send the tracking hits to providers such as *Google Analytics*, *Heap Map*, *New Relic* etc

Default instance of usage tracker is accessible as `morningstar.asterix.usageTracker`

## API
- `trackHit(hitObj)`: This method tracks hit and sends the hitObj to the subscribed usage tracking provider(s). `hitObj` can have following properties
-- `type`: type of hit. Possible values are **event** (default) & **timing**
-- `category`: category for the hit. If category is not specified but sourceComponent is then category is set as the module-type of the root component
-- `action`: what was the action performed. e.g. "component loaded"
-- `label`: Label for the hit: eg: "Export"
-- `value`: Any value contextual to the hit
-- `sourceComponent`: Component instance object from where this was called. If the sourceComponent is specified then following properties are pre-poluated
--- `componentType`: type of component eg: "mstarChildType"
--- `componentTypePath`: path type of component e.g. "mstarGrandParentType.mstarParentType.mstarChildType"
--- `componentModulePath`: path for component eg: "grandparent1.parent1.child1"

- `registerUsageTrackingProvider(name, provider)`: Registers usage-tracking provider
- `unregisterUsageTrackingProvider(name)`: Unregisters tracking provider

## Component-instance decorated usageTracker
Component instance decorated usageTracker accepts component-instance in ctor and creates a tracker instance with component-instance. The component instance is set as the `sourceComponent` in the hitObj before passing to providers.

To get decorated tracker instance you can invoke `morningstar.asterix.usageTracker.getComponentDecoratedTracker(componentInstance)`

## Invoking trackHit method

The default-usageTracker instance is accessible as `morningstar.asterix.usageTracker`. So, to track a hitObj you may invoke `morningstar.asterix.usageTracker.trackHit(hitObj)`

Invoking from components
> `usageTracker` is accessible as
> `scope.usageTracker` inside component's directive. Also, accessible as `componentInstance.getUsageTracker()`. It is highly recommended to
> use this when invoking trackHit method related to component as it would set the component as `hitObj.sourceComponent`
>
> `componentInstance.getUsageTracker().trackHit(hitObj)`
> `scope.usageTracker.trackHit(hitObj)`

## Usage tracking providers
Usage tracking providers are objects that subscribe to usageTracking to get notified for hits so as to capture and report them at a target. For example GoogleAnalyticsUsageTrackingProvider will send all hits to GA service

Usage tracking providers should have a public method `onHit(hitObj)`

### Built-in log providers
Asterix core provides some built-in usage-tracking providers as below. Built-in tracking providers are accessible with `morningstar.asterix.providers`

- GoogleAnalyticsUsageTrackingProvider: Sends tracking hits to GA service
 ```js
 function GoogleAnalyticsUsageTrackingProvider() {
 }
 ```

- FilterUsageTrackingProvider: filters the notifications based on filter criteria
 ```js
 function FilterUsageTrackingProvider(filters) {
 }
 ```
 *filters* has following properties
 -- `componentModulePath`
 -- `componentType`
 -- `componentTypePath`
 -- `type`
 -- `category`
 -- `action`
 -- `filterFunc`

Example:

```js
morningstar.asterix.usageTracker.registerUsageTrackingProvider('googleTrackingProvider', new morningstar.asterix.providers.GoogleAnalyticsUsageTrackingProvider(), {
        type: 'event',
        componentModulePath: new RegExp(/report/i),
        componentType: null,
        componentTypePath: new RegExp(/ecSecurityReportLoader.ecSecurityReport/i),
        category: null,
        action: null,
        filterFunc: function() {
            return hitObj.value.something == 'someValue';
        }
    });
```
