// startref
/// <reference path='../../.references.ts'/>
// endref
module EcPaginator {
    export enum _PaginatorType { ecPaginator }

    export interface EcPaginator {
        /** Component type.
         * @default ecPaginator
         * @options.hidden true
         * @type string
         * @readonly true
         */
        type: _PaginatorType;

        settings: Settings;

        labels: {
            first: AsterixCore.Label;
            last: AsterixCore.Label;
            previous: AsterixCore.Label;
            next: AsterixCore.Label;
        }
    }

    export interface Settings {
        _parameters: {
            totalRows: {
                /**
                 * @default {{_ parameters(_).totalRows _}}
                 */
                binding: string;
            };

            page: any;

            /** Number of rows to move or change at a time
             *
             */
            paginationWindowSize: AsterixNg.Parameter;

            perPage: any;
        };

        _templates: {
            /** Template
             * @format html
             * @default <ec-section>\r\n    <div data-ng-show=\"parameters.lastPage > 1\">\r\n        <ul class=\"ec-pagination\">\r\n            <li class=\"ec-pagination__item ec-pagination__item--first ec-paginator--show-first-and-last\"\r\n                data-ng-if=\"settings.get('showFirstAndLast')\" data-ng-class=\"{ 'ec-pagination__item--disabled': parameters.page == 1 }\">\r\n                <a class=\"ec-pagination__link\" href=\"javascript:;\" data-ng-disabled=\"parameters.page == 1\"\r\n                    data-ng-click=\"setPage(1)\">{{ labels.get('first') }}<\/a>\r\n            <\/li>\r\n            <li class=\"ec-pagination__item ec-pagination__item--previous\"\r\n                data-ng-class=\"{ 'ec-pagination__item--disabled': !(parameters.page > 1) }\"\r\n                data-ng-if=\"!settings.get('dynamicPrevNext') || parameters.page > 1\">\r\n                <a class=\"ec-pagination__link\" href=\"javascript:;\" data-ng-disabled=\"!(parameters.page > 1)\"\r\n                    data-ng-click=\"setPage(parameters.page - 1)\">{{\r\n                    labels.get('previous') }}<\/a>\r\n            <\/li>\r\n            <li class=\"ec-pagination__item ec-pagination__item--number ec-paginator--show-page-numbers\" data-ng-if=\"settings.get('showPageNumbers')\"\r\n                data-ng-class=\"{ 'ec-pagination__item--active': parameters.page == page }\"\r\n                data-ng-repeat=\"page in pages track by $index\">\r\n                <a class=\"ec-pagination__link\" href=\"javascript:;\" data-ng-click=\"setPage(page)\">{{ page }}<\/a>\r\n            <\/li>\r\n            <li class=\"ec-pagination__item ec-pagination__item--next\"\r\n                data-ng-class=\"{ 'ec-pagination__item--disabled': parameters.page >= parameters.lastPage}\"\r\n                data-ng-if=\"!settings.get('dynamicPrevNext') || parameters.page < parameters.lastPage\">\r\n                <a class=\"ec-pagination__link\" href=\"javascript:;\"\r\n                    data-ng-click=\"setPage(parameters.page + 1)\">{{\r\n                    labels.get('next') }}<\/a>\r\n            <\/li>\r\n            <li class=\"ec-pagination__item ec-pagination__item--last ec-paginator--show-first-and-last\"\r\n                data-ng-if=\"settings.get('showFirstAndLast')\"\r\n                data-ng-class=\"{ 'ec-pagination__item--disabled': parameters.page >= parameters.lastPage }\">\r\n                <a class=\"ec-pagination__link\" href=\"javascript:;\"\r\n                    data-ng-disabled=\"parameters.page >= parameters.lastPage\"\r\n                    data-ng-click=\"setPage(parameters.lastPage)\">\r\n                    {{ labels.get('last') }}<\/a>\r\n            <\/li>\r\n        <\/ul>\r\n    <\/div>\r\n<\/ec-section>\r\n
             */
            main: string;
        }

        /** Toggle adaptive perPage number to container width feature
         * @default false
         */
        adaptivePagination: boolean;

        adaptivePaginationRanges: AdaptivePaginationRange[];

        /** If true, hide next/previous buttons when they're inactive.
         * @default true
         */
        dynamicPrevNext: boolean;

        /** The number of wrapping page links.
         * @default 5
         */
        numberOfWrappingPageLinks: number;

        /**
         * @default 1
         */
        page: number;

        /**
         * @default 10
         */
        perPage: number;

        /** Number of rows to move or change at a time
         *
         */
        paginationWindowSize?: number;

        /**
         * @default true
         */
        showPageNumbers: boolean;

        /**
         * @default false
         */
        showFirstAndLast: boolean;

        /**
         * @type integer
         */
        totalRows: number;
    }

    export class AdaptivePaginationRange {
        startPoint: number;
        endPoint: number;
        perPage: number;
    }
}
