// startref
/// <reference path='../../.references.ts'/>
// endref
module EcContainer {
    export interface Settings extends AsterixNg.ComponentSettings {
        _templates: Templates;
        /**
         * Hashmap of SectionConfig
         */
        sections: any;

        defaultSection: SectionConfig;

        breakpoints: Breakpoints;

        /**
         * @default false
         */
        enableToggleForExpand: boolean

        /**
         * @default false
         */
        debugMode: boolean;

        show: string;
    }

    export interface Breakpoints {
        defaults: Breakpoint;
        small: Breakpoint;
        medium: Breakpoint;
        large: Breakpoint;
        xlarge: Breakpoint;
    }

    export interface Breakpoint {
        /**
         * @default null
         */
        collapserPosition?: BreakpointCollapserPosition;

        /**
         * @default null
         */
        autoCollapseMode?: BreakpointActiveType;

        /**
         * Hashmap of GroupConfig
         * @default null
         */
        groups?: any;
    }

    export interface GroupConfig {
        order: number;
        cols: number;

        /**
         * Array of strings (specifying only componentId) OR array of IdentifiedSectionBreakpoint
         */
        sections: any;

        show: string;
    }

    export interface IdentifiedSectionBreakpoint extends SectionBreakpoint {
        sectionId: string;
    }

    export enum BreakpointCollapserPosition {
        none,
        firstPerRow,
        lastPerRow,
        individual
    }

    export enum BreakpointActiveType {
        none,
        expand,
        collapse
    }

    export interface SectionConfig {
        breakpoints: {
            defaults: SectionBreakpoint;
            small: SectionBreakpoint;
            medium: SectionBreakpoint;
            large: SectionBreakpoint;
            xlarge: SectionBreakpoint;
        }

        show: string;
    }

    export interface SectionBreakpoint {
        /**
         * @default null
         */
        autoCollapseMode?: BreakpointActiveType;

        cols: number;

        /**
         * @format html
         * @default null
         */
        footerTemplate?: string;
    }

    export interface Templates {
        /** Title template.
         * @format html
         * @default <h1 id=\"{{ :: headerId }}\" class=\"{{ :: classWithModifier('ec-section__title') }}\">\r\n    <span mstar-tooltip-content=\"{{ :: titleTooltipLabelId && labels.contains(titleTooltipLabelId) ? labels.get(titleTooltipLabelId) : '' }}\" mstar-tooltip data-ng-bind-html=\"title\"><\/span>\r\n<\/h1>\r\n
         */
        containerSectionTitle: string;

        /** Subtitle template.
         * @format html
         * @default <h2 class=\"{{ :: classWithModifier('ec-section__subtitle') }}\" data-ng-if=\"subTitle\">{{ subTitle }}<\/h2>\r\n
         */
        containerSectionSubtitle: string;
    }
}
