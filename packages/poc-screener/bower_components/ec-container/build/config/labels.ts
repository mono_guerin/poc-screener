// startref
/// <reference path='../../.references.ts'/>
// endref
module EcContainer {
    export interface Labels {
        close: AsterixCore.Label;
        open: AsterixCore.Label;
    }
}
