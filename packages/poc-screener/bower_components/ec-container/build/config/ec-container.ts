// startref
/// <reference path='../../.references.ts'/>
// endref
module EcContainer {
    export enum _Type { ecContainer }

    /**
     * @title Section
     */
    export interface EcContainer {
        /** Component type.
         * @default ecContainer
         * @options.hidden true
         * @type string
         * @readonly true
         */
        type: _Type;

        settings: Settings;

        labels: Labels;
    }
}
