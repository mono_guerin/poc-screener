// startref
/// <reference path='../../.references.ts'/>
// endref
module EcSecurityLineChart {
    export interface Settings extends AsterixNg.ComponentSettings {
        __env: {
            production: {
                /** Default apiUrl
                 * @default "https:\/\/quotespeed.morningstar.com\/apis\/api.jsp"
                 */
                apiUrl: string;
            };
        };

        _parameters: {
            compareTickers: AsterixNg.Parameter;
            currencyId: AsterixNg.Parameter;
            growthBaseValue: AsterixNg.Parameter;
            languageId: AsterixNg.Parameter;
            removingInvestment: AsterixNg.Parameter;
            urlKey: AsterixNg.Parameter;
            ticker: AsterixNg.Parameter;
        };

        _templates: Templates;

        /** Default apiUrl
         * @default "https:\/\/markets-uat.morningstar.com\/apis\/api.jsp"
         */
        apiUrl: string;

        /** Settings for the chart
         */
        chartSettings: ChartSettings;

        /** Tickers for comparison separated by comma (,)
         * @default []
         */
        compareTickers?: string[];

        /** Timeline key that will be selected by default
         * @default "year"
         */
        defaultTimelineKey: string[];

        /**
         *  @default false
         */
        enableGrowthAmountInput: boolean;

        /**
         *  @default 200
         */
        growthBaseValue: number;

        /** Namespace for css/scss classes
         * @default ec-security-line-chart
         */
        namespaceClass: string;

        /** Solution key needed by markets to call timeseries API
         *  @default tr15g7dr1r
         */
        urlKey: string;

        /** Details for all timelines
         */
        timelines: TimelineSetting[];

        /** Ticker for the chart
         */
        ticker?: any;

        /** Timelines to show
         */
        visibleTimelineKeys: string[];

        /** provides additional settings needs to be added
         * @default {}
         */
        widgetSettings: any;
    }

    export interface Templates {
        /** The main template.
         * @format html
         * @default <ec-section>\r\n    <div data-ng-switch=\"isInteractiveChart()\">\r\n        <div data-ng-switch-when=\"false\" class=\"{{namespaceClass('__static')}}\">\r\n            <ul class=\"{{namespaceClass('timeframes')}}\">\r\n                <li data-ng-repeat=\"timeline in visibleTimelines track by $index\"\r\n                    class=\"{{namespaceClass('timeframes__timeframe') + ' ' +\r\n                    namespaceClass('timeframes__timeframe-' + timeline.staticChartRangeValue)}}\r\n                    data-ng-class:{'{{namespaceClass('timeframes__timeframe--selected')}}':\r\n                    (timeline.staticChartRangeValue === selectedTimeline.staticChartRangeValue)}\">\r\n                    <div role=\"button\" class=\"{{namespaceClass('timeframes__button')}}\"\r\n                         data-ng-click=\"setTimeRange(timeline, this, $event)\" title=\"{{labels.get(timeline.longLabelKey)}}\">\r\n                        {{labels.get(timeline.abbrLabelKey)}}\r\n                    <\/div>\r\n                <\/li>\r\n            <\/ul>\r\n            <figure class=\"{{namespaceClass('__figure')}}\">\r\n                <img src=\"\" id=\"{{namespaceId('chartImage')}}\" width=\"100%\" height=\"{{settings.get('chartSettings').height}}\" \/>\r\n                <figcaption data-ng-if=\"labels.contains('figCaption')\" class=\"{{namespaceClass('__figcaption')}}\"\r\n                            data-ng-bind=\"labels.get('figCaption')\"><\/figcaption>\r\n            <\/figure>\r\n        <\/div>\r\n        <div data-ng-switch-when=\"true\" class=\"{{namespaceClass('__interactive')}}\">\r\n            <div class=\"{{namespaceClass('growth-amount-box')}}\" data-ng-if=\"settings.get('enableGrowthAmountInput')\">\r\n                <label for=\"{{ ::namespaceId('growth-amount-textbox') }}\"\r\n                    class=\"{{ ::namespaceClass('growth-amount-box__input-label') }}\">\r\n                    {{ ::labels.get(\"growthAmountInput\") }}<\/label>\r\n                <ec-input mstar-component-id=\"growthAmount\" data-ng-model=\"parameters.growthBaseValue\"><\/ec-input>\r\n            <\/div>\r\n            <div class=\"{{namespaceClass('chart')}}\" id=\"{{namespaceId('chartWidget')}}\"><\/div>\r\n            <figcaption data-ng-if=\"labels.contains('figCaption')\" class=\"{{namespaceClass('__figcaption')}}\"\r\n                data-ng-bind=\"labels.get('figCaption')\"><\/figcaption>\r\n        <\/div>\r\n    <\/div>\r\n    <span data-ng-show=\"error\">{{error}}<\/span>\r\n<\/ec-section>\r\n
         */
        main: string;
    }

    export interface ChartSettings {
        /** Above chart color in hexcode eg: e1e1e1
         * @default 339900
         */
        aboveChartColor: string;

        /** Above chart fill-color in hexcode eg: e1e1e1
         * @default 91CC9B
         */
        aboveChartFillColor: string;

        /** Font family for axis labels
         * @default "arial"
         */
        axisFont: string;

        /** axis label color in hexadecimal eg: e1f3d4
         * @default 000000
         */
        axisLabelColor: string;

        /** axis label size in pixels
         * @default 12
         */
        axisLabelSize: number;

        /** Below chart color in hexcode eg: e1e1e1
         * @default CC3333
         */
        belowChartColor: string;

        /** Below chart fill-color in hexcode eg: e1e1e1
         * @default E78E91
         */
        belowChartFillColor: string;

        /** brushImage path eg: style/theme/brush.png
         */
        brushImage: string;

        /** The category for chart
         */
        category: any;

        /** Chart options. Possible values are "interactive", "static"
         * @default static
         */
        chartOptions: string;

        /** Chart type. Possible values are "mountainChart", "lineChart" and "growth10"
         * @default mountainChart
         */
        chartType: string;

        /**
         * Toggle compact controls
         * @default false
         */
        compact: boolean;

        /** The start and end dates with "|" separation. Date format: 2012-01-02|2013-01-01.
         *  2012-01-02 is the start date and 2013-01-01 is the end date.
         */
        customDateRange?: string;

        /** Used to set the data of main chart
         *  Different options are "dividendEffect",
         *  "growth", "growth10K", "marketValue",
         *  "nav", "postTax", "price","totalReturn"
         *  @default price
         */
        dataType: string;

        /** Date format eg: DD/MM/YYYY
         @default DD/MM/YYYY
         */
        dateFormat: string;

        /**
         * Toggle the "1 Day" tab in date range tabs
         * @default true
         */
        enable1DTab: boolean;

        /**
         * Toggle the "5 Day" tab in date range tabs
         * @default true
         */
        enable5DTab: boolean;

        /** fill color in hexcode eg: e1e1e1
         @default cccccc
         */
        fillColor: string;

        /** Starting amount of growth chart can be changed.
         * If set to 5000, the chart starts at 5000 amount.
         * @default 1000
         */
        growthBaseValue: number;

        /** Height of chart in pixels
         * @default 500
         */
        height: number;

        /**
         * Interactive chart intervalType
         * acceptable values are listed in enum IntervalType
         * @default OneYear
         */
        intervalType: IntervalType;

        /** line color in hexcode eg: e1e1e1
         * @default a1c4e6
         */
        lineColor: string;

        /** flag to show/hide legend for ticker
         * @default true
         */
        showLegend: string;

        /**
         * @default false
         */
        showLegendFullName: boolean;

        /**
         * @default 1
         */
        theme: number;

        /** Time range of the chart
         *  Possible values are 'oneday', 'fivedays', 'oneweek', 'onemonth',
         *  'threemonths', 'sixmonths', 'year', 'ytd','threeyears','fiveyears','tenyears','max',
         *  'custom'
         * @default oneweek
         */
        timeRange: string;

        /**
         * Enable EU timeseries data
         * @default false
         */
        useEUTimeSeriesData: boolean;

        /** Width of chart in pixels. '0' will be treated as 100% width of container
         * @default 0
         */
        width: number;

        /** x-axis label color in hexadecimal eg: e1f3d4
         */
        xAxisLabelColor: string;

        /** x axis label text
         * @default ""
         */
        xAxisLabelText: string;

        /** use this setting to change the length of xAxisLabelText
         * if suppose xAxisLabelText is more than 15 letters, you can change the value ex:- xLabelMaxLen: 25*
         * @default 15
         */
        xLabelMaxLen: number;

         /** use this setting to change label position of xAxisLabelText with provided values options:left/right
         * @default left
         */
        xLabelPos: string;

        /** y-axis label color in hexadecimal eg: e1f3d4
         */
        yAxisLabelColor: string;

        /** y axis label text
         * @default ""
         */
        yAxisLabelText: string;

        /**
         * @default 1
         */
        yAxisMargin: number;

        /**
         * @default right
         */
        yAxisOrient: string;

        /** use this setting to change the length of yAxisLabelText
         * if suppose yAxisLabelText is more than 15 letters, you can change the value ex:- yLabelMaxLen: 25
         * @default 15
         */
        yLabelMaxLen: number;

        /** use this setting to change label position of yAxisLabelText with provided values options:left/right
         * @default right
         */
        yLabelPos: string;
    }

    export interface TimelineSetting {
        /** Label key for abbreviation label
         */
        abbrLabelKey: string;

        /** The frequency of the chart.
         *  It should be 'OneDay', 'FiveDay', 'OneMonth',
         *  'ThreeMonth', 'SixMonth', 'OneYear', 'ThreeYear',
         *  'FiveYear'
         */
        interactiveChartRangeValue: string;

        /** The frequency of the static chart. It should be '1', '2', '5', '10', '15', '20', '30', '60',
         *  'd', 'w' and 'm'.
         *  'd': daily, 'w': weekly, 'm': monthly.
         */
        intervalValue: string;

        /** Label key for long label
         */
        longLabelKey: string;

        /** Range value to set in static-chart api for this timeline
         */
        staticChartRangeValue: string;
    }

    export enum IntervalType {
        OneDay,
        FiveDay,
        OneMonth,
        ThreeMonth,
        SixMonth,
        YTD,
        OneYear,
        ThreeYear,
        FiveYear,
        TenYear,
        Max
    }
}
