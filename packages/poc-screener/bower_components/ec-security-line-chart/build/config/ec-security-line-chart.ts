// startref
/// <reference path='../../.references.ts'/>
// endref
module EcSecurityLineChart {
    export enum _Type { ecSecurityLineChart }

    /**
     * @title SecurityLineChart
     */
    export interface EcSecurityLineChart {
        /** Component type.
         * @default ecSecurityLineChart
         * @options.hidden true
         * @type string
         * @readonly true
         */
        type: _Type;

        settings: Settings;

        labels: Labels;

        components: {
            growthAmount: EcInput.EcInput;
        }
    }
}
