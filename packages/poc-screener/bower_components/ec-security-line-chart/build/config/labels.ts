// startref
/// <reference path='../../.references.ts'/>
// endref
module EcSecurityLineChart {
    export interface Labels {
      oneDay: AsterixCore.Label;
      oneWeek: AsterixCore.Label;
      oneMonth: AsterixCore.Label;
      threeMonths: AsterixCore.Label;
      sixMonths: AsterixCore.Label;
      year: AsterixCore.Label;
      threeYears: AsterixCore.Label;
      fiveYears: AsterixCore.Label;
      tenYears: AsterixCore.Label;

      oneDayAbbreviation: AsterixCore.Label;
      oneWeekAbbreviation: AsterixCore.Label;
      oneMonthAbbreviation: AsterixCore.Label;
      threeMonthsAbbreviation: AsterixCore.Label;
      sixMonthsAbbreviation: AsterixCore.Label;
      yearAbbreviation: AsterixCore.Label;
      threeYearsAbbreviation: AsterixCore.Label;
      fiveYearsAbbreviation: AsterixCore.Label;
      tenYearsAbbreviation: AsterixCore.Label;

      figCaption: AsterixCore.Label;
      growthAmountInput: AsterixCore.Label;
      qsApiInitialisationError: AsterixCore.Label;
      tickerNotSetError: AsterixCore.Label;
    }
}