# ecSecurityLineChart

## Introduction
ec-security-line-chart is a component which can render share price chart using QuoteSpeed (QS) api

## Usage
ecSecurityLineChart can be used as an angular directive element

#### Example:
	<ec-security-line-chart mstar-component-id="ecSecurityLineStaticChart"></ec-security-line-chart>

###Configurable Options for ecSecurityLineChart

 - chartSettings
   	 1. chartOptions: Can be "static" (default) or "interactive"
   	 2. chartType: Can be "mountainChart" (default), "lineChart" or "growth10"
   	 3. timeRange: Can be 'oneday', 'fivedays', 'oneweek' (default), 'onemonth', 'threemonths', 'sixmonths', 'year', 'ytd','threeyears','fiveyears','tenyears','max', 'custom'
   	 4. customDateRange: Custom date range incase timeRange is set to "custom". Example 2012-01-02|2013-01-01
   	 5. height: Height of the chart in pixels
   	 6. width: Width of the chart in pixels
   	 7. axisFont: Font family for axis labels. Default is "arial"
   	 8. axisLabelColor: x-axis label color in hexadecimal eg: e1f3d4
   	 9. axisLabelSize: x-axis label size in pixels. Default is 12
   	10. fillColor: Fill color for mountain chart. eg: #e1e1e1

###Providing AWS SSO token to ecSecurityLineChart

ecSecurityLineChart requires an AWS SSO token to render the chart. Token consists of a **Session Id** and **Institution Id**. The token can be retrieved by calling the QuoteSpeed authentication service. Token received from auth service is to be provided as parameter "awsSsoToken" to the ecSecurityLineChart instance

Below is an example code of retrieving token from QS auth service

	//account parameters for auth service
    var ssoSettings = {
        instId: "QSDEMO",
        email: "qs_demo@morningstar.com",
        group: "PROF",
        loginId: "qs_demo@morningstar.com"
    };

	//parameterized url for the auth service
	var authUrlTemplate = "http://markets-uat.morningstar.com/service/auth?instid={{instId}}&email={{email}}&instuid={{loginId}}&group={{group}}";

	//calls auth service and returns a jquery promise
    var authenticateAwsSso = function() {
        var authUrl = morningstar.asterix.util.url.parameterize(authUrlTemplate, ssoSettings);
        return $.ajax(authUrl);
    };

	//calls the token service to get the session-id. Once the session-id is received it creates a token consisting of this session-id and institution-id
	authenticateAwsSso().done(function(jsonData) {
        var data = JSON.parse(jsonData);
        if (data.attachment && data.attachment.SessionID) {
	        //create an AWS SSO token
            var awsSsoToken = {
                instId: ssoSettings.instId,
                sessionId: data.attachment.SessionID
            };
            morningstar.asterix.context.setToken('awsSsoToken', awsSsoToken);
        } else {
            console.log('AWS SSO authentication failed');
        }
    })
    .fail(function() {
        console.log('AWS SSO authentication failed');
    });

> NOTE: The host for the auth-url should be replaced with an intermediate
> web-page on the client webserver that relays the call to the Markets auth
> service since the Markets auth service will not accept requests from
> user agents (browsers) directly. Also, it is good practice to pass the SSO
> account settings from the web-server rather than from the JavaScript
> and pass an anti-forgery request token to the JS to ensure that
> the call to the intermediate auth service is initiated by the JS from
> the client webpage
