// startref
/// <reference path='../../.references.ts'/>
// endref
module EcFilters {
    export interface Settings extends AsterixNg.ComponentSettings {

        _model: {
            filterValues: AsterixCore.DataSourceSettings;
        }

        _parameters: {
            currencyId: AsterixNg.Parameter;
            filtersSelectedValue: AsterixNg.Parameter;
            languageId: AsterixNg.Parameter;
            showSelect50Filters: AsterixNg.Parameter;
            toggleButtonCaption: AsterixNg.Parameter;
            universeId: AsterixNg.Parameter;
            subUniverseId: AsterixNg.Parameter;
            urlKey: AsterixNg.Parameter;
        };

        _templates: Templates;

        /** Filter object to generate filters
         */
        filters: any;

        /** filters definition setting*/
        filtersDefinition: any;

        /** Filter default values setting*/
        filtersSelectedValue: any;

        /** Filter mappings
         */
        mappings: any

        /** The namespaceClass
         * @default ec-filters
         * @type string
         */
        namespaceClass: string;

        /** Setting to enable reset of universeId
         * @type boolean
         * @default true
         */
        resetUniverseId: boolean;

        /** Setting to toggle the  captions
         * @type string
         */
        toggleButtonCaption: string;

        /** String of subUniverseId
         */
        subUniverseId: string;

        /** String of universeId
         */
        universeId: string;

        /** String of urlKey
         * @type string
         */
        urlKey: string;

    }

    export interface Templates {

        /** The checkbox template.
         * @format html
         * @default <ec-input\r\n    mstar-component-id=\"inputCheckbox[{{ filter.id }}]\"\r\n    data-ng-config=\"filter.config\"\r\n    data-ng-model=\"parameters.filtersSelectedValue[filter.id]\"\r\n    data-input-data=\"filter.values || model.filterValues[filter.id]\">\r\n<\/ec-input>\r\n
         */
        checkbox: string;

        /** The chosen select with select template.
         * @format html
         * @default <ec-chosen\r\n    mstar-component-id=\"chosenSelect[{{ filter.id }}]\"\r\n    data-ng-config=\"filter.config\"\r\n    data-ng-model=\"parameters.filtersSelectedValue[filter.id]\"\r\n    data-input-data=\"filter.values || model.filterValues[filter.id]\">\r\n<\/ec-chosen>\r\n
         */
        chosenSelect: string;

        /** The combo-box template.
         * @format html
         * @default <ec-combo-box\r\n    mstar-component-id=\"comboBox[{{ filter.id }}]\"\r\n    data-ng-config=\"filter.config\"\r\n    data-ng-model=\"parameters.filtersSelectedValue[filter.id]\"\r\n    data-input-data=\"filter.values || model.filterValues[filter.id]\">\r\n<\/ec-combo-box>\r\n
         */
        combo: string;

        /** The combo-box with multi select template.
         * @format html
         * @default <ec-chosen\r\n    mstar-component-id=\"chosenMultiple[{{ filter.id }}]\"\r\n    data-ng-model=\"parameters.filtersSelectedValue[filter.id]\"\r\n    data-ng-config=\"filter.config\"\r\n    data-input-data=\"filter.values || model.filterValues[filter.id]\">\r\n<\/ec-chosen>\r\n
         */
        comboMulti: string;

        /** The equity style box template.
         * @format html
         * @default <ec-asset-style\r\n    mstar-component-id=\"equityStyleBox\"\r\n    data-ng-model=\"parameters.filtersSelectedValue[filter.id]\">\r\n<\/ec-asset-style>\r\n
         */
        equityStyleBox: string;

        /** The fund style box template.
         * @format html
         * @default <ec-asset-style\r\n    mstar-component-id=\"fundStyleBox\"\r\n    data-ng-model=\"parameters.filtersSelectedValue[filter.id]\">\r\n<\/ec-asset-style>\r\n
         */
        fundStyleBox: string;

        /** The main template.
         * @format html
         * @default <div class=\"{{ ::namespaceClass() }}\">\r\n    <form name=\"inputsForm\">\r\n        <ec-section data-template-type=\"light\">\r\n            <ec-container mstar-component-id=\"containerFilter\">\r\n                <ec-section data-template-type=\"light\" data-ng-repeat=\"filter in ::filters track by $index\"  section-id=\"{{filter.id}}\">\r\n                    <div data-ng-if=\"filter.type !== 'template'\" class=\"{{ ::namespaceClass('__filter') }}\">\r\n                        <div class=\"{{ ::namespaceClass('__field-group') }}\">\r\n                            <label\r\n                                class=\"{{ ::namespaceClass('__label') }}\"\r\n                                for=\"{{ ::namespaceId('__['+ filter +']') }}\">\r\n                                <span data-ng-attr-mstar-tooltip-content=\"{{ (filter.labelKeyTooltip && !filter.tooltipOnIcon) ? labels.get(filter.labelKeyTooltip) : undefined }}\"\r\n                                    mstar-tooltip data-ng-bind-html=\"labels.get(filter.labelKey || filter.id)\">\r\n                                <\/span>\r\n                                <span data-ng-if=\"filter.tooltipOnIcon\" class=\"{{ namespaceClass('tooltip-icon') }}\"\r\n                                    data-ng-attr-mstar-tooltip-content=\"{{ filter.labelKeyTooltip ? labels.get(filter.labelKeyTooltip) : undefined }}\"\r\n                                    mstar-tooltip>\r\n                                <\/span>\r\n                            <\/label>\r\n                            <div mstar-include=\"templates[filter.type]\"><\/div>\r\n                        <\/div>\r\n                    <\/div>\r\n                    <div data-ng-if=\"filter.type === 'template'\" class=\"{{ ::namespaceClass('__filter') }}\" mstar-include=\"filter.template\"><\/div>\r\n                <\/ec-section>\r\n                <ec-section section-id=\"actionButtons\" class=\"{{ ::namespaceClass('action-buttons-container') }}\">\r\n                    <div class=\"{{::namespaceClass('__button-wrapper')}}\">\r\n                        <ec-button mstar-component-id=\"buttonReset\"><\/ec-button>\r\n                        <ec-button mstar-component-id=\"buttonViewResult\"><\/ec-button>\r\n                        <ec-button mstar-component-id=\"buttonToggleMoreFilters\"><\/ec-button>\r\n                    <\/div>\r\n                <\/ec-section>\r\n            <\/ec-container>\r\n        <\/ec-section>\r\n    <\/form>\r\n<\/div>\r\n
         */
        main: string;

        /** The radio template.
         * @format html
         * @default <ec-input\r\n    mstar-component-id=\"inputRadio[{{ filter.id }}]\"\r\n    data-ng-model=\"parameters.filtersSelectedValue[filter.id]\"\r\n    data-ng-config=\"filter.config\"\r\n    data-input-data=\"filter.values || model.filterValues[filter.id]\">\r\n<\/ec-input>\r\n
         */
        radio: string;

        /** The range with min max handle template.
         * @format html
         * @default <ec-range-slider\r\n    mstar-component-id=\"sliderMinMax[{{ filter.id }}]\"\r\n    data-ng-config=\"filter.config\"\r\n    data-ng-from=\"parameters.filtersSelectedValue[filter.id].min\"\r\n    data-ng-to=\"parameters.filters[filter.id].max\">\r\n<\/ec-range-slider>\r\n
         */
        rangeDouble: string;

        /** The range slider with only max handle  template.
         * @format html
         * @default <ec-range-slider\r\n    mstar-component-id=\"slider[{{ filter.id }}]\"\r\n    data-ng-config=\"filter.config\"\r\n    data-ng-model=\"parameters.filtersSelectedValue[filter.id]\">\r\n<\/ec-range-slider>\r\n
         */
        rangeSingle: string;

        /** The input box template.
         * @format html
         * @default <ec-input\r\n    mstar-component-id=\"inputText\"\r\n    data-ng-model=\"parameters.filtersSelectedValue[filter.id]\"\r\n    data-ng-config=\"filter.config\">\r\n<\/ec-input>\r\n
         */
        text: string;

    }
}
