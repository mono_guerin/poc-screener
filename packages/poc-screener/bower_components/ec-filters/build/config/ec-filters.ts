// startref
/// <reference path='../../.references.ts'/>
// endref
module EcFilters {
    export enum _Type { ecFilters }

    /**
     * @title Filters
     */
    export interface EcFilters {
        /** Component type.
         * @default ecFilters
         * @options.hidden true
         * @type string
         * @readonly true
         */
        type: _Type;

        settings: Settings;

        labels: Labels;

        components: {
            buttonAdditionFilters: EcButton.EcButton;
            buttonReset: EcButton.EcButton;
            buttonToggleMoreFilters: EcButton.EcButton;
            buttonViewMoreFilters: EcButton.EcButton;
            buttonViewLessFilters: EcButton.EcButton;
            buttonViewResult: EcButton.EcButton;
            containerFilter: EcContainer.EcContainer;
            equityStyleBox: EcAssetStyle.EcAssetStyle;
            fundStyleBox: EcAssetStyle.EcAssetStyle;
        }
    }
}
