// startref
/// <reference path='../../.references.ts'/>
// endref
module EcTabs {
    export enum _Type { ecTabs }

    /**
     * @title Tabs
     */
    export interface EcTabs {
        /** Component type.
         * @default ecTabs
         * @options.hidden true
         * @type string
         * @readonly true
         */
        type: _Type;

        settings: Settings;

        labels: Labels;
    }
}
