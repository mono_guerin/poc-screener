// startref
/// <reference path='../../.references.ts'/>
// endref
module EcTabs {
    export interface Settings extends AsterixNg.ComponentSettings {
        _parameters: {
            selectedTabIndex: AsterixNg.Parameter;
            resetTabStatus: AsterixNg.Parameter;
        };

        _templates: Templates;

        /**
        @default tabs
        */
        controlType: string;

        /**
        @default true
        */
        dropdownCaret: boolean;

        /**
        @default ec-tabs
        */
        namespaceClass: string;

        /** When true, to reset all tabs to be inactive.
         * @default false
         */
        resetTabStatus: boolean;

        /**
        @default 0
        */
        selectedTabIndex: number;

        /**
         * When true, show the tab panel.
         * This allows the option to hide it when it isn't needed.
         * @default true
         */
        showTabPanel: boolean;

        /**
        @default tabs
        */
        tabType: string;
    }

    export interface Templates {
        /**
         * @format html
           @default <div class=\"{{::namespaceClass('__dropdown')}}\" data-ng-class=\"open ? namespaceClass('__dropdown--open') : ''\">\r\n    <button class=\"{{::namespaceClass('__btn') + ' ' + namespaceClass('__dropdown-toggle')}}\"\r\n            id=\"{{::namespaceId('-dropdown-btn')}}\"\r\n            data-ng-click=\"open = !open; $event.stopPropagation();\"\r\n            type=\"button\"\r\n            aria-haspopup=\"true\"\r\n            aria-expanded=\"{{open}}\">\r\n        {{ getTitle() }}\r\n        <span class=\"ec-caret\" data-ng-if=\"::settings.get('dropdownCaret') === true\"><\/span>\r\n    <\/button>\r\n    <ul class=\"{{::namespaceClass('__dropdown-menu')}}\"\r\n        data-ng-class=\"open ? namespaceClass('__dropdown-menu--open') : ''\"\r\n        role=\"menu\"\r\n        aria-labelledby=\"{{::namespaceId('-dropdown-btn')}}\">\r\n        <li role=\"presentation\" class=\"{{::namespaceClass('__dropdown-item')}}\" data-ng-class=\"[ tab.active ? namespaceClass('__tab--active') : '', tab.header ? namespaceClass('__tab--header') : '', tab.separator ? namespaceClass('__tab--separator') : '']\" data-ng-repeat=\"tab in tabs\">\r\n            <a href=\"javascript:;\"\r\n                class=\"{{::namespaceClass('__dropdown-link')}}\"\r\n                role=\"menuitem\"\r\n                id=\"{{::namespaceId('-tab') + $index}}\"\r\n                data-ng-click=\"setActiveTab(tab)\"\r\n                aria-selected=\"{{tab.active}}\">\r\n                {{ tab.name }}\r\n            <\/a>\r\n        <\/li>\r\n    <\/ul>\r\n<\/div>\r\n
         */
        dropdown: string;

        /** The main template.
         * @format html
         * @default <div mstar-include=\"templates.tabs\" use-parent-scope=\"true\" data-ng-if=\"settings.get('controlType') === 'tabs'\"><\/div>\r\n<div mstar-include=\"templates.dropdown\" use-parent-scope=\"true\" data-ng-if=\"settings.get('controlType') === 'dropdown'\"><\/div>\r\n<div data-ng-show=\"showTabPanel\" data-ng-class=\"{'{{::namespaceClass('__tab-content--standard')}}' : settings.get('tabType') === 'standard'}\" class=\"{{::namespaceClass('__tab-content')}}\" id=\"{{::namespaceId('-tab-content')}}\" role=\"tabpanel\" tabindex=\"-1\" data-ng-transclude><\/div>\r\n
         */
        main: string;

        /**
         * @format html
           @default <ul class=\"{{::namespaceClass('__tabs')}}\" role=\"tablist\" data-ng-class=\"settings.get('tabType') === 'standard' ? namespaceClass('__tabs--standard') : ''\">\r\n    <li role=\"presentation\" class=\"{{::namespaceClass('__tab')}}\" data-ng-class=\"tab.active ? namespaceClass('__tab--active') : ''\" data-ng-repeat=\"tab in tabs\">\r\n        <a href=\"javascript:;\"\r\n            class=\"{{::namespaceClass('__link')}}\"\r\n            role=\"tab\"\r\n            id=\"{{namespaceId('-tab') + $index}}\"\r\n            data-ng-click=\"setActiveTab(tab)\"\r\n            data-ng-keypress=\"tabKeyPress(tab, $event)\"\r\n            aria-selected=\"{{tab.active}}\"\r\n            aria-label=\"{{tab.ariaLabel ? tab.ariaLabel : tab.name}}\"\r\n            mstar-tooltip mstar-tooltip-content=\"{{settings.get('tooltip').trigger == 'hover' ? tab.tooltip : ''}}\">{{ tab.name }}<\/a>\r\n    <\/li>\r\n<\/ul>\r\n
         */
        tabs: string;
    }
}
