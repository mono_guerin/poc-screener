import Vue from 'vue';
import VueCustomElement from 'vue-custom-element';
Vue.use(VueCustomElement);

import Counter from './mwc-counter.vue';
Vue.customElement('mwc-counter', Counter);

export default Counter;