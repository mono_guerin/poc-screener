const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');

const basePath = path.resolve(__dirname, "");
const modulesPath = path.join(basePath, "node_modules");

module.exports = {
    entry: './src/main.js',
    plugins: [
        new VueLoaderPlugin()
    ],
    module: {
        rules: [{
            test: /\.(js)$/,
            exclude: modulesPath,
            loader: "babel-loader"
        },
        {
            test: /\.vue$/,
            loader: 'vue-loader',
            options: {
                loaders: {}
            }
        }]
    },
    output: {
        filename: 'main.js',
        libraryTarget: 'umd',
        path: path.resolve(basePath, 'dist')
    },
    externals: {
        vue: 'vue',
        'vue-custom-element': 'vue-custom-element'
    }
};
